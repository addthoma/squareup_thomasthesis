.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Catalog"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$ProtoAdapter_Catalog;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DISPLAY_MODIFIER_INSTEAD_OF_OPTION:Ljava/lang/Boolean;

.field public static final DEFAULT_DUPLICATE_SKU_ITEMS_APPLET:Ljava/lang/Boolean;

.field public static final DEFAULT_ENABLE_ITEM_OPTIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_ENABLE_ITEM_OPTIONS_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_ITEM_OPTIONS_EDIT:Ljava/lang/Boolean;

.field public static final DEFAULT_ITEM_OPTIONS_GLOBAL_EDIT:Ljava/lang/Boolean;

.field public static final DEFAULT_LIMIT_VARIATIONS_PER_ITEM:Ljava/lang/Boolean;

.field public static final DEFAULT_PURGE_CATALOG_AFTER_SIGNOUT:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOULD_DISALLOW_ITEMSFE_INVENTORY_API:Ljava/lang/Boolean;

.field public static final DEFAULT_X2_PERFORM_BACKGROUND_SYNC_AFTER_TRANSACTION:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final display_modifier_instead_of_option:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final duplicate_sku_items_applet:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final enable_item_options:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x11
    .end annotation
.end field

.field public final enable_item_options_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xe
    .end annotation
.end field

.field public final item_options_edit:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xf
    .end annotation
.end field

.field public final item_options_global_edit:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x12
    .end annotation
.end field

.field public final limit_variations_per_item:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final purge_catalog_after_signout:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final should_disallow_itemsfe_inventory_api:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final x2_perform_background_sync_after_transaction:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 3840
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$ProtoAdapter_Catalog;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$ProtoAdapter_Catalog;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 3842
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 3846
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_SHOULD_DISALLOW_ITEMSFE_INVENTORY_API:Ljava/lang/Boolean;

    .line 3848
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_PURGE_CATALOG_AFTER_SIGNOUT:Ljava/lang/Boolean;

    .line 3850
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_X2_PERFORM_BACKGROUND_SYNC_AFTER_TRANSACTION:Ljava/lang/Boolean;

    .line 3852
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_DUPLICATE_SKU_ITEMS_APPLET:Ljava/lang/Boolean;

    .line 3854
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_DISPLAY_MODIFIER_INSTEAD_OF_OPTION:Ljava/lang/Boolean;

    .line 3856
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_LIMIT_VARIATIONS_PER_ITEM:Ljava/lang/Boolean;

    .line 3858
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_ENABLE_ITEM_OPTIONS_ANDROID:Ljava/lang/Boolean;

    .line 3860
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_ITEM_OPTIONS_EDIT:Ljava/lang/Boolean;

    .line 3862
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_ENABLE_ITEM_OPTIONS:Ljava/lang/Boolean;

    .line 3864
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_ITEM_OPTIONS_GLOBAL_EDIT:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 12

    .line 3950
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 3961
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 3962
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->should_disallow_itemsfe_inventory_api:Ljava/lang/Boolean;

    .line 3963
    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->purge_catalog_after_signout:Ljava/lang/Boolean;

    .line 3964
    iput-object p3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->x2_perform_background_sync_after_transaction:Ljava/lang/Boolean;

    .line 3965
    iput-object p4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->duplicate_sku_items_applet:Ljava/lang/Boolean;

    .line 3966
    iput-object p5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->display_modifier_instead_of_option:Ljava/lang/Boolean;

    .line 3967
    iput-object p6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->limit_variations_per_item:Ljava/lang/Boolean;

    .line 3968
    iput-object p7, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options_android:Ljava/lang/Boolean;

    .line 3969
    iput-object p8, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_edit:Ljava/lang/Boolean;

    .line 3970
    iput-object p9, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options:Ljava/lang/Boolean;

    .line 3971
    iput-object p10, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_global_edit:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 4078
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 3994
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 3995
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    .line 3996
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->should_disallow_itemsfe_inventory_api:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->should_disallow_itemsfe_inventory_api:Ljava/lang/Boolean;

    .line 3997
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->purge_catalog_after_signout:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->purge_catalog_after_signout:Ljava/lang/Boolean;

    .line 3998
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->x2_perform_background_sync_after_transaction:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->x2_perform_background_sync_after_transaction:Ljava/lang/Boolean;

    .line 3999
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->duplicate_sku_items_applet:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->duplicate_sku_items_applet:Ljava/lang/Boolean;

    .line 4000
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->display_modifier_instead_of_option:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->display_modifier_instead_of_option:Ljava/lang/Boolean;

    .line 4001
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->limit_variations_per_item:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->limit_variations_per_item:Ljava/lang/Boolean;

    .line 4002
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options_android:Ljava/lang/Boolean;

    .line 4003
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_edit:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_edit:Ljava/lang/Boolean;

    .line 4004
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options:Ljava/lang/Boolean;

    .line 4005
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_global_edit:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_global_edit:Ljava/lang/Boolean;

    .line 4006
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 4011
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_a

    .line 4013
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 4014
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->should_disallow_itemsfe_inventory_api:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4015
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->purge_catalog_after_signout:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4016
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->x2_perform_background_sync_after_transaction:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4017
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->duplicate_sku_items_applet:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4018
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->display_modifier_instead_of_option:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4019
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->limit_variations_per_item:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4020
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4021
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_edit:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4022
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4023
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_global_edit:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 4024
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;
    .locals 2

    .line 3976
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;-><init>()V

    .line 3977
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->should_disallow_itemsfe_inventory_api:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->should_disallow_itemsfe_inventory_api:Ljava/lang/Boolean;

    .line 3978
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->purge_catalog_after_signout:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->purge_catalog_after_signout:Ljava/lang/Boolean;

    .line 3979
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->x2_perform_background_sync_after_transaction:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->x2_perform_background_sync_after_transaction:Ljava/lang/Boolean;

    .line 3980
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->duplicate_sku_items_applet:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->duplicate_sku_items_applet:Ljava/lang/Boolean;

    .line 3981
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->display_modifier_instead_of_option:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->display_modifier_instead_of_option:Ljava/lang/Boolean;

    .line 3982
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->limit_variations_per_item:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->limit_variations_per_item:Ljava/lang/Boolean;

    .line 3983
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->enable_item_options_android:Ljava/lang/Boolean;

    .line 3984
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_edit:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->item_options_edit:Ljava/lang/Boolean;

    .line 3985
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->enable_item_options:Ljava/lang/Boolean;

    .line 3986
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_global_edit:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->item_options_global_edit:Ljava/lang/Boolean;

    .line 3987
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 3839
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;
    .locals 2

    .line 4064
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->should_disallow_itemsfe_inventory_api:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->should_disallow_itemsfe_inventory_api:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->should_disallow_itemsfe_inventory_api(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4065
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->purge_catalog_after_signout:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->purge_catalog_after_signout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->purge_catalog_after_signout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4066
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->x2_perform_background_sync_after_transaction:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->x2_perform_background_sync_after_transaction:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->x2_perform_background_sync_after_transaction(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4067
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->duplicate_sku_items_applet:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->duplicate_sku_items_applet:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->duplicate_sku_items_applet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4068
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->display_modifier_instead_of_option:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->display_modifier_instead_of_option:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->display_modifier_instead_of_option(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4069
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->limit_variations_per_item:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->limit_variations_per_item:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->limit_variations_per_item(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4070
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->enable_item_options_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4071
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_edit:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_edit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->item_options_edit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4072
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->enable_item_options(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4073
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_global_edit:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_global_edit:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->item_options_global_edit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    :cond_9
    if-nez v1, :cond_a

    move-object p1, p0

    goto :goto_0

    .line 4074
    :cond_a
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 3839
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;
    .locals 2

    .line 4048
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->should_disallow_itemsfe_inventory_api:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_SHOULD_DISALLOW_ITEMSFE_INVENTORY_API:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->should_disallow_itemsfe_inventory_api(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4049
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->purge_catalog_after_signout:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_PURGE_CATALOG_AFTER_SIGNOUT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->purge_catalog_after_signout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4050
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->x2_perform_background_sync_after_transaction:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_X2_PERFORM_BACKGROUND_SYNC_AFTER_TRANSACTION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->x2_perform_background_sync_after_transaction(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4051
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->duplicate_sku_items_applet:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_DUPLICATE_SKU_ITEMS_APPLET:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->duplicate_sku_items_applet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4052
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->display_modifier_instead_of_option:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_DISPLAY_MODIFIER_INSTEAD_OF_OPTION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->display_modifier_instead_of_option(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4053
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->limit_variations_per_item:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_LIMIT_VARIATIONS_PER_ITEM:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->limit_variations_per_item(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4054
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options_android:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_ENABLE_ITEM_OPTIONS_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->enable_item_options_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4055
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_edit:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_ITEM_OPTIONS_EDIT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->item_options_edit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4056
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_ENABLE_ITEM_OPTIONS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->enable_item_options(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    .line 4057
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_global_edit:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->DEFAULT_ITEM_OPTIONS_GLOBAL_EDIT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->item_options_global_edit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;

    move-result-object v1

    :cond_9
    if-nez v1, :cond_a

    move-object v0, p0

    goto :goto_0

    .line 4058
    :cond_a
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 3839
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4031
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4032
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->should_disallow_itemsfe_inventory_api:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", should_disallow_itemsfe_inventory_api="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->should_disallow_itemsfe_inventory_api:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4033
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->purge_catalog_after_signout:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", purge_catalog_after_signout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->purge_catalog_after_signout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4034
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->x2_perform_background_sync_after_transaction:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", x2_perform_background_sync_after_transaction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->x2_perform_background_sync_after_transaction:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4035
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->duplicate_sku_items_applet:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", duplicate_sku_items_applet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->duplicate_sku_items_applet:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4036
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->display_modifier_instead_of_option:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", display_modifier_instead_of_option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->display_modifier_instead_of_option:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4037
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->limit_variations_per_item:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", limit_variations_per_item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->limit_variations_per_item:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4038
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", enable_item_options_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4039
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_edit:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", item_options_edit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_edit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4040
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", enable_item_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4041
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_global_edit:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", item_options_global_edit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_global_edit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Catalog{"

    .line 4042
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
