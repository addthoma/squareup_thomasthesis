.class public final enum Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;
.super Ljava/lang/Enum;
.source "LoyaltyProgram.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/LoyaltyProgram;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ProgramStatus"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus$ProtoAdapter_ProgramStatus;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

.field public static final enum ACTIVE:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FROZEN:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

.field public static final enum INACTIVE:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

.field public static final enum NONE:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

.field public static final enum UNKNOWN:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 311
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->UNKNOWN:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    .line 316
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    const/4 v2, 0x1

    const-string v3, "NONE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->NONE:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    .line 321
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    const/4 v3, 0x2

    const/4 v4, 0x3

    const-string v5, "FROZEN"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->FROZEN:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    .line 326
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    const/4 v5, 0x4

    const-string v6, "ACTIVE"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->ACTIVE:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    .line 331
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    const/4 v6, 0x5

    const-string v7, "INACTIVE"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->INACTIVE:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    new-array v0, v6, [Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    .line 310
    sget-object v6, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->UNKNOWN:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->NONE:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->FROZEN:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->ACTIVE:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->INACTIVE:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->$VALUES:[Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    .line 333
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus$ProtoAdapter_ProgramStatus;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus$ProtoAdapter_ProgramStatus;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 337
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 338
    iput p3, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 350
    :cond_0
    sget-object p0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->INACTIVE:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    return-object p0

    .line 349
    :cond_1
    sget-object p0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->ACTIVE:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    return-object p0

    .line 348
    :cond_2
    sget-object p0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->FROZEN:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    return-object p0

    .line 347
    :cond_3
    sget-object p0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->NONE:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    return-object p0

    .line 346
    :cond_4
    sget-object p0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->UNKNOWN:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;
    .locals 1

    .line 310
    const-class v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;
    .locals 1

    .line 310
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->$VALUES:[Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    invoke-virtual {v0}, [Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 357
    iget v0, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->value:I

    return v0
.end method
