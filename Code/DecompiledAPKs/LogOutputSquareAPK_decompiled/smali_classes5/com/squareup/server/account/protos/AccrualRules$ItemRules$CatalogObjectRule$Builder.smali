.class public final Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AccrualRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;",
        "Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

.field public points:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1080
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;
    .locals 4

    .line 1101
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;->points:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;->catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;-><init>(Ljava/lang/Long;Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1075
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object(Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;
    .locals 0

    .line 1095
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;->catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    return-object p0
.end method

.method public points(Ljava/lang/Long;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;
    .locals 0

    .line 1087
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;->points:Ljava/lang/Long;

    return-object p0
.end method
