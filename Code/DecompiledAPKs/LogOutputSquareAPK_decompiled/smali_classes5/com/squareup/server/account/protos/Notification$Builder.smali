.class public final Lcom/squareup/server/account/protos/Notification$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Notification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/Notification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/Notification;",
        "Lcom/squareup/server/account/protos/Notification$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public button:Lcom/squareup/server/account/protos/Notification$Button;

.field public buttons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/Notification$Button;",
            ">;"
        }
    .end annotation
.end field

.field public delivery_id:Ljava/lang/String;

.field public lock_out:Ljava/lang/Boolean;

.field public message:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 206
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 207
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/Notification$Builder;->buttons:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/Notification;
    .locals 9

    .line 243
    new-instance v8, Lcom/squareup/server/account/protos/Notification;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification$Builder;->delivery_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/server/account/protos/Notification$Builder;->title:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/server/account/protos/Notification$Builder;->message:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/server/account/protos/Notification$Builder;->button:Lcom/squareup/server/account/protos/Notification$Button;

    iget-object v5, p0, Lcom/squareup/server/account/protos/Notification$Builder;->buttons:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/server/account/protos/Notification$Builder;->lock_out:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/server/account/protos/Notification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/server/account/protos/Notification$Button;Ljava/util/List;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 193
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Notification$Builder;->build()Lcom/squareup/server/account/protos/Notification;

    move-result-object v0

    return-object v0
.end method

.method public button(Lcom/squareup/server/account/protos/Notification$Button;)Lcom/squareup/server/account/protos/Notification$Builder;
    .locals 0

    .line 226
    iput-object p1, p0, Lcom/squareup/server/account/protos/Notification$Builder;->button:Lcom/squareup/server/account/protos/Notification$Button;

    return-object p0
.end method

.method public buttons(Ljava/util/List;)Lcom/squareup/server/account/protos/Notification$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/Notification$Button;",
            ">;)",
            "Lcom/squareup/server/account/protos/Notification$Builder;"
        }
    .end annotation

    .line 231
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 232
    iput-object p1, p0, Lcom/squareup/server/account/protos/Notification$Builder;->buttons:Ljava/util/List;

    return-object p0
.end method

.method public delivery_id(Ljava/lang/String;)Lcom/squareup/server/account/protos/Notification$Builder;
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/server/account/protos/Notification$Builder;->delivery_id:Ljava/lang/String;

    return-object p0
.end method

.method public lock_out(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Notification$Builder;
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/server/account/protos/Notification$Builder;->lock_out:Ljava/lang/Boolean;

    return-object p0
.end method

.method public message(Ljava/lang/String;)Lcom/squareup/server/account/protos/Notification$Builder;
    .locals 0

    .line 221
    iput-object p1, p0, Lcom/squareup/server/account/protos/Notification$Builder;->message:Ljava/lang/String;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/server/account/protos/Notification$Builder;
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/server/account/protos/Notification$Builder;->title:Ljava/lang/String;

    return-object p0
.end method
