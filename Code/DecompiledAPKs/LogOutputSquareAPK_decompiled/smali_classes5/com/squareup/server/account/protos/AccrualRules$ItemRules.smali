.class public final Lcom/squareup/server/account/protos/AccrualRules$ItemRules;
.super Lcom/squareup/wire/AndroidMessage;
.source "AccrualRules.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/AccrualRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ItemRules"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/AccrualRules$ItemRules$ProtoAdapter_ItemRules;,
        Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;,
        Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/AccrualRules$ItemRules;",
        "Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/AccrualRules$ItemRules;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/AccrualRules$ItemRules;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/AccrualRules$ItemRules;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/AccrualRules$ItemRules;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final catalog_object_rules:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.AccrualRules$ItemRules$CatalogObjectRule#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 863
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$ProtoAdapter_ItemRules;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$ProtoAdapter_ItemRules;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 865
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;",
            ">;)V"
        }
    .end annotation

    .line 882
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 886
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p2, "catalog_object_rules"

    .line 887
    invoke-static {p2, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->catalog_object_rules:Ljava/util/List;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 943
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->newBuilder()Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 901
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 902
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    .line 903
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->catalog_object_rules:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->catalog_object_rules:Ljava/util/List;

    .line 904
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 909
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_0

    .line 911
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 912
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->catalog_object_rules:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 913
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_0
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;
    .locals 2

    .line 892
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;-><init>()V

    .line 893
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->catalog_object_rules:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;->catalog_object_rules:Ljava/util/List;

    .line 894
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 862
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->newBuilder()Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/AccrualRules$ItemRules;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules;
    .locals 2

    .line 938
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->catalog_object_rules:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->catalog_object_rules:Ljava/util/List;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;->catalog_object_rules(Ljava/util/List;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object p1, p0

    goto :goto_0

    .line 939
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 862
    check-cast p1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->overlay(Lcom/squareup/server/account/protos/AccrualRules$ItemRules;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/AccrualRules$ItemRules;
    .locals 3

    .line 928
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->catalog_object_rules:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 929
    invoke-static {v0}, Lcom/squareup/wired/Wired;->populateDefaults(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 930
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->catalog_object_rules:Ljava/util/List;

    if-eq v0, v2, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;->catalog_object_rules(Ljava/util/List;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object v0, p0

    goto :goto_0

    .line 932
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 862
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->populateDefaults()Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 920
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 921
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->catalog_object_rules:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", catalog_object_rules="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->catalog_object_rules:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ItemRules{"

    .line 922
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
