.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Posfeatures"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$ProtoAdapter_Posfeatures;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SHOW_DISCARD_ON_ERROR_ON_JAILSCREEN:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final show_discard_on_error_on_jailscreen:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11622
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$ProtoAdapter_Posfeatures;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$ProtoAdapter_Posfeatures;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 11624
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 11628
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->DEFAULT_SHOW_DISCARD_ON_ERROR_ON_JAILSCREEN:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 1

    .line 11641
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 11646
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 11647
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->show_discard_on_error_on_jailscreen:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 11700
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 11661
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 11662
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    .line 11663
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->show_discard_on_error_on_jailscreen:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->show_discard_on_error_on_jailscreen:Ljava/lang/Boolean;

    .line 11664
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 11669
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_1

    .line 11671
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 11672
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->show_discard_on_error_on_jailscreen:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 11673
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;
    .locals 2

    .line 11652
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;-><init>()V

    .line 11653
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->show_discard_on_error_on_jailscreen:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;->show_discard_on_error_on_jailscreen:Ljava/lang/Boolean;

    .line 11654
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 11621
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;
    .locals 2

    .line 11695
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->show_discard_on_error_on_jailscreen:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->show_discard_on_error_on_jailscreen:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;->show_discard_on_error_on_jailscreen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object p1, p0

    goto :goto_0

    .line 11696
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 11621
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;
    .locals 2

    .line 11688
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->show_discard_on_error_on_jailscreen:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->DEFAULT_SHOW_DISCARD_ON_ERROR_ON_JAILSCREEN:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;->show_discard_on_error_on_jailscreen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object v0, p0

    goto :goto_0

    .line 11689
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 11621
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 11680
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 11681
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->show_discard_on_error_on_jailscreen:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", show_discard_on_error_on_jailscreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->show_discard_on_error_on_jailscreen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Posfeatures{"

    .line 11682
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
