.class public final Lcom/squareup/server/account/protos/AppointmentSettings$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AppointmentSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/AppointmentSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/AppointmentSettings;",
        "Lcom/squareup/server/account/protos/AppointmentSettings$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public recently_joined_appointments:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 114
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/AppointmentSettings;
    .locals 3

    .line 127
    new-instance v0, Lcom/squareup/server/account/protos/AppointmentSettings;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AppointmentSettings$Builder;->recently_joined_appointments:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/server/account/protos/AppointmentSettings;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 111
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AppointmentSettings$Builder;->build()Lcom/squareup/server/account/protos/AppointmentSettings;

    move-result-object v0

    return-object v0
.end method

.method public recently_joined_appointments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AppointmentSettings$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/server/account/protos/AppointmentSettings$Builder;->recently_joined_appointments:Ljava/lang/Boolean;

    return-object p0
.end method
