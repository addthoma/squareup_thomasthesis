.class public final Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AccrualRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;",
        "Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public include_tax:Ljava/lang/Boolean;

.field public points:Ljava/lang/Long;

.field public spend:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 772
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;
    .locals 5

    .line 805
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->points:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->spend:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->include_tax:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/Money;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 765
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    move-result-object v0

    return-object v0
.end method

.method public include_tax(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;
    .locals 0

    .line 799
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->include_tax:Ljava/lang/Boolean;

    return-object p0
.end method

.method public points(Ljava/lang/Long;)Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;
    .locals 0

    .line 779
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->points:Ljava/lang/Long;

    return-object p0
.end method

.method public spend(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;
    .locals 0

    .line 789
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->spend:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
