.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Restaurants"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$ProtoAdapter_Restaurants;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CAN_CHANGE_EMPLOYEE_DEFAULTS:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_CHANGE_OT_DEFAULTS:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_SEE_AUTO_GRAT:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_SEE_COVER_COUNTS:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_SEE_SEATING:Ljava/lang/Boolean;

.field public static final DEFAULT_MANUAL_86:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_AUTO_GRATUITIES:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_COMP_VOID_ASSIGN_MOVE_TICKET:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CONFIGURED_BUSINESS_HOURS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_RST_FRIENDLY_T2:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_SALES_LIMITS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_SPLIT_TICKET:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_TABLE_MANAGEMENT:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final can_change_employee_defaults:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final can_change_ot_defaults:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final can_see_auto_grat:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final can_see_cover_counts:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final can_see_seating:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final manual_86:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final use_auto_gratuities:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xd
    .end annotation
.end field

.field public final use_comp_void_assign_move_ticket:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final use_configured_business_hours:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final use_rst_friendly_t2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final use_sales_limits:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final use_split_ticket:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final use_table_management:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16056
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$ProtoAdapter_Restaurants;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$ProtoAdapter_Restaurants;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 16058
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 16062
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_USE_RST_FRIENDLY_T2:Ljava/lang/Boolean;

    .line 16064
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_CAN_CHANGE_OT_DEFAULTS:Ljava/lang/Boolean;

    .line 16066
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_CAN_CHANGE_EMPLOYEE_DEFAULTS:Ljava/lang/Boolean;

    .line 16068
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_CAN_SEE_SEATING:Ljava/lang/Boolean;

    .line 16070
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_CAN_SEE_AUTO_GRAT:Ljava/lang/Boolean;

    .line 16072
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_CAN_SEE_COVER_COUNTS:Ljava/lang/Boolean;

    .line 16074
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_MANUAL_86:Ljava/lang/Boolean;

    .line 16076
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_USE_CONFIGURED_BUSINESS_HOURS:Ljava/lang/Boolean;

    .line 16078
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_USE_SALES_LIMITS:Ljava/lang/Boolean;

    .line 16080
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_USE_COMP_VOID_ASSIGN_MOVE_TICKET:Ljava/lang/Boolean;

    .line 16082
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_USE_SPLIT_TICKET:Ljava/lang/Boolean;

    .line 16084
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_USE_TABLE_MANAGEMENT:Ljava/lang/Boolean;

    .line 16086
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_USE_AUTO_GRATUITIES:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 15

    .line 16225
    sget-object v14, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 16236
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p14}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 16237
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_rst_friendly_t2:Ljava/lang/Boolean;

    .line 16238
    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_ot_defaults:Ljava/lang/Boolean;

    .line 16239
    iput-object p3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_employee_defaults:Ljava/lang/Boolean;

    .line 16240
    iput-object p4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_seating:Ljava/lang/Boolean;

    .line 16241
    iput-object p5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_auto_grat:Ljava/lang/Boolean;

    .line 16242
    iput-object p6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_cover_counts:Ljava/lang/Boolean;

    .line 16243
    iput-object p7, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->manual_86:Ljava/lang/Boolean;

    .line 16244
    iput-object p8, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_configured_business_hours:Ljava/lang/Boolean;

    .line 16245
    iput-object p9, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_sales_limits:Ljava/lang/Boolean;

    .line 16246
    iput-object p10, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_comp_void_assign_move_ticket:Ljava/lang/Boolean;

    .line 16247
    iput-object p11, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_split_ticket:Ljava/lang/Boolean;

    .line 16248
    iput-object p12, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_table_management:Ljava/lang/Boolean;

    .line 16249
    iput-object p13, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_auto_gratuities:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 16374
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 16275
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 16276
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    .line 16277
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_rst_friendly_t2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_rst_friendly_t2:Ljava/lang/Boolean;

    .line 16278
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_ot_defaults:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_ot_defaults:Ljava/lang/Boolean;

    .line 16279
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_employee_defaults:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_employee_defaults:Ljava/lang/Boolean;

    .line 16280
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_seating:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_seating:Ljava/lang/Boolean;

    .line 16281
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_auto_grat:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_auto_grat:Ljava/lang/Boolean;

    .line 16282
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_cover_counts:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_cover_counts:Ljava/lang/Boolean;

    .line 16283
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->manual_86:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->manual_86:Ljava/lang/Boolean;

    .line 16284
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_configured_business_hours:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_configured_business_hours:Ljava/lang/Boolean;

    .line 16285
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_sales_limits:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_sales_limits:Ljava/lang/Boolean;

    .line 16286
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_comp_void_assign_move_ticket:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_comp_void_assign_move_ticket:Ljava/lang/Boolean;

    .line 16287
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_split_ticket:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_split_ticket:Ljava/lang/Boolean;

    .line 16288
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_table_management:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_table_management:Ljava/lang/Boolean;

    .line 16289
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_auto_gratuities:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_auto_gratuities:Ljava/lang/Boolean;

    .line 16290
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 16295
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_d

    .line 16297
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 16298
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_rst_friendly_t2:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16299
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_ot_defaults:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16300
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_employee_defaults:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16301
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_seating:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16302
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_auto_grat:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16303
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_cover_counts:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16304
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->manual_86:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16305
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_configured_business_hours:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16306
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_sales_limits:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16307
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_comp_void_assign_move_ticket:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16308
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_split_ticket:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16309
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_table_management:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16310
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_auto_gratuities:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_c
    add-int/2addr v0, v2

    .line 16311
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_d
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
    .locals 2

    .line 16254
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;-><init>()V

    .line 16255
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_rst_friendly_t2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_rst_friendly_t2:Ljava/lang/Boolean;

    .line 16256
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_ot_defaults:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_change_ot_defaults:Ljava/lang/Boolean;

    .line 16257
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_employee_defaults:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_change_employee_defaults:Ljava/lang/Boolean;

    .line 16258
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_seating:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_see_seating:Ljava/lang/Boolean;

    .line 16259
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_auto_grat:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_see_auto_grat:Ljava/lang/Boolean;

    .line 16260
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_cover_counts:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_see_cover_counts:Ljava/lang/Boolean;

    .line 16261
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->manual_86:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->manual_86:Ljava/lang/Boolean;

    .line 16262
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_configured_business_hours:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_configured_business_hours:Ljava/lang/Boolean;

    .line 16263
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_sales_limits:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_sales_limits:Ljava/lang/Boolean;

    .line 16264
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_comp_void_assign_move_ticket:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_comp_void_assign_move_ticket:Ljava/lang/Boolean;

    .line 16265
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_split_ticket:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_split_ticket:Ljava/lang/Boolean;

    .line 16266
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_table_management:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_table_management:Ljava/lang/Boolean;

    .line 16267
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_auto_gratuities:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_auto_gratuities:Ljava/lang/Boolean;

    .line 16268
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 16055
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;
    .locals 2

    .line 16357
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_rst_friendly_t2:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_rst_friendly_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_rst_friendly_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16358
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_ot_defaults:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_ot_defaults:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_change_ot_defaults(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16359
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_employee_defaults:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_employee_defaults:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_change_employee_defaults(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16360
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_seating:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_seating:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_see_seating(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16361
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_auto_grat:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_auto_grat:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_see_auto_grat(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16362
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_cover_counts:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_cover_counts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_see_cover_counts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16363
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->manual_86:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->manual_86:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->manual_86(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16364
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_configured_business_hours:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_configured_business_hours:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_configured_business_hours(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16365
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_sales_limits:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_sales_limits:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_sales_limits(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16366
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_comp_void_assign_move_ticket:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_comp_void_assign_move_ticket:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_comp_void_assign_move_ticket(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16367
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_split_ticket:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_split_ticket:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_split_ticket(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16368
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_table_management:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_table_management:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_table_management(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16369
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_auto_gratuities:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_auto_gratuities:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_auto_gratuities(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    :cond_c
    if-nez v1, :cond_d

    move-object p1, p0

    goto :goto_0

    .line 16370
    :cond_d
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 16055
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;
    .locals 2

    .line 16338
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_rst_friendly_t2:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_USE_RST_FRIENDLY_T2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_rst_friendly_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16339
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_ot_defaults:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_CAN_CHANGE_OT_DEFAULTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_change_ot_defaults(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16340
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_employee_defaults:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_CAN_CHANGE_EMPLOYEE_DEFAULTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_change_employee_defaults(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16341
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_seating:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_CAN_SEE_SEATING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_see_seating(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16342
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_auto_grat:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_CAN_SEE_AUTO_GRAT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_see_auto_grat(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16343
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_cover_counts:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_CAN_SEE_COVER_COUNTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_see_cover_counts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16344
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->manual_86:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_MANUAL_86:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->manual_86(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16345
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_configured_business_hours:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_USE_CONFIGURED_BUSINESS_HOURS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_configured_business_hours(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16346
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_sales_limits:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_USE_SALES_LIMITS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_sales_limits(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16347
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_comp_void_assign_move_ticket:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_USE_COMP_VOID_ASSIGN_MOVE_TICKET:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_comp_void_assign_move_ticket(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16348
    :cond_9
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_split_ticket:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_USE_SPLIT_TICKET:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_split_ticket(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16349
    :cond_a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_table_management:Ljava/lang/Boolean;

    if-nez v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_USE_TABLE_MANAGEMENT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_table_management(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    .line 16350
    :cond_b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_auto_gratuities:Ljava/lang/Boolean;

    if-nez v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->DEFAULT_USE_AUTO_GRATUITIES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_auto_gratuities(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;

    move-result-object v1

    :cond_c
    if-nez v1, :cond_d

    move-object v0, p0

    goto :goto_0

    .line 16351
    :cond_d
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 16055
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 16318
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 16319
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_rst_friendly_t2:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", use_rst_friendly_t2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_rst_friendly_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16320
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_ot_defaults:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", can_change_ot_defaults="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_ot_defaults:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16321
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_employee_defaults:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", can_change_employee_defaults="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_employee_defaults:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16322
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_seating:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", can_see_seating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_seating:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16323
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_auto_grat:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", can_see_auto_grat="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_auto_grat:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16324
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_cover_counts:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", can_see_cover_counts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_cover_counts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16325
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->manual_86:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", manual_86="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->manual_86:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16326
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_configured_business_hours:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", use_configured_business_hours="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_configured_business_hours:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16327
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_sales_limits:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", use_sales_limits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_sales_limits:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16328
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_comp_void_assign_move_ticket:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", use_comp_void_assign_move_ticket="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_comp_void_assign_move_ticket:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16329
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_split_ticket:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", use_split_ticket="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_split_ticket:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16330
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_table_management:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", use_table_management="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_table_management:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16331
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_auto_gratuities:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", use_auto_gratuities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_auto_gratuities:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Restaurants{"

    .line 16332
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
