.class public final Lcom/squareup/server/payment/ItemizationsMessage;
.super Ljava/lang/Object;
.source "ItemizationsMessage.java"


# instance fields
.field public final adjustments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation
.end field

.field public final bill_id:Ljava/lang/String;

.field public final description:Ljava/lang/String;

.field public final itemizations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation
.end field

.field public final items_model:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;"
        }
    .end annotation
.end field

.field public final payment_id:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;)V"
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/server/payment/ItemizationsMessage;->payment_id:Ljava/lang/String;

    .line 55
    iput-object p2, p0, Lcom/squareup/server/payment/ItemizationsMessage;->bill_id:Ljava/lang/String;

    .line 56
    iput-object p3, p0, Lcom/squareup/server/payment/ItemizationsMessage;->adjustments:Ljava/util/List;

    .line 57
    iput-object p4, p0, Lcom/squareup/server/payment/ItemizationsMessage;->itemizations:Ljava/util/List;

    .line 58
    iput-object p5, p0, Lcom/squareup/server/payment/ItemizationsMessage;->description:Ljava/lang/String;

    .line 59
    iput-object p6, p0, Lcom/squareup/server/payment/ItemizationsMessage;->items_model:Ljava/util/List;

    return-void
.end method

.method public static forBill(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/server/payment/ItemizationsMessage;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/server/payment/ItemizationsMessage;"
        }
    .end annotation

    .line 49
    new-instance v7, Lcom/squareup/server/payment/ItemizationsMessage;

    const/4 v1, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/server/payment/ItemizationsMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V

    return-object v7
.end method

.method public static forCapture(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/server/payment/ItemizationsMessage;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;)",
            "Lcom/squareup/server/payment/ItemizationsMessage;"
        }
    .end annotation

    .line 33
    new-instance v7, Lcom/squareup/server/payment/ItemizationsMessage;

    const/4 v2, 0x0

    move-object v0, v7

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/squareup/server/payment/ItemizationsMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V

    return-object v7
.end method
