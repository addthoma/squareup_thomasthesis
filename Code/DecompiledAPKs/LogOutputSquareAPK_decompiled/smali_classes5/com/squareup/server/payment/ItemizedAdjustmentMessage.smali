.class public Lcom/squareup/server/payment/ItemizedAdjustmentMessage;
.super Ljava/lang/Object;
.source "ItemizedAdjustmentMessage.java"


# instance fields
.field private final applied_amount_cents:J

.field public final applied_money:Lcom/squareup/protos/common/Money;

.field private final currency_code:Lcom/squareup/protos/common/CurrencyCode;

.field public final id:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->id:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->applied_money:Lcom/squareup/protos/common/Money;

    .line 35
    iget-object p1, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->applied_amount_cents:J

    .line 36
    iget-object p1, p2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p1, p0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 42
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 44
    :cond_1
    check-cast p1, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;

    .line 46
    iget-object v2, p0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->applied_amount_cents:J

    .line 47
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v3, p1, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->applied_amount_cents:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 46
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    iget-object p1, p1, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 47
    invoke-static {v2, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 53
    iget-object v1, p0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->id:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->applied_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-wide v1, p0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->applied_amount_cents:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ItemizedAdjustment{applied_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
