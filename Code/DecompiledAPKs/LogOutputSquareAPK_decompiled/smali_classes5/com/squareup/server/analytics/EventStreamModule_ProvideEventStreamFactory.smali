.class public final Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;
.super Ljava/lang/Object;
.source "EventStreamModule_ProvideEventStreamFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/eventstream/v1/EventStream;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final esLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/EsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final installationIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final isReaderSdkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final posBuildProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;"
        }
    .end annotation
.end field

.field private final posSdkVersionNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final processUniqueIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/ProcessUniqueId;",
            ">;"
        }
    .end annotation
.end field

.field private final productVersionCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final productVersionNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final uploaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/Es1BatchUploader;",
            ">;"
        }
    .end annotation
.end field

.field private final userAgentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final windowManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/view/WindowManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/Es1BatchUploader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/EsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/view/WindowManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/ProcessUniqueId;",
            ">;)V"
        }
    .end annotation

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->contextProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p2, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->uploaderProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p3, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->esLoggerProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p4, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->userAgentProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p5, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->posSdkVersionNameProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p6, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->gsonProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p7, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->installationIdProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p8, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->windowManagerProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p9, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->deviceProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p10, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->resProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p11, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->isReaderSdkProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p12, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->productVersionNameProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p13, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->productVersionCodeProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p14, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->posBuildProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p15, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->processUniqueIdProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/Es1BatchUploader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/EsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/view/WindowManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/ProcessUniqueId;",
            ">;)",
            "Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;"
        }
    .end annotation

    .line 94
    new-instance v16, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v16
.end method

.method public static provideEventStream(Landroid/app/Application;Lcom/squareup/server/analytics/Es1BatchUploader;Lcom/squareup/server/analytics/EsLogger;Ljava/lang/String;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/String;Landroid/view/WindowManager;Lcom/squareup/util/Device;Landroid/content/res/Resources;ZLjava/lang/String;ILcom/squareup/util/PosBuild;Lcom/squareup/analytics/ProcessUniqueId;)Lcom/squareup/eventstream/v1/EventStream;
    .locals 0

    .line 102
    invoke-static/range {p0 .. p14}, Lcom/squareup/server/analytics/EventStreamModule;->provideEventStream(Landroid/app/Application;Lcom/squareup/server/analytics/Es1BatchUploader;Lcom/squareup/server/analytics/EsLogger;Ljava/lang/String;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/String;Landroid/view/WindowManager;Lcom/squareup/util/Device;Landroid/content/res/Resources;ZLjava/lang/String;ILcom/squareup/util/PosBuild;Lcom/squareup/analytics/ProcessUniqueId;)Lcom/squareup/eventstream/v1/EventStream;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/eventstream/v1/EventStream;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/eventstream/v1/EventStream;
    .locals 17

    move-object/from16 v0, p0

    .line 82
    iget-object v1, v0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/app/Application;

    iget-object v1, v0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->uploaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/server/analytics/Es1BatchUploader;

    iget-object v1, v0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->esLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/server/analytics/EsLogger;

    iget-object v1, v0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->userAgentProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Ljava/lang/String;

    iget-object v1, v0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->posSdkVersionNameProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Ljava/lang/String;

    iget-object v1, v0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/google/gson/Gson;

    iget-object v1, v0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->installationIdProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Ljava/lang/String;

    iget-object v1, v0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->windowManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Landroid/view/WindowManager;

    iget-object v1, v0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Landroid/content/res/Resources;

    iget-object v1, v0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->isReaderSdkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    iget-object v1, v0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->productVersionNameProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Ljava/lang/String;

    iget-object v1, v0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->productVersionCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v14

    iget-object v1, v0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->posBuildProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/util/PosBuild;

    iget-object v1, v0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->processUniqueIdProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/analytics/ProcessUniqueId;

    invoke-static/range {v2 .. v16}, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->provideEventStream(Landroid/app/Application;Lcom/squareup/server/analytics/Es1BatchUploader;Lcom/squareup/server/analytics/EsLogger;Ljava/lang/String;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/String;Landroid/view/WindowManager;Lcom/squareup/util/Device;Landroid/content/res/Resources;ZLjava/lang/String;ILcom/squareup/util/PosBuild;Lcom/squareup/analytics/ProcessUniqueId;)Lcom/squareup/eventstream/v1/EventStream;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->get()Lcom/squareup/eventstream/v1/EventStream;

    move-result-object v0

    return-object v0
.end method
