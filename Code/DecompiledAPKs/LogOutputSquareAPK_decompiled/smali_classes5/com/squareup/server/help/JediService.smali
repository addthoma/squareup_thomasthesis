.class public interface abstract Lcom/squareup/server/help/JediService;
.super Ljava/lang/Object;
.source "JediService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/help/JediService$JediServiceStandardResponse;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u000fJ\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00032\u0008\u0008\u0001\u0010\t\u001a\u00020\nH\'J\u0018\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u00032\u0008\u0008\u0001\u0010\r\u001a\u00020\u000eH\'\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/server/help/JediService;",
        "",
        "getPanel",
        "Lcom/squareup/server/help/JediService$JediServiceStandardResponse;",
        "Lcom/squareup/protos/jedi/service/PanelResponse;",
        "getPanelRequest",
        "Lcom/squareup/protos/jedi/service/PanelRequest;",
        "searchText",
        "Lcom/squareup/protos/jedi/service/SearchResponse;",
        "searchRequest",
        "Lcom/squareup/protos/jedi/service/SearchRequest;",
        "startSession",
        "Lcom/squareup/protos/jedi/service/StartSessionResponse;",
        "startRequest",
        "Lcom/squareup/protos/jedi/service/StartSessionRequest;",
        "JediServiceStandardResponse",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getPanel(Lcom/squareup/protos/jedi/service/PanelRequest;)Lcom/squareup/server/help/JediService$JediServiceStandardResponse;
    .param p1    # Lcom/squareup/protos/jedi/service/PanelRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/jedi/service/PanelRequest;",
            ")",
            "Lcom/squareup/server/help/JediService$JediServiceStandardResponse<",
            "Lcom/squareup/protos/jedi/service/PanelResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/json; charset=utf-8"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/jedi/session/get_panel"
    .end annotation
.end method

.method public abstract searchText(Lcom/squareup/protos/jedi/service/SearchRequest;)Lcom/squareup/server/help/JediService$JediServiceStandardResponse;
    .param p1    # Lcom/squareup/protos/jedi/service/SearchRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/jedi/service/SearchRequest;",
            ")",
            "Lcom/squareup/server/help/JediService$JediServiceStandardResponse<",
            "Lcom/squareup/protos/jedi/service/SearchResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/json; charset=utf-8"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/jedi/session/search"
    .end annotation
.end method

.method public abstract startSession(Lcom/squareup/protos/jedi/service/StartSessionRequest;)Lcom/squareup/server/help/JediService$JediServiceStandardResponse;
    .param p1    # Lcom/squareup/protos/jedi/service/StartSessionRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/jedi/service/StartSessionRequest;",
            ")",
            "Lcom/squareup/server/help/JediService$JediServiceStandardResponse<",
            "Lcom/squareup/protos/jedi/service/StartSessionResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/json; charset=utf-8"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/jedi/session/start"
    .end annotation
.end method
