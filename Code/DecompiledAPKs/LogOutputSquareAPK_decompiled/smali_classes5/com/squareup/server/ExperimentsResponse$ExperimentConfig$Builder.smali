.class public Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;
.super Ljava/lang/Object;
.source "ExperimentsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private buckets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/ExperimentsResponse$Bucket;",
            ">;"
        }
    .end annotation
.end field

.field private description:Ljava/lang/String;

.field private id:I

.field private name:Ljava/lang/String;

.field private started_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

.field private status:Ljava/lang/String;

.field private updated_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

.field private version:Ljava/lang/String;

.field private winner:Lcom/squareup/server/ExperimentsResponse$Bucket;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->buckets:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addBucket(Lcom/squareup/server/ExperimentsResponse$Bucket;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->buckets:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;
    .locals 12

    .line 191
    new-instance v11, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    iget-object v1, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->buckets:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->description:Ljava/lang/String;

    iget v3, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->id:I

    iget-object v4, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->started_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    iget-object v6, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->status:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->updated_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    iget-object v8, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->version:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->winner:Lcom/squareup/server/ExperimentsResponse$Bucket;

    const/4 v10, 0x0

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;-><init>(Ljava/util/List;Ljava/lang/String;ILjava/lang/String;Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;Ljava/lang/String;Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;Ljava/lang/String;Lcom/squareup/server/ExperimentsResponse$Bucket;Lcom/squareup/server/ExperimentsResponse$1;)V

    return-object v11
.end method

.method public setDescription(Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public setId(I)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;
    .locals 0

    .line 156
    iput p1, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->id:I

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public setStartedAt(Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->started_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    return-object p0
.end method

.method public setStatus(Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->status:Ljava/lang/String;

    return-object p0
.end method

.method public setUpdatedAt(Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->updated_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    return-object p0
.end method

.method public setVersion(Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->version:Ljava/lang/String;

    return-object p0
.end method

.method public setWinner(Lcom/squareup/server/ExperimentsResponse$Bucket;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->winner:Lcom/squareup/server/ExperimentsResponse$Bucket;

    return-object p0
.end method
