.class public Lcom/squareup/server/ContentReader;
.super Ljava/lang/Object;
.source "ContentReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/ContentReader$ContentReadException;
    }
.end annotation


# instance fields
.field private final stream:Ljava/io/InputStream;


# direct methods
.method constructor <init>(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 2

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/ContentReader;->stream:Ljava/io/InputStream;

    .line 56
    iget-object p1, p0, Lcom/squareup/server/ContentReader;->stream:Ljava/io/InputStream;

    if-eqz p1, :cond_0

    return-void

    .line 57
    :cond_0
    new-instance p1, Lcom/squareup/server/ContentReader$ContentReadException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Could not load stream for uri "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/server/ContentReader$ContentReadException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    .line 60
    new-instance p2, Lcom/squareup/server/ContentReader$ContentReadException;

    invoke-direct {p2, p1}, Lcom/squareup/server/ContentReader$ContentReadException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method constructor <init>(Ljava/io/File;)V
    .locals 1

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/squareup/server/ContentReader;->stream:Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 68
    new-instance v0, Lcom/squareup/server/ContentReader$ContentReadException;

    invoke-direct {v0, p1}, Lcom/squareup/server/ContentReader$ContentReadException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static isContentReadFailure(Ljava/lang/Throwable;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 35
    :cond_0
    invoke-static {p0}, Lcom/squareup/server/ContentReader;->isContentReadFailureNonIterative(Ljava/lang/Throwable;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    return v2

    .line 38
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eq v1, p0, :cond_2

    .line 39
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    .line 40
    invoke-static {p0}, Lcom/squareup/server/ContentReader;->isContentReadFailureNonIterative(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_1

    return v2

    :cond_2
    return v0
.end method

.method private static isContentReadFailureNonIterative(Ljava/lang/Throwable;)Z
    .locals 1

    .line 47
    instance-of v0, p0, Lcom/squareup/server/ContentReader$ContentReadException;

    if-nez v0, :cond_1

    instance-of p0, p0, Ljava/lang/SecurityException;

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method


# virtual methods
.method close()V
    .locals 2

    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/squareup/server/ContentReader;->stream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 84
    new-instance v1, Lcom/squareup/server/ContentReader$ContentReadException;

    invoke-direct {v1, v0}, Lcom/squareup/server/ContentReader$ContentReadException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method read([B)I
    .locals 1

    .line 74
    :try_start_0
    iget-object v0, p0, Lcom/squareup/server/ContentReader;->stream:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 76
    new-instance v0, Lcom/squareup/server/ContentReader$ContentReadException;

    invoke-direct {v0, p1}, Lcom/squareup/server/ContentReader$ContentReadException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method
