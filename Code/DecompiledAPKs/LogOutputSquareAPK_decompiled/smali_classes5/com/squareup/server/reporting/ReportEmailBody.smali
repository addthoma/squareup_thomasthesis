.class public Lcom/squareup/server/reporting/ReportEmailBody;
.super Ljava/lang/Object;
.source "ReportEmailBody.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/reporting/ReportEmailBody$DisplayOptions;
    }
.end annotation


# instance fields
.field public final begin_time:Ljava/lang/String;

.field public final display_options:Lcom/squareup/server/reporting/ReportEmailBody$DisplayOptions;

.field public final end_time:Ljava/lang/String;

.field public final to_address:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/squareup/server/reporting/ReportEmailBody;->begin_time:Ljava/lang/String;

    .line 12
    iput-object p2, p0, Lcom/squareup/server/reporting/ReportEmailBody;->end_time:Ljava/lang/String;

    .line 13
    iput-object p3, p0, Lcom/squareup/server/reporting/ReportEmailBody;->to_address:Ljava/lang/String;

    .line 14
    new-instance p1, Lcom/squareup/server/reporting/ReportEmailBody$DisplayOptions;

    invoke-direct {p1, p4}, Lcom/squareup/server/reporting/ReportEmailBody$DisplayOptions;-><init>(Z)V

    iput-object p1, p0, Lcom/squareup/server/reporting/ReportEmailBody;->display_options:Lcom/squareup/server/reporting/ReportEmailBody$DisplayOptions;

    return-void
.end method
