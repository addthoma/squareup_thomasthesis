.class public final Lcom/squareup/server/shipping/UpdateAddressResponse$Error;
.super Ljava/lang/Object;
.source "UpdateAddressResponse.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/shipping/UpdateAddressResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Error"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final field_name:Ljava/lang/String;

.field private final message:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 66
    invoke-direct {p0, v0, v0}, Lcom/squareup/server/shipping/UpdateAddressResponse$Error;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/squareup/server/shipping/UpdateAddressResponse$Error;->field_name:Ljava/lang/String;

    .line 71
    iput-object p2, p0, Lcom/squareup/server/shipping/UpdateAddressResponse$Error;->message:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getFieldName()Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/server/shipping/UpdateAddressResponse$Error;->field_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->fromJson(Ljava/lang/String;)Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    move-result-object v0

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/server/shipping/UpdateAddressResponse$Error;->message:Ljava/lang/String;

    return-object v0
.end method
