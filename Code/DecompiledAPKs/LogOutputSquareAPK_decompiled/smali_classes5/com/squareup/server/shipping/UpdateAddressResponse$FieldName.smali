.class public final enum Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;
.super Ljava/lang/Enum;
.source "UpdateAddressResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/shipping/UpdateAddressResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FieldName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

.field public static final enum CITY:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

.field public static final enum NAME:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

.field public static final enum POSTAL_CODE:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

.field public static final enum STATE:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

.field public static final enum STREET_1:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

.field public static final enum STREET_2:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;


# instance fields
.field private final jsonString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 36
    new-instance v0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    const/4 v1, 0x0

    const-string v2, "NAME"

    const-string v3, "name"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->NAME:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    .line 37
    new-instance v0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    const/4 v2, 0x1

    const-string v3, "STREET_1"

    const-string v4, "street1"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->STREET_1:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    .line 38
    new-instance v0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    const/4 v3, 0x2

    const-string v4, "STREET_2"

    const-string v5, "street2"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->STREET_2:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    .line 39
    new-instance v0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    const/4 v4, 0x3

    const-string v5, "CITY"

    const-string v6, "city"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->CITY:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    .line 40
    new-instance v0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    const/4 v5, 0x4

    const-string v6, "STATE"

    const-string v7, "state"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->STATE:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    .line 41
    new-instance v0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    const/4 v6, 0x5

    const-string v7, "POSTAL_CODE"

    const-string v8, "postal_code"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->POSTAL_CODE:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    .line 35
    sget-object v7, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->NAME:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->STREET_1:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->STREET_2:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->CITY:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->STATE:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->POSTAL_CODE:Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->$VALUES:[Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput-object p3, p0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->jsonString:Ljava/lang/String;

    return-void
.end method

.method public static fromJson(Ljava/lang/String;)Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;
    .locals 5

    .line 49
    invoke-static {}, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->values()[Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 50
    iget-object v4, v3, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->jsonString:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;
    .locals 1

    .line 35
    const-class v0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->$VALUES:[Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    invoke-virtual {v0}, [Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;

    return-object v0
.end method
