.class public abstract Lcom/squareup/sqldelight/Transacter$Transaction;
.super Ljava/lang/Object;
.source "Transacter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqldelight/Transacter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Transaction"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u000b\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010%\n\u0002\u0010\u0008\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u0001\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008&\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010+\u001a\u00020\u00122\u000c\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011J\u0014\u0010-\u001a\u00020\u00122\u000c\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011J\u000f\u0010\u000c\u001a\u0004\u0018\u00010\u0000H\u0000\u00a2\u0006\u0002\u0008.J\r\u0010/\u001a\u00020\u0012H\u0000\u00a2\u0006\u0002\u00080J\u0010\u0010/\u001a\u00020\u00122\u0006\u0010\u001f\u001a\u00020\u0004H$J\u0006\u00101\u001a\u000202J\u001f\u00103\u001a\u00020\u00122\u0017\u00104\u001a\u0013\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u001205\u00a2\u0006\u0002\u00086R+\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00048@@@X\u0080\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\n\u0010\u000b\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR\u0014\u0010\u000c\u001a\u0004\u0018\u00010\u0000X\u00a4\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR6\u0010\u000f\u001a$\u0012 \u0012\u001e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00120\u00110\u0011j\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00120\u0011`\u00130\u0010X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R6\u0010\u0016\u001a$\u0012 \u0012\u001e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00120\u00110\u0011j\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00120\u0011`\u00130\u0010X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0015RP\u0010\u0018\u001a>\u0012\u0004\u0012\u00020\u001a\u00124\u00122\u0012\u0014\u0012\u0012\u0012\u000e\u0012\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u001c0\u001b0\u00110\u0011j\u0018\u0012\u0014\u0012\u0012\u0012\u000e\u0012\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u001c0\u001b0\u0011`\u00130\u0019X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR+\u0010\u001f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00048@@@X\u0080\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\"\u0010\u000b\u001a\u0004\u0008 \u0010\u0007\"\u0004\u0008!\u0010\tR/\u0010$\u001a\u0004\u0018\u00010#2\u0008\u0010\u0003\u001a\u0004\u0018\u00010#8@@@X\u0080\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008)\u0010*\u001a\u0004\u0008%\u0010&\"\u0004\u0008\'\u0010(\u00a8\u00067"
    }
    d2 = {
        "Lcom/squareup/sqldelight/Transacter$Transaction;",
        "",
        "()V",
        "<set-?>",
        "",
        "childrenSuccessful",
        "getChildrenSuccessful$runtime",
        "()Z",
        "setChildrenSuccessful$runtime",
        "(Z)V",
        "childrenSuccessful$delegate",
        "Ljava/util/concurrent/atomic/AtomicBoolean;",
        "enclosingTransaction",
        "getEnclosingTransaction",
        "()Lcom/squareup/sqldelight/Transacter$Transaction;",
        "postCommitHooks",
        "",
        "Lkotlin/Function0;",
        "",
        "Lcom/squareup/sqldelight/internal/Supplier;",
        "getPostCommitHooks$runtime",
        "()Ljava/util/Set;",
        "postRollbackHooks",
        "getPostRollbackHooks$runtime",
        "queriesFuncs",
        "",
        "",
        "",
        "Lcom/squareup/sqldelight/Query;",
        "getQueriesFuncs$runtime",
        "()Ljava/util/Map;",
        "successful",
        "getSuccessful$runtime",
        "setSuccessful$runtime",
        "successful$delegate",
        "Lcom/squareup/sqldelight/Transacter;",
        "transacter",
        "getTransacter$runtime",
        "()Lcom/squareup/sqldelight/Transacter;",
        "setTransacter$runtime",
        "(Lcom/squareup/sqldelight/Transacter;)V",
        "transacter$delegate",
        "Ljava/util/concurrent/atomic/AtomicReference;",
        "afterCommit",
        "function",
        "afterRollback",
        "enclosingTransaction$runtime",
        "endTransaction",
        "endTransaction$runtime",
        "rollback",
        "",
        "transaction",
        "body",
        "Lkotlin/Function1;",
        "Lkotlin/ExtensionFunctionType;",
        "runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final childrenSuccessful$delegate:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final postCommitHooks:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final postRollbackHooks:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final queriesFuncs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/util/List<",
            "Lcom/squareup/sqldelight/Query<",
            "*>;>;>;>;>;"
        }
    .end annotation
.end field

.field private final successful$delegate:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final transacter$delegate:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/squareup/sqldelight/Transacter$Transaction;

    const/4 v1, 0x3

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "successful"

    const-string v5, "getSuccessful$runtime()Z"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "childrenSuccessful"

    const-string v5, "getChildrenSuccessful$runtime()Z"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string/jumbo v3, "transacter"

    const-string v4, "getTransacter$runtime()Lcom/squareup/sqldelight/Transacter;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aput-object v0, v1, v2

    sput-object v1, Lcom/squareup/sqldelight/Transacter$Transaction;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {}, Lcom/squareup/sqldelight/internal/FunctionsJvmKt;->sharedSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->postCommitHooks:Ljava/util/Set;

    .line 47
    invoke-static {}, Lcom/squareup/sqldelight/internal/FunctionsJvmKt;->sharedSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->postRollbackHooks:Ljava/util/Set;

    .line 48
    invoke-static {}, Lcom/squareup/sqldelight/internal/FunctionsJvmKt;->sharedMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->queriesFuncs:Ljava/util/Map;

    .line 50
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->successful$delegate:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 51
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->childrenSuccessful$delegate:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 52
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->transacter$delegate:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method


# virtual methods
.method public final afterCommit(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "function"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->postCommitHooks:Ljava/util/Set;

    invoke-static {p1}, Lcom/squareup/sqldelight/internal/FunctionsJvmKt;->threadLocalRef(Ljava/lang/Object;)Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final afterRollback(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "function"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->postRollbackHooks:Ljava/util/Set;

    invoke-static {p1}, Lcom/squareup/sqldelight/internal/FunctionsJvmKt;->threadLocalRef(Ljava/lang/Object;)Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final enclosingTransaction$runtime()Lcom/squareup/sqldelight/Transacter$Transaction;
    .locals 1

    .line 59
    invoke-virtual {p0}, Lcom/squareup/sqldelight/Transacter$Transaction;->getEnclosingTransaction()Lcom/squareup/sqldelight/Transacter$Transaction;

    move-result-object v0

    return-object v0
.end method

.method protected abstract endTransaction(Z)V
.end method

.method public final endTransaction$runtime()V
    .locals 1

    .line 68
    invoke-virtual {p0}, Lcom/squareup/sqldelight/Transacter$Transaction;->getSuccessful$runtime()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/sqldelight/Transacter$Transaction;->getChildrenSuccessful$runtime()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/squareup/sqldelight/Transacter$Transaction;->endTransaction(Z)V

    return-void
.end method

.method public final getChildrenSuccessful$runtime()Z
    .locals 3

    iget-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->childrenSuccessful$delegate:Ljava/util/concurrent/atomic/AtomicBoolean;

    sget-object v1, Lcom/squareup/sqldelight/Transacter$Transaction;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/sqldelight/internal/AtomicsKt;->getValue(Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Z

    move-result v0

    return v0
.end method

.method protected abstract getEnclosingTransaction()Lcom/squareup/sqldelight/Transacter$Transaction;
.end method

.method public final getPostCommitHooks$runtime()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;>;>;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->postCommitHooks:Ljava/util/Set;

    return-object v0
.end method

.method public final getPostRollbackHooks$runtime()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;>;>;"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->postRollbackHooks:Ljava/util/Set;

    return-object v0
.end method

.method public final getQueriesFuncs$runtime()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/util/List<",
            "Lcom/squareup/sqldelight/Query<",
            "*>;>;>;>;>;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->queriesFuncs:Ljava/util/Map;

    return-object v0
.end method

.method public final getSuccessful$runtime()Z
    .locals 3

    iget-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->successful$delegate:Ljava/util/concurrent/atomic/AtomicBoolean;

    sget-object v1, Lcom/squareup/sqldelight/Transacter$Transaction;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/sqldelight/internal/AtomicsKt;->getValue(Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Z

    move-result v0

    return v0
.end method

.method public final getTransacter$runtime()Lcom/squareup/sqldelight/Transacter;
    .locals 3

    iget-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->transacter$delegate:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Lcom/squareup/sqldelight/Transacter$Transaction;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/sqldelight/internal/AtomicsKt;->getValue(Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sqldelight/Transacter;

    return-object v0
.end method

.method public final rollback()Ljava/lang/Void;
    .locals 1

    .line 73
    new-instance v0, Lcom/squareup/sqldelight/RollbackException;

    invoke-direct {v0}, Lcom/squareup/sqldelight/RollbackException;-><init>()V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final setChildrenSuccessful$runtime(Z)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->childrenSuccessful$delegate:Ljava/util/concurrent/atomic/AtomicBoolean;

    sget-object v1, Lcom/squareup/sqldelight/Transacter$Transaction;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1, p1}, Lcom/squareup/sqldelight/internal/AtomicsKt;->setValue(Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/lang/Object;Lkotlin/reflect/KProperty;Z)V

    return-void
.end method

.method public final setSuccessful$runtime(Z)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->successful$delegate:Ljava/util/concurrent/atomic/AtomicBoolean;

    sget-object v1, Lcom/squareup/sqldelight/Transacter$Transaction;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1, p1}, Lcom/squareup/sqldelight/internal/AtomicsKt;->setValue(Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/lang/Object;Lkotlin/reflect/KProperty;Z)V

    return-void
.end method

.method public final setTransacter$runtime(Lcom/squareup/sqldelight/Transacter;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/sqldelight/Transacter$Transaction;->transacter$delegate:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Lcom/squareup/sqldelight/Transacter$Transaction;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1, p1}, Lcom/squareup/sqldelight/internal/AtomicsKt;->setValue(Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final transaction(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/sqldelight/Transacter$Transaction;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0}, Lcom/squareup/sqldelight/Transacter$Transaction;->getTransacter$runtime()Lcom/squareup/sqldelight/Transacter;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Lcom/squareup/sqldelight/Transacter;->transaction(ZLkotlin/jvm/functions/Function1;)V

    return-void
.end method
