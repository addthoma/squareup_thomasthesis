.class final Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Transaction;
.super Lcom/squareup/sqldelight/Transacter$Transaction;
.source "JdbcSqliteDriver.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Transaction"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0001\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0014R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0001X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Transaction;",
        "Lcom/squareup/sqldelight/Transacter$Transaction;",
        "enclosingTransaction",
        "(Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;Lcom/squareup/sqldelight/Transacter$Transaction;)V",
        "getEnclosingTransaction",
        "()Lcom/squareup/sqldelight/Transacter$Transaction;",
        "endTransaction",
        "",
        "successful",
        "",
        "sqldelight-sqlite-driver"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final enclosingTransaction:Lcom/squareup/sqldelight/Transacter$Transaction;

.field final synthetic this$0:Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;


# direct methods
.method public constructor <init>(Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;Lcom/squareup/sqldelight/Transacter$Transaction;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/sqldelight/Transacter$Transaction;",
            ")V"
        }
    .end annotation

    .line 72
    iput-object p1, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Transaction;->this$0:Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;

    .line 74
    invoke-direct {p0}, Lcom/squareup/sqldelight/Transacter$Transaction;-><init>()V

    iput-object p2, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Transaction;->enclosingTransaction:Lcom/squareup/sqldelight/Transacter$Transaction;

    return-void
.end method


# virtual methods
.method protected endTransaction(Z)V
    .locals 1

    .line 76
    invoke-virtual {p0}, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Transaction;->getEnclosingTransaction()Lcom/squareup/sqldelight/Transacter$Transaction;

    move-result-object v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    .line 78
    iget-object p1, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Transaction;->this$0:Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;

    invoke-static {p1}, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;->access$getConnection$p(Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;)Ljava/sql/Connection;

    move-result-object p1

    const-string v0, "END TRANSACTION"

    invoke-interface {p1, v0}, Ljava/sql/Connection;->prepareStatement(Ljava/lang/String;)Ljava/sql/PreparedStatement;

    move-result-object p1

    invoke-interface {p1}, Ljava/sql/PreparedStatement;->execute()Z

    goto :goto_0

    .line 80
    :cond_0
    iget-object p1, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Transaction;->this$0:Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;

    invoke-static {p1}, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;->access$getConnection$p(Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;)Ljava/sql/Connection;

    move-result-object p1

    const-string v0, "ROLLBACK TRANSACTION"

    invoke-interface {p1, v0}, Ljava/sql/Connection;->prepareStatement(Ljava/lang/String;)Ljava/sql/PreparedStatement;

    move-result-object p1

    invoke-interface {p1}, Ljava/sql/PreparedStatement;->execute()Z

    .line 83
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Transaction;->this$0:Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;

    invoke-static {p1}, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;->access$getTransactions$p(Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;)Ljava/lang/ThreadLocal;

    move-result-object p1

    invoke-virtual {p0}, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Transaction;->getEnclosingTransaction()Lcom/squareup/sqldelight/Transacter$Transaction;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected getEnclosingTransaction()Lcom/squareup/sqldelight/Transacter$Transaction;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Transaction;->enclosingTransaction:Lcom/squareup/sqldelight/Transacter$Transaction;

    return-object v0
.end method
