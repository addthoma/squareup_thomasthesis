.class final Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;
.super Ljava/lang/Object;
.source "JdbcSqliteDriver.kt"

# interfaces
.implements Lcom/squareup/sqldelight/db/SqlPreparedStatement;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0012\n\u0002\u0008\u0002\n\u0002\u0010\u0006\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\nH\u0016J\u001f\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0016\u00a2\u0006\u0002\u0010\u000eJ\u001f\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016\u00a2\u0006\u0002\u0010\u0012J\u001a\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J\r\u0010\u0016\u001a\u00020\u0006H\u0000\u00a2\u0006\u0002\u0008\u0017J\r\u0010\u0018\u001a\u00020\u0019H\u0000\u00a2\u0006\u0002\u0008\u001aR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;",
        "Lcom/squareup/sqldelight/db/SqlPreparedStatement;",
        "preparedStatement",
        "Ljava/sql/PreparedStatement;",
        "(Ljava/sql/PreparedStatement;)V",
        "bindBytes",
        "",
        "index",
        "",
        "bytes",
        "",
        "bindDouble",
        "double",
        "",
        "(ILjava/lang/Double;)V",
        "bindLong",
        "long",
        "",
        "(ILjava/lang/Long;)V",
        "bindString",
        "string",
        "",
        "execute",
        "execute$sqldelight_sqlite_driver",
        "executeQuery",
        "Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;",
        "executeQuery$sqldelight_sqlite_driver",
        "sqldelight-sqlite-driver"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final preparedStatement:Ljava/sql/PreparedStatement;


# direct methods
.method public constructor <init>(Ljava/sql/PreparedStatement;)V
    .locals 1

    const-string v0, "preparedStatement"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;->preparedStatement:Ljava/sql/PreparedStatement;

    return-void
.end method


# virtual methods
.method public bindBytes(I[B)V
    .locals 1

    if-nez p2, :cond_0

    .line 93
    iget-object p2, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;->preparedStatement:Ljava/sql/PreparedStatement;

    const/16 v0, 0x7d4

    invoke-interface {p2, p1, v0}, Ljava/sql/PreparedStatement;->setNull(II)V

    goto :goto_0

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;->preparedStatement:Ljava/sql/PreparedStatement;

    invoke-interface {v0, p1, p2}, Ljava/sql/PreparedStatement;->setBytes(I[B)V

    :goto_0
    return-void
.end method

.method public bindDouble(ILjava/lang/Double;)V
    .locals 3

    if-nez p2, :cond_0

    .line 109
    iget-object p2, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;->preparedStatement:Ljava/sql/PreparedStatement;

    const/4 v0, 0x7

    invoke-interface {p2, p1, v0}, Ljava/sql/PreparedStatement;->setNull(II)V

    goto :goto_0

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;->preparedStatement:Ljava/sql/PreparedStatement;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-interface {v0, p1, v1, v2}, Ljava/sql/PreparedStatement;->setDouble(ID)V

    :goto_0
    return-void
.end method

.method public bindLong(ILjava/lang/Long;)V
    .locals 3

    if-nez p2, :cond_0

    .line 101
    iget-object p2, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;->preparedStatement:Ljava/sql/PreparedStatement;

    const/4 v0, 0x4

    invoke-interface {p2, p1, v0}, Ljava/sql/PreparedStatement;->setNull(II)V

    goto :goto_0

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;->preparedStatement:Ljava/sql/PreparedStatement;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, p1, v1, v2}, Ljava/sql/PreparedStatement;->setLong(IJ)V

    :goto_0
    return-void
.end method

.method public bindString(ILjava/lang/String;)V
    .locals 1

    if-nez p2, :cond_0

    .line 117
    iget-object p2, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;->preparedStatement:Ljava/sql/PreparedStatement;

    const/16 v0, 0xc

    invoke-interface {p2, p1, v0}, Ljava/sql/PreparedStatement;->setNull(II)V

    goto :goto_0

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;->preparedStatement:Ljava/sql/PreparedStatement;

    invoke-interface {v0, p1, p2}, Ljava/sql/PreparedStatement;->setString(ILjava/lang/String;)V

    :goto_0
    return-void
.end method

.method public final execute$sqldelight_sqlite_driver()V
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;->preparedStatement:Ljava/sql/PreparedStatement;

    invoke-interface {v0}, Ljava/sql/PreparedStatement;->execute()Z

    return-void
.end method

.method public final executeQuery$sqldelight_sqlite_driver()Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;
    .locals 4

    .line 124
    new-instance v0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;

    iget-object v1, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;->preparedStatement:Ljava/sql/PreparedStatement;

    invoke-interface {v1}, Ljava/sql/PreparedStatement;->executeQuery()Ljava/sql/ResultSet;

    move-result-object v2

    const-string v3, "preparedStatement.executeQuery()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;-><init>(Ljava/sql/PreparedStatement;Ljava/sql/ResultSet;)V

    return-object v0
.end method
