.class public final Lcom/squareup/thread/CoroutineDispatchers;
.super Ljava/lang/Object;
.source "CoroutineDispatchers.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\u0006R\u0011\u0010\t\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0006R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\r\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u0006\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/thread/CoroutineDispatchers;",
        "",
        "()V",
        "computation",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "getComputation",
        "()Lkotlinx/coroutines/CoroutineDispatcher;",
        "io",
        "getIo",
        "main",
        "getMain",
        "mainHandlerDispatcher",
        "Lkotlinx/coroutines/android/HandlerDispatcher;",
        "mainImmediate",
        "getMainImmediate",
        "impl-coroutines_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/thread/CoroutineDispatchers;

.field private static final main:Lkotlinx/coroutines/CoroutineDispatcher;

.field private static final mainHandlerDispatcher:Lkotlinx/coroutines/android/HandlerDispatcher;

.field private static final mainImmediate:Lkotlinx/coroutines/CoroutineDispatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 9
    new-instance v0, Lcom/squareup/thread/CoroutineDispatchers;

    invoke-direct {v0}, Lcom/squareup/thread/CoroutineDispatchers;-><init>()V

    sput-object v0, Lcom/squareup/thread/CoroutineDispatchers;->INSTANCE:Lcom/squareup/thread/CoroutineDispatchers;

    .line 18
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    const-string v1, "main"

    invoke-static {v0, v1}, Lkotlinx/coroutines/android/HandlerDispatcherKt;->from(Landroid/os/Handler;Ljava/lang/String;)Lkotlinx/coroutines/android/HandlerDispatcher;

    move-result-object v0

    sput-object v0, Lcom/squareup/thread/CoroutineDispatchers;->mainHandlerDispatcher:Lkotlinx/coroutines/android/HandlerDispatcher;

    .line 22
    sget-object v0, Lcom/squareup/thread/CoroutineDispatchers;->mainHandlerDispatcher:Lkotlinx/coroutines/android/HandlerDispatcher;

    move-object v1, v0

    check-cast v1, Lkotlinx/coroutines/CoroutineDispatcher;

    sput-object v1, Lcom/squareup/thread/CoroutineDispatchers;->main:Lkotlinx/coroutines/CoroutineDispatcher;

    .line 23
    invoke-virtual {v0}, Lkotlinx/coroutines/android/HandlerDispatcher;->getImmediate()Lkotlinx/coroutines/android/HandlerDispatcher;

    move-result-object v0

    check-cast v0, Lkotlinx/coroutines/CoroutineDispatcher;

    sput-object v0, Lcom/squareup/thread/CoroutineDispatchers;->mainImmediate:Lkotlinx/coroutines/CoroutineDispatcher;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getComputation()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1

    .line 21
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getDefault()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public final getIo()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1

    .line 20
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getIO()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public final getMain()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1

    .line 22
    sget-object v0, Lcom/squareup/thread/CoroutineDispatchers;->main:Lkotlinx/coroutines/CoroutineDispatcher;

    return-object v0
.end method

.method public final getMainImmediate()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1

    .line 23
    sget-object v0, Lcom/squareup/thread/CoroutineDispatchers;->mainImmediate:Lkotlinx/coroutines/CoroutineDispatcher;

    return-object v0
.end method
