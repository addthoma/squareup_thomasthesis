.class public final Lcom/squareup/thread/Rx1SchedulerModule_ProvideComputationSchedulerFactory;
.super Ljava/lang/Object;
.source "Rx1SchedulerModule_ProvideComputationSchedulerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/thread/Rx1SchedulerModule_ProvideComputationSchedulerFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lrx/Scheduler;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/thread/Rx1SchedulerModule_ProvideComputationSchedulerFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideComputationSchedulerFactory$InstanceHolder;->access$000()Lcom/squareup/thread/Rx1SchedulerModule_ProvideComputationSchedulerFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideComputationScheduler()Lrx/Scheduler;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule;->provideComputationScheduler()Lrx/Scheduler;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideComputationSchedulerFactory;->get()Lrx/Scheduler;

    move-result-object v0

    return-object v0
.end method

.method public get()Lrx/Scheduler;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideComputationSchedulerFactory;->provideComputationScheduler()Lrx/Scheduler;

    move-result-object v0

    return-object v0
.end method
