.class public Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;
.super Ljava/lang/Object;
.source "ExecutorRegistry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/thread/executor/ExecutorRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StoppableExecutor"
.end annotation


# instance fields
.field private final executorService:Ljava/util/concurrent/ExecutorService;

.field private final stoppableSerialExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;


# direct methods
.method public constructor <init>(Lcom/squareup/thread/executor/StoppableSerialExecutor;)V
    .locals 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "stoppableSerialExecutor"

    .line 38
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/thread/executor/StoppableSerialExecutor;

    iput-object p1, p0, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;->stoppableSerialExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    const/4 p1, 0x0

    .line 39
    iput-object p1, p0, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;->executorService:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 1

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executorService"

    .line 43
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ExecutorService;

    iput-object p1, p0, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;->executorService:Ljava/util/concurrent/ExecutorService;

    const/4 p1, 0x0

    .line 44
    iput-object p1, p0, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;->stoppableSerialExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    return-void
.end method


# virtual methods
.method public awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;->stoppableSerialExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    if-eqz v0, :cond_0

    .line 56
    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/thread/executor/StoppableSerialExecutor;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result p1

    return p1

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;->executorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1, p2, p3}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result p1

    return p1
.end method

.method public shutdown()V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;->executorService:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;->stoppableSerialExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/squareup/thread/executor/StoppableSerialExecutor;->shutdown()V

    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;->executorService:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;->stoppableSerialExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    .line 63
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method
