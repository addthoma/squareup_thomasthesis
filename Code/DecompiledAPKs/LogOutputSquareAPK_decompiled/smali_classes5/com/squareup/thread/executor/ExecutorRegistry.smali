.class public final Lcom/squareup/thread/executor/ExecutorRegistry;
.super Ljava/lang/Object;
.source "ExecutorRegistry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;
    }
.end annotation


# static fields
.field public static final EXECUTOR_TERMINATION_TIMEOUT_MILLIS:I = 0x7d0

.field private static enabled:Z

.field private static final executors:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;",
            "[",
            "Ljava/lang/StackTraceElement;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 72
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/squareup/thread/executor/ExecutorRegistry;->executors:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized enable()V
    .locals 2

    const-class v0, Lcom/squareup/thread/executor/ExecutorRegistry;

    monitor-enter v0

    const/4 v1, 0x1

    .line 99
    :try_start_0
    sput-boolean v1, Lcom/squareup/thread/executor/ExecutorRegistry;->enabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized register(Lcom/squareup/thread/executor/StoppableSerialExecutor;)Lcom/squareup/thread/executor/StoppableSerialExecutor;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/squareup/thread/executor/StoppableSerialExecutor;",
            ">(TT;)TT;"
        }
    .end annotation

    const-class v0, Lcom/squareup/thread/executor/ExecutorRegistry;

    monitor-enter v0

    .line 83
    :try_start_0
    sget-boolean v1, Lcom/squareup/thread/executor/ExecutorRegistry;->enabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    monitor-exit v0

    return-object p0

    .line 84
    :cond_0
    :try_start_1
    sget-object v1, Lcom/squareup/thread/executor/ExecutorRegistry;->executors:Ljava/util/Map;

    new-instance v2, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;

    invoke-direct {v2, p0}, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;-><init>(Lcom/squareup/thread/executor/StoppableSerialExecutor;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 85
    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized register(Ljava/util/concurrent/ExecutorService;)Ljava/util/concurrent/ExecutorService;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/util/concurrent/ExecutorService;",
            ">(TT;)TT;"
        }
    .end annotation

    const-class v0, Lcom/squareup/thread/executor/ExecutorRegistry;

    monitor-enter v0

    .line 93
    :try_start_0
    sget-boolean v1, Lcom/squareup/thread/executor/ExecutorRegistry;->enabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    monitor-exit v0

    return-object p0

    .line 94
    :cond_0
    :try_start_1
    sget-object v1, Lcom/squareup/thread/executor/ExecutorRegistry;->executors:Ljava/util/Map;

    new-instance v2, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;

    invoke-direct {v2, p0}, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;-><init>(Ljava/util/concurrent/ExecutorService;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 95
    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized reset()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;",
            "[",
            "Ljava/lang/StackTraceElement;",
            ">;"
        }
    .end annotation

    const-class v0, Lcom/squareup/thread/executor/ExecutorRegistry;

    monitor-enter v0

    .line 107
    :try_start_0
    sget-boolean v1, Lcom/squareup/thread/executor/ExecutorRegistry;->enabled:Z

    if-eqz v1, :cond_0

    .line 108
    new-instance v1, Ljava/util/LinkedHashMap;

    sget-object v2, Lcom/squareup/thread/executor/ExecutorRegistry;->executors:Ljava/util/Map;

    invoke-direct {v1, v2}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 109
    sget-object v2, Lcom/squareup/thread/executor/ExecutorRegistry;->executors:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    monitor-exit v0

    return-object v1

    .line 107
    :cond_0
    :try_start_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Not enabled."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized shutdownExecutors()V
    .locals 14

    const-class v0, Lcom/squareup/thread/executor/ExecutorRegistry;

    monitor-enter v0

    .line 120
    :try_start_0
    invoke-static {}, Lcom/squareup/thread/executor/ExecutorRegistry;->reset()Ljava/util/Map;

    move-result-object v1

    const-string v2, "Shutting down Executors. Count: %d."

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Object;

    .line 121
    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-static {v2, v4}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 122
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;

    .line 123
    invoke-virtual {v4}, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;->shutdown()V

    goto :goto_0

    :cond_0
    const-string v2, "Waiting up to %d milliseconds for %d Executors to terminate."

    const/4 v4, 0x2

    new-array v5, v4, [Ljava/lang/Object;

    const/16 v7, 0x7d0

    .line 127
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v3

    .line 126
    invoke-static {v2, v5}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 128
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    const-wide/16 v9, 0x7d0

    add-long/2addr v7, v9

    .line 129
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 130
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;

    .line 131
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/StackTraceElement;

    const-wide/16 v9, 0x0

    .line 132
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v11

    sub-long v11, v7, v11

    invoke-static {v9, v10, v11, v12}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v11, "Waiting up to %d milliseconds for Executor: %s."

    new-array v12, v4, [Ljava/lang/Object;

    .line 134
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v12, v6

    aput-object v5, v12, v3

    invoke-static {v11, v12}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 135
    sget-object v11, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v9, v10, v11}, Lcom/squareup/thread/executor/ExecutorRegistry$StoppableExecutor;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v11

    if-eqz v11, :cond_1

    goto :goto_1

    :cond_1
    const-string v1, "%s did not terminate. Registration:\n%s"

    new-array v7, v4, [Ljava/lang/Object;

    aput-object v5, v7, v6

    .line 137
    invoke-static {v2}, Lcom/squareup/util/StackTraceStrings;->toString([Ljava/lang/StackTraceElement;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v3

    .line 136
    invoke-static {v1, v7}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 139
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v8, " was created here"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 140
    invoke-virtual {v1, v2}, Ljava/lang/RuntimeException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 141
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v7, "%s did not terminate in less than %dms, see cause."

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v5, v4, v6

    .line 143
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v3

    .line 141
    invoke-static {v7, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    move-exception v1

    .line 146
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    .line 147
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 150
    :cond_2
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
