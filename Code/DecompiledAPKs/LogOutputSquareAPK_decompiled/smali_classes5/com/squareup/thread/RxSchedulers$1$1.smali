.class Lcom/squareup/thread/RxSchedulers$1$1;
.super Lio/reactivex/Scheduler$Worker;
.source "RxSchedulers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/thread/RxSchedulers$1;->createWorker()Lio/reactivex/Scheduler$Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private volatile disposed:Z

.field final synthetic this$0:Lcom/squareup/thread/RxSchedulers$1;


# direct methods
.method constructor <init>(Lcom/squareup/thread/RxSchedulers$1;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/squareup/thread/RxSchedulers$1$1;->this$0:Lcom/squareup/thread/RxSchedulers$1;

    invoke-direct {p0}, Lio/reactivex/Scheduler$Worker;-><init>()V

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    const/4 v0, 0x1

    .line 86
    iput-boolean v0, p0, Lcom/squareup/thread/RxSchedulers$1$1;->disposed:Z

    .line 87
    iget-object v0, p0, Lcom/squareup/thread/RxSchedulers$1$1;->this$0:Lcom/squareup/thread/RxSchedulers$1;

    invoke-static {v0}, Lcom/squareup/thread/RxSchedulers$1;->access$100(Lcom/squareup/thread/RxSchedulers$1;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .line 92
    iget-boolean v0, p0, Lcom/squareup/thread/RxSchedulers$1$1;->disposed:Z

    return v0
.end method

.method public schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lio/reactivex/disposables/Disposable;
    .locals 2

    if-eqz p1, :cond_4

    if-eqz p4, :cond_3

    .line 57
    iget-boolean v0, p0, Lcom/squareup/thread/RxSchedulers$1$1;->disposed:Z

    if-eqz v0, :cond_0

    .line 58
    invoke-static {}, Lio/reactivex/disposables/Disposables;->disposed()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1

    .line 61
    :cond_0
    invoke-static {p1}, Lio/reactivex/plugins/RxJavaPlugins;->onSchedule(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object p1

    .line 64
    iget-object v0, p0, Lcom/squareup/thread/RxSchedulers$1$1;->this$0:Lcom/squareup/thread/RxSchedulers$1;

    invoke-static {v0, p1, p2, p3}, Lcom/squareup/thread/RxSchedulers$1;->access$000(Lcom/squareup/thread/RxSchedulers$1;Ljava/lang/Runnable;J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    invoke-static {}, Lio/reactivex/disposables/Disposables;->empty()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1

    .line 68
    :cond_1
    new-instance v0, Lcom/squareup/thread/RxSchedulers$1$ScheduledRunnable;

    iget-object v1, p0, Lcom/squareup/thread/RxSchedulers$1$1;->this$0:Lcom/squareup/thread/RxSchedulers$1;

    invoke-direct {v0, v1, p1}, Lcom/squareup/thread/RxSchedulers$1$ScheduledRunnable;-><init>(Lcom/squareup/thread/RxSchedulers$1;Ljava/lang/Runnable;)V

    .line 70
    iget-object p1, p0, Lcom/squareup/thread/RxSchedulers$1$1;->this$0:Lcom/squareup/thread/RxSchedulers$1;

    invoke-static {p1}, Lcom/squareup/thread/RxSchedulers$1;->access$100(Lcom/squareup/thread/RxSchedulers$1;)Landroid/os/Handler;

    move-result-object p1

    invoke-static {p1, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;

    move-result-object p1

    .line 71
    iput-object p0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 73
    iget-object v1, p0, Lcom/squareup/thread/RxSchedulers$1$1;->this$0:Lcom/squareup/thread/RxSchedulers$1;

    invoke-static {v1}, Lcom/squareup/thread/RxSchedulers$1;->access$100(Lcom/squareup/thread/RxSchedulers$1;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide p2

    invoke-virtual {v1, p1, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 76
    iget-boolean p1, p0, Lcom/squareup/thread/RxSchedulers$1$1;->disposed:Z

    if-eqz p1, :cond_2

    .line 77
    iget-object p1, p0, Lcom/squareup/thread/RxSchedulers$1$1;->this$0:Lcom/squareup/thread/RxSchedulers$1;

    invoke-static {p1}, Lcom/squareup/thread/RxSchedulers$1;->access$100(Lcom/squareup/thread/RxSchedulers$1;)Landroid/os/Handler;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 78
    invoke-static {}, Lio/reactivex/disposables/Disposables;->disposed()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1

    :cond_2
    return-object v0

    .line 55
    :cond_3
    new-instance p1, Ljava/lang/NullPointerException;

    const-string/jumbo p2, "unit == null"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 54
    :cond_4
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "run == null"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
