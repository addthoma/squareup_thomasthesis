.class public final Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsTutorialEvents;
.super Ljava/lang/Object;
.source "SeparatedPrintoutsTutorialEvents.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsTutorialEvents;",
        "",
        "()V",
        "PRINTOUTS_HAND_TO_MERCHANT",
        "",
        "PRINTOUTS_PRINT_RECEIPT",
        "separated-printouts_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsTutorialEvents;

.field public static final PRINTOUTS_HAND_TO_MERCHANT:Ljava/lang/String; = "T2 Shown Hand Terminal to Merchant"

.field public static final PRINTOUTS_PRINT_RECEIPT:Ljava/lang/String; = "T2 Shown Individual Print Receipt"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4
    new-instance v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsTutorialEvents;

    invoke-direct {v0}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsTutorialEvents;-><init>()V

    sput-object v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsTutorialEvents;->INSTANCE:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsTutorialEvents;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
