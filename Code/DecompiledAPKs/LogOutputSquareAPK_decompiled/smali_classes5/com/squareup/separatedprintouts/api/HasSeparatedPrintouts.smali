.class public interface abstract Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;
.super Ljava/lang/Object;
.source "HasSeparatedPrintouts.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts$NeverHasSeparatedPrintouts;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001:\u0001\rJ&\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH&J\u0008\u0010\u000b\u001a\u00020\u0003H&J\u0008\u0010\u000c\u001a\u00020\u0003H&\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;",
        "",
        "hasPrintoutsForUnsavedOpenTicket",
        "",
        "order",
        "Lcom/squareup/payment/OrderSnapshot;",
        "openTicket",
        "Lcom/squareup/tickets/OpenTicket;",
        "voidedItems",
        "",
        "Lcom/squareup/checkout/CartItem;",
        "hasPrintoutsWithNoPaperReceipt",
        "hasPrintoutsWithPaperReceipt",
        "NeverHasSeparatedPrintouts",
        "separated-printouts_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract hasPrintoutsForUnsavedOpenTicket(Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/OrderSnapshot;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)Z"
        }
    .end annotation
.end method

.method public abstract hasPrintoutsWithNoPaperReceipt()Z
.end method

.method public abstract hasPrintoutsWithPaperReceipt()Z
.end method
