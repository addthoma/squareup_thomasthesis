.class public final Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideR4DecoderFactory;
.super Ljava/lang/Object;
.source "SquarewaveLibraryModule_ProvideR4DecoderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/squarewave/m1/R4Decoder;",
        ">;"
    }
.end annotation


# instance fields
.field private final decoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/m1/MessengerR4Decoder;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/squarewave/library/SquarewaveLibraryModule;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/squarewave/library/SquarewaveLibraryModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/m1/MessengerR4Decoder;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideR4DecoderFactory;->module:Lcom/squareup/squarewave/library/SquarewaveLibraryModule;

    .line 26
    iput-object p2, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideR4DecoderFactory;->decoderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Ljavax/inject/Provider;)Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideR4DecoderFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/squarewave/library/SquarewaveLibraryModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/m1/MessengerR4Decoder;",
            ">;)",
            "Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideR4DecoderFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideR4DecoderFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideR4DecoderFactory;-><init>(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideR4Decoder(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Lcom/squareup/squarewave/m1/MessengerR4Decoder;)Lcom/squareup/squarewave/m1/R4Decoder;
    .locals 0

    .line 41
    invoke-virtual {p0, p1}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule;->provideR4Decoder(Lcom/squareup/squarewave/m1/MessengerR4Decoder;)Lcom/squareup/squarewave/m1/R4Decoder;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/squarewave/m1/R4Decoder;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/squarewave/m1/R4Decoder;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideR4DecoderFactory;->module:Lcom/squareup/squarewave/library/SquarewaveLibraryModule;

    iget-object v1, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideR4DecoderFactory;->decoderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/squarewave/m1/MessengerR4Decoder;

    invoke-static {v0, v1}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideR4DecoderFactory;->provideR4Decoder(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Lcom/squareup/squarewave/m1/MessengerR4Decoder;)Lcom/squareup/squarewave/m1/R4Decoder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideR4DecoderFactory;->get()Lcom/squareup/squarewave/m1/R4Decoder;

    move-result-object v0

    return-object v0
.end method
