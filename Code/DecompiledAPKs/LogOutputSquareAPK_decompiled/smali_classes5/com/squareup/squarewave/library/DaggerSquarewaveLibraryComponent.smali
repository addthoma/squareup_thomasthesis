.class public final Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;
.super Ljava/lang/Object;
.source "DaggerSquarewaveLibraryComponent.java"

# interfaces
.implements Lcom/squareup/squarewave/library/SquarewaveLibraryComponent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_application;,
        Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_swipeBus;,
        Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_telephonyManager;,
        Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_mainThread;,
        Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$Builder;
    }
.end annotation


# instance fields
.field private androidHeadsetConnectionListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;",
            ">;"
        }
    .end annotation
.end field

.field private applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private messengerR4DecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/m1/MessengerR4Decoder;",
            ">;"
        }
    .end annotation
.end field

.field private messengerSampleProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/gum/MessengerSampleProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private provideBadEventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation
.end field

.field private provideClassifyingDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/ClassifyingDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideCrashnadoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;"
        }
    .end annotation
.end field

.field private provideEventDataForwarderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/EventDataForwarder;",
            ">;"
        }
    .end annotation
.end field

.field private provideFastSqLinkSignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideGen2SignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideInputSampleRateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private provideMessengerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SingleCardreaderMessenger;",
            ">;"
        }
    .end annotation
.end field

.field private provideMicRecorderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/MicRecorder;",
            ">;"
        }
    .end annotation
.end field

.field private provideO1SignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/o1/O1SignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideR4DecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/m1/R4Decoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideR4FastSignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideR4SlowSignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideReaderTypeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/ReaderTypeProvider;",
            ">;"
        }
    .end annotation
.end field

.field private provideSampleFeederProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SampleFeeder;",
            ">;"
        }
    .end annotation
.end field

.field private provideSampleProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/gum/SampleProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private provideSlowSqLinkSignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideSquarewaveDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SquarewaveDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideSwipeEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/SwipeEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private swipeBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBus;",
            ">;"
        }
    .end annotation
.end field

.field private telephonyManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;)V
    .locals 0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    invoke-direct {p0, p1, p2}, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->initialize(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$1;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1, p2}, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;-><init>(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;)V

    return-void
.end method

.method public static builder()Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$Builder;
    .locals 2

    .line 119
    new-instance v0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$Builder;-><init>(Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$1;)V

    return-object v0
.end method

.method private initialize(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;)V
    .locals 13

    .line 125
    invoke-static {p1}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideCrashnadoFactory;->create(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideCrashnadoFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideCrashnadoProvider:Ljavax/inject/Provider;

    .line 126
    invoke-static {}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAndroidDeviceParamsFactory;->create()Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAndroidDeviceParamsFactory;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideInputSampleRateFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideInputSampleRateFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideInputSampleRateProvider:Ljavax/inject/Provider;

    .line 127
    new-instance v0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_mainThread;

    invoke-direct {v0, p2}, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_mainThread;-><init>(Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;)V

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->mainThreadProvider:Ljavax/inject/Provider;

    .line 128
    new-instance v0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_telephonyManager;

    invoke-direct {v0, p2}, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_telephonyManager;-><init>(Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;)V

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->telephonyManagerProvider:Ljavax/inject/Provider;

    .line 129
    invoke-static {p1}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideBadEventSinkFactory;->create(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideBadEventSinkFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideBadEventSinkProvider:Ljavax/inject/Provider;

    .line 130
    new-instance v0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_swipeBus;

    invoke-direct {v0, p2}, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_swipeBus;-><init>(Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;)V

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->swipeBusProvider:Ljavax/inject/Provider;

    .line 131
    invoke-static {p1}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideMessengerFactory;->create(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideMessengerFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideMessengerProvider:Ljavax/inject/Provider;

    .line 132
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideMessengerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/squarewave/gum/MessengerSampleProcessor_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/squarewave/gum/MessengerSampleProcessor_Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->messengerSampleProcessorProvider:Ljavax/inject/Provider;

    .line 133
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->messengerSampleProcessorProvider:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSampleProcessorFactory;->create(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Ljavax/inject/Provider;)Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSampleProcessorFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideSampleProcessorProvider:Ljavax/inject/Provider;

    .line 134
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideSampleProcessorProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSampleFeederFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AudioModule_ProvideSampleFeederFactory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideSampleFeederProvider:Ljavax/inject/Provider;

    .line 135
    invoke-static {}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideO1SignalDecoderFactory;->create()Lcom/squareup/wavpool/swipe/DecoderModule_ProvideO1SignalDecoderFactory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideO1SignalDecoderProvider:Ljavax/inject/Provider;

    .line 136
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideMessengerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/squarewave/m1/MessengerR4Decoder_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/squarewave/m1/MessengerR4Decoder_Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->messengerR4DecoderProvider:Ljavax/inject/Provider;

    .line 137
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->messengerR4DecoderProvider:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideR4DecoderFactory;->create(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Ljavax/inject/Provider;)Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideR4DecoderFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideR4DecoderProvider:Ljavax/inject/Provider;

    .line 138
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideR4DecoderProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideFastSqLinkSignalDecoderFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/DecoderModule_ProvideFastSqLinkSignalDecoderFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideFastSqLinkSignalDecoderProvider:Ljavax/inject/Provider;

    .line 139
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideFastSqLinkSignalDecoderProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4FastSignalDecoderFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4FastSignalDecoderFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideR4FastSignalDecoderProvider:Ljavax/inject/Provider;

    .line 140
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideR4DecoderProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideSlowSqLinkSignalDecoderFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/DecoderModule_ProvideSlowSqLinkSignalDecoderFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideSlowSqLinkSignalDecoderProvider:Ljavax/inject/Provider;

    .line 141
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideSlowSqLinkSignalDecoderProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4SlowSignalDecoderFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4SlowSignalDecoderFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideR4SlowSignalDecoderProvider:Ljavax/inject/Provider;

    .line 142
    invoke-static {}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideGen2SignalDecoderFactory;->create()Lcom/squareup/wavpool/swipe/DecoderModule_ProvideGen2SignalDecoderFactory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideGen2SignalDecoderProvider:Ljavax/inject/Provider;

    .line 143
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideO1SignalDecoderProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideR4FastSignalDecoderProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideR4SlowSignalDecoderProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideGen2SignalDecoderProvider:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideClassifyingDecoderProvider:Ljavax/inject/Provider;

    .line 144
    invoke-static {p1}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSwipeEventLoggerFactory;->create(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSwipeEventLoggerFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideSwipeEventLoggerProvider:Ljavax/inject/Provider;

    .line 145
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideSwipeEventLoggerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/AudioModule_ProvideEventDataForwarderFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AudioModule_ProvideEventDataForwarderFactory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideEventDataForwarderProvider:Ljavax/inject/Provider;

    .line 146
    invoke-static {p1}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideCardReaderIdFactory;->create(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideCardReaderIdFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    .line 147
    invoke-static {p1}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideReaderTypeProviderFactory;->create(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideReaderTypeProviderFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideReaderTypeProvider:Ljavax/inject/Provider;

    .line 148
    new-instance p1, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_application;

    invoke-direct {p1, p2}, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_application;-><init>(Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;)V

    iput-object p1, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->applicationProvider:Ljavax/inject/Provider;

    .line 149
    iget-object p1, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->applicationProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->androidHeadsetConnectionListenerProvider:Ljavax/inject/Provider;

    .line 150
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideCrashnadoProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideBadEventSinkProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->swipeBusProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideSampleFeederProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideClassifyingDecoderProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/wavpool/swipe/AsyncDecoderModule_ProvideDecoderExecutorFactory;->create()Lcom/squareup/wavpool/swipe/AsyncDecoderModule_ProvideDecoderExecutorFactory;

    move-result-object v6

    iget-object v7, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideSampleProcessorProvider:Ljavax/inject/Provider;

    iget-object v8, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideEventDataForwarderProvider:Ljavax/inject/Provider;

    iget-object v9, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideSwipeEventLoggerProvider:Ljavax/inject/Provider;

    iget-object v10, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    iget-object v11, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideReaderTypeProvider:Ljavax/inject/Provider;

    iget-object v12, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->androidHeadsetConnectionListenerProvider:Ljavax/inject/Provider;

    invoke-static/range {v0 .. v12}, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideSquarewaveDecoderProvider:Ljavax/inject/Provider;

    .line 151
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideCrashnadoProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideInputSampleRateProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAndroidDeviceParamsFactory;->create()Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAndroidDeviceParamsFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->telephonyManagerProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideBadEventSinkProvider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideSquarewaveDecoderProvider:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->androidHeadsetConnectionListenerProvider:Ljavax/inject/Provider;

    invoke-static/range {v0 .. v7}, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideMicRecorderProvider:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public squarewave()Lcom/squareup/squarewave/library/SquarewaveLibrary;
    .locals 3

    .line 156
    new-instance v0, Lcom/squareup/squarewave/library/SquarewaveLibrary;

    iget-object v1, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideMicRecorderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/wavpool/swipe/Recorder;

    iget-object v2, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;->provideEventDataForwarderProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/squarewave/EventDataListener;

    invoke-direct {v0, v1, v2}, Lcom/squareup/squarewave/library/SquarewaveLibrary;-><init>(Lcom/squareup/wavpool/swipe/Recorder;Lcom/squareup/squarewave/EventDataListener;)V

    return-object v0
.end method
