.class synthetic Lcom/squareup/squarewave/gum/Mapping$1;
.super Ljava/lang/Object;
.source "Mapping.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squarewave/gum/Mapping;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$Card$Brand:[I

.field static final synthetic $SwitchMap$com$squareup$protos$logging$events$swipe_experience$SignalFound$LinkType:[I

.field static final synthetic $SwitchMap$com$squareup$protos$logging$events$swipe_experience$SignalFound$ReaderType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 94
    invoke-static {}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->values()[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$protos$logging$events$swipe_experience$SignalFound$ReaderType:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$protos$logging$events$swipe_experience$SignalFound$ReaderType:[I

    sget-object v2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->GEN2_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    invoke-virtual {v2}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$protos$logging$events$swipe_experience$SignalFound$ReaderType:[I

    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->O1_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    invoke-virtual {v3}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$protos$logging$events$swipe_experience$SignalFound$ReaderType:[I

    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->R4_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    invoke-virtual {v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    .line 79
    :catch_2
    invoke-static {}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->values()[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$protos$logging$events$swipe_experience$SignalFound$LinkType:[I

    :try_start_3
    sget-object v3, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$protos$logging$events$swipe_experience$SignalFound$LinkType:[I

    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->GEN2:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-virtual {v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v3, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$protos$logging$events$swipe_experience$SignalFound$LinkType:[I

    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->O1:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-virtual {v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->ordinal()I

    move-result v4

    aput v1, v3, v4
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v3, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$protos$logging$events$swipe_experience$SignalFound$LinkType:[I

    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-virtual {v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    const/4 v3, 0x4

    :try_start_6
    sget-object v4, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$protos$logging$events$swipe_experience$SignalFound$LinkType:[I

    sget-object v5, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-virtual {v5}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    .line 16
    :catch_6
    invoke-static {}, Lcom/squareup/Card$Brand;->values()[Lcom/squareup/Card$Brand;

    move-result-object v4

    array-length v4, v4

    new-array v4, v4, [I

    sput-object v4, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$Card$Brand:[I

    :try_start_7
    sget-object v4, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$Card$Brand:[I

    sget-object v5, Lcom/squareup/Card$Brand;->VISA:Lcom/squareup/Card$Brand;

    invoke-virtual {v5}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v5

    aput v0, v4, v5
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v0, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$Card$Brand:[I

    sget-object v4, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    invoke-virtual {v4}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v4

    aput v1, v0, v4
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    :try_start_9
    sget-object v0, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$Card$Brand:[I

    sget-object v1, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v0, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$Card$Brand:[I

    sget-object v1, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    aput v3, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    :try_start_b
    sget-object v0, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$Card$Brand:[I

    sget-object v1, Lcom/squareup/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :catch_b
    :try_start_c
    sget-object v0, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$Card$Brand:[I

    sget-object v1, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_c

    :catch_c
    :try_start_d
    sget-object v0, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$Card$Brand:[I

    sget-object v1, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_d

    :catch_d
    :try_start_e
    sget-object v0, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$Card$Brand:[I

    sget-object v1, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_e

    :catch_e
    :try_start_f
    sget-object v0, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$Card$Brand:[I

    sget-object v1, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_f

    :catch_f
    return-void
.end method
