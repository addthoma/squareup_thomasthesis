.class public final Lcom/squareup/squarewave/gum/M1PacketCardDataEncrypted;
.super Ljava/lang/Object;
.source "M1PacketCardDataEncrypted.java"


# static fields
.field private static final M1_PACKET_AUTH_TAG_LENGTH:I = 0x8

.field private static final M1_PACKET_MAX_TRACK_1_LENGTH:I = 0x80

.field private static final M1_PACKET_MAX_TRACK_2_LENGTH:I = 0x80

.field private static final M1_PACKET_MAX_TRACK_3_LENGTH:I = 0x80


# instance fields
.field public final authTag:Ljava/nio/ByteBuffer;

.field public final trackData:Ljava/nio/ByteBuffer;


# direct methods
.method constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x8

    .line 15
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 16
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataEncrypted;->authTag:Ljava/nio/ByteBuffer;

    .line 17
    iput-object p1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataEncrypted;->trackData:Ljava/nio/ByteBuffer;

    .line 18
    iget-object p1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataEncrypted;->trackData:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataEncrypted;->authTag:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 19
    iget-object p1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataEncrypted;->trackData:Ljava/nio/ByteBuffer;

    const/16 v0, 0x180

    invoke-static {p1, v0}, Lcom/squareup/squarewave/gum/Buffers;->next(Ljava/nio/ByteBuffer;I)Ljava/nio/ByteBuffer;

    return-void
.end method
