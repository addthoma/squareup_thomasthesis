.class public Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;
.super Ljava/lang/Object;
.source "SqLinkSignalDecoder.java"

# interfaces
.implements Lcom/squareup/squarewave/SignalDecoder;


# instance fields
.field private final linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

.field private final r4Decoder:Lcom/squareup/squarewave/m1/R4Decoder;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/m1/R4Decoder;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;)V
    .locals 2

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    if-eq p2, v0, :cond_1

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    if-ne p2, v0, :cond_0

    goto :goto_0

    .line 31
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unsupported linkType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 34
    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->r4Decoder:Lcom/squareup/squarewave/m1/R4Decoder;

    .line 35
    iput-object p2, p0, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    return-void
.end method

.method private buildCard(Lcom/squareup/squarewave/gum/CardDataPacket;)Lcom/squareup/Card;
    .locals 4

    .line 157
    new-instance v0, Lcom/squareup/Card$Builder;

    invoke-direct {v0}, Lcom/squareup/Card$Builder;-><init>()V

    .line 158
    sget-object v1, Lcom/squareup/Card$InputType;->R4_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->inputType(Lcom/squareup/Card$InputType;)Lcom/squareup/Card$Builder;

    .line 160
    iget-object v1, p1, Lcom/squareup/squarewave/gum/CardDataPacket;->fullPacket:Ljava/nio/ByteBuffer;

    invoke-static {v1}, Lcom/squareup/squarewave/util/Base64;->encode(Ljava/nio/ByteBuffer;)[C

    move-result-object v1

    .line 161
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v2

    .line 163
    invoke-virtual {v0, v2}, Lcom/squareup/Card$Builder;->trackData(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v2

    iget-object v3, p1, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataAuthenticated:Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;

    iget-object v3, v3, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->last4:Ljava/lang/String;

    .line 164
    invoke-virtual {v2, v3}, Lcom/squareup/Card$Builder;->pan(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v2

    iget-object v3, p1, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataAuthenticated:Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;

    iget-object v3, v3, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->name:Ljava/lang/String;

    .line 165
    invoke-virtual {v2, v3}, Lcom/squareup/Card$Builder;->name(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v2

    iget-object v3, p1, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataAuthenticated:Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;

    iget-object v3, v3, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->issuer:Lcom/squareup/Card$Brand;

    .line 166
    invoke-virtual {v2, v3}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    .line 168
    invoke-virtual {v0}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object v0

    const/16 v2, 0x30

    .line 171
    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([CC)V

    .line 172
    invoke-virtual {p1}, Lcom/squareup/squarewave/gum/CardDataPacket;->clearAllData()V

    return-object v0
.end method

.method private buildCardData(Lcom/squareup/squarewave/gum/CardDataPacket;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;)Lcom/squareup/squarewave/decode/DemodResult;
    .locals 4

    .line 97
    iget-object v0, p1, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataAuthenticated:Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;

    .line 98
    iget-object v1, p1, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataPlainText:Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;

    .line 100
    invoke-virtual {v0}, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->track1SuccessfullyDecoded()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->track1_read_success:Ljava/lang/Boolean;

    .line 101
    invoke-virtual {v0}, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->track2SuccessfullyDecoded()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->track2_read_success:Ljava/lang/Boolean;

    .line 102
    iget-object v2, v1, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->swipeDirection:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    iput-object v2, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    .line 104
    iget-wide v2, v1, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->counter:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p3, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->counter:Ljava/lang/Long;

    .line 105
    iget-object v2, p1, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataPlainText:Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;

    iget-wide v2, v2, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->entropy:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p3, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->entropy:Ljava/lang/Long;

    .line 106
    iget-short v2, v1, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->bitPeriodBWD:S

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p3, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->bit_period_bwd:Ljava/lang/Integer;

    .line 107
    iget-short v2, v1, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->bitPeriodBit140:S

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p3, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->bit_period_bit_140:Ljava/lang/Integer;

    .line 108
    iget-short v1, v1, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->bitPeriodEndOfSwipe:S

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p3, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->bit_period_end_of_swipe:Ljava/lang/Integer;

    .line 109
    iget v1, v0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t1Len:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p3, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track1_len:Ljava/lang/Integer;

    .line 110
    iget v1, v0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t2Len:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p3, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track2_len:Ljava/lang/Integer;

    .line 111
    iget-byte v1, v0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t1Result:B

    invoke-direct {p0, v1}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->errorByteToEnum(B)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    move-result-object v1

    iput-object v1, p3, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    .line 112
    iget-byte v1, v0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t2Result:B

    invoke-direct {p0, v1}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->errorByteToEnum(B)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    move-result-object v1

    iput-object v1, p3, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    .line 113
    iget-object v1, v0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->name:Ljava/lang/String;

    .line 114
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p3, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->name_length:Ljava/lang/Integer;

    .line 120
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p3, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->name_not_blank:Ljava/lang/Boolean;

    .line 122
    invoke-virtual {v0}, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->track1SuccessfullyDecoded()Z

    move-result p3

    if-nez p3, :cond_2

    invoke-virtual {v0}, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->track2SuccessfullyDecoded()Z

    move-result p3

    if-eqz p3, :cond_1

    goto :goto_1

    .line 128
    :cond_1
    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->successfulDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object p1

    return-object p1

    .line 123
    :cond_2
    :goto_1
    iget-object p3, v0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->issuer:Lcom/squareup/Card$Brand;

    invoke-static {p3}, Lcom/squareup/squarewave/gum/Mapping;->mapBrandToIssuer(Lcom/squareup/Card$Brand;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    move-result-object p3

    iput-object p3, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    .line 124
    invoke-direct {p0, p1}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->buildCard(Lcom/squareup/squarewave/gum/CardDataPacket;)Lcom/squareup/Card;

    move-result-object p1

    .line 125
    invoke-virtual {v0}, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->track1SuccessfullyDecoded()Z

    move-result p2

    .line 126
    invoke-virtual {v0}, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->track2SuccessfullyDecoded()Z

    move-result p3

    .line 125
    invoke-static {p1, p2, p3}, Lcom/squareup/squarewave/decode/DemodResult;->successfulDemodWithCard(Lcom/squareup/Card;ZZ)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object p1

    return-object p1
.end method

.method private buildDemodInfo(Lcom/squareup/squarewave/gum/StatsAndMaybePacket;I)Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;
    .locals 2

    .line 178
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;-><init>()V

    .line 179
    invoke-direct {p0}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->linkTypeToDemodType()Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->demod_type:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    .line 180
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    iput-object p2, v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->runtime_in_us:Ljava/lang/Integer;

    .line 181
    iget-object p1, p1, Lcom/squareup/squarewave/gum/StatsAndMaybePacket;->stats:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    iput-object p1, v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->sqlink_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    .line 182
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;

    move-result-object p1

    return-object p1
.end method

.method private buildFromCardDataPacket(Lcom/squareup/squarewave/Signal;Lcom/squareup/squarewave/gum/CardDataPacket;)Lcom/squareup/squarewave/decode/DemodResult;
    .locals 3

    .line 64
    iget-object p1, p1, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    .line 65
    iget-object v0, p0, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    iput-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 66
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->R4_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    iput-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p2, Lcom/squareup/squarewave/gum/CardDataPacket;->sqLinkHeader:Lcom/squareup/squarewave/gum/SqLinkHeader;

    iget-object v1, v1, Lcom/squareup/squarewave/gum/SqLinkHeader;->readerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_hardware_id:Ljava/lang/String;

    .line 68
    invoke-virtual {p2}, Lcom/squareup/squarewave/gum/CardDataPacket;->getPacketType()Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->asProtoPacketType()Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 70
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;-><init>()V

    .line 73
    sget-object v1, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder$1;->$SwitchMap$com$squareup$squarewave$gum$SqLinkHeader$PacketType:[I

    invoke-virtual {p2}, Lcom/squareup/squarewave/gum/CardDataPacket;->getPacketType()Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 75
    new-instance v1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;-><init>()V

    .line 76
    invoke-direct {p0, p2, p1, v1}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->buildCardData(Lcom/squareup/squarewave/gum/CardDataPacket;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v2

    .line 77
    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;->r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    goto :goto_0

    .line 85
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "packetType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/squarewave/gum/CardDataPacket;->getPacketType()Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 82
    :cond_1
    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->successfulDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v2

    .line 88
    :goto_0
    iget-object v1, p2, Lcom/squareup/squarewave/gum/CardDataPacket;->sqLinkHeader:Lcom/squareup/squarewave/gum/SqLinkHeader;

    iget v1, v1, Lcom/squareup/squarewave/gum/SqLinkHeader;->hwMajorRev:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;->hardware_major_revision:Ljava/lang/Integer;

    .line 89
    iget-object p2, p2, Lcom/squareup/squarewave/gum/CardDataPacket;->sqLinkHeader:Lcom/squareup/squarewave/gum/SqLinkHeader;

    iget p2, p2, Lcom/squareup/squarewave/gum/SqLinkHeader;->hwMinorRev:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    iput-object p2, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;->hardware_minor_revision:Ljava/lang/Integer;

    .line 90
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    move-result-object p2

    iput-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    return-object v2
.end method

.method private errorByteToEnum(B)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;
    .locals 2

    .line 133
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;-><init>()V

    const/4 v1, 0x0

    .line 134
    invoke-virtual {p0, p1, v1}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->isSet(BI)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->track_not_present:Ljava/lang/Boolean;

    const/4 v1, 0x1

    .line 135
    invoke-virtual {p0, p1, v1}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->isSet(BI)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->no_zeros_detected:Ljava/lang/Boolean;

    const/4 v1, 0x2

    .line 136
    invoke-virtual {p0, p1, v1}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->isSet(BI)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->card_data_too_long:Ljava/lang/Boolean;

    const/4 v1, 0x3

    .line 137
    invoke-virtual {p0, p1, v1}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->isSet(BI)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->card_data_too_short:Ljava/lang/Boolean;

    const/4 v1, 0x4

    .line 138
    invoke-virtual {p0, p1, v1}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->isSet(BI)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->missing_start_sentinel:Ljava/lang/Boolean;

    const/4 v1, 0x5

    .line 139
    invoke-virtual {p0, p1, v1}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->isSet(BI)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->missing_end_sentinel:Ljava/lang/Boolean;

    const/4 v1, 0x6

    .line 140
    invoke-virtual {p0, p1, v1}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->isSet(BI)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->parity_error:Ljava/lang/Boolean;

    const/4 v1, 0x7

    .line 141
    invoke-virtual {p0, p1, v1}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->isSet(BI)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->lrc_failure:Ljava/lang/Boolean;

    .line 142
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    move-result-object p1

    return-object p1
.end method

.method private linkTypeToDemodType()Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;
    .locals 2

    .line 190
    iget-object v0, p0, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    :goto_0
    return-object v0
.end method


# virtual methods
.method public decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;
    .locals 6

    .line 39
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 40
    iget-object v2, p0, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->r4Decoder:Lcom/squareup/squarewave/m1/R4Decoder;

    iget-object v3, p0, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-virtual {p1}, Lcom/squareup/squarewave/Signal;->samples()[S

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/squareup/squarewave/m1/R4Decoder;->decodeR4Packet(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;[S)Lcom/squareup/squarewave/gum/StatsAndMaybePacket;

    move-result-object v2

    .line 43
    invoke-virtual {v2}, Lcom/squareup/squarewave/gum/StatsAndMaybePacket;->isSuccessfulDemod()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 44
    sget-object v3, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder$1;->$SwitchMap$com$squareup$squarewave$gum$SqLinkHeader$PacketType:[I

    iget-object v4, v2, Lcom/squareup/squarewave/gum/StatsAndMaybePacket;->packet:Lcom/squareup/squarewave/gum/CardDataPacket;

    invoke-virtual {v4}, Lcom/squareup/squarewave/gum/CardDataPacket;->getPacketType()Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    .line 50
    iget-object v3, v2, Lcom/squareup/squarewave/gum/StatsAndMaybePacket;->packet:Lcom/squareup/squarewave/gum/CardDataPacket;

    invoke-direct {p0, p1, v3}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->buildFromCardDataPacket(Lcom/squareup/squarewave/Signal;Lcom/squareup/squarewave/gum/CardDataPacket;)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v3

    goto :goto_0

    .line 54
    :cond_0
    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->failedDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v3

    .line 57
    :goto_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long/2addr v4, v0

    const-wide/16 v0, 0x3e8

    div-long/2addr v4, v0

    long-to-int v0, v4

    .line 58
    iget-object p1, p1, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->demod_info:Ljava/util/List;

    invoke-direct {p0, v2, v0}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;->buildDemodInfo(Lcom/squareup/squarewave/gum/StatsAndMaybePacket;I)Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v3
.end method

.method isSet(BI)Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x1

    shl-int p2, v0, p2

    and-int/2addr p1, p2

    if-eqz p1, :cond_0

    .line 151
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method
