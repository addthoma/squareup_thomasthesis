.class public Lcom/squareup/squarewave/EventDataForwarder;
.super Ljava/lang/Object;
.source "EventDataForwarder.java"

# interfaces
.implements Lcom/squareup/squarewave/EventDataListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/squarewave/EventDataForwarder$NoOpCarrierDetectListener;,
        Lcom/squareup/squarewave/EventDataForwarder$OnCarrierDetectedListener;
    }
.end annotation


# static fields
.field public static final SAMPLE_RATE:I = 0xac44


# instance fields
.field private listener:Lcom/squareup/squarewave/EventDataForwarder$OnCarrierDetectedListener;

.field private final logger:Lcom/squareup/logging/SwipeEventLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/logging/SwipeEventLogger;)V
    .locals 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/squarewave/EventDataForwarder;->logger:Lcom/squareup/logging/SwipeEventLogger;

    .line 16
    new-instance p1, Lcom/squareup/squarewave/EventDataForwarder$NoOpCarrierDetectListener;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lcom/squareup/squarewave/EventDataForwarder$NoOpCarrierDetectListener;-><init>(Lcom/squareup/squarewave/EventDataForwarder;Lcom/squareup/squarewave/EventDataForwarder$1;)V

    iput-object p1, p0, Lcom/squareup/squarewave/EventDataForwarder;->listener:Lcom/squareup/squarewave/EventDataForwarder$OnCarrierDetectedListener;

    return-void
.end method


# virtual methods
.method public onReceiveEventData(Lcom/squareup/squarewave/gum/EventData;)V
    .locals 4

    .line 24
    iget-object v0, p0, Lcom/squareup/squarewave/EventDataForwarder;->listener:Lcom/squareup/squarewave/EventDataForwarder$OnCarrierDetectedListener;

    if-nez v0, :cond_0

    return-void

    .line 26
    :cond_0
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;-><init>()V

    .line 27
    iget-object v1, p1, Lcom/squareup/squarewave/gum/EventData;->carrierDetectInfo:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    .line 28
    iget-object v1, p1, Lcom/squareup/squarewave/gum/EventData;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    .line 30
    iget-object v1, p1, Lcom/squareup/squarewave/gum/EventData;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    sget-object v2, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->SIGNAL_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    if-ne v1, v2, :cond_1

    .line 34
    new-instance v1, Lcom/squareup/squarewave/Signal;

    iget-object v2, p1, Lcom/squareup/squarewave/gum/EventData;->signal:[S

    const v3, 0xac44

    iget-object p1, p1, Lcom/squareup/squarewave/gum/EventData;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-direct {v1, v2, v3, p1, v0}, Lcom/squareup/squarewave/Signal;-><init>([SILcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;)V

    .line 35
    iget-object p1, p0, Lcom/squareup/squarewave/EventDataForwarder;->listener:Lcom/squareup/squarewave/EventDataForwarder$OnCarrierDetectedListener;

    invoke-interface {p1, v1}, Lcom/squareup/squarewave/EventDataForwarder$OnCarrierDetectedListener;->onCarrierDetect(Lcom/squareup/squarewave/Signal;)V

    goto :goto_0

    .line 37
    :cond_1
    iget-object p1, p0, Lcom/squareup/squarewave/EventDataForwarder;->logger:Lcom/squareup/logging/SwipeEventLogger;

    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/logging/SwipeEventLogger;->logReaderCarrierDetectEvent(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;)V

    :goto_0
    return-void
.end method

.method public setOnCarrierDetectedListener(Lcom/squareup/squarewave/EventDataForwarder$OnCarrierDetectedListener;)V
    .locals 1

    if-nez p1, :cond_0

    .line 20
    new-instance p1, Lcom/squareup/squarewave/EventDataForwarder$NoOpCarrierDetectListener;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lcom/squareup/squarewave/EventDataForwarder$NoOpCarrierDetectListener;-><init>(Lcom/squareup/squarewave/EventDataForwarder;Lcom/squareup/squarewave/EventDataForwarder$1;)V

    :cond_0
    iput-object p1, p0, Lcom/squareup/squarewave/EventDataForwarder;->listener:Lcom/squareup/squarewave/EventDataForwarder$OnCarrierDetectedListener;

    return-void
.end method
