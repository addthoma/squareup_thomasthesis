.class public Lcom/squareup/squarewave/Signal;
.super Ljava/lang/Object;
.source "Signal.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/squarewave/Signal$Builder;
    }
.end annotation


# instance fields
.field public final eventBuilder:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

.field private final sampleRate:I

.field private final samples:[S

.field public final signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

.field private final triggersError:Z


# direct methods
.method private constructor <init>(Lcom/squareup/squarewave/Signal$Builder;)V
    .locals 2

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-static {p1}, Lcom/squareup/squarewave/Signal$Builder;->access$000(Lcom/squareup/squarewave/Signal$Builder;)[S

    move-result-object v0

    const-string v1, "samples"

    invoke-static {v1, v0}, Lcom/squareup/squarewave/Signal;->require(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [S

    iput-object v0, p0, Lcom/squareup/squarewave/Signal;->samples:[S

    .line 67
    invoke-static {p1}, Lcom/squareup/squarewave/Signal$Builder;->access$100(Lcom/squareup/squarewave/Signal$Builder;)I

    move-result v0

    iput v0, p0, Lcom/squareup/squarewave/Signal;->sampleRate:I

    .line 68
    invoke-static {p1}, Lcom/squareup/squarewave/Signal$Builder;->access$200(Lcom/squareup/squarewave/Signal$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/squarewave/Signal;->triggersError:Z

    .line 69
    invoke-static {p1}, Lcom/squareup/squarewave/Signal$Builder;->access$300(Lcom/squareup/squarewave/Signal$Builder;)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/Signal;->eventBuilder:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    .line 70
    invoke-static {p1}, Lcom/squareup/squarewave/Signal$Builder;->access$400(Lcom/squareup/squarewave/Signal$Builder;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/squarewave/Signal$Builder;Lcom/squareup/squarewave/Signal$1;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1}, Lcom/squareup/squarewave/Signal;-><init>(Lcom/squareup/squarewave/Signal$Builder;)V

    return-void
.end method

.method public constructor <init>([SILcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;)V
    .locals 1

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "samples"

    .line 49
    invoke-static {v0, p1}, Lcom/squareup/squarewave/Signal;->require(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [S

    iput-object p1, p0, Lcom/squareup/squarewave/Signal;->samples:[S

    .line 50
    iput p2, p0, Lcom/squareup/squarewave/Signal;->sampleRate:I

    const/4 p1, 0x1

    .line 53
    iput-boolean p1, p0, Lcom/squareup/squarewave/Signal;->triggersError:Z

    .line 54
    iput-object p4, p0, Lcom/squareup/squarewave/Signal;->eventBuilder:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    .line 55
    new-instance p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;-><init>()V

    iput-object p1, p0, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    .line 57
    iget-object p1, p0, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    sget-object p4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->UNKNOWN_SIGNAL:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    iput-object p4, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 58
    iget-object p1, p0, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    iput-object p3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 61
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    iput-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->sample_rate:Ljava/lang/Integer;

    .line 62
    iget-object p1, p0, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->demod_info:Ljava/util/List;

    return-void
.end method

.method public constructor <init>([SIZ)V
    .locals 1

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "samples"

    .line 35
    invoke-static {v0, p1}, Lcom/squareup/squarewave/Signal;->require(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [S

    iput-object p1, p0, Lcom/squareup/squarewave/Signal;->samples:[S

    .line 36
    iput p2, p0, Lcom/squareup/squarewave/Signal;->sampleRate:I

    .line 37
    iput-boolean p3, p0, Lcom/squareup/squarewave/Signal;->triggersError:Z

    .line 38
    new-instance p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;-><init>()V

    iput-object p1, p0, Lcom/squareup/squarewave/Signal;->eventBuilder:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    .line 39
    new-instance p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;-><init>()V

    iput-object p1, p0, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    .line 41
    iget-object p1, p0, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    sget-object p3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->UNKNOWN_SIGNAL:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    iput-object p3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 42
    iget-object p1, p0, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    iput-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->sample_rate:Ljava/lang/Integer;

    .line 43
    iget-object p1, p0, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->demod_info:Ljava/util/List;

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/squarewave/Signal;)[S
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/squareup/squarewave/Signal;->samples:[S

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/squarewave/Signal;)I
    .locals 0

    .line 11
    iget p0, p0, Lcom/squareup/squarewave/Signal;->sampleRate:I

    return p0
.end method

.method static synthetic access$700(Lcom/squareup/squarewave/Signal;)Z
    .locals 0

    .line 11
    iget-boolean p0, p0, Lcom/squareup/squarewave/Signal;->triggersError:Z

    return p0
.end method

.method private static require(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TT;)TT;"
        }
    .end annotation

    if-eqz p1, :cond_0

    return-object p1

    .line 102
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Required object is null: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/squarewave/Signal$Builder;
    .locals 1

    .line 107
    new-instance v0, Lcom/squareup/squarewave/Signal$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/squarewave/Signal$Builder;-><init>(Lcom/squareup/squarewave/Signal;)V

    return-object v0
.end method

.method public sampleRate()I
    .locals 1

    .line 74
    iget v0, p0, Lcom/squareup/squarewave/Signal;->sampleRate:I

    return v0
.end method

.method public samples()[S
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/squarewave/Signal;->samples:[S

    return-object v0
.end method

.method public triggersError()Z
    .locals 1

    .line 91
    iget-boolean v0, p0, Lcom/squareup/squarewave/Signal;->triggersError:Z

    return v0
.end method

.method public zeroize()V
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/squareup/squarewave/Signal;->samples:[S

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([SS)V

    return-void
.end method
