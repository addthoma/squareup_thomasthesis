.class public Lcom/squareup/squarewave/gen2/Reading;
.super Ljava/lang/Object;
.source "Reading.java"


# instance fields
.field public final level:I

.field private final maxSampleIndex:I

.field private final minSampleIndex:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lcom/squareup/squarewave/gen2/Reading;->level:I

    .line 26
    iput p2, p0, Lcom/squareup/squarewave/gen2/Reading;->minSampleIndex:I

    .line 27
    iput p3, p0, Lcom/squareup/squarewave/gen2/Reading;->maxSampleIndex:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/squarewave/gen2/Peak;Lcom/squareup/squarewave/gen2/Peak;)V
    .locals 1

    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/squarewave/gen2/Reading;-><init>(Lcom/squareup/squarewave/gen2/Peak;Lcom/squareup/squarewave/gen2/Peak;Lcom/squareup/squarewave/gen2/Peak;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/squarewave/gen2/Peak;Lcom/squareup/squarewave/gen2/Peak;Lcom/squareup/squarewave/gen2/Peak;)V
    .locals 2

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iget v0, p1, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    iget v1, p3, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    if-ge v0, v1, :cond_0

    .line 39
    iget p1, p1, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    iput p1, p0, Lcom/squareup/squarewave/gen2/Reading;->minSampleIndex:I

    .line 40
    iget p1, p3, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    iput p1, p0, Lcom/squareup/squarewave/gen2/Reading;->maxSampleIndex:I

    goto :goto_0

    .line 42
    :cond_0
    iget p3, p3, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    iput p3, p0, Lcom/squareup/squarewave/gen2/Reading;->minSampleIndex:I

    .line 43
    iget p1, p1, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    iput p1, p0, Lcom/squareup/squarewave/gen2/Reading;->maxSampleIndex:I

    :goto_0
    if-nez p2, :cond_1

    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    const/4 p1, 0x1

    .line 46
    :goto_1
    iput p1, p0, Lcom/squareup/squarewave/gen2/Reading;->level:I

    return-void
.end method


# virtual methods
.method public getMaxSampleIndex()I
    .locals 1

    .line 60
    iget v0, p0, Lcom/squareup/squarewave/gen2/Reading;->maxSampleIndex:I

    return v0
.end method

.method public getMinSampleIndex()I
    .locals 1

    .line 53
    iget v0, p0, Lcom/squareup/squarewave/gen2/Reading;->minSampleIndex:I

    return v0
.end method

.method public sampleCount()I
    .locals 2

    .line 65
    iget v0, p0, Lcom/squareup/squarewave/gen2/Reading;->maxSampleIndex:I

    iget v1, p0, Lcom/squareup/squarewave/gen2/Reading;->minSampleIndex:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 69
    iget v0, p0, Lcom/squareup/squarewave/gen2/Reading;->level:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
