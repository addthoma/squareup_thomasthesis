.class public Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;
.super Ljava/lang/Object;
.source "Gen2Track2Decoder.java"

# interfaces
.implements Lcom/squareup/squarewave/gen2/Gen2ReadingsDecoder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/squarewave/gen2/Gen2Track2Decoder$Holder;
    }
.end annotation


# static fields
.field protected static final COUNTRY_CODE_LENGTH:I = 0x3

.field protected static final EXPIRATION_DATE_LENGTH:I = 0x4

.field private static final FS:C = '='

.field private static final SS:C = ';'

.field private static final UNKNOWN_CARD_CHARACTER:Lcom/squareup/squarewave/gen2/CardCharacter;


# instance fields
.field private final bitsPerCharacter:I

.field private final characterSet:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/squarewave/gen2/CardCharacter;",
            ">;"
        }
    .end annotation
.end field

.field private final startSentinelPattern:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 87
    new-instance v0, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v1, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->UNKNOWN:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v2, 0x7e

    invoke-direct {v0, v2, v1}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    sput-object v0, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->UNKNOWN_CARD_CHARACTER:Lcom/squareup/squarewave/gen2/CardCharacter;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 91
    invoke-static {}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->createCharacterSet()Ljava/util/Map;

    move-result-object v0

    const-string v1, "11010"

    const/4 v2, 0x5

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;-><init>(Ljava/util/Map;Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/squarewave/gen2/Gen2Track2Decoder$1;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/util/Map;Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/squarewave/gen2/CardCharacter;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->characterSet:Ljava/util/Map;

    .line 104
    iput-object p2, p0, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->startSentinelPattern:Ljava/lang/String;

    .line 105
    iput p3, p0, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bitsPerCharacter:I

    return-void
.end method

.method protected static bits(II)Ljava/lang/Integer;
    .locals 0

    .line 315
    invoke-static {p1}, Lcom/squareup/squarewave/MathUtil;->reverse(I)I

    move-result p1

    rsub-int/lit8 p0, p0, 0x1f

    ushr-int p0, p1, p0

    .line 318
    invoke-static {p0}, Lcom/squareup/squarewave/MathUtil;->bitCount(I)I

    move-result p1

    .line 319
    invoke-static {p1}, Lcom/squareup/squarewave/MathUtil;->isOdd(I)Z

    move-result p1

    if-nez p1, :cond_0

    or-int/lit8 p0, p0, 0x1

    .line 321
    :cond_0
    new-instance p1, Ljava/lang/Integer;

    invoke-direct {p1, p0}, Ljava/lang/Integer;-><init>(I)V

    return-object p1
.end method

.method private createCard(Ljava/util/List;)Lcom/squareup/Card;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/squarewave/gen2/CharacterReading;",
            ">;)",
            "Lcom/squareup/Card;"
        }
    .end annotation

    .line 213
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 214
    new-array v1, v0, [C

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 216
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/squarewave/gen2/CharacterReading;

    .line 217
    invoke-virtual {v3}, Lcom/squareup/squarewave/gen2/CharacterReading;->getCharacter()C

    move-result v3

    aput-char v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 220
    :cond_0
    invoke-virtual {p0, v1}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->createCard([C)Lcom/squareup/Card;

    move-result-object p1

    return-object p1
.end method

.method private static createCharacterSet()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/squarewave/gen2/CardCharacter;",
            ">;"
        }
    .end annotation

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 45
    invoke-static {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bits(II)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v4, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->DATA_NUMERIC:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v5, 0x30

    invoke-direct {v3, v5, v4}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x1

    .line 46
    invoke-static {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bits(II)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v4, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->DATA_NUMERIC:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v5, 0x31

    invoke-direct {v3, v5, v4}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x2

    .line 47
    invoke-static {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bits(II)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v4, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->DATA_NUMERIC:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v5, 0x32

    invoke-direct {v3, v5, v4}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x3

    .line 48
    invoke-static {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bits(II)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v4, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->DATA_NUMERIC:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v5, 0x33

    invoke-direct {v3, v5, v4}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    invoke-static {v1, v1}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bits(II)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v4, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->DATA_NUMERIC:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v5, 0x34

    invoke-direct {v3, v5, v4}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x5

    .line 50
    invoke-static {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bits(II)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v4, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->DATA_NUMERIC:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v5, 0x35

    invoke-direct {v3, v5, v4}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x6

    .line 51
    invoke-static {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bits(II)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v4, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->DATA_NUMERIC:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v5, 0x36

    invoke-direct {v3, v5, v4}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x7

    .line 52
    invoke-static {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bits(II)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v4, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->DATA_NUMERIC:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v5, 0x37

    invoke-direct {v3, v5, v4}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v2, 0x8

    .line 53
    invoke-static {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bits(II)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v4, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->DATA_NUMERIC:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v5, 0x38

    invoke-direct {v3, v5, v4}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v2, 0x9

    .line 54
    invoke-static {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bits(II)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v4, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->DATA_NUMERIC:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v5, 0x39

    invoke-direct {v3, v5, v4}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v2, 0xa

    .line 55
    invoke-static {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bits(II)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v4, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->CONTROL:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v5, 0x3a

    invoke-direct {v3, v5, v4}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v2, 0xb

    .line 56
    invoke-static {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bits(II)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v4, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->START_SENTINEL:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v5, 0x3b

    invoke-direct {v3, v5, v4}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v2, 0xc

    .line 57
    invoke-static {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bits(II)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v4, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->CONTROL:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v5, 0x3c

    invoke-direct {v3, v5, v4}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v2, 0xd

    .line 58
    invoke-static {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bits(II)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v4, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->SEPARATOR:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v5, 0x3d

    invoke-direct {v3, v5, v4}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v2, 0xe

    .line 59
    invoke-static {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bits(II)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v4, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->CONTROL:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v5, 0x3e

    invoke-direct {v3, v5, v4}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v2, 0xf

    .line 60
    invoke-static {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bits(II)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/squareup/squarewave/gen2/CardCharacter;

    sget-object v3, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->END_SENTINEL:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    const/16 v4, 0x3f

    invoke-direct {v2, v4, v3}, Lcom/squareup/squarewave/gen2/CardCharacter;-><init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method private decodeCharacter(I)Lcom/squareup/squarewave/gen2/CardCharacter;
    .locals 2

    .line 297
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->characterSet:Ljava/util/Map;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/squarewave/gen2/CardCharacter;

    if-nez p1, :cond_0

    .line 298
    sget-object p1, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->UNKNOWN_CARD_CHARACTER:Lcom/squareup/squarewave/gen2/CardCharacter;

    :cond_0
    return-object p1
.end method

.method private decodeCharacter([Lcom/squareup/squarewave/gen2/Reading;II)Lcom/squareup/squarewave/gen2/CharacterReading;
    .locals 4

    const/4 v0, 0x0

    move v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v0, p3, :cond_2

    .line 271
    aget-object v2, p1, v0

    .line 272
    iget v3, v2, Lcom/squareup/squarewave/gen2/Reading;->level:I

    if-nez v3, :cond_0

    shl-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 275
    :cond_0
    iget v2, v2, Lcom/squareup/squarewave/gen2/Reading;->level:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    shl-int/lit8 v1, v1, 0x1

    or-int/2addr v1, v3

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 280
    :cond_2
    invoke-direct {p0, v1}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->decodeCharacter(I)Lcom/squareup/squarewave/gen2/CardCharacter;

    move-result-object v0

    .line 281
    new-instance v1, Lcom/squareup/squarewave/gen2/CharacterReading;

    invoke-direct {v1, v0, p1, p2, p3}, Lcom/squareup/squarewave/gen2/CharacterReading;-><init>(Lcom/squareup/squarewave/gen2/CardCharacter;[Lcom/squareup/squarewave/gen2/Reading;II)V

    return-object v1
.end method

.method public static instance()Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;
    .locals 1

    .line 32
    invoke-static {}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder$Holder;->access$100()Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;

    move-result-object v0

    return-object v0
.end method

.method private lrc([Lcom/squareup/squarewave/gen2/Reading;I[I)Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;
    .locals 7

    .line 230
    array-length v0, p3

    .line 232
    array-length v1, p1

    add-int v2, p2, v0

    add-int/lit8 v3, v2, 0x1

    if-ge v1, v3, :cond_0

    .line 234
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->MISSING_LRC:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-object p1

    .line 239
    :cond_0
    new-array v1, v0, [I

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v4, v0, :cond_1

    add-int v6, p2, v4

    .line 241
    aget-object v6, p1, v6

    iget v6, v6, Lcom/squareup/squarewave/gen2/Reading;->level:I

    aput v6, v1, v4

    .line 242
    aget v6, v1, v4

    add-int/2addr v5, v6

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 244
    :cond_1
    aget-object p1, p1, v2

    iget p1, p1, Lcom/squareup/squarewave/gen2/Reading;->level:I

    add-int/2addr v5, p1

    .line 245
    invoke-static {v5}, Lcom/squareup/squarewave/MathUtil;->isOdd(I)Z

    move-result p1

    if-nez p1, :cond_2

    .line 246
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->LRC_SELF_PARITY:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-object p1

    :cond_2
    :goto_1
    if-ge v3, v0, :cond_4

    .line 253
    aget p1, p3, v3

    aget p2, v1, v3

    add-int/2addr p1, p2

    invoke-static {p1}, Lcom/squareup/squarewave/MathUtil;->isOdd(I)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 254
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->CARD_DATA_FAILED_LRC_CHECK:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-object p1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 258
    :cond_4
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-object p1
.end method


# virtual methods
.method public createCard([C)Lcom/squareup/Card;
    .locals 6

    .line 342
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([C)V

    const/16 p1, 0x3b

    .line 343
    invoke-virtual {v0, p1}, Ljava/lang/String;->indexOf(I)I

    move-result p1

    const/16 v1, 0x3d

    .line 344
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    const/4 v3, 0x0

    if-nez p1, :cond_4

    if-gez v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 p1, p1, 0x1

    .line 350
    invoke-virtual {v0, p1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    add-int/lit8 v2, v2, 0x1

    .line 356
    invoke-virtual {p0, p1}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->hasCountryCode(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    add-int/lit8 v2, v2, 0x3

    :cond_1
    add-int/lit8 v4, v2, 0x4

    .line 361
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-le v4, v5, :cond_2

    goto :goto_0

    .line 363
    :cond_2
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, v1, :cond_3

    goto :goto_0

    .line 366
    :cond_3
    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 369
    :goto_0
    new-instance v1, Lcom/squareup/Card$Builder;

    invoke-direct {v1}, Lcom/squareup/Card$Builder;-><init>()V

    sget-object v2, Lcom/squareup/Card$InputType;->GEN2_TRACK2:Lcom/squareup/Card$InputType;

    invoke-virtual {v1, v2}, Lcom/squareup/Card$Builder;->inputType(Lcom/squareup/Card$InputType;)Lcom/squareup/Card$Builder;

    move-result-object v1

    .line 370
    invoke-virtual {v1, v0}, Lcom/squareup/Card$Builder;->trackData(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v0

    .line 371
    invoke-virtual {v0, p1}, Lcom/squareup/Card$Builder;->pan(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 372
    invoke-virtual {p1, v3}, Lcom/squareup/Card$Builder;->expirationDate(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 373
    invoke-virtual {p1}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object p1

    return-object p1

    :cond_4
    :goto_1
    return-object v3
.end method

.method public decode([Lcom/squareup/squarewave/gen2/Reading;Lcom/squareup/squarewave/gen2/CardDataStart;Ljava/util/List;Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/squareup/squarewave/gen2/Reading;",
            "Lcom/squareup/squarewave/gen2/CardDataStart;",
            "Ljava/util/List<",
            "Lcom/squareup/squarewave/gen2/CharacterReading;",
            ">;",
            "Lcom/squareup/squarewave/gen2/Gen2Swipe;",
            ")",
            "Lcom/squareup/squarewave/gen2/Gen2DemodResult;"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    .line 129
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->clear()V

    .line 132
    iget v3, v0, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->bitsPerCharacter:I

    .line 133
    array-length v4, v1

    sub-int/2addr v4, v3

    .line 135
    sget-object v5, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->END_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    add-int/lit8 v6, v3, -0x1

    .line 142
    new-array v7, v6, [I

    .line 143
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/squarewave/gen2/CardDataStart;->getStartSentinelIndex()I

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    :goto_0
    const/4 v11, 0x1

    if-ge v8, v4, :cond_4

    const/4 v12, 0x0

    :goto_1
    if-ge v12, v6, :cond_0

    .line 151
    aget v13, v7, v12

    add-int v14, v8, v12

    aget-object v14, v1, v14

    iget v14, v14, Lcom/squareup/squarewave/gen2/Reading;->level:I

    add-int/2addr v13, v14

    aput v13, v7, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    :cond_0
    add-int v12, v8, v3

    .line 154
    invoke-direct {p0, v1, v8, v12}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->decodeCharacter([Lcom/squareup/squarewave/gen2/Reading;II)Lcom/squareup/squarewave/gen2/CharacterReading;

    move-result-object v8

    .line 156
    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    invoke-virtual {v8}, Lcom/squareup/squarewave/gen2/CharacterReading;->getType()Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    move-result-object v13

    sget-object v14, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->UNKNOWN:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    if-ne v13, v14, :cond_1

    const/4 v10, 0x1

    .line 162
    :cond_1
    invoke-virtual {v8}, Lcom/squareup/squarewave/gen2/CharacterReading;->getType()Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    move-result-object v8

    sget-object v13, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->END_SENTINEL:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    if-ne v8, v13, :cond_3

    if-eqz v10, :cond_2

    .line 164
    sget-object v5, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->BAD_CHARACTER:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    goto :goto_2

    .line 166
    :cond_2
    invoke-direct {p0, v1, v12, v7}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->lrc([Lcom/squareup/squarewave/gen2/Reading;I[I)Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    move-result-object v5

    goto :goto_2

    :cond_3
    move v8, v12

    goto :goto_0

    :cond_4
    :goto_2
    const/4 v1, 0x0

    .line 173
    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    if-ne v5, v3, :cond_5

    .line 176
    invoke-direct {p0, v2}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->createCard(Ljava/util/List;)Lcom/squareup/Card;

    move-result-object v1

    if-nez v1, :cond_5

    .line 180
    sget-object v5, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->DECODE_FAILURE:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    .line 186
    :cond_5
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->clear()V

    .line 189
    sget-object v2, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    if-ne v5, v2, :cond_7

    .line 190
    invoke-static {v1, v9, v11}, Lcom/squareup/squarewave/decode/DemodResult;->successfulDemodWithCard(Lcom/squareup/Card;ZZ)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v2

    .line 191
    invoke-virtual/range {p4 .. p4}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->getSignal()Lcom/squareup/squarewave/Signal;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    .line 192
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->GEN2:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    iput-object v4, v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 193
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->GEN2_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    iput-object v4, v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    .line 194
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/squarewave/gen2/CardDataStart;->isForward()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 195
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->FORWARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    iput-object v4, v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    goto :goto_3

    .line 197
    :cond_6
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->BACKWARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    iput-object v4, v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    .line 199
    :goto_3
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->GEN2_SUCCESSFUL_SWIPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    iput-object v4, v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 200
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->track2_read_success:Ljava/lang/Boolean;

    .line 201
    invoke-virtual {v1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/squarewave/gum/Mapping;->mapBrandToIssuer(Lcom/squareup/Card$Brand;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    move-result-object v1

    iput-object v1, v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    goto :goto_4

    .line 203
    :cond_7
    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->failedDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v2

    .line 205
    :goto_4
    new-instance v1, Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    invoke-direct {v1, v2, v5}, Lcom/squareup/squarewave/gen2/Gen2DemodResult;-><init>(Lcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;)V

    return-object v1
.end method

.method protected hasCountryCode(Ljava/lang/CharSequence;)Z
    .locals 4

    .line 116
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v3, 0x35

    if-ne v0, v3, :cond_0

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result p1

    const/16 v0, 0x39

    if-ne p1, v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public final startSentinelIndex(Ljava/lang/String;)I
    .locals 2

    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->startSentinelPattern:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 123
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p1

    if-gez p1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    add-int/lit8 p1, p1, 0x4

    :goto_0
    return p1
.end method
