.class public Lcom/squareup/squarewave/gen2/CardCharacter;
.super Ljava/lang/Object;
.source "CardCharacter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/squarewave/gen2/CardCharacter$Type;
    }
.end annotation


# instance fields
.field private final character:C

.field private final type:Lcom/squareup/squarewave/gen2/CardCharacter$Type;


# direct methods
.method public constructor <init>(CLcom/squareup/squarewave/gen2/CardCharacter$Type;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-char p1, p0, Lcom/squareup/squarewave/gen2/CardCharacter;->character:C

    .line 30
    iput-object p2, p0, Lcom/squareup/squarewave/gen2/CardCharacter;->type:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    return-void
.end method


# virtual methods
.method public getCharacter()C
    .locals 1

    .line 35
    iget-char v0, p0, Lcom/squareup/squarewave/gen2/CardCharacter;->character:C

    return v0
.end method

.method public getType()Lcom/squareup/squarewave/gen2/CardCharacter$Type;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/CardCharacter;->type:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    return-object v0
.end method
