.class public final Lcom/squareup/squarewave/gen2/Denoiser$Order;
.super Ljava/lang/Object;
.source "Denoiser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squarewave/gen2/Denoiser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Order"
.end annotation


# static fields
.field public static final ORDER_11:Lcom/squareup/squarewave/gen2/Denoiser$Order;

.field public static final ORDER_13:Lcom/squareup/squarewave/gen2/Denoiser$Order;

.field public static final ORDER_15:Lcom/squareup/squarewave/gen2/Denoiser$Order;

.field public static final ORDER_17:Lcom/squareup/squarewave/gen2/Denoiser$Order;

.field public static final ORDER_19:Lcom/squareup/squarewave/gen2/Denoiser$Order;

.field public static final ORDER_21:Lcom/squareup/squarewave/gen2/Denoiser$Order;

.field public static final ORDER_23:Lcom/squareup/squarewave/gen2/Denoiser$Order;

.field public static final ORDER_25:Lcom/squareup/squarewave/gen2/Denoiser$Order;

.field public static final ORDER_5:Lcom/squareup/squarewave/gen2/Denoiser$Order;

.field public static final ORDER_7:Lcom/squareup/squarewave/gen2/Denoiser$Order;

.field public static final ORDER_9:Lcom/squareup/squarewave/gen2/Denoiser$Order;

.field public static final ORDER_NONE:Lcom/squareup/squarewave/gen2/Denoiser$Order;

.field public static final VALUES:[Lcom/squareup/squarewave/gen2/Denoiser$Order;


# instance fields
.field private final coefficients:[I

.field private final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 29
    new-instance v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v3, 0x0

    aput v3, v2, v3

    const/4 v4, -0x1

    invoke-direct {v0, v4, v2}, Lcom/squareup/squarewave/gen2/Denoiser$Order;-><init>(I[I)V

    sput-object v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_NONE:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    .line 31
    new-instance v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;

    const/4 v2, 0x3

    new-array v4, v2, [I

    fill-array-data v4, :array_0

    const/16 v5, 0x23

    invoke-direct {v0, v5, v4}, Lcom/squareup/squarewave/gen2/Denoiser$Order;-><init>(I[I)V

    sput-object v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_5:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    .line 33
    new-instance v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;

    const/4 v4, 0x4

    new-array v5, v4, [I

    fill-array-data v5, :array_1

    const/16 v6, 0x15

    invoke-direct {v0, v6, v5}, Lcom/squareup/squarewave/gen2/Denoiser$Order;-><init>(I[I)V

    sput-object v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_7:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    .line 35
    new-instance v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;

    const/4 v5, 0x5

    new-array v6, v5, [I

    fill-array-data v6, :array_2

    const/16 v7, 0xe7

    invoke-direct {v0, v7, v6}, Lcom/squareup/squarewave/gen2/Denoiser$Order;-><init>(I[I)V

    sput-object v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_9:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    .line 37
    new-instance v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;

    const/4 v6, 0x6

    new-array v7, v6, [I

    fill-array-data v7, :array_3

    const/16 v8, 0x1ad

    invoke-direct {v0, v8, v7}, Lcom/squareup/squarewave/gen2/Denoiser$Order;-><init>(I[I)V

    sput-object v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_11:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    .line 39
    new-instance v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;

    const/4 v7, 0x7

    new-array v8, v7, [I

    fill-array-data v8, :array_4

    const/16 v9, 0x8f

    invoke-direct {v0, v9, v8}, Lcom/squareup/squarewave/gen2/Denoiser$Order;-><init>(I[I)V

    sput-object v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_13:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    .line 41
    new-instance v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;

    const/16 v8, 0x8

    new-array v9, v8, [I

    fill-array-data v9, :array_5

    const/16 v10, 0x451

    invoke-direct {v0, v10, v9}, Lcom/squareup/squarewave/gen2/Denoiser$Order;-><init>(I[I)V

    sput-object v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_15:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    .line 43
    new-instance v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;

    const/16 v9, 0x9

    new-array v10, v9, [I

    fill-array-data v10, :array_6

    const/16 v11, 0x143

    invoke-direct {v0, v11, v10}, Lcom/squareup/squarewave/gen2/Denoiser$Order;-><init>(I[I)V

    sput-object v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_17:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    .line 45
    new-instance v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;

    const/16 v10, 0xa

    new-array v11, v10, [I

    fill-array-data v11, :array_7

    const/16 v12, 0x8d5

    invoke-direct {v0, v12, v11}, Lcom/squareup/squarewave/gen2/Denoiser$Order;-><init>(I[I)V

    sput-object v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_19:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    .line 47
    new-instance v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;

    const/16 v11, 0xb

    new-array v12, v11, [I

    fill-array-data v12, :array_8

    const/16 v13, 0xbf3

    invoke-direct {v0, v13, v12}, Lcom/squareup/squarewave/gen2/Denoiser$Order;-><init>(I[I)V

    sput-object v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_21:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    .line 49
    new-instance v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;

    const/16 v12, 0xc

    new-array v13, v12, [I

    fill-array-data v13, :array_9

    const/16 v14, 0x325

    invoke-direct {v0, v14, v13}, Lcom/squareup/squarewave/gen2/Denoiser$Order;-><init>(I[I)V

    sput-object v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_23:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    .line 51
    new-instance v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;

    const/16 v13, 0xd

    new-array v13, v13, [I

    fill-array-data v13, :array_a

    const/16 v14, 0x1437

    invoke-direct {v0, v14, v13}, Lcom/squareup/squarewave/gen2/Denoiser$Order;-><init>(I[I)V

    sput-object v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_25:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    new-array v0, v12, [Lcom/squareup/squarewave/gen2/Denoiser$Order;

    .line 54
    sget-object v12, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_NONE:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    aput-object v12, v0, v3

    sget-object v3, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_5:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_7:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    const/4 v3, 0x2

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_9:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_11:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_13:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_15:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_17:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_19:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_21:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_23:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_25:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    aput-object v1, v0, v11

    sput-object v0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->VALUES:[Lcom/squareup/squarewave/gen2/Denoiser$Order;

    return-void

    nop

    :array_0
    .array-data 4
        -0x3
        0xc
        0x11
    .end array-data

    :array_1
    .array-data 4
        -0x2
        0x3
        0x6
        0x7
    .end array-data

    :array_2
    .array-data 4
        -0x15
        0xe
        0x27
        0x36
        0x3b
    .end array-data

    :array_3
    .array-data 4
        -0x24
        0x9
        0x2c
        0x45
        0x54
        0x59
    .end array-data

    :array_4
    .array-data 4
        -0xb
        0x0
        0x9
        0x10
        0x15
        0x18
        0x19
    .end array-data

    :array_5
    .array-data 4
        -0x4e
        -0xd
        0x2a
        0x57
        0x7a
        0x93
        0xa2
        0xa7
    .end array-data

    :array_6
    .array-data 4
        -0x15
        -0x6
        0x7
        0x12
        0x1b
        0x22
        0x27
        0x2a
        0x2b
    .end array-data

    :array_7
    .array-data 4
        -0x88
        -0x33
        0x18
        0x59
        0x90
        0xbd
        0xe0
        0xf9
        0x108
        0x10d
    .end array-data

    :array_8
    .array-data 4
        -0xab
        -0x4c
        0x9
        0x54
        0x95
        0xcc
        0xf9
        0x11c
        0x135
        0x144
        0x149
    .end array-data

    :array_9
    .array-data 4
        -0x2a
        -0x15
        -0x2
        0xf
        0x1e
        0x2b
        0x36
        0x3f
        0x46
        0x4b
        0x4e
        0x4f
    .end array-data

    :array_a
    .array-data 4
        -0xfd
        -0x8a
        -0x21
        0x3e
        0x93
        0xde
        0x11f
        0x157
        0x183
        0x1a6
        0x1bf
        0x1ce
        0x1d3
    .end array-data
.end method

.method private constructor <init>(I[I)V
    .locals 4

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput p1, p0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->h:I

    .line 76
    array-length p1, p2

    shl-int/lit8 p1, p1, 0x1

    add-int/lit8 p1, p1, -0x1

    new-array p1, p1, [I

    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->coefficients:[I

    .line 79
    iget-object p1, p0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->coefficients:[I

    array-length v0, p2

    const/4 v1, 0x0

    invoke-static {p2, v1, p1, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 81
    array-length p1, p2

    .line 82
    array-length v0, p2

    add-int/lit8 v0, v0, -0x2

    :goto_0
    if-ltz v0, :cond_0

    .line 83
    iget-object v1, p0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->coefficients:[I

    add-int/lit8 v2, p1, 0x1

    aget v3, p2, v0

    aput v3, v1, p1

    add-int/lit8 v0, v0, -0x1

    move p1, v2

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/squareup/squarewave/gen2/Denoiser$Order;)I
    .locals 0

    .line 20
    iget p0, p0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->h:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/squarewave/gen2/Denoiser$Order;)[I
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->coefficients:[I

    return-object p0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ORDER_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/squarewave/gen2/Denoiser$Order;->coefficients:[I

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
