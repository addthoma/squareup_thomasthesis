.class public final enum Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;
.super Ljava/lang/Enum;
.source "FelicaBrandMapping.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\u0008\u0080\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;",
        "",
        "protoFelicaBrand",
        "Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;",
        "transactionFelicaBrand",
        "Lcom/squareup/transactionhistory/FelicaBrand;",
        "(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Lcom/squareup/transactionhistory/FelicaBrand;)V",
        "getProtoFelicaBrand$public_release",
        "()Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;",
        "getTransactionFelicaBrand$public_release",
        "()Lcom/squareup/transactionhistory/FelicaBrand;",
        "FELICA_ID",
        "FELICA_TRANSPORTATION",
        "FELICA_QP",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;

.field public static final enum FELICA_ID:Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;

.field public static final enum FELICA_QP:Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;

.field public static final enum FELICA_TRANSPORTATION:Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;


# instance fields
.field private final protoFelicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

.field private final transactionFelicaBrand:Lcom/squareup/transactionhistory/FelicaBrand;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;

    new-instance v1, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;

    .line 18
    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_ID:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 19
    sget-object v3, Lcom/squareup/transactionhistory/FelicaBrand;->FELICA_ID:Lcom/squareup/transactionhistory/FelicaBrand;

    const/4 v4, 0x0

    const-string v5, "FELICA_ID"

    .line 17
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Lcom/squareup/transactionhistory/FelicaBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;->FELICA_ID:Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;

    .line 23
    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_TRANSPORTATION:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 24
    sget-object v3, Lcom/squareup/transactionhistory/FelicaBrand;->FELICA_TRANSPORTATION:Lcom/squareup/transactionhistory/FelicaBrand;

    const/4 v4, 0x1

    const-string v5, "FELICA_TRANSPORTATION"

    .line 21
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Lcom/squareup/transactionhistory/FelicaBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;->FELICA_TRANSPORTATION:Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;

    .line 27
    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_QP:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 28
    sget-object v3, Lcom/squareup/transactionhistory/FelicaBrand;->FELICA_QP:Lcom/squareup/transactionhistory/FelicaBrand;

    const/4 v4, 0x2

    const-string v5, "FELICA_QP"

    .line 26
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Lcom/squareup/transactionhistory/FelicaBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;->FELICA_QP:Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;->$VALUES:[Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Lcom/squareup/transactionhistory/FelicaBrand;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;",
            "Lcom/squareup/transactionhistory/FelicaBrand;",
            ")V"
        }
    .end annotation

    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;->protoFelicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    iput-object p4, p0, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;->transactionFelicaBrand:Lcom/squareup/transactionhistory/FelicaBrand;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;
    .locals 1

    const-class v0, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;

    return-object p0
.end method

.method public static values()[Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;
    .locals 1

    sget-object v0, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;->$VALUES:[Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;

    invoke-virtual {v0}, [Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;

    return-object v0
.end method


# virtual methods
.method public final getProtoFelicaBrand$public_release()Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;->protoFelicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-object v0
.end method

.method public final getTransactionFelicaBrand$public_release()Lcom/squareup/transactionhistory/FelicaBrand;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;->transactionFelicaBrand:Lcom/squareup/transactionhistory/FelicaBrand;

    return-object v0
.end method
