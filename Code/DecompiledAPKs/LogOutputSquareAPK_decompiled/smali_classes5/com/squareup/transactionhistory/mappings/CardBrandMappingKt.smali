.class public final Lcom/squareup/transactionhistory/mappings/CardBrandMappingKt;
.super Ljava/lang/Object;
.source "CardBrandMapping.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCardBrandMapping.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CardBrandMapping.kt\ncom/squareup/transactionhistory/mappings/CardBrandMappingKt\n*L\n1#1,118:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000e\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u0004\u0018\u00010\u0002\u001a\u000e\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u0004\u0018\u00010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "toCardBrand",
        "Lcom/squareup/transactionhistory/CardBrand;",
        "Lcom/squareup/Card$Brand;",
        "Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toCardBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/transactionhistory/CardBrand;
    .locals 7

    const/4 v0, 0x0

    if-eqz p0, :cond_4

    .line 114
    invoke-static {}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->values()[Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    move-result-object v1

    .line 115
    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_2

    aget-object v5, v1, v4

    invoke-virtual {v5}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->getBillsCardBrand$public_release()Lcom/squareup/Card$Brand;

    move-result-object v6

    if-ne v6, p0, :cond_0

    const/4 v6, 0x1

    goto :goto_1

    :cond_0
    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_1

    move-object v0, v5

    goto :goto_2

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    :goto_2
    if-eqz v0, :cond_3

    .line 116
    invoke-virtual {v0}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->getTransactionHistoryCardBrand$public_release()Lcom/squareup/transactionhistory/CardBrand;

    move-result-object p0

    if-eqz p0, :cond_3

    goto :goto_3

    :cond_3
    sget-object p0, Lcom/squareup/transactionhistory/CardBrand;->UNKNOWN:Lcom/squareup/transactionhistory/CardBrand;

    :goto_3
    move-object v0, p0

    :cond_4
    return-object v0
.end method

.method public static final toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/transactionhistory/CardBrand;
    .locals 7

    const/4 v0, 0x0

    if-eqz p0, :cond_4

    .line 107
    invoke-static {}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->values()[Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    move-result-object v1

    .line 108
    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_2

    aget-object v5, v1, v4

    invoke-virtual {v5}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->getProtoCardBrand$public_release()Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    move-result-object v6

    if-ne v6, p0, :cond_0

    const/4 v6, 0x1

    goto :goto_1

    :cond_0
    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_1

    move-object v0, v5

    goto :goto_2

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    :goto_2
    if-eqz v0, :cond_3

    .line 109
    invoke-virtual {v0}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->getTransactionHistoryCardBrand$public_release()Lcom/squareup/transactionhistory/CardBrand;

    move-result-object p0

    if-eqz p0, :cond_3

    goto :goto_3

    :cond_3
    sget-object p0, Lcom/squareup/transactionhistory/CardBrand;->UNKNOWN:Lcom/squareup/transactionhistory/CardBrand;

    :goto_3
    move-object v0, p0

    :cond_4
    return-object v0
.end method
