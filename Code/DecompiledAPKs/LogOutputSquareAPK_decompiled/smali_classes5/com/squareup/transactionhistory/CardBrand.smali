.class public final enum Lcom/squareup/transactionhistory/CardBrand;
.super Ljava/lang/Enum;
.source "TenderInfo.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/transactionhistory/CardBrand;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0012\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/CardBrand;",
        "",
        "(Ljava/lang/String;I)V",
        "UNKNOWN",
        "VISA",
        "MASTERCARD",
        "AMERICAN_EXPRESS",
        "DISCOVER",
        "DISCOVER_DINERS",
        "JCB",
        "CHINA_UNIONPAY",
        "SQUARE_GIFT_CARD_V2",
        "INTERAC",
        "SQUARE_CAPITAL_CARD",
        "EFTPOS",
        "FELICA",
        "ALIPAY",
        "CASH_APP",
        "EBT",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/transactionhistory/CardBrand;

.field public static final enum ALIPAY:Lcom/squareup/transactionhistory/CardBrand;

.field public static final enum AMERICAN_EXPRESS:Lcom/squareup/transactionhistory/CardBrand;

.field public static final enum CASH_APP:Lcom/squareup/transactionhistory/CardBrand;

.field public static final enum CHINA_UNIONPAY:Lcom/squareup/transactionhistory/CardBrand;

.field public static final enum DISCOVER:Lcom/squareup/transactionhistory/CardBrand;

.field public static final enum DISCOVER_DINERS:Lcom/squareup/transactionhistory/CardBrand;

.field public static final enum EBT:Lcom/squareup/transactionhistory/CardBrand;

.field public static final enum EFTPOS:Lcom/squareup/transactionhistory/CardBrand;

.field public static final enum FELICA:Lcom/squareup/transactionhistory/CardBrand;

.field public static final enum INTERAC:Lcom/squareup/transactionhistory/CardBrand;

.field public static final enum JCB:Lcom/squareup/transactionhistory/CardBrand;

.field public static final enum MASTERCARD:Lcom/squareup/transactionhistory/CardBrand;

.field public static final enum SQUARE_CAPITAL_CARD:Lcom/squareup/transactionhistory/CardBrand;

.field public static final enum SQUARE_GIFT_CARD_V2:Lcom/squareup/transactionhistory/CardBrand;

.field public static final enum UNKNOWN:Lcom/squareup/transactionhistory/CardBrand;

.field public static final enum VISA:Lcom/squareup/transactionhistory/CardBrand;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/squareup/transactionhistory/CardBrand;

    new-instance v1, Lcom/squareup/transactionhistory/CardBrand;

    const/4 v2, 0x0

    const-string v3, "UNKNOWN"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/CardBrand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/CardBrand;->UNKNOWN:Lcom/squareup/transactionhistory/CardBrand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/CardBrand;

    const/4 v2, 0x1

    const-string v3, "VISA"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/CardBrand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/CardBrand;->VISA:Lcom/squareup/transactionhistory/CardBrand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/CardBrand;

    const/4 v2, 0x2

    const-string v3, "MASTERCARD"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/CardBrand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/CardBrand;->MASTERCARD:Lcom/squareup/transactionhistory/CardBrand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/CardBrand;

    const/4 v2, 0x3

    const-string v3, "AMERICAN_EXPRESS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/CardBrand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/CardBrand;->AMERICAN_EXPRESS:Lcom/squareup/transactionhistory/CardBrand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/CardBrand;

    const/4 v2, 0x4

    const-string v3, "DISCOVER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/CardBrand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/CardBrand;->DISCOVER:Lcom/squareup/transactionhistory/CardBrand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/CardBrand;

    const/4 v2, 0x5

    const-string v3, "DISCOVER_DINERS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/CardBrand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/CardBrand;->DISCOVER_DINERS:Lcom/squareup/transactionhistory/CardBrand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/CardBrand;

    const/4 v2, 0x6

    const-string v3, "JCB"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/CardBrand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/CardBrand;->JCB:Lcom/squareup/transactionhistory/CardBrand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/CardBrand;

    const/4 v2, 0x7

    const-string v3, "CHINA_UNIONPAY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/CardBrand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/CardBrand;->CHINA_UNIONPAY:Lcom/squareup/transactionhistory/CardBrand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/CardBrand;

    const/16 v2, 0x8

    const-string v3, "SQUARE_GIFT_CARD_V2"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/CardBrand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/CardBrand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/transactionhistory/CardBrand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/CardBrand;

    const/16 v2, 0x9

    const-string v3, "INTERAC"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/CardBrand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/CardBrand;->INTERAC:Lcom/squareup/transactionhistory/CardBrand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/CardBrand;

    const/16 v2, 0xa

    const-string v3, "SQUARE_CAPITAL_CARD"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/CardBrand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/CardBrand;->SQUARE_CAPITAL_CARD:Lcom/squareup/transactionhistory/CardBrand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/CardBrand;

    const/16 v2, 0xb

    const-string v3, "EFTPOS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/CardBrand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/CardBrand;->EFTPOS:Lcom/squareup/transactionhistory/CardBrand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/CardBrand;

    const/16 v2, 0xc

    const-string v3, "FELICA"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/CardBrand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/CardBrand;->FELICA:Lcom/squareup/transactionhistory/CardBrand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/CardBrand;

    const/16 v2, 0xd

    const-string v3, "ALIPAY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/CardBrand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/CardBrand;->ALIPAY:Lcom/squareup/transactionhistory/CardBrand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/CardBrand;

    const/16 v2, 0xe

    const-string v3, "CASH_APP"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/CardBrand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/CardBrand;->CASH_APP:Lcom/squareup/transactionhistory/CardBrand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/CardBrand;

    const-string v2, "EBT"

    const/16 v3, 0xf

    invoke-direct {v1, v2, v3}, Lcom/squareup/transactionhistory/CardBrand;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/CardBrand;->EBT:Lcom/squareup/transactionhistory/CardBrand;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/transactionhistory/CardBrand;->$VALUES:[Lcom/squareup/transactionhistory/CardBrand;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/transactionhistory/CardBrand;
    .locals 1

    const-class v0, Lcom/squareup/transactionhistory/CardBrand;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/transactionhistory/CardBrand;

    return-object p0
.end method

.method public static values()[Lcom/squareup/transactionhistory/CardBrand;
    .locals 1

    sget-object v0, Lcom/squareup/transactionhistory/CardBrand;->$VALUES:[Lcom/squareup/transactionhistory/CardBrand;

    invoke-virtual {v0}, [Lcom/squareup/transactionhistory/CardBrand;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/transactionhistory/CardBrand;

    return-object v0
.end method
