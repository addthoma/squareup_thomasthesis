.class public final Lcom/squareup/transactionhistory/historical/HistoricalTransactionsStoreKt;
.super Ljava/lang/Object;
.source "HistoricalTransactionsStore.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\n\u0010\u0000\"\u00020\u00012\u00020\u0001*\n\u0010\u0002\"\u00020\u00032\u00020\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "FetchTransactionFailure",
        "Lcom/squareup/transactionhistory/historical/FetchTransactionResult$Failure;",
        "FetchTransactionSuccess",
        "Lcom/squareup/transactionhistory/historical/FetchTransactionResult$Success;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
