.class public interface abstract Lcom/squareup/transactionhistory/processed/ProcessedTransactionsLoader;
.super Ljava/lang/Object;
.source "ProcessedTransactionsLoader.kt"

# interfaces
.implements Lcom/squareup/transactionhistory/ConfigurableTransactionsLoader;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/transactionhistory/ConfigurableTransactionsLoader<",
        "Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/processed/ProcessedTransactionsLoader;",
        "Lcom/squareup/transactionhistory/ConfigurableTransactionsLoader;",
        "Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
