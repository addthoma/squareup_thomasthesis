.class Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperViewListLifeCycleListener;
.super Ljava/lang/Object;
.source "StickyListHeadersListView.java"

# interfaces
.implements Lcom/squareup/stickylistheaders/WrapperViewList$LifeCycleListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/stickylistheaders/StickyListHeadersListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WrapperViewListLifeCycleListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;


# direct methods
.method private constructor <init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)V
    .locals 0

    .line 546
    iput-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperViewListLifeCycleListener;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Lcom/squareup/stickylistheaders/StickyListHeadersListView$1;)V
    .locals 0

    .line 546
    invoke-direct {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperViewListLifeCycleListener;-><init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)V

    return-void
.end method


# virtual methods
.method public onDispatchDrawOccurred(Landroid/graphics/Canvas;)V
    .locals 6

    .line 551
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 552
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperViewListLifeCycleListener;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-static {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$800(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Lcom/squareup/stickylistheaders/WrapperViewList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/stickylistheaders/WrapperViewList;->getFixedFirstVisibleItem()I

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$900(Lcom/squareup/stickylistheaders/StickyListHeadersListView;I)V

    .line 554
    :cond_0
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperViewListLifeCycleListener;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-static {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$200(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 555
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperViewListLifeCycleListener;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-static {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$1000(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Z

    move-result v0

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_1

    .line 556
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    const/4 v0, 0x0

    .line 557
    iget-object v3, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperViewListLifeCycleListener;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-static {v3}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$1100(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)I

    move-result v3

    iget-object v4, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperViewListLifeCycleListener;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-virtual {v4}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->getRight()I

    move-result v4

    iget-object v5, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperViewListLifeCycleListener;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-virtual {v5}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->getBottom()I

    move-result v5

    invoke-virtual {p1, v0, v3, v4, v5}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 558
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperViewListLifeCycleListener;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-static {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$200(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Landroid/view/View;

    move-result-object v3

    invoke-static {v0, p1, v3, v1, v2}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$1200(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 559
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0

    .line 561
    :cond_1
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperViewListLifeCycleListener;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-static {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$200(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Landroid/view/View;

    move-result-object v3

    invoke-static {v0, p1, v3, v1, v2}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$1300(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Landroid/graphics/Canvas;Landroid/view/View;J)Z

    :cond_2
    :goto_0
    return-void
.end method
