.class public Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;
.super Landroid/view/View$BaseSavedState;
.source "StickyListHeadersSavedState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/stickylistheaders/StickyListHeadersSavedState$StickyListHeadersSavedStateCreator;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private listState:Landroid/os/Parcelable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 38
    new-instance v0, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState$StickyListHeadersSavedStateCreator;

    invoke-direct {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState$StickyListHeadersSavedStateCreator;-><init>()V

    sput-object v0, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 31
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;->listState:Landroid/os/Parcelable;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 26
    iput-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;->listState:Landroid/os/Parcelable;

    return-void
.end method


# virtual methods
.method public getListState()Landroid/os/Parcelable;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;->listState:Landroid/os/Parcelable;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 42
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 43
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;->listState:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
