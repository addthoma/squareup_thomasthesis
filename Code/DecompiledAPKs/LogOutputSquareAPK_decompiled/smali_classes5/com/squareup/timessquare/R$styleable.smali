.class public final Lcom/squareup/timessquare/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/timessquare/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final CalendarPickerView:[I

.field public static final CalendarPickerView_android_background:I = 0x0

.field public static final CalendarPickerView_tsquare_dayBackground:I = 0x1

.field public static final CalendarPickerView_tsquare_dayTextColor:I = 0x2

.field public static final CalendarPickerView_tsquare_displayAlwaysDigitNumbers:I = 0x3

.field public static final CalendarPickerView_tsquare_displayDayNamesHeaderRow:I = 0x4

.field public static final CalendarPickerView_tsquare_displayHeader:I = 0x5

.field public static final CalendarPickerView_tsquare_dividerColor:I = 0x6

.field public static final CalendarPickerView_tsquare_headerTextColor:I = 0x7

.field public static final CalendarPickerView_tsquare_titleTextStyle:I = 0x8

.field public static final calendar_cell:[I

.field public static final calendar_cell_tsquare_state_current_month:I = 0x0

.field public static final calendar_cell_tsquare_state_highlighted:I = 0x1

.field public static final calendar_cell_tsquare_state_range_first:I = 0x2

.field public static final calendar_cell_tsquare_state_range_last:I = 0x3

.field public static final calendar_cell_tsquare_state_range_middle:I = 0x4

.field public static final calendar_cell_tsquare_state_selectable:I = 0x5

.field public static final calendar_cell_tsquare_state_today:I = 0x6


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x9

    new-array v0, v0, [I

    .line 95
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/timessquare/R$styleable;->CalendarPickerView:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    .line 105
    fill-array-data v0, :array_1

    sput-object v0, Lcom/squareup/timessquare/R$styleable;->calendar_cell:[I

    return-void

    :array_0
    .array-data 4
        0x10100d4
        0x7f04047c
        0x7f04047d
        0x7f04047e
        0x7f04047f
        0x7f040480
        0x7f040481
        0x7f040482
        0x7f04048a
    .end array-data

    :array_1
    .array-data 4
        0x7f040483
        0x7f040484
        0x7f040485
        0x7f040486
        0x7f040487
        0x7f040488
        0x7f040489
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
