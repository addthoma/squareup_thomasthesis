.class public Lcom/squareup/timessquare/CalendarPickerView;
.super Landroid/widget/ListView;
.source "CalendarPickerView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/timessquare/CalendarPickerView$DefaultOnInvalidDateSelectedListener;,
        Lcom/squareup/timessquare/CalendarPickerView$CellClickInterceptor;,
        Lcom/squareup/timessquare/CalendarPickerView$DateSelectableFilter;,
        Lcom/squareup/timessquare/CalendarPickerView$OnInvalidDateSelectedListener;,
        Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;,
        Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;,
        Lcom/squareup/timessquare/CalendarPickerView$MonthCellWithMonthIndex;,
        Lcom/squareup/timessquare/CalendarPickerView$CellClickedListener;,
        Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;,
        Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;
    }
.end annotation


# static fields
.field private static final explicitlyNumericYearLocaleLanguages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final adapter:Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;

.field private cellClickInterceptor:Lcom/squareup/timessquare/CalendarPickerView$CellClickInterceptor;

.field private final cells:Lcom/squareup/timessquare/IndexedLinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/timessquare/IndexedLinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/squareup/timessquare/MonthCellDescriptor;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private dateConfiguredListener:Lcom/squareup/timessquare/CalendarPickerView$DateSelectableFilter;

.field private dateListener:Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;

.field private dateTypeface:Landroid/graphics/Typeface;

.field private dayBackgroundResId:I

.field private dayTextColorResId:I

.field private dayViewAdapter:Lcom/squareup/timessquare/DayViewAdapter;

.field private decorators:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/timessquare/CalendarCellDecorator;",
            ">;"
        }
    .end annotation
.end field

.field private displayAlwaysDigitNumbers:Z

.field private displayDayNamesHeaderRow:Z

.field private displayHeader:Z

.field private displayOnly:Z

.field private dividerColor:I

.field private fullDateFormat:Ljava/text/DateFormat;

.field private headerTextColor:I

.field final highlightedCals:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation
.end field

.field final highlightedCells:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/timessquare/MonthCellDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field private invalidDateListener:Lcom/squareup/timessquare/CalendarPickerView$OnInvalidDateSelectedListener;

.field final listener:Lcom/squareup/timessquare/MonthView$Listener;

.field private locale:Ljava/util/Locale;

.field private maxCal:Ljava/util/Calendar;

.field private minCal:Ljava/util/Calendar;

.field private final monthBuilder:Ljava/lang/StringBuilder;

.field private monthCounter:Ljava/util/Calendar;

.field private monthFormatter:Ljava/util/Formatter;

.field final months:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/timessquare/MonthDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field private monthsReverseOrder:Z

.field final selectedCals:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation
.end field

.field final selectedCells:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/timessquare/MonthCellDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field selectionMode:Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;

.field private timeZone:Ljava/util/TimeZone;

.field private titleTextStyle:I

.field private titleTypeface:Landroid/graphics/Typeface;

.field today:Ljava/util/Calendar;

.field private weekdayNameFormat:Ljava/text/DateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "ar"

    const-string v2, "my"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    .line 68
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/squareup/timessquare/CalendarPickerView;->explicitlyNumericYearLocaleLanguages:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .line 125
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    new-instance v0, Lcom/squareup/timessquare/IndexedLinkedHashMap;

    invoke-direct {v0}, Lcom/squareup/timessquare/IndexedLinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->cells:Lcom/squareup/timessquare/IndexedLinkedHashMap;

    .line 73
    new-instance v0, Lcom/squareup/timessquare/CalendarPickerView$CellClickedListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/timessquare/CalendarPickerView$CellClickedListener;-><init>(Lcom/squareup/timessquare/CalendarPickerView;Lcom/squareup/timessquare/CalendarPickerView$1;)V

    iput-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->listener:Lcom/squareup/timessquare/MonthView$Listener;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->months:Ljava/util/List;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->highlightedCells:Ljava/util/List;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->highlightedCals:Ljava/util/List;

    .line 102
    new-instance v0, Lcom/squareup/timessquare/CalendarPickerView$DefaultOnInvalidDateSelectedListener;

    invoke-direct {v0, p0, v1}, Lcom/squareup/timessquare/CalendarPickerView$DefaultOnInvalidDateSelectedListener;-><init>(Lcom/squareup/timessquare/CalendarPickerView;Lcom/squareup/timessquare/CalendarPickerView$1;)V

    iput-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->invalidDateListener:Lcom/squareup/timessquare/CalendarPickerView$OnInvalidDateSelectedListener;

    .line 106
    new-instance v0, Lcom/squareup/timessquare/DefaultDayViewAdapter;

    invoke-direct {v0}, Lcom/squareup/timessquare/DefaultDayViewAdapter;-><init>()V

    iput-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->dayViewAdapter:Lcom/squareup/timessquare/DayViewAdapter;

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x32

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthBuilder:Ljava/lang/StringBuilder;

    .line 127
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 128
    sget-object v2, Lcom/squareup/timessquare/R$styleable;->CalendarPickerView:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 129
    sget v2, Lcom/squareup/timessquare/R$styleable;->CalendarPickerView_android_background:I

    sget v3, Lcom/squareup/timessquare/R$color;->calendar_bg:I

    .line 130
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 129
    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 131
    sget v3, Lcom/squareup/timessquare/R$styleable;->CalendarPickerView_tsquare_dividerColor:I

    sget v4, Lcom/squareup/timessquare/R$color;->calendar_divider:I

    .line 132
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 131
    invoke-virtual {p2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    iput v3, p0, Lcom/squareup/timessquare/CalendarPickerView;->dividerColor:I

    .line 133
    sget v3, Lcom/squareup/timessquare/R$styleable;->CalendarPickerView_tsquare_dayBackground:I

    sget v4, Lcom/squareup/timessquare/R$drawable;->calendar_bg_selector:I

    invoke-virtual {p2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, p0, Lcom/squareup/timessquare/CalendarPickerView;->dayBackgroundResId:I

    .line 135
    sget v3, Lcom/squareup/timessquare/R$styleable;->CalendarPickerView_tsquare_dayTextColor:I

    sget v4, Lcom/squareup/timessquare/R$color;->calendar_text_selector:I

    invoke-virtual {p2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, p0, Lcom/squareup/timessquare/CalendarPickerView;->dayTextColorResId:I

    .line 137
    sget v3, Lcom/squareup/timessquare/R$styleable;->CalendarPickerView_tsquare_titleTextStyle:I

    sget v4, Lcom/squareup/timessquare/R$style;->CalendarTitle:I

    invoke-virtual {p2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, p0, Lcom/squareup/timessquare/CalendarPickerView;->titleTextStyle:I

    .line 139
    sget v3, Lcom/squareup/timessquare/R$styleable;->CalendarPickerView_tsquare_displayHeader:I

    const/4 v4, 0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lcom/squareup/timessquare/CalendarPickerView;->displayHeader:Z

    .line 140
    sget v3, Lcom/squareup/timessquare/R$styleable;->CalendarPickerView_tsquare_headerTextColor:I

    sget v5, Lcom/squareup/timessquare/R$color;->calendar_text_active:I

    .line 141
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 140
    invoke-virtual {p2, v3, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->headerTextColor:I

    .line 142
    sget v0, Lcom/squareup/timessquare/R$styleable;->CalendarPickerView_tsquare_displayDayNamesHeaderRow:I

    .line 143
    invoke-virtual {p2, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->displayDayNamesHeaderRow:Z

    .line 144
    sget v0, Lcom/squareup/timessquare/R$styleable;->CalendarPickerView_tsquare_displayAlwaysDigitNumbers:I

    const/4 v3, 0x0

    .line 145
    invoke-virtual {p2, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->displayAlwaysDigitNumbers:Z

    .line 146
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 148
    new-instance p2, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;

    invoke-direct {p2, p0, v1}, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;-><init>(Lcom/squareup/timessquare/CalendarPickerView;Lcom/squareup/timessquare/CalendarPickerView$1;)V

    iput-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->adapter:Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;

    .line 149
    invoke-virtual {p0, v1}, Lcom/squareup/timessquare/CalendarPickerView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 150
    invoke-virtual {p0, v3}, Lcom/squareup/timessquare/CalendarPickerView;->setDividerHeight(I)V

    .line 151
    invoke-virtual {p0, v2}, Lcom/squareup/timessquare/CalendarPickerView;->setBackgroundColor(I)V

    .line 152
    invoke-virtual {p0, v2}, Lcom/squareup/timessquare/CalendarPickerView;->setCacheColorHint(I)V

    .line 153
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    .line 154
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    .line 155
    iget-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    invoke-static {p2, v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->today:Ljava/util/Calendar;

    .line 156
    iget-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    invoke-static {p2, v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->minCal:Ljava/util/Calendar;

    .line 157
    iget-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    invoke-static {p2, v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->maxCal:Ljava/util/Calendar;

    .line 158
    iget-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    invoke-static {p2, v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthCounter:Ljava/util/Calendar;

    .line 159
    new-instance p2, Ljava/text/SimpleDateFormat;

    sget v0, Lcom/squareup/timessquare/R$string;->day_name_format:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    invoke-direct {p2, p1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->weekdayNameFormat:Ljava/text/DateFormat;

    .line 160
    iget-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->weekdayNameFormat:Ljava/text/DateFormat;

    iget-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {p1, p2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 161
    iget-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    const/4 p2, 0x2

    invoke-static {p2, p1}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->fullDateFormat:Ljava/text/DateFormat;

    .line 162
    iget-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->fullDateFormat:Ljava/text/DateFormat;

    iget-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {p1, p2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 164
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarPickerView;->isInEditMode()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 165
    iget-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    iget-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    invoke-static {p1, p2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object p1

    .line 166
    invoke-virtual {p1, v4, v4}, Ljava/util/Calendar;->add(II)V

    .line 168
    new-instance p2, Ljava/util/Date;

    invoke-direct {p2}, Ljava/util/Date;-><init>()V

    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p0, p2, p1}, Lcom/squareup/timessquare/CalendarPickerView;->init(Ljava/util/Date;Ljava/util/Date;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;

    move-result-object p1

    new-instance p2, Ljava/util/Date;

    invoke-direct {p2}, Ljava/util/Date;-><init>()V

    .line 169
    invoke-virtual {p1, p2}, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;->withSelectedDate(Ljava/util/Date;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;

    :cond_0
    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/timessquare/CalendarPickerView;)Ljava/util/Calendar;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->minCal:Ljava/util/Calendar;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/timessquare/CalendarPickerView;)Ljava/util/Calendar;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->maxCal:Ljava/util/Calendar;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/timessquare/CalendarPickerView;Ljava/util/Date;)Z
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/timessquare/CalendarPickerView;->isDateSelectable(Ljava/util/Date;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$1300(Lcom/squareup/timessquare/CalendarPickerView;)Lcom/squareup/timessquare/CalendarPickerView$OnInvalidDateSelectedListener;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->invalidDateListener:Lcom/squareup/timessquare/CalendarPickerView$OnInvalidDateSelectedListener;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/timessquare/CalendarPickerView;Ljava/util/Date;Lcom/squareup/timessquare/MonthCellDescriptor;)Z
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Lcom/squareup/timessquare/CalendarPickerView;->doSelectDate(Ljava/util/Date;Lcom/squareup/timessquare/MonthCellDescriptor;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$1500(Lcom/squareup/timessquare/CalendarPickerView;)Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->dateListener:Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/squareup/timessquare/CalendarPickerView;)Lcom/squareup/timessquare/DayViewAdapter;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->dayViewAdapter:Lcom/squareup/timessquare/DayViewAdapter;

    return-object p0
.end method

.method static synthetic access$1700(Lcom/squareup/timessquare/CalendarPickerView;)I
    .locals 0

    .line 47
    iget p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->dividerColor:I

    return p0
.end method

.method static synthetic access$1800(Lcom/squareup/timessquare/CalendarPickerView;)I
    .locals 0

    .line 47
    iget p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->dayBackgroundResId:I

    return p0
.end method

.method static synthetic access$1900(Lcom/squareup/timessquare/CalendarPickerView;)I
    .locals 0

    .line 47
    iget p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->dayTextColorResId:I

    return p0
.end method

.method static synthetic access$2000(Lcom/squareup/timessquare/CalendarPickerView;)I
    .locals 0

    .line 47
    iget p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->titleTextStyle:I

    return p0
.end method

.method static synthetic access$2100(Lcom/squareup/timessquare/CalendarPickerView;)Z
    .locals 0

    .line 47
    iget-boolean p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->displayHeader:Z

    return p0
.end method

.method static synthetic access$2200(Lcom/squareup/timessquare/CalendarPickerView;)I
    .locals 0

    .line 47
    iget p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->headerTextColor:I

    return p0
.end method

.method static synthetic access$2300(Lcom/squareup/timessquare/CalendarPickerView;)Z
    .locals 0

    .line 47
    iget-boolean p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->displayDayNamesHeaderRow:Z

    return p0
.end method

.method static synthetic access$2400(Lcom/squareup/timessquare/CalendarPickerView;)Z
    .locals 0

    .line 47
    iget-boolean p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->displayAlwaysDigitNumbers:Z

    return p0
.end method

.method static synthetic access$2500(Lcom/squareup/timessquare/CalendarPickerView;)Ljava/util/List;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->decorators:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$2600(Lcom/squareup/timessquare/CalendarPickerView;)Lcom/squareup/timessquare/IndexedLinkedHashMap;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->cells:Lcom/squareup/timessquare/IndexedLinkedHashMap;

    return-object p0
.end method

.method static synthetic access$2700(Lcom/squareup/timessquare/CalendarPickerView;)Landroid/graphics/Typeface;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->titleTypeface:Landroid/graphics/Typeface;

    return-object p0
.end method

.method static synthetic access$2800(Lcom/squareup/timessquare/CalendarPickerView;)Landroid/graphics/Typeface;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->dateTypeface:Landroid/graphics/Typeface;

    return-object p0
.end method

.method static synthetic access$2900(Lcom/squareup/timessquare/CalendarPickerView;)Ljava/text/DateFormat;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->fullDateFormat:Ljava/text/DateFormat;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/timessquare/CalendarPickerView;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/squareup/timessquare/CalendarPickerView;->validateAndUpdate()V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/timessquare/CalendarPickerView;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/squareup/timessquare/CalendarPickerView;->scrollToSelectedDates()V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/timessquare/CalendarPickerView;)Ljava/util/Locale;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/timessquare/CalendarPickerView;)Ljava/text/DateFormat;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->weekdayNameFormat:Ljava/text/DateFormat;

    return-object p0
.end method

.method static synthetic access$602(Lcom/squareup/timessquare/CalendarPickerView;Ljava/text/DateFormat;)Ljava/text/DateFormat;
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->weekdayNameFormat:Ljava/text/DateFormat;

    return-object p1
.end method

.method static synthetic access$700(Lcom/squareup/timessquare/CalendarPickerView;)Z
    .locals 0

    .line 47
    iget-boolean p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->displayOnly:Z

    return p0
.end method

.method static synthetic access$702(Lcom/squareup/timessquare/CalendarPickerView;Z)Z
    .locals 0

    .line 47
    iput-boolean p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->displayOnly:Z

    return p1
.end method

.method static synthetic access$800(Lcom/squareup/timessquare/CalendarPickerView;)Z
    .locals 0

    .line 47
    iget-boolean p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthsReverseOrder:Z

    return p0
.end method

.method static synthetic access$802(Lcom/squareup/timessquare/CalendarPickerView;Z)Z
    .locals 0

    .line 47
    iput-boolean p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthsReverseOrder:Z

    return p1
.end method

.method static synthetic access$900(Lcom/squareup/timessquare/CalendarPickerView;)Lcom/squareup/timessquare/CalendarPickerView$CellClickInterceptor;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/timessquare/CalendarPickerView;->cellClickInterceptor:Lcom/squareup/timessquare/CalendarPickerView$CellClickInterceptor;

    return-object p0
.end method

.method private applyMultiSelect(Ljava/util/Date;Ljava/util/Calendar;)Ljava/util/Date;
    .locals 3

    .line 793
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/timessquare/MonthCellDescriptor;

    .line 794
    invoke-virtual {v1}, Lcom/squareup/timessquare/MonthCellDescriptor;->getDate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 p1, 0x0

    .line 796
    invoke-virtual {v1, p1}, Lcom/squareup/timessquare/MonthCellDescriptor;->setSelected(Z)V

    .line 797
    iget-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    const/4 p1, 0x0

    .line 802
    :cond_1
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Calendar;

    .line 803
    invoke-static {v1, p2}, Lcom/squareup/timessquare/CalendarPickerView;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 804
    iget-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-interface {p2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_3
    return-object p1
.end method

.method private static betweenDates(Ljava/util/Calendar;Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 0

    .line 1014
    invoke-virtual {p0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p0

    .line 1015
    invoke-static {p0, p1, p2}, Lcom/squareup/timessquare/CalendarPickerView;->betweenDates(Ljava/util/Date;Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result p0

    return p0
.end method

.method static betweenDates(Ljava/util/Date;Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 1

    .line 1019
    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    .line 1020
    invoke-virtual {p0, p1}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1021
    :cond_0
    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private clearOldSelections()V
    .locals 5

    .line 771
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/timessquare/MonthCellDescriptor;

    const/4 v2, 0x0

    .line 773
    invoke-virtual {v1, v2}, Lcom/squareup/timessquare/MonthCellDescriptor;->setSelected(Z)V

    .line 775
    iget-object v2, p0, Lcom/squareup/timessquare/CalendarPickerView;->dateListener:Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;

    if-eqz v2, :cond_0

    .line 776
    invoke-virtual {v1}, Lcom/squareup/timessquare/MonthCellDescriptor;->getDate()Ljava/util/Date;

    move-result-object v2

    .line 778
    iget-object v3, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectionMode:Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;

    sget-object v4, Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;->RANGE:Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;

    if-ne v3, v4, :cond_2

    .line 779
    iget-object v3, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-eqz v1, :cond_1

    .line 780
    iget-object v3, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_0

    .line 781
    :cond_1
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->dateListener:Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;

    invoke-interface {v1, v2}, Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;->onDateUnselected(Ljava/util/Date;)V

    goto :goto_0

    .line 784
    :cond_2
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->dateListener:Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;

    invoke-interface {v1, v2}, Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;->onDateUnselected(Ljava/util/Date;)V

    goto :goto_0

    .line 788
    :cond_3
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 789
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method private static containsDate(Ljava/util/List;Ljava/util/Calendar;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/Calendar;",
            ">;",
            "Ljava/util/Calendar;",
            ")Z"
        }
    .end annotation

    .line 983
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 984
    invoke-static {p1, v0}, Lcom/squareup/timessquare/CalendarPickerView;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method private containsDate(Ljava/util/List;Ljava/util/Date;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/Calendar;",
            ">;",
            "Ljava/util/Date;",
            ")Z"
        }
    .end annotation

    .line 977
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    invoke-static {v0, v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 978
    invoke-virtual {v0, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 979
    invoke-static {p1, v0}, Lcom/squareup/timessquare/CalendarPickerView;->containsDate(Ljava/util/List;Ljava/util/Calendar;)Z

    move-result p1

    return p1
.end method

.method private static dbg(Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .line 556
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "minDate: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, "\nmaxDate: "

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private doSelectDate(Ljava/util/Date;Lcom/squareup/timessquare/MonthCellDescriptor;)Z
    .locals 9

    .line 691
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    invoke-static {v0, v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 692
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 694
    invoke-static {v0}, Lcom/squareup/timessquare/CalendarPickerView;->setMidnight(Ljava/util/Calendar;)V

    .line 697
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/timessquare/MonthCellDescriptor;

    .line 698
    sget-object v3, Lcom/squareup/timessquare/RangeState;->NONE:Lcom/squareup/timessquare/RangeState;

    invoke-virtual {v2, v3}, Lcom/squareup/timessquare/MonthCellDescriptor;->setRangeState(Lcom/squareup/timessquare/RangeState;)V

    goto :goto_0

    .line 701
    :cond_0
    sget-object v1, Lcom/squareup/timessquare/CalendarPickerView$3;->$SwitchMap$com$squareup$timessquare$CalendarPickerView$SelectionMode:[I

    iget-object v2, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectionMode:Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;

    invoke-virtual {v2}, Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v1, v3, :cond_3

    const/4 v4, 0x2

    if-eq v1, v4, :cond_2

    const/4 v4, 0x3

    if-ne v1, v4, :cond_1

    .line 717
    invoke-direct {p0}, Lcom/squareup/timessquare/CalendarPickerView;->clearOldSelections()V

    goto :goto_1

    .line 720
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unknown selectionMode "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectionMode:Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 713
    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/squareup/timessquare/CalendarPickerView;->applyMultiSelect(Ljava/util/Date;Ljava/util/Calendar;)Ljava/util/Date;

    move-result-object p1

    goto :goto_1

    .line 703
    :cond_3
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v3, :cond_4

    .line 705
    invoke-direct {p0}, Lcom/squareup/timessquare/CalendarPickerView;->clearOldSelections()V

    goto :goto_1

    .line 706
    :cond_4
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 708
    invoke-direct {p0}, Lcom/squareup/timessquare/CalendarPickerView;->clearOldSelections()V

    :cond_5
    :goto_1
    if-eqz p1, :cond_b

    .line 725
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/timessquare/MonthCellDescriptor;

    invoke-virtual {v1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 726
    :cond_6
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 727
    invoke-virtual {p2, v3}, Lcom/squareup/timessquare/MonthCellDescriptor;->setSelected(Z)V

    .line 729
    :cond_7
    iget-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 731
    iget-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectionMode:Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;

    sget-object v0, Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;->RANGE:Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;

    if-ne p2, v0, :cond_b

    iget-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    if-le p2, v3, :cond_b

    .line 733
    iget-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/timessquare/MonthCellDescriptor;

    invoke-virtual {p2}, Lcom/squareup/timessquare/MonthCellDescriptor;->getDate()Ljava/util/Date;

    move-result-object p2

    .line 734
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/timessquare/MonthCellDescriptor;

    invoke-virtual {v0}, Lcom/squareup/timessquare/MonthCellDescriptor;->getDate()Ljava/util/Date;

    move-result-object v0

    .line 735
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/timessquare/MonthCellDescriptor;

    sget-object v4, Lcom/squareup/timessquare/RangeState;->FIRST:Lcom/squareup/timessquare/RangeState;

    invoke-virtual {v1, v4}, Lcom/squareup/timessquare/MonthCellDescriptor;->setRangeState(Lcom/squareup/timessquare/RangeState;)V

    .line 736
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/timessquare/MonthCellDescriptor;

    sget-object v4, Lcom/squareup/timessquare/RangeState;->LAST:Lcom/squareup/timessquare/RangeState;

    invoke-virtual {v1, v4}, Lcom/squareup/timessquare/MonthCellDescriptor;->setRangeState(Lcom/squareup/timessquare/RangeState;)V

    .line 738
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->cells:Lcom/squareup/timessquare/IndexedLinkedHashMap;

    iget-object v4, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Calendar;

    invoke-direct {p0, v4}, Lcom/squareup/timessquare/CalendarPickerView;->monthKey(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/squareup/timessquare/IndexedLinkedHashMap;->getIndexOfKey(Ljava/lang/Object;)I

    move-result v1

    .line 739
    iget-object v4, p0, Lcom/squareup/timessquare/CalendarPickerView;->cells:Lcom/squareup/timessquare/IndexedLinkedHashMap;

    iget-object v5, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Calendar;

    invoke-direct {p0, v5}, Lcom/squareup/timessquare/CalendarPickerView;->monthKey(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/timessquare/IndexedLinkedHashMap;->getIndexOfKey(Ljava/lang/Object;)I

    move-result v4

    :goto_2
    if-gt v1, v4, :cond_b

    .line 741
    iget-object v5, p0, Lcom/squareup/timessquare/CalendarPickerView;->cells:Lcom/squareup/timessquare/IndexedLinkedHashMap;

    invoke-virtual {v5, v1}, Lcom/squareup/timessquare/IndexedLinkedHashMap;->getValueAtIndex(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 742
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_8
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 743
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_9
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/timessquare/MonthCellDescriptor;

    .line 744
    invoke-virtual {v7}, Lcom/squareup/timessquare/MonthCellDescriptor;->getDate()Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 745
    invoke-virtual {v7}, Lcom/squareup/timessquare/MonthCellDescriptor;->getDate()Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 746
    invoke-virtual {v7}, Lcom/squareup/timessquare/MonthCellDescriptor;->isSelectable()Z

    move-result v8

    if-eqz v8, :cond_9

    .line 747
    invoke-virtual {v7, v3}, Lcom/squareup/timessquare/MonthCellDescriptor;->setSelected(Z)V

    .line 748
    sget-object v8, Lcom/squareup/timessquare/RangeState;->MIDDLE:Lcom/squareup/timessquare/RangeState;

    invoke-virtual {v7, v8}, Lcom/squareup/timessquare/MonthCellDescriptor;->setRangeState(Lcom/squareup/timessquare/RangeState;)V

    .line 749
    iget-object v8, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 758
    :cond_b
    invoke-direct {p0}, Lcom/squareup/timessquare/CalendarPickerView;->validateAndUpdate()V

    if-eqz p1, :cond_c

    const/4 v2, 0x1

    :cond_c
    return v2
.end method

.method private formatMonthDate(Ljava/util/Date;)Ljava/lang/String;
    .locals 9

    .line 642
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 651
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    invoke-static {v1}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 654
    iget-boolean v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->displayAlwaysDigitNumbers:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/timessquare/CalendarPickerView;->explicitlyNumericYearLocaleLanguages:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    .line 655
    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 656
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 657
    new-instance v2, Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarPickerView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/squareup/timessquare/R$string;->month_only_name_format:I

    .line 658
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 659
    new-instance v3, Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarPickerView;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/squareup/timessquare/R$string;->year_only_format:I

    .line 660
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-direct {v3, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 661
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 662
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 665
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarPickerView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthFormatter:Ljava/util/Formatter;

    .line 666
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    iget-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {p1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v8

    const/16 v7, 0x34

    .line 665
    invoke-static/range {v1 .. v8}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object p1

    .line 666
    invoke-virtual {p1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object p1

    .line 670
    :goto_0
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthBuilder:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 673
    invoke-static {v0}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    return-object p1
.end method

.method private getMonthCellWithIndexByDate(Ljava/util/Date;)Lcom/squareup/timessquare/CalendarPickerView$MonthCellWithMonthIndex;
    .locals 6

    .line 862
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    invoke-static {v0, v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 863
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 864
    invoke-direct {p0, v0}, Lcom/squareup/timessquare/CalendarPickerView;->monthKey(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object p1

    .line 865
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    iget-object v2, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    invoke-static {v1, v2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v1

    .line 867
    iget-object v2, p0, Lcom/squareup/timessquare/CalendarPickerView;->cells:Lcom/squareup/timessquare/IndexedLinkedHashMap;

    invoke-virtual {v2, p1}, Lcom/squareup/timessquare/IndexedLinkedHashMap;->getIndexOfKey(Ljava/lang/Object;)I

    move-result v2

    .line 868
    iget-object v3, p0, Lcom/squareup/timessquare/CalendarPickerView;->cells:Lcom/squareup/timessquare/IndexedLinkedHashMap;

    invoke-virtual {v3, p1}, Lcom/squareup/timessquare/IndexedLinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 869
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 870
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/timessquare/MonthCellDescriptor;

    .line 871
    invoke-virtual {v4}, Lcom/squareup/timessquare/MonthCellDescriptor;->getDate()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 872
    invoke-static {v1, v0}, Lcom/squareup/timessquare/CalendarPickerView;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Lcom/squareup/timessquare/MonthCellDescriptor;->isSelectable()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 873
    new-instance p1, Lcom/squareup/timessquare/CalendarPickerView$MonthCellWithMonthIndex;

    invoke-direct {p1, v4, v2}, Lcom/squareup/timessquare/CalendarPickerView$MonthCellWithMonthIndex;-><init>(Lcom/squareup/timessquare/MonthCellDescriptor;I)V

    return-object p1

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method private isDateSelectable(Ljava/util/Date;)Z
    .locals 1

    .line 1029
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->dateConfiguredListener:Lcom/squareup/timessquare/CalendarPickerView$DateSelectableFilter;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lcom/squareup/timessquare/CalendarPickerView$DateSelectableFilter;->isDateSelectable(Ljava/util/Date;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private static maxDate(Ljava/util/List;)Ljava/util/Calendar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/Calendar;",
            ">;)",
            "Ljava/util/Calendar;"
        }
    .end annotation

    if-eqz p0, :cond_1

    .line 1000
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 1003
    :cond_0
    invoke-static {p0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1004
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Calendar;

    return-object p0

    :cond_1
    :goto_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static minDate(Ljava/util/List;)Ljava/util/Calendar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/Calendar;",
            ">;)",
            "Ljava/util/Calendar;"
        }
    .end annotation

    if-eqz p0, :cond_1

    .line 992
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 995
    :cond_0
    invoke-static {p0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    const/4 v0, 0x0

    .line 996
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Calendar;

    return-object p0

    :cond_1
    :goto_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private monthKey(Lcom/squareup/timessquare/MonthDescriptor;)Ljava/lang/String;
    .locals 2

    .line 767
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/timessquare/MonthDescriptor;->getYear()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/timessquare/MonthDescriptor;->getMonth()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private monthKey(Ljava/util/Calendar;)Ljava/lang/String;
    .locals 2

    .line 763
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private static sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 3

    const/4 v0, 0x2

    .line 1008
    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v1, v0, :cond_0

    .line 1009
    invoke-virtual {p0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x5

    .line 1010
    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result p0

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result p1

    if-ne p0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method private static sameMonth(Ljava/util/Calendar;Lcom/squareup/timessquare/MonthDescriptor;)Z
    .locals 3

    const/4 v0, 0x2

    .line 1025
    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/timessquare/MonthDescriptor;->getMonth()I

    move-result v1

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, v2}, Ljava/util/Calendar;->get(I)I

    move-result p0

    invoke-virtual {p1}, Lcom/squareup/timessquare/MonthDescriptor;->getYear()I

    move-result p1

    if-ne p0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method private scrollToSelectedDates()V
    .locals 7

    .line 436
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    invoke-static {v0, v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v3, v1

    .line 437
    :goto_0
    iget-object v4, p0, Lcom/squareup/timessquare/CalendarPickerView;->months:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 438
    iget-object v4, p0, Lcom/squareup/timessquare/CalendarPickerView;->months:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/timessquare/MonthDescriptor;

    if-nez v1, :cond_2

    .line 440
    iget-object v5, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Calendar;

    .line 441
    invoke-static {v6, v4}, Lcom/squareup/timessquare/CalendarPickerView;->sameMonth(Ljava/util/Calendar;Lcom/squareup/timessquare/MonthDescriptor;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 442
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    if-nez v3, :cond_2

    .line 446
    invoke-static {v0, v4}, Lcom/squareup/timessquare/CalendarPickerView;->sameMonth(Ljava/util/Calendar;Lcom/squareup/timessquare/MonthDescriptor;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 447
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    if-eqz v1, :cond_4

    .line 452
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/timessquare/CalendarPickerView;->scrollToSelectedMonth(I)V

    goto :goto_1

    :cond_4
    if-eqz v3, :cond_5

    .line 454
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/timessquare/CalendarPickerView;->scrollToSelectedMonth(I)V

    :cond_5
    :goto_1
    return-void
.end method

.method private scrollToSelectedMonth(I)V
    .locals 1

    const/4 v0, 0x0

    .line 416
    invoke-direct {p0, p1, v0}, Lcom/squareup/timessquare/CalendarPickerView;->scrollToSelectedMonth(IZ)V

    return-void
.end method

.method private scrollToSelectedMonth(IZ)V
    .locals 1

    .line 420
    new-instance v0, Lcom/squareup/timessquare/CalendarPickerView$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/timessquare/CalendarPickerView$1;-><init>(Lcom/squareup/timessquare/CalendarPickerView;IZ)V

    invoke-virtual {p0, v0}, Lcom/squareup/timessquare/CalendarPickerView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static setMidnight(Ljava/util/Calendar;)V
    .locals 2

    const/4 v0, 0x0

    const/16 v1, 0xb

    .line 561
    invoke-virtual {p0, v1, v0}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xc

    .line 562
    invoke-virtual {p0, v1, v0}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xd

    .line 563
    invoke-virtual {p0, v1, v0}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xe

    .line 564
    invoke-virtual {p0, v1, v0}, Ljava/util/Calendar;->set(II)V

    return-void
.end method

.method private validateAndUpdate()V
    .locals 1

    .line 409
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarPickerView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->adapter:Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;

    invoke-virtual {p0, v0}, Lcom/squareup/timessquare/CalendarPickerView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 412
    :cond_0
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->adapter:Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;

    invoke-virtual {v0}, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method private validateDate(Ljava/util/Date;)V
    .locals 4

    if-eqz p1, :cond_1

    .line 682
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->minCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->maxCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 683
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/squareup/timessquare/CalendarPickerView;->minCal:Ljava/util/Calendar;

    .line 685
    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/squareup/timessquare/CalendarPickerView;->maxCal:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p1, v1, v2

    const-string p1, "SelectedDate must be between minDate and maxDate.%nminDate: %s%nmaxDate: %s%nselectedDate: %s"

    .line 683
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 680
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Selected date must be non-null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public clearHighlightedDates()V
    .locals 3

    .line 840
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->highlightedCells:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/timessquare/MonthCellDescriptor;

    const/4 v2, 0x0

    .line 841
    invoke-virtual {v1, v2}, Lcom/squareup/timessquare/MonthCellDescriptor;->setHighlighted(Z)V

    goto :goto_0

    .line 843
    :cond_0
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->highlightedCells:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 844
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->highlightedCals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 846
    invoke-direct {p0}, Lcom/squareup/timessquare/CalendarPickerView;->validateAndUpdate()V

    return-void
.end method

.method public clearSelectedDates()V
    .locals 3

    .line 831
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/timessquare/MonthCellDescriptor;

    .line 832
    sget-object v2, Lcom/squareup/timessquare/RangeState;->NONE:Lcom/squareup/timessquare/RangeState;

    invoke-virtual {v1, v2}, Lcom/squareup/timessquare/MonthCellDescriptor;->setRangeState(Lcom/squareup/timessquare/RangeState;)V

    goto :goto_0

    .line 835
    :cond_0
    invoke-direct {p0}, Lcom/squareup/timessquare/CalendarPickerView;->clearOldSelections()V

    .line 836
    invoke-direct {p0}, Lcom/squareup/timessquare/CalendarPickerView;->validateAndUpdate()V

    return-void
.end method

.method public fixDialogDimens()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 484
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarPickerView;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarPickerView;->getMeasuredWidth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "Fixing dimensions to h = %d / w = %d"

    invoke-static {v1, v0}, Lcom/squareup/timessquare/Logr;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 486
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarPickerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarPickerView;->getMeasuredHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 487
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarPickerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarPickerView;->getMeasuredWidth()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 489
    new-instance v0, Lcom/squareup/timessquare/CalendarPickerView$2;

    invoke-direct {v0, p0}, Lcom/squareup/timessquare/CalendarPickerView$2;-><init>(Lcom/squareup/timessquare/CalendarPickerView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/timessquare/CalendarPickerView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public getDecorators()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/timessquare/CalendarCellDecorator;",
            ">;"
        }
    .end annotation

    .line 121
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->decorators:Ljava/util/List;

    return-object v0
.end method

.method getMonthCells(Lcom/squareup/timessquare/MonthDescriptor;Ljava/util/Calendar;)Ljava/util/List;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/timessquare/MonthDescriptor;",
            "Ljava/util/Calendar;",
            ")",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/squareup/timessquare/MonthCellDescriptor;",
            ">;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 927
    iget-object v1, v0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    iget-object v2, v0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    invoke-static {v1, v2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v1

    .line 928
    invoke-virtual/range {p2 .. p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 929
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x5

    const/4 v4, 0x1

    .line 930
    invoke-virtual {v1, v3, v4}, Ljava/util/Calendar;->set(II)V

    const/4 v5, 0x7

    .line 931
    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 932
    invoke-virtual {v1}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v7

    sub-int/2addr v7, v6

    if-lez v7, :cond_0

    add-int/lit8 v7, v7, -0x7

    .line 936
    :cond_0
    invoke-virtual {v1, v3, v7}, Ljava/util/Calendar;->add(II)V

    .line 938
    iget-object v6, v0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-static {v6}, Lcom/squareup/timessquare/CalendarPickerView;->minDate(Ljava/util/List;)Ljava/util/Calendar;

    move-result-object v6

    .line 939
    iget-object v7, v0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-static {v7}, Lcom/squareup/timessquare/CalendarPickerView;->maxDate(Ljava/util/List;)Ljava/util/Calendar;

    move-result-object v7

    :cond_1
    const/4 v8, 0x2

    .line 941
    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/timessquare/MonthDescriptor;->getMonth()I

    move-result v10

    add-int/2addr v10, v4

    if-lt v9, v10, :cond_2

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/timessquare/MonthDescriptor;->getYear()I

    move-result v10

    if-ge v9, v10, :cond_9

    .line 942
    :cond_2
    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/timessquare/MonthDescriptor;->getYear()I

    move-result v10

    if-gt v9, v10, :cond_9

    new-array v9, v4, [Ljava/lang/Object;

    .line 943
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v10

    const/4 v11, 0x0

    aput-object v10, v9, v11

    const-string v10, "Building week row starting at %s"

    invoke-static {v10, v9}, Lcom/squareup/timessquare/Logr;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 944
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 945
    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    :goto_0
    if-ge v10, v5, :cond_1

    .line 947
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v13

    .line 948
    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v12

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/timessquare/MonthDescriptor;->getMonth()I

    move-result v14

    if-ne v12, v14, :cond_3

    const/4 v14, 0x1

    goto :goto_1

    :cond_3
    const/4 v14, 0x0

    :goto_1
    if-eqz v14, :cond_4

    .line 949
    iget-object v12, v0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-static {v12, v1}, Lcom/squareup/timessquare/CalendarPickerView;->containsDate(Ljava/util/List;Ljava/util/Calendar;)Z

    move-result v12

    if-eqz v12, :cond_4

    const/16 v16, 0x1

    goto :goto_2

    :cond_4
    const/16 v16, 0x0

    :goto_2
    if-eqz v14, :cond_5

    .line 950
    iget-object v12, v0, Lcom/squareup/timessquare/CalendarPickerView;->minCal:Ljava/util/Calendar;

    iget-object v15, v0, Lcom/squareup/timessquare/CalendarPickerView;->maxCal:Ljava/util/Calendar;

    .line 951
    invoke-static {v1, v12, v15}, Lcom/squareup/timessquare/CalendarPickerView;->betweenDates(Ljava/util/Calendar;Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-direct {v0, v13}, Lcom/squareup/timessquare/CalendarPickerView;->isDateSelectable(Ljava/util/Date;)Z

    move-result v12

    if-eqz v12, :cond_5

    const/4 v15, 0x1

    goto :goto_3

    :cond_5
    const/4 v15, 0x0

    .line 952
    :goto_3
    iget-object v12, v0, Lcom/squareup/timessquare/CalendarPickerView;->today:Ljava/util/Calendar;

    invoke-static {v1, v12}, Lcom/squareup/timessquare/CalendarPickerView;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v17

    .line 953
    iget-object v12, v0, Lcom/squareup/timessquare/CalendarPickerView;->highlightedCals:Ljava/util/List;

    invoke-static {v12, v1}, Lcom/squareup/timessquare/CalendarPickerView;->containsDate(Ljava/util/List;Ljava/util/Calendar;)Z

    move-result v18

    .line 954
    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v19

    .line 956
    sget-object v12, Lcom/squareup/timessquare/RangeState;->NONE:Lcom/squareup/timessquare/RangeState;

    .line 957
    iget-object v5, v0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v4, :cond_8

    .line 958
    invoke-static {v6, v1}, Lcom/squareup/timessquare/CalendarPickerView;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 959
    sget-object v5, Lcom/squareup/timessquare/RangeState;->FIRST:Lcom/squareup/timessquare/RangeState;

    :goto_4
    move-object/from16 v20, v5

    goto :goto_5

    .line 960
    :cond_6
    iget-object v5, v0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-static {v5}, Lcom/squareup/timessquare/CalendarPickerView;->maxDate(Ljava/util/List;)Ljava/util/Calendar;

    move-result-object v5

    invoke-static {v5, v1}, Lcom/squareup/timessquare/CalendarPickerView;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 961
    sget-object v5, Lcom/squareup/timessquare/RangeState;->LAST:Lcom/squareup/timessquare/RangeState;

    goto :goto_4

    .line 962
    :cond_7
    invoke-static {v1, v6, v7}, Lcom/squareup/timessquare/CalendarPickerView;->betweenDates(Ljava/util/Calendar;Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 963
    sget-object v5, Lcom/squareup/timessquare/RangeState;->MIDDLE:Lcom/squareup/timessquare/RangeState;

    goto :goto_4

    :cond_8
    move-object/from16 v20, v12

    .line 967
    :goto_5
    new-instance v5, Lcom/squareup/timessquare/MonthCellDescriptor;

    move-object v12, v5

    invoke-direct/range {v12 .. v20}, Lcom/squareup/timessquare/MonthCellDescriptor;-><init>(Ljava/util/Date;ZZZZZILcom/squareup/timessquare/RangeState;)V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 970
    invoke-virtual {v1, v3, v4}, Ljava/util/Calendar;->add(II)V

    add-int/lit8 v10, v10, 0x1

    const/4 v5, 0x7

    goto/16 :goto_0

    :cond_9
    return-object v2
.end method

.method public getSelectedDate()Ljava/util/Date;
    .locals 2

    .line 542
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSelectedDates()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation

    .line 546
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 547
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/timessquare/MonthCellDescriptor;

    .line 548
    invoke-virtual {v2}, Lcom/squareup/timessquare/MonthCellDescriptor;->getDate()Ljava/util/Date;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 550
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-object v0
.end method

.method public highlightDates(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/util/Date;",
            ">;)V"
        }
    .end annotation

    .line 812
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    .line 813
    invoke-direct {p0, v0}, Lcom/squareup/timessquare/CalendarPickerView;->validateDate(Ljava/util/Date;)V

    .line 815
    invoke-direct {p0, v0}, Lcom/squareup/timessquare/CalendarPickerView;->getMonthCellWithIndexByDate(Ljava/util/Date;)Lcom/squareup/timessquare/CalendarPickerView$MonthCellWithMonthIndex;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 817
    iget-object v2, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    iget-object v3, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    invoke-static {v2, v3}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v2

    .line 818
    invoke-virtual {v2, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 819
    iget-object v0, v1, Lcom/squareup/timessquare/CalendarPickerView$MonthCellWithMonthIndex;->cell:Lcom/squareup/timessquare/MonthCellDescriptor;

    .line 821
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->highlightedCells:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 822
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->highlightedCals:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    .line 823
    invoke-virtual {v0, v1}, Lcom/squareup/timessquare/MonthCellDescriptor;->setHighlighted(Z)V

    goto :goto_0

    .line 827
    :cond_1
    invoke-direct {p0}, Lcom/squareup/timessquare/CalendarPickerView;->validateAndUpdate()V

    return-void
.end method

.method public init(Ljava/util/Date;Ljava/util/Date;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;
    .locals 2

    .line 286
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/squareup/timessquare/CalendarPickerView;->init(Ljava/util/Date;Ljava/util/Date;Ljava/util/TimeZone;Ljava/util/Locale;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;

    move-result-object p1

    return-object p1
.end method

.method public init(Ljava/util/Date;Ljava/util/Date;Ljava/util/Locale;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;
    .locals 1

    .line 337
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/squareup/timessquare/CalendarPickerView;->init(Ljava/util/Date;Ljava/util/Date;Ljava/util/TimeZone;Ljava/util/Locale;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;

    move-result-object p1

    return-object p1
.end method

.method public init(Ljava/util/Date;Ljava/util/Date;Ljava/util/TimeZone;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;
    .locals 1

    .line 309
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/squareup/timessquare/CalendarPickerView;->init(Ljava/util/Date;Ljava/util/Date;Ljava/util/TimeZone;Ljava/util/Locale;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;

    move-result-object p1

    return-object p1
.end method

.method public init(Ljava/util/Date;Ljava/util/Date;Ljava/util/TimeZone;Ljava/util/Locale;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;
    .locals 6

    if-eqz p1, :cond_6

    if-eqz p2, :cond_6

    .line 195
    invoke-virtual {p1, p2}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_5

    if-eqz p4, :cond_4

    if-eqz p3, :cond_3

    .line 207
    iput-object p3, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    .line 208
    iput-object p4, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    .line 209
    invoke-static {p3, p4}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->today:Ljava/util/Calendar;

    .line 210
    invoke-static {p3, p4}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->minCal:Ljava/util/Calendar;

    .line 211
    invoke-static {p3, p4}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->maxCal:Ljava/util/Calendar;

    .line 212
    invoke-static {p3, p4}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthCounter:Ljava/util/Calendar;

    .line 213
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->months:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/timessquare/MonthDescriptor;

    .line 214
    invoke-virtual {v1}, Lcom/squareup/timessquare/MonthDescriptor;->getDate()Ljava/util/Date;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/timessquare/CalendarPickerView;->formatMonthDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/timessquare/MonthDescriptor;->setLabel(Ljava/lang/String;)V

    goto :goto_0

    .line 216
    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    .line 217
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarPickerView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/timessquare/R$string;->day_name_format:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->weekdayNameFormat:Ljava/text/DateFormat;

    .line 218
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->weekdayNameFormat:Ljava/text/DateFormat;

    invoke-virtual {v0, p3}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    const/4 v0, 0x2

    .line 219
    invoke-static {v0, p4}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->fullDateFormat:Ljava/text/DateFormat;

    .line 220
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->fullDateFormat:Ljava/text/DateFormat;

    invoke-virtual {v1, p3}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 221
    new-instance p3, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthBuilder:Ljava/lang/StringBuilder;

    invoke-direct {p3, v1, p4}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object p3, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthFormatter:Ljava/util/Formatter;

    .line 223
    sget-object p3, Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;->SINGLE:Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;

    iput-object p3, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectionMode:Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;

    .line 225
    iget-object p3, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCals:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->clear()V

    .line 226
    iget-object p3, p0, Lcom/squareup/timessquare/CalendarPickerView;->selectedCells:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->clear()V

    .line 227
    iget-object p3, p0, Lcom/squareup/timessquare/CalendarPickerView;->highlightedCals:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->clear()V

    .line 228
    iget-object p3, p0, Lcom/squareup/timessquare/CalendarPickerView;->highlightedCells:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->clear()V

    .line 231
    iget-object p3, p0, Lcom/squareup/timessquare/CalendarPickerView;->cells:Lcom/squareup/timessquare/IndexedLinkedHashMap;

    invoke-virtual {p3}, Lcom/squareup/timessquare/IndexedLinkedHashMap;->clear()V

    .line 232
    iget-object p3, p0, Lcom/squareup/timessquare/CalendarPickerView;->months:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->clear()V

    .line 233
    iget-object p3, p0, Lcom/squareup/timessquare/CalendarPickerView;->minCal:Ljava/util/Calendar;

    invoke-virtual {p3, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 234
    iget-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->maxCal:Ljava/util/Calendar;

    invoke-virtual {p1, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 235
    iget-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->minCal:Ljava/util/Calendar;

    invoke-static {p1}, Lcom/squareup/timessquare/CalendarPickerView;->setMidnight(Ljava/util/Calendar;)V

    .line 236
    iget-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->maxCal:Ljava/util/Calendar;

    invoke-static {p1}, Lcom/squareup/timessquare/CalendarPickerView;->setMidnight(Ljava/util/Calendar;)V

    const/4 p1, 0x0

    .line 237
    iput-boolean p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->displayOnly:Z

    .line 241
    iget-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->maxCal:Ljava/util/Calendar;

    const/16 p3, 0xc

    const/4 p4, -0x1

    invoke-virtual {p2, p3, p4}, Ljava/util/Calendar;->add(II)V

    .line 244
    iget-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthCounter:Ljava/util/Calendar;

    iget-object p3, p0, Lcom/squareup/timessquare/CalendarPickerView;->minCal:Ljava/util/Calendar;

    invoke-virtual {p3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 245
    iget-object p2, p0, Lcom/squareup/timessquare/CalendarPickerView;->maxCal:Ljava/util/Calendar;

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result p2

    .line 246
    iget-object p3, p0, Lcom/squareup/timessquare/CalendarPickerView;->maxCal:Ljava/util/Calendar;

    const/4 p4, 0x1

    invoke-virtual {p3, p4}, Ljava/util/Calendar;->get(I)I

    move-result p3

    .line 247
    :goto_1
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthCounter:Ljava/util/Calendar;

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-le v1, p2, :cond_1

    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthCounter:Ljava/util/Calendar;

    .line 248
    invoke-virtual {v1, p4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ge v1, p3, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthCounter:Ljava/util/Calendar;

    .line 249
    invoke-virtual {v1, p4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    add-int/lit8 v2, p3, 0x1

    if-ge v1, v2, :cond_2

    .line 250
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthCounter:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    .line 251
    new-instance v2, Lcom/squareup/timessquare/MonthDescriptor;

    iget-object v3, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthCounter:Ljava/util/Calendar;

    .line 252
    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget-object v4, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthCounter:Ljava/util/Calendar;

    invoke-virtual {v4, p4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 253
    invoke-direct {p0, v1}, Lcom/squareup/timessquare/CalendarPickerView;->formatMonthDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v1, v5}, Lcom/squareup/timessquare/MonthDescriptor;-><init>(IILjava/util/Date;Ljava/lang/String;)V

    .line 254
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->cells:Lcom/squareup/timessquare/IndexedLinkedHashMap;

    invoke-direct {p0, v2}, Lcom/squareup/timessquare/CalendarPickerView;->monthKey(Lcom/squareup/timessquare/MonthDescriptor;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthCounter:Ljava/util/Calendar;

    invoke-virtual {p0, v2, v4}, Lcom/squareup/timessquare/CalendarPickerView;->getMonthCells(Lcom/squareup/timessquare/MonthDescriptor;Ljava/util/Calendar;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/squareup/timessquare/IndexedLinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-array v1, p4, [Ljava/lang/Object;

    aput-object v2, v1, p1

    const-string v3, "Adding month %s"

    .line 255
    invoke-static {v3, v1}, Lcom/squareup/timessquare/Logr;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 256
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->months:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 257
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->monthCounter:Ljava/util/Calendar;

    invoke-virtual {v1, v0, p4}, Ljava/util/Calendar;->add(II)V

    goto :goto_1

    .line 260
    :cond_2
    invoke-direct {p0}, Lcom/squareup/timessquare/CalendarPickerView;->validateAndUpdate()V

    .line 261
    new-instance p1, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;

    invoke-direct {p1, p0}, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;-><init>(Lcom/squareup/timessquare/CalendarPickerView;)V

    return-object p1

    .line 203
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Time zone is null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 200
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Locale is null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 196
    :cond_5
    new-instance p3, Ljava/lang/IllegalArgumentException;

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "minDate must be before maxDate.  "

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    invoke-static {p1, p2}, Lcom/squareup/timessquare/CalendarPickerView;->dbg(Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p3

    .line 192
    :cond_6
    new-instance p3, Ljava/lang/IllegalArgumentException;

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "minDate and maxDate must be non-null.  "

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    invoke-static {p1, p2}, Lcom/squareup/timessquare/CalendarPickerView;->dbg(Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p3
.end method

.method protected onMeasure(II)V
    .locals 1

    .line 534
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->months:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 538
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onMeasure(II)V

    return-void

    .line 535
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Must have at least one month to display.  Did you forget to call init()?"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public scrollToDate(Ljava/util/Date;)Z
    .locals 3

    .line 461
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView;->timeZone:Ljava/util/TimeZone;

    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView;->locale:Ljava/util/Locale;

    invoke-static {v0, v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 462
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/4 p1, 0x0

    const/4 v1, 0x0

    .line 463
    :goto_0
    iget-object v2, p0, Lcom/squareup/timessquare/CalendarPickerView;->months:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 464
    iget-object v2, p0, Lcom/squareup/timessquare/CalendarPickerView;->months:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/timessquare/MonthDescriptor;

    .line 465
    invoke-static {v0, v2}, Lcom/squareup/timessquare/CalendarPickerView;->sameMonth(Ljava/util/Calendar;Lcom/squareup/timessquare/MonthDescriptor;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 466
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_2

    .line 471
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/timessquare/CalendarPickerView;->scrollToSelectedMonth(I)V

    const/4 p1, 0x1

    :cond_2
    return p1
.end method

.method public selectDate(Ljava/util/Date;)Z
    .locals 1

    const/4 v0, 0x0

    .line 604
    invoke-virtual {p0, p1, v0}, Lcom/squareup/timessquare/CalendarPickerView;->selectDate(Ljava/util/Date;Z)Z

    move-result p1

    return p1
.end method

.method public selectDate(Ljava/util/Date;Z)Z
    .locals 2

    .line 619
    invoke-direct {p0, p1}, Lcom/squareup/timessquare/CalendarPickerView;->validateDate(Ljava/util/Date;)V

    .line 621
    invoke-direct {p0, p1}, Lcom/squareup/timessquare/CalendarPickerView;->getMonthCellWithIndexByDate(Ljava/util/Date;)Lcom/squareup/timessquare/CalendarPickerView$MonthCellWithMonthIndex;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 622
    invoke-direct {p0, p1}, Lcom/squareup/timessquare/CalendarPickerView;->isDateSelectable(Ljava/util/Date;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 625
    :cond_0
    iget-object v1, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthCellWithMonthIndex;->cell:Lcom/squareup/timessquare/MonthCellDescriptor;

    invoke-direct {p0, p1, v1}, Lcom/squareup/timessquare/CalendarPickerView;->doSelectDate(Ljava/util/Date;Lcom/squareup/timessquare/MonthCellDescriptor;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 627
    iget v0, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthCellWithMonthIndex;->monthIndex:I

    invoke-direct {p0, v0, p2}, Lcom/squareup/timessquare/CalendarPickerView;->scrollToSelectedMonth(IZ)V

    :cond_1
    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public setCellClickInterceptor(Lcom/squareup/timessquare/CalendarPickerView$CellClickInterceptor;)V
    .locals 0

    .line 1071
    iput-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->cellClickInterceptor:Lcom/squareup/timessquare/CalendarPickerView$CellClickInterceptor;

    return-void
.end method

.method public setCustomDayView(Lcom/squareup/timessquare/DayViewAdapter;)V
    .locals 0

    .line 1063
    iput-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->dayViewAdapter:Lcom/squareup/timessquare/DayViewAdapter;

    .line 1064
    iget-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->adapter:Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;

    if-eqz p1, :cond_0

    .line 1065
    invoke-virtual {p1}, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public setDateSelectableFilter(Lcom/squareup/timessquare/CalendarPickerView$DateSelectableFilter;)V
    .locals 0

    .line 1053
    iput-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->dateConfiguredListener:Lcom/squareup/timessquare/CalendarPickerView$DateSelectableFilter;

    return-void
.end method

.method public setDateTypeface(Landroid/graphics/Typeface;)V
    .locals 0

    .line 509
    iput-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->dateTypeface:Landroid/graphics/Typeface;

    .line 510
    invoke-direct {p0}, Lcom/squareup/timessquare/CalendarPickerView;->validateAndUpdate()V

    return-void
.end method

.method public setDecorators(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/timessquare/CalendarCellDecorator;",
            ">;)V"
        }
    .end annotation

    .line 114
    iput-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->decorators:Ljava/util/List;

    .line 115
    iget-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->adapter:Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;

    if-eqz p1, :cond_0

    .line 116
    invoke-virtual {p1}, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public setOnDateSelectedListener(Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;)V
    .locals 0

    .line 1033
    iput-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->dateListener:Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;

    return-void
.end method

.method public setOnInvalidDateSelectedListener(Lcom/squareup/timessquare/CalendarPickerView$OnInvalidDateSelectedListener;)V
    .locals 0

    .line 1042
    iput-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->invalidDateListener:Lcom/squareup/timessquare/CalendarPickerView$OnInvalidDateSelectedListener;

    return-void
.end method

.method public setTitleTypeface(Landroid/graphics/Typeface;)V
    .locals 0

    .line 501
    iput-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView;->titleTypeface:Landroid/graphics/Typeface;

    .line 502
    invoke-direct {p0}, Lcom/squareup/timessquare/CalendarPickerView;->validateAndUpdate()V

    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .locals 0

    .line 517
    invoke-virtual {p0, p1}, Lcom/squareup/timessquare/CalendarPickerView;->setTitleTypeface(Landroid/graphics/Typeface;)V

    .line 518
    invoke-virtual {p0, p1}, Lcom/squareup/timessquare/CalendarPickerView;->setDateTypeface(Landroid/graphics/Typeface;)V

    return-void
.end method

.method public unfixDialogDimens()V
    .locals 2

    const-string v0, "Reset the fixed dimensions to allow for re-measurement"

    .line 526
    invoke-static {v0}, Lcom/squareup/timessquare/Logr;->d(Ljava/lang/String;)V

    .line 528
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarPickerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 529
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarPickerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 530
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarPickerView;->requestLayout()V

    return-void
.end method
