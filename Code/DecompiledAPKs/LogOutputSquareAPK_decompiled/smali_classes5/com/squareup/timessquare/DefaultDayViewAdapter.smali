.class public Lcom/squareup/timessquare/DefaultDayViewAdapter;
.super Ljava/lang/Object;
.source "DefaultDayViewAdapter.java"

# interfaces
.implements Lcom/squareup/timessquare/DayViewAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public makeCellView(Lcom/squareup/timessquare/CalendarCellView;)V
    .locals 5

    .line 13
    new-instance v0, Landroid/widget/TextView;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    .line 14
    invoke-virtual {p1}, Lcom/squareup/timessquare/CalendarCellView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/timessquare/R$style;->CalendarCell_CalendarDate:I

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    .line 15
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setDuplicateParentStateEnabled(Z)V

    .line 17
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    const/16 v4, 0x10

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/timessquare/CalendarCellView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 19
    invoke-virtual {p1, v0}, Lcom/squareup/timessquare/CalendarCellView;->setDayOfMonthTextView(Landroid/widget/TextView;)V

    return-void
.end method
