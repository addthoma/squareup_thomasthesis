.class final Lcom/squareup/time/RealCurrentTime$RealClock$withZone$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealCurrentTime.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/time/RealCurrentTime$RealClock;->withZone(Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/Clock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lorg/threeten/bp/ZoneId;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Lorg/threeten/bp/ZoneId;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $zone:Lorg/threeten/bp/ZoneId;


# direct methods
.method constructor <init>(Lorg/threeten/bp/ZoneId;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/time/RealCurrentTime$RealClock$withZone$1;->$zone:Lorg/threeten/bp/ZoneId;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/time/RealCurrentTime$RealClock$withZone$1;->invoke()Lorg/threeten/bp/ZoneId;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Lorg/threeten/bp/ZoneId;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/time/RealCurrentTime$RealClock$withZone$1;->$zone:Lorg/threeten/bp/ZoneId;

    return-object v0
.end method
