.class public interface abstract Lcom/squareup/time/CurrentTime;
.super Ljava/lang/Object;
.source "CurrentTime.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/time/CurrentTime$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H\u0016J\u0008\u0010\u0006\u001a\u00020\u0007H\u0016J\u0008\u0010\u0008\u001a\u00020\tH\u0016J\u0008\u0010\n\u001a\u00020\u000bH\u0016J\u0008\u0010\u000c\u001a\u00020\rH\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/time/CurrentTime;",
        "",
        "clock",
        "Lorg/threeten/bp/Clock;",
        "instant",
        "Lorg/threeten/bp/Instant;",
        "localDate",
        "Lorg/threeten/bp/LocalDate;",
        "localDateTime",
        "Lorg/threeten/bp/LocalDateTime;",
        "localTime",
        "Lorg/threeten/bp/LocalTime;",
        "zonedDateTime",
        "Lorg/threeten/bp/ZonedDateTime;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract clock()Lorg/threeten/bp/Clock;
.end method

.method public abstract instant()Lorg/threeten/bp/Instant;
.end method

.method public abstract localDate()Lorg/threeten/bp/LocalDate;
.end method

.method public abstract localDateTime()Lorg/threeten/bp/LocalDateTime;
.end method

.method public abstract localTime()Lorg/threeten/bp/LocalTime;
.end method

.method public abstract zonedDateTime()Lorg/threeten/bp/ZonedDateTime;
.end method
