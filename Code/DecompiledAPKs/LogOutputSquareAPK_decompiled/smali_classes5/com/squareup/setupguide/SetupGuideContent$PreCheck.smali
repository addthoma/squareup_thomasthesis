.class public final Lcom/squareup/setupguide/SetupGuideContent$PreCheck;
.super Ljava/lang/Object;
.source "SetupGuideConfiguration.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/setupguide/SetupGuideContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PreCheck"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000b\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupGuideContent$PreCheck;",
        "",
        "dependsOn",
        "Lcom/squareup/setupguide/SetupGuideContent;",
        "preCheckFailTitleId",
        "",
        "preCheckFailActionId",
        "(Lcom/squareup/setupguide/SetupGuideContent;II)V",
        "getDependsOn",
        "()Lcom/squareup/setupguide/SetupGuideContent;",
        "getPreCheckFailActionId",
        "()I",
        "getPreCheckFailTitleId",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dependsOn:Lcom/squareup/setupguide/SetupGuideContent;

.field private final preCheckFailActionId:I

.field private final preCheckFailTitleId:I


# direct methods
.method public constructor <init>(Lcom/squareup/setupguide/SetupGuideContent;II)V
    .locals 1

    const-string v0, "dependsOn"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->dependsOn:Lcom/squareup/setupguide/SetupGuideContent;

    iput p2, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->preCheckFailTitleId:I

    iput p3, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->preCheckFailActionId:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/setupguide/SetupGuideContent$PreCheck;Lcom/squareup/setupguide/SetupGuideContent;IIILjava/lang/Object;)Lcom/squareup/setupguide/SetupGuideContent$PreCheck;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->dependsOn:Lcom/squareup/setupguide/SetupGuideContent;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->preCheckFailTitleId:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->preCheckFailActionId:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->copy(Lcom/squareup/setupguide/SetupGuideContent;II)Lcom/squareup/setupguide/SetupGuideContent$PreCheck;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/setupguide/SetupGuideContent;
    .locals 1

    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->dependsOn:Lcom/squareup/setupguide/SetupGuideContent;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->preCheckFailTitleId:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->preCheckFailActionId:I

    return v0
.end method

.method public final copy(Lcom/squareup/setupguide/SetupGuideContent;II)Lcom/squareup/setupguide/SetupGuideContent$PreCheck;
    .locals 1

    const-string v0, "dependsOn"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;-><init>(Lcom/squareup/setupguide/SetupGuideContent;II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;

    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->dependsOn:Lcom/squareup/setupguide/SetupGuideContent;

    iget-object v1, p1, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->dependsOn:Lcom/squareup/setupguide/SetupGuideContent;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->preCheckFailTitleId:I

    iget v1, p1, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->preCheckFailTitleId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->preCheckFailActionId:I

    iget p1, p1, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->preCheckFailActionId:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDependsOn()Lcom/squareup/setupguide/SetupGuideContent;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->dependsOn:Lcom/squareup/setupguide/SetupGuideContent;

    return-object v0
.end method

.method public final getPreCheckFailActionId()I
    .locals 1

    .line 40
    iget v0, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->preCheckFailActionId:I

    return v0
.end method

.method public final getPreCheckFailTitleId()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->preCheckFailTitleId:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->dependsOn:Lcom/squareup/setupguide/SetupGuideContent;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->preCheckFailTitleId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->preCheckFailActionId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PreCheck(dependsOn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->dependsOn:Lcom/squareup/setupguide/SetupGuideContent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", preCheckFailTitleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->preCheckFailTitleId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", preCheckFailActionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;->preCheckFailActionId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
