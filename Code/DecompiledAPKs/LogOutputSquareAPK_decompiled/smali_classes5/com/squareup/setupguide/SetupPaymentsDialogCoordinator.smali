.class public final Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SetupPaymentsDialogCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSetupPaymentsDialogCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SetupPaymentsDialogCoordinator.kt\ncom/squareup/setupguide/SetupPaymentsDialogCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,81:1\n1103#2,7:82\n1103#2,7:89\n*E\n*S KotlinDebug\n*F\n+ 1 SetupPaymentsDialogCoordinator.kt\ncom/squareup/setupguide/SetupPaymentsDialogCoordinator\n*L\n67#1,7:82\n71#1,7:89\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0014H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;",
        "(Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;)V",
        "iconImageView",
        "Landroid/widget/ImageView;",
        "messageTextView",
        "Lcom/squareup/widgets/MessageView;",
        "primaryButtonView",
        "Lcom/squareup/noho/NohoButton;",
        "secondaryButtonView",
        "titleTextView",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "update",
        "data",
        "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private iconImageView:Landroid/widget/ImageView;

.field private messageTextView:Lcom/squareup/widgets/MessageView;

.field private primaryButtonView:Lcom/squareup/noho/NohoButton;

.field private final runner:Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;

.field private secondaryButtonView:Lcom/squareup/noho/NohoButton;

.field private titleTextView:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->runner:Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;

    return-void
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;)Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->runner:Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;

    return-object p0
.end method

.method public static final synthetic access$update(Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->update(Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 38
    sget v0, Lcom/squareup/setupguide/R$id;->setup_guide_dialog_icon:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->iconImageView:Landroid/widget/ImageView;

    .line 39
    sget v0, Lcom/squareup/setupguide/R$id;->setup_guide_dialog_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->titleTextView:Lcom/squareup/widgets/MessageView;

    .line 40
    sget v0, Lcom/squareup/setupguide/R$id;->setup_guide_dialog_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->messageTextView:Lcom/squareup/widgets/MessageView;

    .line 41
    sget v0, Lcom/squareup/setupguide/R$id;->setup_guide_dialog_primary:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->primaryButtonView:Lcom/squareup/noho/NohoButton;

    .line 42
    sget v0, Lcom/squareup/setupguide/R$id;->setup_guide_dialog_secondary:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->secondaryButtonView:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method private final update(Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;)V
    .locals 4

    .line 47
    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->getIconId()Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 48
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->iconImageView:Landroid/widget/ImageView;

    const-string v2, "iconImageView"

    if-nez v0, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 49
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->iconImageView:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->getIconId()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 53
    :cond_2
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->titleTextView:Lcom/squareup/widgets/MessageView;

    const-string/jumbo v2, "titleTextView"

    if-nez v0, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->getTitleId()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 54
    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->getMessageId()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_9

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 55
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->messageTextView:Lcom/squareup/widgets/MessageView;

    const-string v3, "messageTextView"

    if-nez v0, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 56
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->messageTextView:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_5

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->getMessageId()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 59
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->titleTextView:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v0}, Lcom/squareup/widgets/MessageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_8

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 60
    iget-object v3, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->titleTextView:Lcom/squareup/widgets/MessageView;

    if-nez v3, :cond_7

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v3}, Lcom/squareup/widgets/MessageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 61
    sget v3, Lcom/squareup/noho/R$dimen;->noho_title_over_text_padding:I

    .line 60
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_0

    .line 59
    :cond_8
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 66
    :cond_9
    :goto_0
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->primaryButtonView:Lcom/squareup/noho/NohoButton;

    const-string v2, "primaryButtonView"

    if-nez v0, :cond_a

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->getPrimaryButtonId()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoButton;->setText(I)V

    .line 67
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->primaryButtonView:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_b

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    check-cast v0, Landroid/view/View;

    .line 82
    new-instance v2, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->getSecondaryButtonId()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_f

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 72
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->secondaryButtonView:Lcom/squareup/noho/NohoButton;

    const-string v2, "secondaryButtonView"

    if-nez v0, :cond_c

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    check-cast v0, Landroid/view/View;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 73
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->secondaryButtonView:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_d

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->getSecondaryButtonId()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setText(I)V

    .line 74
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->secondaryButtonView:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_e

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    check-cast v0, Landroid/view/View;

    .line 89
    new-instance v1, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator$update$$inlined$let$lambda$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator$update$$inlined$let$lambda$1;-><init>(Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_f
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->bindViews(Landroid/view/View;)V

    .line 32
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;->runner:Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;

    invoke-interface {v0}, Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;->setupPaymentsDialogScreenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 33
    new-instance v1, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator$attach$1;-><init>(Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "runner.setupPaymentsDial\u2026 { data -> update(data) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
