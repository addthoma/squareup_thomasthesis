.class public final Lcom/squareup/setupguide/SetupGuideCompletionDialog$Factory;
.super Ljava/lang/Object;
.source "SetupGuideCompletionDialog.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/setupguide/SetupGuideCompletionDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0010\u0010\u0008\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0002\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupGuideCompletionDialog$Factory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final createDialog(Landroid/content/Context;)Landroid/app/Dialog;
    .locals 4

    .line 60
    new-instance v0, Lcom/squareup/dialog/GlassDialog;

    sget v1, Lcom/squareup/noho/R$style;->Theme_Noho_Dialog_NoBackground:I

    invoke-direct {v0, p1, v1}, Lcom/squareup/dialog/GlassDialog;-><init>(Landroid/content/Context;I)V

    .line 61
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object p1

    check-cast p1, Lcom/squareup/setupguide/SetupGuideCompletionDialog;

    .line 63
    sget v1, Lcom/squareup/setupguide/R$layout;->setup_guide_dialog_layout:I

    invoke-virtual {v0, v1}, Lcom/squareup/dialog/GlassDialog;->setContentView(I)V

    .line 64
    invoke-virtual {v0}, Lcom/squareup/dialog/GlassDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v2, "dialog.window!!"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/4 v3, -0x2

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 65
    invoke-virtual {v0}, Lcom/squareup/dialog/GlassDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 66
    invoke-virtual {v0}, Lcom/squareup/dialog/GlassDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    check-cast p1, Lcom/squareup/coordinators/CoordinatorProvider;

    invoke-static {v1, p1}, Lcom/squareup/coordinators/Coordinators;->bind(Landroid/view/View;Lcom/squareup/coordinators/CoordinatorProvider;)V

    .line 68
    check-cast v0, Landroid/app/Dialog;

    return-object v0
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/setupguide/SetupGuideCompletionDialog$Factory;->createDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "just(createDialog(context))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
