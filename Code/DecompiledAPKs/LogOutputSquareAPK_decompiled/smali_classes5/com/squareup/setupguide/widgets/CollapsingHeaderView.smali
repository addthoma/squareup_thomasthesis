.class public final Lcom/squareup/setupguide/widgets/CollapsingHeaderView;
.super Lcom/google/android/material/appbar/AppBarLayout;
.source "CollapsingHeaderView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCollapsingHeaderView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CollapsingHeaderView.kt\ncom/squareup/setupguide/widgets/CollapsingHeaderView\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,219:1\n1642#2,2:220\n1103#3,7:222\n*E\n*S KotlinDebug\n*F\n+ 1 CollapsingHeaderView.kt\ncom/squareup/setupguide/widgets/CollapsingHeaderView\n*L\n81#1,2:220\n149#1,7:222\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u0001LB\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u0014\u00103\u001a\u0002042\u000c\u00105\u001a\u0008\u0012\u0004\u0012\u00020406J0\u00107\u001a\u0002042\u0006\u00108\u001a\u0002092\u0006\u0010:\u001a\u00020;2\u0006\u0010<\u001a\u00020;2\u0006\u0010=\u001a\u00020;2\u0006\u0010>\u001a\u00020;H\u0014J\u0014\u0010?\u001a\u0002042\u000c\u00105\u001a\u0008\u0012\u0004\u0012\u00020406J\u0010\u0010@\u001a\u0002042\u0008\u0008\u0001\u0010A\u001a\u00020;J!\u0010B\u001a\u0002042\u0008\u0008\u0001\u0010C\u001a\u00020;2\n\u0008\u0003\u0010D\u001a\u0004\u0018\u00010;\u00a2\u0006\u0002\u0010EJ\u0014\u0010F\u001a\u000204*\u00020G2\u0006\u0010H\u001a\u00020;H\u0002J\u001c\u0010I\u001a\u000204*\u00020G2\u0006\u0010J\u001a\u00020;2\u0006\u0010K\u001a\u00020;H\u0002R(\u0010\t\u001a\u0004\u0018\u00010\u00082\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00088F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0007\u001a\u00020\u00108F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0018\u001a\u00060\u0019R\u00020\u0000X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010 \u001a\u0004\u0018\u00010\u00102\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00108F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008!\u0010\u0013\"\u0004\u0008\"\u0010\u0015R$\u0010#\u001a\u00020\u00102\u0006\u0010\u0007\u001a\u00020\u00108F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008$\u0010\u0013\"\u0004\u0008%\u0010\u0015R\u0014\u0010&\u001a\u0008\u0012\u0004\u0012\u00020(0\'X\u0082.\u00a2\u0006\u0002\n\u0000R0\u0010*\u001a\u0008\u0012\u0004\u0012\u00020(0\'2\u000c\u0010)\u001a\u0008\u0012\u0004\u0012\u00020(0\'8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008+\u0010,\"\u0004\u0008-\u0010.R\u000e\u0010/\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u000201X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00102\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006M"
    }
    d2 = {
        "Lcom/squareup/setupguide/widgets/CollapsingHeaderView;",
        "Lcom/google/android/material/appbar/AppBarLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "value",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "actionBarGlyph",
        "getActionBarGlyph",
        "()Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "setActionBarGlyph",
        "(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V",
        "actionBarGlyphView",
        "Lcom/squareup/glyph/SquareGlyphView;",
        "",
        "actionBarText",
        "getActionBarText",
        "()Ljava/lang/CharSequence;",
        "setActionBarText",
        "(Ljava/lang/CharSequence;)V",
        "actionBarTitle",
        "Lcom/squareup/marketfont/MarketTextView;",
        "animationOffsetListener",
        "Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;",
        "bottomTextView",
        "Lcom/squareup/noho/NohoLabel;",
        "collapsableToolbar",
        "Lcom/google/android/material/appbar/CollapsingToolbarLayout;",
        "headerContainer",
        "Landroid/widget/LinearLayout;",
        "headerTitle",
        "getHeaderTitle",
        "setHeaderTitle",
        "headerValue",
        "getHeaderValue",
        "setHeaderValue",
        "overflowMenuConfig",
        "",
        "",
        "menu",
        "overflowMenuConfiguration",
        "getOverflowMenuConfiguration",
        "()Ljava/util/Set;",
        "setOverflowMenuConfiguration",
        "(Ljava/util/Set;)V",
        "titleAndGlyph",
        "toolBar",
        "Landroidx/appcompat/widget/Toolbar;",
        "topTextView",
        "onClose",
        "",
        "command",
        "Lkotlin/Function0;",
        "onLayout",
        "changed",
        "",
        "l",
        "",
        "t",
        "r",
        "b",
        "onOverflowMenuItemClicked",
        "setHeaderColor",
        "headerColorRes",
        "setTextColors",
        "textColorRes",
        "actionBarTextColorRes",
        "(ILjava/lang/Integer;)V",
        "setHeight",
        "Landroid/view/View;",
        "height",
        "setVerticalMargin",
        "topMargin",
        "bottomMargin",
        "AnimationOffsetListener",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBarGlyphView:Lcom/squareup/glyph/SquareGlyphView;

.field private final actionBarTitle:Lcom/squareup/marketfont/MarketTextView;

.field private animationOffsetListener:Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;

.field private final bottomTextView:Lcom/squareup/noho/NohoLabel;

.field private final collapsableToolbar:Lcom/google/android/material/appbar/CollapsingToolbarLayout;

.field private final headerContainer:Landroid/widget/LinearLayout;

.field private overflowMenuConfig:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final titleAndGlyph:Landroid/widget/LinearLayout;

.field private final toolBar:Landroidx/appcompat/widget/Toolbar;

.field private final topTextView:Lcom/squareup/noho/NohoLabel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/google/android/material/appbar/AppBarLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    new-instance v0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;-><init>(Lcom/squareup/setupguide/widgets/CollapsingHeaderView;I)V

    iput-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->animationOffsetListener:Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;

    .line 91
    sget v0, Lcom/squareup/setupguide/R$layout;->collapsing_header_view:I

    move-object v1, p0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {p1, v0, v1}, Lcom/google/android/material/appbar/AppBarLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 93
    sget v0, Lcom/squareup/setupguide/R$id;->toolbar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    iput-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->toolBar:Landroidx/appcompat/widget/Toolbar;

    .line 94
    sget v0, Lcom/squareup/setupguide/R$id;->header_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->headerContainer:Landroid/widget/LinearLayout;

    .line 95
    sget v0, Lcom/squareup/setupguide/R$id;->action_bar_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->actionBarTitle:Lcom/squareup/marketfont/MarketTextView;

    .line 96
    sget v0, Lcom/squareup/setupguide/R$id;->title_and_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->titleAndGlyph:Landroid/widget/LinearLayout;

    .line 97
    sget v0, Lcom/squareup/setupguide/R$id;->collapse_toolbar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/appbar/CollapsingToolbarLayout;

    iput-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->collapsableToolbar:Lcom/google/android/material/appbar/CollapsingToolbarLayout;

    .line 98
    sget v0, Lcom/squareup/setupguide/R$id;->top_text_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    iput-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->topTextView:Lcom/squareup/noho/NohoLabel;

    .line 99
    sget v0, Lcom/squareup/setupguide/R$id;->bottom_text_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    iput-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->bottomTextView:Lcom/squareup/noho/NohoLabel;

    .line 100
    sget v0, Lcom/squareup/setupguide/R$id;->up_button_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->actionBarGlyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 103
    invoke-virtual {p0}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$dimen;->marin_action_bar_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 105
    invoke-virtual {p0}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/setupguide/R$dimen;->overlay_on_collapsing_header:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 109
    sget-object v3, Lcom/squareup/setupguide/R$styleable;->CollapsingHeaderView:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 112
    sget p2, Lcom/squareup/setupguide/R$styleable;->CollapsingHeaderView_toolBarHeight:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    .line 113
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->toolBar:Landroidx/appcompat/widget/Toolbar;

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0, p2}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->setHeight(Landroid/view/View;I)V

    .line 116
    sget v0, Lcom/squareup/setupguide/R$styleable;->CollapsingHeaderView_headerTopMargin:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 118
    sget v3, Lcom/squareup/setupguide/R$styleable;->CollapsingHeaderView_headerBottomMargin:I

    .line 117
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 121
    sget v3, Lcom/squareup/setupguide/R$styleable;->CollapsingHeaderView_overlay:I

    invoke-virtual {p1, v3, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 122
    iget-object v3, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->headerContainer:Landroid/widget/LinearLayout;

    check-cast v3, Landroid/view/View;

    add-int/2addr p2, v0

    add-int/2addr v2, v1

    invoke-direct {p0, v3, p2, v2}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->setVerticalMargin(Landroid/view/View;II)V

    .line 127
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 130
    iget-object p1, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->animationOffsetListener:Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;

    check-cast p1, Lcom/google/android/material/appbar/AppBarLayout$OnOffsetChangedListener;

    invoke-virtual {p0, p1}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->addOnOffsetChangedListener(Lcom/google/android/material/appbar/AppBarLayout$OnOffsetChangedListener;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 32
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static final synthetic access$getActionBarTitle$p(Lcom/squareup/setupguide/widgets/CollapsingHeaderView;)Lcom/squareup/marketfont/MarketTextView;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->actionBarTitle:Lcom/squareup/marketfont/MarketTextView;

    return-object p0
.end method

.method public static final synthetic access$getHeaderContainer$p(Lcom/squareup/setupguide/widgets/CollapsingHeaderView;)Landroid/widget/LinearLayout;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->headerContainer:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method private final setHeight(Landroid/view/View;I)V
    .locals 0

    .line 186
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    .line 187
    iput p2, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    return-void
.end method

.method public static synthetic setTextColors$default(Lcom/squareup/setupguide/widgets/CollapsingHeaderView;ILjava/lang/Integer;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 138
    check-cast p2, Ljava/lang/Integer;

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->setTextColors(ILjava/lang/Integer;)V

    return-void
.end method

.method private final setVerticalMargin(Landroid/view/View;II)V
    .locals 0

    .line 179
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 180
    iput p2, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 181
    iput p3, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    return-void

    .line 179
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final getActionBarGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->actionBarGlyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphView;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    return-object v0
.end method

.method public final getActionBarText()Ljava/lang/CharSequence;
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->actionBarTitle:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "actionBarTitle.text"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getHeaderTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->topTextView:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoLabel;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final getHeaderValue()Ljava/lang/CharSequence;
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->bottomTextView:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoLabel;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "bottomTextView.text"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getOverflowMenuConfiguration()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->overflowMenuConfig:Ljava/util/Set;

    if-nez v0, :cond_0

    const-string v1, "overflowMenuConfig"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final onClose(Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "command"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->actionBarGlyphView:Lcom/squareup/glyph/SquareGlyphView;

    check-cast v0, Landroid/view/View;

    .line 222
    new-instance v1, Lcom/squareup/setupguide/widgets/CollapsingHeaderView$onClose$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView$onClose$$inlined$onClickDebounced$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .line 168
    invoke-super/range {p0 .. p5}, Lcom/google/android/material/appbar/AppBarLayout;->onLayout(ZIIII)V

    .line 171
    iget-object p1, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->headerContainer:Landroid/widget/LinearLayout;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/setupguide/widgets/CollapsingHeaderViewKt;->access$getTopCoordinate$p(Landroid/view/View;)I

    move-result p1

    iget-object p2, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->titleAndGlyph:Landroid/widget/LinearLayout;

    check-cast p2, Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/setupguide/widgets/CollapsingHeaderViewKt;->access$getBottomCoordinate$p(Landroid/view/View;)I

    move-result p2

    sub-int/2addr p1, p2

    .line 172
    iget-object p2, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->animationOffsetListener:Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;

    invoke-virtual {p2, p1}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;->setTotalScrollRange(I)V

    return-void
.end method

.method public final onOverflowMenuItemClicked(Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "command"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->toolBar:Landroidx/appcompat/widget/Toolbar;

    new-instance v1, Lcom/squareup/setupguide/widgets/CollapsingHeaderView$onOverflowMenuItemClicked$1;

    invoke-direct {v1, p1}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView$onOverflowMenuItemClicked$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Landroidx/appcompat/widget/Toolbar$OnMenuItemClickListener;

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setOnMenuItemClickListener(Landroidx/appcompat/widget/Toolbar$OnMenuItemClickListener;)V

    return-void
.end method

.method public final setActionBarGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->actionBarGlyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 71
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->actionBarGlyphView:Lcom/squareup/glyph/SquareGlyphView;

    check-cast v0, Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public final setActionBarText(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->actionBarTitle:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setHeaderColor(I)V
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->collapsableToolbar:Lcom/google/android/material/appbar/CollapsingToolbarLayout;

    invoke-virtual {p0}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/appbar/CollapsingToolbarLayout;->setBackgroundColor(I)V

    .line 135
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->toolBar:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/Toolbar;->setBackgroundColor(I)V

    return-void
.end method

.method public final setHeaderTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->topTextView:Lcom/squareup/noho/NohoLabel;

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setHeaderValue(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->bottomTextView:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setOverflowMenuConfiguration(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "menu"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 78
    iput-object p1, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->overflowMenuConfig:Ljava/util/Set;

    .line 79
    iget-object p1, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->toolBar:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/setupguide/R$drawable;->setup_guide_overflow_icon:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setOverflowIcon(Landroid/graphics/drawable/Drawable;)V

    .line 80
    iget-object p1, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->toolBar:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p1}, Landroidx/appcompat/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 81
    iget-object p1, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->overflowMenuConfig:Ljava/util/Set;

    if-nez p1, :cond_0

    const-string v0, "overflowMenuConfig"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Ljava/lang/Iterable;

    .line 220
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 83
    iget-object v1, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->toolBar:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {v1}, Landroidx/appcompat/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0

    .line 86
    :cond_1
    iget-object p1, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->toolBar:Landroidx/appcompat/widget/Toolbar;

    const/4 v0, 0x0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setOverflowIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    return-void
.end method

.method public final setTextColors(ILjava/lang/Integer;)V
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->topTextView:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p0}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setTextColor(I)V

    .line 140
    iget-object v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->bottomTextView:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p0}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setTextColor(I)V

    .line 142
    invoke-virtual {p0}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    :cond_0
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    .line 143
    iget-object p2, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->actionBarTitle:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 144
    iget-object p2, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->actionBarGlyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    .line 145
    iget-object p2, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->toolBar:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p2}, Landroidx/appcompat/widget/Toolbar;->getOverflowIcon()Landroid/graphics/drawable/Drawable;

    move-result-object p2

    if-eqz p2, :cond_1

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    :cond_1
    return-void
.end method
