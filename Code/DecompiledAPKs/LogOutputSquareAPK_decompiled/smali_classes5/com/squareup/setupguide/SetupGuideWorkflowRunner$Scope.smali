.class public final Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "SetupGuideWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/setupguide/SetupGuideWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Scope"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSetupGuideWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SetupGuideWorkflowRunner.kt\ncom/squareup/setupguide/SetupGuideWorkflowRunner$Scope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,56:1\n35#2:57\n24#3,4:58\n*E\n*S KotlinDebug\n*F\n+ 1 SetupGuideWorkflowRunner.kt\ncom/squareup/setupguide/SetupGuideWorkflowRunner$Scope\n*L\n26#1:57\n35#1,4:58\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016R\u001c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00048\u0006X\u0087\u0004\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0005\u0010\u0002\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope;",
        "Lcom/squareup/ui/main/InMainActivityScope;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "CREATOR$annotations",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope;

    invoke-direct {v0}, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope;-><init>()V

    sput-object v0, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope;->INSTANCE:Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope;

    .line 58
    new-instance v0, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 61
    sput-object v0, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method

.method public static synthetic CREATOR$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-super {p0, p1}, Lcom/squareup/ui/main/InMainActivityScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 57
    const-class v1, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 26
    check-cast p1, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$ParentComponent;

    .line 28
    invoke-interface {p1}, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$ParentComponent;->setupGuideWorkflowRunner()Lcom/squareup/setupguide/SetupGuideWorkflowRunner;

    move-result-object p1

    const-string v1, "builder"

    .line 29
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/setupguide/SetupGuideWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    return-object v0
.end method
