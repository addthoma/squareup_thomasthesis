.class public final Lcom/squareup/setupguide/SetupGuideCompletionDialog;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "SetupGuideCompletionDialog.kt"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/setupguide/SetupGuideCompletionDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/setupguide/SetupGuideCompletionDialog$ParentComponent;,
        Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;,
        Lcom/squareup/setupguide/SetupGuideCompletionDialog$Runner;,
        Lcom/squareup/setupguide/SetupGuideCompletionDialog$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSetupGuideCompletionDialog.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SetupGuideCompletionDialog.kt\ncom/squareup/setupguide/SetupGuideCompletionDialog\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,72:1\n43#2:73\n*E\n*S KotlinDebug\n*F\n+ 1 SetupGuideCompletionDialog.kt\ncom/squareup/setupguide/SetupGuideCompletionDialog\n*L\n50#1:73\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003:\u0004\u000f\u0010\u0011\u0012B\u0015\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010\n\u001a\u00020\u0001H\u0016J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u000e\u0010\u0006\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupGuideCompletionDialog;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "Lcom/squareup/container/MaybePersistent;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "data",
        "Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;",
        "parentTreeKey",
        "(Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;Lcom/squareup/ui/main/RegisterTreeKey;)V",
        "getData",
        "()Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;",
        "getParentKey",
        "provideCoordinator",
        "Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;",
        "view",
        "Landroid/view/View;",
        "Factory",
        "ParentComponent",
        "Runner",
        "ScreenData",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final data:Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;

.field private final parentTreeKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method public constructor <init>(Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parentTreeKey"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog;->data:Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;

    iput-object p2, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog;->parentTreeKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method public final getData()Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog;->data:Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog;->parentTreeKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideCompletionDialog;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    .line 27
    invoke-static {p0}, Lcom/squareup/container/MaybePersistent$DefaultImpls;->isPersistent(Lcom/squareup/container/MaybePersistent;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 0

    .line 27
    invoke-virtual {p0, p1}, Lcom/squareup/setupguide/SetupGuideCompletionDialog;->provideCoordinator(Landroid/view/View;)Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    const-class v0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ParentComponent;

    .line 50
    invoke-interface {p1}, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ParentComponent;->setupGuideCompletionDialogCoordinator()Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;

    move-result-object p1

    return-object p1
.end method
