.class public final Lcom/squareup/setupguide/SetupGuideContent$IDVContent;
.super Lcom/squareup/setupguide/SetupGuideContent;
.source "SetupGuideConfiguration.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/setupguide/SetupGuideContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "IDVContent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0017\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001BM\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0003\u0012\n\u0008\u0001\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0003H\u00c6\u0003J\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0010J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u000bH\u00c6\u0003JX\u0010 \u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u00032\n\u0008\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000bH\u00c6\u0001\u00a2\u0006\u0002\u0010!J\u0013\u0010\"\u001a\u00020#2\u0008\u0010$\u001a\u0004\u0018\u00010%H\u00d6\u0003J\t\u0010&\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\'\u001a\u00020(H\u00d6\u0001R\u0014\u0010\n\u001a\u00020\u000bX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0018\u0010\u0007\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0011\u001a\u0004\u0008\u000f\u0010\u0010R\u0014\u0010\u0006\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0014\u0010\u0005\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0013R\u0016\u0010\u0008\u001a\u0004\u0018\u00010\tX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0013R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0013\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupGuideContent$IDVContent;",
        "Lcom/squareup/setupguide/SetupGuideContent;",
        "titleId",
        "",
        "subtitleId",
        "iconId",
        "completionTitleId",
        "completionMessageId",
        "preCheck",
        "Lcom/squareup/setupguide/SetupGuideContent$PreCheck;",
        "actionItemName",
        "Lcom/squareup/protos/checklist/common/ActionItemName;",
        "(IIIILjava/lang/Integer;Lcom/squareup/setupguide/SetupGuideContent$PreCheck;Lcom/squareup/protos/checklist/common/ActionItemName;)V",
        "getActionItemName",
        "()Lcom/squareup/protos/checklist/common/ActionItemName;",
        "getCompletionMessageId",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "getCompletionTitleId",
        "()I",
        "getIconId",
        "getPreCheck",
        "()Lcom/squareup/setupguide/SetupGuideContent$PreCheck;",
        "getSubtitleId",
        "getTitleId",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "(IIIILjava/lang/Integer;Lcom/squareup/setupguide/SetupGuideContent$PreCheck;Lcom/squareup/protos/checklist/common/ActionItemName;)Lcom/squareup/setupguide/SetupGuideContent$IDVContent;",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionItemName:Lcom/squareup/protos/checklist/common/ActionItemName;

.field private final completionMessageId:Ljava/lang/Integer;

.field private final completionTitleId:I

.field private final iconId:I

.field private final preCheck:Lcom/squareup/setupguide/SetupGuideContent$PreCheck;

.field private final subtitleId:I

.field private final titleId:I


# direct methods
.method public constructor <init>(IIIILjava/lang/Integer;Lcom/squareup/setupguide/SetupGuideContent$PreCheck;Lcom/squareup/protos/checklist/common/ActionItemName;)V
    .locals 1

    const-string v0, "actionItemName"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 69
    invoke-direct {p0, v0}, Lcom/squareup/setupguide/SetupGuideContent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->titleId:I

    iput p2, p0, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->subtitleId:I

    iput p3, p0, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->iconId:I

    iput p4, p0, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->completionTitleId:I

    iput-object p5, p0, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->completionMessageId:Ljava/lang/Integer;

    iput-object p6, p0, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->preCheck:Lcom/squareup/setupguide/SetupGuideContent$PreCheck;

    iput-object p7, p0, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->actionItemName:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-void
.end method

.method public synthetic constructor <init>(IIIILjava/lang/Integer;Lcom/squareup/setupguide/SetupGuideContent$PreCheck;Lcom/squareup/protos/checklist/common/ActionItemName;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 67
    check-cast v0, Lcom/squareup/setupguide/SetupGuideContent$PreCheck;

    move-object v7, v0

    goto :goto_0

    :cond_0
    move-object v7, p6

    :goto_0
    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;-><init>(IIIILjava/lang/Integer;Lcom/squareup/setupguide/SetupGuideContent$PreCheck;Lcom/squareup/protos/checklist/common/ActionItemName;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/setupguide/SetupGuideContent$IDVContent;IIIILjava/lang/Integer;Lcom/squareup/setupguide/SetupGuideContent$PreCheck;Lcom/squareup/protos/checklist/common/ActionItemName;ILjava/lang/Object;)Lcom/squareup/setupguide/SetupGuideContent$IDVContent;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getTitleId()I

    move-result p1

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getSubtitleId()I

    move-result p2

    :cond_1
    move p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getIconId()I

    move-result p3

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getCompletionTitleId()I

    move-result p4

    :cond_3
    move v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getCompletionMessageId()Ljava/lang/Integer;

    move-result-object p5

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getPreCheck()Lcom/squareup/setupguide/SetupGuideContent$PreCheck;

    move-result-object p6

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getActionItemName()Lcom/squareup/protos/checklist/common/ActionItemName;

    move-result-object p7

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move p3, p1

    move p4, p9

    move p5, v0

    move p6, v1

    move-object p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->copy(IIIILjava/lang/Integer;Lcom/squareup/setupguide/SetupGuideContent$PreCheck;Lcom/squareup/protos/checklist/common/ActionItemName;)Lcom/squareup/setupguide/SetupGuideContent$IDVContent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getTitleId()I

    move-result v0

    return v0
.end method

.method public final component2()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getSubtitleId()I

    move-result v0

    return v0
.end method

.method public final component3()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getIconId()I

    move-result v0

    return v0
.end method

.method public final component4()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getCompletionTitleId()I

    move-result v0

    return v0
.end method

.method public final component5()Ljava/lang/Integer;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getCompletionMessageId()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final component6()Lcom/squareup/setupguide/SetupGuideContent$PreCheck;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getPreCheck()Lcom/squareup/setupguide/SetupGuideContent$PreCheck;

    move-result-object v0

    return-object v0
.end method

.method public final component7()Lcom/squareup/protos/checklist/common/ActionItemName;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getActionItemName()Lcom/squareup/protos/checklist/common/ActionItemName;

    move-result-object v0

    return-object v0
.end method

.method public final copy(IIIILjava/lang/Integer;Lcom/squareup/setupguide/SetupGuideContent$PreCheck;Lcom/squareup/protos/checklist/common/ActionItemName;)Lcom/squareup/setupguide/SetupGuideContent$IDVContent;
    .locals 9

    const-string v0, "actionItemName"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;-><init>(IIIILjava/lang/Integer;Lcom/squareup/setupguide/SetupGuideContent$PreCheck;Lcom/squareup/protos/checklist/common/ActionItemName;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getTitleId()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getTitleId()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getSubtitleId()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getSubtitleId()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getIconId()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getIconId()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getCompletionTitleId()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getCompletionTitleId()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getCompletionMessageId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getCompletionMessageId()Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getPreCheck()Lcom/squareup/setupguide/SetupGuideContent$PreCheck;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getPreCheck()Lcom/squareup/setupguide/SetupGuideContent$PreCheck;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getActionItemName()Lcom/squareup/protos/checklist/common/ActionItemName;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getActionItemName()Lcom/squareup/protos/checklist/common/ActionItemName;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getActionItemName()Lcom/squareup/protos/checklist/common/ActionItemName;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->actionItemName:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object v0
.end method

.method public getCompletionMessageId()Ljava/lang/Integer;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->completionMessageId:Ljava/lang/Integer;

    return-object v0
.end method

.method public getCompletionTitleId()I
    .locals 1

    .line 65
    iget v0, p0, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->completionTitleId:I

    return v0
.end method

.method public getIconId()I
    .locals 1

    .line 64
    iget v0, p0, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->iconId:I

    return v0
.end method

.method public getPreCheck()Lcom/squareup/setupguide/SetupGuideContent$PreCheck;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->preCheck:Lcom/squareup/setupguide/SetupGuideContent$PreCheck;

    return-object v0
.end method

.method public getSubtitleId()I
    .locals 1

    .line 63
    iget v0, p0, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->subtitleId:I

    return v0
.end method

.method public getTitleId()I
    .locals 1

    .line 62
    iget v0, p0, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->titleId:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getTitleId()I

    move-result v0

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getSubtitleId()I

    move-result v1

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getIconId()I

    move-result v1

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getCompletionTitleId()I

    move-result v1

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getCompletionMessageId()Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getPreCheck()Lcom/squareup/setupguide/SetupGuideContent$PreCheck;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getActionItemName()Lcom/squareup/protos/checklist/common/ActionItemName;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IDVContent(titleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getTitleId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", subtitleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getSubtitleId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", iconId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getIconId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", completionTitleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getCompletionTitleId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", completionMessageId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getCompletionMessageId()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", preCheck="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getPreCheck()Lcom/squareup/setupguide/SetupGuideContent$PreCheck;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", actionItemName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideContent$IDVContent;->getActionItemName()Lcom/squareup/protos/checklist/common/ActionItemName;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
