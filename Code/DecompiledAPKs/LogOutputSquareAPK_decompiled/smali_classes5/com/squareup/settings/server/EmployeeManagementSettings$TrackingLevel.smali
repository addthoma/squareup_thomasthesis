.class final enum Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;
.super Ljava/lang/Enum;
.source "EmployeeManagementSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settings/server/EmployeeManagementSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "TrackingLevel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

.field public static final enum COMPLETE_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

.field public static final enum GUEST_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;


# instance fields
.field public final serverName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 63
    new-instance v0, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    const/4 v1, 0x0

    const-string v2, "GUEST_MODE"

    const-string v3, "employee_management_tracking_level_restrict_actions"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;->GUEST_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    .line 64
    new-instance v0, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    const/4 v2, 0x1

    const-string v3, "COMPLETE_MODE"

    const-string v4, "employee_management_tracking_level_complete"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;->COMPLETE_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    .line 62
    sget-object v3, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;->GUEST_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;->COMPLETE_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;->$VALUES:[Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 69
    iput-object p3, p0, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;->serverName:Ljava/lang/String;

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;
    .locals 5

    .line 73
    invoke-static {}, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;->values()[Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 74
    iget-object v4, v3, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;->serverName:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;
    .locals 1

    .line 62
    const-class v0, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    return-object p0
.end method

.method public static values()[Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;
    .locals 1

    .line 62
    sget-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;->$VALUES:[Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    invoke-virtual {v0}, [Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    return-object v0
.end method
