.class public final Lcom/squareup/settings/server/RealUserSettingsProvider;
.super Ljava/lang/Object;
.source "RealUserSettingsProvider.kt"

# interfaces
.implements Lcom/squareup/settings/server/UserSettingsProvider;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealUserSettingsProvider.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealUserSettingsProvider.kt\ncom/squareup/settings/server/RealUserSettingsProvider\n*L\n1#1,33:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016J\n\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J\n\u0010\u000f\u001a\u0004\u0018\u00010\u000cH\u0016J\u0008\u0010\u0010\u001a\u00020\u0011H\u0016J\u0008\u0010\u0012\u001a\u00020\u000cH\u0016J\u0008\u0010\u0013\u001a\u00020\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u000cH\u0016J\u0008\u0010\u0016\u001a\u00020\u000cH\u0016J\u0008\u0010\u0017\u001a\u00020\u0018H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0007\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/settings/server/RealUserSettingsProvider;",
        "Lcom/squareup/settings/server/UserSettingsProvider;",
        "loggedInStatusProvider",
        "Lcom/squareup/account/LoggedInStatusProvider;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/account/LoggedInStatusProvider;Lcom/squareup/settings/server/AccountStatusSettings;)V",
        "userSettings",
        "Lcom/squareup/settings/server/UserSettings;",
        "getUserSettings",
        "()Lcom/squareup/settings/server/UserSettings;",
        "getBusinessName",
        "",
        "getCountryCode",
        "Lcom/squareup/CountryCode;",
        "getCuratedImageUrl",
        "getCurrencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "getEmail",
        "getMcc",
        "",
        "getName",
        "getToken",
        "hasUserSettings",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final loggedInStatusProvider:Lcom/squareup/account/LoggedInStatusProvider;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/account/LoggedInStatusProvider;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loggedInStatusProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/settings/server/RealUserSettingsProvider;->loggedInStatusProvider:Lcom/squareup/account/LoggedInStatusProvider;

    iput-object p2, p0, Lcom/squareup/settings/server/RealUserSettingsProvider;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method


# virtual methods
.method public getBusinessName()Ljava/lang/String;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/settings/server/RealUserSettingsProvider;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getBusinessName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getCountryCode()Lcom/squareup/CountryCode;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/settings/server/RealUserSettingsProvider;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCodeOrNull()Lcom/squareup/CountryCode;

    move-result-object v0

    return-object v0
.end method

.method public getCuratedImageUrl()Ljava/lang/String;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/settings/server/RealUserSettingsProvider;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCuratedImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 2

    .line 19
    invoke-virtual {p0}, Lcom/squareup/settings/server/RealUserSettingsProvider;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    const-string/jumbo v1, "userSettings.currency"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/settings/server/RealUserSettingsProvider;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getEmail()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getMcc()I
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/settings/server/RealUserSettingsProvider;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getMcc()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/settings/server/RealUserSettingsProvider;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/settings/server/RealUserSettingsProvider;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public final getUserSettings()Lcom/squareup/settings/server/UserSettings;
    .locals 2

    .line 13
    iget-object v0, p0, Lcom/squareup/settings/server/RealUserSettingsProvider;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    const-string v1, "settings.userSettings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public hasUserSettings()Z
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/settings/server/RealUserSettingsProvider;->loggedInStatusProvider:Lcom/squareup/account/LoggedInStatusProvider;

    invoke-interface {v0}, Lcom/squareup/account/LoggedInStatusProvider;->lastLoggedInStatus()Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;->isLoggedIn()Z

    move-result v0

    return v0
.end method
