.class public Lcom/squareup/settings/server/InstantDepositsSettings;
.super Ljava/lang/Object;
.source "InstantDepositsSettings.java"


# instance fields
.field private final accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

.field private final allowInstantDeposit:Z

.field private final features:Lcom/squareup/settings/server/Features;

.field private final statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method protected constructor <init>(Ljava/lang/Boolean;Lcom/squareup/settings/server/Features;Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p3, p0, Lcom/squareup/settings/server/InstantDepositsSettings;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    .line 33
    iput-object p2, p0, Lcom/squareup/settings/server/InstantDepositsSettings;->features:Lcom/squareup/settings/server/Features;

    .line 34
    iput-object p4, p0, Lcom/squareup/settings/server/InstantDepositsSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    if-eqz p1, :cond_1

    .line 35
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    iput-boolean p1, p0, Lcom/squareup/settings/server/InstantDepositsSettings;->allowInstantDeposit:Z

    return-void
.end method

.method private feeAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 2

    .line 117
    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    sget-object p1, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private feePercentage(Ljava/lang/Integer;)Lcom/squareup/util/Percentage;
    .locals 1

    if-eqz p1, :cond_0

    .line 112
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1}, Lcom/squareup/util/Percentage;->fromBasisPoints(I)Lcom/squareup/util/Percentage;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 113
    invoke-static {p1}, Lcom/squareup/util/Percentage;->fromBasisPoints(I)Lcom/squareup/util/Percentage;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private getInstantDeposits()Lcom/squareup/server/account/protos/InstantDeposits;
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/squareup/settings/server/InstantDepositsSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;-><init>()V

    .line 64
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    move-result-object v0

    .line 65
    new-instance v1, Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    invoke-direct {v1}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;-><init>()V

    .line 66
    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->instant_deposit_fee(Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    move-result-object v1

    .line 67
    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->same_day_deposit_fee(Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits;

    move-result-object v0

    return-object v0

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/squareup/settings/server/InstantDepositsSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    return-object v0
.end method

.method private instantDepositEnabled()Z
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/squareup/settings/server/InstantDepositsSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INSTANT_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private isAccountFrozen()Z
    .locals 3

    .line 121
    iget-object v0, p0, Lcom/squareup/settings/server/InstantDepositsSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ACCOUNT_FREEZE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/InstantDepositsSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->frozen:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method


# virtual methods
.method public allowInstantDeposit(Z)V
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/squareup/settings/server/InstantDepositsSettings;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    new-instance v1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;-><init>()V

    .line 107
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->allow_instant_deposit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 108
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->build()Lcom/squareup/server/account/protos/PreferencesRequest;

    move-result-object p1

    .line 106
    invoke-interface {v0, p1}, Lcom/squareup/accountstatus/AccountStatusProvider;->maybeUpdatePreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)V

    return-void
.end method

.method public getLinkedCard()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;
    .locals 1

    .line 74
    invoke-direct {p0}, Lcom/squareup/settings/server/InstantDepositsSettings;->getInstantDeposits()Lcom/squareup/server/account/protos/InstantDeposits;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/InstantDeposits;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    return-object v0
.end method

.method public hasLinkedCard()Z
    .locals 1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/settings/server/InstantDepositsSettings;->getLinkedCard()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public instantDepositAllowed()Z
    .locals 1

    .line 48
    invoke-direct {p0}, Lcom/squareup/settings/server/InstantDepositsSettings;->instantDepositEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/settings/server/InstantDepositsSettings;->allowInstantDeposit:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public instantDepositFeeAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 90
    invoke-direct {p0}, Lcom/squareup/settings/server/InstantDepositsSettings;->getInstantDeposits()Lcom/squareup/server/account/protos/InstantDeposits;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/InstantDeposits;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    iget-object v0, v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v0}, Lcom/squareup/settings/server/InstantDepositsSettings;->feeAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public instantDepositFeeBasisPoints()Ljava/lang/Integer;
    .locals 1

    .line 82
    invoke-direct {p0}, Lcom/squareup/settings/server/InstantDepositsSettings;->getInstantDeposits()Lcom/squareup/server/account/protos/InstantDeposits;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/InstantDeposits;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    iget-object v0, v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    return-object v0
.end method

.method public instantDepositFeePercentage()Lcom/squareup/util/Percentage;
    .locals 1

    .line 86
    invoke-virtual {p0}, Lcom/squareup/settings/server/InstantDepositsSettings;->instantDepositFeeBasisPoints()Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/settings/server/InstantDepositsSettings;->feePercentage(Ljava/lang/Integer;)Lcom/squareup/util/Percentage;

    move-result-object v0

    return-object v0
.end method

.method public sameDayDepositFeeAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 98
    invoke-direct {p0}, Lcom/squareup/settings/server/InstantDepositsSettings;->getInstantDeposits()Lcom/squareup/server/account/protos/InstantDeposits;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/InstantDeposits;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    iget-object v0, v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v0}, Lcom/squareup/settings/server/InstantDepositsSettings;->feeAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public sameDayDepositFeePercentage()Lcom/squareup/util/Percentage;
    .locals 1

    .line 94
    invoke-direct {p0}, Lcom/squareup/settings/server/InstantDepositsSettings;->getInstantDeposits()Lcom/squareup/server/account/protos/InstantDeposits;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/InstantDeposits;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    iget-object v0, v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    invoke-direct {p0, v0}, Lcom/squareup/settings/server/InstantDepositsSettings;->feePercentage(Ljava/lang/Integer;)Lcom/squareup/util/Percentage;

    move-result-object v0

    return-object v0
.end method

.method public showInstantDeposit()Z
    .locals 3

    .line 52
    iget-object v0, p0, Lcom/squareup/settings/server/InstantDepositsSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INSTANT_DEPOSIT_REQUIRES_LINKED_CARD:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 53
    invoke-virtual {p0}, Lcom/squareup/settings/server/InstantDepositsSettings;->instantDepositAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/settings/server/InstantDepositsSettings;->isAccountFrozen()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/InstantDepositsSettings;->hasLinkedCard()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 55
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/settings/server/InstantDepositsSettings;->instantDepositAllowed()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/squareup/settings/server/InstantDepositsSettings;->isAccountFrozen()Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method
