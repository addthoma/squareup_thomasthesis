.class public Lcom/squareup/settings/server/SupportUrlSettings;
.super Ljava/lang/Object;
.source "SupportUrlSettings.java"


# static fields
.field static final ALL_HARDWARE_PRODUCT_KEY:Ljava/lang/String;

.field static final CONTACT_SUPPORT_URL:Ljava/lang/String; = "https://squareup.com/help/contact"

.field static final DEFAULT_TWITTER_HANDLE:Ljava/lang/String; = "SqSupport"

.field static final HELP_CENTER_URL:Ljava/lang/String; = "https://squareup.com/help"

.field static final PRIVACY_POLICY_URL:Ljava/lang/String; = "https://squareup.com/legal/privacy"

.field static final R12_PRODUCT_KEY:Ljava/lang/String;

.field static final R4_PRODUCT_KEY:Ljava/lang/String;

.field static final R6_PRODUCT_KEY:Ljava/lang/String;

.field static final SELLER_AGREEMENT_URL:Ljava/lang/String; = "https://squareup.com/us/en/legal/general/ua"

.field static final SHOP_BASE_URL:Ljava/lang/String; = "https://squareup.com/shop/hardware"

.field static final US_ASCII:Ljava/nio/charset/Charset;


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "US-ASCII"

    .line 18
    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/squareup/settings/server/SupportUrlSettings;->US_ASCII:Ljava/nio/charset/Charset;

    const-string v0, "r4"

    .line 26
    invoke-static {v0}, Lcom/squareup/settings/server/SupportUrlSettings;->getProductKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/settings/server/SupportUrlSettings;->R4_PRODUCT_KEY:Ljava/lang/String;

    const-string v0, "r12"

    .line 27
    invoke-static {v0}, Lcom/squareup/settings/server/SupportUrlSettings;->getProductKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/settings/server/SupportUrlSettings;->R12_PRODUCT_KEY:Ljava/lang/String;

    const-string v0, "r6"

    .line 28
    invoke-static {v0}, Lcom/squareup/settings/server/SupportUrlSettings;->getProductKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/settings/server/SupportUrlSettings;->R6_PRODUCT_KEY:Ljava/lang/String;

    const-string v0, ""

    .line 29
    invoke-static {v0}, Lcom/squareup/settings/server/SupportUrlSettings;->getProductKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/settings/server/SupportUrlSettings;->ALL_HARDWARE_PRODUCT_KEY:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/squareup/server/account/protos/AccountStatusResponse;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/settings/server/SupportUrlSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    .line 36
    iput-object p2, p0, Lcom/squareup/settings/server/SupportUrlSettings;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method static getProductKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 123
    sget-object v0, Lcom/squareup/settings/server/SupportUrlSettings;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p0

    const/16 v0, 0x9

    invoke-static {p0, v0}, Lcom/squareup/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private getProductUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 101
    iget-object v0, p0, Lcom/squareup/settings/server/SupportUrlSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    invoke-static {v0}, Lcom/squareup/server/account/UserUtils;->getCountryCodeOrNull(Lcom/squareup/server/account/protos/User;)Lcom/squareup/CountryCode;

    move-result-object v0

    .line 102
    iget-object v1, p0, Lcom/squareup/settings/server/SupportUrlSettings;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    if-eqz v0, :cond_2

    if-nez v1, :cond_0

    goto :goto_0

    .line 107
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 108
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://squareup.com/shop/hardware"

    .line 110
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 111
    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 112
    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 114
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "p"

    .line 115
    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 116
    invoke-virtual {v2, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 119
    :cond_1
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method public getContactSupportUrl()Ljava/lang/String;
    .locals 1

    const-string v0, "https://squareup.com/help/contact"

    return-object v0
.end method

.method public getHelpCenterUrl()Ljava/lang/String;
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/squareup/settings/server/SupportUrlSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->help_center_url:Ljava/lang/String;

    .line 41
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "https://squareup.com/help"

    :cond_0
    return-object v0
.end method

.method public getPrivacyPolicyUrl()Ljava/lang/String;
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/settings/server/SupportUrlSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->privacy_policy_url:Ljava/lang/String;

    .line 46
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "https://squareup.com/legal/privacy"

    :cond_0
    return-object v0
.end method

.method public getReorderContactlessUrl()Ljava/lang/String;
    .locals 1

    .line 70
    sget-object v0, Lcom/squareup/settings/server/SupportUrlSettings;->R12_PRODUCT_KEY:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/settings/server/SupportUrlSettings;->getProductUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReorderHardwareUrl()Ljava/lang/String;
    .locals 1

    .line 75
    sget-object v0, Lcom/squareup/settings/server/SupportUrlSettings;->ALL_HARDWARE_PRODUCT_KEY:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/settings/server/SupportUrlSettings;->getProductUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReorderReaderUrl()Ljava/lang/String;
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/settings/server/SupportUrlSettings;->R4_PRODUCT_KEY:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/settings/server/SupportUrlSettings;->getProductUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReorderReaderUrl(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/String;
    .locals 1

    .line 80
    sget-object v0, Lcom/squareup/settings/server/SupportUrlSettings$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    .line 91
    sget-object p1, Lcom/squareup/settings/server/SupportUrlSettings;->ALL_HARDWARE_PRODUCT_KEY:Ljava/lang/String;

    goto :goto_0

    .line 88
    :cond_0
    sget-object p1, Lcom/squareup/settings/server/SupportUrlSettings;->R4_PRODUCT_KEY:Ljava/lang/String;

    goto :goto_0

    .line 85
    :cond_1
    sget-object p1, Lcom/squareup/settings/server/SupportUrlSettings;->R6_PRODUCT_KEY:Ljava/lang/String;

    goto :goto_0

    .line 82
    :cond_2
    sget-object p1, Lcom/squareup/settings/server/SupportUrlSettings;->R12_PRODUCT_KEY:Ljava/lang/String;

    .line 93
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/settings/server/SupportUrlSettings;->getProductUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getSellerAgreementUrl()Ljava/lang/String;
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/settings/server/SupportUrlSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->seller_agreement_url:Ljava/lang/String;

    .line 51
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "https://squareup.com/us/en/legal/general/ua"

    :cond_0
    return-object v0
.end method

.method public getSupportTwitterHandle()Ljava/lang/String;
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/settings/server/SupportUrlSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_twitter:Ljava/lang/String;

    .line 56
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "SqSupport"

    :cond_0
    return-object v0
.end method
