.class public final synthetic Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$7EvqRiiPZo9B-6hTM0W8gXDSSHk;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# instance fields
.field private final synthetic f$0:Z

.field private final synthetic f$1:Lcom/squareup/shared/catalog/models/CatalogTax;

.field private final synthetic f$2:Z

.field private final synthetic f$3:Lcom/squareup/settings/server/FeesEditor$TaxItemUpdater;


# direct methods
.method public synthetic constructor <init>(ZLcom/squareup/shared/catalog/models/CatalogTax;ZLcom/squareup/settings/server/FeesEditor$TaxItemUpdater;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$7EvqRiiPZo9B-6hTM0W8gXDSSHk;->f$0:Z

    iput-object p2, p0, Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$7EvqRiiPZo9B-6hTM0W8gXDSSHk;->f$1:Lcom/squareup/shared/catalog/models/CatalogTax;

    iput-boolean p3, p0, Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$7EvqRiiPZo9B-6hTM0W8gXDSSHk;->f$2:Z

    iput-object p4, p0, Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$7EvqRiiPZo9B-6hTM0W8gXDSSHk;->f$3:Lcom/squareup/settings/server/FeesEditor$TaxItemUpdater;

    return-void
.end method


# virtual methods
.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 4

    iget-boolean v0, p0, Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$7EvqRiiPZo9B-6hTM0W8gXDSSHk;->f$0:Z

    iget-object v1, p0, Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$7EvqRiiPZo9B-6hTM0W8gXDSSHk;->f$1:Lcom/squareup/shared/catalog/models/CatalogTax;

    iget-boolean v2, p0, Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$7EvqRiiPZo9B-6hTM0W8gXDSSHk;->f$2:Z

    iget-object v3, p0, Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$7EvqRiiPZo9B-6hTM0W8gXDSSHk;->f$3:Lcom/squareup/settings/server/FeesEditor$TaxItemUpdater;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/squareup/settings/server/RealFeesEditor;->lambda$writeTax$1(ZLcom/squareup/shared/catalog/models/CatalogTax;ZLcom/squareup/settings/server/FeesEditor$TaxItemUpdater;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;

    move-result-object p1

    return-object p1
.end method
