.class public Lcom/squareup/settings/server/SwipeChipCardsSettings;
.super Ljava/lang/Object;
.source "SwipeChipCardsSettings.java"


# instance fields
.field private final accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/settings/server/SwipeChipCardsSettings;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    .line 23
    iput-object p2, p0, Lcom/squareup/settings/server/SwipeChipCardsSettings;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public isAllowed()Z
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/settings/server/SwipeChipCardsSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_ALLOW_SWIPE_FOR_CHIP_CARDS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public isEnabled()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/settings/server/SwipeChipCardsSettings;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    invoke-interface {v0}, Lcom/squareup/accountstatus/AccountStatusProvider;->latest()Lio/reactivex/Observable;

    move-result-object v0

    .line 39
    invoke-static {}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresentObservable()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/settings/server/-$$Lambda$SwipeChipCardsSettings$8vCTrNqZNsznyQUPsigDEDeiieE;

    invoke-direct {v1, p0}, Lcom/squareup/settings/server/-$$Lambda$SwipeChipCardsSettings$8vCTrNqZNsznyQUPsigDEDeiieE;-><init>(Lcom/squareup/settings/server/SwipeChipCardsSettings;)V

    .line 40
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public isEnabledBlocking()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 34
    invoke-virtual {p0}, Lcom/squareup/settings/server/SwipeChipCardsSettings;->isEnabled()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2BlockingSupport;->getValueOrThrow(Lio/reactivex/Observable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$isEnabled$0$SwipeChipCardsSettings(Lcom/squareup/server/account/protos/AccountStatusResponse;)Ljava/lang/Boolean;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 40
    invoke-virtual {p0}, Lcom/squareup/settings/server/SwipeChipCardsSettings;->isAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 41
    invoke-static {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Preferences;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/Preferences;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 40
    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public setEnabled(Z)V
    .locals 2

    .line 50
    invoke-virtual {p0}, Lcom/squareup/settings/server/SwipeChipCardsSettings;->isAllowed()Z

    move-result v0

    const-string v1, "Changing the setting is not allowed."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/settings/server/SwipeChipCardsSettings;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    new-instance v1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;-><init>()V

    .line 53
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_allow_swipe_for_chip_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 54
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->build()Lcom/squareup/server/account/protos/PreferencesRequest;

    move-result-object p1

    .line 52
    invoke-interface {v0, p1}, Lcom/squareup/accountstatus/AccountStatusProvider;->maybeUpdatePreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)V

    return-void
.end method
