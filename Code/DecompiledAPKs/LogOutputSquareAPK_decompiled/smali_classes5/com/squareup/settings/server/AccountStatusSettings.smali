.class public Lcom/squareup/settings/server/AccountStatusSettings;
.super Ljava/lang/Object;
.source "AccountStatusSettings.java"


# instance fields
.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final lazyAccountStatus:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final statusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private storeAndForwardEnabledSetting:Lcom/squareup/payment/StoreAndForwardEnabledSetting;

.field private tenderSettingsCache:Lkotlin/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/Pair<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldagger/Lazy;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/payment/StoreAndForwardEnabledSetting;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            "Lcom/squareup/payment/StoreAndForwardEnabledSetting;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lcom/squareup/settings/server/AccountStatusSettings;->lazyAccountStatus:Ldagger/Lazy;

    .line 96
    iput-object p2, p0, Lcom/squareup/settings/server/AccountStatusSettings;->statusProvider:Ljavax/inject/Provider;

    .line 97
    iput-object p3, p0, Lcom/squareup/settings/server/AccountStatusSettings;->localeProvider:Ljavax/inject/Provider;

    .line 98
    iput-object p4, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    .line 99
    iput-object p5, p0, Lcom/squareup/settings/server/AccountStatusSettings;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    .line 100
    iput-object p6, p0, Lcom/squareup/settings/server/AccountStatusSettings;->storeAndForwardEnabledSetting:Lcom/squareup/payment/StoreAndForwardEnabledSetting;

    return-void
.end method

.method private getPreferences()Lcom/squareup/server/account/protos/Preferences;
    .locals 1

    .line 538
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    return-object v0
.end method

.method public static getPreferences(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Preferences;
    .locals 1

    if-nez p0, :cond_0

    .line 108
    sget-object p0, Lcom/squareup/server/account/PreferenceUtils;->EMPTY_PREFERENCES:Lcom/squareup/server/account/protos/Preferences;

    return-object p0

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    if-eqz v0, :cond_1

    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    goto :goto_0

    :cond_1
    sget-object p0, Lcom/squareup/server/account/PreferenceUtils;->EMPTY_PREFERENCES:Lcom/squareup/server/account/protos/Preferences;

    :goto_0
    return-object p0
.end method

.method private getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 1

    .line 534
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->statusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccountStatusResponse;

    return-object v0
.end method

.method static synthetic lambda$refresh$2(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "Status Refresh"

    .line 140
    invoke-static {p0, v0}, Lcom/squareup/receiving/SuccessOrFailureLogger;->logFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$settingsAvailable$0(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lkotlin/Unit;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 123
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method


# virtual methods
.method public canEditItemOptionGlobally()Z
    .locals 2

    .line 403
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldSyncItemOptions()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ITEM_OPTION_EDIT_MASTER:Lcom/squareup/settings/server/Features$Feature;

    .line 404
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ITEM_OPTION_GLOBAL_EDIT:Lcom/squareup/settings/server/Features$Feature;

    .line 405
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public canEditItemWithItemOptions()Z
    .locals 2

    .line 397
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldSyncItemOptions()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ITEM_OPTION_EDIT_MASTER:Lcom/squareup/settings/server/Features$Feature;

    .line 398
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ITEM_OPTION_LOCAL_EDIT:Lcom/squareup/settings/server/Features$Feature;

    .line 399
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public canPublishMerchantProfile()Z
    .locals 2

    .line 280
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getEmployeeSettings()Lcom/squareup/settings/server/EmployeeSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeSettings;->isEmployee()Z

    move-result v0

    .line 281
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v1, v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_publish_merchant_card:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public canUseRewards()Z
    .locals 2

    .line 357
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REWARDS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public canUseTableManagement()Z
    .locals 2

    .line 522
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_TABLE_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public canUseTimecards()Z
    .locals 2

    .line 361
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_timecards:Ljava/lang/Boolean;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getAppointmentSettings()Lcom/squareup/settings/server/AppointmentSettings;
    .locals 2

    .line 264
    new-instance v0, Lcom/squareup/settings/server/AppointmentSettings;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/settings/server/AppointmentSettings;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-object v0
.end method

.method public getBusinessBankingSettings()Lcom/squareup/settings/server/BusinessBankingSettings;
    .locals 2

    .line 191
    new-instance v0, Lcom/squareup/settings/server/BusinessBankingSettings;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/settings/server/BusinessBankingSettings;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-object v0
.end method

.method public getDeviceCredential()Lcom/squareup/server/account/protos/DeviceCredential;
    .locals 1

    .line 240
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    return-object v0
.end method

.method public getDisputesSettings()Lcom/squareup/settings/server/DisputesSettings;
    .locals 3

    .line 223
    new-instance v0, Lcom/squareup/settings/server/DisputesSettings;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {v0, v1, v2}, Lcom/squareup/settings/server/DisputesSettings;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method

.method public getEmployeeSettings()Lcom/squareup/settings/server/EmployeeSettings;
    .locals 2

    .line 236
    new-instance v0, Lcom/squareup/settings/server/EmployeeSettings;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/settings/server/EmployeeSettings;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-object v0
.end method

.method public getFeeTypes()Lcom/squareup/server/account/FeeTypes;
    .locals 1

    .line 268
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    invoke-static {v0}, Lcom/squareup/server/account/FeeTypes;->fromProto(Lcom/squareup/server/account/protos/FeeTypesProto;)Lcom/squareup/server/account/FeeTypes;

    move-result-object v0

    return-object v0
.end method

.method public getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;
    .locals 3

    .line 248
    new-instance v0, Lcom/squareup/settings/server/GiftCardSettings;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {v0, v1, v2}, Lcom/squareup/settings/server/GiftCardSettings;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method

.method public getInstantDepositsSettings()Lcom/squareup/settings/server/InstantDepositsSettings;
    .locals 5

    .line 185
    new-instance v0, Lcom/squareup/settings/server/InstantDepositsSettings;

    .line 186
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/server/account/protos/Preferences;->allow_instant_deposit:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    iget-object v3, p0, Lcom/squareup/settings/server/AccountStatusSettings;->lazyAccountStatus:Ldagger/Lazy;

    .line 187
    invoke-interface {v3}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/accountstatus/AccountStatusProvider;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/settings/server/InstantDepositsSettings;-><init>(Ljava/lang/Boolean;Lcom/squareup/settings/server/Features;Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-object v0
.end method

.method public getItemBatchSize()J
    .locals 2

    .line 276
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items_batch_size:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMenuBehaviorSettings()Lcom/squareup/settings/server/RstMenuBehaviorSettings;
    .locals 2

    .line 514
    new-instance v0, Lcom/squareup/settings/server/RstMenuBehaviorSettings;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/settings/server/RstMenuBehaviorSettings;-><init>(Lcom/squareup/server/account/protos/Preferences;)V

    return-object v0
.end method

.method public getMerchantProfileSettings()Lcom/squareup/settings/server/MerchantProfileSettings;
    .locals 3

    .line 209
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    .line 210
    new-instance v1, Lcom/squareup/settings/server/MerchantProfileSettings;

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/settings/server/AccountStatusSettings;->lazyAccountStatus:Ldagger/Lazy;

    .line 211
    invoke-interface {v2}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/accountstatus/AccountStatusProvider;

    invoke-direct {v1, v0, v2}, Lcom/squareup/settings/server/MerchantProfileSettings;-><init>(Ljava/lang/Boolean;Lcom/squareup/accountstatus/AccountStatusProvider;)V

    return-object v1
.end method

.method public getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;
    .locals 2

    .line 215
    new-instance v0, Lcom/squareup/settings/server/MerchantRegisterSettings;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/settings/server/MerchantRegisterSettings;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-object v0
.end method

.method public getMerchantUnits()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/MerchantUnit;",
            ">;"
        }
    .end annotation

    .line 443
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_units:Ljava/util/List;

    return-object v0
.end method

.method public getNotifications()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/Notification;",
            ">;"
        }
    .end annotation

    .line 290
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->notifications:Ljava/util/List;

    return-object v0
.end method

.method public getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;
    .locals 3

    .line 219
    new-instance v0, Lcom/squareup/settings/server/OnboardingSettings;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {v0, v1, v2}, Lcom/squareup/settings/server/OnboardingSettings;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method

.method public getOrderHubSettings()Lcom/squareup/settings/server/OrderHubSettings;
    .locals 3

    .line 195
    new-instance v0, Lcom/squareup/settings/server/OrderHubSettings;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {v0, v1, v2}, Lcom/squareup/settings/server/OrderHubSettings;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method

.method public getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;
    .locals 3

    .line 244
    new-instance v0, Lcom/squareup/settings/server/PaymentSettings;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {v0, v1, v2}, Lcom/squareup/settings/server/PaymentSettings;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method

.method public getReturnPolicy()Ljava/lang/String;
    .locals 1

    .line 227
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 228
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->return_policy:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getRstOrderManagerSettings()Lcom/squareup/settings/server/RstOrderManagerSettings;
    .locals 3

    .line 518
    new-instance v0, Lcom/squareup/settings/server/RstOrderManagerSettings;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {v0, v1, v2}, Lcom/squareup/settings/server/RstOrderManagerSettings;-><init>(Lcom/squareup/server/account/protos/Preferences;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method

.method public getServerTimeMillis()Ljava/lang/Long;
    .locals 5

    .line 298
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->server_time:Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 302
    :cond_0
    :try_start_0
    invoke-static {v0}, Lcom/squareup/util/Times;->parseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v2

    .line 304
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to parse server_time \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\""

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    return-object v1
.end method

.method public getSignatureSettings()Lcom/squareup/settings/server/SignatureSettings;
    .locals 13

    .line 145
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    .line 146
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    .line 148
    iget-object v2, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->CAN_ALWAYS_SKIP_SIGNATURE:Lcom/squareup/settings/server/Features$Feature;

    .line 149
    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/server/account/protos/Preferences;->skip_signature_always:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/server/account/protos/Preferences;->skip_signature_always:Ljava/lang/Boolean;

    .line 151
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    .line 153
    :goto_0
    new-instance v2, Lcom/squareup/settings/server/SignatureSettings;

    iget-object v5, v0, Lcom/squareup/server/account/protos/Preferences;->skip_signature:Ljava/lang/Boolean;

    if-eqz v5, :cond_1

    iget-object v5, v0, Lcom/squareup/server/account/protos/Preferences;->skip_signature:Ljava/lang/Boolean;

    .line 154
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v7, 0x1

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    :goto_1
    iget-object v5, v0, Lcom/squareup/server/account/protos/Preferences;->use_paper_signature:Ljava/lang/Boolean;

    if-eqz v5, :cond_2

    iget-object v5, v0, Lcom/squareup/server/account/protos/Preferences;->use_paper_signature:Ljava/lang/Boolean;

    .line 155
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v8, 0x1

    goto :goto_2

    :cond_2
    const/4 v8, 0x0

    :goto_2
    iget-object v5, v0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    .line 157
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v9, 0x1

    goto :goto_3

    :cond_3
    const/4 v9, 0x0

    :goto_3
    iget-object v5, v0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    if-eqz v5, :cond_4

    iget-object v5, v0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    .line 159
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v10, 0x1

    goto :goto_4

    :cond_4
    const/4 v10, 0x0

    .line 160
    :goto_4
    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->canSkipSignaturesForSmallPayments()Z

    move-result v11

    iget-object v1, v0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    .line 162
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v12, 0x1

    goto :goto_5

    :cond_5
    const/4 v12, 0x0

    :goto_5
    move-object v5, v2

    invoke-direct/range {v5 .. v12}, Lcom/squareup/settings/server/SignatureSettings;-><init>(ZZZZZZZ)V

    return-object v2
.end method

.method public getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;
    .locals 11

    .line 310
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    .line 311
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v1

    .line 313
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v2

    iget-object v6, v2, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;

    .line 314
    new-instance v2, Lcom/squareup/settings/server/StoreAndForwardSettings;

    iget-object v3, p0, Lcom/squareup/settings/server/AccountStatusSettings;->storeAndForwardEnabledSetting:Lcom/squareup/payment/StoreAndForwardEnabledSetting;

    invoke-interface {v3}, Lcom/squareup/payment/StoreAndForwardEnabledSetting;->enabled()Z

    move-result v4

    iget-object v5, v1, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    iget-object v1, p0, Lcom/squareup/settings/server/AccountStatusSettings;->lazyAccountStatus:Ldagger/Lazy;

    .line 316
    invoke-interface {v1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/accountstatus/AccountStatusProvider;

    iget-object v8, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    iget-object v9, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    iget-object v10, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    move-object v3, v2

    invoke-direct/range {v3 .. v10}, Lcom/squareup/settings/server/StoreAndForwardSettings;-><init>(ZLcom/squareup/protos/common/Money;Ljava/lang/Integer;Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/server/account/protos/StoreAndForwardKey;Lcom/squareup/server/account/protos/StoreAndForwardKey;Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;)V

    return-object v2
.end method

.method public getSubscriptions()Lcom/squareup/settings/server/Subscriptions;
    .locals 2

    .line 504
    new-instance v0, Lcom/squareup/settings/server/Subscriptions;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/settings/server/Subscriptions;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-object v0
.end method

.method public getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;
    .locals 3

    .line 252
    new-instance v0, Lcom/squareup/settings/server/SupportUrlSettings;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/settings/server/AccountStatusSettings;->localeProvider:Ljavax/inject/Provider;

    invoke-direct {v0, v1, v2}, Lcom/squareup/settings/server/SupportUrlSettings;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public getTaxIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/TaxId;",
            ">;"
        }
    .end annotation

    .line 272
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tax_ids:Ljava/util/List;

    return-object v0
.end method

.method public getTenderSettings()Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 479
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->custom_tender_options_proto:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 480
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 484
    :cond_0
    iget-object v2, p0, Lcom/squareup/settings/server/AccountStatusSettings;->tenderSettingsCache:Lkotlin/Pair;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 485
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->tenderSettingsCache:Lkotlin/Pair;

    invoke-virtual {v0}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/devicesettings/TenderSettings;

    return-object v0

    :cond_1
    const/4 v2, 0x0

    .line 489
    :try_start_0
    sget-object v3, Lcom/squareup/protos/client/devicesettings/TenderSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 490
    invoke-static {v0, v2}, Lcom/squareup/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/devicesettings/TenderSettings;

    .line 491
    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v0, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/squareup/settings/server/AccountStatusSettings;->tenderSettingsCache:Lkotlin/Pair;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    :catch_0
    move-exception v3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v4, "Error decoding custom_tender_options_proto string, returning default tender types instead."

    .line 497
    invoke-static {v3, v4, v2}, Ltimber/log/Timber;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 498
    new-instance v2, Lkotlin/Pair;

    invoke-direct {v2, v0, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/squareup/settings/server/AccountStatusSettings;->tenderSettingsCache:Lkotlin/Pair;

    :cond_2
    :goto_0
    return-object v1
.end method

.method public getTerminalSettings()Lcom/squareup/settings/server/TerminalSettings;
    .locals 2

    .line 256
    new-instance v0, Lcom/squareup/settings/server/TerminalSettings;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/settings/server/TerminalSettings;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-object v0
.end method

.method public getTipSettings()Lcom/squareup/settings/server/TipSettings;
    .locals 4

    .line 199
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    .line 200
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/server/account/protos/AccountStatusResponse;->tipping:Lcom/squareup/server/account/protos/Tipping;

    iget-object v2, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_REQUIRED:Lcom/squareup/settings/server/Features$Feature;

    .line 201
    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    .line 200
    invoke-static {v0, v1, v2}, Lcom/squareup/settings/server/TipSettings;->from(Lcom/squareup/server/account/protos/Preferences;Lcom/squareup/server/account/protos/Tipping;Z)Lcom/squareup/settings/server/TipSettings;

    move-result-object v0

    return-object v0
.end method

.method public getTutorialSettings()Lcom/squareup/settings/server/TutorialSettings;
    .locals 3

    .line 205
    new-instance v0, Lcom/squareup/settings/server/TutorialSettings;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {v0, v1, v2}, Lcom/squareup/settings/server/TutorialSettings;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method

.method public getUserSettings()Lcom/squareup/settings/server/UserSettings;
    .locals 2

    .line 232
    new-instance v0, Lcom/squareup/settings/server/UserSettings;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/settings/server/UserSettings;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-object v0
.end method

.method public getVideoUrlSettings()Lcom/squareup/settings/server/VideoUrlSettings;
    .locals 2

    .line 260
    new-instance v0, Lcom/squareup/settings/server/VideoUrlSettings;

    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/settings/server/VideoUrlSettings;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-object v0
.end method

.method public hasPosBestAvailableProductIntent()Z
    .locals 2

    .line 419
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    .line 420
    iget-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    iget-object v1, v1, Lcom/squareup/server/account/protos/ProductIntent;->best_available:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    iget-object v0, v0, Lcom/squareup/server/account/protos/ProductIntent;->best_available:Ljava/lang/String;

    sget-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_POS:Lcom/squareup/settings/server/ProductIntent;

    .line 422
    invoke-virtual {v1}, Lcom/squareup/settings/server/ProductIntent;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isAccountFrozen()Z
    .locals 2

    .line 530
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->frozen:Ljava/lang/Boolean;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isEmployeeManagementEnabledForAccount()Z
    .locals 2

    .line 365
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isHideModifiersOnReceiptsEnabled()Z
    .locals 2

    .line 345
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HIDE_MODIFIERS_ON_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public isInventoryApiDisallowed()Z
    .locals 2

    .line 373
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISALLOW_ITEMSFE_INVENTORY_API:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public isInventoryPlusEnabled()Z
    .locals 2

    .line 369
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVENTORY_PLUS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public isSkipModifierDetailScreenEnabled()Z
    .locals 2

    .line 353
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SKIP_MODIFIER_DETAIL_SCREEN:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$settingsAvailableRx2$1$AccountStatusSettings(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p0
.end method

.method public refresh()V
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->lazyAccountStatus:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/AccountStatusProvider;

    invoke-interface {v0}, Lcom/squareup/accountstatus/AccountStatusProvider;->fetch()Lio/reactivex/Single;

    move-result-object v0

    sget-object v1, Lcom/squareup/settings/server/-$$Lambda$AccountStatusSettings$qhk2JwtXVagQ-PHpUR9Dwld1_vU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$AccountStatusSettings$qhk2JwtXVagQ-PHpUR9Dwld1_vU;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public setSignatureSettings(ZZZZZ)V
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->lazyAccountStatus:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/AccountStatusProvider;

    new-instance v1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;-><init>()V

    .line 175
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->skip_signature_always(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 176
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->skip_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 177
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_paper_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 178
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->for_paper_signature_always_print_customer_copy(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 179
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->for_paper_signature_use_quick_tip_receipt(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 180
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->for_paper_signature_print_additional_auth_slip(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 181
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->build()Lcom/squareup/server/account/protos/PreferencesRequest;

    move-result-object p1

    .line 174
    invoke-interface {v0, p1}, Lcom/squareup/accountstatus/AccountStatusProvider;->maybeUpdatePreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)V

    return-void
.end method

.method public setTenderSettings(Lcom/squareup/protos/client/devicesettings/TenderSettings;)V
    .locals 2

    .line 508
    new-instance v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;-><init>()V

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 509
    invoke-virtual {v1, p1}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p1

    const/4 v1, 0x2

    invoke-static {p1, v1}, Lcom/squareup/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p1

    .line 508
    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->custom_tender_options_proto(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 509
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->build()Lcom/squareup/server/account/protos/PreferencesRequest;

    move-result-object p1

    .line 510
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->lazyAccountStatus:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/AccountStatusProvider;

    invoke-interface {v0, p1}, Lcom/squareup/accountstatus/AccountStatusProvider;->maybeUpdatePreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)V

    return-void
.end method

.method public setTipSettings(ZZZZLcom/squareup/settings/server/TipSettings$TippingCalculationPhase;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZZ",
            "Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;",
            "Ljava/util/List<",
            "Lcom/squareup/util/Percentage;",
            ">;)V"
        }
    .end annotation

    if-eqz p6, :cond_1

    .line 451
    invoke-interface {p6}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    goto :goto_0

    .line 452
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "customPercentages.size() = "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p6}, Ljava/util/List;->size()I

    move-result p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_1
    :goto_0
    if-nez p6, :cond_2

    const/4 p6, 0x0

    goto :goto_1

    .line 457
    :cond_2
    invoke-static {p6}, Lcom/squareup/settings/server/TipHelper;->tipsToJsonNumberArray(Ljava/util/List;)Ljava/lang/String;

    move-result-object p6

    .line 458
    :goto_1
    invoke-virtual {p5}, Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;->toString()Ljava/lang/String;

    move-result-object p5

    .line 459
    new-instance v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;-><init>()V

    .line 460
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 461
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_use_custom_percentages(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 462
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_use_manual_tip_entry(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 463
    invoke-virtual {p1, p5}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_calculation_phase(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 464
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_separate_tipping_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 465
    invoke-virtual {p1, p6}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_custom_percentages(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 466
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->build()Lcom/squareup/server/account/protos/PreferencesRequest;

    move-result-object p1

    .line 467
    iget-object p2, p0, Lcom/squareup/settings/server/AccountStatusSettings;->lazyAccountStatus:Ldagger/Lazy;

    invoke-interface {p2}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/accountstatus/AccountStatusProvider;

    invoke-interface {p2, p1}, Lcom/squareup/accountstatus/AccountStatusProvider;->maybeUpdatePreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)V

    return-void
.end method

.method public settingsAvailable()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 120
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->lazyAccountStatus:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/AccountStatusProvider;

    .line 121
    invoke-interface {v0}, Lcom/squareup/accountstatus/AccountStatusProvider;->latest()Lio/reactivex/Observable;

    move-result-object v0

    .line 122
    invoke-static {}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresentObservable()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/settings/server/-$$Lambda$AccountStatusSettings$CEbUQy4OjW4tuxZjLDRLRd3E_ME;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$AccountStatusSettings$CEbUQy4OjW4tuxZjLDRLRd3E_ME;

    .line 123
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public settingsAvailableRx2()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation

    .line 131
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->lazyAccountStatus:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/AccountStatusProvider;

    invoke-interface {v0}, Lcom/squareup/accountstatus/AccountStatusProvider;->latest()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresentObservable()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/settings/server/-$$Lambda$AccountStatusSettings$yQI6HGoKHuqFF6tJfRBFue2noSk;

    invoke-direct {v1, p0}, Lcom/squareup/settings/server/-$$Lambda$AccountStatusSettings$yQI6HGoKHuqFF6tJfRBFue2noSk;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;)V

    .line 132
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public shouldDisplayModifierInsteadOfOption()Z
    .locals 2

    .line 385
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISPLAY_MODIFIER_INSTEAD_OF_OPTION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public shouldEnableWholePurchaseDiscounts()Z
    .locals 2

    .line 415
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_WHOLE_PURCHASE_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public shouldLimitVariations()Z
    .locals 2

    .line 389
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LIMIT_VARIATIONS_PER_ITEM:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public shouldPerformCatalogSyncAfterTransactionOnX2()Z
    .locals 2

    .line 381
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PERFORM_CATALOG_SYNC_AFTER_TRANSACTION_ON_X2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public shouldShowAutoGratuity()Z
    .locals 2

    .line 329
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_AUTO_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public shouldShowFeeBreakdownTableOnReceipts()Z
    .locals 2

    .line 325
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_FEE_BREAKDOWN_TABLE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public shouldShowInclusiveTaxesInCart()Z
    .locals 2

    .line 321
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_INCLUSIVE_TAXES_IN_CART:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public shouldShowLibraryFirstPhone()Z
    .locals 2

    .line 333
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_ITEMS_LIBRARY_AFTER_LOGIN:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public shouldSyncItemOptions()Z
    .locals 2

    .line 393
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_ITEM_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public shouldUseBillAmendments()Z
    .locals 2

    .line 409
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    .line 410
    iget-object v1, v0, Lcom/squareup/server/account/protos/Preferences;->amended_reopens_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->amended_reopens_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public shouldUseConditionalTaxes()Z
    .locals 2

    .line 337
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CONDITIONAL_TAXES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public shouldUseDeviceSettings()Z
    .locals 2

    .line 377
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEVICE_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public varargs supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/squareup/api/items/Item$Type;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation

    .line 430
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 431
    sget-object v1, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 432
    sget-object v1, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 433
    iget-object v1, p0, Lcom/squareup/settings/server/AccountStatusSettings;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v1}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowPartiallyRolledOutFeature()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 434
    sget-object v1, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz p1, :cond_1

    .line 437
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    :cond_1
    return-object v0
.end method

.method public supportsInvoiceTipping()Z
    .locals 1

    .line 286
    invoke-direct {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStatusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoice_tipping:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
