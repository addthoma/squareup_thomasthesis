.class public final Lcom/squareup/settings/server/TerminalSettings;
.super Ljava/lang/Object;
.source "TerminalSettings.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/settings/server/TerminalSettings;",
        "",
        "accountStatusResponse",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "(Lcom/squareup/server/account/protos/AccountStatusResponse;)V",
        "terminalApiSettingEnabled",
        "",
        "getTerminalApiSettingEnabled",
        "()Z",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method public constructor <init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 1

    const-string v0, "accountStatusResponse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/settings/server/TerminalSettings;->accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    return-void
.end method


# virtual methods
.method public final getTerminalApiSettingEnabled()Z
    .locals 2

    .line 16
    iget-object v0, p0, Lcom/squareup/settings/server/TerminalSettings;->accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
