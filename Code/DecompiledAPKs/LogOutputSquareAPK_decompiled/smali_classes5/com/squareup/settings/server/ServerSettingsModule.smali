.class public abstract Lcom/squareup/settings/server/ServerSettingsModule;
.super Ljava/lang/Object;
.source "ServerSettingsModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideAccountStatusSettingsApiUrl(Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl;)Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideUserSettingsProvider(Lcom/squareup/settings/server/RealUserSettingsProvider;)Lcom/squareup/settings/server/UserSettingsProvider;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
