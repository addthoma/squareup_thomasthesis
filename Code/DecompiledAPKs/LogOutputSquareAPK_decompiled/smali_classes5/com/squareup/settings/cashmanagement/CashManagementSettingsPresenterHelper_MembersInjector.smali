.class public final Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper_MembersInjector;
.super Ljava/lang/Object;
.source "CashManagementSettingsPresenterHelper_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final priceLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper_MembersInjector;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper_MembersInjector;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;",
            ">;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectCurrencyCode(Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method public static injectMoneyFormatter(Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 48
    iput-object p1, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static injectPriceLocaleHelper(Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;Lcom/squareup/money/PriceLocaleHelper;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;)V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper_MembersInjector;->injectMoneyFormatter(Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;Lcom/squareup/text/Formatter;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper_MembersInjector;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper_MembersInjector;->injectCurrencyCode(Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper_MembersInjector;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/money/PriceLocaleHelper;

    invoke-static {p1, v0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper_MembersInjector;->injectPriceLocaleHelper(Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;Lcom/squareup/money/PriceLocaleHelper;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    invoke-virtual {p0, p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper_MembersInjector;->injectMembers(Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;)V

    return-void
.end method
