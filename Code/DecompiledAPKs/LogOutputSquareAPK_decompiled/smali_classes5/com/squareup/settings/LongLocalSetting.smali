.class public Lcom/squareup/settings/LongLocalSetting;
.super Lcom/squareup/settings/AbstractLocalSetting;
.source "LongLocalSetting.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/settings/AbstractLocalSetting<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final defaultValue:J


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2

    const-wide/16 v0, 0x0

    .line 10
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/squareup/settings/LongLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;J)V

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;J)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/AbstractLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 15
    iput-wide p3, p0, Lcom/squareup/settings/LongLocalSetting;->defaultValue:J

    return-void
.end method


# virtual methods
.method protected doGet()Ljava/lang/Long;
    .locals 4

    .line 20
    iget-object v0, p0, Lcom/squareup/settings/LongLocalSetting;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/squareup/settings/LongLocalSetting;->key:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/settings/LongLocalSetting;->defaultValue:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/squareup/settings/LongLocalSetting;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/squareup/settings/LongLocalSetting;->key:Ljava/lang/String;

    iget-wide v2, p0, Lcom/squareup/settings/LongLocalSetting;->defaultValue:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doGet()Ljava/lang/Object;
    .locals 1

    .line 5
    invoke-virtual {p0}, Lcom/squareup/settings/LongLocalSetting;->doGet()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/Long;)V
    .locals 4

    .line 25
    iget-object v0, p0, Lcom/squareup/settings/LongLocalSetting;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/settings/LongLocalSetting;->key:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/settings/LongLocalSetting;->apply(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method public bridge synthetic set(Ljava/lang/Object;)V
    .locals 0

    .line 5
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/squareup/settings/LongLocalSetting;->set(Ljava/lang/Long;)V

    return-void
.end method
