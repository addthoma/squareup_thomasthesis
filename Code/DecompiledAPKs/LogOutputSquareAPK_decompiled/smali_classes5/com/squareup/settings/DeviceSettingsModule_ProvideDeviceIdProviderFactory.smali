.class public final Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;
.super Ljava/lang/Object;
.source "DeviceSettingsModule_ProvideDeviceIdProviderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/DeviceIdProvider;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final installationIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final systemPropertiesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/SystemProperties;",
            ">;"
        }
    .end annotation
.end field

.field private final telephonyManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/SystemProperties;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;->applicationProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;->telephonyManagerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;->installationIdProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;->preferencesProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;->systemPropertiesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/SystemProperties;",
            ">;)",
            "Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;"
        }
    .end annotation

    .line 53
    new-instance v6, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static provideDeviceIdProvider(Landroid/app/Application;Landroid/telephony/TelephonyManager;Ljavax/inject/Provider;Landroid/content/SharedPreferences;Lcom/squareup/util/SystemProperties;)Lcom/squareup/settings/DeviceIdProvider;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Landroid/telephony/TelephonyManager;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/SharedPreferences;",
            "Lcom/squareup/util/SystemProperties;",
            ")",
            "Lcom/squareup/settings/DeviceIdProvider;"
        }
    .end annotation

    .line 59
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/settings/DeviceSettingsModule;->provideDeviceIdProvider(Landroid/app/Application;Landroid/telephony/TelephonyManager;Ljavax/inject/Provider;Landroid/content/SharedPreferences;Lcom/squareup/util/SystemProperties;)Lcom/squareup/settings/DeviceIdProvider;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/DeviceIdProvider;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/DeviceIdProvider;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;->telephonyManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;->installationIdProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/SharedPreferences;

    iget-object v4, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;->systemPropertiesProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/util/SystemProperties;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;->provideDeviceIdProvider(Landroid/app/Application;Landroid/telephony/TelephonyManager;Ljavax/inject/Provider;Landroid/content/SharedPreferences;Lcom/squareup/util/SystemProperties;)Lcom/squareup/settings/DeviceIdProvider;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;->get()Lcom/squareup/settings/DeviceIdProvider;

    move-result-object v0

    return-object v0
.end method
