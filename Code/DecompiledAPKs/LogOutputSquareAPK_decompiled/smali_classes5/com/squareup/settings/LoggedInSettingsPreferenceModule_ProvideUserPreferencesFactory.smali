.class public final Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideUserPreferencesFactory;
.super Ljava/lang/Object;
.source "LoggedInSettingsPreferenceModule_ProvideUserPreferencesFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Landroid/content/SharedPreferences;",
        ">;"
    }
.end annotation


# instance fields
.field private final appProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final userIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideUserPreferencesFactory;->appProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideUserPreferencesFactory;->userIdProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideUserPreferencesFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideUserPreferencesFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideUserPreferencesFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideUserPreferencesFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideUserPreferences(Landroid/app/Application;Ljava/lang/String;)Landroid/content/SharedPreferences;
    .locals 0

    .line 40
    invoke-static {p0, p1}, Lcom/squareup/settings/LoggedInSettingsPreferenceModule;->provideUserPreferences(Landroid/app/Application;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/SharedPreferences;

    return-object p0
.end method


# virtual methods
.method public get()Landroid/content/SharedPreferences;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideUserPreferencesFactory;->appProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideUserPreferencesFactory;->userIdProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideUserPreferencesFactory;->provideUserPreferences(Landroid/app/Application;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideUserPreferencesFactory;->get()Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method
