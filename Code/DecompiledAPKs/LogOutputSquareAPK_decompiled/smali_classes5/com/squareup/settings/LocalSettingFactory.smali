.class public interface abstract Lcom/squareup/settings/LocalSettingFactory;
.super Ljava/lang/Object;
.source "LocalSettingFactory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/settings/LocalSettingFactory$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J?\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u0003\"\u0008\u0008\u0000\u0010\u0004*\u00020\u00012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u00062\u0006\u0010\u0007\u001a\u00020\u00082\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u0001H\u0004H&\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/settings/LocalSettingFactory;",
        "",
        "create",
        "Lcom/squareup/settings/LocalSetting;",
        "T",
        "clazz",
        "Ljava/lang/Class;",
        "key",
        "",
        "defaultValue",
        "(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/settings/LocalSetting;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract create(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            "TT;)",
            "Lcom/squareup/settings/LocalSetting<",
            "TT;>;"
        }
    .end annotation
.end method
