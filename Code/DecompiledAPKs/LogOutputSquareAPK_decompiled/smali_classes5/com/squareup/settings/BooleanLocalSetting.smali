.class public Lcom/squareup/settings/BooleanLocalSetting;
.super Lcom/squareup/settings/AbstractLocalSetting;
.source "BooleanLocalSetting.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/settings/AbstractLocalSetting<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final defaultValue:Z


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V
    .locals 0

    .line 9
    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/AbstractLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 10
    iput-boolean p3, p0, Lcom/squareup/settings/BooleanLocalSetting;->defaultValue:Z

    return-void
.end method


# virtual methods
.method protected doGet()Ljava/lang/Boolean;
    .locals 3

    .line 15
    iget-object v0, p0, Lcom/squareup/settings/BooleanLocalSetting;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/squareup/settings/BooleanLocalSetting;->key:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/squareup/settings/BooleanLocalSetting;->defaultValue:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doGet()Ljava/lang/Object;
    .locals 1

    .line 5
    invoke-virtual {p0}, Lcom/squareup/settings/BooleanLocalSetting;->doGet()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/Boolean;)V
    .locals 2

    .line 19
    iget-object v0, p0, Lcom/squareup/settings/BooleanLocalSetting;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/settings/BooleanLocalSetting;->key:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/settings/BooleanLocalSetting;->apply(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method public bridge synthetic set(Ljava/lang/Object;)V
    .locals 0

    .line 5
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/settings/BooleanLocalSetting;->set(Ljava/lang/Boolean;)V

    return-void
.end method
