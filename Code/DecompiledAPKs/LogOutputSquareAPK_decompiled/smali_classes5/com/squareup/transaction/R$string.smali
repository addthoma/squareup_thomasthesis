.class public final Lcom/squareup/transaction/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transaction/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final additional:I = 0x7f12009f

.field public static final buyer_banner_item_edited:I = 0x7f1201df

.field public static final buyer_banner_item_removed:I = 0x7f1201e0

.field public static final buyer_cart_section_header_purchase:I = 0x7f1201f7

.field public static final buyer_cart_section_header_return:I = 0x7f1201f8

.field public static final buyer_printed_receipt_tender_cash:I = 0x7f120239

.field public static final buyer_printed_receipt_tender_no_sale:I = 0x7f12023b

.field public static final buyer_remaining_card_balance:I = 0x7f120258

.field public static final buyer_remaining_card_balance_for_hud:I = 0x7f120259

.field public static final buyer_signature_disclaimer:I = 0x7f12026d

.field public static final buyer_signature_disclaimer_no_auth:I = 0x7f12026e

.field public static final buyer_signature_disclaimer_no_name:I = 0x7f12026f

.field public static final buyer_signature_disclaimer_no_name_no_auth:I = 0x7f120270

.field public static final cart_discounts:I = 0x7f120349

.field public static final cart_tax_row:I = 0x7f120358

.field public static final cart_taxes_title:I = 0x7f12035b

.field public static final cash:I = 0x7f12035f

.field public static final emv_pin_blocked_msg:I = 0x7f120a57

.field public static final emv_pin_blocked_title:I = 0x7f120a58

.field public static final kitchen_printing_order:I = 0x7f120ea8

.field public static final offline_mode_cnp_card_not_accepted:I = 0x7f1210cd

.field public static final offline_mode_declined:I = 0x7f1210ce

.field public static final offline_mode_invalid_location:I = 0x7f1210d4

.field public static final offline_mode_non_whitelisted_brand:I = 0x7f1210d6

.field public static final offline_mode_transaction_limit_message:I = 0x7f1210d9

.field public static final paper_signature_card_issuer_agreement:I = 0x7f121322

.field public static final processing_payments_expiring:I = 0x7f1214f0

.field public static final processing_payments_expiring_by_name:I = 0x7f1214f1

.field public static final split_tender_card_brand_and_digits:I = 0x7f12184f

.field public static final split_tender_card_declined:I = 0x7f121850

.field public static final split_tender_card_declined_no_digits:I = 0x7f121851

.field public static final split_tender_card_tendered:I = 0x7f121852

.field public static final split_tender_card_tendered_plus_tip:I = 0x7f121853

.field public static final split_tender_cash_declined:I = 0x7f121854

.field public static final split_tender_cash_tendered:I = 0x7f121855

.field public static final split_tender_other_declined:I = 0x7f12185c

.field public static final split_tender_other_tendered:I = 0x7f12185d

.field public static final tip:I = 0x7f1219ae

.field public static final upload_transaction_ledger_empty:I = 0x7f121b01


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
