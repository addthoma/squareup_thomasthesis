.class public abstract Lcom/squareup/statusbar/launcher/NoSignInLauncherModule;
.super Ljava/lang/Object;
.source "NoSignInLauncherModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideSignInLauncher(Lcom/squareup/statusbar/launcher/NoSignInLauncher;)Lcom/squareup/statusbar/launcher/SignInLauncher;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
