.class public final Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;
.super Ljava/lang/Object;
.source "StatusBarEventManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/statusbar/event/StatusBarEventManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static enterFullscreenMode(Lcom/squareup/statusbar/event/StatusBarEventManager;Lio/reactivex/Completable;)V
    .locals 0

    const-string/jumbo p0, "until"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static enterFullscreenMode(Lcom/squareup/statusbar/event/StatusBarEventManager;Ljava/lang/String;)V
    .locals 0

    const-string p0, "sessionKey"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static exitFullscreenMode(Lcom/squareup/statusbar/event/StatusBarEventManager;Ljava/lang/String;)V
    .locals 0

    const-string p0, "sessionKey"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static inFullscreenMode(Lcom/squareup/statusbar/event/StatusBarEventManager;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 96
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object p0

    const-string v0, "Observable.empty()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static launchSignInOrRegister(Lcom/squareup/statusbar/event/StatusBarEventManager;)V
    .locals 0

    return-void
.end method

.method public static onBadgesChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Ljava/lang/String;I)V
    .locals 0

    const-string p0, "applet"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static onBranStatusChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus;)V
    .locals 0

    const-string p0, "branStatus"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static onChargerTypeChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/statusbar/event/StatusBarEventManager$ChargerType;)V
    .locals 0

    const-string p0, "chargerType"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static onConfigureDeviceButtonStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/statusbar/event/StatusBarEventManager$ConfigureDeviceButtonState;)V
    .locals 0

    const-string p0, "configureDeviceButtonState"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static onCriticalSoftwareUpdateAvailable(Lcom/squareup/statusbar/event/StatusBarEventManager;Z)V
    .locals 0

    return-void
.end method

.method public static onEditModeChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Z)V
    .locals 0

    return-void
.end method

.method public static onEmployeeManagementButtonStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/statusbar/event/StatusBarEventManager$EmployeeManagementButtonState;)V
    .locals 0

    const-string p0, "employeeManagementButtonState"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static onInternalPrinterOutOfPaper(Lcom/squareup/statusbar/event/StatusBarEventManager;Z)V
    .locals 0

    return-void
.end method

.method public static onInternalPrinterUnrecoverableError(Lcom/squareup/statusbar/event/StatusBarEventManager;Z)V
    .locals 0

    return-void
.end method

.method public static onLowBatteryStatusChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/statusbar/event/StatusBarEventManager$LowBatteryDialogStatus;)V
    .locals 0

    const-string p0, "lowBatteryDialogStatus"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static onNotificationButtonStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationButtonState;)V
    .locals 0

    const-string p0, "notificationButtonState"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static onNotificationCenterStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState;)V
    .locals 0

    const-string p0, "notificationCenterState"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static onOobFinished(Lcom/squareup/statusbar/event/StatusBarEventManager;)V
    .locals 0

    return-void
.end method

.method public static onPrinterDoorStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Z)V
    .locals 0

    return-void
.end method

.method public static onSwitcherStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState;)V
    .locals 0

    const-string p0, "switcherState"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static showBfdUpdateStatusDialog(Lcom/squareup/statusbar/event/StatusBarEventManager;)V
    .locals 0

    return-void
.end method

.method public static showNotificationListDialog(Lcom/squareup/statusbar/event/StatusBarEventManager;)V
    .locals 0

    return-void
.end method
