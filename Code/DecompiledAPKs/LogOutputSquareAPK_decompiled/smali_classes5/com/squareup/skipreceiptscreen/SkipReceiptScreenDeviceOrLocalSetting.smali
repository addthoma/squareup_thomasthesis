.class public final Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;
.super Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;
.source "SkipReceiptScreenDeviceOrLocalSetting.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B-\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u000e\u0008\u0001\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00020\t\u00a2\u0006\u0002\u0010\nJ\r\u0010\u000b\u001a\u00020\u0002H\u0014\u00a2\u0006\u0002\u0010\u000cJ\r\u0010\r\u001a\u00020\u0002H\u0014\u00a2\u0006\u0002\u0010\u000cJ\u0006\u0010\u000e\u001a\u00020\u0002J\u0008\u0010\u000f\u001a\u00020\u0002H\u0014J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0014R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;",
        "Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;",
        "",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "accountStatusProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "skipReceiptScreen",
        "Lcom/squareup/settings/LocalSetting;",
        "(Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;)V",
        "getValueFromDeviceProfile",
        "()Ljava/lang/Boolean;",
        "getValueFromLocalSettings",
        "isAllowed",
        "isAllowedWhenUsingLocal",
        "setValueLocallyInternal",
        "",
        "enabledLocally",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final skipReceiptScreen:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;)V
    .locals 1
    .param p3    # Lcom/squareup/settings/LocalSetting;
        .annotation runtime Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreen;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "skipReceiptScreen"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 17
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;-><init>(Lcom/squareup/settings/server/Features;Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;->accountStatusProvider:Ljavax/inject/Provider;

    iput-object p3, p0, Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;->skipReceiptScreen:Lcom/squareup/settings/LocalSetting;

    return-void
.end method


# virtual methods
.method protected getValueFromDeviceProfile()Ljava/lang/Boolean;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;->accountStatusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->skip_receipt_screen:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getValueFromDeviceProfile()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;->getValueFromDeviceProfile()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected getValueFromLocalSettings()Ljava/lang/Boolean;
    .locals 2

    .line 19
    iget-object v0, p0, Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;->skipReceiptScreen:Lcom/squareup/settings/LocalSetting;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "skipReceiptScreen.get(false)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public bridge synthetic getValueFromLocalSettings()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;->getValueFromLocalSettings()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final isAllowed()Z
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;->isAllowedWhenUsingLocal()Z

    move-result v0

    return v0
.end method

.method protected isAllowedWhenUsingLocal()Z
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->FAST_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic setValueLocallyInternal(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;->setValueLocallyInternal(Z)V

    return-void
.end method

.method protected setValueLocallyInternal(Z)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;->skipReceiptScreen:Lcom/squareup/settings/LocalSetting;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method
