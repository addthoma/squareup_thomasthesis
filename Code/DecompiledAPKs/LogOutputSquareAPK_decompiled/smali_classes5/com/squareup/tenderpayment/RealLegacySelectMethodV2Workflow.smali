.class public final Lcom/squareup/tenderpayment/RealLegacySelectMethodV2Workflow;
.super Ljava/lang/Object;
.source "RealLegacySelectMethodV2Workflow.kt"

# interfaces
.implements Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J@\u0010\u000e\u001a:\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0008\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\n\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000bj\u0002`\u000c0\tj\n\u0012\u0006\u0008\u0001\u0012\u00020\n`\r0\u0006H\u0016RF\u0010\u0005\u001a:\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0008\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\n\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000bj\u0002`\u000c0\tj\n\u0012\u0006\u0008\u0001\u0012\u00020\n`\r0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/RealLegacySelectMethodV2Workflow;",
        "Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;",
        "selectMethodScreenWorkflowStarter",
        "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$Starter;",
        "(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$Starter;)V",
        "workflow",
        "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;",
        "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "asStatefulWorkflow",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final workflow:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter<",
            "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$Starter;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "selectMethodScreenWorkflowStarter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    .line 19
    new-instance v1, Lcom/squareup/tenderpayment/RealLegacySelectMethodV2Workflow$workflow$1;

    invoke-direct {v1, p1}, Lcom/squareup/tenderpayment/RealLegacySelectMethodV2Workflow$workflow$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$Starter;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 20
    new-instance v2, Lcom/squareup/tenderpayment/RealLegacySelectMethodV2Workflow$workflow$2;

    invoke-direct {v2, p1}, Lcom/squareup/tenderpayment/RealLegacySelectMethodV2Workflow$workflow$2;-><init>(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$Starter;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 15
    invoke-direct {v0, v1, v2}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    iput-object v0, p0, Lcom/squareup/tenderpayment/RealLegacySelectMethodV2Workflow;->workflow:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    return-void
.end method


# virtual methods
.method public bridge synthetic asStatefulWorkflow()Lcom/squareup/workflow/StatefulWorkflow;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/RealLegacySelectMethodV2Workflow;->asStatefulWorkflow()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/StatefulWorkflow;

    return-object v0
.end method

.method public asStatefulWorkflow()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter<",
            "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealLegacySelectMethodV2Workflow;->workflow:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    return-object v0
.end method
