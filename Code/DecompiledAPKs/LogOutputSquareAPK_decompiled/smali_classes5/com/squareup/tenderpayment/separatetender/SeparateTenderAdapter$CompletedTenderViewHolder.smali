.class final Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;
.super Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;
.source "SeparateTenderAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CompletedTenderViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J,\u0010\u0013\u001a\u00020\u00142\u001c\u0010\u000b\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rj\u0002`\u00100\u000c2\u0006\u0010\u0011\u001a\u00020\u0012J\u0008\u0010\u0015\u001a\u00020\u0014H\u0016J\u0008\u0010\u0016\u001a\u00020\u0014H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u000b\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rj\u0002`\u00100\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;",
        "view",
        "Landroid/view/View;",
        "(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;Landroid/view/View;)V",
        "cancelButton",
        "Landroid/widget/TextView;",
        "disposable",
        "Lio/reactivex/disposables/SerialDisposable;",
        "lineRow",
        "Lcom/squareup/ui/account/view/LineRow;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderScreen;",
        "tender",
        "Lcom/squareup/payment/tender/BaseTender;",
        "bind",
        "",
        "onAttach",
        "onDetach",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cancelButton:Landroid/widget/TextView;

.field private final disposable:Lio/reactivex/disposables/SerialDisposable;

.field private final lineRow:Lcom/squareup/ui/account/view/LineRow;

.field private screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private tender:Lcom/squareup/payment/tender/BaseTender;

.field final synthetic this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;


# direct methods
.method public constructor <init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 409
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    .line 411
    invoke-direct {p0, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 412
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->disposable:Lio/reactivex/disposables/SerialDisposable;

    .line 414
    sget p1, Lcom/squareup/tenderworkflow/R$id;->split_tender_completed_payments_cancel_button:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->cancelButton:Landroid/widget/TextView;

    .line 416
    sget p1, Lcom/squareup/tenderworkflow/R$id;->split_tender_completed_payments_line_row:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/LineRow;

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->lineRow:Lcom/squareup/ui/account/view/LineRow;

    return-void
.end method

.method public static final synthetic access$getCancelButton$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;)Landroid/widget/TextView;
    .locals 0

    .line 409
    iget-object p0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->cancelButton:Landroid/widget/TextView;

    return-object p0
.end method

.method public static final synthetic access$getTender$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;)Lcom/squareup/payment/tender/BaseTender;
    .locals 1

    .line 409
    iget-object p0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->tender:Lcom/squareup/payment/tender/BaseTender;

    if-nez p0, :cond_0

    const-string v0, "tender"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setTender$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;Lcom/squareup/payment/tender/BaseTender;)V
    .locals 0

    .line 409
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->tender:Lcom/squareup/payment/tender/BaseTender;

    return-void
.end method


# virtual methods
.method public final bind(Lio/reactivex/Observable;Lcom/squareup/payment/tender/BaseTender;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;>;",
            "Lcom/squareup/payment/tender/BaseTender;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tender"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 426
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->screens:Lio/reactivex/Observable;

    .line 427
    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->tender:Lcom/squareup/payment/tender/BaseTender;

    return-void
.end method

.method public onAttach()V
    .locals 4

    .line 431
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->lineRow:Lcom/squareup/ui/account/view/LineRow;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->tender:Lcom/squareup/payment/tender/BaseTender;

    const-string v2, "tender"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/payment/tender/BaseTender;->getTenderTypeGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 432
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->lineRow:Lcom/squareup/ui/account/view/LineRow;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->tender:Lcom/squareup/payment/tender/BaseTender;

    if-nez v1, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1}, Lcom/squareup/payment/tender/BaseTender;->getShortTenderMessage()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    .line 433
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->lineRow:Lcom/squareup/ui/account/view/LineRow;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-static {v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->access$getMoneyFormatter$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/text/Formatter;

    move-result-object v1

    iget-object v3, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->tender:Lcom/squareup/payment/tender/BaseTender;

    if-nez v3, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v3}, Lcom/squareup/payment/tender/BaseTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 434
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->lineRow:Lcom/squareup/ui/account/view/LineRow;

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 435
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->lineRow:Lcom/squareup/ui/account/view/LineRow;

    sget v1, Lcom/squareup/noho/R$dimen;->noho_text_size_body:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setTitleTextSize(I)V

    .line 436
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->lineRow:Lcom/squareup/ui/account/view/LineRow;

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setValueWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 437
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->lineRow:Lcom/squareup/ui/account/view/LineRow;

    sget v1, Lcom/squareup/noho/R$dimen;->noho_text_size_body:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setValueTextSize(I)V

    .line 439
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->disposable:Lio/reactivex/disposables/SerialDisposable;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->screens:Lio/reactivex/Observable;

    if-nez v1, :cond_3

    const-string v2, "screens"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder$onAttach$1;

    invoke-direct {v2, p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder$onAttach$1;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public onDetach()V
    .locals 2

    .line 449
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->disposable:Lio/reactivex/disposables/SerialDisposable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method
