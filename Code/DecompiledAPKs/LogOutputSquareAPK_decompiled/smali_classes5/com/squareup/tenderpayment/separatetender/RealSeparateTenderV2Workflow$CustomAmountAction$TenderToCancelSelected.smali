.class public final Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;
.super Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction;
.source "RealSeparateTenderV2Workflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TenderToCancelSelected"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0005\u001a\u00020\u0003H\u00c2\u0003J\u0013\u0010\u0006\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\nH\u00d6\u0003J\t\u0010\u000b\u001a\u00020\u000cH\u00d6\u0001J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u0010*\u0008\u0012\u0004\u0012\u00020\u00120\u0011H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;",
        "Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction;",
        "cancelledTender",
        "Lcom/squareup/payment/tender/BaseTender;",
        "(Lcom/squareup/payment/tender/BaseTender;)V",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cancelledTender:Lcom/squareup/payment/tender/BaseTender;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/tender/BaseTender;)V
    .locals 1

    const-string v0, "cancelledTender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 225
    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;->cancelledTender:Lcom/squareup/payment/tender/BaseTender;

    return-void
.end method

.method private final component1()Lcom/squareup/payment/tender/BaseTender;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;->cancelledTender:Lcom/squareup/payment/tender/BaseTender;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;Lcom/squareup/payment/tender/BaseTender;ILjava/lang/Object;)Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;->cancelledTender:Lcom/squareup/payment/tender/BaseTender;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;->copy(Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
            ">;)",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$ConfirmCancelSeparate;

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;->cancelledTender:Lcom/squareup/payment/tender/BaseTender;

    invoke-direct {v0, v2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$ConfirmCancelSeparate;-><init>(Lcom/squareup/payment/tender/BaseTender;)V

    move-object v10, v0

    check-cast v10, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x7f

    const/4 v12, 0x0

    invoke-static/range {v1 .. v12}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->copy$default(Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;ILjava/lang/Object;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 223
    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public final copy(Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;
    .locals 1

    const-string v0, "cancelledTender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;-><init>(Lcom/squareup/payment/tender/BaseTender;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;->cancelledTender:Lcom/squareup/payment/tender/BaseTender;

    iget-object p1, p1, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;->cancelledTender:Lcom/squareup/payment/tender/BaseTender;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;->cancelledTender:Lcom/squareup/payment/tender/BaseTender;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TenderToCancelSelected(cancelledTender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;->cancelledTender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
