.class public final Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "SeparateTenderCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final moneyLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator_Factory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator_Factory_Factory;->moneyLocaleHelperProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator_Factory_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator_Factory_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/text/Formatter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;"
        }
    .end annotation

    .line 53
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;-><init>(Lcom/squareup/text/Formatter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;
    .locals 4

    .line 41
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator_Factory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator_Factory_Factory;->moneyLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/money/MoneyLocaleHelper;

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator_Factory_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator_Factory_Factory;->newInstance(Lcom/squareup/text/Formatter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator_Factory_Factory;->get()Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
