.class final Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$2;
.super Ljava/lang/Object;
.source "RealSelectMethodWorkflow.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->createScreen(Lcom/squareup/workflow/legacy/Screen$Key;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$2;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;)V
    .locals 1

    .line 1317
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$2;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$getTutorialCore$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/tutorialv2/TutorialCore;

    move-result-object p1

    const-string v0, "SelectMethodScreen selected a non-card option"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 229
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$2;->call(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;)V

    return-void
.end method
