.class public Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;
.super Ljava/lang/Object;
.source "TenderSettingsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/TenderSettingsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TenderSettingsUpdate"
.end annotation


# instance fields
.field public final destCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

.field public final isDragUpdate:Z

.field public final settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

.field public final srcCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

.field public final tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Z)V
    .locals 0

    .line 500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501
    iput-object p1, p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    .line 502
    iput-object p2, p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 503
    iput-object p3, p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->destCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    .line 504
    iput-object p4, p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->srcCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    .line 505
    iput-boolean p5, p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->isDragUpdate:Z

    return-void
.end method
