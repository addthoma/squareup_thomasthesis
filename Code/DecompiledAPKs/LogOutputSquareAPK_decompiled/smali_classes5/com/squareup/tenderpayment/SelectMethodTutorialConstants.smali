.class public final Lcom/squareup/tenderpayment/SelectMethodTutorialConstants;
.super Ljava/lang/Object;
.source "SelectMethodTutorialConstants.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodTutorialConstants;",
        "",
        "()V",
        "LEAVING",
        "",
        "LEAVING_IN_RANGE",
        "ON_NONCARD_SELECTED",
        "SHOWN",
        "SHOWN_ABOVE_MAXIMUM",
        "SHOWN_BELOW_MINIMUM",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/tenderpayment/SelectMethodTutorialConstants;

.field public static final LEAVING:Ljava/lang/String; = "Leaving SelectMethodScreen"

.field public static final LEAVING_IN_RANGE:Ljava/lang/String; = "Leaving SelectMethodScreen, card amount in range"

.field public static final ON_NONCARD_SELECTED:Ljava/lang/String; = "SelectMethodScreen selected a non-card option"

.field public static final SHOWN:Ljava/lang/String; = "Shown SelectMethodScreen"

.field public static final SHOWN_ABOVE_MAXIMUM:Ljava/lang/String; = "Shown SelectMethodScreen, above max card amount"

.field public static final SHOWN_BELOW_MINIMUM:Ljava/lang/String; = "Shown SelectMethodScreen, below min card amount"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 3
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodTutorialConstants;

    invoke-direct {v0}, Lcom/squareup/tenderpayment/SelectMethodTutorialConstants;-><init>()V

    sput-object v0, Lcom/squareup/tenderpayment/SelectMethodTutorialConstants;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodTutorialConstants;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
