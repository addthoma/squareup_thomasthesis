.class public final Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$Starter;
.super Ljava/lang/Object;
.source "RealSelectMethodWorkflow.kt"

# interfaces
.implements Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$Starter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Starter"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0008\u0001\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u000bH\u0016R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$Starter;",
        "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$Starter;",
        "workflowProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;",
        "(Ljavax/inject/Provider;)V",
        "start",
        "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow;",
        "startArgs",
        "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final workflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "workflowProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$Starter;->workflowProvider:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public start(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow;
    .locals 1

    const-string v0, "startArgs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$Starter;->workflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    .line 267
    invoke-static {v0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$start(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    const-string/jumbo p1, "workflow"

    .line 268
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow;

    return-object v0
.end method

.method public start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow;
    .locals 1

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 272
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$Starter;->workflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    .line 273
    invoke-static {v0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$startFromSnapshot(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/workflow/Snapshot;)V

    const-string/jumbo p1, "workflow"

    .line 274
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow;

    return-object v0
.end method
