.class final Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$1;
.super Ljava/lang/Object;
.source "RealSelectMethodWorkflow.kt"

# interfaces
.implements Lcom/squareup/fsm/Guard;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;-><init>(Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/util/Device;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/badbus/BadBus;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/tutorialv2/TutorialCore;Lrx/Scheduler;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;Lcom/squareup/tenderpayment/SelectMethodScreensFactory;Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Z
    .locals 1

    .line 366
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$getStartArgs$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getReaderPaymentsEnabled()Z

    move-result v0

    return v0
.end method
