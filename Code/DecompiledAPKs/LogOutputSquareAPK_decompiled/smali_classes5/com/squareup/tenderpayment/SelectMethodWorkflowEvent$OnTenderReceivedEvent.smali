.class public abstract Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;
.super Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;
.source "SelectMethodWorkflowEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "OnTenderReceivedEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnCashTenderReceivedEvent;,
        Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnConfirmCardOnFileChargedEvent;,
        Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnChargeCardOnFileEvent;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u000b\u000c\rB\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u0082\u0001\u0003\u000e\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;",
        "tender",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
        "tenderedAmount",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/common/Money;)V",
        "getTender",
        "()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
        "getTenderedAmount",
        "()Lcom/squareup/protos/common/Money;",
        "OnCashTenderReceivedEvent",
        "OnChargeCardOnFileEvent",
        "OnConfirmCardOnFileChargedEvent",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnCashTenderReceivedEvent;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnConfirmCardOnFileChargedEvent;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnChargeCardOnFileEvent;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

.field private final tenderedAmount:Lcom/squareup/protos/common/Money;


# direct methods
.method private constructor <init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/common/Money;)V
    .locals 1

    const/4 v0, 0x0

    .line 74
    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;->tenderedAmount:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/common/Money;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 71
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/common/Money;)V

    return-void
.end method


# virtual methods
.method public final getTender()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    return-object v0
.end method

.method public final getTenderedAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;->tenderedAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method
