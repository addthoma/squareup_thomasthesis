.class final Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;
.super Ljava/lang/Object;
.source "PaymentInputHandlers.kt"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/PaymentInputHandlersKt;->awaitPaymentEvent(Lcom/squareup/ui/main/errors/PaymentInputHandler;Z)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/ObservableEmitter;",
        "Lcom/squareup/tenderpayment/PaymentInputHandlerOutput;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $shouldEnableNfcField:Z

.field final synthetic $this_awaitPaymentEvent:Lcom/squareup/ui/main/errors/PaymentInputHandler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/errors/PaymentInputHandler;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;->$this_awaitPaymentEvent:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    iput-boolean p2, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;->$shouldEnableNfcField:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "Lcom/squareup/tenderpayment/PaymentInputHandlerOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v0, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    .line 39
    new-instance v1, Lkotlin/jvm/internal/Ref$BooleanRef;

    invoke-direct {v1}, Lkotlin/jvm/internal/Ref$BooleanRef;-><init>()V

    const/4 v2, 0x1

    iput-boolean v2, v1, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    .line 41
    new-instance v2, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$1;

    invoke-direct {v2, p0, v1, v0}, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$1;-><init>(Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;Lkotlin/jvm/internal/Ref$BooleanRef;Lio/reactivex/disposables/SerialDisposable;)V

    check-cast v2, Lio/reactivex/functions/Cancellable;

    invoke-interface {p1, v2}, Lio/reactivex/ObservableEmitter;->setCancellable(Lio/reactivex/functions/Cancellable;)V

    .line 50
    iget-object v2, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;->$this_awaitPaymentEvent:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v2}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->getEvents()Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$2;

    invoke-direct {v3, v1, p1}, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$2;-><init>(Lkotlin/jvm/internal/Ref$BooleanRef;Lio/reactivex/ObservableEmitter;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    .line 58
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;->$shouldEnableNfcField:Z

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;->$this_awaitPaymentEvent:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    new-instance v1, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$3;

    invoke-direct {v1, p1}, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$3;-><init>(Lio/reactivex/ObservableEmitter;)V

    check-cast v1, Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->initializeWithNfcFieldOn(Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)V

    goto :goto_0

    .line 78
    :cond_0
    iget-object p1, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;->$this_awaitPaymentEvent:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->initializeWithoutNfc()V

    :goto_0
    return-void
.end method
