.class public final Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper_Factory;
.super Ljava/lang/Object;
.source "RealManualCardEntryScreenDataHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final cnpFeesMessageHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final formatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper_Factory;->cnpFeesMessageHelperProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper_Factory;->resProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper_Factory;->formatterProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper_Factory;->transactionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)",
            "Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/cnp/CnpFeesMessageHelper;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;)Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            ")",
            "Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;"
        }
    .end annotation

    .line 52
    new-instance v0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;-><init>(Lcom/squareup/cnp/CnpFeesMessageHelper;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper_Factory;->cnpFeesMessageHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cnp/CnpFeesMessageHelper;

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper_Factory;->formatterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/text/Formatter;

    iget-object v3, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/payment/Transaction;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper_Factory;->newInstance(Lcom/squareup/cnp/CnpFeesMessageHelper;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;)Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper_Factory;->get()Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;

    move-result-object v0

    return-object v0
.end method
