.class public final Lcom/squareup/tenderpayment/SelectMethod$ScreenData;
.super Ljava/lang/Object;
.source "SelectMethodScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u00c3\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0006\u0012\u0006\u0010\n\u001a\u00020\u0006\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u0006\u0010\u0011\u001a\u00020\u0008\u0012\u0006\u0010\u0012\u001a\u00020\u0008\u0012\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u000e\u0012\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u000e\u0012\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u000e\u0012\u0006\u0010\u0017\u001a\u00020\u0006\u0012\u0006\u0010\u0018\u001a\u00020\u0006\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u0012\u0006\u0010\u001d\u001a\u00020\u001c\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u00a2\u0006\u0002\u0010 J\u0006\u0010!\u001a\u00020\"J\u000c\u0010#\u001a\u00020\"*\u00020\u001fH\u0002R\u0010\u0010\u0011\u001a\u00020\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u000e8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001b\u001a\u00020\u001c8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u000e8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u00020\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u000e8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u00020\u000c8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u00020\u001a8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001d\u001a\u00020\u001c8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        "",
        "amountDue",
        "Lcom/squareup/protos/common/Money;",
        "totalAmountDue",
        "isSplitTenderPayment",
        "",
        "splitTenderButtonLabel",
        "Lcom/squareup/tenderpayment/SelectMethod$TextData;",
        "isPaymentInCardRange",
        "isPaymentInGiftCardRange",
        "splitTenderState",
        "Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;",
        "primaryTenderViewData",
        "",
        "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
        "secondaryTenderViewData",
        "actionablePromptText",
        "paymentPromptText",
        "quickCashOptions",
        "creditCardOptions",
        "Lcom/squareup/tenderpayment/CardOnFileSummary;",
        "giftCardOptions",
        "isTablet",
        "disableQuickCashOptions",
        "toastData",
        "Lcom/squareup/tenderpayment/SelectMethod$ToastData;",
        "currentSplitNumber",
        "",
        "totalSplits",
        "cardOption",
        "Lcom/squareup/payment/CardOptionEnabled;",
        "(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLcom/squareup/tenderpayment/SelectMethod$TextData;ZZLcom/squareup/tenderpayment/SelectMethod$SplitTenderState;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/SelectMethod$TextData;Lcom/squareup/tenderpayment/SelectMethod$TextData;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;JJLcom/squareup/payment/CardOptionEnabled;)V",
        "getPrimaryTutorialString",
        "",
        "getTutorialString",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final actionablePromptText:Lcom/squareup/tenderpayment/SelectMethod$TextData;

.field public final amountDue:Lcom/squareup/protos/common/Money;

.field private final cardOption:Lcom/squareup/payment/CardOptionEnabled;

.field public final creditCardOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/CardOnFileSummary;",
            ">;"
        }
    .end annotation
.end field

.field public final currentSplitNumber:J

.field public final disableQuickCashOptions:Z

.field public final giftCardOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/CardOnFileSummary;",
            ">;"
        }
    .end annotation
.end field

.field public final isPaymentInCardRange:Z

.field public final isPaymentInGiftCardRange:Z

.field public final isSplitTenderPayment:Z

.field public final isTablet:Z

.field public final paymentPromptText:Lcom/squareup/tenderpayment/SelectMethod$TextData;

.field public final primaryTenderViewData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
            ">;"
        }
    .end annotation
.end field

.field public final quickCashOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field public final secondaryTenderViewData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
            ">;"
        }
    .end annotation
.end field

.field public final splitTenderButtonLabel:Lcom/squareup/tenderpayment/SelectMethod$TextData;

.field public final splitTenderState:Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;

.field public final toastData:Lcom/squareup/tenderpayment/SelectMethod$ToastData;

.field public final totalAmountDue:Lcom/squareup/protos/common/Money;

.field public final totalSplits:J


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLcom/squareup/tenderpayment/SelectMethod$TextData;ZZLcom/squareup/tenderpayment/SelectMethod$SplitTenderState;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/SelectMethod$TextData;Lcom/squareup/tenderpayment/SelectMethod$TextData;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;JJLcom/squareup/payment/CardOptionEnabled;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Z",
            "Lcom/squareup/tenderpayment/SelectMethod$TextData;",
            "ZZ",
            "Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;",
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
            ">;",
            "Lcom/squareup/tenderpayment/SelectMethod$TextData;",
            "Lcom/squareup/tenderpayment/SelectMethod$TextData;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/tenderpayment/CardOnFileSummary;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/tenderpayment/CardOnFileSummary;",
            ">;ZZ",
            "Lcom/squareup/tenderpayment/SelectMethod$ToastData;",
            "JJ",
            "Lcom/squareup/payment/CardOptionEnabled;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p4

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    move-object/from16 v8, p11

    move-object/from16 v9, p12

    move-object/from16 v10, p13

    move-object/from16 v11, p14

    move-object/from16 v12, p17

    move-object/from16 v13, p22

    const-string v14, "amountDue"

    invoke-static {v1, v14}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v14, "totalAmountDue"

    invoke-static {v2, v14}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "splitTenderButtonLabel"

    invoke-static {v3, v14}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "splitTenderState"

    invoke-static {v4, v14}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "primaryTenderViewData"

    invoke-static {v5, v14}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "secondaryTenderViewData"

    invoke-static {v6, v14}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "actionablePromptText"

    invoke-static {v7, v14}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "paymentPromptText"

    invoke-static {v8, v14}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "quickCashOptions"

    invoke-static {v9, v14}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "creditCardOptions"

    invoke-static {v10, v14}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "giftCardOptions"

    invoke-static {v11, v14}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v14, "toastData"

    invoke-static {v12, v14}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "cardOption"

    invoke-static {v13, v14}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->amountDue:Lcom/squareup/protos/common/Money;

    iput-object v2, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->totalAmountDue:Lcom/squareup/protos/common/Money;

    move/from16 v1, p3

    iput-boolean v1, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->isSplitTenderPayment:Z

    iput-object v3, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->splitTenderButtonLabel:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move/from16 v1, p5

    iput-boolean v1, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->isPaymentInCardRange:Z

    move/from16 v1, p6

    iput-boolean v1, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->isPaymentInGiftCardRange:Z

    iput-object v4, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->splitTenderState:Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;

    iput-object v5, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->primaryTenderViewData:Ljava/util/List;

    iput-object v6, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->secondaryTenderViewData:Ljava/util/List;

    iput-object v7, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->actionablePromptText:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iput-object v8, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->paymentPromptText:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move/from16 v1, p15

    iput-boolean v1, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->isTablet:Z

    move/from16 v1, p16

    iput-boolean v1, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->disableQuickCashOptions:Z

    iput-object v12, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->toastData:Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    move-wide/from16 v1, p18

    iput-wide v1, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->currentSplitNumber:J

    move-wide/from16 v1, p20

    iput-wide v1, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->totalSplits:J

    iput-object v13, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->cardOption:Lcom/squareup/payment/CardOptionEnabled;

    .line 141
    move-object v1, v9

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->quickCashOptions:Ljava/util/List;

    .line 142
    move-object v1, v10

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->creditCardOptions:Ljava/util/List;

    .line 143
    move-object v1, v11

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->giftCardOptions:Ljava/util/List;

    return-void
.end method

.method private final getTutorialString(Lcom/squareup/payment/CardOptionEnabled;)Ljava/lang/String;
    .locals 1

    .line 149
    instance-of v0, p1, Lcom/squareup/payment/CardOptionEnabled$ShouldBeEnabled;

    if-eqz v0, :cond_0

    const-string p1, "Shown SelectMethodScreen"

    goto :goto_0

    .line 150
    :cond_0
    instance-of v0, p1, Lcom/squareup/payment/CardOptionEnabled$ShouldBeDisabled$AboveMax;

    if-eqz v0, :cond_1

    const-string p1, "Shown SelectMethodScreen, above max card amount"

    goto :goto_0

    .line 151
    :cond_1
    instance-of p1, p1, Lcom/squareup/payment/CardOptionEnabled$ShouldBeDisabled$BelowMin;

    if-eqz p1, :cond_2

    const-string p1, "Shown SelectMethodScreen, below min card amount"

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public final getPrimaryTutorialString()Ljava/lang/String;
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->cardOption:Lcom/squareup/payment/CardOptionEnabled;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->getTutorialString(Lcom/squareup/payment/CardOptionEnabled;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
