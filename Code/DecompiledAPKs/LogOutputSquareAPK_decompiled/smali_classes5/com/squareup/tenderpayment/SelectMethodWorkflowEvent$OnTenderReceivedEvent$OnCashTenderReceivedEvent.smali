.class public final Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnCashTenderReceivedEvent;
.super Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;
.source "SelectMethodWorkflowEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnCashTenderReceivedEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnCashTenderReceivedEvent;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;",
        "tenderedAmount",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/protos/common/Money;)V",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;)V
    .locals 2

    const-string/jumbo v0, "tenderedAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    sget-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager;->CASH:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    const-string v1, "TenderSettingsManager.CASH"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/common/Money;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
