.class public final Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$DefaultImpls;
.super Ljava/lang/Object;
.source "SelectMethodScreenWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static abandon(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow;)V
    .locals 0

    check-cast p0, Lcom/squareup/workflow/rx1/Workflow;

    invoke-static {p0}, Lcom/squareup/workflow/rx1/Workflow$DefaultImpls;->abandon(Lcom/squareup/workflow/rx1/Workflow;)V

    return-void
.end method

.method public static toCompletable(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow;)Lrx/Completable;
    .locals 0

    check-cast p0, Lcom/squareup/workflow/rx1/Workflow;

    invoke-static {p0}, Lcom/squareup/workflow/rx1/Workflow$DefaultImpls;->toCompletable(Lcom/squareup/workflow/rx1/Workflow;)Lrx/Completable;

    move-result-object p0

    return-object p0
.end method
