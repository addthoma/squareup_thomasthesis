.class public final Lcom/squareup/tenderpayment/SelectMethod$TextData$Companion;
.super Ljava/lang/Object;
.source "SelectMethodScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethod$TextData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethod$TextData$Companion;",
        "",
        "()V",
        "empty",
        "Lcom/squareup/tenderpayment/SelectMethod$TextData;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 231
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethod$TextData$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final empty()Lcom/squareup/tenderpayment/SelectMethod$TextData;
    .locals 11
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 233
    new-instance v10, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    .line 238
    sget-object v5, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const-string v0, "DEFAULT"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, v10

    .line 233
    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-object v10
.end method
