.class Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;
.super Ljava/lang/Object;
.source "TenderSettingsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/TenderSettingsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TenderSettingsMasterList"
.end annotation


# instance fields
.field private final completeTenderList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;"
        }
    .end annotation
.end field

.field final primaryTenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;"
        }
    .end annotation
.end field

.field final secondaryTenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;)V"
        }
    .end annotation

    .line 533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 534
    iput-object p1, p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;->primaryTenders:Ljava/util/List;

    .line 535
    iput-object p2, p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;->secondaryTenders:Ljava/util/List;

    .line 536
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;->completeTenderList:Ljava/util/List;

    .line 537
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;->completeTenderList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 538
    iget-object p1, p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;->completeTenderList:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method


# virtual methods
.method public contains(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;)Z
    .locals 1

    .line 542
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;->completeTenderList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method
