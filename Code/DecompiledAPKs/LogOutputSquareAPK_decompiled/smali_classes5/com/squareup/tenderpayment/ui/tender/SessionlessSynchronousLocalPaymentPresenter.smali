.class public Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;
.super Ljava/lang/Object;
.source "SessionlessSynchronousLocalPaymentPresenter.java"


# instance fields
.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 26
    iput-object p2, p0, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    .line 27
    iput-object p3, p0, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    return-void
.end method

.method private doCall(Lcom/squareup/payment/tender/BaseTender$Builder;)Z
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 75
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    if-nez v1, :cond_1

    .line 76
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/payment/tender/BaseTender$Builder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 79
    :cond_1
    invoke-virtual {v0, p1}, Lcom/squareup/payment/BillPayment;->addTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object p1

    .line 80
    invoke-virtual {v0, p1}, Lcom/squareup/payment/BillPayment;->shouldCallAuthOnTender(Lcom/squareup/payment/tender/BaseTender;)Z

    move-result p1

    const-string v0, "Expected synchronous AddTendersRequest."

    .line 81
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    return p1
.end method


# virtual methods
.method public startSynchronousCashPayment(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    iget-object p1, p0, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p1}, Lcom/squareup/payment/TenderInEdit;->clearCashTender()Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object p1

    goto :goto_0

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/payment/tender/TenderFactory;->createCash(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object p1

    .line 44
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;->doCall(Lcom/squareup/payment/tender/BaseTender$Builder;)Z

    move-result p1

    return p1
.end method

.method public startSynchronousOtherPayment(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Z
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    .line 64
    invoke-virtual {v0}, Lcom/squareup/payment/tender/TenderFactory;->createOther()Lcom/squareup/payment/tender/OtherTender$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/OtherTender$Builder;->setType(Lcom/squareup/server/account/protos/OtherTenderType;)Lcom/squareup/payment/tender/OtherTender$Builder;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/payment/tender/OtherTender$Builder;->setNote(Ljava/lang/String;)Lcom/squareup/payment/tender/OtherTender$Builder;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/squareup/payment/tender/OtherTender$Builder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object p1

    .line 65
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;->doCall(Lcom/squareup/payment/tender/BaseTender$Builder;)Z

    move-result p1

    return p1
.end method

.method public startSynchronousZeroAmountPayment()Z
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/TenderFactory;->createZeroToReplaceCash()Lcom/squareup/payment/tender/ZeroTender$Builder;

    move-result-object v0

    .line 54
    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;->doCall(Lcom/squareup/payment/tender/BaseTender$Builder;)Z

    move-result v0

    return v0
.end method
