.class final Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$2;
.super Ljava/lang/Object;
.source "PaymentInputHandlers.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;->subscribe(Lio/reactivex/ObservableEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/ui/main/errors/PaymentEvent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "event",
        "Lcom/squareup/ui/main/errors/PaymentEvent;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $emitter:Lio/reactivex/ObservableEmitter;

.field final synthetic $shouldCancelPaymentOnCancelled:Lkotlin/jvm/internal/Ref$BooleanRef;


# direct methods
.method constructor <init>(Lkotlin/jvm/internal/Ref$BooleanRef;Lio/reactivex/ObservableEmitter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$2;->$shouldCancelPaymentOnCancelled:Lkotlin/jvm/internal/Ref$BooleanRef;

    iput-object p2, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$2;->$emitter:Lio/reactivex/ObservableEmitter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/ui/main/errors/PaymentEvent;)V
    .locals 3

    .line 51
    iget-object v0, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$2;->$shouldCancelPaymentOnCancelled:Lkotlin/jvm/internal/Ref$BooleanRef;

    .line 52
    instance-of v1, p1, Lcom/squareup/ui/main/errors/TakeDipPayment;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    instance-of v1, p1, Lcom/squareup/ui/main/errors/TakeTapPayment;

    if-eqz v1, :cond_1

    :goto_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x1

    .line 51
    :goto_1
    iput-boolean v1, v0, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    .line 55
    iget-object v0, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$2;->$emitter:Lio/reactivex/ObservableEmitter;

    new-instance v1, Lcom/squareup/tenderpayment/PaymentInputHandlerOutput$PaymentEventReceived;

    const-string v2, "event"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p1}, Lcom/squareup/tenderpayment/PaymentInputHandlerOutput$PaymentEventReceived;-><init>(Lcom/squareup/ui/main/errors/PaymentEvent;)V

    invoke-interface {v0, v1}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/squareup/ui/main/errors/PaymentEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$2;->accept(Lcom/squareup/ui/main/errors/PaymentEvent;)V

    return-void
.end method
