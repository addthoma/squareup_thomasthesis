.class public final Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;
.super Ljava/lang/Object;
.source "SelectMethodWorkflowRenderer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerCheckoutTenderOptionFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final expirationHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/card/ExpirationHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final quickCashCalculatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/QuickCashCalculator;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderOptionMapProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/QuickCashCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/card/ExpirationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;)V"
        }
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->quickCashCalculatorProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p4, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->expirationHelperProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p5, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p6, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->tenderOptionMapProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p7, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p8, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p9, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->buyerCheckoutTenderOptionFactoryProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p10, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/QuickCashCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/card/ExpirationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;)",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;"
        }
    .end annotation

    .line 82
    new-instance v11, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/money/QuickCashCalculator;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/card/ExpirationHelper;Lcom/squareup/util/Device;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;Lcom/squareup/crm/CustomerManagementSettings;)Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/money/QuickCashCalculator;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/card/ExpirationHelper;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ")",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;"
        }
    .end annotation

    .line 91
    new-instance v11, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;-><init>(Lcom/squareup/money/QuickCashCalculator;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/card/ExpirationHelper;Lcom/squareup/util/Device;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;Lcom/squareup/crm/CustomerManagementSettings;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;
    .locals 11

    .line 70
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->quickCashCalculatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/money/QuickCashCalculator;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->expirationHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/card/ExpirationHelper;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->tenderOptionMapProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->buyerCheckoutTenderOptionFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/crm/CustomerManagementSettings;

    invoke-static/range {v1 .. v10}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->newInstance(Lcom/squareup/money/QuickCashCalculator;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/card/ExpirationHelper;Lcom/squareup/util/Device;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;Lcom/squareup/crm/CustomerManagementSettings;)Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer_Factory;->get()Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    move-result-object v0

    return-object v0
.end method
