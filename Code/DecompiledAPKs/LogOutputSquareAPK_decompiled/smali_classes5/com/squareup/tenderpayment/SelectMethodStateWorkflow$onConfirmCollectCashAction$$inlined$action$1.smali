.class public final Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;
.super Ljava/lang/Object;
.source "WorkflowAction.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onConfirmCollectCashAction(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/protos/common/Money;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowAction.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt$action$2\n+ 2 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt\n+ 3 SelectMethodStateWorkflow.kt\ncom/squareup/tenderpayment/SelectMethodStateWorkflow\n*L\n1#1,211:1\n181#2:212\n890#3,34:213\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0018\u0010\u0004\u001a\u00020\u0005*\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006H\u0016\u00a8\u0006\u0007\u00b8\u0006\u0008"
    }
    d2 = {
        "com/squareup/workflow/WorkflowActionKt$action$2",
        "Lcom/squareup/workflow/WorkflowAction;",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "workflow-core",
        "com/squareup/workflow/WorkflowActionKt$action$$inlined$action$2"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $name$inlined:Ljava/lang/String;

.field final synthetic $startArgs$inlined:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

.field final synthetic $state$inlined:Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

.field final synthetic $tenderedAmount$inlined:Lcom/squareup/protos/common/Money;

.field final synthetic this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->$name$inlined:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->$tenderedAmount$inlined:Lcom/squareup/protos/common/Money;

    iput-object p4, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->$state$inlined:Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    iput-object p5, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->$startArgs$inlined:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            ">;)",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "-",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$getAnalytics$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->COLLECT_CASH_DIALOG_OKAY:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 215
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->$tenderedAmount$inlined:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-static {}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$getCUSTOM_CASH_VALUE$cp()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$getServices$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->$state$inlined:Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getSplitTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->payCashCustom(Lcom/squareup/protos/common/Money;)V

    .line 218
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayCashCustomAmount;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$PayCashCustomAmount;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$getTransaction$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-nez v0, :cond_1

    .line 221
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnCashTenderReceivedEvent;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->$tenderedAmount$inlined:Lcom/squareup/protos/common/Money;

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnCashTenderReceivedEvent;-><init>(Lcom/squareup/protos/common/Money;)V

    .line 222
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {v1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$getServices$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;

    move-result-object v1

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;

    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->$state$inlined:Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    invoke-virtual {v2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getSplitTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->completeTender(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object v0

    .line 224
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {v1, v0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$toTenderPaymentResult(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)Lcom/squareup/tenderpayment/TenderPaymentResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$getTenderInEdit$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 232
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$getTenderInEdit$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    .line 234
    :cond_2
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->$startArgs$inlined:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    invoke-static {v0, v1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$getTenderAmount(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 235
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {v1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$getTenderInEdit$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {v2}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$getTenderFactory$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/payment/tender/TenderFactory;

    move-result-object v2

    invoke-virtual {v2, v0, v0}, Lcom/squareup/payment/tender/TenderFactory;->createCash(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-interface {v1, v0}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 236
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$getCompleter$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/tenderpayment/TenderCompleter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->$tenderedAmount$inlined:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, v1}, Lcom/squareup/tenderpayment/TenderCompleter;->payCashTender(Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object v0

    .line 238
    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_SPLIT_TENDER:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    if-ne v0, v1, :cond_3

    .line 240
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$getChangeHudToaster$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/tenderpayment/ChangeHudToaster;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/tenderpayment/ChangeHudToaster;->clear()V

    .line 241
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$QuickCashReceived;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$QuickCashReceived;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 243
    :cond_3
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    const-string v2, "completeTenderResult"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$toTenderPaymentResult(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)Lcom/squareup/tenderpayment/TenderPaymentResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WorkflowAction("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->$name$inlined:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
