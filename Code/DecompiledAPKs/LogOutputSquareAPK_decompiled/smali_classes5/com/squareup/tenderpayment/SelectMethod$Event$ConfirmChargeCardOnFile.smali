.class public final Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;
.super Lcom/squareup/tenderpayment/SelectMethod$Event;
.source "SelectMethodScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethod$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConfirmChargeCardOnFile"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;",
        "Lcom/squareup/tenderpayment/SelectMethod$Event;",
        "tenderedAmount",
        "Lcom/squareup/protos/common/Money;",
        "instrumentToken",
        "",
        "cardNameAndNumber",
        "(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;)V",
        "getCardNameAndNumber",
        "()Ljava/lang/String;",
        "getInstrumentToken",
        "getTenderedAmount",
        "()Lcom/squareup/protos/common/Money;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardNameAndNumber:Ljava/lang/String;

.field private final instrumentToken:Ljava/lang/String;

.field private final tenderedAmount:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "tenderedAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instrumentToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardNameAndNumber"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 100
    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/SelectMethod$Event;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;->tenderedAmount:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;->instrumentToken:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;->cardNameAndNumber:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getCardNameAndNumber()Ljava/lang/String;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;->cardNameAndNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final getInstrumentToken()Ljava/lang/String;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;->instrumentToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getTenderedAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;->tenderedAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method
