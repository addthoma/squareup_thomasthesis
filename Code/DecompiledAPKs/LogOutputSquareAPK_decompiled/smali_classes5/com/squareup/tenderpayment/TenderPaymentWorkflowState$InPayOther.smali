.class public final Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;
.super Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;
.source "TenderPaymentWorkflowState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InPayOther"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\u0019\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u000fH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;",
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;",
        "input",
        "Lcom/squareup/checkoutflow/payother/PayOtherInput;",
        "restartSelectMethodWorkflowData",
        "Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;",
        "(Lcom/squareup/checkoutflow/payother/PayOtherInput;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;)V",
        "getInput",
        "()Lcom/squareup/checkoutflow/payother/PayOtherInput;",
        "getRestartSelectMethodWorkflowData",
        "()Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final input:Lcom/squareup/checkoutflow/payother/PayOtherInput;

.field private final restartSelectMethodWorkflowData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther$Creator;

    invoke-direct {v0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther$Creator;-><init>()V

    sput-object v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/checkoutflow/payother/PayOtherInput;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;)V
    .locals 1

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restartSelectMethodWorkflowData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->input:Lcom/squareup/checkoutflow/payother/PayOtherInput;

    iput-object p2, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->restartSelectMethodWorkflowData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;Lcom/squareup/checkoutflow/payother/PayOtherInput;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;ILjava/lang/Object;)Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->input:Lcom/squareup/checkoutflow/payother/PayOtherInput;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->restartSelectMethodWorkflowData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->copy(Lcom/squareup/checkoutflow/payother/PayOtherInput;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;)Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/checkoutflow/payother/PayOtherInput;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->input:Lcom/squareup/checkoutflow/payother/PayOtherInput;

    return-object v0
.end method

.method public final component2()Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->restartSelectMethodWorkflowData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    return-object v0
.end method

.method public final copy(Lcom/squareup/checkoutflow/payother/PayOtherInput;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;)Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;
    .locals 1

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restartSelectMethodWorkflowData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;

    invoke-direct {v0, p1, p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;-><init>(Lcom/squareup/checkoutflow/payother/PayOtherInput;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->input:Lcom/squareup/checkoutflow/payother/PayOtherInput;

    iget-object v1, p1, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->input:Lcom/squareup/checkoutflow/payother/PayOtherInput;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->restartSelectMethodWorkflowData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    iget-object p1, p1, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->restartSelectMethodWorkflowData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getInput()Lcom/squareup/checkoutflow/payother/PayOtherInput;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->input:Lcom/squareup/checkoutflow/payother/PayOtherInput;

    return-object v0
.end method

.method public final getRestartSelectMethodWorkflowData()Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->restartSelectMethodWorkflowData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->input:Lcom/squareup/checkoutflow/payother/PayOtherInput;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->restartSelectMethodWorkflowData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InPayOther(input="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->input:Lcom/squareup/checkoutflow/payother/PayOtherInput;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", restartSelectMethodWorkflowData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->restartSelectMethodWorkflowData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->input:Lcom/squareup/checkoutflow/payother/PayOtherInput;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->restartSelectMethodWorkflowData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
