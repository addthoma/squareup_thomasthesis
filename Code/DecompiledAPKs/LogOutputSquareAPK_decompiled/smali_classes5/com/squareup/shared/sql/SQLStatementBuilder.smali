.class public Lcom/squareup/shared/sql/SQLStatementBuilder;
.super Ljava/lang/Object;
.source "SQLStatementBuilder.java"


# instance fields
.field private index:I

.field private final statement:Lcom/squareup/shared/sql/SQLStatement;


# direct methods
.method private constructor <init>(Lcom/squareup/shared/sql/SQLStatement;)V
    .locals 0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->statement:Lcom/squareup/shared/sql/SQLStatement;

    return-void
.end method

.method public static forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;
    .locals 0

    .line 20
    invoke-interface {p0, p1}, Lcom/squareup/shared/sql/SQLDatabase;->compileStatement(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatement;

    move-result-object p0

    .line 21
    new-instance p1, Lcom/squareup/shared/sql/SQLStatementBuilder;

    invoke-direct {p1, p0}, Lcom/squareup/shared/sql/SQLStatementBuilder;-><init>(Lcom/squareup/shared/sql/SQLStatement;)V

    return-object p1
.end method

.method private nonNullBinding(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x1

    if-nez p1, :cond_0

    .line 70
    iget-object p1, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->statement:Lcom/squareup/shared/sql/SQLStatement;

    iget v1, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->index:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->index:I

    invoke-interface {p1, v1}, Lcom/squareup/shared/sql/SQLStatement;->bindNull(I)V

    const/4 p1, 0x0

    return p1

    :cond_0
    return v0
.end method


# virtual methods
.method public bindBlob([B)Lcom/squareup/shared/sql/SQLStatementBuilder;
    .locals 2

    .line 46
    invoke-direct {p0, p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->nonNullBinding(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->statement:Lcom/squareup/shared/sql/SQLStatement;

    iget v1, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->index:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->index:I

    invoke-interface {v0, p1, v1}, Lcom/squareup/shared/sql/SQLStatement;->bindBlob([BI)V

    :cond_0
    return-object p0
.end method

.method public bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;
    .locals 3

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->nonNullBinding(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->statement:Lcom/squareup/shared/sql/SQLStatement;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v1, p1

    iget p1, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->index:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->index:I

    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/shared/sql/SQLStatement;->bindLong(JI)V

    :cond_0
    return-object p0
.end method

.method public bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;
    .locals 3

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->nonNullBinding(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->statement:Lcom/squareup/shared/sql/SQLStatement;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget p1, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->index:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->index:I

    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/shared/sql/SQLStatement;->bindLong(JI)V

    :cond_0
    return-object p0
.end method

.method public bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;
    .locals 2

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->nonNullBinding(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->statement:Lcom/squareup/shared/sql/SQLStatement;

    iget v1, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->index:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->index:I

    invoke-interface {v0, p1, v1}, Lcom/squareup/shared/sql/SQLStatement;->bindString(Ljava/lang/String;I)V

    :cond_0
    return-object p0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 61
    instance-of v0, p1, Lcom/squareup/shared/sql/SQLStatementBuilder;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 62
    check-cast p1, Lcom/squareup/shared/sql/SQLStatementBuilder;

    .line 63
    iget-object v0, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->statement:Lcom/squareup/shared/sql/SQLStatement;

    iget-object p1, p1, Lcom/squareup/shared/sql/SQLStatementBuilder;->statement:Lcom/squareup/shared/sql/SQLStatement;

    if-ne v0, p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public execute()V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->statement:Lcom/squareup/shared/sql/SQLStatement;

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLStatement;->execute()V

    return-void
.end method

.method public executeUpdateDelete()I
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/shared/sql/SQLStatementBuilder;->statement:Lcom/squareup/shared/sql/SQLStatement;

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLStatement;->executeUpdateDelete()I

    move-result v0

    return v0
.end method
