.class public Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;
.super Ljava/lang/Object;
.source "ItemizationDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;,
        Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;
    }
.end annotation


# instance fields
.field private final addedAt:Ljava/util/Date;

.field private applicableRuleIDs:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private backingType:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

.field private final blacklistedDiscounts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final categoryID:Ljava/lang/String;

.field private final clientServerIds:Lcom/squareup/shared/pricing/models/ClientServerIds;

.field private final existingAutoDiscounts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final fractional:Z

.field private final itemID:Ljava/lang/String;

.field private quantity:Ljava/math/BigDecimal;

.field private final unitPrice:Lcom/squareup/shared/pricing/models/MonetaryAmount;

.field private final variationID:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/models/ClientServerIds;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/shared/pricing/models/MonetaryAmount;Ljava/util/Date;Ljava/util/Set;ZLjava/util/Set;Ljava/util/Set;Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/math/BigDecimal;",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/models/MonetaryAmount;",
            "Ljava/util/Date;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;Z",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;",
            ")V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->quantity:Ljava/math/BigDecimal;

    .line 41
    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->clientServerIds:Lcom/squareup/shared/pricing/models/ClientServerIds;

    .line 42
    iput-object p3, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->variationID:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->itemID:Ljava/lang/String;

    .line 44
    iput-object p5, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->categoryID:Ljava/lang/String;

    .line 45
    iput-object p6, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->unitPrice:Lcom/squareup/shared/pricing/models/MonetaryAmount;

    .line 46
    iput-object p7, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->addedAt:Ljava/util/Date;

    .line 47
    iput-object p8, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->applicableRuleIDs:Ljava/util/Set;

    .line 48
    iput-boolean p9, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->fractional:Z

    .line 49
    iput-object p10, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->existingAutoDiscounts:Ljava/util/Set;

    .line 50
    iput-object p11, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->blacklistedDiscounts:Ljava/util/Set;

    .line 51
    iput-object p12, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->backingType:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    return-void
.end method

.method synthetic constructor <init>(Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/models/ClientServerIds;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/shared/pricing/models/MonetaryAmount;Ljava/util/Date;Ljava/util/Set;ZLjava/util/Set;Ljava/util/Set;Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$1;)V
    .locals 0

    .line 13
    invoke-direct/range {p0 .. p12}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;-><init>(Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/models/ClientServerIds;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/shared/pricing/models/MonetaryAmount;Ljava/util/Date;Ljava/util/Set;ZLjava/util/Set;Ljava/util/Set;Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;)V

    return-void
.end method

.method public static earliestAdded(Ljava/util/List;)Ljava/util/Date;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;",
            ">;)",
            "Ljava/util/Date;"
        }
    .end annotation

    .line 129
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    if-eqz v0, :cond_1

    .line 130
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getAddedAt()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 131
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getAddedAt()Ljava/util/Date;

    move-result-object v0

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static latestAdded(Ljava/util/List;)Ljava/util/Date;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;",
            ">;)",
            "Ljava/util/Date;"
        }
    .end annotation

    .line 142
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    if-eqz v0, :cond_1

    .line 143
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getAddedAt()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 144
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getAddedAt()Ljava/util/Date;

    move-result-object v0

    goto :goto_0

    :cond_2
    return-object v0
.end method


# virtual methods
.method public addApplicableRule(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)V
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->applicableRuleIDs:Ljava/util/Set;

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public decrement(Ljava/math/BigDecimal;)V
    .locals 3

    .line 98
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->quantity:Ljava/math/BigDecimal;

    return-void

    .line 99
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 100
    invoke-virtual {p1}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    const/4 p1, 0x1

    iget-object v2, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->quantity:Ljava/math/BigDecimal;

    .line 101
    invoke-virtual {v2}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, p1

    const-string p1, "Cannot decrement %s, only %s available"

    .line 100
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getAddedAt()Ljava/util/Date;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->addedAt:Ljava/util/Date;

    return-object v0
.end method

.method public getBackingType()Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->backingType:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    return-object v0
.end method

.method public getBlacklistedDiscounts()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 117
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->blacklistedDiscounts:Ljava/util/Set;

    return-object v0
.end method

.method public getCategoryID()Ljava/lang/String;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->categoryID:Ljava/lang/String;

    return-object v0
.end method

.method public getClientServerIds()Lcom/squareup/shared/pricing/models/ClientServerIds;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->clientServerIds:Lcom/squareup/shared/pricing/models/ClientServerIds;

    return-object v0
.end method

.method public getExistingAutoDiscounts()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->existingAutoDiscounts:Ljava/util/Set;

    return-object v0
.end method

.method public getFractional()Z
    .locals 1

    .line 110
    iget-boolean v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->fractional:Z

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->clientServerIds:Lcom/squareup/shared/pricing/models/ClientServerIds;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/models/ClientServerIds;->getServerId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->clientServerIds:Lcom/squareup/shared/pricing/models/ClientServerIds;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/models/ClientServerIds;->getServerId()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->clientServerIds:Lcom/squareup/shared/pricing/models/ClientServerIds;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/models/ClientServerIds;->getClientId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItemID()Ljava/lang/String;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->itemID:Ljava/lang/String;

    return-object v0
.end method

.method public getQuantity()Ljava/math/BigDecimal;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->quantity:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getUnitPrice()Lcom/squareup/shared/pricing/models/MonetaryAmount;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->unitPrice:Lcom/squareup/shared/pricing/models/MonetaryAmount;

    return-object v0
.end method

.method public getVariationID()Ljava/lang/String;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->variationID:Ljava/lang/String;

    return-object v0
.end method

.method public increment(Ljava/math/BigDecimal;)V
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->quantity:Ljava/math/BigDecimal;

    return-void
.end method

.method public isApplicable(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Z
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->applicableRuleIDs:Ljava/util/Set;

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method
