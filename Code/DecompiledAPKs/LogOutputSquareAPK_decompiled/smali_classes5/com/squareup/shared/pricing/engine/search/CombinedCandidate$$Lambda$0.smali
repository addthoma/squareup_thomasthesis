.class final synthetic Lcom/squareup/shared/pricing/engine/search/CombinedCandidate$$Lambda$0;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;


# static fields
.field static final $instance:Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate$$Lambda$0;

    invoke-direct {v0}, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate$$Lambda$0;-><init>()V

    sput-object v0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate$$Lambda$0;->$instance:Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public perform(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Ljava/math/BigDecimal;
    .locals 0

    invoke-virtual {p1, p2}, Ljava/math/BigDecimal;->max(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1
.end method
