.class public interface abstract Lcom/squareup/shared/pricing/engine/PricingEngine;
.super Ljava/lang/Object;
.source "PricingEngine.java"


# virtual methods
.method public abstract applyRules(Ljava/util/TimeZone;Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;Ljava/util/List;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TimeZone;",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;",
            ">;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "Lcom/squareup/shared/pricing/engine/PricingEngineResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract applyRules(Ljava/util/TimeZone;Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;Ljava/util/List;Ljava/util/Set;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TimeZone;",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "Lcom/squareup/shared/pricing/engine/PricingEngineResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract applyRules(Ljava/util/TimeZone;Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;Ljava/util/List;Ljava/util/Set;ZLcom/squareup/shared/catalog/CatalogCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TimeZone;",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "Lcom/squareup/shared/pricing/engine/PricingEngineResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract blacklist(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract clearBlacklist()V
.end method
