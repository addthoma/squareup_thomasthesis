.class final synthetic Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$$Lambda$0;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ProductSetVisitor;


# instance fields
.field private final arg$1:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final arg$2:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method constructor <init>(Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$$Lambda$0;->arg$1:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$$Lambda$0;->arg$2:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method


# virtual methods
.method public visit(Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;)V
    .locals 2

    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$$Lambda$0;->arg$1:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$$Lambda$0;->arg$2:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-static {v0, v1, p1}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->lambda$recordRuleApplied$0$MetricsInProgress(Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;)V

    return-void
.end method
