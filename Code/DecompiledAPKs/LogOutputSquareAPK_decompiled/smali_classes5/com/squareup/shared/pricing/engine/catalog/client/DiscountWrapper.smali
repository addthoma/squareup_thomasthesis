.class public Lcom/squareup/shared/pricing/engine/catalog/client/DiscountWrapper;
.super Ljava/lang/Object;
.source "DiscountWrapper.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;


# instance fields
.field private final catalogDiscount:Lcom/squareup/shared/catalog/models/CatalogDiscount;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/catalog/client/DiscountWrapper;->catalogDiscount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    return-void
.end method


# virtual methods
.method public getAmount()Lcom/squareup/shared/pricing/models/MonetaryAmount;
    .locals 5

    .line 18
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/DiscountWrapper;->catalogDiscount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 24
    :cond_0
    iget-object v2, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    if-eqz v2, :cond_1

    .line 25
    iget-object v1, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v1}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v1

    .line 28
    :cond_1
    new-instance v2, Lcom/squareup/shared/pricing/models/MonetaryAmount;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-direct {v2, v3, v4, v1}, Lcom/squareup/shared/pricing/models/MonetaryAmount;-><init>(JLjava/util/Currency;)V

    return-object v2
.end method

.method public getMaximumAmount()Lcom/squareup/shared/pricing/models/MonetaryAmount;
    .locals 5

    .line 32
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/DiscountWrapper;->catalogDiscount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getMaximumAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 38
    :cond_0
    iget-object v2, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    if-eqz v2, :cond_1

    .line 39
    iget-object v1, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v1}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v1

    .line 42
    :cond_1
    new-instance v2, Lcom/squareup/shared/pricing/models/MonetaryAmount;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-direct {v2, v3, v4, v1}, Lcom/squareup/shared/pricing/models/MonetaryAmount;-><init>(JLjava/util/Currency;)V

    return-object v2
.end method

.method public getPercentage()Ljava/lang/String;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/DiscountWrapper;->catalogDiscount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getPercentage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isPercentageDiscount()Z
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/DiscountWrapper;->catalogDiscount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->isPercentageDiscount()Z

    move-result v0

    return v0
.end method
