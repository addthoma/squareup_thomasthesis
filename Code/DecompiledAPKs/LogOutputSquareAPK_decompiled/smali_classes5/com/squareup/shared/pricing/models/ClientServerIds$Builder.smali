.class public Lcom/squareup/shared/pricing/models/ClientServerIds$Builder;
.super Ljava/lang/Object;
.source "ClientServerIds.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/pricing/models/ClientServerIds;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field clientId:Ljava/lang/String;

.field serverId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/pricing/models/ClientServerIds;
    .locals 3

    .line 104
    new-instance v0, Lcom/squareup/shared/pricing/models/ClientServerIds;

    iget-object v1, p0, Lcom/squareup/shared/pricing/models/ClientServerIds$Builder;->clientId:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/shared/pricing/models/ClientServerIds$Builder;->serverId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/shared/pricing/models/ClientServerIds;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public clientId(Ljava/lang/String;)Lcom/squareup/shared/pricing/models/ClientServerIds$Builder;
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/shared/pricing/models/ClientServerIds$Builder;->clientId:Ljava/lang/String;

    return-object p0
.end method

.method public serverId(Ljava/lang/String;)Lcom/squareup/shared/pricing/models/ClientServerIds$Builder;
    .locals 0

    .line 94
    iput-object p1, p0, Lcom/squareup/shared/pricing/models/ClientServerIds$Builder;->serverId:Ljava/lang/String;

    return-object p0
.end method
