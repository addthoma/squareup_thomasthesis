.class public Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl;
.super Ljava/lang/Object;
.source "RuleSetImpl.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/rules/RuleSet;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;
    }
.end annotation


# instance fields
.field private final end:Ljava/util/Date;

.field private final rules:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            ">;"
        }
    .end annotation
.end field

.field private final start:Ljava/util/Date;


# direct methods
.method constructor <init>(Ljava/util/Date;Ljava/util/Date;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl;->start:Ljava/util/Date;

    .line 17
    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl;->end:Ljava/util/Date;

    .line 18
    iput-object p3, p0, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl;->rules:Ljava/util/Set;

    return-void
.end method

.method public static newBuilder()Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;

    invoke-direct {v0}, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getEnd()Ljava/util/Date;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl;->end:Ljava/util/Date;

    return-object v0
.end method

.method public getRules()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            ">;"
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl;->rules:Ljava/util/Set;

    return-object v0
.end method

.method public getStart()Ljava/util/Date;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl;->start:Ljava/util/Date;

    return-object v0
.end method
