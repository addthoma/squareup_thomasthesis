.class public Lcom/squareup/shared/pricing/models/ClientServerIds;
.super Ljava/lang/Object;
.source "ClientServerIds.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/pricing/models/ClientServerIds$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/squareup/shared/pricing/models/ClientServerIds;",
        ">;"
    }
.end annotation


# instance fields
.field private clientId:Ljava/lang/String;

.field private serverId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/shared/pricing/models/ClientServerIds;->clientId:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lcom/squareup/shared/pricing/models/ClientServerIds;->serverId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/squareup/shared/pricing/models/ClientServerIds;)I
    .locals 3

    .line 56
    invoke-static {}, Lcom/squareup/shared/catalog/utils/ChainComparison;->start()Lcom/squareup/shared/catalog/utils/ChainComparison;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/pricing/models/ClientServerIds;->clientId:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/shared/pricing/models/ClientServerIds;->clientId:Ljava/lang/String;

    .line 57
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/ChainComparison;->compareWithNull(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/shared/catalog/utils/ChainComparison;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/pricing/models/ClientServerIds;->serverId:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/shared/pricing/models/ClientServerIds;->serverId:Ljava/lang/String;

    .line 58
    invoke-virtual {v0, v1, p1}, Lcom/squareup/shared/catalog/utils/ChainComparison;->compareWithNull(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/shared/catalog/utils/ChainComparison;

    move-result-object p1

    .line 59
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/utils/ChainComparison;->result()I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/shared/pricing/models/ClientServerIds;

    invoke-virtual {p0, p1}, Lcom/squareup/shared/pricing/models/ClientServerIds;->compareTo(Lcom/squareup/shared/pricing/models/ClientServerIds;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 49
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 50
    :cond_1
    check-cast p1, Lcom/squareup/shared/pricing/models/ClientServerIds;

    .line 51
    iget-object v2, p0, Lcom/squareup/shared/pricing/models/ClientServerIds;->clientId:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/shared/pricing/models/ClientServerIds;->clientId:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/shared/pricing/models/ClientServerIds;->serverId:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/shared/pricing/models/ClientServerIds;->serverId:Ljava/lang/String;

    .line 52
    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/shared/pricing/models/ClientServerIds;->clientId:Ljava/lang/String;

    return-object v0
.end method

.method public getServerId()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/shared/pricing/models/ClientServerIds;->serverId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 63
    iget-object v1, p0, Lcom/squareup/shared/pricing/models/ClientServerIds;->clientId:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/pricing/models/ClientServerIds;->serverId:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    iget-object v1, p0, Lcom/squareup/shared/pricing/models/ClientServerIds;->clientId:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", clientId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/shared/pricing/models/ClientServerIds;->clientId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    :cond_0
    iget-object v1, p0, Lcom/squareup/shared/pricing/models/ClientServerIds;->serverId:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", serverId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/shared/pricing/models/ClientServerIds;->serverId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "IdPair{"

    .line 44
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
