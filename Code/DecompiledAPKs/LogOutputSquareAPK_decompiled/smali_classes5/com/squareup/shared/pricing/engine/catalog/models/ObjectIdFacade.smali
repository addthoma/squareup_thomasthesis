.class public interface abstract Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade;
.super Ljava/lang/Object;
.source "ObjectIdFacade.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;
    }
.end annotation


# static fields
.field public static final DEFAULT_ID:Ljava/lang/String; = ""


# virtual methods
.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getType()Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;
.end method
