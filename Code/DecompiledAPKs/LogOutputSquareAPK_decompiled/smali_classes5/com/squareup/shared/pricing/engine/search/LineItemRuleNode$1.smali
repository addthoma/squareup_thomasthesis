.class synthetic Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode$1;
.super Ljava/lang/Object;
.source "LineItemRuleNode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$shared$pricing$engine$catalog$models$PricingRuleFacade$ExcludeStrategy:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 78
    invoke-static {}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ExcludeStrategy;->values()[Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ExcludeStrategy;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode$1;->$SwitchMap$com$squareup$shared$pricing$engine$catalog$models$PricingRuleFacade$ExcludeStrategy:[I

    :try_start_0
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode$1;->$SwitchMap$com$squareup$shared$pricing$engine$catalog$models$PricingRuleFacade$ExcludeStrategy:[I

    sget-object v1, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ExcludeStrategy;->LEAST_EXPENSIVE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ExcludeStrategy;

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ExcludeStrategy;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode$1;->$SwitchMap$com$squareup$shared$pricing$engine$catalog$models$PricingRuleFacade$ExcludeStrategy:[I

    sget-object v1, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ExcludeStrategy;->MOST_EXPENSIVE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ExcludeStrategy;

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ExcludeStrategy;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method
