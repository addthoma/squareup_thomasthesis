.class public Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;
.super Ljava/lang/Object;
.source "RecurrenceRule.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/ical/rrule/RecurrenceRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WeekDayNum"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;",
        ">;"
    }
.end annotation


# instance fields
.field private final ordwk:Ljava/lang/Integer;

.field private final weekDay:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;


# direct methods
.method public constructor <init>(Ljava/lang/Integer;Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->ordwk:Ljava/lang/Integer;

    .line 66
    iput-object p2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->weekDay:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    return-void
.end method

.method public static newBuilder()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;
    .locals 1

    .line 98
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    invoke-direct {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public compareTo(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;)I
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->ordwk:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v1, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->ordwk:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 122
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object p1, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->ordwk:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    :goto_0
    sub-int/2addr v0, p1

    return v0

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->weekDay:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    invoke-static {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->access$000(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)I

    move-result v0

    iget-object p1, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->weekDay:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    invoke-static {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->access$000(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)I

    move-result p1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    invoke-virtual {p0, p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->compareTo(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 80
    :cond_0
    instance-of v1, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 82
    :cond_1
    check-cast p1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    .line 84
    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->ordwk:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v3, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->ordwk:Ljava/lang/Integer;

    invoke-virtual {v1, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    :cond_2
    iget-object v1, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->ordwk:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    :goto_0
    return v2

    .line 85
    :cond_3
    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->weekDay:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    iget-object p1, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->weekDay:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    if-eq v1, p1, :cond_4

    return v2

    :cond_4
    return v0
.end method

.method public getOrdwk()Ljava/lang/Integer;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->ordwk:Ljava/lang/Integer;

    return-object v0
.end method

.method public getWeekDay()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->weekDay:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .line 92
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->ordwk:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 93
    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->weekDay:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method
