.class public final Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogTicketTemplate.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/TicketTemplate;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method

.method public static create(Lcom/squareup/api/items/TicketTemplate;)Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;
    .locals 3

    .line 26
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_TEMPLATE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    iget-object v2, p0, Lcom/squareup/api/items/TicketTemplate;->id:Ljava/lang/String;

    invoke-virtual {v1, v2, p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapObjectMessage(Ljava/lang/String;Lcom/squareup/wire/Message;)Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public static create(Ljava/lang/String;ILcom/squareup/api/sync/ObjectId;)Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_TEMPLATE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    .line 22
    invoke-static {v0, p0, p1, p2}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->doCreate(Lcom/squareup/api/sync/ObjectWrapper$Builder;Ljava/lang/String;ILcom/squareup/api/sync/ObjectId;)Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    move-result-object p0

    return-object p0
.end method

.method public static createForTest(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;
    .locals 1

    .line 32
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_TEMPLATE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 33
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 34
    invoke-virtual {v0, p3}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p3

    .line 35
    iget-object p3, p3, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    invoke-static {p0, p1, p2, p3}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->doCreate(Lcom/squareup/api/sync/ObjectWrapper$Builder;Ljava/lang/String;ILcom/squareup/api/sync/ObjectId;)Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    move-result-object p0

    return-object p0
.end method

.method private static doCreate(Lcom/squareup/api/sync/ObjectWrapper$Builder;Ljava/lang/String;ILcom/squareup/api/sync/ObjectId;)Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;
    .locals 3

    .line 40
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    new-instance v1, Lcom/squareup/api/items/TicketTemplate$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/TicketTemplate$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v2, v2, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    .line 41
    invoke-virtual {v1, v2}, Lcom/squareup/api/items/TicketTemplate$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object v1

    .line 42
    invoke-virtual {v1, p1}, Lcom/squareup/api/items/TicketTemplate$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object p1

    .line 43
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/api/items/TicketTemplate$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object p1

    .line 44
    invoke-virtual {p1, p3}, Lcom/squareup/api/items/TicketTemplate$Builder;->ticket_group(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lcom/squareup/api/items/TicketTemplate$Builder;->build()Lcom/squareup/api/items/TicketTemplate;

    move-result-object p1

    .line 40
    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->ticket_template(Lcom/squareup/api/items/TicketTemplate;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 45
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 2

    .line 57
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketTemplate;

    iget-object v0, v0, Lcom/squareup/api/items/TicketTemplate;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getOrdinal()I
    .locals 2

    .line 61
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketTemplate;

    iget-object v0, v0, Lcom/squareup/api/items/TicketTemplate;->ordinal:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/TicketTemplate;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getReferentTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;"
        }
    .end annotation

    .line 75
    sget-object v0, Lcom/squareup/api/items/Type;->TICKET_GROUP:Lcom/squareup/api/items/Type;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRelations()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 71
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_TICKET_TEMPLATE_TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getTicketGroupId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getSortText()Ljava/lang/String;
    .locals 1

    .line 53
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getOrdinal()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/Queries;->fixedLengthOrdinal(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTicketGroupId()Ljava/lang/String;
    .locals 2

    .line 65
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketTemplate;

    .line 66
    iget-object v1, v0, Lcom/squareup/api/items/TicketTemplate;->ticket_group:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/api/items/TicketTemplate;->ticket_group:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 67
    iget-object v0, v0, Lcom/squareup/api/items/TicketTemplate;->ticket_group:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    .line 80
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getOrdinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getTicketGroupId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "%s{id=%s, name=%s, ordinal=%d, ticket_group_id=%s}"

    .line 79
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
