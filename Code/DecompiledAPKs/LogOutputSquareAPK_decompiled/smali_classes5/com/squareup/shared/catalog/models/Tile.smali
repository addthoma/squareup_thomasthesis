.class public final Lcom/squareup/shared/catalog/models/Tile;
.super Ljava/lang/Object;
.source "Tile.java"


# instance fields
.field public final object:Lcom/squareup/shared/catalog/models/CatalogObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;"
        }
    .end annotation
.end field

.field public final pageMembership:Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

.field public final price:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;Lcom/squareup/shared/catalog/models/CatalogObject;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 15
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/shared/catalog/models/Tile;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;Lcom/squareup/shared/catalog/models/CatalogObject;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;Lcom/squareup/shared/catalog/models/CatalogObject;Lcom/squareup/protos/common/Money;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;",
            "Lcom/squareup/protos/common/Money;",
            ")V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/shared/catalog/models/Tile;->pageMembership:Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    .line 20
    iput-object p2, p0, Lcom/squareup/shared/catalog/models/Tile;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 21
    iput-object p3, p0, Lcom/squareup/shared/catalog/models/Tile;->price:Lcom/squareup/protos/common/Money;

    return-void
.end method
