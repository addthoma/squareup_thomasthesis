.class public final enum Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;
.super Ljava/lang/Enum;
.source "PricingRuleLookupTableReader.java"

# interfaces
.implements Lcom/squareup/shared/catalog/HasQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;",
        ">;",
        "Lcom/squareup/shared/catalog/HasQuery;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;

.field public static final enum SEARCH_PRICING_RULES_BY_ACTIVE_DATES:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;


# instance fields
.field private final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 23
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;

    const-string v1, "SELECT {pricing_rule_id} FROM {pricing_rule_lookup_table} WHERE ({starts_at} <= ? OR {starts_at} IS NULL) AND ({ends_at} > ? OR {ends_at} IS NULL)"

    .line 24
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "pricing_rule_id"

    .line 28
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "pricing_rule_lookup_table"

    const-string v3, "pricing_rule_lookup"

    .line 29
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "starts_at"

    .line 30
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "ends_at"

    .line 31
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 32
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 33
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "SEARCH_PRICING_RULES_BY_ACTIVE_DATES"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;->SEARCH_PRICING_RULES_BY_ACTIVE_DATES:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;

    .line 22
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;->SEARCH_PRICING_RULES_BY_ACTIVE_DATES:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput-object p3, p0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;->query:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;
    .locals 1

    .line 22
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;
    .locals 1

    .line 22
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;

    return-object v0
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;->query:Ljava/lang/String;

    return-object v0
.end method
