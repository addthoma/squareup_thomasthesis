.class public final Lcom/squareup/shared/catalog/models/CatalogDiscount;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogDiscount.java"

# interfaces
.implements Lcom/squareup/shared/catalog/models/SupportsSearch;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/Discount;",
        ">;",
        "Lcom/squareup/shared/catalog/models/SupportsSearch;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 67
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method

.method public static create(Ljava/lang/String;Lcom/squareup/api/items/Discount$DiscountType;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogDiscount;
    .locals 1

    const/4 v0, 0x0

    .line 30
    invoke-static {v0, p0, p1, p2, p3}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->createForTesting(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$DiscountType;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogDiscount;

    move-result-object p0

    return-object p0
.end method

.method public static createCompForTesting(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogDiscount;
    .locals 0

    .line 40
    invoke-static {p0, p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->createCompForTesting(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogDiscount;

    move-result-object p0

    return-object p0
.end method

.method public static createCompForTesting(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogDiscount;
    .locals 6

    .line 44
    sget-object v2, Lcom/squareup/api/items/Discount$DiscountType;->FIXED:Lcom/squareup/api/items/Discount$DiscountType;

    sget-object v5, Lcom/squareup/api/items/Discount$ApplicationMethod;->COMP:Lcom/squareup/api/items/Discount$ApplicationMethod;

    const-string v3, "100"

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->createForTesting(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$DiscountType;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/shared/catalog/models/CatalogDiscount;

    move-result-object p0

    return-object p0
.end method

.method public static createForTesting(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$DiscountType;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogDiscount;
    .locals 6

    .line 36
    sget-object v5, Lcom/squareup/api/items/Discount$ApplicationMethod;->MANUALLY_APPLIED:Lcom/squareup/api/items/Discount$ApplicationMethod;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->createForTesting(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$DiscountType;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/shared/catalog/models/CatalogDiscount;

    move-result-object p0

    return-object p0
.end method

.method private static createForTesting(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$DiscountType;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/shared/catalog/models/CatalogDiscount;
    .locals 2

    .line 56
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 57
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    new-instance v1, Lcom/squareup/api/items/Discount$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/Discount$Builder;-><init>()V

    invoke-virtual {v1, p1}, Lcom/squareup/api/items/Discount$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    .line 58
    invoke-virtual {p1, v1}, Lcom/squareup/api/items/Discount$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object p1

    .line 59
    invoke-virtual {p1, p2}, Lcom/squareup/api/items/Discount$Builder;->discount_type(Lcom/squareup/api/items/Discount$DiscountType;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object p1

    .line 60
    invoke-virtual {p1, p3}, Lcom/squareup/api/items/Discount$Builder;->percentage(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object p1

    .line 61
    invoke-static {p4}, Lcom/squareup/shared/catalog/utils/Dineros;->toDineroOrNull(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/api/items/Discount$Builder;->amount(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object p1

    .line 62
    invoke-virtual {p1, p5}, Lcom/squareup/api/items/Discount$Builder;->application_method(Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object p1

    .line 63
    invoke-virtual {p1}, Lcom/squareup/api/items/Discount$Builder;->build()Lcom/squareup/api/items/Discount;

    move-result-object p1

    .line 57
    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->discount(Lcom/squareup/api/items/Discount;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 63
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public static fromDiscountForTesting(Lcom/squareup/api/items/Discount;)Lcom/squareup/shared/catalog/models/CatalogDiscount;
    .locals 1

    .line 49
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    .line 50
    invoke-virtual {v0, p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->discount(Lcom/squareup/api/items/Discount;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 51
    new-instance p0, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object p0
.end method


# virtual methods
.method public getAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 126
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 127
    :cond_0
    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getApplicationMethod()Lcom/squareup/api/items/Discount$ApplicationMethod;
    .locals 2

    .line 150
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    sget-object v1, Lcom/squareup/api/items/Discount$ApplicationMethod;->MANUALLY_APPLIED:Lcom/squareup/api/items/Discount$ApplicationMethod;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount$ApplicationMethod;

    return-object v0
.end method

.method public getColor()Ljava/lang/String;
    .locals 2

    .line 158
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->color:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getCompOrdinal()I
    .locals 2

    .line 154
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->comp_ordinal:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/Discount;->DEFAULT_COMP_ORDINAL:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getDiscountType()Lcom/squareup/api/items/Discount$DiscountType;
    .locals 2

    .line 146
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    sget-object v1, Lcom/squareup/api/items/Discount;->DEFAULT_DISCOUNT_TYPE:Lcom/squareup/api/items/Discount$DiscountType;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount$DiscountType;

    return-object v0
.end method

.method public getMaximumAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 137
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->maximum_amount:Lcom/squareup/protos/common/dinero/Money;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 138
    :cond_0
    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getMerchantCatalogObjectToken()Ljava/lang/String;
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 96
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v0, v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getModifyTaxBasis()Lcom/squareup/api/items/Discount$ModifyTaxBasis;
    .locals 2

    .line 142
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->modify_tax_basis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    sget-object v1, Lcom/squareup/api/items/Discount;->DEFAULT_MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 100
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPasscodeRequired()Z
    .locals 2

    .line 162
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->pin_required:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/Discount;->DEFAULT_PIN_REQUIRED:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getPercentage()Ljava/lang/String;
    .locals 1

    .line 108
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    return-object v0
.end method

.method public getSortText()Ljava/lang/String;
    .locals 1

    .line 237
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isComp()Z
    .locals 2

    .line 118
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getApplicationMethod()Lcom/squareup/api/items/Discount$ApplicationMethod;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/Discount$ApplicationMethod;->COMP:Lcom/squareup/api/items/Discount$ApplicationMethod;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isPercentageDiscount()Z
    .locals 2

    .line 112
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getDiscountType()Lcom/squareup/api/items/Discount$DiscountType;

    move-result-object v0

    .line 113
    sget-object v1, Lcom/squareup/api/items/Discount$DiscountType;->VARIABLE_PERCENTAGE:Lcom/squareup/api/items/Discount$DiscountType;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/squareup/api/items/Discount$DiscountType;->FIXED:Lcom/squareup/api/items/Discount$DiscountType;

    if-ne v0, v1, :cond_0

    .line 114
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isVariable()Z
    .locals 2

    .line 166
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getDiscountType()Lcom/squareup/api/items/Discount$DiscountType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 167
    sget-object v1, Lcom/squareup/api/items/Discount$DiscountType;->FIXED:Lcom/squareup/api/items/Discount$DiscountType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public searchKeywords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 171
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/StringUtils;->normalizedKeywordsForWordPrefixSearchWithSpecialCharactersIgnored(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public updateOrSame(Ljava/lang/String;Lcom/squareup/api/items/Discount$DiscountType;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/api/items/Discount$ModifyTaxBasis;)Lcom/squareup/shared/catalog/models/CatalogDiscount;
    .locals 3

    .line 72
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getDiscountType()Lcom/squareup/api/items/Discount$DiscountType;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/api/items/Discount$DiscountType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    invoke-static {p3, v0}, Lcom/squareup/shared/catalog/utils/ObjectUtils;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {p4, v0}, Lcom/squareup/shared/catalog/utils/ObjectUtils;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getModifyTaxBasis()Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    move-result-object v0

    invoke-static {p5, v0}, Lcom/squareup/shared/catalog/utils/ObjectUtils;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    return-object p0

    .line 82
    :cond_2
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v1

    .line 83
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/items/Discount;

    invoke-virtual {v2}, Lcom/squareup/api/items/Discount;->newBuilder()Lcom/squareup/api/items/Discount$Builder;

    move-result-object v2

    .line 84
    invoke-virtual {v2, p1}, Lcom/squareup/api/items/Discount$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object p1

    .line 85
    invoke-virtual {p1, p2}, Lcom/squareup/api/items/Discount$Builder;->discount_type(Lcom/squareup/api/items/Discount$DiscountType;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object p1

    .line 86
    invoke-virtual {p1, p3}, Lcom/squareup/api/items/Discount$Builder;->percentage(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object p1

    .line 87
    invoke-static {p4}, Lcom/squareup/shared/catalog/utils/Dineros;->toDineroOrNull(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/api/items/Discount$Builder;->amount(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object p1

    .line 88
    invoke-virtual {p1, p5}, Lcom/squareup/api/items/Discount$Builder;->modify_tax_basis(Lcom/squareup/api/items/Discount$ModifyTaxBasis;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object p1

    .line 89
    invoke-virtual {p1}, Lcom/squareup/api/items/Discount$Builder;->build()Lcom/squareup/api/items/Discount;

    move-result-object p1

    .line 83
    invoke-virtual {v1, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->discount(Lcom/squareup/api/items/Discount;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    .line 90
    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method
