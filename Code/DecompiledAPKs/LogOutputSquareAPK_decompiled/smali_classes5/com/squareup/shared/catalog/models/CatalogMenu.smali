.class public final Lcom/squareup/shared/catalog/models/CatalogMenu;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogMenu.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogMenu$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/Menu;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method


# virtual methods
.method public getMerchantCatalogObjectToken()Ljava/lang/String;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenu;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Menu;

    iget-object v0, v0, Lcom/squareup/api/items/Menu;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 27
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenu;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Menu;

    iget-object v0, v0, Lcom/squareup/api/items/Menu;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v0, v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 31
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenu;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Menu;

    iget-object v0, v0, Lcom/squareup/api/items/Menu;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getReferentTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;"
        }
    .end annotation

    .line 51
    sget-object v0, Lcom/squareup/api/items/Type;->TAG:Lcom/squareup/api/items/Type;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRelations()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 39
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 40
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenu;->getRootTag()Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 41
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_MENU_MENU_GROUP:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenu;->getRootTag()Lcom/squareup/api/sync/ObjectId;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public getRootTag()Lcom/squareup/api/sync/ObjectId;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenu;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Menu;

    iget-object v0, v0, Lcom/squareup/api/items/Menu;->root_tag:Lcom/squareup/api/sync/ObjectId;

    return-object v0
.end method

.method public getSortText()Ljava/lang/String;
    .locals 1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenu;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
