.class Lcom/squareup/shared/catalog/sync/WriteMessage$2;
.super Lcom/squareup/shared/catalog/sync/SyncTask;
.source "WriteMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/catalog/sync/WriteMessage;->onResponse(Lcom/squareup/api/rpc/Response;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/sync/SyncTask<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/catalog/sync/WriteMessage;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/WriteMessage;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/WriteMessage$2;->this$0:Lcom/squareup/shared/catalog/sync/WriteMessage;

    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/SyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public perform(Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/WriteMessage$2;->this$0:Lcom/squareup/shared/catalog/sync/WriteMessage;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/WriteMessage;->access$000(Lcom/squareup/shared/catalog/sync/WriteMessage;)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->deletePendingWriteRequest(J)V

    const/4 p1, 0x0

    return-object p1
.end method
