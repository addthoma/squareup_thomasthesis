.class final synthetic Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/shared/catalog/sync/SyncCallback;


# instance fields
.field private final arg$1:Lcom/squareup/shared/catalog/sync/GetMessage;

.field private final arg$2:J

.field private final arg$3:Lcom/squareup/shared/catalog/sync/SyncCallback;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/GetMessage;JLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$1;->arg$1:Lcom/squareup/shared/catalog/sync/GetMessage;

    iput-wide p2, p0, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$1;->arg$2:J

    iput-object p4, p0, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$1;->arg$3:Lcom/squareup/shared/catalog/sync/SyncCallback;

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$1;->arg$1:Lcom/squareup/shared/catalog/sync/GetMessage;

    iget-wide v1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$1;->arg$2:J

    iget-object v3, p0, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$1;->arg$3:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/squareup/shared/catalog/sync/GetMessage;->lambda$handleModifiedObjectSets$1$GetMessage(JLcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method
