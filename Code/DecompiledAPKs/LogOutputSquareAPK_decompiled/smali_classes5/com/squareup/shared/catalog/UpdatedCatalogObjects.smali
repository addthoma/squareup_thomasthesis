.class public Lcom/squareup/shared/catalog/UpdatedCatalogObjects;
.super Ljava/lang/Object;
.source "UpdatedCatalogObjects.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;
    }
.end annotation


# instance fields
.field public final isLocalEdit:Z

.field public final updatedCategoriesById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItemCategory;",
            ">;"
        }
    .end annotation
.end field

.field public final updatedDiscountsById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;"
        }
    .end annotation
.end field

.field public final updatedImagesById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItemImage;",
            ">;"
        }
    .end annotation
.end field

.field public final updatedItemModifierListsById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            ">;"
        }
    .end annotation
.end field

.field public final updatedItemModifierOptionsByModifierListId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;>;"
        }
    .end annotation
.end field

.field public final updatedItemsById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            ">;"
        }
    .end annotation
.end field

.field public final updatedPricingRulesById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogPricingRule;",
            ">;"
        }
    .end annotation
.end field

.field public final updatedTicketGroupsById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogTicketGroup;",
            ">;"
        }
    .end annotation
.end field

.field public final updatedTicketTemplatesByTicketGroupId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;>;"
        }
    .end annotation
.end field

.field public final updatedTimePeriodsById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogTimePeriod;",
            ">;"
        }
    .end annotation
.end field

.field public final updatedVariationsByItemId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;)V
    .locals 1

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iget-boolean v0, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->isLocalEdit:Z

    iput-boolean v0, p0, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->isLocalEdit:Z

    .line 44
    iget-object v0, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedCategoriesById:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedCategoriesById:Ljava/util/Map;

    .line 45
    iget-object v0, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedImagesById:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedImagesById:Ljava/util/Map;

    .line 46
    iget-object v0, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedVariationsByItemId:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedVariationsByItemId:Ljava/util/Map;

    .line 47
    iget-object v0, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedItemsById:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedItemsById:Ljava/util/Map;

    .line 48
    iget-object v0, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedDiscountsById:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedDiscountsById:Ljava/util/Map;

    .line 49
    iget-object v0, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedItemModifierListsById:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedItemModifierListsById:Ljava/util/Map;

    .line 50
    iget-object v0, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedItemModifierOptionsByModifierListId:Ljava/util/Map;

    .line 51
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedItemModifierOptionsByModifierListId:Ljava/util/Map;

    .line 52
    iget-object v0, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedPricingRulesById:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedPricingRulesById:Ljava/util/Map;

    .line 53
    iget-object v0, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedTicketGroupsById:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedTicketGroupsById:Ljava/util/Map;

    .line 54
    iget-object v0, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedTicketTemplatesByTicketGroupId:Ljava/util/Map;

    .line 55
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedTicketTemplatesByTicketGroupId:Ljava/util/Map;

    .line 56
    iget-object p1, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedTimePeriodsById:Ljava/util/Map;

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedTimePeriodsById:Ljava/util/Map;

    return-void
.end method
