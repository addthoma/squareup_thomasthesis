.class public final Lcom/squareup/shared/catalog/models/CatalogItemImage;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogItemImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/ItemImage;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method


# virtual methods
.method public builder()Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemImage;)V

    return-object v0
.end method

.method public getMerchantCatalogObjectToken()Ljava/lang/String;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemImage;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemImage;

    iget-object v0, v0, Lcom/squareup/api/items/ItemImage;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemImage;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemImage;

    iget-object v0, v0, Lcom/squareup/api/items/ItemImage;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v0, v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 2

    .line 20
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemImage;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemImage;

    iget-object v0, v0, Lcom/squareup/api/items/ItemImage;->url:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
