.class public Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;
.super Ljava/lang/Object;
.source "PrimitiveOptionValueCombination.java"

# interfaces
.implements Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;


# instance fields
.field private final optionValueIdPairs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;->optionValueIdPairs:Ljava/util/List;

    return-void
.end method

.method public static fromByteArray([B)Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 36
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    .line 37
    invoke-virtual {v0, p0}, Lokio/Buffer;->write([B)Lokio/Buffer;

    .line 38
    invoke-static {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;->readFromSource(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    move-result-object p0

    return-object p0
.end method

.method private static readFromSource(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 50
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    .line 51
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 53
    invoke-static {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->readFromSource(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 56
    :cond_0
    new-instance p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    invoke-direct {p0, v1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;-><init>(Ljava/util/List;)V

    return-object p0
.end method

.method private writeToSink(Lokio/BufferedSink;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 42
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;->optionValueCount()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 43
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;->optionValueIdPairs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 44
    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->writeToSink(Lokio/BufferedSink;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public copyOptionValueIdPairs()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;->optionValueIdPairs:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 64
    :cond_0
    instance-of v0, p1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 68
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object v0

    check-cast p1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    .line 69
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object p1

    .line 68
    invoke-interface {v0, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getName(Ljava/util/Map;)Ljava/lang/String;
    .locals 0

    invoke-static {p0, p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination$$CC;->getName$$dflt$$(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;Ljava/util/Map;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public hashCode()I
    .locals 3

    .line 75
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;->optionValueIdPairs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    mul-int/lit8 v1, v1, 0x1f

    .line 76
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    :cond_0
    return v1
.end method

.method public optionValueCount()I
    .locals 1

    invoke-static {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination$$CC;->optionValueCount$$dflt$$(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;)I

    move-result v0

    return v0
.end method

.method public toByteArray()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 30
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    .line 31
    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;->writeToSink(Lokio/BufferedSink;)V

    .line 32
    invoke-virtual {v0}, Lokio/Buffer;->readByteArray()[B

    move-result-object v0

    return-object v0
.end method
