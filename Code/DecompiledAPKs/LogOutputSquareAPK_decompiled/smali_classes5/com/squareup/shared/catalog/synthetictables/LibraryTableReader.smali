.class public Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;
.super Ljava/lang/Object;
.source "LibraryTableReader.java"

# interfaces
.implements Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;,
        Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;
    }
.end annotation


# static fields
.field private static final READ_ALL_SUBJECT:Ljava/lang/String;

.field private static final READ_ALL_SUBJECT_INCLUDE_UNIT:Ljava/lang/String;

.field private static final READ_ALL_SUBJECT_SELECT_STATEMENT:Ljava/lang/String;

.field private static final VARIATION_WORD_PREDICATES:Ljava/lang/String; = "%variation_word_predicates%"

.field private static final WORD_PREDICATES:Ljava/lang/String; = "%word_predicates%"


# instance fields
.field private helper:Lcom/squareup/shared/sql/DatabaseHelper;

.field private final sourceTables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "SELECT {adapter_id}, {id}, {object_type}, {item_type}, {name}, {image_id}, {image_url}, {variation_count}, {price_amt}, {price_cur}, {duration}, {discount_percentage}, {discount_type}, {abbrev}, {color}, {default_variation}, {search_words}, {category_id}, {ordinal}, {naming_method} "

    .line 80
    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "adapter_id"

    const-string v2, "_id"

    .line 86
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "id"

    const-string v2, "object_id"

    .line 87
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "object_type"

    .line 88
    invoke-virtual {v0, v1, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "item_type"

    .line 89
    invoke-virtual {v0, v1, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "name"

    .line 90
    invoke-virtual {v0, v1, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "image_id"

    .line 91
    invoke-virtual {v0, v1, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "image_url"

    .line 92
    invoke-virtual {v0, v1, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string/jumbo v1, "variation_count"

    const-string/jumbo v2, "variations"

    .line 93
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "price_amt"

    const-string v2, "price_amount"

    .line 94
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "price_cur"

    const-string v2, "price_currency"

    .line 95
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "duration"

    .line 96
    invoke-virtual {v0, v1, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "discount_percentage"

    .line 97
    invoke-virtual {v0, v1, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "discount_type"

    .line 98
    invoke-virtual {v0, v1, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "abbrev"

    const-string v2, "abbreviation"

    .line 99
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "color"

    .line 100
    invoke-virtual {v0, v1, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "default_variation"

    .line 101
    invoke-virtual {v0, v1, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "search_words"

    .line 102
    invoke-virtual {v0, v1, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "category_id"

    .line 103
    invoke-virtual {v0, v1, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "ordinal"

    .line 104
    invoke-virtual {v0, v1, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "naming_method"

    .line 105
    invoke-virtual {v0, v1, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 107
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->READ_ALL_SUBJECT_SELECT_STATEMENT:Ljava/lang/String;

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->READ_ALL_SUBJECT_SELECT_STATEMENT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "FROM {table} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 110
    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "table"

    const-string v2, "library"

    .line 111
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 113
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->READ_ALL_SUBJECT:Ljava/lang/String;

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->READ_ALL_SUBJECT_SELECT_STATEMENT:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", {measurement_unit_blob} FROM {table} LEFT OUTER JOIN {connect_objects_table} ON {table}.{measurement_unit_token} = {connect_objects_table}.{measurement_unit_id} "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v2, "library"

    .line 120
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "measurement_unit_blob"

    const-string v2, "connectv2_object_blob"

    .line 121
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "connect_objects_table"

    const-string v2, "connectv2_objects"

    .line 122
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "measurement_unit_token"

    const-string v2, "measurement_unit_token"

    .line 123
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    const-string v1, "measurement_unit_id"

    const-string v2, "connectv2_object_id"

    .line 124
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 126
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->READ_ALL_SUBJECT_INCLUDE_UNIT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable;Lcom/squareup/shared/catalog/synthetictables/LibraryTable;Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable;)V
    .locals 4

    .line 519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p3, :cond_0

    new-array p3, v0, [Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;

    aput-object p1, p3, v2

    aput-object p2, p3, v1

    .line 521
    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->sourceTables:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 v3, 0x3

    new-array v3, v3, [Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;

    aput-object p1, v3, v2

    aput-object p2, v3, v1

    aput-object p3, v3, v0

    .line 523
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->sourceTables:Ljava/util/List;

    :goto_0
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .line 75
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->READ_ALL_SUBJECT_INCLUDE_UNIT:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .line 75
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->READ_ALL_SUBJECT:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .line 75
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->READ_ALL_SUBJECT_SELECT_STATEMENT:Ljava/lang/String;

    return-object v0
.end method

.method private checkItemVariationSkuAndNameTable(Z)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x0

    .line 1010
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->sourceTables:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;

    .line 1011
    instance-of v1, v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable;

    if-eqz v1, :cond_1

    const/4 p1, 0x1

    :cond_2
    if-eqz p1, :cond_3

    return-void

    .line 1017
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "ItemVariationSkuAndNameTable is required when includingVariationNames is true"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static escapeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 1033
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1034
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object p0

    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    aget-char v3, p0, v2

    const/16 v4, 0x5e

    if-ne v3, v4, :cond_0

    const-string v3, "^^"

    .line 1036
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_0
    const/16 v4, 0x5f

    if-ne v3, v4, :cond_1

    const-string v3, "^_"

    .line 1038
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    const/16 v4, 0x25

    if-ne v3, v4, :cond_2

    const-string v3, "^%"

    .line 1040
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1042
    :cond_2
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1045
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static mergeParams([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 7

    .line 1049
    array-length v0, p0

    array-length v1, p1

    add-int/2addr v0, v1

    new-array v0, v0, [Ljava/lang/String;

    .line 1052
    array-length v1, p0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v5, p0, v3

    add-int/lit8 v6, v4, 0x1

    .line 1053
    aput-object v5, v0, v4

    add-int/lit8 v3, v3, 0x1

    move v4, v6

    goto :goto_0

    .line 1055
    :cond_0
    array-length p0, p1

    :goto_1
    if-ge v2, p0, :cond_1

    aget-object v1, p1, v2

    add-int/lit8 v3, v4, 0x1

    .line 1056
    aput-object v1, v0, v4

    add-int/lit8 v2, v2, 0x1

    move v4, v3

    goto :goto_1

    :cond_1
    return-object v0
.end method

.method private static rawQueryWithObjectTypesAndItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Lcom/squareup/shared/sql/SQLCursor;"
        }
    .end annotation

    .line 907
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0, p2}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 908
    sget-object p2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0, p2}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    .line 910
    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->size()I

    move-result p2

    invoke-static {p2}, Lcom/squareup/shared/catalog/Queries;->groupPlaceholder(I)Ljava/lang/String;

    move-result-object p2

    const-string v1, "%object_types%"

    invoke-virtual {p1, v1, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 911
    invoke-interface {p3}, Ljava/util/Set;->size()I

    move-result p2

    invoke-static {p2}, Lcom/squareup/shared/catalog/Queries;->groupPlaceholder(I)Ljava/lang/String;

    move-result-object p2

    const-string v1, "%item_types%"

    invoke-virtual {p1, v1, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 913
    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->size()I

    move-result p2

    invoke-interface {p3}, Ljava/util/Set;->size()I

    move-result v1

    add-int/2addr p2, v1

    new-array p2, p2, [Ljava/lang/String;

    .line 915
    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;

    add-int/lit8 v3, v1, 0x1

    .line 916
    invoke-static {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, p2, v1

    move v1, v3

    goto :goto_0

    .line 918
    :cond_0
    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item$Type;

    add-int/lit8 v2, v1, 0x1

    .line 919
    invoke-virtual {v0}, Lcom/squareup/api/items/Item$Type;->getValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p2, v1

    move v1, v2

    goto :goto_1

    .line 922
    :cond_1
    invoke-interface {p0, p1, p2}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p0

    return-object p0
.end method

.method private varargs rawWordPredicateQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;[",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/sql/SQLCursor;"
        }
    .end annotation

    .line 935
    invoke-direct {p0, p3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->wordPrefixPredicateForSearchName(Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;

    move-result-object v0

    .line 937
    invoke-static {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;->access$400(Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "%word_predicates%"

    invoke-virtual {p2, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 939
    invoke-static {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;->access$300(Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {p6, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->mergeParams([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p6

    if-eqz p4, :cond_0

    .line 943
    invoke-direct {p0, p3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->wordPrefixPredicateForVariationName(Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;

    move-result-object p3

    .line 945
    invoke-static {p3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;->access$400(Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;)Ljava/lang/String;

    move-result-object p4

    const-string v0, "%variation_word_predicates%"

    invoke-virtual {p2, v0, p4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 947
    invoke-static {p3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;->access$300(Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;)Ljava/util/List;

    move-result-object p3

    new-array p4, v1, [Ljava/lang/String;

    invoke-interface {p3, p4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p3

    check-cast p3, [Ljava/lang/String;

    invoke-static {p6, p3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->mergeParams([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p6

    .line 950
    :cond_0
    invoke-static {p5, p6}, Lcom/squareup/shared/catalog/Queries;->buildParamsWithItemTypes(Ljava/util/List;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p3

    .line 952
    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result p4

    invoke-static {p4}, Lcom/squareup/shared/catalog/Queries;->groupPlaceholder(I)Ljava/lang/String;

    move-result-object p4

    const-string p5, "%item_types%"

    invoke-virtual {p2, p5, p4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 954
    invoke-interface {p1, p2, p3}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method private wordPrefixPredicateForNormalizedString(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;
    .locals 7

    .line 969
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/StringUtils;->removeSpecialCharacters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 970
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/StringUtils;->splitByWhiteSpace(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    .line 974
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    .line 975
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 978
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 980
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    .line 982
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 983
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 985
    invoke-static {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->escapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_1

    const-string v4, " AND "

    .line 988
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v4, "({column_name} LIKE ? ESCAPE \'^\' )"

    .line 991
    invoke-static {v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v4

    const-string v5, "column_name"

    .line 992
    invoke-virtual {v4, v5, p2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v4

    .line 993
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v4

    .line 994
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 996
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "% "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "%"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 998
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1001
    :cond_2
    new-instance p1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object p1
.end method

.method private wordPrefixPredicateForSearchName(Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;
    .locals 1

    const-string v0, "search_words"

    .line 958
    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->wordPrefixPredicateForNormalizedString(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;

    move-result-object p1

    return-object p1
.end method

.method private wordPrefixPredicateForVariationName(Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;
    .locals 1

    const-string/jumbo v0, "variation_search_words"

    .line 962
    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->wordPrefixPredicateForNormalizedString(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public countCatalogItemsAndDiscountsForCollectionSectionIndex(Ljava/lang/String;Ljava/util/List;)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 771
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v2

    .line 773
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 774
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->escapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "%"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 776
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    const/4 v8, 0x0

    .line 779
    :try_start_0
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->COUNT_ITEMS_AND_DISCOUNTS_FOR_SEARCH_TERM_FOR_COLLATION_SECTION_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 780
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v9, 0x1

    new-array v7, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object p1, v7, v10

    move-object v1, p0

    move-object v6, p2

    .line 779
    invoke-direct/range {v1 .. v7}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->rawWordPredicateQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v8

    .line 782
    :goto_0
    invoke-interface {v8}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 783
    invoke-interface {v8, v10}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result p1

    .line 784
    invoke-interface {v8, v9}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result p2

    .line 785
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    if-eqz v8, :cond_1

    .line 789
    invoke-interface {v8}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_1
    return-object v0

    :catchall_0
    move-exception p1

    if-eqz v8, :cond_2

    invoke-interface {v8}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_2
    throw p1
.end method

.method public countCatalogItemsForAllNonEmptyCategory(Ljava/util/List;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 670
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 671
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    const/4 v2, 0x0

    .line 674
    :try_start_0
    sget-object v3, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->COUNT_ITEMS_FOR_ALL_NON_EMPTY_CATEGORIES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v5, v4, [Ljava/lang/String;

    invoke-static {v0, v3, p1, v5}, Lcom/squareup/shared/catalog/Queries;->rawQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v2

    .line 676
    :goto_0
    invoke-interface {v2}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 677
    invoke-interface {v2, v4}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    .line 678
    invoke-interface {v2, v0}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result v0

    .line 679
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    if-eqz v2, :cond_1

    .line 683
    invoke-interface {v2}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_1
    return-object v1

    :catchall_0
    move-exception p1

    if-eqz v2, :cond_2

    invoke-interface {v2}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_2
    throw p1
.end method

.method public countCatalogItemsForCollationSectionIndexForCategoryId(Ljava/lang/String;Ljava/util/List;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 740
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 741
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    const/4 v2, 0x0

    .line 744
    :try_start_0
    sget-object v3, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->COUNT_ITEMS_IN_CATEGORY_FOR_COLLATION_SECTION_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 745
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    .line 744
    invoke-static {v0, v3, p2, v5}, Lcom/squareup/shared/catalog/Queries;->rawQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v2

    .line 748
    :goto_0
    invoke-interface {v2}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 749
    invoke-interface {v2, v6}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result p1

    .line 750
    invoke-interface {v2, v4}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result p2

    .line 751
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    if-eqz v2, :cond_1

    .line 755
    invoke-interface {v2}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_1
    return-object v1

    :catchall_0
    move-exception p1

    if-eqz v2, :cond_2

    invoke-interface {v2}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_2
    throw p1
.end method

.method public countCatalogObjectsForCollationSectionIndex(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 711
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 712
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    const/4 v2, 0x0

    .line 715
    :try_start_0
    new-instance v3, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    sget-object v4, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->COUNT_OBJECTS_FOR_COLLATION_SECTION_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 716
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v4

    .line 715
    invoke-static {v0, v4, p1, p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->rawQueryWithObjectTypesAndItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    const-string p2, ""

    invoke-direct {v3, p1, p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 718
    :goto_0
    :try_start_1
    invoke-interface {v3}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 719
    invoke-interface {v3, p1}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result p1

    const/4 p2, 0x1

    .line 720
    invoke-interface {v3, p2}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result p2

    .line 721
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 725
    :cond_0
    invoke-interface {v3}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object v1

    :catchall_0
    move-exception p1

    move-object v2, v3

    goto :goto_1

    :catchall_1
    move-exception p1

    :goto_1
    if-eqz v2, :cond_1

    invoke-interface {v2}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_1
    throw p1
.end method

.method public findCatalogItemsForCategoryId(Ljava/lang/String;Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;"
        }
    .end annotation

    .line 694
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 695
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->FIND_ITEMS_FOR_CATEGORY:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 696
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, p2, v2}, Lcom/squareup/shared/catalog/Queries;->rawQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    .line 698
    new-instance p2, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    const-string v0, ""

    invoke-direct {p2, p1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V

    return-object p2
.end method

.method public onRegistered(Lcom/squareup/shared/sql/DatabaseHelper;)V
    .locals 0

    .line 528
    iput-object p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    return-void
.end method

.method public readAllCategories()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 4

    .line 596
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 597
    new-instance v1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    sget-object v2, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_CATEGORIES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v0

    const-string v2, ""

    invoke-direct {v1, v0, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V

    return-object v1
.end method

.method public readAllCategoryDiscountItemOrderByType(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;"
        }
    .end annotation

    .line 540
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 541
    new-instance v1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    sget-object v2, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_CATEGORY_DISCOUNT_ITEM_ORDER_BY_TYPE:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 543
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    .line 542
    invoke-static {v0, v2, p1, v3}, Lcom/squareup/shared/catalog/Queries;->rawQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    const-string v0, ""

    invoke-direct {v1, p1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V

    return-object v1
.end method

.method public readAllDiscounts()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 4

    .line 607
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 608
    new-instance v1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    sget-object v2, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_DISCOUNTS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v0

    const-string v2, ""

    invoke-direct {v1, v0, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V

    return-object v1
.end method

.method public readAllGiftCards()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 1

    .line 616
    sget-object v0, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readItemsWithTypes(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object v0

    return-object v0
.end method

.method public readAllItemsAvailableForPickup()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 4

    .line 620
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 621
    new-instance v1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    sget-object v2, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_ITEMS_AVAILABLE_FOR_PICKUP:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 622
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v0

    const-string v2, ""

    invoke-direct {v1, v0, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V

    return-object v1
.end method

.method public readAllModifierLists()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 4

    .line 626
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 627
    new-instance v1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    sget-object v2, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_MODIFIER_LISTS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v0

    const-string v2, ""

    invoke-direct {v1, v0, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V

    return-object v1
.end method

.method public readAllNonEmptyCategories(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;"
        }
    .end annotation

    .line 601
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 602
    new-instance v1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    sget-object v2, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_NONEMPTY_CATEGORIES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 603
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-static {v0, v2, p1, v3}, Lcom/squareup/shared/catalog/Queries;->rawQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    const-string v0, ""

    invoke-direct {v1, p1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V

    return-object v1
.end method

.method public readAllObjectsOfTypeFromLibraryTable(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;"
        }
    .end annotation

    .line 580
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$1;->$SwitchMap$com$squareup$api$items$Type:[I

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->getProtoObjectType()Lcom/squareup/api/items/Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/api/items/Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 p2, 0x2

    if-eq v0, p2, :cond_2

    const/4 p2, 0x3

    if-eq v0, p2, :cond_1

    const/4 p2, 0x4

    if-ne v0, p2, :cond_0

    .line 588
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllModifierLists()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    return-object p1

    .line 590
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Object type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 591
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " is not supported"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 586
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllDiscounts()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    return-object p1

    .line 584
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllCategories()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    return-object p1

    .line 582
    :cond_3
    invoke-virtual {p0, p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readItemsWithTypes(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    return-object p1
.end method

.method public readAllServices()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 1

    .line 612
    sget-object v0, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readItemsWithTypes(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object v0

    return-object v0
.end method

.method public readAllTicketGroups()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 4

    .line 631
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 632
    new-instance v1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    sget-object v2, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_TICKET_GROUPS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v0

    const-string v2, ""

    invoke-direct {v1, v0, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V

    return-object v1
.end method

.method public readAllUsedCategoriesAndEmpty(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 556
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllCategoryDiscountItemOrderByType(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 557
    :try_start_1
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCount()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-eqz v0, :cond_1

    .line 561
    new-instance p1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    const-string v2, ""

    invoke-direct {p1, v1, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V

    goto :goto_0

    .line 563
    :cond_1
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllNonEmptyCategories(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    .line 565
    :goto_0
    new-instance v2, Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;

    invoke-direct {v2, p1, v0}, Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    .line 568
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    :cond_2
    return-object v2

    :catchall_0
    move-exception p1

    goto :goto_1

    :catchall_1
    move-exception p1

    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    :cond_3
    throw p1
.end method

.method public readItemsWithTypes(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;"
        }
    .end annotation

    .line 640
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 641
    new-instance v1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    sget-object v2, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_ITEMS_WITH_TYPES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 642
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-static {v0, v2, p1, v3}, Lcom/squareup/shared/catalog/Queries;->rawQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    const-string v0, ""

    invoke-direct {v1, p1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V

    return-object v1
.end method

.method public readObjectsWithTypes(Ljava/util/Set;Ljava/util/Set;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;"
        }
    .end annotation

    .line 655
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 656
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "No item types provided for ITEM query."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 658
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 659
    new-instance v1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    sget-object v2, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_OBJECTS_WITH_TYPES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 661
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v2

    .line 660
    invoke-static {v0, v2, p1, p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->rawQueryWithObjectTypesAndItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    const-string p2, ""

    invoke-direct {v1, p1, p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V

    return-object v1
.end method

.method public sourceTables()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;"
        }
    .end annotation

    .line 532
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->sourceTables:Ljava/util/List;

    return-object v0
.end method

.method public wordPrefixSearchByNameAndTypes(Ljava/lang/String;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/util/List;Z)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;Z)",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;"
        }
    .end annotation

    if-eqz p2, :cond_2

    .line 843
    invoke-direct {p0, p4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->checkItemVariationSkuAndNameTable(Z)V

    .line 845
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v2

    .line 846
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 847
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->escapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 852
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v3, 0x1

    const/4 v5, 0x0

    if-ne p2, v1, :cond_1

    .line 853
    invoke-static {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object p2

    if-eqz p4, :cond_0

    .line 856
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_BY_NAME_AND_VARIATION_NAME_WITH_ITEM_TYPE:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    goto :goto_0

    .line 857
    :cond_0
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_BY_NAME_WITH_ITEM_TYPE:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 859
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v6

    const/4 v1, 0x2

    new-array v7, v1, [Ljava/lang/String;

    aput-object p2, v7, v5

    aput-object v0, v7, v3

    move-object v1, p0

    move-object v3, v6

    move v5, p4

    move-object v6, p3

    invoke-direct/range {v1 .. v7}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->rawWordPredicateQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p2

    goto :goto_1

    .line 862
    :cond_1
    invoke-static {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object p2

    new-array p3, v3, [Ljava/lang/String;

    aput-object p2, p3, v5

    .line 867
    invoke-direct {p0, v4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->wordPrefixPredicateForSearchName(Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;

    move-result-object p2

    .line 868
    invoke-static {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;->access$300(Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;)Ljava/util/List;

    move-result-object p4

    new-array v0, v5, [Ljava/lang/String;

    invoke-interface {p4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p4

    check-cast p4, [Ljava/lang/String;

    invoke-static {p3, p4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->mergeParams([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p3

    .line 870
    sget-object p4, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_BY_NAME_WITH_OBJECT_TYPE:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 871
    invoke-virtual {p4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object p4

    .line 872
    invoke-static {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;->access$400(Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "%word_predicates%"

    invoke-virtual {p4, v0, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 870
    invoke-interface {v2, p2, p3}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p2

    .line 875
    :goto_1
    new-instance p3, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-direct {p3, p2, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V

    return-object p3

    .line 840
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Object type cannot be null. Use searchByName instead."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public wordPrefixSearchForItemsInCategory(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;Z)",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;"
        }
    .end annotation

    .line 888
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v2

    .line 889
    invoke-static {p2}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 891
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->escapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz p4, :cond_0

    .line 894
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_FOR_ITEMS_BY_NAME_AND_VARIATION_NAME_IN_CATEGORY:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    goto :goto_0

    .line 895
    :cond_0
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_FOR_ITEMS_BY_NAME_IN_CATEGORY:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 898
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x2

    new-array v7, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v7, v1

    const/4 p1, 0x1

    aput-object v0, v7, p1

    move-object v1, p0

    move v5, p4

    move-object v6, p3

    .line 897
    invoke-direct/range {v1 .. v7}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->rawWordPredicateQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    .line 901
    new-instance p3, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-direct {p3, p1, p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V

    return-object p3
.end method

.method public wordPrefixSearchForItemsOrDiscountsByName(Ljava/lang/String;Ljava/util/List;Z)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;Z)",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;"
        }
    .end annotation

    .line 805
    invoke-direct {p0, p3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->checkItemVariationSkuAndNameTable(Z)V

    .line 807
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v2

    .line 808
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-static {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v0

    .line 809
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-static {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v1

    .line 811
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 812
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->escapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "%"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x3

    new-array v7, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v7, v5

    const/4 v0, 0x1

    aput-object v1, v7, v0

    const/4 v0, 0x2

    aput-object v3, v7, v0

    if-eqz p3, :cond_0

    .line 819
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_BY_NAME_AND_VARIATION_NAME_FOR_ITEMS_OR_DISCOUNTS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    goto :goto_0

    .line 820
    :cond_0
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_BY_NAME_FOR_ITEMS_OR_DISCOUNTS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 823
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v3

    move-object v1, p0

    move v5, p3

    move-object v6, p2

    .line 822
    invoke-direct/range {v1 .. v7}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->rawWordPredicateQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p2

    .line 825
    new-instance p3, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-direct {p3, p2, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V

    return-object p3
.end method
