.class public Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$Builder;
.super Ljava/lang/Object;
.source "CatalogResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/connectv2/models/CatalogResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

.field private resource:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;-><init>()V

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->RESOURCE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    .line 42
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$Builder;->resource:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogResource;)V
    .locals 1

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iget-object v0, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    .line 47
    iget-object p1, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$Builder;->resource:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogResource;Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$1;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogResource;)V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/connectv2/models/CatalogResource;
    .locals 3

    .line 61
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource;

    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    iget-object v2, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$Builder;->resource:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;

    .line 63
    invoke-virtual {v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->resource_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v1

    .line 64
    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v0
.end method

.method public setResouceDescription(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$Builder;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$Builder;->resource:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;->description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;

    return-object p0
.end method

.method public setResourceName(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$Builder;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$Builder;->resource:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;

    return-object p0
.end method
