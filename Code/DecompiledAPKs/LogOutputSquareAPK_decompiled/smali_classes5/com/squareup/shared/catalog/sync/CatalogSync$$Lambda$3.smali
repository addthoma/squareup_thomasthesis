.class final synthetic Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final arg$1:Lcom/squareup/shared/catalog/sync/CatalogSync;

.field private final arg$2:Ljava/util/concurrent/Callable;

.field private final arg$3:Lcom/squareup/shared/catalog/sync/SyncCallback;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$3;->arg$1:Lcom/squareup/shared/catalog/sync/CatalogSync;

    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$3;->arg$2:Ljava/util/concurrent/Callable;

    iput-object p3, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$3;->arg$3:Lcom/squareup/shared/catalog/sync/SyncCallback;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$3;->arg$1:Lcom/squareup/shared/catalog/sync/CatalogSync;

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$3;->arg$2:Ljava/util/concurrent/Callable;

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$3;->arg$3:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/sync/CatalogSync;->lambda$executeOnFileThread$4$CatalogSync(Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method
