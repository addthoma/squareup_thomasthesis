.class public Lcom/squareup/shared/catalog/utils/WeeklySchedule;
.super Ljava/lang/Object;
.source "WeeklySchedule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;
    }
.end annotation


# instance fields
.field private schedules:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet<",
            "Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule;->schedules:Ljava/util/TreeSet;

    return-void
.end method

.method private sameHourAndMinute(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 2

    const/16 v0, 0xb

    .line 83
    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-ne v1, v0, :cond_0

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result p1

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result p2

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public addSchedule(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)V
    .locals 4

    if-eqz p1, :cond_3

    .line 44
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->access$000(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)Ljava/util/TreeSet;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->access$000(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)Ljava/util/TreeSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule;->schedules:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;

    .line 49
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->access$100(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)Ljava/util/Calendar;

    move-result-object v2

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->access$100(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)Ljava/util/Calendar;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/squareup/shared/catalog/utils/WeeklySchedule;->sameHourAndMinute(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->access$200(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)Ljava/util/Calendar;

    move-result-object v2

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->access$200(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)Ljava/util/Calendar;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/squareup/shared/catalog/utils/WeeklySchedule;->sameHourAndMinute(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 50
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->access$000(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)Ljava/util/TreeSet;

    move-result-object v0

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->access$000(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)Ljava/util/TreeSet;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    .line 52
    iget-object p1, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule;->schedules:Ljava/util/TreeSet;

    invoke-virtual {p1, v1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 53
    iget-object p1, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule;->schedules:Ljava/util/TreeSet;

    invoke-virtual {p1, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    return-void

    .line 58
    :cond_2
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule;->schedules:Ljava/util/TreeSet;

    new-instance v1, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;

    invoke-direct {v1, p1}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;-><init>(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)V

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_0
    return-void
.end method

.method public addWeeklySchedule(Lcom/squareup/shared/catalog/utils/WeeklySchedule;)V
    .locals 1

    .line 38
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/utils/WeeklySchedule;->getSchedules()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;

    .line 39
    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/utils/WeeklySchedule;->addSchedule(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public getSchedules()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;",
            ">;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule;->schedules:Ljava/util/TreeSet;

    return-object v0
.end method

.method public toLocalizedString(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x3

    new-array v1, v0, [Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    .line 66
    sget-object v2, Lcom/squareup/shared/i18n/LocalizedListHelper;->AS_IS:Lcom/squareup/shared/i18n/Key;

    const/4 v3, 0x1

    .line 67
    invoke-static {v3, v2}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->exactly(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    move-result-object v2

    const/4 v4, 0x0

    aput-object v2, v1, v4

    sget-object v2, Lcom/squareup/shared/catalog/CatalogKeyManifest;->LIST_TWO:Lcom/squareup/shared/i18n/Key;

    const/4 v4, 0x2

    .line 68
    invoke-static {v4, v2}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->exactly(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    move-result-object v2

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SCHEDULE_MANY:Lcom/squareup/shared/i18n/Key;

    .line 69
    invoke-static {v0, v2}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->orMore(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    move-result-object v0

    aput-object v0, v1, v4

    .line 66
    invoke-static {p1, v1}, Lcom/squareup/shared/i18n/LocalizedListHelper;->list(Lcom/squareup/shared/i18n/Localizer;[Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)Lcom/squareup/shared/i18n/LocalizedListHelper;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule;->schedules:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;

    .line 71
    invoke-virtual {v2, p1}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->daysOfTheWeek(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;

    move-result-object v3

    .line 72
    invoke-virtual {v2, p1}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->timeRange(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;

    move-result-object v2

    .line 73
    sget-object v4, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SCHEDULE:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p1, v4}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object v4

    const-string v5, "days_of_the_week"

    .line 74
    invoke-interface {v4, v5, v3}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object v3

    const-string/jumbo v4, "time_range"

    .line 75
    invoke-interface {v3, v4, v2}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object v2

    .line 76
    invoke-interface {v2}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object v2

    .line 77
    invoke-virtual {v0, v2}, Lcom/squareup/shared/i18n/LocalizedListHelper;->put(Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedListHelper;

    goto :goto_0

    .line 79
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/shared/i18n/LocalizedListHelper;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
