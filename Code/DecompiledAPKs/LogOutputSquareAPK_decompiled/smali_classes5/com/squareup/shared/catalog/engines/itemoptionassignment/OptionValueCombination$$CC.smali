.class public abstract synthetic Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination$$CC;
.super Ljava/lang/Object;


# direct methods
.method public static getName$$dflt$$(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;Ljava/util/Map;)Ljava/lang/String;
    .locals 4

    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    .line 28
    :goto_0
    invoke-interface {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 29
    invoke-interface {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 30
    invoke-virtual {v2, p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->getValueNameWithAllItemOptionsByIds(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "valueName"

    .line 31
    invoke-static {v2, v3}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    invoke-interface {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    const-string v2, ", "

    .line 33
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 37
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static optionValueCount$$dflt$$(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;)I
    .locals 0

    .line 22
    invoke-interface {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    return p0
.end method
