.class public Lcom/squareup/shared/catalog/sync/SyncError;
.super Ljava/lang/Object;
.source "SyncError.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;
    }
.end annotation


# instance fields
.field public final errorMessage:Ljava/lang/String;

.field public final errorType:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;Ljava/lang/String;)V
    .locals 0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/SyncError;->errorType:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    .line 62
    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/SyncError;->errorMessage:Ljava/lang/String;

    return-void
.end method
