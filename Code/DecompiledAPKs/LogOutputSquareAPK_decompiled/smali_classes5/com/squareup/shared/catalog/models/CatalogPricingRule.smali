.class public final Lcom/squareup/shared/catalog/models/CatalogPricingRule;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogPricingRule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/PricingRule;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method

.method private getDiscountObject()Lcom/squareup/api/sync/ObjectId;
    .locals 1

    .line 133
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->discount_id:Lcom/squareup/api/sync/ObjectId;

    return-object v0
.end method


# virtual methods
.method public getApplicationMode()Lcom/squareup/api/items/PricingRule$ApplicationMode;
    .locals 2

    .line 192
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->application_mode:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    sget-object v1, Lcom/squareup/api/items/PricingRule;->DEFAULT_APPLICATION_MODE:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule$ApplicationMode;

    return-object v0
.end method

.method public getApplyOrMatchProductSetId()Ljava/lang/String;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->apply_products:Lcom/squareup/api/sync/ObjectId;

    if-nez v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getMatchProductSetId()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 96
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->apply_products:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getApplyProductSetId()Ljava/lang/String;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->apply_products:Lcom/squareup/api/sync/ObjectId;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 103
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->apply_products:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getDiscountId()Ljava/lang/String;
    .locals 1

    .line 140
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->discount_id:Lcom/squareup/api/sync/ObjectId;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 143
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->discount_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getDiscountTargetScope()Lcom/squareup/api/items/PricingRule$DiscountTargetScope;
    .locals 2

    .line 200
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->discount_target_scope:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    sget-object v1, Lcom/squareup/api/items/PricingRule;->DEFAULT_DISCOUNT_TARGET_SCOPE:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    return-object v0
.end method

.method public getExcludeProductSetId()Ljava/lang/String;
    .locals 1

    .line 113
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->exclude_products:Lcom/squareup/api/sync/ObjectId;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 116
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->exclude_products:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getExcludeStrategy()Lcom/squareup/api/items/PricingRule$ExcludeStrategy;
    .locals 2

    .line 126
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->exclude_strategy:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    sget-object v1, Lcom/squareup/api/items/PricingRule;->DEFAULT_EXCLUDE_STRATEGY:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    return-object v0
.end method

.method public getMatchProductSetId()Ljava/lang/String;
    .locals 1

    .line 84
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->match_products:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getMaxApplicationsPerAttachment()I
    .locals 2

    .line 208
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->max_applications_per_attachment:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/PricingRule;->DEFAULT_MAX_APPLICATIONS_PER_ATTACHMENT:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 64
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getReferentTypes()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/api/items/Type;

    .line 56
    sget-object v1, Lcom/squareup/api/items/Type;->TIME_PERIOD:Lcom/squareup/api/items/Type;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->DISCOUNT:Lcom/squareup/api/items/Type;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRelations()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 42
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 44
    invoke-direct {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getDiscountObject()Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 45
    new-instance v1, Lcom/squareup/shared/catalog/models/CatalogRelation;

    sget-object v2, Lcom/squareup/api/items/Type;->PRICING_RULE:Lcom/squareup/api/items/Type;

    invoke-direct {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getDiscountObject()Lcom/squareup/api/sync/ObjectId;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    iget-object v3, v3, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    invoke-direct {v1, v2, v3}, Lcom/squareup/shared/catalog/models/CatalogRelation;-><init>(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)V

    .line 46
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getDiscountId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    :cond_0
    new-instance v1, Lcom/squareup/shared/catalog/models/CatalogRelation;

    sget-object v2, Lcom/squareup/api/items/Type;->PRICING_RULE:Lcom/squareup/api/items/Type;

    sget-object v3, Lcom/squareup/api/items/Type;->TIME_PERIOD:Lcom/squareup/api/items/Type;

    invoke-direct {v1, v2, v3}, Lcom/squareup/shared/catalog/models/CatalogRelation;-><init>(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)V

    .line 50
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getValidity()Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getSortText()Ljava/lang/String;
    .locals 1

    .line 60
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStackable()Lcom/squareup/api/items/PricingRule$Stackable;
    .locals 2

    .line 184
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->stackable:Lcom/squareup/api/items/PricingRule$Stackable;

    sget-object v1, Lcom/squareup/api/items/PricingRule;->DEFAULT_STACKABLE:Lcom/squareup/api/items/PricingRule$Stackable;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule$Stackable;

    return-object v0
.end method

.method public getValidFrom()Ljava/lang/String;
    .locals 2

    .line 147
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->valid_from:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getValidUntil()Ljava/lang/String;
    .locals 2

    .line 164
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->valid_until:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getValidity()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 72
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule;->validity:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 73
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 74
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/sync/ObjectId;

    .line 75
    iget-object v2, v2, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    .line 214
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getMatchProductSetId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getApplyProductSetId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getExcludeProductSetId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getDiscountId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "%s{id=%s, name=%s, match_product_set=%s, apply_product_set=%s, exclude_product_set=%s, discount=%s}"

    .line 213
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public validFrom(Ljava/util/TimeZone;)Ljava/util/Date;
    .locals 2

    .line 156
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getValidFrom()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 157
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 160
    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseDateTime(Ljava/lang/String;Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public validUntil(Ljava/util/TimeZone;)Ljava/util/Date;
    .locals 2

    .line 173
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getValidUntil()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 174
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 177
    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseDateTime(Ljava/lang/String;Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method
