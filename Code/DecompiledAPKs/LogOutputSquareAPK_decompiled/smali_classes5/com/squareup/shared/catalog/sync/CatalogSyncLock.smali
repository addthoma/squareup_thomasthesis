.class public final Lcom/squareup/shared/catalog/sync/CatalogSyncLock;
.super Ljava/lang/Object;
.source "CatalogSyncLock.java"


# instance fields
.field private dead:Z

.field private final lock:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 24
    invoke-static {}, Lcom/squareup/shared/catalog/utils/UUID;->generateId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLock;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLock;->lock:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method assertLive()V
    .locals 2

    .line 42
    iget-boolean v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLock;->dead:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot re-use a dead lock"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method die()V
    .locals 1

    .line 46
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLock;->assertLive()V

    const/4 v0, 0x1

    .line 47
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLock;->dead:Z

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 32
    instance-of v0, p1, Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 33
    :cond_0
    check-cast p1, Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    .line 34
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLock;->lock:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/shared/catalog/sync/CatalogSyncLock;->lock:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/squareup/shared/catalog/utils/ObjectUtils;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 38
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLock;->lock:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/ObjectUtils;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
