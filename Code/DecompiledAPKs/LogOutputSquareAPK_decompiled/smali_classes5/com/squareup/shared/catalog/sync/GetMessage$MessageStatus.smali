.class final enum Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;
.super Ljava/lang/Enum;
.source "GetMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/sync/GetMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "MessageStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

.field public static final enum COMPLETE:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

.field public static final enum IN_PROGRESS:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

.field public static final enum MESSAGE_SENT:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

.field public static final enum READY:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

.field public static final enum SKIP:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 56
    new-instance v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    const/4 v1, 0x0

    const-string v2, "READY"

    invoke-direct {v0, v2, v1}, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->READY:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    new-instance v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    const/4 v2, 0x1

    const-string v3, "MESSAGE_SENT"

    invoke-direct {v0, v3, v2}, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->MESSAGE_SENT:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    new-instance v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    const/4 v3, 0x2

    const-string v4, "IN_PROGRESS"

    invoke-direct {v0, v4, v3}, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->IN_PROGRESS:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    new-instance v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    const/4 v4, 0x3

    const-string v5, "SKIP"

    invoke-direct {v0, v5, v4}, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->SKIP:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    new-instance v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    const/4 v5, 0x4

    const-string v6, "COMPLETE"

    invoke-direct {v0, v6, v5}, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->COMPLETE:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    .line 55
    sget-object v6, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->READY:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->MESSAGE_SENT:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->IN_PROGRESS:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->SKIP:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->COMPLETE:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->$VALUES:[Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;
    .locals 1

    .line 55
    const-class v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->$VALUES:[Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    return-object v0
.end method
