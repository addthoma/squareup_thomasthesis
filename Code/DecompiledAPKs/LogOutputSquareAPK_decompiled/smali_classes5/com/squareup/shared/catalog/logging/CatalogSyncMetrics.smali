.class public Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;
.super Ljava/lang/Object;
.source "CatalogSyncMetrics.java"


# static fields
.field public static final MIN_NUMBER_OF_OBJECTS_FOR_INCREMENTAL_SYNC_EVENT:I = 0x3e8

.field public static final MIN_NUMBER_OF_OBJECTS_FOR_LARGE_SYNC_EVENT:I = 0x1388


# instance fields
.field public final databaseDurationMs:J

.field public final databaseObjectsPerSecond:I

.field public final initialSync:Z

.field public final numberOfObjects:I

.field public final numberOfObjectsDeleted:I

.field public final numberOfObjectsUpdated:I

.field public final totalDurationMs:J

.field public final totalObjectsPerSecond:I


# direct methods
.method public constructor <init>(ZIIJJ)V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-boolean p1, p0, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->initialSync:Z

    add-int p1, p2, p3

    .line 46
    iput p1, p0, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->numberOfObjects:I

    .line 47
    iput p2, p0, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->numberOfObjectsUpdated:I

    .line 48
    iput p3, p0, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->numberOfObjectsDeleted:I

    .line 49
    iput-wide p4, p0, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->totalDurationMs:J

    .line 50
    iget p1, p0, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->numberOfObjects:I

    invoke-direct {p0, p1, p4, p5}, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->objectsPerSecond(IJ)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->totalObjectsPerSecond:I

    .line 51
    iput-wide p6, p0, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->databaseDurationMs:J

    .line 52
    iget p1, p0, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->numberOfObjects:I

    invoke-direct {p0, p1, p6, p7}, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->objectsPerSecond(IJ)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->databaseObjectsPerSecond:I

    return-void
.end method

.method private objectsPerSecond(IJ)I
    .locals 2

    long-to-double p2, p2

    const-wide v0, 0x408f400000000000L    # 1000.0

    div-double/2addr p2, v0

    int-to-double v0, p1

    div-double/2addr v0, p2

    double-to-int p1, v0

    return p1
.end method
