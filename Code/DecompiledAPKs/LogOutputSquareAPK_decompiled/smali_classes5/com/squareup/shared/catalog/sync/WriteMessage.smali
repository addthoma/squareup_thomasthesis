.class final Lcom/squareup/shared/catalog/sync/WriteMessage;
.super Lcom/squareup/shared/catalog/sync/CatalogMessage;
.source "WriteMessage.java"


# instance fields
.field private final catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

.field private final pendingWriteId:J


# direct methods
.method constructor <init>(Lcom/squareup/api/rpc/Request;JLcom/squareup/shared/catalog/sync/CatalogSync;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/sync/CatalogMessage;-><init>(Lcom/squareup/api/rpc/Request;)V

    .line 18
    iput-wide p2, p0, Lcom/squareup/shared/catalog/sync/WriteMessage;->pendingWriteId:J

    .line 19
    iput-object p4, p0, Lcom/squareup/shared/catalog/sync/WriteMessage;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/shared/catalog/sync/WriteMessage;)J
    .locals 2

    .line 11
    iget-wide v0, p0, Lcom/squareup/shared/catalog/sync/WriteMessage;->pendingWriteId:J

    return-wide v0
.end method


# virtual methods
.method public onResponse(Lcom/squareup/api/rpc/Response;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/rpc/Response;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 23
    iget-object v0, p1, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    if-eqz v0, :cond_1

    .line 27
    iget-object v0, p1, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    iget-object v0, v0, Lcom/squareup/api/rpc/Error;->sync_error_code:Lcom/squareup/api/sync/SyncErrorCode;

    sget-object v1, Lcom/squareup/api/sync/SyncErrorCode;->UNAUTHORIZED:Lcom/squareup/api/sync/SyncErrorCode;

    if-ne v0, v1, :cond_0

    .line 28
    iget-object p2, p0, Lcom/squareup/shared/catalog/sync/WriteMessage;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    new-instance v0, Lcom/squareup/shared/catalog/sync/WriteMessage$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/shared/catalog/sync/WriteMessage$1;-><init>(Lcom/squareup/shared/catalog/sync/WriteMessage;Lcom/squareup/api/rpc/Response;)V

    .line 34
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object p1

    .line 28
    invoke-virtual {p2, v0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->executeSyncTask(Lcom/squareup/shared/catalog/sync/SyncTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/WriteMessage;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    iget-object p1, p1, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    invoke-static {v0, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->handleRpcError(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/api/rpc/Error;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    .line 39
    invoke-interface {p2, p1}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void

    .line 43
    :cond_1
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/WriteMessage;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    new-instance v0, Lcom/squareup/shared/catalog/sync/WriteMessage$2;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/sync/WriteMessage$2;-><init>(Lcom/squareup/shared/catalog/sync/WriteMessage;)V

    .line 48
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object v1

    .line 43
    invoke-virtual {p1, v0, v1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->executeSyncTask(Lcom/squareup/shared/catalog/sync/SyncTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    .line 50
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncResults;->empty()Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method
