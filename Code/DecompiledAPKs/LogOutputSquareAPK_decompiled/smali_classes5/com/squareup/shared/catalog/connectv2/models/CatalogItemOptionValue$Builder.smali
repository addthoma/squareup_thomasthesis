.class public Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;
.super Ljava/lang/Object;
.source "CatalogItemOptionValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

.field private itemOptionValue:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;-><init>()V

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_OPTION_VAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 58
    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    .line 59
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->itemOptionValue:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)V
    .locals 1

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iget-object v0, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    .line 65
    iget-object p1, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->itemOptionValue:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->itemOptionValue:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_option_value_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object v0

    .line 126
    new-instance v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    invoke-direct {v1, v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v1
.end method

.method public getColor()Ljava/lang/String;
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->itemOptionValue:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->color:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getItemOptionID()Ljava/lang/String;
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->itemOptionValue:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->item_option_id:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getItemVariationCount()J
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->itemOptionValue:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->item_variation_count:Ljava/lang/Long;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;->DEFAULT_ITEM_VARIATION_COUNT:Ljava/lang/Long;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->itemOptionValue:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getOrdinal()I
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->itemOptionValue:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->ordinal:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getUid()Ljava/lang/String;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->id:Ljava/lang/String;

    return-object v0
.end method

.method public setColor(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->itemOptionValue:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->color(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    return-object p0
.end method

.method public setItemOptionID(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->itemOptionValue:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->item_option_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    return-object p0
.end method

.method public setItemVariationCount(Ljava/lang/Long;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->itemOptionValue:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->item_variation_count(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->itemOptionValue:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    return-object p0
.end method

.method public setOrdinal(Ljava/lang/Integer;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->itemOptionValue:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;

    return-object p0
.end method
