.class public Lcom/squareup/shared/catalog/utils/ChainComparison;
.super Ljava/lang/Object;
.source "ChainComparison.java"


# static fields
.field private static EQUAL:Lcom/squareup/shared/catalog/utils/ChainComparison;

.field private static GREATER_THAN:Lcom/squareup/shared/catalog/utils/ChainComparison;

.field private static LESS_THAN:Lcom/squareup/shared/catalog/utils/ChainComparison;


# instance fields
.field private final result:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 8
    new-instance v0, Lcom/squareup/shared/catalog/utils/ChainComparison;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/utils/ChainComparison;-><init>(I)V

    sput-object v0, Lcom/squareup/shared/catalog/utils/ChainComparison;->EQUAL:Lcom/squareup/shared/catalog/utils/ChainComparison;

    .line 9
    new-instance v0, Lcom/squareup/shared/catalog/utils/ChainComparison;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/utils/ChainComparison;-><init>(I)V

    sput-object v0, Lcom/squareup/shared/catalog/utils/ChainComparison;->LESS_THAN:Lcom/squareup/shared/catalog/utils/ChainComparison;

    .line 10
    new-instance v0, Lcom/squareup/shared/catalog/utils/ChainComparison;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/utils/ChainComparison;-><init>(I)V

    sput-object v0, Lcom/squareup/shared/catalog/utils/ChainComparison;->GREATER_THAN:Lcom/squareup/shared/catalog/utils/ChainComparison;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput p1, p0, Lcom/squareup/shared/catalog/utils/ChainComparison;->result:I

    return-void
.end method

.method public static start()Lcom/squareup/shared/catalog/utils/ChainComparison;
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/shared/catalog/utils/ChainComparison;->EQUAL:Lcom/squareup/shared/catalog/utils/ChainComparison;

    return-object v0
.end method


# virtual methods
.method public compare(II)Lcom/squareup/shared/catalog/utils/ChainComparison;
    .locals 1

    .line 23
    iget v0, p0, Lcom/squareup/shared/catalog/utils/ChainComparison;->result:I

    if-nez v0, :cond_1

    if-ge p1, p2, :cond_0

    .line 25
    sget-object p1, Lcom/squareup/shared/catalog/utils/ChainComparison;->LESS_THAN:Lcom/squareup/shared/catalog/utils/ChainComparison;

    return-object p1

    :cond_0
    if-le p1, p2, :cond_1

    .line 27
    sget-object p1, Lcom/squareup/shared/catalog/utils/ChainComparison;->GREATER_THAN:Lcom/squareup/shared/catalog/utils/ChainComparison;

    return-object p1

    :cond_1
    return-object p0
.end method

.method public compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/shared/catalog/utils/ChainComparison;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable<",
            "TT;>;>(TT;TT;)",
            "Lcom/squareup/shared/catalog/utils/ChainComparison;"
        }
    .end annotation

    .line 34
    iget v0, p0, Lcom/squareup/shared/catalog/utils/ChainComparison;->result:I

    if-nez v0, :cond_1

    .line 35
    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result p1

    if-gez p1, :cond_0

    .line 37
    sget-object p1, Lcom/squareup/shared/catalog/utils/ChainComparison;->LESS_THAN:Lcom/squareup/shared/catalog/utils/ChainComparison;

    return-object p1

    :cond_0
    if-lez p1, :cond_1

    .line 39
    sget-object p1, Lcom/squareup/shared/catalog/utils/ChainComparison;->GREATER_THAN:Lcom/squareup/shared/catalog/utils/ChainComparison;

    return-object p1

    :cond_1
    return-object p0
.end method

.method public compareWithNull(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/shared/catalog/utils/ChainComparison;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable<",
            "TT;>;>(TT;TT;)",
            "Lcom/squareup/shared/catalog/utils/ChainComparison;"
        }
    .end annotation

    .line 47
    iget v0, p0, Lcom/squareup/shared/catalog/utils/ChainComparison;->result:I

    if-nez v0, :cond_4

    if-nez p1, :cond_1

    if-nez p2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    .line 50
    :cond_2
    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result p1

    :goto_0
    if-gez p1, :cond_3

    .line 52
    sget-object p1, Lcom/squareup/shared/catalog/utils/ChainComparison;->LESS_THAN:Lcom/squareup/shared/catalog/utils/ChainComparison;

    return-object p1

    :cond_3
    if-lez p1, :cond_4

    .line 54
    sget-object p1, Lcom/squareup/shared/catalog/utils/ChainComparison;->GREATER_THAN:Lcom/squareup/shared/catalog/utils/ChainComparison;

    return-object p1

    :cond_4
    return-object p0
.end method

.method public result()I
    .locals 1

    .line 61
    iget v0, p0, Lcom/squareup/shared/catalog/utils/ChainComparison;->result:I

    return v0
.end method
