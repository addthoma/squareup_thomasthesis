.class public Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;
.super Ljava/lang/Object;
.source "CatalogSyncLocal.java"


# instance fields
.field private final catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

.field private final locks:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private versionSyncInProgress:Z


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/CatalogStoreProvider;)V
    .locals 1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->locks:Ljava/util/Set;

    .line 28
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    return-void
.end method

.method private assertVersionSyncInProgress()V
    .locals 2

    .line 218
    iget-boolean v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->versionSyncInProgress:Z

    if-eqz v0, :cond_0

    return-void

    .line 219
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No Catalog sync in progress. No sync-related operations on CatalogStore is allowed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;
    .locals 1

    .line 208
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->assertNoVersionSyncInProgress()V

    .line 209
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->get()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    return-object v0
.end method

.method private getCatalogStoreInSync()Lcom/squareup/shared/catalog/CatalogStore;
    .locals 1

    .line 213
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->assertVersionSyncInProgress()V

    .line 214
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->get()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public applyVersionBatch(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;Z)V"
        }
    .end annotation

    .line 86
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->locked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 92
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "Catalog: Applying a batch of updates: cogs %s writes, %s deletes, connect v2 %s writes, %s deletes"

    .line 90
    invoke-static {v1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->getCatalogStoreInSync()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1, p2, p5}, Lcom/squareup/shared/catalog/CatalogStore;->applyCogsVersionBatch(Ljava/util/List;Ljava/util/List;Z)V

    .line 94
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->getCatalogStoreInSync()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object p1

    invoke-interface {p1, p3, p4}, Lcom/squareup/shared/catalog/CatalogStore;->applyConnectV2Batch(Ljava/util/List;Ljava/util/List;)V

    return-void

    .line 87
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "applyVersionBatch() must not be called when Catalog is locked."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public assertNoVersionSyncInProgress()V
    .locals 2

    .line 104
    iget-boolean v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->versionSyncInProgress:Z

    if-nez v0, :cond_0

    return-void

    .line 105
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Catalog sync in progress. No non-sync related operations on CatalogStore is allowed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public beginVersionSync(JLjava/util/List;Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;)Z"
        }
    .end annotation

    .line 41
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->locked()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array p1, v1, [Ljava/lang/Object;

    const-string p2, "Catalog: Ignoring apply server version. Catalog is locked."

    .line 42
    invoke-static {p2, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return v1

    .line 46
    :cond_0
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    .line 47
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/shared/catalog/CatalogStore;->beginVersionSync(JLjava/util/List;Ljava/util/List;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 48
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->assertNoVersionSyncInProgress()V

    const/4 p3, 0x1

    .line 49
    iput-boolean p3, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->versionSyncInProgress:Z

    new-array p4, p3, [Ljava/lang/Object;

    .line 50
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    aput-object p1, p4, v1

    const-string p1, "Catalog: Applying server version %s."

    invoke-static {p1, p4}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return p3

    :cond_1
    return v1
.end method

.method buildSyntheticTablesFromLocal(Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;)V
    .locals 1

    .line 204
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/CatalogStore;->rebuildSyntheticTables(Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;)V

    return-void
.end method

.method deletePendingWriteRequest(J)V
    .locals 1

    .line 135
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->locked()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 136
    :cond_0
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogStore;->deletePendingWriteRequest(J)V

    return-void
.end method

.method public endVersionSync(JZ)V
    .locals 2

    .line 64
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->locked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->getCatalogStoreInSync()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/squareup/shared/catalog/CatalogStore;->endVersionSync(JZLjava/util/Date;)V

    .line 69
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->assertVersionSyncInProgress()V

    const/4 p3, 0x0

    .line 70
    iput-boolean p3, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->versionSyncInProgress:Z

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 71
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    aput-object p1, v0, p3

    const-string p1, "Catalog: End applying server version %s."

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 65
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "endVersionSync() must not be called when Catalog is locked."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public hasAppliedServerVersion()Z
    .locals 1

    .line 99
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogStore;->readLastSyncTimestamp()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method lock(Ljava/lang/Object;)V
    .locals 2

    .line 168
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->locks:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 169
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->locks:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p1, v1

    const-string v0, "Catalog now has %d locks"

    invoke-static {v0, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method locked()Z
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->locks:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method readAllSyncedConnectV2ObjectTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;"
        }
    .end annotation

    .line 157
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogStore;->readAllSyncedConnectV2ObjectTypes()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method readCogsAppliedServerVersion()J
    .locals 2

    .line 141
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogStore;->readCogsAppliedServerVersion()J

    move-result-wide v0

    return-wide v0
.end method

.method readLastSyncTimestamp()Ljava/util/Date;
    .locals 1

    .line 149
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogStore;->readLastSyncTimestamp()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method readPendingWriteRequests()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/PendingWriteRequest;",
            ">;"
        }
    .end annotation

    .line 130
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogStore;->readPendingWriteRequests()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method readSessionId()Ljava/lang/Long;
    .locals 1

    .line 112
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogStore;->readSessionId()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method readVersionSyncIncomplete()Z
    .locals 1

    .line 126
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogStore;->readVersionSyncIncomplete()Z

    move-result v0

    return v0
.end method

.method requiresSyntheticTableRebuild()Z
    .locals 1

    .line 199
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogStore;->requiresSyntheticTableRebuild()Z

    move-result v0

    return v0
.end method

.method resetAndThrow(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->resetAndThrow(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method unlock(Ljava/lang/Object;)V
    .locals 2

    .line 180
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->locks:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->locks:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    .line 184
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->locks:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v0

    const-string v0, "Catalog now has %d locks"

    invoke-static {v0, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 181
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Lock already released."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method writeSessionId(J)V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Catalog: Writing session ID to storage."

    .line 117
    invoke-static {v1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 118
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogStore;->writeSessionId(J)V

    return-void
.end method
