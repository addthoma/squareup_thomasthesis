.class public interface abstract Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;
.super Ljava/lang/Object;
.source "CatalogConnectV2Endpoint.java"


# virtual methods
.method public abstract batchRetrieveCatalogObjects(Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract batchUpsert(Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract delete(Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract searchCatalogObjects(Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract upsert(Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;",
            ">;"
        }
    .end annotation
.end method
