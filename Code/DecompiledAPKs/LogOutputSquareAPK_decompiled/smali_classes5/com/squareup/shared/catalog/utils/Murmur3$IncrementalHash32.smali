.class public Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;
.super Ljava/lang/Object;
.source "Murmur3.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/utils/Murmur3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IncrementalHash32"
.end annotation


# instance fields
.field hash:I

.field tail:[B

.field tailLen:I

.field totalLen:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [B

    .line 389
    iput-object v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->tail:[B

    return-void
.end method


# virtual methods
.method public final add([BII)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    if-nez v3, :cond_0

    return-void

    .line 401
    :cond_0
    iget v4, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->totalLen:I

    add-int/2addr v4, v3

    iput v4, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->totalLen:I

    .line 402
    iget v4, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->tailLen:I

    add-int v5, v4, v3

    const/4 v6, 0x4

    if-ge v5, v6, :cond_1

    .line 403
    iget-object v5, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->tail:[B

    invoke-static {v1, v2, v5, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 404
    iget v1, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->tailLen:I

    add-int/2addr v1, v3

    iput v1, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->tailLen:I

    return-void

    :cond_1
    const v5, -0x19ab949c

    const/16 v7, 0xd

    const v8, 0x1b873593

    const/16 v9, 0xf

    const v10, -0x3361d2af    # -8.2930312E7f

    const/4 v11, 0x3

    const/4 v12, 0x0

    const/4 v13, 0x2

    if-lez v4, :cond_5

    sub-int/2addr v6, v4

    const/4 v14, 0x1

    if-eq v4, v14, :cond_4

    if-eq v4, v13, :cond_3

    if-ne v4, v11, :cond_2

    .line 419
    iget-object v4, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->tail:[B

    aget-byte v15, v4, v12

    aget-byte v14, v4, v14

    aget-byte v4, v4, v13

    aget-byte v13, v1, v2

    invoke-static {v15, v14, v4, v13}, Lcom/squareup/shared/catalog/utils/Murmur3;->access$000(BBBB)I

    move-result v4

    goto :goto_0

    .line 421
    :cond_2
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v4}, Ljava/lang/AssertionError;-><init>(I)V

    throw v1

    .line 416
    :cond_3
    iget-object v4, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->tail:[B

    aget-byte v13, v4, v12

    aget-byte v4, v4, v14

    aget-byte v14, v1, v2

    add-int/lit8 v15, v2, 0x1

    aget-byte v15, v1, v15

    invoke-static {v13, v4, v14, v15}, Lcom/squareup/shared/catalog/utils/Murmur3;->access$000(BBBB)I

    move-result v4

    goto :goto_0

    .line 413
    :cond_4
    iget-object v4, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->tail:[B

    aget-byte v4, v4, v12

    aget-byte v13, v1, v2

    add-int/lit8 v14, v2, 0x1

    aget-byte v14, v1, v14

    add-int/lit8 v15, v2, 0x2

    aget-byte v15, v1, v15

    invoke-static {v4, v13, v14, v15}, Lcom/squareup/shared/catalog/utils/Murmur3;->access$000(BBBB)I

    move-result v4

    :goto_0
    mul-int v4, v4, v10

    .line 425
    invoke-static {v4, v9}, Ljava/lang/Integer;->rotateLeft(II)I

    move-result v4

    mul-int v4, v4, v8

    .line 427
    iget v13, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    xor-int/2addr v4, v13

    iput v4, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    .line 428
    iget v4, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    invoke-static {v4, v7}, Ljava/lang/Integer;->rotateLeft(II)I

    move-result v4

    mul-int/lit8 v4, v4, 0x5

    add-int/2addr v4, v5

    iput v4, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    goto :goto_1

    :cond_5
    const/4 v6, 0x0

    :goto_1
    sub-int/2addr v3, v6

    add-int/2addr v2, v6

    shr-int/lit8 v4, v3, 0x2

    const/4 v6, 0x0

    :goto_2
    if-ge v6, v4, :cond_6

    shl-int/lit8 v13, v6, 0x2

    add-int/2addr v13, v2

    .line 436
    aget-byte v14, v1, v13

    add-int/lit8 v15, v13, 0x1

    aget-byte v15, v1, v15

    add-int/lit8 v16, v13, 0x2

    aget-byte v12, v1, v16

    add-int/2addr v13, v11

    aget-byte v13, v1, v13

    invoke-static {v14, v15, v12, v13}, Lcom/squareup/shared/catalog/utils/Murmur3;->access$000(BBBB)I

    move-result v12

    mul-int v12, v12, v10

    .line 440
    invoke-static {v12, v9}, Ljava/lang/Integer;->rotateLeft(II)I

    move-result v12

    mul-int v12, v12, v8

    .line 442
    iget v13, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    xor-int/2addr v12, v13

    iput v12, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    .line 443
    iget v12, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    invoke-static {v12, v7}, Ljava/lang/Integer;->rotateLeft(II)I

    move-result v12

    mul-int/lit8 v12, v12, 0x5

    add-int/2addr v12, v5

    iput v12, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    add-int/lit8 v6, v6, 0x1

    const/4 v12, 0x0

    goto :goto_2

    :cond_6
    const/4 v6, 0x2

    shl-int/2addr v4, v6

    sub-int v5, v3, v4

    .line 447
    iput v5, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->tailLen:I

    if-ne v4, v3, :cond_7

    return-void

    :cond_7
    add-int/2addr v2, v4

    .line 449
    iget-object v3, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->tail:[B

    iget v4, v0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->tailLen:I

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method public final end()I
    .locals 5

    .line 454
    iget v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->tailLen:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_2

    const/4 v3, 0x2

    if-eq v0, v3, :cond_1

    const/4 v4, 0x3

    if-eq v0, v4, :cond_0

    goto :goto_2

    .line 456
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->tail:[B

    aget-byte v0, v0, v3

    shl-int/lit8 v0, v0, 0x10

    xor-int/2addr v0, v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 458
    :goto_0
    iget-object v3, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->tail:[B

    aget-byte v1, v3, v1

    shl-int/lit8 v1, v1, 0x8

    xor-int/2addr v0, v1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    .line 460
    :goto_1
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->tail:[B

    aget-byte v1, v1, v2

    xor-int/2addr v0, v1

    const v1, -0x3361d2af    # -8.2930312E7f

    mul-int v0, v0, v1

    const/16 v1, 0xf

    .line 464
    invoke-static {v0, v1}, Ljava/lang/Integer;->rotateLeft(II)I

    move-result v0

    const v1, 0x1b873593

    mul-int v0, v0, v1

    .line 466
    iget v1, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    xor-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    .line 470
    :goto_2
    iget v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    iget v1, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->totalLen:I

    xor-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    .line 471
    iget v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    .line 472
    iget v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    const v1, -0x7a143595

    mul-int v0, v0, v1

    iput v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    .line 473
    iget v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    ushr-int/lit8 v1, v0, 0xd

    xor-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    .line 474
    iget v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    const v1, -0x3d4d51cb

    mul-int v0, v0, v1

    iput v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    .line 475
    iget v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    .line 476
    iget v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    return v0
.end method

.method public final start(I)V
    .locals 1

    const/4 v0, 0x0

    .line 395
    iput v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->totalLen:I

    iput v0, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->tailLen:I

    .line 396
    iput p1, p0, Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;->hash:I

    return-void
.end method
