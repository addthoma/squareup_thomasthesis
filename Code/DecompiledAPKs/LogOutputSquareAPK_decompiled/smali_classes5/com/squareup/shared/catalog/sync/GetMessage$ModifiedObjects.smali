.class Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;
.super Ljava/lang/Object;
.source "GetMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/sync/GetMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ModifiedObjects"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final deleted:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final updated:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    .line 529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    goto :goto_0

    .line 530
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->updated:Ljava/util/List;

    if-eqz p2, :cond_1

    goto :goto_1

    .line 531
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p2

    :goto_1
    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->deleted:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/shared/catalog/sync/GetMessage$1;)V
    .locals 0

    .line 525
    invoke-direct {p0, p1, p2}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;)Z
    .locals 0

    .line 525
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->isEmpty()Z

    move-result p0

    return p0
.end method

.method static synthetic access$1500(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;)V
    .locals 0

    .line 525
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->merge(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;)I
    .locals 0

    .line 525
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->size()I

    move-result p0

    return p0
.end method

.method static synthetic access$400(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;)Ljava/util/List;
    .locals 0

    .line 525
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->updated:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;)Ljava/util/List;
    .locals 0

    .line 525
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->deleted:Ljava/util/List;

    return-object p0
.end method

.method private isEmpty()Z
    .locals 1

    .line 540
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private merge(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects<",
            "TT;>;)V"
        }
    .end annotation

    .line 535
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->updated:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->updated:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 536
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->deleted:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->deleted:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method private size()I
    .locals 2

    .line 544
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->updated:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->deleted:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
