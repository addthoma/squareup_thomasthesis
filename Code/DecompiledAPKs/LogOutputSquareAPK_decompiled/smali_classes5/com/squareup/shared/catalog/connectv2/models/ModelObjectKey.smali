.class public final Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;
.super Ljava/lang/Object;
.source "ModelObjectKey.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey<",
        "TT;>;>;"
    }
.end annotation


# instance fields
.field private final id:Ljava/lang/String;

.field private final type:Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->type:Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;

    .line 21
    iput-object p2, p0, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->id:Ljava/lang/String;

    return-void
.end method

.method public static create(Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;",
            ">(TT;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey<",
            "TT;>;"
        }
    .end annotation

    .line 16
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    invoke-direct {v0, p0, p1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;-><init>(Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public compareTo(Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey<",
            "TT;>;)I"
        }
    .end annotation

    if-ne p0, p1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->getType()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->getType()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;->compareTo(Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;)I

    move-result v0

    :goto_0
    if-eqz v0, :cond_1

    return v0

    .line 50
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->compareTo(Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 36
    :cond_0
    instance-of v1, p1, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 40
    :cond_1
    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    .line 41
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->getType()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->getType()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;->compareTo(Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;)I

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->type:Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;

    return-object v0
.end method
