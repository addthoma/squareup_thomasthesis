.class public Lcom/squareup/shared/catalog/SqliteCatalogStore;
.super Ljava/lang/Object;
.source "SqliteCatalogStore.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogStore;


# static fields
.field private static final DEFAULT_BATCH_SIZE:I = 0x2710

.field private static final DROP_TABLE_IF_EXISTS:Ljava/lang/String; = "DROP TABLE IF EXISTS ?"

.field private static final MAX_PARAMETER_COUNT:I = 0x3e7


# instance fields
.field private final clock:Lcom/squareup/shared/catalog/logging/Clock;

.field private final helper:Lcom/squareup/shared/sql/DatabaseHelper;

.field private isVersionSyncInProgress:Z

.field private requiresSyntheticTableRebuild:Ljava/lang/Boolean;

.field private final storeUpdateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

.field private final syntheticTables:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;"
        }
    .end annotation
.end field

.field private syntheticTablesToDrop:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private syntheticTablesToRebuild:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/sql/DatabaseHelper;Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;Ljava/util/Set;Lcom/squareup/shared/catalog/logging/Clock;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/DatabaseHelper;",
            "Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;",
            "Lcom/squareup/shared/catalog/logging/Clock;",
            ")V"
        }
    .end annotation

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTablesToRebuild:Ljava/util/Set;

    .line 94
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTablesToDrop:Ljava/util/Set;

    .line 101
    iput-object p1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    .line 102
    iput-object p2, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->storeUpdateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

    .line 103
    iput-object p3, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTables:Ljava/util/Set;

    .line 104
    iput-object p4, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    return-void
.end method

.method static synthetic access$000([BLcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 0

    .line 60
    invoke-static {p0, p1}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->parseBlob([BLcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p0

    return-object p0
.end method

.method private addObjectToUpdatedCatalogObjectBuilder(Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;Lcom/squareup/shared/catalog/models/CatalogObject;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;Z)V"
        }
    .end annotation

    .line 506
    sget-object v0, Lcom/squareup/shared/catalog/SqliteCatalogStore$2;->$SwitchMap$com$squareup$api$items$Type:[I

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogObject;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->getProtoObjectType()Lcom/squareup/api/items/Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/api/items/Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 576
    :pswitch_0
    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    .line 577
    iget-object p1, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedTimePeriodsById:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getId()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 565
    :pswitch_1
    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    .line 566
    iget-object p3, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedTicketTemplatesByTicketGroupId:Ljava/util/Map;

    .line 567
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getTicketGroupId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/List;

    if-nez p3, :cond_0

    .line 569
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 570
    iget-object p1, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedTicketTemplatesByTicketGroupId:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getTicketGroupId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 573
    :cond_0
    invoke-interface {p3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 561
    :pswitch_2
    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;

    .line 562
    iget-object p1, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedTicketGroupsById:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->getId()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 545
    :pswitch_3
    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;

    .line 546
    iget-object p3, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedItemModifierOptionsByModifierListId:Ljava/util/Map;

    .line 548
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getModifierListId()Ljava/lang/String;

    move-result-object v0

    .line 547
    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/List;

    if-nez p3, :cond_1

    .line 550
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 551
    iget-object p1, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedItemModifierOptionsByModifierListId:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getModifierListId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 554
    :cond_1
    invoke-interface {p3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 541
    :pswitch_4
    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    .line 542
    iget-object p1, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedItemModifierListsById:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 557
    :pswitch_5
    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    .line 558
    iget-object p1, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedPricingRulesById:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getId()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 533
    :pswitch_6
    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 534
    iget-object p1, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedCategoriesById:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getId()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 537
    :pswitch_7
    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 538
    iget-object p1, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedDiscountsById:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getId()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 523
    :pswitch_8
    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 524
    iget-object p3, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedVariationsByItemId:Ljava/util/Map;

    .line 525
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getItemId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/List;

    if-nez p3, :cond_2

    .line 527
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 528
    iget-object p1, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedVariationsByItemId:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getItemId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 530
    :cond_2
    invoke-interface {p3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 512
    :pswitch_9
    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItem;

    .line 513
    iget-object v0, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedItemsById:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p3, :cond_3

    .line 517
    iget-object p3, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedVariationsByItemId:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    if-nez p3, :cond_3

    .line 518
    iget-object p1, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedVariationsByItemId:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object p2

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 508
    :pswitch_a
    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemImage;

    .line 509
    iget-object p1, p1, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->updatedImagesById:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemImage;->getId()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private applyConnectV2Deletes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
            "*>;>;)V"
        }
    .end annotation

    .line 440
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 443
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide v0

    .line 446
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;

    .line 447
    invoke-static {p1, v3}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->deleteConnectV2ObjectAndRelations(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/connectv2/models/ModelObject;)V

    goto :goto_0

    .line 449
    :cond_1
    iget-object p1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {p1}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide v2

    const/4 p1, 0x2

    new-array v4, p1, [Ljava/lang/Object;

    .line 450
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    sub-long/2addr v2, v0

    .line 451
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v4, v3

    const-string v2, "Deleted %s objects from core tables in %s ms"

    .line 450
    invoke-static {v2, v4}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 452
    iget-object v2, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v2}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide v4

    new-array p1, p1, [Ljava/lang/Object;

    .line 453
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p1, v6

    sub-long/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    aput-object p2, p1, v3

    const-string p2, "Overall deleted %s items in %s ms"

    invoke-static {p2, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private applyConnectV2ObjectChangesToDb(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
            "*>;>;)V"
        }
    .end annotation

    .line 338
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->storeUpdateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

    invoke-interface {v0, p2, p3}, Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;->updateConnectV2Objects(Ljava/util/Collection;Ljava/util/Collection;)V

    .line 340
    invoke-direct {p0, p1, p3}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->applyConnectV2Deletes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;)V

    .line 341
    invoke-direct {p0, p1, p2}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->applyConnectV2Updates(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;)V

    return-void
.end method

.method private applyConnectV2Updates(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
            "*>;>;)V"
        }
    .end annotation

    .line 487
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 490
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide v0

    .line 494
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;

    .line 495
    invoke-static {p1, v3}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->writeConnectV2Object(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/connectv2/models/ModelObject;)V

    goto :goto_0

    .line 497
    :cond_1
    iget-object p1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {p1}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide v2

    const/4 p1, 0x2

    new-array v4, p1, [Ljava/lang/Object;

    .line 498
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    sub-long/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v4, v3

    const-string v2, "Updated %s objects in core tables in %s ms"

    invoke-static {v2, v4}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 500
    iget-object v2, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v2}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide v4

    new-array p1, p1, [Ljava/lang/Object;

    .line 501
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p1, v6

    sub-long/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    aput-object p2, p1, v3

    const-string p2, "Overall updated %s items in %s ms"

    invoke-static {p2, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private applyDeletes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;Z)V"
        }
    .end annotation

    .line 346
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide v0

    .line 350
    new-instance v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;

    invoke-direct {v2, p3}, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;-><init>(Z)V

    .line 352
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_1
    :goto_0
    :pswitch_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 353
    sget-object v4, Lcom/squareup/shared/catalog/SqliteCatalogStore$2;->$SwitchMap$com$squareup$api$items$Type:[I

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogObject;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->getProtoObjectType()Lcom/squareup/api/items/Type;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/api/items/Type;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 416
    :pswitch_1
    iget-object v4, v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedTimePeriodIds:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 396
    :pswitch_2
    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    .line 397
    iget-object v4, v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedTicketTemplateIds:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 399
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->hasObjectExtension()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 400
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getTicketGroupId()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 404
    :cond_2
    sget-object v4, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_TICKET_TEMPLATE_TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 405
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v4, v3}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->findFirstReferentId(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    if-eqz v3, :cond_1

    .line 410
    iget-object v4, v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedTicketGroupIds:Ljava/util/Set;

    .line 411
    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 412
    iget-object v4, v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->updatedTicketTemplateByGroupIds:Ljava/util/Map;

    sget-object v5, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 392
    :pswitch_3
    iget-object v4, v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedTicketGroupIds:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 393
    iget-object v4, v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->updatedTicketTemplateByGroupIds:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 389
    :pswitch_4
    iget-object v4, v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedModifierOptionIds:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 384
    :pswitch_5
    iget-object v4, v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedPricingRuleIds:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 386
    :pswitch_6
    iget-object v4, v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedModifierListIds:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 381
    :pswitch_7
    iget-object v4, v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedCategoryIds:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 378
    :pswitch_8
    iget-object v4, v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedDiscountIds:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 361
    :pswitch_9
    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 362
    iget-object v4, v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedVariations:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 363
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->hasObjectExtension()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 364
    iget-object v4, v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedVariationItemIds:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v5

    .line 365
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getItemId()Ljava/lang/String;

    move-result-object v3

    .line 364
    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 369
    :cond_3
    sget-object v4, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_VARIATION_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v4, v5}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->findFirstReferentId(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 371
    iget-object v5, v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedVariationItemIds:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 373
    :cond_4
    iget-object v4, v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedVariationItemIds:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v3

    const-string v5, ""

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 358
    :pswitch_a
    iget-object v4, v2, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedItemIds:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 423
    :cond_5
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_2
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 424
    invoke-static {p1, v3}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->deleteObjectAndRelations(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogObject;)V

    goto :goto_2

    .line 426
    :cond_6
    iget-object p3, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {p3}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide v3

    const/4 p3, 0x2

    new-array v5, p3, [Ljava/lang/Object;

    .line 427
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v5, v7

    sub-long/2addr v3, v0

    .line 428
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v5, v4

    const-string v3, "Deleted %s objects from core tables in %s ms"

    .line 427
    invoke-static {v3, v5}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 430
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->build()Lcom/squareup/shared/catalog/DeletedCatalogObjects;

    move-result-object v2

    .line 431
    iget-object v3, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTables:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;

    .line 432
    invoke-interface {v5, p1, v2}, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;->applyDeletes(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/DeletedCatalogObjects;)V

    goto :goto_3

    .line 435
    :cond_7
    iget-object p1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {p1}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide v2

    new-array p1, p3, [Ljava/lang/Object;

    .line 436
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p1, v7

    sub-long/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    aput-object p2, p1, v4

    const-string p2, "Overall deleted %s items in %s ms"

    invoke-static {p2, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private applyObjectChangesToDb(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;Ljava/util/Collection;ZZZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;ZZZ)V"
        }
    .end annotation

    .line 325
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->storeUpdateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

    invoke-interface {v0, p2, p3}, Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;->updateCogsObjects(Ljava/util/Collection;Ljava/util/Collection;)V

    .line 327
    invoke-direct {p0, p1, p3, p6}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->applyDeletes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;Z)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p4

    move v4, p5

    move v5, p6

    .line 328
    invoke-direct/range {v0 .. v5}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->applyUpdates(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;ZZZ)V

    return-void
.end method

.method private applyUpdates(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;ZZZ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;ZZZ)V"
        }
    .end annotation

    .line 458
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 461
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide v0

    .line 462
    new-instance v2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;

    invoke-direct {v2, p5}, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;-><init>(Z)V

    .line 469
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p5

    :goto_0
    invoke-interface {p5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 470
    invoke-static {p1, v3}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->writeObject(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogObject;)V

    .line 471
    invoke-static {p1, v3}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->writeRelations(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogObject;)V

    .line 472
    invoke-direct {p0, v2, v3, p4}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->addObjectToUpdatedCatalogObjectBuilder(Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;Lcom/squareup/shared/catalog/models/CatalogObject;Z)V

    goto :goto_0

    .line 474
    :cond_1
    iget-object p5, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {p5}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide v3

    const/4 p5, 0x2

    new-array v5, p5, [Ljava/lang/Object;

    .line 475
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v5, v7

    sub-long/2addr v3, v0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v5, v4

    const-string v3, "Updated %s objects in core tables in %s ms"

    invoke-static {v3, v5}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 477
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->build()Lcom/squareup/shared/catalog/UpdatedCatalogObjects;

    move-result-object v2

    .line 478
    iget-object v3, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTables:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;

    .line 479
    invoke-interface {v5, p1, v2, p3, p4}, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;->applyUpdates(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/UpdatedCatalogObjects;ZZ)V

    goto :goto_1

    .line 482
    :cond_2
    iget-object p1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {p1}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide p3

    new-array p1, p5, [Ljava/lang/Object;

    .line 483
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p1, v7

    sub-long/2addr p3, v0

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    aput-object p2, p1, v4

    const-string p2, "Overall updated %s items in %s ms"

    invoke-static {p2, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private static assertHasMerchantCatalogToken(Lcom/squareup/shared/catalog/models/CatalogObjectType;)V
    .locals 3

    .line 772
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->doesProtoHaveMerchantCatalogToken()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 773
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Merchant catalog object token is not yet exposed in the proto: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 775
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->getExtension()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ". If necessary, please update the proto and override method getMerchantCatalogObjectToken in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 777
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->getCatalogItemClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static countReferrers(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/lang/String;)I
    .locals 2

    .line 992
    iget-object v0, p1, Lcom/squareup/shared/catalog/models/CatalogRelation;->referrerType:Lcom/squareup/api/items/Type;

    invoke-virtual {v0}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v0

    .line 993
    iget-object p1, p1, Lcom/squareup/shared/catalog/models/CatalogRelation;->referentType:Lcom/squareup/api/items/Type;

    invoke-virtual {p1}, Lcom/squareup/api/items/Type;->getValue()I

    move-result p1

    .line 994
    invoke-static {}, Lcom/squareup/shared/catalog/ReferencesTable;->instance()Lcom/squareup/shared/catalog/ReferencesTable;

    move-result-object v1

    invoke-virtual {v1, p0, v0, p1, p2}, Lcom/squareup/shared/catalog/ReferencesTable;->countReferrers(Lcom/squareup/shared/sql/SQLDatabase;IILjava/lang/String;)I

    move-result p0

    return p0
.end method

.method private static deleteConnectV2ObjectAndRelations(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/connectv2/models/ModelObject;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
            "*>;)V"
        }
    .end annotation

    .line 1317
    invoke-static {}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->instance()Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;->getKey()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->delete(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V

    .line 1318
    invoke-static {}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ReferencesTable;->instance()Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ReferencesTable;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;->getKey()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ReferencesTable;->deleteAllReferences(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V

    return-void
.end method

.method private static deleteObjectAndRelations(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogObject;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;)V"
        }
    .end annotation

    .line 1312
    invoke-static {}, Lcom/squareup/shared/catalog/ObjectsTable;->instance()Lcom/squareup/shared/catalog/ObjectsTable;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/squareup/shared/catalog/ObjectsTable;->delete(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V

    .line 1313
    invoke-static {}, Lcom/squareup/shared/catalog/ReferencesTable;->instance()Lcom/squareup/shared/catalog/ReferencesTable;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, Lcom/squareup/shared/catalog/ReferencesTable;->deleteAllReferences(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V

    return-void
.end method

.method private deleteUnsupportedConnectV2ObjectTypes(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;)V"
        }
    .end annotation

    .line 1261
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 1262
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 1263
    invoke-static {}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->instance()Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->getValue()I

    move-result v1

    int-to-long v3, v1

    invoke-virtual {v2, v0, v3, v4}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->delete(Lcom/squareup/shared/sql/SQLDatabase;J)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private enqueue(Lcom/squareup/api/rpc/Request;)V
    .locals 5

    .line 214
    invoke-static {p1}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->requireSessionState(Lcom/squareup/api/rpc/Request;)V

    .line 215
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 218
    iget-object v1, p1, Lcom/squareup/api/rpc/Request;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    iget-object v1, v1, Lcom/squareup/api/sync/WritableSessionState;->seq:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 219
    invoke-static {}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable;->instance()Lcom/squareup/shared/catalog/PendingWriteRequestsTable;

    move-result-object v3

    sget-object v4, Lcom/squareup/api/rpc/Request;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p1

    invoke-virtual {v3, v0, v1, v2, p1}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable;->enqueue(Lcom/squareup/shared/sql/SQLDatabase;J[B)V

    return-void
.end method

.method private static findFirstReferentId(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 1000
    iget-object v0, p1, Lcom/squareup/shared/catalog/models/CatalogRelation;->referrerType:Lcom/squareup/api/items/Type;

    invoke-virtual {v0}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v0

    .line 1001
    iget-object p1, p1, Lcom/squareup/shared/catalog/models/CatalogRelation;->referentType:Lcom/squareup/api/items/Type;

    invoke-virtual {p1}, Lcom/squareup/api/items/Type;->getValue()I

    move-result p1

    .line 1002
    invoke-static {}, Lcom/squareup/shared/catalog/ReferencesTable;->instance()Lcom/squareup/shared/catalog/ReferencesTable;

    move-result-object v1

    .line 1003
    invoke-virtual {v1, p0, v0, p1, p2}, Lcom/squareup/shared/catalog/ReferencesTable;->findFirstReferentId(Lcom/squareup/shared/sql/SQLDatabase;IILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static findReferrers(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 1010
    :try_start_0
    iget-object v1, p1, Lcom/squareup/shared/catalog/models/CatalogRelation;->referrerType:Lcom/squareup/api/items/Type;

    invoke-virtual {v1}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v1

    .line 1011
    iget-object v2, p1, Lcom/squareup/shared/catalog/models/CatalogRelation;->referentType:Lcom/squareup/api/items/Type;

    invoke-virtual {v2}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v2

    .line 1013
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v4, 0x0

    move-object v5, v0

    const/4 v0, 0x0

    .line 1014
    :goto_0
    :try_start_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_1

    .line 1016
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    sub-int/2addr v6, v0

    const/16 v7, 0x3e5

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    add-int/2addr v6, v0

    .line 1017
    invoke-interface {p2, v0, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 1019
    invoke-static {}, Lcom/squareup/shared/catalog/ReferencesTable;->instance()Lcom/squareup/shared/catalog/ReferencesTable;

    move-result-object v7

    invoke-virtual {v7, p0, v1, v2, v0}, Lcom/squareup/shared/catalog/ReferencesTable;->findReferrers(Lcom/squareup/shared/sql/SQLDatabase;IILjava/util/List;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v5

    .line 1021
    :goto_1
    invoke-interface {v5}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1022
    invoke-interface {v5, v4}, Lcom/squareup/shared/sql/SQLCursor;->getBlob(I)[B

    move-result-object v0

    .line 1023
    sget-object v7, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v7, v0}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectWrapper;

    .line 1024
    iget-object v7, p1, Lcom/squareup/shared/catalog/models/CatalogRelation;->referrerType:Lcom/squareup/api/items/Type;

    invoke-static {v7}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeFromProtoType(Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v0

    .line 1025
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    move v0, v6

    goto :goto_0

    .line 1028
    :cond_1
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v5, :cond_2

    .line 1033
    invoke-interface {v5}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_2
    return-object p0

    :catchall_0
    move-exception p0

    goto :goto_3

    :catch_0
    move-exception p0

    move-object v0, v5

    goto :goto_2

    :catchall_1
    move-exception p0

    move-object v5, v0

    goto :goto_3

    :catch_1
    move-exception p0

    .line 1030
    :goto_2
    :try_start_2
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_3
    if-eqz v5, :cond_3

    .line 1033
    invoke-interface {v5}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_3
    throw p0
.end method

.method private insertSupportedConnectV2ObjectTypes(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;)V"
        }
    .end annotation

    .line 1269
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 1271
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 1273
    invoke-static {}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->instance()Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->getValue()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v2, v0, v3, v4}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->deleteAllObjectsWithType(Lcom/squareup/shared/sql/SQLDatabase;J)V

    .line 1274
    invoke-static {}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->instance()Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->getValue()I

    move-result v1

    int-to-long v3, v1

    invoke-virtual {v2, v0, v3, v4}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->insert(Lcom/squareup/shared/sql/SQLDatabase;J)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static parseBlob([BLcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>([B",
            "Lcom/squareup/api/items/Type;",
            ")TT;"
        }
    .end annotation

    .line 936
    :try_start_0
    sget-object v0, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p0}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/sync/ObjectWrapper;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 940
    invoke-static {p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeFromProtoType(Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p0

    return-object p0

    :catch_0
    move-exception p0

    .line 938
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p1
.end method

.method private parseModelObjectFromCursor(Lcom/squareup/shared/sql/SQLCursor;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">(",
            "Lcom/squareup/shared/sql/SQLCursor;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 662
    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLCursor;->getLong(I)J

    const/4 v0, 0x1

    .line 663
    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLCursor;->getBlob(I)[B

    move-result-object p1

    .line 665
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;-><init>()V

    .line 666
    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->parse([B)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object p1

    return-object p1
.end method

.method private static readById(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/Class;Ljava/lang/String;Z)Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            "Z)TT;"
        }
    .end annotation

    .line 721
    invoke-static {p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeFromObject(Ljava/lang/Class;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p1

    .line 722
    invoke-static {}, Lcom/squareup/shared/catalog/ObjectsTable;->instance()Lcom/squareup/shared/catalog/ObjectsTable;

    move-result-object v0

    invoke-virtual {v0, p0, p2}, Lcom/squareup/shared/catalog/ObjectsTable;->selectObjectById(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p0

    .line 724
    :try_start_0
    invoke-interface {p0}, Lcom/squareup/shared/sql/SQLCursor;->getCount()I

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    if-nez p3, :cond_0

    const/4 p1, 0x0

    .line 743
    invoke-interface {p0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object p1

    .line 726
    :cond_0
    :try_start_1
    new-instance p3, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Found "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 727
    invoke-interface {p0}, Lcom/squareup/shared/sql/SQLCursor;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 729
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " for id "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p3

    .line 736
    :cond_1
    invoke-interface {p0}, Lcom/squareup/shared/sql/SQLCursor;->moveToFirst()Z

    const/4 p2, 0x0

    .line 737
    invoke-interface {p0, p2}, Lcom/squareup/shared/sql/SQLCursor;->getBlob(I)[B

    move-result-object p2

    .line 738
    sget-object p3, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {p3, p2}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/api/sync/ObjectWrapper;

    .line 739
    invoke-virtual {p1, p2}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 743
    invoke-interface {p0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 741
    :try_start_2
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 743
    :goto_0
    invoke-interface {p0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw p1
.end method

.method public static readByIdOrNull(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 676
    invoke-static {p0, p1, p2, v0}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->readById(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/Class;Ljava/lang/String;Z)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p0

    return-object p0
.end method

.method public static readByIds(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation

    .line 811
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 812
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 815
    :goto_0
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 816
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v3

    sub-int/2addr v3, v2

    const/16 v4, 0x3e7

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 818
    invoke-static {}, Lcom/squareup/shared/catalog/ObjectsTable;->instance()Lcom/squareup/shared/catalog/ObjectsTable;

    move-result-object v4

    add-int/2addr v3, v2

    .line 819
    invoke-interface {v0, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v4, p0, v2}, Lcom/squareup/shared/catalog/ObjectsTable;->selectObjectsByIds(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v2

    .line 823
    :goto_1
    :try_start_0
    invoke-interface {v2}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 824
    invoke-interface {v2, v1}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    .line 825
    invoke-interface {v2, v5}, Lcom/squareup/shared/sql/SQLCursor;->getBlob(I)[B

    move-result-object v5

    .line 826
    sget-object v6, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v6, v5}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/api/sync/ObjectWrapper;

    .line 827
    invoke-static {v5}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->objectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v5

    .line 828
    invoke-interface {p1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 833
    :cond_0
    invoke-interface {v2}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    move v2, v3

    goto :goto_0

    :catchall_0
    move-exception p0

    goto :goto_2

    :catch_0
    move-exception p0

    .line 831
    :try_start_1
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 833
    :goto_2
    invoke-interface {v2}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw p0

    :cond_1
    return-object p1
.end method

.method private static readByToken(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 749
    invoke-static {p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeFromObject(Ljava/lang/Class;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p1

    .line 750
    invoke-static {p1}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->assertHasMerchantCatalogToken(Lcom/squareup/shared/catalog/models/CatalogObjectType;)V

    .line 752
    invoke-static {}, Lcom/squareup/shared/catalog/ObjectsTable;->instance()Lcom/squareup/shared/catalog/ObjectsTable;

    move-result-object v0

    invoke-virtual {v0, p0, p2}, Lcom/squareup/shared/catalog/ObjectsTable;->selectObjectByToken(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p0

    .line 754
    :try_start_0
    invoke-interface {p0}, Lcom/squareup/shared/sql/SQLCursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    .line 757
    invoke-interface {p0}, Lcom/squareup/shared/sql/SQLCursor;->getCount()I

    move-result p2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p2, :cond_0

    const/4 p1, 0x0

    .line 767
    invoke-interface {p0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object p1

    .line 760
    :cond_0
    :try_start_1
    invoke-interface {p0}, Lcom/squareup/shared/sql/SQLCursor;->moveToFirst()Z

    const/4 p2, 0x0

    .line 761
    invoke-interface {p0, p2}, Lcom/squareup/shared/sql/SQLCursor;->getBlob(I)[B

    move-result-object p2

    .line 762
    sget-object v0, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p2}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/api/sync/ObjectWrapper;

    .line 763
    invoke-virtual {p1, p2}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 767
    invoke-interface {p0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object p1

    .line 755
    :cond_1
    :try_start_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Found more than one objects for merchant catalog object token "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 765
    :try_start_3
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 767
    :goto_0
    invoke-interface {p0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw p1
.end method

.method private static requireConnectV2Id(Lcom/squareup/shared/catalog/connectv2/models/ModelObject;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject;",
            ">(TT;)V"
        }
    .end annotation

    .line 77
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;->getKey()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->getId()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/StringUtils;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    return-void

    .line 78
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "ConnectV2 Object has no ID"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static requireId(Lcom/squareup/shared/catalog/models/CatalogObject;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject;",
            ">(TT;)V"
        }
    .end annotation

    .line 71
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/StringUtils;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    return-void

    .line 72
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Object has no ID"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static requireSessionState(Lcom/squareup/api/rpc/Request;)V
    .locals 1

    .line 83
    iget-object p0, p0, Lcom/squareup/api/rpc/Request;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    if-eqz p0, :cond_0

    return-void

    .line 84
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Request has no session state"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static writeConnectV2Object(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/connectv2/models/ModelObject;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject;",
            ">(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "TT;)V"
        }
    .end annotation

    .line 1301
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;->toByteArray()[B

    move-result-object v6

    .line 1306
    invoke-static {}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->instance()Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;

    move-result-object v0

    .line 1307
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;->getKey()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->getId()Ljava/lang/String;

    move-result-object v2

    .line 1308
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;->getType()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;->getStableIdentifier()J

    move-result-wide v3

    const-string v5, ""

    move-object v1, p0

    .line 1307
    invoke-virtual/range {v0 .. v6}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->insertOrReplace(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;JLjava/lang/String;[B)V

    return-void
.end method

.method private static writeObject(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogObject;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject;",
            ">(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "TT;)V"
        }
    .end annotation

    .line 1292
    sget-object v0, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object v9

    .line 1293
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v0

    .line 1294
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v0, v1, :cond_0

    move-object v1, p1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/api/items/Item$Type;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    move-object v7, v1

    .line 1295
    invoke-static {}, Lcom/squareup/shared/catalog/ObjectsTable;->instance()Lcom/squareup/shared/catalog/ObjectsTable;

    move-result-object v2

    .line 1296
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v5

    .line 1297
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->getProtoTypeValue()I

    move-result v6

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getSortText()Ljava/lang/String;

    move-result-object v8

    move-object v3, p0

    .line 1296
    invoke-virtual/range {v2 .. v9}, Lcom/squareup/shared/catalog/ObjectsTable;->insertOrReplace(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/String;[B)V

    return-void
.end method

.method private static writeRelations(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogObject;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "TT;)V"
        }
    .end annotation

    .line 1279
    invoke-static {}, Lcom/squareup/shared/catalog/ReferencesTable;->instance()Lcom/squareup/shared/catalog/ReferencesTable;

    move-result-object v6

    .line 1280
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, p0, v0}, Lcom/squareup/shared/catalog/ReferencesTable;->deleteAllReferences(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V

    .line 1281
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getRelations()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1282
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogRelation;

    iget-object v1, v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->referentType:Lcom/squareup/api/items/Type;

    invoke-virtual {v1}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v8

    .line 1283
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    .line 1284
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->getProtoTypeValue()I

    move-result v2

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v3

    move-object v0, v6

    move-object v1, p0

    move v4, v8

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/shared/catalog/ReferencesTable;->write(Lcom/squareup/shared/sql/SQLDatabase;ILjava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private writeVersionSyncIncomplete(Z)V
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 134
    invoke-static {}, Lcom/squareup/shared/catalog/MetadataTable;->instance()Lcom/squareup/shared/catalog/MetadataTable;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/squareup/shared/catalog/MetadataTable;->updateSyncIncomplete(ZLcom/squareup/shared/sql/SQLDatabase;)V

    return-void
.end method


# virtual methods
.method public applyCogsVersionBatch(Ljava/util/List;Ljava/util/List;Z)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;Z)V"
        }
    .end annotation

    move-object v8, p0

    .line 283
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->readCogsAppliedServerVersion()J

    move-result-wide v0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    .line 285
    :goto_0
    iget-object v0, v8, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v11

    .line 286
    iget-object v0, v8, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide v12

    .line 289
    invoke-interface {v11}, Lcom/squareup/shared/sql/SQLDatabase;->beginTransaction()V

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, v11

    move-object/from16 v3, p2

    move-object v4, p1

    move/from16 v6, p3

    .line 291
    :try_start_0
    invoke-direct/range {v1 .. v7}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->applyObjectChangesToDb(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;Ljava/util/Collection;ZZZ)V

    .line 292
    invoke-interface {v11}, Lcom/squareup/shared/sql/SQLDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294
    invoke-interface {v11}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    .line 296
    iget-object v0, v8, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v12

    new-array v2, v9, [Ljava/lang/Object;

    .line 297
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v2, v10

    const-string v0, "Total applyCogsVersionBatch() time including transaction: %d ms"

    invoke-static {v0, v2}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 294
    invoke-interface {v11}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    throw v0
.end method

.method public applyConnectV2Batch(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;)V"
        }
    .end annotation

    .line 304
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 305
    iget-object v1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v1}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide v1

    .line 308
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->beginTransaction()V

    .line 310
    :try_start_0
    invoke-direct {p0, v0, p2, p1}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->applyConnectV2ObjectChangesToDb(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 311
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    .line 315
    iget-object p1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {p1}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide p1

    sub-long/2addr p1, v1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 316
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "Total applyConnectV2Batch() time including transaction: %d ms"

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception p1

    .line 313
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    throw p1
.end method

.method public beginVersionSync(JLjava/util/List;Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;)Z"
        }
    .end annotation

    .line 226
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->readCogsAppliedServerVersion()J

    move-result-wide v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    cmp-long v4, p1, v0

    if-gtz v4, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 228
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->readLastSyncTimestamp()Ljava/util/Date;

    move-result-object p2

    if-eqz p2, :cond_1

    const/4 p2, 0x1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    .line 230
    :goto_1
    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    if-eqz v0, :cond_3

    new-array p1, v3, [Ljava/lang/Object;

    const-string p2, "CatalogStore: No need to begin a sync. The server version <= applied version, while supported connect v2 object types are up to date."

    .line 233
    invoke-static {p2, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return v3

    .line 239
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->hasPendingPutRequests()Z

    move-result p1

    if-eqz p1, :cond_4

    new-array p1, v3, [Ljava/lang/Object;

    const-string p2, "CatalogStore: Cannot begin a sync. There are pending put requests enqueued."

    .line 240
    invoke-static {p2, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return v3

    .line 246
    :cond_4
    invoke-direct {p0, v2}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->writeVersionSyncIncomplete(Z)V

    .line 248
    iget-object p1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {p1}, Lcom/squareup/shared/sql/DatabaseHelper;->getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object p1

    .line 250
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->beginTransaction()V

    .line 251
    iput-boolean v2, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->isVersionSyncInProgress:Z

    .line 254
    invoke-direct {p0, p3}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->insertSupportedConnectV2ObjectTypes(Ljava/util/List;)V

    .line 255
    invoke-direct {p0, p4}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->deleteUnsupportedConnectV2ObjectTypes(Ljava/util/List;)V

    return v2
.end method

.method public close()V
    .locals 2

    .line 1251
    iget-boolean v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->isVersionSyncInProgress:Z

    if-eqz v0, :cond_0

    .line 1252
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    const/4 v1, 0x0

    .line 1253
    iput-boolean v1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->isVersionSyncInProgress:Z

    .line 1254
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    .line 1256
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->close()V

    return-void
.end method

.method public count(Lcom/squareup/shared/catalog/models/CatalogObjectType;)I
    .locals 2

    .line 625
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 626
    invoke-static {}, Lcom/squareup/shared/catalog/ObjectsTable;->instance()Lcom/squareup/shared/catalog/ObjectsTable;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->getProtoTypeValue()I

    move-result p1

    invoke-virtual {v1, v0, p1}, Lcom/squareup/shared/catalog/ObjectsTable;->countObjectsWithType(Lcom/squareup/shared/sql/SQLDatabase;I)I

    move-result p1

    return p1
.end method

.method public countItemsWithTypes(Ljava/util/List;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)I"
        }
    .end annotation

    .line 620
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 621
    invoke-static {}, Lcom/squareup/shared/catalog/ObjectsTable;->instance()Lcom/squareup/shared/catalog/ObjectsTable;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/squareup/shared/catalog/ObjectsTable;->countItemsWithTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/List;)I

    move-result p1

    return p1
.end method

.method public countRelatedItems(Lcom/squareup/shared/catalog/ManyToMany$Lookup;Ljava/util/List;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/ManyToMany$Lookup<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)I"
        }
    .end annotation

    .line 946
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v2

    .line 947
    invoke-static {}, Lcom/squareup/shared/catalog/ReferencesTable;->instance()Lcom/squareup/shared/catalog/ReferencesTable;

    move-result-object v1

    iget-object v0, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->joinType:Lcom/squareup/api/items/Type;

    invoke-virtual {v0}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v3

    iget-object v0, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->referentType:Lcom/squareup/api/items/Type;

    .line 948
    invoke-virtual {v0}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v4

    iget-object v0, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->referrerType:Lcom/squareup/api/items/Type;

    invoke-virtual {v0}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v5

    iget-object v6, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->fromId:Ljava/lang/String;

    move-object v7, p2

    .line 947
    invoke-virtual/range {v1 .. v7}, Lcom/squareup/shared/catalog/ReferencesTable;->countRelatedItems(Lcom/squareup/shared/sql/SQLDatabase;IIILjava/lang/String;Ljava/util/List;)I

    move-result p1

    return p1
.end method

.method public deletePendingWriteRequest(J)V
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 139
    invoke-static {}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable;->instance()Lcom/squareup/shared/catalog/PendingWriteRequestsTable;

    move-result-object v1

    invoke-virtual {v1, v0, p1, p2}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable;->delete(Lcom/squareup/shared/sql/SQLDatabase;J)I

    return-void
.end method

.method public endVersionSync(JZLjava/util/Date;)V
    .locals 2

    .line 261
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p3, :cond_0

    .line 264
    :try_start_0
    invoke-static {}, Lcom/squareup/shared/catalog/MetadataTable;->instance()Lcom/squareup/shared/catalog/MetadataTable;

    move-result-object p3

    .line 265
    invoke-virtual {p3, p1, p2, v0}, Lcom/squareup/shared/catalog/MetadataTable;->updateCogsAppliedServerVersion(JLcom/squareup/shared/sql/SQLDatabase;)V

    .line 266
    invoke-virtual {p3, p4, v0}, Lcom/squareup/shared/catalog/MetadataTable;->updateSyncTimestamp(Ljava/util/Date;Lcom/squareup/shared/sql/SQLDatabase;)V

    .line 267
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->setTransactionSuccessful()V

    .line 268
    iget-object p1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->storeUpdateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;->dispatchSyncUpdates()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 272
    iput-boolean v1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->isVersionSyncInProgress:Z

    .line 273
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    .line 274
    iget-object p2, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->storeUpdateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

    invoke-interface {p2}, Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;->cleanUp()V

    throw p1

    .line 272
    :cond_0
    :goto_0
    iput-boolean v1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->isVersionSyncInProgress:Z

    .line 273
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    .line 274
    iget-object p1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->storeUpdateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;->cleanUp()V

    .line 278
    invoke-direct {p0, v1}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->writeVersionSyncIncomplete(Z)V

    return-void
.end method

.method public findReferences(Lcom/squareup/api/items/Type;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/Type;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 959
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    const/4 v1, 0x0

    .line 962
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/api/items/Type;->getValue()I

    move-result p1

    .line 964
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 965
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/Type;

    .line 966
    invoke-virtual {v3}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 969
    :cond_0
    invoke-static {}, Lcom/squareup/shared/catalog/ReferencesTable;->instance()Lcom/squareup/shared/catalog/ReferencesTable;

    move-result-object p3

    .line 970
    invoke-virtual {p3, v0, p1, v2, p2}, Lcom/squareup/shared/catalog/ReferencesTable;->findAllIdsForRelationship(Lcom/squareup/shared/sql/SQLDatabase;ILjava/util/List;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v1

    .line 972
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 973
    :goto_1
    invoke-interface {v1}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 974
    new-instance p2, Lcom/squareup/api/sync/ObjectType$Builder;

    invoke-direct {p2}, Lcom/squareup/api/sync/ObjectType$Builder;-><init>()V

    const/4 p3, 0x0

    .line 975
    invoke-interface {v1, p3}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result p3

    invoke-static {p3}, Lcom/squareup/api/items/Type;->fromValue(I)Lcom/squareup/api/items/Type;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/api/sync/ObjectType$Builder;->type(Lcom/squareup/api/items/Type;)Lcom/squareup/api/sync/ObjectType$Builder;

    move-result-object p2

    .line 976
    invoke-virtual {p2}, Lcom/squareup/api/sync/ObjectType$Builder;->build()Lcom/squareup/api/sync/ObjectType;

    move-result-object p2

    .line 977
    new-instance p3, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {p3}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    .line 978
    invoke-virtual {p3, p2}, Lcom/squareup/api/sync/ObjectId$Builder;->type(Lcom/squareup/api/sync/ObjectType;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object p2

    const/4 p3, 0x1

    .line 979
    invoke-interface {v1, p3}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object p2

    .line 980
    invoke-virtual {p2}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object p2

    .line 981
    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 983
    :cond_1
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_2

    .line 986
    invoke-interface {v1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_2
    return-object p1

    :catchall_0
    move-exception p1

    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_3
    throw p1
.end method

.method public findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 953
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 954
    invoke-static {v0, p1, p2}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->findReferrers(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getSqliteDatabase()Lcom/squareup/shared/sql/SQLDatabase;
    .locals 1

    .line 1323
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    return-object v0
.end method

.method hasPendingPutRequests()Z
    .locals 2

    .line 585
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 586
    invoke-static {}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable;->instance()Lcom/squareup/shared/catalog/PendingWriteRequestsTable;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable;->hasPutRequests(Lcom/squareup/shared/sql/SQLDatabase;)Z

    move-result v0

    return v0
.end method

.method public readAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 2

    .line 841
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 842
    invoke-static {}, Lcom/squareup/shared/catalog/ObjectsTable;->instance()Lcom/squareup/shared/catalog/ObjectsTable;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->getProtoTypeValue()I

    move-result p1

    invoke-virtual {v1, v0, p1}, Lcom/squareup/shared/catalog/ObjectsTable;->selectObjectsByType(Lcom/squareup/shared/sql/SQLDatabase;I)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method public readAllConnectV2Objects(Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 4

    .line 846
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 847
    invoke-static {}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->instance()Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;

    move-result-object v1

    .line 848
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->getStableIdentifier()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->selectObjectsByType(Lcom/squareup/shared/sql/SQLDatabase;J)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method public readAllSyncedConnectV2ObjectTypes()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;"
        }
    .end annotation

    .line 596
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 597
    invoke-static {}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->instance()Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->readAllObjectTypes(Lcom/squareup/shared/sql/SQLDatabase;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v0

    .line 599
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 602
    :goto_0
    :try_start_0
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    .line 603
    invoke-interface {v0, v2}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result v2

    .line 605
    invoke-static {v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->fromValue(I)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    move-result-object v2

    .line 604
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 610
    :cond_0
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw v1
.end method

.method public readAndParseAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            ")",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 853
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->readAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    .line 855
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 856
    :goto_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 857
    invoke-interface {p1, v1}, Lcom/squareup/shared/sql/SQLCursor;->getBlob(I)[B

    move-result-object v1

    .line 858
    sget-object v2, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v2, v1}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/sync/ObjectWrapper;

    .line 859
    invoke-static {v1}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->objectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v1

    .line 860
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 866
    :cond_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 864
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 866
    :goto_1
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw v0
.end method

.method public readAndParseAllConnectV2Objects(Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">(",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;",
            ")",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 873
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->readAllConnectV2Objects(Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    .line 875
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 876
    :goto_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 877
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->parseModelObjectFromCursor(Lcom/squareup/shared/sql/SQLCursor;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object v1

    .line 878
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 884
    :cond_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 882
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 884
    :goto_1
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw v0
.end method

.method public readById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 630
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    const/4 v1, 0x1

    .line 631
    invoke-static {v0, p1, p2, v1}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->readById(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/Class;Ljava/lang/String;Z)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    return-object p1
.end method

.method public readByIdOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 670
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    const/4 v1, 0x0

    .line 671
    invoke-static {v0, p1, p2, v1}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->readById(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/Class;Ljava/lang/String;Z)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    return-object p1
.end method

.method public readByIds(Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation

    .line 805
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 806
    invoke-static {v0, p1, p2}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->readByIds(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public readByTokenOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 680
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 681
    invoke-static {v0, p1, p2}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->readByToken(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    return-object p1
.end method

.method public readByTokens(Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation

    .line 686
    invoke-static {p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeFromObject(Ljava/lang/Class;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p1

    .line 687
    invoke-static {p1}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->assertHasMerchantCatalogToken(Lcom/squareup/shared/catalog/models/CatalogObjectType;)V

    .line 689
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 690
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 691
    iget-object v1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v1}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 694
    :goto_0
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 695
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v4

    sub-int/2addr v4, v3

    const/16 v5, 0x3e7

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 697
    invoke-static {}, Lcom/squareup/shared/catalog/ObjectsTable;->instance()Lcom/squareup/shared/catalog/ObjectsTable;

    move-result-object v5

    add-int/2addr v4, v3

    .line 698
    invoke-interface {v0, v3, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v5, v1, v3}, Lcom/squareup/shared/catalog/ObjectsTable;->selectObjectByTokens(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v3

    .line 702
    :goto_1
    :try_start_0
    invoke-interface {v3}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 703
    invoke-interface {v3, v2}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    .line 704
    invoke-interface {v3, v6}, Lcom/squareup/shared/sql/SQLCursor;->getBlob(I)[B

    move-result-object v6

    .line 705
    sget-object v7, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v7, v6}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/api/sync/ObjectWrapper;

    .line 706
    invoke-static {v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->objectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v6

    .line 707
    invoke-interface {p1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 712
    :cond_0
    invoke-interface {v3}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    move v3, v4

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_0
    move-exception p1

    .line 710
    :try_start_1
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 712
    :goto_2
    invoke-interface {v3}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw p1

    :cond_1
    return-object p1
.end method

.method public readClientStateSeq()J
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 124
    invoke-static {}, Lcom/squareup/shared/catalog/MetadataTable;->instance()Lcom/squareup/shared/catalog/MetadataTable;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/MetadataTable;->selectClientStateSeq(Lcom/squareup/shared/sql/SQLDatabase;)J

    move-result-wide v0

    return-wide v0
.end method

.method public readCogsAppliedServerVersion()J
    .locals 2

    .line 590
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 591
    invoke-static {}, Lcom/squareup/shared/catalog/MetadataTable;->instance()Lcom/squareup/shared/catalog/MetadataTable;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/MetadataTable;->selectCogsAppliedServerVersion(Lcom/squareup/shared/sql/SQLDatabase;)J

    move-result-wide v0

    return-wide v0
.end method

.method public readConnectV2ObjectById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 636
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 638
    sget-object v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->typeFromModelClass(Ljava/lang/Class;)Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;

    move-result-object v1

    .line 639
    invoke-static {}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->instance()Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;

    move-result-object v2

    .line 640
    invoke-interface {v1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;->getStableIdentifier()J

    move-result-wide v3

    invoke-virtual {v2, v0, p2, v3, v4}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->selectObjectById(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;J)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v0

    .line 642
    :try_start_0
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->getCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 650
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->moveToFirst()Z

    .line 652
    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->parseModelObjectFromCursor(Lcom/squareup/shared/sql/SQLCursor;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 656
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object p1

    .line 643
    :cond_0
    :try_start_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 644
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 646
    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " for id "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 654
    :try_start_2
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 656
    :goto_0
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw p1
.end method

.method public readConnectV2ObjectsByIds(Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 784
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 785
    invoke-static {}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->instance()Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;

    move-result-object v1

    .line 786
    sget-object v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;

    invoke-virtual {v2, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->typeFromModelClass(Ljava/lang/Class;)Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;

    move-result-object p1

    .line 788
    invoke-interface {p1}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;->getStableIdentifier()J

    move-result-wide v2

    invoke-virtual {v1, v0, p2, v2, v3}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->selectObjectsByIds(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;J)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    .line 790
    :try_start_0
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 791
    :goto_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 792
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->parseModelObjectFromCursor(Lcom/squareup/shared/sql/SQLCursor;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object v0

    .line 793
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 799
    :cond_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object p2

    :catchall_0
    move-exception p2

    goto :goto_1

    :catch_0
    move-exception p2

    .line 797
    :try_start_1
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 799
    :goto_1
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw p2
.end method

.method public readIdsForAllObjectsOfType(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 890
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    const/4 v1, 0x0

    .line 894
    :try_start_0
    invoke-static {}, Lcom/squareup/shared/catalog/ObjectsTable;->instance()Lcom/squareup/shared/catalog/ObjectsTable;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->getProtoTypeValue()I

    move-result p1

    invoke-virtual {v2, v0, p1, p2}, Lcom/squareup/shared/catalog/ObjectsTable;->selectObjectIds(Lcom/squareup/shared/sql/SQLDatabase;ILjava/util/List;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v1

    .line 895
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 896
    :goto_0
    invoke-interface {v1}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x0

    .line 897
    invoke-interface {v1, p2}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    .line 902
    invoke-interface {v1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_1
    return-object p1

    :catchall_0
    move-exception p1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_2
    throw p1
.end method

.method public readLastSyncTimestamp()Ljava/util/Date;
    .locals 2

    .line 615
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 616
    invoke-static {}, Lcom/squareup/shared/catalog/MetadataTable;->instance()Lcom/squareup/shared/catalog/MetadataTable;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/MetadataTable;->selectSyncTimestamp(Lcom/squareup/shared/sql/SQLDatabase;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public readPendingWriteRequests()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/PendingWriteRequest;",
            ">;"
        }
    .end annotation

    .line 143
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 144
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    .line 147
    :try_start_0
    invoke-static {}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable;->instance()Lcom/squareup/shared/catalog/PendingWriteRequestsTable;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable;->readAll(Lcom/squareup/shared/sql/SQLDatabase;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v2

    .line 148
    :goto_0
    invoke-interface {v2}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 149
    invoke-interface {v2, v0}, Lcom/squareup/shared/sql/SQLCursor;->getLong(I)J

    move-result-wide v3

    .line 150
    sget-object v0, Lcom/squareup/api/rpc/Request;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v5, 0x1

    invoke-interface {v2, v5}, Lcom/squareup/shared/sql/SQLCursor;->getBlob(I)[B

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/rpc/Request;

    .line 151
    new-instance v5, Lcom/squareup/shared/catalog/PendingWriteRequest;

    invoke-direct {v5, v3, v4, v0}, Lcom/squareup/shared/catalog/PendingWriteRequest;-><init>(JLcom/squareup/api/rpc/Request;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 153
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_1

    .line 158
    invoke-interface {v2}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_1
    return-object v0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 155
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v3, "Failed to parse protobuf from blob"

    invoke-direct {v1, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    if-eqz v2, :cond_2

    .line 158
    invoke-interface {v2}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_2
    throw v0
.end method

.method public readSessionId()Ljava/lang/Long;
    .locals 2

    .line 113
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 114
    invoke-static {}, Lcom/squareup/shared/catalog/MetadataTable;->instance()Lcom/squareup/shared/catalog/MetadataTable;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/MetadataTable;->selectSessionId(Lcom/squareup/shared/sql/SQLDatabase;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public readVersionSyncIncomplete()Z
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 129
    invoke-static {}, Lcom/squareup/shared/catalog/MetadataTable;->instance()Lcom/squareup/shared/catalog/MetadataTable;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/MetadataTable;->selectSyncIncomplete(Lcom/squareup/shared/sql/SQLDatabase;)Z

    move-result v0

    return v0
.end method

.method public rebuildSyntheticTables(Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;)V
    .locals 12

    .line 1138
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->requiresSyntheticTableRebuild:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1143
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 1144
    iget-object v1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v1}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide v1

    .line 1145
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->beginTransaction()V

    const/4 v3, 0x0

    .line 1149
    :try_start_0
    iget-object v4, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTablesToDrop:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v6, "DROP TABLE IF EXISTS ?"

    const-string v7, "?"

    .line 1150
    invoke-virtual {v6, v7, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_0

    .line 1154
    :cond_0
    iget-object v4, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTablesToRebuild:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;

    .line 1155
    invoke-interface {v5, v0}, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;->create(Lcom/squareup/shared/sql/SQLDatabase;)V

    goto :goto_1

    .line 1158
    :cond_1
    new-instance v4, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;-><init>(Z)V

    .line 1159
    invoke-static {}, Lcom/squareup/shared/catalog/ObjectsTable;->instance()Lcom/squareup/shared/catalog/ObjectsTable;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/squareup/shared/catalog/ObjectsTable;->readAllObjects(Lcom/squareup/shared/sql/SQLDatabase;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v3

    .line 1160
    invoke-static {}, Lcom/squareup/shared/catalog/ObjectsTable;->instance()Lcom/squareup/shared/catalog/ObjectsTable;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/squareup/shared/catalog/ObjectsTable;->countAllObjects(Lcom/squareup/shared/sql/SQLDatabase;)I

    move-result v6

    const/4 v7, 0x1

    if-eqz v6, :cond_6

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1166
    :cond_2
    :goto_2
    invoke-interface {v3}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1167
    sget-object v10, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-interface {v3, v5}, Lcom/squareup/shared/sql/SQLCursor;->getBlob(I)[B

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/api/sync/ObjectWrapper;

    .line 1168
    invoke-static {v10}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v11

    .line 1169
    invoke-virtual {v11, v10}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v10

    .line 1170
    invoke-direct {p0, v4, v10, v8}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->addObjectToUpdatedCatalogObjectBuilder(Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;Lcom/squareup/shared/catalog/models/CatalogObject;Z)V

    add-int/2addr v9, v7

    .line 1175
    rem-int/lit16 v10, v9, 0x2710

    if-nez v10, :cond_2

    .line 1176
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->build()Lcom/squareup/shared/catalog/UpdatedCatalogObjects;

    move-result-object v4

    .line 1177
    iget-object v10, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTablesToRebuild:Ljava/util/Set;

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;

    .line 1178
    invoke-interface {v11, v0, v4, v7, v8}, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;->applyUpdates(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/UpdatedCatalogObjects;ZZ)V

    goto :goto_3

    .line 1181
    :cond_3
    new-instance v4, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;

    invoke-direct {v4, v5}, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;-><init>(Z)V

    mul-int/lit8 v8, v9, 0x64

    .line 1182
    div-int/2addr v8, v6

    invoke-interface {p1, v8}, Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;->onNext(I)V

    const/4 v8, 0x0

    goto :goto_2

    .line 1187
    :cond_4
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/UpdatedCatalogObjects$Builder;->build()Lcom/squareup/shared/catalog/UpdatedCatalogObjects;

    move-result-object v4

    .line 1188
    rem-int/lit16 v6, v6, 0x2710

    if-eqz v6, :cond_5

    .line 1189
    iget-object v6, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTablesToRebuild:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;

    .line 1190
    invoke-interface {v9, v0, v4, v7, v8}, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;->applyUpdates(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/UpdatedCatalogObjects;ZZ)V

    goto :goto_4

    :cond_5
    const/16 v4, 0x64

    .line 1193
    invoke-interface {p1, v4}, Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;->onNext(I)V

    .line 1197
    :cond_6
    invoke-static {}, Lcom/squareup/shared/catalog/SyntheticVersionsTable;->instance()Lcom/squareup/shared/catalog/SyntheticVersionsTable;

    move-result-object p1

    iget-object v4, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTables:Ljava/util/Set;

    invoke-virtual {p1, v0, v4}, Lcom/squareup/shared/catalog/SyntheticVersionsTable;->clearAllAndWriteVersions(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Set;)V

    .line 1198
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_7

    .line 1203
    invoke-interface {v3}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    .line 1205
    :cond_7
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    .line 1208
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->requiresSyntheticTableRebuild:Ljava/lang/Boolean;

    new-array p1, v7, [Ljava/lang/Object;

    .line 1209
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/logging/Clock;->getElapsedRealtime()J

    move-result-wide v3

    sub-long/2addr v3, v1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, p1, v5

    const-string v0, "Rebuilt SyntheticTables in %d ms."

    invoke-static {v0, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception p1

    goto :goto_5

    :catch_0
    move-exception p1

    .line 1200
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_5
    if-eqz v3, :cond_8

    .line 1203
    invoke-interface {v3}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    .line 1205
    :cond_8
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    throw p1

    .line 1139
    :cond_9
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Attempted to rebuild synthetic tables when requiresSyntheticTableRebuild is false."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public requiresSyntheticTableRebuild()Z
    .locals 7

    .line 1214
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->requiresSyntheticTableRebuild:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1215
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 1220
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 1222
    invoke-static {}, Lcom/squareup/shared/catalog/SyntheticVersionsTable;->instance()Lcom/squareup/shared/catalog/SyntheticVersionsTable;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/SyntheticVersionsTable;->readAllVersions(Lcom/squareup/shared/sql/SQLDatabase;)Ljava/util/Map;

    move-result-object v0

    .line 1224
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1225
    iget-object v2, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTables:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;

    .line 1226
    invoke-interface {v3}, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;->tableName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1228
    invoke-interface {v3}, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;->tableName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1229
    iget-object v4, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTablesToRebuild:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1233
    :cond_2
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1234
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;

    if-nez v3, :cond_4

    .line 1237
    iget-object v3, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTablesToDrop:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1238
    :cond_4
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-interface {v3}, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;->tableVersion()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1240
    iget-object v4, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTablesToDrop:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1241
    iget-object v2, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTablesToRebuild:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1245
    :cond_5
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTablesToRebuild:Ljava/util/Set;

    .line 1246
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->syntheticTablesToDrop:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    :cond_7
    :goto_2
    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->requiresSyntheticTableRebuild:Ljava/lang/Boolean;

    .line 1245
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public resolve(Lcom/squareup/shared/catalog/ManyToMany$Lookup;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/ManyToMany$Lookup<",
            "TT;*>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 1039
    invoke-virtual {p0, p1, v0}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->resolve(Lcom/squareup/shared/catalog/ManyToMany$Lookup;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public resolve(Lcom/squareup/shared/catalog/ManyToMany$Lookup;Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/ManyToMany$Lookup<",
            "TT;*>;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 1044
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v2

    const/4 v0, 0x0

    .line 1048
    :try_start_0
    invoke-static {}, Lcom/squareup/shared/catalog/ReferencesTable;->instance()Lcom/squareup/shared/catalog/ReferencesTable;

    move-result-object v1

    iget-object v3, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->joinType:Lcom/squareup/api/items/Type;

    .line 1049
    invoke-virtual {v3}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v3

    iget-object v4, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->referentType:Lcom/squareup/api/items/Type;

    invoke-virtual {v4}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v4

    iget-object v5, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->referrerType:Lcom/squareup/api/items/Type;

    .line 1050
    invoke-virtual {v5}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v5

    iget-object v6, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->fromId:Ljava/lang/String;

    move-object v7, p2

    .line 1049
    invoke-virtual/range {v1 .. v7}, Lcom/squareup/shared/catalog/ReferencesTable;->resolve(Lcom/squareup/shared/sql/SQLDatabase;IIILjava/lang/String;Ljava/util/List;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v0

    .line 1051
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 1052
    :goto_0
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 1053
    invoke-interface {v0, v1}, Lcom/squareup/shared/sql/SQLCursor;->getBlob(I)[B

    move-result-object v1

    .line 1054
    sget-object v2, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v2, v1}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/sync/ObjectWrapper;

    .line 1055
    iget-object v2, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->referentType:Lcom/squareup/api/items/Type;

    invoke-static {v2}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeFromProtoType(Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v1

    .line 1056
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1058
    :cond_0
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 1063
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_1
    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    .line 1060
    :try_start_1
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    if-eqz v0, :cond_2

    .line 1063
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_2
    throw p1
.end method

.method public resolveIdsForRelationship(Lcom/squareup/shared/catalog/ManyToMany$Lookup;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/ManyToMany$Lookup<",
            "TT;*>;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1118
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    const/4 v1, 0x0

    .line 1121
    :try_start_0
    invoke-static {}, Lcom/squareup/shared/catalog/ReferencesTable;->instance()Lcom/squareup/shared/catalog/ReferencesTable;

    move-result-object v2

    iget-object v3, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->referentType:Lcom/squareup/api/items/Type;

    .line 1122
    invoke-virtual {v3}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v3

    iget-object v4, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->referrerType:Lcom/squareup/api/items/Type;

    .line 1123
    invoke-virtual {v4}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v4

    iget-object p1, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->fromId:Ljava/lang/String;

    .line 1122
    invoke-virtual {v2, v0, v3, v4, p1}, Lcom/squareup/shared/catalog/ReferencesTable;->resolveIdsForRelationship(Lcom/squareup/shared/sql/SQLDatabase;IILjava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v1

    .line 1125
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 1126
    :goto_0
    invoke-interface {v1}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1127
    invoke-interface {v1, v0}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1129
    :cond_0
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 1132
    invoke-interface {v1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_1
    return-object p1

    :catchall_0
    move-exception p1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_2
    throw p1
.end method

.method public resolveMemberships(Lcom/squareup/shared/catalog/ManyToMany$Lookup;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/ManyToMany$Lookup<",
            "TT;*>;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 1071
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    const/4 v1, 0x0

    .line 1075
    :try_start_0
    invoke-static {}, Lcom/squareup/shared/catalog/ReferencesTable;->instance()Lcom/squareup/shared/catalog/ReferencesTable;

    move-result-object v2

    iget-object v3, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->joinType:Lcom/squareup/api/items/Type;

    .line 1076
    invoke-virtual {v3}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v3

    iget-object v4, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->fromId:Ljava/lang/String;

    invoke-virtual {v2, v0, v3, v4, p2}, Lcom/squareup/shared/catalog/ReferencesTable;->resolveMembershipsWithPossibleIds(Lcom/squareup/shared/sql/SQLDatabase;ILjava/lang/String;Ljava/util/List;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v1

    .line 1078
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 1079
    :goto_0
    invoke-interface {v1}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1080
    invoke-interface {v1, v0}, Lcom/squareup/shared/sql/SQLCursor;->getBlob(I)[B

    move-result-object v0

    .line 1081
    sget-object v2, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v2, v0}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectWrapper;

    .line 1082
    iget-object v2, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->joinType:Lcom/squareup/api/items/Type;

    invoke-static {v2}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeFromProtoType(Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v0

    .line 1083
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1085
    :cond_0
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 1090
    invoke-interface {v1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_1
    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    .line 1087
    :try_start_1
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    if-eqz v1, :cond_2

    .line 1090
    invoke-interface {v1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_2
    throw p1
.end method

.method public resolveOuter(Lcom/squareup/shared/catalog/ManyToMany$Lookup;)Lcom/squareup/shared/catalog/TypedCursor;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;J:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/ManyToMany$Lookup<",
            "TT;TJ;>;)",
            "Lcom/squareup/shared/catalog/TypedCursor<",
            "Lcom/squareup/shared/catalog/Related<",
            "TT;TJ;>;>;"
        }
    .end annotation

    .line 910
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v2

    .line 912
    iget-object v0, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->referentType:Lcom/squareup/api/items/Type;

    .line 913
    iget-object v7, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->joinType:Lcom/squareup/api/items/Type;

    .line 915
    invoke-static {}, Lcom/squareup/shared/catalog/ReferencesTable;->instance()Lcom/squareup/shared/catalog/ReferencesTable;

    move-result-object v1

    .line 916
    invoke-virtual {v7}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v3

    iget-object v4, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->referrerType:Lcom/squareup/api/items/Type;

    invoke-virtual {v4}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v4

    iget-object v5, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->fromId:Ljava/lang/String;

    .line 917
    invoke-virtual {v0}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v6

    .line 916
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/shared/catalog/ReferencesTable;->resolveOuter(Lcom/squareup/shared/sql/SQLDatabase;IILjava/lang/String;I)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    .line 918
    new-instance v1, Lcom/squareup/shared/catalog/SqliteCatalogStore$1;

    invoke-direct {v1, p0, p1, v0, v7}, Lcom/squareup/shared/catalog/SqliteCatalogStore$1;-><init>(Lcom/squareup/shared/catalog/SqliteCatalogStore;Lcom/squareup/shared/sql/SQLCursor;Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)V

    return-object v1
.end method

.method public resolveRelatedObjectIds(Lcom/squareup/shared/catalog/ManyToMany$Lookup;Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/ManyToMany$Lookup<",
            "TT;*>;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1097
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v2

    const/4 v0, 0x0

    .line 1100
    :try_start_0
    invoke-static {}, Lcom/squareup/shared/catalog/ReferencesTable;->instance()Lcom/squareup/shared/catalog/ReferencesTable;

    move-result-object v1

    iget-object v3, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->referentType:Lcom/squareup/api/items/Type;

    .line 1101
    invoke-virtual {v3}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v3

    iget-object v4, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->referrerType:Lcom/squareup/api/items/Type;

    .line 1102
    invoke-virtual {v4}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v4

    iget-object v5, p1, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->fromId:Ljava/lang/String;

    move-object v6, p2

    .line 1101
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/shared/catalog/ReferencesTable;->resolveRelatedIdsWithPossibleIds(Lcom/squareup/shared/sql/SQLDatabase;IILjava/lang/String;Ljava/util/List;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v0

    .line 1104
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 1105
    :goto_0
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x0

    .line 1106
    invoke-interface {v0, p2}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1108
    :cond_0
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 1111
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_1
    return-object p1

    :catchall_0
    move-exception p1

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_2
    throw p1
.end method

.method public writeAndEnqueue(Ljava/util/Collection;Ljava/util/Collection;Lcom/squareup/api/rpc/Request;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Lcom/squareup/api/rpc/Request;",
            ")V"
        }
    .end annotation

    .line 165
    invoke-static {p3}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->requireSessionState(Lcom/squareup/api/rpc/Request;)V

    .line 166
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 167
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->beginTransaction()V

    .line 169
    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 170
    invoke-static {v2}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->requireId(Lcom/squareup/shared/catalog/models/CatalogObject;)V

    goto :goto_0

    .line 173
    :cond_0
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 174
    invoke-static {v2}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->requireId(Lcom/squareup/shared/catalog/models/CatalogObject;)V

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x1

    move-object v1, p0

    move-object v2, v0

    move-object v3, p1

    move-object v4, p2

    .line 177
    invoke-direct/range {v1 .. v7}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->applyObjectChangesToDb(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;Ljava/util/Collection;ZZZ)V

    .line 179
    invoke-direct {p0, p3}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->enqueue(Lcom/squareup/api/rpc/Request;)V

    .line 180
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->setTransactionSuccessful()V

    .line 181
    iget-object p1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->storeUpdateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;->dispatchLocalEdits()V

    const-string p1, "CatalogStore: Successfully updated items"

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    .line 182
    invoke-static {p1, p2}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    .line 185
    iget-object p1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->storeUpdateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;->cleanUp()V

    return-void

    :catchall_0
    move-exception p1

    .line 184
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    .line 185
    iget-object p2, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->storeUpdateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

    invoke-interface {p2}, Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;->cleanUp()V

    throw p1
.end method

.method public writeClientStateSeq(J)V
    .locals 2

    .line 118
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 119
    invoke-static {}, Lcom/squareup/shared/catalog/MetadataTable;->instance()Lcom/squareup/shared/catalog/MetadataTable;

    move-result-object v1

    invoke-virtual {v1, p1, p2, v0}, Lcom/squareup/shared/catalog/MetadataTable;->updateClientStateSeq(JLcom/squareup/shared/sql/SQLDatabase;)V

    return-void
.end method

.method public writeConnectV2Objects(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;",
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;)V"
        }
    .end annotation

    .line 191
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 192
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->beginTransaction()V

    .line 194
    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;

    .line 195
    invoke-static {v2}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->requireConnectV2Id(Lcom/squareup/shared/catalog/connectv2/models/ModelObject;)V

    goto :goto_0

    .line 198
    :cond_0
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;

    .line 199
    invoke-static {v2}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->requireConnectV2Id(Lcom/squareup/shared/catalog/connectv2/models/ModelObject;)V

    goto :goto_1

    .line 202
    :cond_1
    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->applyConnectV2ObjectChangesToDb(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 204
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->setTransactionSuccessful()V

    .line 205
    iget-object p1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->storeUpdateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;->dispatchLocalEdits()V

    const-string p1, "CatalogStore: Successfully updated items in ConnectV2"

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    .line 206
    invoke-static {p1, p2}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    .line 209
    iget-object p1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->storeUpdateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;->cleanUp()V

    return-void

    :catchall_0
    move-exception p1

    .line 208
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    .line 209
    iget-object p2, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->storeUpdateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

    invoke-interface {p2}, Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;->cleanUp()V

    throw p1
.end method

.method public writeSessionId(J)V
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 109
    invoke-static {}, Lcom/squareup/shared/catalog/MetadataTable;->instance()Lcom/squareup/shared/catalog/MetadataTable;

    move-result-object v1

    invoke-virtual {v1, p1, p2, v0}, Lcom/squareup/shared/catalog/MetadataTable;->updateSessionId(JLcom/squareup/shared/sql/SQLDatabase;)V

    return-void
.end method
