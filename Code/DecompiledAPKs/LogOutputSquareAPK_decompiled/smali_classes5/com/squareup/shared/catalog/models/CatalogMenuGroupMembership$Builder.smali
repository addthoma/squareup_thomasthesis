.class public final Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership$Builder;
.super Ljava/lang/Object;
.source "CatalogMenuGroupMembership.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final menuGroupMembership:Lcom/squareup/api/items/MenuGroupMembership$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->MENU_GROUP_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 81
    new-instance v0, Lcom/squareup/api/items/MenuGroupMembership$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/MenuGroupMembership$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/MenuGroupMembership$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/MenuGroupMembership$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership$Builder;->menuGroupMembership:Lcom/squareup/api/items/MenuGroupMembership$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership;
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership$Builder;->menuGroupMembership:Lcom/squareup/api/items/MenuGroupMembership$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/MenuGroupMembership$Builder;->build()Lcom/squareup/api/items/MenuGroupMembership;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_group_membership(Lcom/squareup/api/items/MenuGroupMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 101
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public setItemMemberId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership$Builder;
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership$Builder;->menuGroupMembership:Lcom/squareup/api/items/MenuGroupMembership$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/MenuGroupMembership$Builder;->member(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/MenuGroupMembership$Builder;

    return-object p0
.end method

.method public setMenuGroupMemberId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership$Builder;
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership$Builder;->menuGroupMembership:Lcom/squareup/api/items/MenuGroupMembership$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->MENU_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/MenuGroupMembership$Builder;->member(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/MenuGroupMembership$Builder;

    return-object p0
.end method

.method public setParentMenuGroupId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership$Builder;
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership$Builder;->menuGroupMembership:Lcom/squareup/api/items/MenuGroupMembership$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->MENU_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/MenuGroupMembership$Builder;->parent_menu_group(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/MenuGroupMembership$Builder;

    return-object p0
.end method
