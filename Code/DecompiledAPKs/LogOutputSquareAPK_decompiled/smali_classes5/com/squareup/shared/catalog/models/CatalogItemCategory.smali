.class public Lcom/squareup/shared/catalog/models/CatalogItemCategory;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogItemCategory.java"

# interfaces
.implements Lcom/squareup/shared/catalog/models/SupportsSearch;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/MenuCategory;",
        ">;",
        "Lcom/squareup/shared/catalog/models/SupportsSearch;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method


# virtual methods
.method public getAbbreviation()Ljava/lang/String;
    .locals 2

    .line 39
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/MenuCategory;

    iget-object v0, v0, Lcom/squareup/api/items/MenuCategory;->abbreviation:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getColor()Ljava/lang/String;
    .locals 2

    .line 43
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/MenuCategory;

    iget-object v0, v0, Lcom/squareup/api/items/MenuCategory;->color:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getMerchantCatalogObjectToken()Ljava/lang/String;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/MenuCategory;

    iget-object v0, v0, Lcom/squareup/api/items/MenuCategory;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 27
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/MenuCategory;

    iget-object v0, v0, Lcom/squareup/api/items/MenuCategory;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v0, v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 31
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/MenuCategory;

    iget-object v0, v0, Lcom/squareup/api/items/MenuCategory;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getOrdinal()I
    .locals 2

    .line 35
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/MenuCategory;

    iget-object v0, v0, Lcom/squareup/api/items/MenuCategory;->ordinal:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/MenuCategory;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getSortText()Ljava/lang/String;
    .locals 1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public searchKeywords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 51
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/StringUtils;->normalizedKeywordsForWordPrefixSearchWithSpecialCharactersIgnored(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
