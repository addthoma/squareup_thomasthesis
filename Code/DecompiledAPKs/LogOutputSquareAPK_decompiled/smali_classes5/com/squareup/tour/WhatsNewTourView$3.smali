.class Lcom/squareup/tour/WhatsNewTourView$3;
.super Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;
.source "WhatsNewTourView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tour/WhatsNewTourView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tour/WhatsNewTourView;


# direct methods
.method constructor <init>(Lcom/squareup/tour/WhatsNewTourView;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/tour/WhatsNewTourView$3;->this$0:Lcom/squareup/tour/WhatsNewTourView;

    invoke-direct {p0}, Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .locals 3

    .line 76
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView$3;->this$0:Lcom/squareup/tour/WhatsNewTourView;

    invoke-static {v0}, Lcom/squareup/tour/WhatsNewTourView;->access$200(Lcom/squareup/tour/WhatsNewTourView;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tour/WhatsNewTourView$3;->this$0:Lcom/squareup/tour/WhatsNewTourView;

    invoke-static {v1}, Lcom/squareup/tour/WhatsNewTourView;->access$000(Lcom/squareup/tour/WhatsNewTourView;)Lcom/squareup/tour/TourAdapter;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/tour/TourAdapter;->pages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    if-ge p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 77
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView$3;->this$0:Lcom/squareup/tour/WhatsNewTourView;

    iget-object v0, v0, Lcom/squareup/tour/WhatsNewTourView;->presenter:Lcom/squareup/tour/WhatsNewTourScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->setCurrentPage(I)V

    return-void
.end method
