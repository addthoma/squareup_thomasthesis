.class public final Lcom/squareup/tour/WhatsNewTourScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "WhatsNewTourScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/tour/WhatsNewTourScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tour/WhatsNewTourScreen$Component;,
        Lcom/squareup/tour/WhatsNewTourScreen$ParentComponent;,
        Lcom/squareup/tour/WhatsNewTourScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/tour/WhatsNewTourScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final onlyUnseen:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 120
    sget-object v0, Lcom/squareup/tour/-$$Lambda$WhatsNewTourScreen$OFato_LeSINRYLqcgiIrYg1jnUY;->INSTANCE:Lcom/squareup/tour/-$$Lambda$WhatsNewTourScreen$OFato_LeSINRYLqcgiIrYg1jnUY;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/tour/WhatsNewTourScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    .line 42
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    .line 43
    iput-boolean p1, p0, Lcom/squareup/tour/WhatsNewTourScreen;->onlyUnseen:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/tour/WhatsNewTourScreen;)Z
    .locals 0

    .line 30
    iget-boolean p0, p0, Lcom/squareup/tour/WhatsNewTourScreen;->onlyUnseen:Z

    return p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/tour/WhatsNewTourScreen;
    .locals 1

    .line 121
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 122
    :goto_0
    new-instance p0, Lcom/squareup/tour/WhatsNewTourScreen;

    invoke-direct {p0, v0}, Lcom/squareup/tour/WhatsNewTourScreen;-><init>(Z)V

    return-object p0
.end method

.method public static onlyUnseen()Lcom/squareup/tour/WhatsNewTourScreen;
    .locals 2

    .line 33
    new-instance v0, Lcom/squareup/tour/WhatsNewTourScreen;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/tour/WhatsNewTourScreen;-><init>(Z)V

    return-object v0
.end method

.method public static showAll()Lcom/squareup/tour/WhatsNewTourScreen;
    .locals 2

    .line 37
    new-instance v0, Lcom/squareup/tour/WhatsNewTourScreen;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tour/WhatsNewTourScreen;-><init>(Z)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 106
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InMainActivityScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 107
    iget-boolean p2, p0, Lcom/squareup/tour/WhatsNewTourScreen;->onlyUnseen:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 51
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 47
    sget v0, Lcom/squareup/tour/R$layout;->whats_new_tour_view:I

    return v0
.end method
