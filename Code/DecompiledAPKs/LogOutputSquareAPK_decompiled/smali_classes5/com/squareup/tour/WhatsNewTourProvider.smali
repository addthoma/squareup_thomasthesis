.class public interface abstract Lcom/squareup/tour/WhatsNewTourProvider;
.super Ljava/lang/Object;
.source "WhatsNewTourProvider.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0018\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/tour/WhatsNewTourProvider;",
        "",
        "displayAfterOnboarding",
        "",
        "getDisplayAfterOnboarding",
        "()Z",
        "tourPages",
        "",
        "Lcom/squareup/tour/SerializableHasTourPages;",
        "getTourPages",
        "()Ljava/util/List;",
        "pos-tour_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getDisplayAfterOnboarding()Z
.end method

.method public abstract getTourPages()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/tour/SerializableHasTourPages;",
            ">;"
        }
    .end annotation
.end method
