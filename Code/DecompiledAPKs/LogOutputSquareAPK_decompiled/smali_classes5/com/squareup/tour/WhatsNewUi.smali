.class public Lcom/squareup/tour/WhatsNewUi;
.super Ljava/lang/Object;
.source "WhatsNewUi.java"


# instance fields
.field private final badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

.field private final home:Lcom/squareup/ui/main/Home;

.field private final switchEmployeesEducationPresenter:Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private final tutorialPresenter:Lcom/squareup/register/tutorial/TutorialPresenter;

.field private final whatsNewSettings:Lcom/squareup/tour/WhatsNewSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/tour/WhatsNewSettings;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/ui/main/Home;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/tour/WhatsNewUi;->whatsNewSettings:Lcom/squareup/tour/WhatsNewSettings;

    .line 34
    iput-object p2, p0, Lcom/squareup/tour/WhatsNewUi;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    .line 35
    iput-object p4, p0, Lcom/squareup/tour/WhatsNewUi;->tutorialPresenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    .line 36
    iput-object p3, p0, Lcom/squareup/tour/WhatsNewUi;->switchEmployeesEducationPresenter:Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;

    .line 37
    iput-object p5, p0, Lcom/squareup/tour/WhatsNewUi;->home:Lcom/squareup/ui/main/Home;

    .line 38
    iput-object p6, p0, Lcom/squareup/tour/WhatsNewUi;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method

.method private hasActiveV2Tutorial()Z
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewUi;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    invoke-interface {v0}, Lcom/squareup/tutorialv2/TutorialCore;->hasActiveTutorial()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2BlockingSupport;->getValueOrThrow(Lio/reactivex/Observable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private shouldShowWhatsNewOverHome()Z
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewUi;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewUi;->tutorialPresenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->hasActiveTutorial()Z

    move-result v0

    if-nez v0, :cond_1

    .line 75
    invoke-direct {p0}, Lcom/squareup/tour/WhatsNewUi;->hasActiveV2Tutorial()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/tour/WhatsNewUi;->switchEmployeesEducationPresenter:Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;

    .line 76
    invoke-virtual {v0}, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->shouldBeShown()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    if-eqz v1, :cond_3

    .line 79
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewUi;->whatsNewSettings:Lcom/squareup/tour/WhatsNewSettings;

    invoke-virtual {v0}, Lcom/squareup/tour/WhatsNewSettings;->markAllPagesAsSeen()V

    .line 82
    :cond_3
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewUi;->whatsNewSettings:Lcom/squareup/tour/WhatsNewSettings;

    invoke-virtual {v0}, Lcom/squareup/tour/WhatsNewSettings;->hasUnseenPages()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public maybeShowPopup()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 60
    invoke-direct {p0}, Lcom/squareup/tour/WhatsNewUi;->shouldShowWhatsNewOverHome()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/squareup/tour/WhatsNewTourScreen;->onlyUnseen()Lcom/squareup/tour/WhatsNewTourScreen;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public redirectForWhatsNew(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 3

    .line 42
    iget-object v0, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 43
    iget-object v1, p0, Lcom/squareup/tour/WhatsNewUi;->home:Lcom/squareup/ui/main/Home;

    iget-object v2, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-interface {v1, v2}, Lcom/squareup/ui/main/Home;->getHomeScreens(Lflow/History;)Ljava/util/List;

    move-result-object v1

    .line 45
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {p0}, Lcom/squareup/tour/WhatsNewUi;->maybeShowPopup()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 48
    new-instance v1, Lcom/squareup/container/RedirectStep$Result;

    iget-object p1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-static {p1, v0}, Lcom/squareup/container/Command;->set(Lflow/History;Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/Command;

    move-result-object p1

    const-string v0, "What\'s New"

    invoke-direct {v1, v0, p1}, Lcom/squareup/container/RedirectStep$Result;-><init>(Ljava/lang/String;Lcom/squareup/container/Command;)V

    return-object v1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method
