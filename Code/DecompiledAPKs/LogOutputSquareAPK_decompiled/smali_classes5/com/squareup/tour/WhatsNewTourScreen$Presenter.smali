.class Lcom/squareup/tour/WhatsNewTourScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "WhatsNewTourScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tour/WhatsNewTourScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/tour/WhatsNewTourView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private complete:Z

.field private currentPage:I

.field private final flow:Lflow/Flow;

.field private pages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/tour/Tour$HasTourPages;",
            ">;"
        }
    .end annotation
.end field

.field private final whatsNewSettings:Lcom/squareup/tour/WhatsNewSettings;


# direct methods
.method constructor <init>(Lcom/squareup/tour/WhatsNewSettings;Lcom/squareup/analytics/Analytics;Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 65
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->whatsNewSettings:Lcom/squareup/tour/WhatsNewSettings;

    .line 67
    iput-object p2, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 68
    iput-object p3, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public getCurrentPage()I
    .locals 1

    .line 94
    iget v0, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->currentPage:I

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    .line 72
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 73
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/tour/WhatsNewTourScreen;

    .line 74
    invoke-static {p1}, Lcom/squareup/tour/WhatsNewTourScreen;->access$000(Lcom/squareup/tour/WhatsNewTourScreen;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->whatsNewSettings:Lcom/squareup/tour/WhatsNewSettings;

    .line 75
    invoke-virtual {v0}, Lcom/squareup/tour/WhatsNewSettings;->getAllUnseenPages()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->whatsNewSettings:Lcom/squareup/tour/WhatsNewSettings;

    invoke-virtual {v0}, Lcom/squareup/tour/WhatsNewSettings;->getAllPages()Ljava/util/List;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->pages:Ljava/util/List;

    .line 76
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->pages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    iput-boolean v2, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->complete:Z

    .line 77
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/ui/tour/TourClosedEvent;

    .line 78
    invoke-static {p1}, Lcom/squareup/tour/WhatsNewTourScreen;->access$000(Lcom/squareup/tour/WhatsNewTourScreen;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/ui/tour/TourClosedEvent$Origin;->WHATS_NEW_ON_LOGIN:Lcom/squareup/ui/tour/TourClosedEvent$Origin;

    goto :goto_2

    :cond_2
    sget-object p1, Lcom/squareup/ui/tour/TourClosedEvent$Origin;->WHATS_NEW_FROM_SUPPORT:Lcom/squareup/ui/tour/TourClosedEvent$Origin;

    :goto_2
    iget-boolean v3, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->complete:Z

    invoke-direct {v2, p1, v3, v1}, Lcom/squareup/ui/tour/TourClosedEvent;-><init>(Lcom/squareup/ui/tour/TourClosedEvent$Origin;ZZ)V

    .line 77
    invoke-interface {v0, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 81
    iget-object p1, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->whatsNewSettings:Lcom/squareup/tour/WhatsNewSettings;

    invoke-virtual {p1}, Lcom/squareup/tour/WhatsNewSettings;->markAllPagesAsSeen()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 85
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 86
    invoke-virtual {p0}, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tour/WhatsNewTourView;

    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->pages:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/squareup/tour/WhatsNewTourView;->setItems(Ljava/util/List;)V

    return-void
.end method

.method public setCurrentPage(I)V
    .locals 2

    .line 98
    iput p1, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->currentPage:I

    .line 99
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->pages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    if-ne p1, v0, :cond_0

    .line 100
    iput-boolean v1, p0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->complete:Z

    :cond_0
    return-void
.end method
