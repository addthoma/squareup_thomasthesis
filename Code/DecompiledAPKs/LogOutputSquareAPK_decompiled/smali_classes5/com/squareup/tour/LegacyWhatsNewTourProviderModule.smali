.class public abstract Lcom/squareup/tour/LegacyWhatsNewTourProviderModule;
.super Ljava/lang/Object;
.source "LegacyWhatsNewTourProviderModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideTourProvider(Lcom/squareup/tour/LegacyWhatsNewTourProvider;)Lcom/squareup/tour/WhatsNewTourProvider;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
