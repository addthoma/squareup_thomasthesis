.class public Lcom/squareup/swipe/AnalyticsSwipeEventLogger;
.super Ljava/lang/Object;
.source "AnalyticsSwipeEventLogger.java"

# interfaces
.implements Lcom/squareup/logging/SwipeEventLogger;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public logReaderCarrierDetectEvent(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;)V
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;

    invoke-direct {v1, p1}, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;-><init>(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
