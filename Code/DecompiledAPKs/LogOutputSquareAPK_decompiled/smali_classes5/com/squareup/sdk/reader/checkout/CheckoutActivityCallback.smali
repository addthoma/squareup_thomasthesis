.class public interface abstract Lcom/squareup/sdk/reader/checkout/CheckoutActivityCallback;
.super Ljava/lang/Object;
.source "CheckoutActivityCallback.java"

# interfaces
.implements Lcom/squareup/sdk/reader/core/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/sdk/reader/core/Callback<",
        "Lcom/squareup/sdk/reader/core/Result<",
        "Lcom/squareup/sdk/reader/checkout/CheckoutResult;",
        "Lcom/squareup/sdk/reader/core/ResultError<",
        "Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;",
        ">;>;>;"
    }
.end annotation
