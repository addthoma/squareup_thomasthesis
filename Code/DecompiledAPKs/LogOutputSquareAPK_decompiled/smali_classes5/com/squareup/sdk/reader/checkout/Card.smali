.class public final Lcom/squareup/sdk/reader/checkout/Card;
.super Ljava/lang/Object;
.source "Card.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/reader/checkout/Card$Builder;,
        Lcom/squareup/sdk/reader/checkout/Card$Brand;
    }
.end annotation


# instance fields
.field private final brand:Lcom/squareup/sdk/reader/checkout/Card$Brand;

.field private final cardholderName:Ljava/lang/String;

.field private final expirationMonth:Ljava/lang/Integer;

.field private final expirationYear:Ljava/lang/Integer;

.field private final id:Ljava/lang/String;

.field private final lastFourDigits:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/sdk/reader/checkout/Card$Brand;Ljava/lang/String;)V
    .locals 1

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "brand"

    .line 49
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/Card$Brand;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Card;->brand:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    const/4 p1, 0x0

    .line 50
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Card;->cardholderName:Ljava/lang/String;

    .line 51
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Card;->expirationMonth:Ljava/lang/Integer;

    .line 52
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Card;->expirationYear:Ljava/lang/Integer;

    .line 53
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Card;->id:Ljava/lang/String;

    const-string p1, "lastFourDigits"

    .line 54
    invoke-static {p2, p1}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Card;->lastFourDigits:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/Card$Builder;)V
    .locals 1

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Card$Builder;->access$100(Lcom/squareup/sdk/reader/checkout/Card$Builder;)Lcom/squareup/sdk/reader/checkout/Card$Brand;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/Card;->brand:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    .line 59
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Card$Builder;->access$200(Lcom/squareup/sdk/reader/checkout/Card$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/Card;->cardholderName:Ljava/lang/String;

    .line 60
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Card$Builder;->access$300(Lcom/squareup/sdk/reader/checkout/Card$Builder;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/Card;->expirationMonth:Ljava/lang/Integer;

    .line 61
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Card$Builder;->access$400(Lcom/squareup/sdk/reader/checkout/Card$Builder;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/Card;->expirationYear:Ljava/lang/Integer;

    .line 62
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Card$Builder;->access$500(Lcom/squareup/sdk/reader/checkout/Card$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/Card;->id:Ljava/lang/String;

    .line 63
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Card$Builder;->access$600(Lcom/squareup/sdk/reader/checkout/Card$Builder;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Card;->lastFourDigits:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/Card$Builder;Lcom/squareup/sdk/reader/checkout/Card$1;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/Card;-><init>(Lcom/squareup/sdk/reader/checkout/Card$Builder;)V

    return-void
.end method

.method public static newCardBuilder(Lcom/squareup/sdk/reader/checkout/Card$Brand;Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/Card$Builder;
    .locals 2

    const-string v0, "brand"

    .line 15
    invoke-static {p0, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "lastFourDigits"

    .line 16
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 17
    new-instance v0, Lcom/squareup/sdk/reader/checkout/Card$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/sdk/reader/checkout/Card$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Card$Brand;Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/Card$1;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_d

    .line 118
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_5

    .line 120
    :cond_1
    check-cast p1, Lcom/squareup/sdk/reader/checkout/Card;

    .line 122
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Card;->brand:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/Card;->brand:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    if-eq v2, v3, :cond_2

    return v1

    .line 123
    :cond_2
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Card;->lastFourDigits:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/Card;->lastFourDigits:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    goto :goto_0

    :cond_3
    iget-object v2, p1, Lcom/squareup/sdk/reader/checkout/Card;->lastFourDigits:Ljava/lang/String;

    if-eqz v2, :cond_4

    :goto_0
    return v1

    .line 127
    :cond_4
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Card;->cardholderName:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/Card;->cardholderName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    goto :goto_1

    :cond_5
    iget-object v2, p1, Lcom/squareup/sdk/reader/checkout/Card;->cardholderName:Ljava/lang/String;

    if-eqz v2, :cond_6

    :goto_1
    return v1

    .line 131
    :cond_6
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Card;->id:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/Card;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    goto :goto_2

    :cond_7
    iget-object v2, p1, Lcom/squareup/sdk/reader/checkout/Card;->id:Ljava/lang/String;

    if-eqz v2, :cond_8

    :goto_2
    return v1

    .line 132
    :cond_8
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Card;->expirationMonth:Ljava/lang/Integer;

    if-eqz v2, :cond_9

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/Card;->expirationMonth:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    goto :goto_3

    :cond_9
    iget-object v2, p1, Lcom/squareup/sdk/reader/checkout/Card;->expirationMonth:Ljava/lang/Integer;

    if-eqz v2, :cond_a

    :goto_3
    return v1

    .line 136
    :cond_a
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Card;->expirationYear:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/sdk/reader/checkout/Card;->expirationYear:Ljava/lang/Integer;

    if-eqz v2, :cond_b

    invoke-virtual {v2, p1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_c

    goto :goto_4

    :cond_b
    if-eqz p1, :cond_c

    :goto_4
    return v1

    :cond_c
    return v0

    :cond_d
    :goto_5
    return v1
.end method

.method public getBrand()Lcom/squareup/sdk/reader/checkout/Card$Brand;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Card;->brand:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    return-object v0
.end method

.method public getCardholderName()Ljava/lang/String;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Card;->cardholderName:Ljava/lang/String;

    return-object v0
.end method

.method public getExpirationMonth()Ljava/lang/Integer;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Card;->expirationMonth:Ljava/lang/Integer;

    return-object v0
.end method

.method public getExpirationYear()Ljava/lang/Integer;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Card;->expirationYear:Ljava/lang/Integer;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Card;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getLastFourDigits()Ljava/lang/String;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Card;->lastFourDigits:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .line 145
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Card;->brand:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/sdk/reader/checkout/Card$Brand;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 146
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Card;->lastFourDigits:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 147
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Card;->cardholderName:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 148
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Card;->id:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 149
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Card;->expirationMonth:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 150
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Card;->expirationYear:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Card{brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/Card;->brand:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cardholderName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/Card;->cardholderName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", expirationMonth="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Card;->expirationMonth:Ljava/lang/Integer;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", expirationYear="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Card;->expirationYear:Ljava/lang/Integer;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", id=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Card;->id:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", lastFourDigits=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Card;->lastFourDigits:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
