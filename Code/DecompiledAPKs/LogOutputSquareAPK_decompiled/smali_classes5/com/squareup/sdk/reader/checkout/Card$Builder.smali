.class public final Lcom/squareup/sdk/reader/checkout/Card$Builder;
.super Ljava/lang/Object;
.source "Card.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/reader/checkout/Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private brand:Lcom/squareup/sdk/reader/checkout/Card$Brand;

.field private cardholderName:Ljava/lang/String;

.field private expirationMonth:Ljava/lang/Integer;

.field private expirationYear:Ljava/lang/Integer;

.field private id:Ljava/lang/String;

.field private lastFourDigits:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/Card$Brand;Ljava/lang/String;)V
    .locals 0

    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Card$Builder;->brand:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    const/4 p1, 0x0

    .line 180
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Card$Builder;->cardholderName:Ljava/lang/String;

    .line 181
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Card$Builder;->id:Ljava/lang/String;

    .line 182
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Card$Builder;->expirationMonth:Ljava/lang/Integer;

    .line 183
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Card$Builder;->expirationYear:Ljava/lang/Integer;

    .line 184
    iput-object p2, p0, Lcom/squareup/sdk/reader/checkout/Card$Builder;->lastFourDigits:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/Card$Brand;Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/Card$1;)V
    .locals 0

    .line 170
    invoke-direct {p0, p1, p2}, Lcom/squareup/sdk/reader/checkout/Card$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Card$Brand;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/sdk/reader/checkout/Card$Builder;)Lcom/squareup/sdk/reader/checkout/Card$Brand;
    .locals 0

    .line 170
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Card$Builder;->brand:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/sdk/reader/checkout/Card$Builder;)Ljava/lang/String;
    .locals 0

    .line 170
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Card$Builder;->cardholderName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/sdk/reader/checkout/Card$Builder;)Ljava/lang/Integer;
    .locals 0

    .line 170
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Card$Builder;->expirationMonth:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/sdk/reader/checkout/Card$Builder;)Ljava/lang/Integer;
    .locals 0

    .line 170
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Card$Builder;->expirationYear:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/sdk/reader/checkout/Card$Builder;)Ljava/lang/String;
    .locals 0

    .line 170
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Card$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/sdk/reader/checkout/Card$Builder;)Ljava/lang/String;
    .locals 0

    .line 170
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Card$Builder;->lastFourDigits:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/sdk/reader/checkout/Card;
    .locals 2

    .line 208
    new-instance v0, Lcom/squareup/sdk/reader/checkout/Card;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sdk/reader/checkout/Card;-><init>(Lcom/squareup/sdk/reader/checkout/Card$Builder;Lcom/squareup/sdk/reader/checkout/Card$1;)V

    return-object v0
.end method

.method public cardholderName(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/Card$Builder;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Card$Builder;->cardholderName:Ljava/lang/String;

    return-object p0
.end method

.method public expirationMonth(I)Lcom/squareup/sdk/reader/checkout/Card$Builder;
    .locals 0

    .line 198
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Card$Builder;->expirationMonth:Ljava/lang/Integer;

    return-object p0
.end method

.method public expirationYear(I)Lcom/squareup/sdk/reader/checkout/Card$Builder;
    .locals 0

    .line 203
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Card$Builder;->expirationYear:Ljava/lang/Integer;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/Card$Builder;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Card$Builder;->id:Ljava/lang/String;

    return-object p0
.end method
