.class public interface abstract Lcom/squareup/sdk/reader/authorization/DeauthorizeCallback;
.super Ljava/lang/Object;
.source "DeauthorizeCallback.java"

# interfaces
.implements Lcom/squareup/sdk/reader/core/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/sdk/reader/core/Callback<",
        "Lcom/squareup/sdk/reader/core/Result<",
        "Ljava/lang/Void;",
        "Lcom/squareup/sdk/reader/core/ResultError<",
        "Lcom/squareup/sdk/reader/authorization/DeauthorizeErrorCode;",
        ">;>;>;"
    }
.end annotation
