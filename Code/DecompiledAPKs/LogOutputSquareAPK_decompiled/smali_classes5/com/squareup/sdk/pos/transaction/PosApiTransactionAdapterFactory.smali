.class public abstract Lcom/squareup/sdk/pos/transaction/PosApiTransactionAdapterFactory;
.super Ljava/lang/Object;
.source "PosApiTransactionAdapterFactory.java"

# interfaces
.implements Lcom/google/gson/TypeAdapterFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/google/gson/TypeAdapterFactory;
    .locals 1

    .line 11
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValueGson_PosApiTransactionAdapterFactory;

    invoke-direct {v0}, Lcom/squareup/sdk/pos/transaction/AutoValueGson_PosApiTransactionAdapterFactory;-><init>()V

    return-object v0
.end method
