.class final Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails;
.super Lcom/squareup/sdk/pos/transaction/$AutoValue_TenderCardDetails;
.source "AutoValue_TenderCardDetails.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails$1;

    invoke-direct {v0}, Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails$1;-><init>()V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/sdk/pos/transaction/Card;Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Lcom/squareup/sdk/pos/transaction/$AutoValue_TenderCardDetails;-><init>(Lcom/squareup/sdk/pos/transaction/Card;Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails;->card()Lcom/squareup/sdk/pos/transaction/Card;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 29
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails;->entryMethod()Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
