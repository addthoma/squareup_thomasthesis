.class final Lcom/squareup/sdk/pos/transaction/AutoValue_Tender$1;
.super Ljava/lang/Object;
.source "AutoValue_Tender.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;
    .locals 11

    .line 12
    new-instance v10, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;

    .line 13
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 14
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v2, 0x0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    :cond_0
    move-object v3, v2

    :goto_0
    const-class v0, Lcom/squareup/sdk/pos/transaction/DateTime;

    .line 15
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/sdk/pos/transaction/DateTime;

    const-class v0, Lcom/squareup/sdk/pos/transaction/Money;

    .line 16
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/sdk/pos/transaction/Money;

    const-class v0, Lcom/squareup/sdk/pos/transaction/Money;

    .line 17
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/sdk/pos/transaction/Money;

    .line 18
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    goto :goto_1

    :cond_1
    move-object v7, v2

    .line 19
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/sdk/pos/transaction/Tender$Type;->valueOf(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Tender$Type;

    move-result-object v8

    const-class v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

    .line 20
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

    const-class v0, Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

    .line 21
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

    move-object v0, v10

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, p1

    invoke-direct/range {v0 .. v9}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/DateTime;Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/Tender$Type;Lcom/squareup/sdk/pos/transaction/TenderCardDetails;Lcom/squareup/sdk/pos/transaction/TenderCashDetails;)V

    return-object v10
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 9
    invoke-virtual {p0, p1}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;
    .locals 0

    .line 26
    new-array p1, p1, [Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 9
    invoke-virtual {p0, p1}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender$1;->newArray(I)[Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;

    move-result-object p1

    return-object p1
.end method
