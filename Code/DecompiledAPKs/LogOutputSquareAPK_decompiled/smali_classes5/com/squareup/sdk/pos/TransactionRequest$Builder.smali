.class public final Lcom/squareup/sdk/pos/TransactionRequest$Builder;
.super Ljava/lang/Object;
.source "TransactionRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/TransactionRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field allowSplitTender:Z

.field autoReturn:Z

.field final currencyCode:Lcom/squareup/sdk/pos/CurrencyCode;

.field customerId:Ljava/lang/String;

.field locationId:Ljava/lang/String;

.field note:Ljava/lang/String;

.field skipReceipt:Z

.field state:Ljava/lang/String;

.field final tenderTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/pos/TransactionRequest$TenderType;",
            ">;"
        }
    .end annotation
.end field

.field final totalAmount:I


# direct methods
.method public constructor <init>(ILcom/squareup/sdk/pos/CurrencyCode;)V
    .locals 0

    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-ltz p1, :cond_0

    .line 197
    iput p1, p0, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->totalAmount:I

    const-string p1, "currencyCode"

    .line 198
    invoke-static {p2, p1}, Lcom/squareup/sdk/pos/internal/PosSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/pos/CurrencyCode;

    iput-object p1, p0, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->currencyCode:Lcom/squareup/sdk/pos/CurrencyCode;

    .line 199
    const-class p1, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    invoke-static {p1}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->tenderTypes:Ljava/util/Set;

    return-void

    .line 195
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo p2, "totalAmount must be non-negative"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public allowSplitTender(Z)Lcom/squareup/sdk/pos/TransactionRequest$Builder;
    .locals 0

    .line 321
    iput-boolean p1, p0, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->allowSplitTender:Z

    return-object p0
.end method

.method public autoReturn(Z)Lcom/squareup/sdk/pos/TransactionRequest$Builder;
    .locals 0

    .line 261
    iput-boolean p1, p0, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->autoReturn:Z

    return-object p0
.end method

.method public build()Lcom/squareup/sdk/pos/TransactionRequest;
    .locals 1

    .line 329
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest;

    invoke-direct {v0, p0}, Lcom/squareup/sdk/pos/TransactionRequest;-><init>(Lcom/squareup/sdk/pos/TransactionRequest$Builder;)V

    return-object v0
.end method

.method public customerId(Ljava/lang/String;)Lcom/squareup/sdk/pos/TransactionRequest$Builder;
    .locals 0

    .line 304
    iput-object p1, p0, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->customerId:Ljava/lang/String;

    return-object p0
.end method

.method public enforceBusinessLocation(Ljava/lang/String;)Lcom/squareup/sdk/pos/TransactionRequest$Builder;
    .locals 0

    .line 278
    iput-object p1, p0, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->locationId:Ljava/lang/String;

    return-object p0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/sdk/pos/TransactionRequest$Builder;
    .locals 2

    if-eqz p1, :cond_1

    .line 248
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x1f4

    if-gt v0, v1, :cond_0

    goto :goto_0

    .line 249
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "note character length must be less than 500"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 252
    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public restrictTendersTo(Ljava/util/Collection;)Lcom/squareup/sdk/pos/TransactionRequest$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/sdk/pos/TransactionRequest$TenderType;",
            ">;)",
            "Lcom/squareup/sdk/pos/TransactionRequest$Builder;"
        }
    .end annotation

    const-string/jumbo v0, "tenderTypes"

    .line 214
    invoke-static {p1, v0}, Lcom/squareup/sdk/pos/internal/PosSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 215
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->tenderTypes:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 219
    iget-object v0, p0, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->tenderTypes:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-object p0

    .line 216
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Please restrict to at least one TenderType."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public varargs restrictTendersTo([Lcom/squareup/sdk/pos/TransactionRequest$TenderType;)Lcom/squareup/sdk/pos/TransactionRequest$Builder;
    .locals 1

    const-string/jumbo v0, "tenderTypes"

    .line 226
    invoke-static {p1, v0}, Lcom/squareup/sdk/pos/internal/PosSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 227
    array-length v0, p1

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->tenderTypes:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 231
    iget-object v0, p0, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->tenderTypes:Ljava/util/Set;

    invoke-static {v0, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    return-object p0

    .line 228
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Please restrict to at least one TenderType."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public skipReceipt(Z)Lcom/squareup/sdk/pos/TransactionRequest$Builder;
    .locals 0

    .line 313
    iput-boolean p1, p0, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->skipReceipt:Z

    return-object p0
.end method

.method public state(Ljava/lang/String;)Lcom/squareup/sdk/pos/TransactionRequest$Builder;
    .locals 0

    .line 291
    iput-object p1, p0, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->state:Ljava/lang/String;

    return-object p0
.end method
