.class public abstract Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink;
.super Lcom/squareup/squid/SquareDeviceDeepLink;
.source "SquareDeviceDeepLink.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squid/SquareDeviceDeepLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "StatusBarLink"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$ExitTourLink;,
        Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00032\u00020\u0001:\u0002\u0003\u0004B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0001\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink;",
        "Lcom/squareup/squid/SquareDeviceDeepLink;",
        "()V",
        "Companion",
        "ExitTourLink",
        "Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$ExitTourLink;",
        "square-device_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final EXIT_TOUR_DEEP_LINK:Ljava/lang/String; = "square-register://statusbar/exitTour"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink;->Companion:Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 14
    invoke-direct {p0, v0}, Lcom/squareup/squid/SquareDeviceDeepLink;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink;-><init>()V

    return-void
.end method

.method public static final synthetic access$Companion()Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$Companion;
    .locals 1

    sget-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink;->Companion:Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$Companion;

    return-object v0
.end method
