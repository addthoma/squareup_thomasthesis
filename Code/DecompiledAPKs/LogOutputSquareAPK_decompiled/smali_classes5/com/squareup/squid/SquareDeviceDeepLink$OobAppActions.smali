.class public abstract Lcom/squareup/squid/SquareDeviceDeepLink$OobAppActions;
.super Lcom/squareup/squid/SquareDeviceDeepLink;
.source "SquareDeviceDeepLink.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squid/SquareDeviceDeepLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "OobAppActions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/squid/SquareDeviceDeepLink$OobAppActions$OobAcceptTerms;,
        Lcom/squareup/squid/SquareDeviceDeepLink$OobAppActions$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \t2\u00020\u0001:\u0002\t\nB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0007\u001a\u00020\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0001\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/squid/SquareDeviceDeepLink$OobAppActions;",
        "Lcom/squareup/squid/SquareDeviceDeepLink;",
        "action",
        "",
        "(Ljava/lang/String;)V",
        "getAction",
        "()Ljava/lang/String;",
        "getIntent",
        "Landroid/content/Intent;",
        "Companion",
        "OobAcceptTerms",
        "Lcom/squareup/squid/SquareDeviceDeepLink$OobAppActions$OobAcceptTerms;",
        "square-device_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ACTION_OOB_ACCEPT_TERMS:Ljava/lang/String; = "com.squareup.oob.intent.action.AcceptedTerms"

.field public static final Companion:Lcom/squareup/squid/SquareDeviceDeepLink$OobAppActions$Companion;


# instance fields
.field private final action:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/squid/SquareDeviceDeepLink$OobAppActions$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/squid/SquareDeviceDeepLink$OobAppActions$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$OobAppActions;->Companion:Lcom/squareup/squid/SquareDeviceDeepLink$OobAppActions$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 119
    invoke-direct {p0, v0}, Lcom/squareup/squid/SquareDeviceDeepLink;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/squid/SquareDeviceDeepLink$OobAppActions;->action:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 117
    invoke-direct {p0, p1}, Lcom/squareup/squid/SquareDeviceDeepLink$OobAppActions;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getAction()Ljava/lang/String;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/squid/SquareDeviceDeepLink$OobAppActions;->action:Ljava/lang/String;

    return-object v0
.end method

.method public final getIntent()Landroid/content/Intent;
    .locals 2

    .line 120
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/squareup/squid/SquareDeviceDeepLink$OobAppActions;->action:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
