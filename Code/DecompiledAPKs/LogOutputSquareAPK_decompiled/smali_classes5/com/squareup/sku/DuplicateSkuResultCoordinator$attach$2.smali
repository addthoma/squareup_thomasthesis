.class final Lcom/squareup/sku/DuplicateSkuResultCoordinator$attach$2;
.super Ljava/lang/Object;
.source "DuplicateSkuResultCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/BiConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/sku/DuplicateSkuResultCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/BiConsumer<",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
        ">;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0006\u001a\n \u0005*\u0004\u0018\u00010\u00070\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "list",
        "",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/sku/DuplicateSkuResultCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/sku/DuplicateSkuResultCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator$attach$2;->this$0:Lcom/squareup/sku/DuplicateSkuResultCoordinator;

    iput-object p2, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Ljava/util/List;

    check-cast p2, Ljava/lang/Throwable;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/sku/DuplicateSkuResultCoordinator$attach$2;->accept(Ljava/util/List;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final accept(Ljava/util/List;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 48
    iget-object p2, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator$attach$2;->this$0:Lcom/squareup/sku/DuplicateSkuResultCoordinator;

    iget-object v0, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-static {p2, v0}, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->access$configureActionBar(Lcom/squareup/sku/DuplicateSkuResultCoordinator;Landroid/view/View;)V

    .line 49
    iget-object p2, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator$attach$2;->this$0:Lcom/squareup/sku/DuplicateSkuResultCoordinator;

    const-string v0, "list"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/util/Collection;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->access$inflateVariationList(Lcom/squareup/sku/DuplicateSkuResultCoordinator;Ljava/util/List;)V

    return-void
.end method
