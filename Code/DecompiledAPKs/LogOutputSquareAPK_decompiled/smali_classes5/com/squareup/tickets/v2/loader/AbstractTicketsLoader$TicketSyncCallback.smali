.class Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;
.super Ljava/lang/Object;
.source "AbstractTicketsLoader.java"

# interfaces
.implements Lcom/squareup/tickets/TicketsCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TicketSyncCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/tickets/TicketsCallback<",
        "Lcom/squareup/tickets/TicketRowCursorList;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;


# direct methods
.method private constructor <init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)V
    .locals 0

    .line 313
    iput-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$1;)V
    .locals 0

    .line 313
    invoke-direct {p0, p1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/tickets/TicketsResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsResult<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation

    .line 317
    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tickets/TicketRowCursorList;

    if-nez p1, :cond_0

    return-void

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    invoke-static {v0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->access$500(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 328
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->setTicketCursor(Lcom/squareup/tickets/TicketRowCursorList;Z)V

    goto :goto_0

    .line 329
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->getSearchText()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    invoke-static {v2}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->access$600(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/tickets/TicketRowCursorList;->getSearchText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 330
    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->getSort()Lcom/squareup/tickets/TicketSort;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    invoke-static {v3}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->access$600(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/tickets/TicketRowCursorList;->getSort()Lcom/squareup/tickets/TicketSort;

    move-result-object v3

    if-ne v0, v3, :cond_2

    .line 331
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    invoke-virtual {v0, p1, v2}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->setTicketCursor(Lcom/squareup/tickets/TicketRowCursorList;Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 335
    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->getSearchText()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->getSort()Lcom/squareup/tickets/TicketSort;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/tickets/TicketSort;->name()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    const/4 p1, 0x2

    iget-object v1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    invoke-static {v1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->access$600(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/tickets/TicketRowCursorList;->getSearchText()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, p1

    const/4 p1, 0x3

    iget-object v1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    .line 336
    invoke-static {v1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->access$600(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/tickets/TicketRowCursorList;->getSort()Lcom/squareup/tickets/TicketSort;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/tickets/TicketSort;->name()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, p1

    const-string p1, "[Tickets_Loader] Did not set new cursor from sync as the cursor has a filter of \"%s\"+%s while current filter is \"%s\"+%s."

    .line 333
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 338
    :goto_0
    iget-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    invoke-static {p1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->access$700(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Lcom/squareup/log/tickets/OpenTicketsLogger;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/log/tickets/OpenTicketsLogger;->stopListResponseToTicketList()V

    return-void
.end method
