.class public Lcom/squareup/tickets/RealOpenTicketsSettings;
.super Ljava/lang/Object;
.source "RealOpenTicketsSettings.java"

# interfaces
.implements Lcom/squareup/tickets/OpenTicketsSettings;


# static fields
.field private static final DEAULT_TICKET_TEMPLATE_COUNT:I = 0xa

.field private static final MAX_TICKET_GROUP_COUNT:I = 0x64

.field private static final MAX_TICKET_TEMPLATES_PER_GROUP_COUNT:I = 0x64


# instance fields
.field private final accountStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final defaultTicketTemplateCount:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final device:Lcom/squareup/util/Device;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final maxTicketGroupCount:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final maxTicketTemplatesPerGroup:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final onPredefinedTicketsEnabled:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketEnabledSetting:Lcom/squareup/tickets/OpenTicketEnabledSetting;

.field private final openTicketsAsHomeScreenEnabled:Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;

.field private final openTicketsAsSavedCarts:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final predefinedTicketsEnabled:Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Ljavax/inject/Provider;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;Lcom/squareup/tickets/OpenTicketEnabledSetting;Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/tickets/OpenTicketEnabledSetting;",
            "Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;",
            "Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->onPredefinedTicketsEnabled:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 67
    iput-object p1, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->analytics:Lcom/squareup/analytics/Analytics;

    .line 68
    iput-object p2, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->device:Lcom/squareup/util/Device;

    .line 69
    iput-object p4, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->accountStatusProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p5, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 71
    iput-object p6, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->features:Lcom/squareup/settings/server/Features;

    .line 72
    iput-object p3, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 73
    iput-object p9, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->openTicketsAsHomeScreenEnabled:Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;

    .line 74
    iput-object p10, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->openTicketsAsSavedCarts:Ljavax/inject/Provider;

    .line 75
    iput-object p7, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->openTicketEnabledSetting:Lcom/squareup/tickets/OpenTicketEnabledSetting;

    .line 76
    iput-object p8, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->predefinedTicketsEnabled:Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;

    .line 77
    iput-object p11, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->defaultTicketTemplateCount:Lcom/squareup/settings/LocalSetting;

    .line 78
    iput-object p12, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->maxTicketGroupCount:Lcom/squareup/settings/LocalSetting;

    .line 79
    iput-object p13, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->maxTicketTemplatesPerGroup:Lcom/squareup/settings/LocalSetting;

    return-void
.end method


# virtual methods
.method public getDefaultTicketTemplateCount()I
    .locals 2

    .line 275
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->defaultTicketTemplateCount:Lcom/squareup/settings/LocalSetting;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getMaxTicketGroupCount()I
    .locals 2

    .line 290
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->accountStatusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    if-eqz v0, :cond_0

    .line 291
    iget-object v1, v0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_groups_max:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 292
    iget-object v1, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->maxTicketGroupCount:Lcom/squareup/settings/LocalSetting;

    iget-object v0, v0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_groups_max:Ljava/lang/Integer;

    invoke-interface {v1, v0}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    .line 294
    :cond_0
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->maxTicketGroupCount:Lcom/squareup/settings/LocalSetting;

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getMaxTicketTemplatesPerGroup()I
    .locals 2

    .line 310
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->accountStatusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    if-eqz v0, :cond_0

    .line 311
    iget-object v1, v0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_templates_per_group_max:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 312
    iget-object v1, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->maxTicketTemplatesPerGroup:Lcom/squareup/settings/LocalSetting;

    iget-object v0, v0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_templates_per_group_max:Ljava/lang/Integer;

    invoke-interface {v1, v0}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->maxTicketTemplatesPerGroup:Lcom/squareup/settings/LocalSetting;

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public isBulkDeleteAllowed()Z
    .locals 2

    .line 136
    invoke-virtual {p0}, Lcom/squareup/tickets/RealOpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_BULK_DELETE:Lcom/squareup/settings/server/Features$Feature;

    .line 137
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->device:Lcom/squareup/util/Device;

    .line 138
    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 139
    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isBulkVoidAllowed()Z
    .locals 2

    .line 148
    invoke-virtual {p0}, Lcom/squareup/tickets/RealOpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_BULK_DELETE:Lcom/squareup/settings/server/Features$Feature;

    .line 149
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->device:Lcom/squareup/util/Device;

    .line 150
    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 151
    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isDipTapToCreateTicketAllowed()Z
    .locals 2

    .line 119
    invoke-virtual {p0}, Lcom/squareup/tickets/RealOpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_DIP_TAP_TO_CREATE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isMergeTicketsAllowed()Z
    .locals 2

    .line 162
    invoke-virtual {p0}, Lcom/squareup/tickets/RealOpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_MERGE:Lcom/squareup/settings/server/Features$Feature;

    .line 163
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    invoke-virtual {p0}, Lcom/squareup/tickets/RealOpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->device:Lcom/squareup/util/Device;

    .line 165
    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isMoveTicketsAllowed()Z
    .locals 2

    .line 176
    invoke-virtual {p0}, Lcom/squareup/tickets/RealOpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_MERGE:Lcom/squareup/settings/server/Features$Feature;

    .line 177
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    invoke-virtual {p0}, Lcom/squareup/tickets/RealOpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->device:Lcom/squareup/util/Device;

    .line 179
    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOpenTicketsAllowed()Z
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->openTicketEnabledSetting:Lcom/squareup/tickets/OpenTicketEnabledSetting;

    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicketEnabledSetting;->isAllowedWhenUsingLocal()Z

    move-result v0

    return v0
.end method

.method public isOpenTicketsAsHomeScreenEnabled()Z
    .locals 1

    .line 258
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->openTicketsAsHomeScreenEnabled:Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;

    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isOpenTicketsEmployeeFilteringEnabled()Z
    .locals 2

    .line 103
    invoke-virtual {p0}, Lcom/squareup/tickets/RealOpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_EMPLOYEE_FILTERING:Lcom/squareup/settings/server/Features$Feature;

    .line 104
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 105
    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->isEldmEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOpenTicketsEnabled()Z
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/tickets/RealOpenTicketsSettings;->isSavedCartsMode()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->openTicketEnabledSetting:Lcom/squareup/tickets/OpenTicketEnabledSetting;

    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicketEnabledSetting;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isPredefinedTicketsAllowed()Z
    .locals 1

    .line 230
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->predefinedTicketsEnabled:Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;

    invoke-virtual {v0}, Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;->isAllowedWhenUsingLocal()Z

    move-result v0

    return v0
.end method

.method public isPredefinedTicketsEnabled()Z
    .locals 1

    .line 238
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->predefinedTicketsEnabled:Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;

    invoke-virtual {v0}, Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isSavedCartsMode()Z
    .locals 1

    .line 212
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->openTicketsAsSavedCarts:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isSplitTicketsAllowed()Z
    .locals 2

    .line 127
    invoke-virtual {p0}, Lcom/squareup/tickets/RealOpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SPLIT_TICKET:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSwipeToCreateTicketAllowed()Z
    .locals 2

    .line 112
    invoke-virtual {p0}, Lcom/squareup/tickets/RealOpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_SWIPE_TO_CREATE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isTicketTransferAllowed()Z
    .locals 2

    .line 188
    invoke-virtual {p0}, Lcom/squareup/tickets/RealOpenTicketsSettings;->isOpenTicketsEmployeeFilteringEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_TRANSFER:Lcom/squareup/settings/server/Features$Feature;

    .line 189
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->device:Lcom/squareup/util/Device;

    .line 190
    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 191
    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->isEldmEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isUsingDeviceProfiles()Z
    .locals 2

    .line 205
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEVICE_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public onPredefinedTicketsEnabled()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 330
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->onPredefinedTicketsEnabled:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public setDefaultTicketTemplateCount(I)V
    .locals 1

    .line 283
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->defaultTicketTemplateCount:Lcom/squareup/settings/LocalSetting;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    .line 221
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->openTicketEnabledSetting:Lcom/squareup/tickets/OpenTicketEnabledSetting;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/tickets/OpenTicketEnabledSetting;->setValueLocally(Ljava/lang/Object;)V

    .line 222
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->onPredefinedTicketsEnabled:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {p0}, Lcom/squareup/tickets/RealOpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 223
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/log/tickets/TicketSettingChanged;->openTicketsEnabled(Z)Lcom/squareup/log/tickets/TicketSettingChanged;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public setMaxTicketGroupCount(I)V
    .locals 1

    .line 302
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->maxTicketGroupCount:Lcom/squareup/settings/LocalSetting;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public setMaxTicketTemplatesPerGroup(I)V
    .locals 1

    .line 323
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->maxTicketTemplatesPerGroup:Lcom/squareup/settings/LocalSetting;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public setOpenTicketsAsHomeScreenEnabled(Z)V
    .locals 2

    .line 266
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->openTicketsAsHomeScreenEnabled:Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;->setValueLocally(Ljava/lang/Object;)V

    .line 267
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/log/tickets/TicketSettingChanged;->openTicketsAsHomeScreenEnabled(Z)Lcom/squareup/log/tickets/TicketSettingChanged;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public setPredefinedTicketsEnabled(Z)V
    .locals 2

    .line 247
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->predefinedTicketsEnabled:Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;

    invoke-virtual {v0}, Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;->useDeviceProfile()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->predefinedTicketsEnabled:Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;->setValueLocally(Ljava/lang/Object;)V

    .line 250
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->onPredefinedTicketsEnabled:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 251
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/log/tickets/TicketSettingChanged;->predefinedTicketsEnabled(Z)Lcom/squareup/log/tickets/TicketSettingChanged;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public shouldShowOptInBetaWarning()Z
    .locals 2

    .line 198
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_SHOW_BETA_OPT_IN:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method
