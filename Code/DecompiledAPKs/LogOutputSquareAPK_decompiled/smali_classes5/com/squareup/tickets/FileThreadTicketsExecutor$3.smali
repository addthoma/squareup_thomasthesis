.class Lcom/squareup/tickets/FileThreadTicketsExecutor$3;
.super Ljava/lang/Object;
.source "FileThreadTicketsExecutor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tickets/FileThreadTicketsExecutor;->executeOnFileThread(Ljava/util/concurrent/Callable;Lcom/squareup/tickets/TicketsCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tickets/FileThreadTicketsExecutor;

.field final synthetic val$callback:Lcom/squareup/tickets/TicketsCallback;

.field final synthetic val$work:Ljava/util/concurrent/Callable;


# direct methods
.method constructor <init>(Lcom/squareup/tickets/FileThreadTicketsExecutor;Ljava/util/concurrent/Callable;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$3;->this$0:Lcom/squareup/tickets/FileThreadTicketsExecutor;

    iput-object p2, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$3;->val$work:Ljava/util/concurrent/Callable;

    iput-object p3, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$3;->val$callback:Lcom/squareup/tickets/TicketsCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 60
    :try_start_0
    iget-object v0, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$3;->val$work:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    iget-object v1, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$3;->this$0:Lcom/squareup/tickets/FileThreadTicketsExecutor;

    invoke-static {v1}, Lcom/squareup/tickets/FileThreadTicketsExecutor;->access$100(Lcom/squareup/tickets/FileThreadTicketsExecutor;)Lcom/squareup/thread/executor/MainThread;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$3;->val$callback:Lcom/squareup/tickets/TicketsCallback;

    invoke-static {v1, v2, v0}, Lcom/squareup/tickets/FileThreadTicketsExecutor;->access$300(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/TicketsCallback;Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    const-string v1, "Error executing ticket task."

    .line 62
    invoke-static {v0, v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 63
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$3;->this$0:Lcom/squareup/tickets/FileThreadTicketsExecutor;

    invoke-static {v0}, Lcom/squareup/tickets/FileThreadTicketsExecutor;->access$100(Lcom/squareup/tickets/FileThreadTicketsExecutor;)Lcom/squareup/thread/executor/MainThread;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$3;->val$callback:Lcom/squareup/tickets/TicketsCallback;

    invoke-static {v0, v2, v1}, Lcom/squareup/tickets/FileThreadTicketsExecutor;->access$200(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/TicketsCallback;Ljava/lang/Throwable;)V

    return-void
.end method
