.class public final synthetic Lcom/squareup/tickets/-$$Lambda$RealTickets$5fDB-wxb-Ae-MVaCPNXnr5p03rA;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/tickets/RegisterTicketTask;


# instance fields
.field private final synthetic f$0:Lcom/squareup/tickets/RealTickets;

.field private final synthetic f$1:Ljava/lang/String;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/tickets/RealTickets;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$5fDB-wxb-Ae-MVaCPNXnr5p03rA;->f$0:Lcom/squareup/tickets/RealTickets;

    iput-object p2, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$5fDB-wxb-Ae-MVaCPNXnr5p03rA;->f$1:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public bridge synthetic perform(Lcom/squareup/tickets/TicketDatabase;)Ljava/lang/Object;
    .locals 0

    invoke-static {p0, p1}, Lcom/squareup/tickets/RegisterTicketTask$-CC;->$default$perform(Lcom/squareup/tickets/RegisterTicketTask;Lcom/squareup/tickets/TicketDatabase;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final perform(Lcom/squareup/tickets/TicketStore;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$5fDB-wxb-Ae-MVaCPNXnr5p03rA;->f$0:Lcom/squareup/tickets/RealTickets;

    iget-object v1, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$5fDB-wxb-Ae-MVaCPNXnr5p03rA;->f$1:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/tickets/RealTickets;->lambda$getTicketAndLock$4$RealTickets(Ljava/lang/String;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/tickets/OpenTicket;

    move-result-object p1

    return-object p1
.end method
