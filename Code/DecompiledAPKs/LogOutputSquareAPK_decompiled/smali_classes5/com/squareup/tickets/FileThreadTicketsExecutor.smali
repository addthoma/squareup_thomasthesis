.class public Lcom/squareup/tickets/FileThreadTicketsExecutor;
.super Ljava/lang/Object;
.source "FileThreadTicketsExecutor.java"

# interfaces
.implements Lcom/squareup/tickets/TicketsExecutor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tickets/FileThreadTicketsExecutor$Failure;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D::",
        "Lcom/squareup/tickets/TicketDatabase;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/tickets/TicketsExecutor<",
        "TD;>;"
    }
.end annotation


# instance fields
.field private final fileThreadExecutor:Ljava/util/concurrent/Executor;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final ticketDb:Lcom/squareup/tickets/TicketDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/TicketDatabase;Ljava/util/concurrent/Executor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/MainThread;",
            "TD;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 24
    iput-object p2, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor;->ticketDb:Lcom/squareup/tickets/TicketDatabase;

    .line 25
    iput-object p3, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/tickets/FileThreadTicketsExecutor;)Lcom/squareup/tickets/TicketDatabase;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor;->ticketDb:Lcom/squareup/tickets/TicketDatabase;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/tickets/FileThreadTicketsExecutor;)Lcom/squareup/thread/executor/MainThread;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/TicketsCallback;Ljava/lang/Throwable;)V
    .locals 0

    .line 15
    invoke-static {p0, p1, p2}, Lcom/squareup/tickets/FileThreadTicketsExecutor;->fail(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/TicketsCallback;Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/TicketsCallback;Ljava/lang/Object;)V
    .locals 0

    .line 15
    invoke-static {p0, p1, p2}, Lcom/squareup/tickets/FileThreadTicketsExecutor;->succeed(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/TicketsCallback;Ljava/lang/Object;)V

    return-void
.end method

.method private executeOnFileThread(Ljava/util/concurrent/Callable;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable<",
            "TT;>;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/tickets/FileThreadTicketsExecutor$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/tickets/FileThreadTicketsExecutor$3;-><init>(Lcom/squareup/tickets/FileThreadTicketsExecutor;Ljava/util/concurrent/Callable;Lcom/squareup/tickets/TicketsCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static fail(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/TicketsCallback;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "TT;>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 83
    new-instance v0, Lcom/squareup/tickets/FileThreadTicketsExecutor$5;

    invoke-direct {v0, p1, p2}, Lcom/squareup/tickets/FileThreadTicketsExecutor$5;-><init>(Lcom/squareup/tickets/TicketsCallback;Ljava/lang/Throwable;)V

    invoke-interface {p0, v0}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static succeed(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/TicketsCallback;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "TT;>;TT;)V"
        }
    .end annotation

    .line 74
    new-instance v0, Lcom/squareup/tickets/FileThreadTicketsExecutor$4;

    invoke-direct {v0, p1, p2}, Lcom/squareup/tickets/FileThreadTicketsExecutor$4;-><init>(Lcom/squareup/tickets/TicketsCallback;Ljava/lang/Object;)V

    invoke-interface {p0, v0}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/tickets/TicketsTask<",
            "TT;TD;>;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "Cannot interact with tickets from outside of main thread."

    .line 44
    invoke-static {v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread(Ljava/lang/String;)V

    const-string v0, "task"

    .line 45
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "callback"

    .line 46
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 47
    new-instance v0, Lcom/squareup/tickets/FileThreadTicketsExecutor$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/tickets/FileThreadTicketsExecutor$2;-><init>(Lcom/squareup/tickets/FileThreadTicketsExecutor;Lcom/squareup/tickets/TicketsTask;)V

    invoke-direct {p0, v0, p2}, Lcom/squareup/tickets/FileThreadTicketsExecutor;->executeOnFileThread(Ljava/util/concurrent/Callable;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 2

    const-string v0, "Cannot interact with tickets from outside of main thread."

    .line 29
    invoke-static {v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread(Ljava/lang/String;)V

    const-string/jumbo v0, "work"

    .line 30
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 31
    iget-object v0, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/tickets/FileThreadTicketsExecutor$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/tickets/FileThreadTicketsExecutor$1;-><init>(Lcom/squareup/tickets/FileThreadTicketsExecutor;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
