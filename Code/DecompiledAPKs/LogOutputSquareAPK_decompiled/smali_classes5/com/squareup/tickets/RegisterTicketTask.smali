.class public interface abstract Lcom/squareup/tickets/RegisterTicketTask;
.super Ljava/lang/Object;
.source "RegisterTicketTask.java"

# interfaces
.implements Lcom/squareup/tickets/TicketsTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/tickets/TicketsTask<",
        "TT;",
        "Lcom/squareup/tickets/TicketStore;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract perform(Lcom/squareup/tickets/TicketStore;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketStore;",
            ")TT;"
        }
    .end annotation
.end method
