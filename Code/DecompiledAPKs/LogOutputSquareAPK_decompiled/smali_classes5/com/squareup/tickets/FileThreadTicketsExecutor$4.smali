.class final Lcom/squareup/tickets/FileThreadTicketsExecutor$4;
.super Ljava/lang/Object;
.source "FileThreadTicketsExecutor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tickets/FileThreadTicketsExecutor;->succeed(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/TicketsCallback;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$callback:Lcom/squareup/tickets/TicketsCallback;

.field final synthetic val$result:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/squareup/tickets/TicketsCallback;Ljava/lang/Object;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$4;->val$callback:Lcom/squareup/tickets/TicketsCallback;

    iput-object p2, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$4;->val$result:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$4;->val$callback:Lcom/squareup/tickets/TicketsCallback;

    iget-object v1, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$4;->val$result:Ljava/lang/Object;

    invoke-static {v1}, Lcom/squareup/tickets/TicketsResults;->of(Ljava/lang/Object;)Lcom/squareup/tickets/TicketsResult;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/tickets/TicketsCallback;->call(Lcom/squareup/tickets/TicketsResult;)V

    return-void
.end method
