.class synthetic Lcom/squareup/tickets/SqliteTicketStore$1;
.super Ljava/lang/Object;
.source "SqliteTicketStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/SqliteTicketStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$tickets$TicketSort:[I

.field static final synthetic $SwitchMap$com$squareup$tickets$TicketStore$EmployeeAccess:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 791
    invoke-static {}, Lcom/squareup/tickets/TicketSort;->values()[Lcom/squareup/tickets/TicketSort;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore$1;->$SwitchMap$com$squareup$tickets$TicketSort:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/tickets/SqliteTicketStore$1;->$SwitchMap$com$squareup$tickets$TicketSort:[I

    sget-object v2, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    invoke-virtual {v2}, Lcom/squareup/tickets/TicketSort;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/tickets/SqliteTicketStore$1;->$SwitchMap$com$squareup$tickets$TicketSort:[I

    sget-object v3, Lcom/squareup/tickets/TicketSort;->RECENT:Lcom/squareup/tickets/TicketSort;

    invoke-virtual {v3}, Lcom/squareup/tickets/TicketSort;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/tickets/SqliteTicketStore$1;->$SwitchMap$com$squareup$tickets$TicketSort:[I

    sget-object v4, Lcom/squareup/tickets/TicketSort;->OLDEST:Lcom/squareup/tickets/TicketSort;

    invoke-virtual {v4}, Lcom/squareup/tickets/TicketSort;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v3, Lcom/squareup/tickets/SqliteTicketStore$1;->$SwitchMap$com$squareup$tickets$TicketSort:[I

    sget-object v4, Lcom/squareup/tickets/TicketSort;->AMOUNT:Lcom/squareup/tickets/TicketSort;

    invoke-virtual {v4}, Lcom/squareup/tickets/TicketSort;->ordinal()I

    move-result v4

    const/4 v5, 0x4

    aput v5, v3, v4
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v3, Lcom/squareup/tickets/SqliteTicketStore$1;->$SwitchMap$com$squareup$tickets$TicketSort:[I

    sget-object v4, Lcom/squareup/tickets/TicketSort;->EMPLOYEE_NAME:Lcom/squareup/tickets/TicketSort;

    invoke-virtual {v4}, Lcom/squareup/tickets/TicketSort;->ordinal()I

    move-result v4

    const/4 v5, 0x5

    aput v5, v3, v4
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v3, Lcom/squareup/tickets/SqliteTicketStore$1;->$SwitchMap$com$squareup$tickets$TicketSort:[I

    sget-object v4, Lcom/squareup/tickets/TicketSort;->UNORDERED:Lcom/squareup/tickets/TicketSort;

    invoke-virtual {v4}, Lcom/squareup/tickets/TicketSort;->ordinal()I

    move-result v4

    const/4 v5, 0x6

    aput v5, v3, v4
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    .line 723
    :catch_5
    invoke-static {}, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->values()[Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/squareup/tickets/SqliteTicketStore$1;->$SwitchMap$com$squareup$tickets$TicketStore$EmployeeAccess:[I

    :try_start_6
    sget-object v3, Lcom/squareup/tickets/SqliteTicketStore$1;->$SwitchMap$com$squareup$tickets$TicketStore$EmployeeAccess:[I

    sget-object v4, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->IGNORE_EMPLOYEES:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    invoke-virtual {v4}, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore$1;->$SwitchMap$com$squareup$tickets$TicketStore$EmployeeAccess:[I

    sget-object v3, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->ALL_EMPLOYEES:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    invoke-virtual {v3}, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->ordinal()I

    move-result v3

    aput v1, v0, v3
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore$1;->$SwitchMap$com$squareup$tickets$TicketStore$EmployeeAccess:[I

    sget-object v1, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->ONE_EMPLOYEE:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    invoke-virtual {v1}, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    return-void
.end method
