.class final enum Lcom/squareup/tickets/VectorClocks$SyncAction;
.super Ljava/lang/Enum;
.source "VectorClocks.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/VectorClocks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "SyncAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/tickets/VectorClocks$SyncAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/tickets/VectorClocks$SyncAction;

.field public static final enum ACCEPT_CHANGE:Lcom/squareup/tickets/VectorClocks$SyncAction;

.field public static final enum NO_OP:Lcom/squareup/tickets/VectorClocks$SyncAction;

.field public static final enum REJECT_CHANGE_UPDATE_SERVER:Lcom/squareup/tickets/VectorClocks$SyncAction;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 34
    new-instance v0, Lcom/squareup/tickets/VectorClocks$SyncAction;

    const/4 v1, 0x0

    const-string v2, "REJECT_CHANGE_UPDATE_SERVER"

    invoke-direct {v0, v2, v1}, Lcom/squareup/tickets/VectorClocks$SyncAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/VectorClocks$SyncAction;->REJECT_CHANGE_UPDATE_SERVER:Lcom/squareup/tickets/VectorClocks$SyncAction;

    .line 46
    new-instance v0, Lcom/squareup/tickets/VectorClocks$SyncAction;

    const/4 v2, 0x1

    const-string v3, "ACCEPT_CHANGE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/tickets/VectorClocks$SyncAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/VectorClocks$SyncAction;->ACCEPT_CHANGE:Lcom/squareup/tickets/VectorClocks$SyncAction;

    .line 57
    new-instance v0, Lcom/squareup/tickets/VectorClocks$SyncAction;

    const/4 v3, 0x2

    const-string v4, "NO_OP"

    invoke-direct {v0, v4, v3}, Lcom/squareup/tickets/VectorClocks$SyncAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/VectorClocks$SyncAction;->NO_OP:Lcom/squareup/tickets/VectorClocks$SyncAction;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/tickets/VectorClocks$SyncAction;

    .line 25
    sget-object v4, Lcom/squareup/tickets/VectorClocks$SyncAction;->REJECT_CHANGE_UPDATE_SERVER:Lcom/squareup/tickets/VectorClocks$SyncAction;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/tickets/VectorClocks$SyncAction;->ACCEPT_CHANGE:Lcom/squareup/tickets/VectorClocks$SyncAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/tickets/VectorClocks$SyncAction;->NO_OP:Lcom/squareup/tickets/VectorClocks$SyncAction;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/tickets/VectorClocks$SyncAction;->$VALUES:[Lcom/squareup/tickets/VectorClocks$SyncAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/tickets/VectorClocks$SyncAction;
    .locals 1

    .line 25
    const-class v0, Lcom/squareup/tickets/VectorClocks$SyncAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/tickets/VectorClocks$SyncAction;

    return-object p0
.end method

.method public static values()[Lcom/squareup/tickets/VectorClocks$SyncAction;
    .locals 1

    .line 25
    sget-object v0, Lcom/squareup/tickets/VectorClocks$SyncAction;->$VALUES:[Lcom/squareup/tickets/VectorClocks$SyncAction;

    invoke-virtual {v0}, [Lcom/squareup/tickets/VectorClocks$SyncAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/tickets/VectorClocks$SyncAction;

    return-object v0
.end method
