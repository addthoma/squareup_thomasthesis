.class public final Lcom/squareup/tickets/RealTicketCardNameHandler;
.super Ljava/lang/Object;
.source "RealTicketCardNameHandler.kt"

# interfaces
.implements Lcom/squareup/tickets/TicketCardNameHandler;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u0019\u001a\u00020\u0013H\u0016J\u0010\u0010\u001a\u001a\u00020\u00132\u0006\u0010\u000f\u001a\u00020\rH\u0002J\u0010\u0010\u001b\u001a\u00020\u00132\u0006\u0010\u001c\u001a\u00020\u0011H\u0016J\u0010\u0010\u001d\u001a\u00020\u00132\u0006\u0010\u000f\u001a\u00020\rH\u0016J\u0010\u0010\u001e\u001a\u00020\u00132\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0008\u0010!\u001a\u00020\u0013H\u0016J\u0010\u0010\"\u001a\u00020\u00132\u0006\u0010#\u001a\u00020$H\u0002J\u0010\u0010\"\u001a\u00020\u00132\u0006\u0010#\u001a\u00020%H\u0017J\u0010\u0010&\u001a\u00020\u00132\u0006\u0010\'\u001a\u00020(H\u0016J\u0008\u0010)\u001a\u00020\u0013H\u0016J\u0008\u0010*\u001a\u00020\u0013H\u0016J@\u0010+\u001a\u00020\u001326\u0010\u000b\u001a2\u0012\u0013\u0012\u00110\r\u00a2\u0006\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0010\u0012\u0013\u0012\u00110\u0011\u00a2\u0006\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0012\u0012\u0004\u0012\u00020\u00130\u000cH\u0016JH\u0010+\u001a\u00020\u00132\u0006\u0010\'\u001a\u00020(26\u0010\u000b\u001a2\u0012\u0013\u0012\u00110\r\u00a2\u0006\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0010\u0012\u0013\u0012\u00110\u0011\u00a2\u0006\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0012\u0012\u0004\u0012\u00020\u00130\u000cH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R@\u0010\u000b\u001a4\u0012\u0013\u0012\u00110\r\u00a2\u0006\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0010\u0012\u0013\u0012\u00110\u0011\u00a2\u0006\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0012\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/tickets/RealTicketCardNameHandler;",
        "Lcom/squareup/tickets/TicketCardNameHandler;",
        "res",
        "Lcom/squareup/util/Res;",
        "toaster",
        "Lcom/squareup/hudtoaster/HudToaster;",
        "badBus",
        "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
        "openTicketsSettings",
        "Lcom/squareup/tickets/OpenTicketsSettings;",
        "(Lcom/squareup/util/Res;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/tickets/OpenTicketsSettings;)V",
        "callback",
        "Lkotlin/Function2;",
        "",
        "Lkotlin/ParameterName;",
        "name",
        "formattedName",
        "",
        "isSuccessful",
        "",
        "canDip",
        "canSwipe",
        "canSwipeOrDip",
        "cardListenerDisposable",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "clearSwipeListeners",
        "handleRetrievedName",
        "onDipFailure",
        "cardRemovedEarly",
        "onDipSuccess",
        "onDipTerminated",
        "message",
        "Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;",
        "onReinsertCard",
        "onSwipe",
        "event",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
        "registerToScope",
        "scope",
        "Lmortar/MortarScope;",
        "registerWithLifeCycle",
        "removeCallback",
        "setCallback",
        "open-tickets_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

.field private callback:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/String;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final canDip:Z

.field private final canSwipe:Z

.field private final canSwipeOrDip:Z

.field private final cardListenerDisposable:Lio/reactivex/disposables/CompositeDisposable;

.field private final res:Lcom/squareup/util/Res;

.field private final toaster:Lcom/squareup/hudtoaster/HudToaster;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/tickets/OpenTicketsSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "toaster"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badBus"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "openTicketsSettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->toaster:Lcom/squareup/hudtoaster/HudToaster;

    iput-object p3, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    .line 33
    invoke-interface {p4}, Lcom/squareup/tickets/OpenTicketsSettings;->isSwipeToCreateTicketAllowed()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->canSwipe:Z

    .line 34
    invoke-interface {p4}, Lcom/squareup/tickets/OpenTicketsSettings;->isDipTapToCreateTicketAllowed()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->canDip:Z

    .line 35
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->cardListenerDisposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 36
    iget-boolean p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->canSwipe:Z

    if-nez p1, :cond_1

    iget-boolean p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->canDip:Z

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    iput-boolean p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->canSwipeOrDip:Z

    return-void
.end method

.method public static final synthetic access$getCallback$p(Lcom/squareup/tickets/RealTicketCardNameHandler;)Lkotlin/jvm/functions/Function2;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->callback:Lkotlin/jvm/functions/Function2;

    return-object p0
.end method

.method public static final synthetic access$onSwipe(Lcom/squareup/tickets/RealTicketCardNameHandler;Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/tickets/RealTicketCardNameHandler;->onSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V

    return-void
.end method

.method public static final synthetic access$setCallback$p(Lcom/squareup/tickets/RealTicketCardNameHandler;Lkotlin/jvm/functions/Function2;)V
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->callback:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method private final handleRetrievedName(Ljava/lang/String;)V
    .locals 4

    .line 146
    iget-object v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->res:Lcom/squareup/util/Res;

    invoke-static {v0, p1}, Lcom/squareup/text/Cards;->formattedCardOwnerName(Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 147
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->toaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 149
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 150
    sget v2, Lcom/squareup/opentickets/R$string;->open_tickets_unable_to_read:I

    const/4 v3, 0x0

    .line 148
    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)Z

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->callback:Lkotlin/jvm/functions/Function2;

    if-eqz v0, :cond_1

    const-string v1, "formattedName"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_1
    return-void
.end method

.method private final onSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 4

    .line 48
    iget-boolean p1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;->swipeStraight:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 49
    sget p1, Lcom/squareup/common/strings/R$string;->swipe_failed_bad_swipe_message_swipe_straight:I

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 54
    :goto_0
    iget-boolean v1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->canSwipe:Z

    if-eqz v1, :cond_1

    .line 55
    iget-object v1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->toaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_SWIPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v3, Lcom/squareup/opentickets/R$string;->open_tickets_unable_to_read:I

    invoke-interface {v1, v2, v3, p1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)Z

    .line 56
    iget-object p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->callback:Lkotlin/jvm/functions/Function2;

    if-eqz p1, :cond_1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, ""

    invoke-interface {p1, v1, v0}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_1
    return-void
.end method


# virtual methods
.method public clearSwipeListeners()V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->cardListenerDisposable:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public onDipFailure(Z)V
    .locals 3

    .line 67
    iget-boolean v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->canDip:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 69
    iget-object p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->toaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->TRY_INSERTING_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

    check-cast v1, Lcom/squareup/hudtoaster/HudToaster$ToastBundle;

    invoke-interface {p1, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    goto :goto_0

    .line 71
    :cond_0
    iget-object p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->toaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 72
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_NOT_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 73
    sget v2, Lcom/squareup/opentickets/R$string;->open_tickets_unable_to_read:I

    .line 71
    invoke-interface {p1, v1, v2, v0}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)Z

    .line 77
    :goto_0
    iget-object p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->callback:Lkotlin/jvm/functions/Function2;

    if-eqz p1, :cond_1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, ""

    invoke-interface {p1, v1, v0}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_1
    return-void
.end method

.method public onDipSuccess(Ljava/lang/String;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-boolean v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->canDip:Z

    if-eqz v0, :cond_0

    .line 62
    invoke-direct {p0, p1}, Lcom/squareup/tickets/RealTicketCardNameHandler;->handleRetrievedName(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onDipTerminated(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 4

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    iget-boolean v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->canDip:Z

    if-eqz v0, :cond_0

    .line 84
    invoke-static {p1}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object p1

    .line 85
    iget-object v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->toaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 86
    invoke-virtual {p1}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    const-string v2, "messageResources.getGlyph()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1, v2}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->getTitle(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 87
    iget-object v3, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1, v3}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->getMessage(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 85
    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 89
    iget-object p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->callback:Lkotlin/jvm/functions/Function2;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, ""

    invoke-interface {p1, v1, v0}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_0
    return-void
.end method

.method public onReinsertCard()V
    .locals 2

    .line 94
    iget-boolean v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->canDip:Z

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->toaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->MUST_REINSERT_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    check-cast v1, Lcom/squareup/hudtoaster/HudToaster$ToastBundle;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    :cond_0
    return-void
.end method

.method public onSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-boolean v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->canSwipe:Z

    if-eqz v0, :cond_0

    .line 43
    iget-object p1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    const-string v0, "event.card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/Card;->getName()Ljava/lang/String;

    move-result-object p1

    const-string v0, "event.card.name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/tickets/RealTicketCardNameHandler;->handleRetrievedName(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public registerToScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    iget-boolean v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->canSwipeOrDip:Z

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->successfulSwipes()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/tickets/RealTicketCardNameHandler$registerToScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/tickets/RealTicketCardNameHandler$registerToScope$1;-><init>(Lcom/squareup/tickets/RealTicketCardNameHandler;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "badBus.successfulSwipes(\u2026 -> this.onSwipe(event) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->failedSwipes()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/tickets/RealTicketCardNameHandler$registerToScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/tickets/RealTicketCardNameHandler$registerToScope$2;-><init>(Lcom/squareup/tickets/RealTicketCardNameHandler;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "badBus.failedSwipes().su\u2026 -> this.onSwipe(event) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    :cond_0
    return-void
.end method

.method public registerWithLifeCycle()V
    .locals 3

    .line 111
    iget-boolean v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->canSwipeOrDip:Z

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->cardListenerDisposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 113
    iget-object v1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v1}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->successfulSwipes()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/tickets/RealTicketCardNameHandler$registerWithLifeCycle$1;

    invoke-direct {v2, p0}, Lcom/squareup/tickets/RealTicketCardNameHandler$registerWithLifeCycle$1;-><init>(Lcom/squareup/tickets/RealTicketCardNameHandler;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 112
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 115
    iget-object v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->cardListenerDisposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 116
    iget-object v1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v1}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->failedSwipes()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/tickets/RealTicketCardNameHandler$registerWithLifeCycle$2;

    invoke-direct {v2, p0}, Lcom/squareup/tickets/RealTicketCardNameHandler$registerWithLifeCycle$2;-><init>(Lcom/squareup/tickets/RealTicketCardNameHandler;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 115
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    :cond_0
    return-void
.end method

.method public removeCallback()V
    .locals 1

    const/4 v0, 0x0

    .line 142
    check-cast v0, Lkotlin/jvm/functions/Function2;

    iput-object v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->callback:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public setCallback(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/String;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "callback"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    iput-object p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->callback:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public setCallback(Lmortar/MortarScope;Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmortar/MortarScope;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/String;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    iput-object p2, p0, Lcom/squareup/tickets/RealTicketCardNameHandler;->callback:Lkotlin/jvm/functions/Function2;

    .line 130
    new-instance v0, Lcom/squareup/tickets/RealTicketCardNameHandler$setCallback$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/tickets/RealTicketCardNameHandler$setCallback$1;-><init>(Lcom/squareup/tickets/RealTicketCardNameHandler;Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/squareup/mortar/MortarScopes;->onExit(Ljava/lang/Runnable;)Lmortar/Scoped;

    move-result-object p2

    invoke-virtual {p1, p2}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
