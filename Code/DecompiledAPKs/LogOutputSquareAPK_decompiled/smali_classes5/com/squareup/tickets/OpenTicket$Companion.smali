.class public final Lcom/squareup/tickets/OpenTicket$Companion;
.super Ljava/lang/Object;
.source "OpenTicket.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/OpenTicket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0007J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0007J@\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\n\u001a\u00020\u000bH\u0007J\u001a\u0010\u0017\u001a\u0004\u0018\u00010\u00072\u0006\u0010\u0018\u001a\u00020\u000f2\u0006\u0010\u0019\u001a\u00020\u001aH\u0007R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/tickets/OpenTicket$Companion;",
        "",
        "()V",
        "EMPTY_ITEMIZATIONS_LINE_ITEMS",
        "Lcom/squareup/protos/client/bills/Cart$LineItems;",
        "kotlin.jvm.PlatformType",
        "createTicket",
        "Lcom/squareup/tickets/OpenTicket;",
        "ticketProto",
        "Lcom/squareup/protos/client/tickets/Ticket;",
        "currentTime",
        "Ljava/util/Date;",
        "cart",
        "Lcom/squareup/protos/client/bills/Cart;",
        "name",
        "",
        "note",
        "ticketGroup",
        "Lcom/squareup/api/items/TicketGroup;",
        "ticketTemplate",
        "Lcom/squareup/api/items/TicketTemplate;",
        "employeeInfo",
        "Lcom/squareup/permissions/EmployeeInfo;",
        "loadFromBundle",
        "key",
        "bundle",
        "Landroid/os/Bundle;",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 922
    invoke-direct {p0}, Lcom/squareup/tickets/OpenTicket$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final createTicket(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/tickets/OpenTicket;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string/jumbo v0, "ticketProto"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1005
    new-instance v0, Lcom/squareup/tickets/OpenTicket;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/tickets/OpenTicket;-><init>(Lcom/squareup/protos/client/tickets/Ticket;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final createTicket(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Lcom/squareup/permissions/EmployeeInfo;Ljava/util/Date;)Lcom/squareup/tickets/OpenTicket;
    .locals 5
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "employeeInfo"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 981
    new-instance v0, Lcom/squareup/tickets/OpenTicket;

    .line 982
    new-instance v1, Lcom/squareup/protos/client/tickets/Ticket$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;-><init>()V

    .line 983
    new-instance v2, Lcom/squareup/protos/client/ISO8601Date;

    invoke-static {p6}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/protos/client/ISO8601Date;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->client_updated_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v1

    .line 985
    new-instance v2, Lcom/squareup/protos/client/IdPair;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v4, v3}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 984
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->ticket_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v1

    .line 987
    new-instance v2, Lcom/squareup/protos/client/tickets/VectorClock;

    invoke-static {}, Lcom/squareup/tickets/VectorClocks;->buildClientClock()Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    move-result-object v3

    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/protos/client/tickets/VectorClock;-><init>(Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->vector_clock(Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v1

    .line 989
    new-instance v2, Lcom/squareup/protos/client/bills/Cart$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/Cart$Builder;-><init>()V

    .line 991
    invoke-static {}, Lcom/squareup/tickets/OpenTicket;->access$getEMPTY_ITEMIZATIONS_LINE_ITEMS$cp()Lcom/squareup/protos/client/bills/Cart$LineItems;

    move-result-object v3

    .line 990
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Cart$Builder;->line_items(Lcom/squareup/protos/client/bills/Cart$LineItems;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v2

    .line 993
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Cart$Builder;->build()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v2

    .line 988
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v1

    .line 995
    invoke-virtual {v1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object v1

    const-string v2, "Ticket.Builder()\n       \u2026 )\n              .build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 981
    invoke-direct {v0, v1, v4}, Lcom/squareup/tickets/OpenTicket;-><init>(Lcom/squareup/protos/client/tickets/Ticket;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 997
    invoke-virtual {v0, p1, p2}, Lcom/squareup/tickets/OpenTicket;->setNameAndNote(Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    invoke-virtual {v0, p3, p4}, Lcom/squareup/tickets/OpenTicket;->setTicketGroupAndTemplate(Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V

    .line 999
    invoke-virtual {v0, p5}, Lcom/squareup/tickets/OpenTicket;->setEmployeeInfo(Lcom/squareup/permissions/EmployeeInfo;)V

    .line 1000
    invoke-static {v0, p6}, Lcom/squareup/tickets/OpenTicket;->access$setCreatedAt(Lcom/squareup/tickets/OpenTicket;Ljava/util/Date;)V

    return-object v0
.end method

.method public final createTicket(Ljava/util/Date;Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/tickets/OpenTicket;
    .locals 7
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "currentTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cart"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 944
    iget-object v0, p2, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 948
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "UUID.randomUUID().toString()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 949
    :goto_1
    iget-object v2, p2, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    if-eqz v2, :cond_2

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    goto :goto_2

    :cond_2
    move-object v2, v1

    .line 951
    :goto_2
    new-instance v3, Lcom/squareup/tickets/OpenTicket;

    .line 952
    new-instance v4, Lcom/squareup/protos/client/tickets/Ticket$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/tickets/Ticket$Builder;-><init>()V

    .line 953
    new-instance v5, Lcom/squareup/protos/client/ISO8601Date;

    invoke-static {p1}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/squareup/protos/client/ISO8601Date;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->client_updated_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v4

    .line 954
    new-instance v5, Lcom/squareup/protos/client/IdPair;

    invoke-direct {v5, v1, v0}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->ticket_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 955
    new-instance v4, Lcom/squareup/protos/client/tickets/VectorClock;

    invoke-static {}, Lcom/squareup/tickets/VectorClocks;->buildClientClock()Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    move-result-object v5

    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/squareup/protos/client/tickets/VectorClock;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->vector_clock(Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 956
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 957
    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 958
    invoke-virtual {v0}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object v0

    const-string v2, "Ticket.Builder()\n       \u2026y)\n              .build()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 951
    invoke-direct {v3, v0, v1}, Lcom/squareup/tickets/OpenTicket;-><init>(Lcom/squareup/protos/client/tickets/Ticket;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 961
    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz p2, :cond_3

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    if-eqz p2, :cond_3

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    :cond_3
    if-nez v1, :cond_4

    .line 962
    invoke-static {v3, p1}, Lcom/squareup/tickets/OpenTicket;->access$setCreatedAt(Lcom/squareup/tickets/OpenTicket;Ljava/util/Date;)V

    :cond_4
    return-object v3
.end method

.method public final loadFromBundle(Ljava/lang/String;Landroid/os/Bundle;)Lcom/squareup/tickets/OpenTicket;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bundle"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 932
    invoke-virtual {p2, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 933
    new-instance v0, Lcom/squareup/tickets/OpenTicket;

    .line 934
    invoke-virtual {p2, p1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/tickets/TicketWrapper;->protoFromByteArray([B)Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    const-string p2, "protoFromByteArray(bundle.getByteArray(key))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 933
    invoke-direct {v0, p1, v1}, Lcom/squareup/tickets/OpenTicket;-><init>(Lcom/squareup/protos/client/tickets/Ticket;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0
.end method
