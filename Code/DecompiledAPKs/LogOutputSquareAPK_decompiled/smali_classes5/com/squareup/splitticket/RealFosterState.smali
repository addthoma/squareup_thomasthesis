.class public final Lcom/squareup/splitticket/RealFosterState;
.super Ljava/lang/Object;
.source "RealFosterState.kt"

# interfaces
.implements Lcom/squareup/splitticket/FosterState;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealFosterState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealFosterState.kt\ncom/squareup/splitticket/RealFosterState\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,15:1\n704#2:16\n777#2,2:17\n*E\n*S KotlinDebug\n*F\n+ 1 RealFosterState.kt\ncom/squareup/splitticket/RealFosterState\n*L\n13#1:16\n13#1,2:17\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u001a\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/splitticket/RealFosterState;",
        "Lcom/squareup/splitticket/FosterState;",
        "order",
        "Lcom/squareup/payment/Order;",
        "(Lcom/squareup/payment/Order;)V",
        "cartAmountDiscounts",
        "",
        "Lcom/squareup/checkout/Discount;",
        "getCartAmountDiscounts",
        "()Ljava/util/List;",
        "getOrder",
        "()Lcom/squareup/payment/Order;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final order:Lcom/squareup/payment/Order;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Order;)V
    .locals 1

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/splitticket/RealFosterState;->order:Lcom/squareup/payment/Order;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/splitticket/RealFosterState;Lcom/squareup/payment/Order;ILjava/lang/Object;)Lcom/squareup/splitticket/RealFosterState;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/squareup/splitticket/RealFosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/splitticket/RealFosterState;->copy(Lcom/squareup/payment/Order;)Lcom/squareup/splitticket/RealFosterState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/payment/Order;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/splitticket/RealFosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Lcom/squareup/payment/Order;)Lcom/squareup/splitticket/RealFosterState;
    .locals 1

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/splitticket/RealFosterState;

    invoke-direct {v0, p1}, Lcom/squareup/splitticket/RealFosterState;-><init>(Lcom/squareup/payment/Order;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/splitticket/RealFosterState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/splitticket/RealFosterState;

    invoke-virtual {p0}, Lcom/squareup/splitticket/RealFosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/splitticket/RealFosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getCartAmountDiscounts()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 11
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealFosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAllAppliedDiscountsInDisplayOrder()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 16
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 17
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/checkout/Discount;

    .line 13
    invoke-virtual {v3}, Lcom/squareup/checkout/Discount;->isCartScopeAmountDiscount()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 18
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public getOrder()Lcom/squareup/payment/Order;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/splitticket/RealFosterState;->order:Lcom/squareup/payment/Order;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/splitticket/RealFosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RealFosterState(order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/splitticket/RealFosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
