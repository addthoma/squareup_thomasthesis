.class public final Lcom/squareup/splitticket/analytics/SplitTicketNewActionEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "SplitTicketNewActionEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0017\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0006\u0010\u0007\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/splitticket/analytics/SplitTicketNewActionEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "containsUnitPricedItem",
        "",
        "(Z)V",
        "contains_unit_priced_item",
        "contains_unit_priced_item$annotations",
        "()V",
        "getContains_unit_priced_item",
        "()Z",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final contains_unit_priced_item:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 2

    .line 11
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "Split Ticket: Split New"

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 13
    iput-boolean p1, p0, Lcom/squareup/splitticket/analytics/SplitTicketNewActionEvent;->contains_unit_priced_item:Z

    return-void
.end method

.method public static synthetic contains_unit_priced_item$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final getContains_unit_priced_item()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/squareup/splitticket/analytics/SplitTicketNewActionEvent;->contains_unit_priced_item:Z

    return v0
.end method
