.class public abstract Lcom/squareup/splitticket/TicketSplitterModule;
.super Ljava/lang/Object;
.source "TicketSplitterModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u00002\u00020\u0001:\u0001\u0013B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH!J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH!J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H!\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/splitticket/TicketSplitterModule;",
        "",
        "()V",
        "bindOrderPrintingDispatcherWrapper",
        "Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;",
        "orderPrintingDispatcherWrapper",
        "Lcom/squareup/splitticket/RealOrderPrintingDispatcherWrapper;",
        "bindPricingEngineServiceWrapper",
        "Lcom/squareup/splitticket/PricingEngineServiceWrapper;",
        "pricingEngineServiceWrapper",
        "Lcom/squareup/splitticket/RealPricingEngineServiceWrapper;",
        "bindSplitTransactionHandler",
        "Lcom/squareup/splitticket/SplitTransactionHandler;",
        "splitTransactionHandler",
        "Lcom/squareup/splitticket/RealSplitTransactionHandler;",
        "bindTicketSplitter",
        "Lcom/squareup/splitticket/TicketSplitter;",
        "ticketSplitter",
        "Lcom/squareup/splitticket/RealTicketSplitter;",
        "TicketSplitterBudleKeyModule",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindOrderPrintingDispatcherWrapper(Lcom/squareup/splitticket/RealOrderPrintingDispatcherWrapper;)Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindPricingEngineServiceWrapper(Lcom/squareup/splitticket/RealPricingEngineServiceWrapper;)Lcom/squareup/splitticket/PricingEngineServiceWrapper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindSplitTransactionHandler(Lcom/squareup/splitticket/RealSplitTransactionHandler;)Lcom/squareup/splitticket/SplitTransactionHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindTicketSplitter(Lcom/squareup/splitticket/RealTicketSplitter;)Lcom/squareup/splitticket/TicketSplitter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
