.class public final Lcom/squareup/splitticket/RealTicketSplitterKt;
.super Ljava/lang/Object;
.source "RealTicketSplitter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\u001a4\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u00042\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u0000\u00a8\u0006\t"
    }
    d2 = {
        "swizzleAllItemIds",
        "",
        "oldIdsToNewIds",
        "",
        "",
        "ticketIdToSkip",
        "statesToIterate",
        "",
        "Lcom/squareup/splitticket/RealSplitState;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final swizzleAllItemIds(Ljava/util/Map;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/Collection<",
            "Lcom/squareup/splitticket/RealSplitState;",
            ">;)V"
        }
    .end annotation

    const-string v0, "oldIdsToNewIds"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "ticketIdToSkip"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "statesToIterate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 660
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/splitticket/RealSplitState;

    .line 661
    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->getTicketId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 664
    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->getItems()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_2

    .line 665
    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->getItems()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 666
    iget-object v4, v3, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v4, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {p0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 669
    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->getOrder$impl_release()Lcom/squareup/payment/Order;

    move-result-object v5

    .line 671
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v3

    .line 672
    new-instance v6, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v6}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    invoke-virtual {v6, v4}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/checkout/CartItem$Builder;->idPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v3

    .line 673
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v3

    .line 669
    invoke-virtual {v5, v1, v3}, Lcom/squareup/payment/Order;->replaceItem(ILcom/squareup/checkout/CartItem;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 682
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->getOrder$impl_release()Lcom/squareup/payment/Order;

    move-result-object v0

    new-instance v1, Lcom/squareup/splitticket/RealTicketSplitterKt$swizzleAllItemIds$1;

    invoke-direct {v1, p0}, Lcom/squareup/splitticket/RealTicketSplitterKt$swizzleAllItemIds$1;-><init>(Ljava/util/Map;)V

    check-cast v1, Lcom/squareup/checkout/CartItem$Transform;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order;->replaceDeletedItems(Lcom/squareup/checkout/CartItem$Transform;)V

    goto :goto_0

    :cond_3
    return-void
.end method
