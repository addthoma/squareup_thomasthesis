.class public final Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$Factory;
.super Ljava/lang/Object;
.source "SecureTouchModeSelectionCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0019\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J*\u0010\u0007\u001a\u00020\u00082\"\u0010\t\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000bj\u0008\u0012\u0004\u0012\u00020\u000c`\u000e0\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$Factory;",
        "",
        "accessibilitySettings",
        "Lcom/squareup/accessibility/AccessibilitySettings;",
        "mainThread",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/accessibility/AccessibilitySettings;Lio/reactivex/Scheduler;)V",
        "create",
        "Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

.field private final mainThread:Lio/reactivex/Scheduler;


# direct methods
.method public constructor <init>(Lcom/squareup/accessibility/AccessibilitySettings;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accessibilitySettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$Factory;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    iput-object p2, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$Factory;->mainThread:Lio/reactivex/Scheduler;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$Factory;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    iget-object v2, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$Factory;->mainThread:Lio/reactivex/Scheduler;

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/accessibility/AccessibilitySettings;Lio/reactivex/Scheduler;)V

    return-object v0
.end method
