.class public final Lcom/squareup/securetouch/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final keypad_screen_button_internal_margin:I = 0x7f0701dd

.field public static final keypad_screen_gutter:I = 0x7f0701de

.field public static final keypad_screen_keypad_gutter_horizontal:I = 0x7f0701df

.field public static final keypad_screen_star_gap:I = 0x7f0701e0

.field public static final keypad_screen_star_radius:I = 0x7f0701e1

.field public static final keypad_screen_title_size:I = 0x7f0701e2

.field public static final noho_squid_keypad_button_text_size:I = 0x7f0703aa

.field public static final noho_squid_keypad_cancel_button_icon_height:I = 0x7f0703ab

.field public static final noho_squid_keypad_cancel_button_icon_width:I = 0x7f0703ac

.field public static final noho_squid_keypad_delete_icon_height:I = 0x7f0703ad

.field public static final noho_squid_keypad_delete_icon_width:I = 0x7f0703ae

.field public static final noho_squid_keypad_digit_text_bottom_margin:I = 0x7f0703af

.field public static final noho_squid_keypad_divider_width:I = 0x7f0703b0

.field public static final noho_squid_keypad_done_button_icon_height:I = 0x7f0703b1

.field public static final noho_squid_keypad_done_button_icon_width:I = 0x7f0703b2

.field public static final noho_squid_keypad_margin_bottom:I = 0x7f0703b3

.field public static final noho_squid_keypad_margin_top:I = 0x7f0703b4

.field public static final noho_squid_keypad_padding_horizontal:I = 0x7f0703b5

.field public static final noho_squid_keypad_subtext_bottom_margin:I = 0x7f0703b6

.field public static final noho_squid_keypad_subtext_size:I = 0x7f0703b7

.field public static final noho_squid_keypad_text_size:I = 0x7f0703b8

.field public static final noho_squid_keypad_title_margin_top:I = 0x7f0703b9

.field public static final secure_touch_mode_selection_screen_button_border_width:I = 0x7f0704ab

.field public static final secure_touch_mode_selection_screen_button_gap:I = 0x7f0704ac

.field public static final secure_touch_mode_selection_screen_button_height:I = 0x7f0704ad

.field public static final secure_touch_mode_selection_screen_display_amount_text_size:I = 0x7f0704ae

.field public static final secure_touch_mode_selection_screen_padding:I = 0x7f0704af

.field public static final secure_touch_mode_selection_screen_text_size:I = 0x7f0704b0

.field public static final secure_touch_mode_selection_screen_title_padding:I = 0x7f0704b1


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
