.class public final Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;
.super Ljava/lang/Object;
.source "SecureTouchAccessibilityPinEntryAudioPlayer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B!\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0006\u0010\u000b\u001a\u00020\u000cJ\u000e\u0010\r\u001a\u00020\u000c2\u0006\u0010\u000e\u001a\u00020\u000fJ\u0012\u0010\u0010\u001a\u00020\u000c2\u0008\u0008\u0001\u0010\u0011\u001a\u00020\u0012H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;",
        "",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "audioExecutor",
        "Lcom/squareup/thread/executor/SerialExecutor;",
        "application",
        "Landroid/app/Application;",
        "(Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/thread/executor/SerialExecutor;Landroid/app/Application;)V",
        "player",
        "Landroid/media/MediaPlayer;",
        "destroy",
        "",
        "maybePlayAudio",
        "audio",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio;",
        "playAudio",
        "resId",
        "",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$Companion;

.field private static final SECURE_TOUCH_ACCESSIBILITY_MODE_ERROR_PATH:I

.field private static final SECURE_TOUCH_ACCESSIBILITY_MODE_RECOGNIZED_ENTRY_PATH:I

.field private static final SECURE_TOUCH_ACCESSIBILITY_MODE_SWIPE_PATH:I

.field private static final SECURE_TOUCH_ACCESSIBILITY_MODE_TOUCH_DOWN_PATH:I


# instance fields
.field private final application:Landroid/app/Application;

.field private final audioExecutor:Lcom/squareup/thread/executor/SerialExecutor;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final player:Landroid/media/MediaPlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->Companion:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$Companion;

    .line 76
    sget v0, Lcom/squareup/securetouch/accessibility/impl/R$raw;->error:I

    sput v0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->SECURE_TOUCH_ACCESSIBILITY_MODE_ERROR_PATH:I

    .line 77
    sget v0, Lcom/squareup/securetouch/accessibility/impl/R$raw;->recognized_entry:I

    sput v0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->SECURE_TOUCH_ACCESSIBILITY_MODE_RECOGNIZED_ENTRY_PATH:I

    .line 78
    sget v0, Lcom/squareup/securetouch/accessibility/impl/R$raw;->swipe:I

    sput v0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->SECURE_TOUCH_ACCESSIBILITY_MODE_SWIPE_PATH:I

    .line 79
    sget v0, Lcom/squareup/securetouch/accessibility/impl/R$raw;->touch_down:I

    sput v0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->SECURE_TOUCH_ACCESSIBILITY_MODE_TOUCH_DOWN_PATH:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/thread/executor/SerialExecutor;Landroid/app/Application;)V
    .locals 1
    .param p2    # Lcom/squareup/thread/executor/SerialExecutor;
        .annotation runtime Lcom/squareup/util/AudioThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "buyerLocaleOverride"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audioExecutor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "application"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p2, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->audioExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    iput-object p3, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->application:Landroid/app/Application;

    .line 33
    new-instance p1, Landroid/media/MediaPlayer;

    invoke-direct {p1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->player:Landroid/media/MediaPlayer;

    return-void
.end method

.method public static final synthetic access$getApplication$p(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;)Landroid/app/Application;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->application:Landroid/app/Application;

    return-object p0
.end method

.method public static final synthetic access$getPlayer$p(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;)Landroid/media/MediaPlayer;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->player:Landroid/media/MediaPlayer;

    return-object p0
.end method

.method private final playAudio(I)V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->audioExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    new-instance v1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$playAudio$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$playAudio$1;-><init>(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;I)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/SerialExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public final destroy()V
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->audioExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    new-instance v1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$destroy$1;

    invoke-direct {v1, p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$destroy$1;-><init>(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/SerialExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final maybePlayAudio(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio;)V
    .locals 2

    const-string v0, "audio"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    instance-of v0, p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$ErrorAudio;

    if-eqz v0, :cond_0

    sget p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->SECURE_TOUCH_ACCESSIBILITY_MODE_ERROR_PATH:I

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->playAudio(I)V

    goto :goto_0

    .line 37
    :cond_0
    instance-of v0, p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$RecognizedEntryAudio;

    if-eqz v0, :cond_1

    sget p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->SECURE_TOUCH_ACCESSIBILITY_MODE_RECOGNIZED_ENTRY_PATH:I

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->playAudio(I)V

    goto :goto_0

    .line 38
    :cond_1
    instance-of v0, p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$SwipeAudio;

    if-eqz v0, :cond_2

    sget p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->SECURE_TOUCH_ACCESSIBILITY_MODE_SWIPE_PATH:I

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->playAudio(I)V

    goto :goto_0

    .line 39
    :cond_2
    instance-of v0, p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$TouchDownAudio;

    if-eqz v0, :cond_3

    sget p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->SECURE_TOUCH_ACCESSIBILITY_MODE_TOUCH_DOWN_PATH:I

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->playAudio(I)V

    goto :goto_0

    .line 40
    :cond_3
    instance-of v0, p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$TooManyDigitsAudio;

    if-eqz v0, :cond_4

    .line 41
    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->application:Landroid/app/Application;

    check-cast p1, Landroid/content/Context;

    .line 42
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v0

    sget v1, Lcom/squareup/securetouch/accessibility/impl/R$string;->accessible_keypad_error_too_many_digits:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 40
    invoke-static {p1, v0}, Lcom/squareup/accessibility/Accessibility;->submitAccessibilityAnnouncementEvent(Landroid/content/Context;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 44
    :cond_4
    instance-of v0, p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$TooFewDigitsAudio;

    if-eqz v0, :cond_5

    .line 45
    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->application:Landroid/app/Application;

    check-cast p1, Landroid/content/Context;

    .line 46
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v0

    sget v1, Lcom/squareup/securetouch/accessibility/impl/R$string;->accessible_keypad_error_too_few_digits:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 44
    invoke-static {p1, v0}, Lcom/squareup/accessibility/Accessibility;->submitAccessibilityAnnouncementEvent(Landroid/content/Context;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 48
    :cond_5
    instance-of p1, p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$NoAudioRequired;

    if-eqz p1, :cond_6

    :goto_0
    return-void

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
