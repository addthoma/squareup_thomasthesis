.class public final Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;
.super Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;
.source "SecureTouchAccessibilityPinEntryState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EnablingKeypad"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0017\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
        "digitsEntered",
        "",
        "pinEntryState",
        "Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "(ILcom/squareup/securetouch/SecureTouchPinEntryState;)V",
        "getDigitsEntered",
        "()I",
        "getPinEntryState",
        "()Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final digitsEntered:I

.field private final pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;


# direct methods
.method public constructor <init>(ILcom/squareup/securetouch/SecureTouchPinEntryState;)V
    .locals 1

    const-string v0, "pinEntryState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 14
    invoke-direct {p0, v0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->digitsEntered:I

    iput-object p2, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/squareup/securetouch/SecureTouchPinEntryState;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const/4 p1, 0x0

    .line 12
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;-><init>(ILcom/squareup/securetouch/SecureTouchPinEntryState;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;ILcom/squareup/securetouch/SecureTouchPinEntryState;ILjava/lang/Object;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->getDigitsEntered()I

    move-result p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object p2

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->copy(ILcom/squareup/securetouch/SecureTouchPinEntryState;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->getDigitsEntered()I

    move-result v0

    return v0
.end method

.method public final component2()Lcom/squareup/securetouch/SecureTouchPinEntryState;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v0

    return-object v0
.end method

.method public final copy(ILcom/squareup/securetouch/SecureTouchPinEntryState;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;
    .locals 1

    const-string v0, "pinEntryState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;

    invoke-direct {v0, p1, p2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;-><init>(ILcom/squareup/securetouch/SecureTouchPinEntryState;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->getDigitsEntered()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->getDigitsEntered()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getDigitsEntered()I
    .locals 1

    .line 12
    iget v0, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->digitsEntered:I

    return v0
.end method

.method public getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->getDigitsEntered()I

    move-result v0

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EnablingKeypad(digitsEntered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->getDigitsEntered()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", pinEntryState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
