.class public final Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;
.super Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action;
.source "RealSecureTouchAccessibilityPinEntryWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReleaseEventDevOnly"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\u0018\u0010\u0011\u001a\u00020\u0012*\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00150\u0013H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;",
        "Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action;",
        "secureTouchPoint",
        "Lcom/squareup/securetouch/SecureTouchPoint;",
        "(Lcom/squareup/securetouch/SecureTouchPoint;)V",
        "getSecureTouchPoint",
        "()Lcom/squareup/securetouch/SecureTouchPoint;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final secureTouchPoint:Lcom/squareup/securetouch/SecureTouchPoint;


# direct methods
.method public constructor <init>(Lcom/squareup/securetouch/SecureTouchPoint;)V
    .locals 1

    const-string v0, "secureTouchPoint"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 320
    invoke-direct {p0, v0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;->secureTouchPoint:Lcom/squareup/securetouch/SecureTouchPoint;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;Lcom/squareup/securetouch/SecureTouchPoint;ILjava/lang/Object;)Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;->secureTouchPoint:Lcom/squareup/securetouch/SecureTouchPoint;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;->copy(Lcom/squareup/securetouch/SecureTouchPoint;)Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 322
    new-instance v0, Lcom/squareup/securetouch/MotionReleaseEventDevOnly;

    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;->secureTouchPoint:Lcom/squareup/securetouch/SecureTouchPoint;

    invoke-direct {v0, v1}, Lcom/squareup/securetouch/MotionReleaseEventDevOnly;-><init>(Lcom/squareup/securetouch/SecureTouchPoint;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Lcom/squareup/securetouch/SecureTouchPoint;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;->secureTouchPoint:Lcom/squareup/securetouch/SecureTouchPoint;

    return-object v0
.end method

.method public final copy(Lcom/squareup/securetouch/SecureTouchPoint;)Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;
    .locals 1

    const-string v0, "secureTouchPoint"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;

    invoke-direct {v0, p1}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;-><init>(Lcom/squareup/securetouch/SecureTouchPoint;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;->secureTouchPoint:Lcom/squareup/securetouch/SecureTouchPoint;

    iget-object p1, p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;->secureTouchPoint:Lcom/squareup/securetouch/SecureTouchPoint;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getSecureTouchPoint()Lcom/squareup/securetouch/SecureTouchPoint;
    .locals 1

    .line 319
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;->secureTouchPoint:Lcom/squareup/securetouch/SecureTouchPoint;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;->secureTouchPoint:Lcom/squareup/securetouch/SecureTouchPoint;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReleaseEventDevOnly(secureTouchPoint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ReleaseEventDevOnly;->secureTouchPoint:Lcom/squareup/securetouch/SecureTouchPoint;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
