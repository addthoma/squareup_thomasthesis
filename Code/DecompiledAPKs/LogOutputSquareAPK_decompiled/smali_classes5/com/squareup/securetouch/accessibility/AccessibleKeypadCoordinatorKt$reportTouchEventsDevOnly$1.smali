.class final Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt$reportTouchEventsDevOnly$1;
.super Ljava/lang/Object;
.source "AccessibleKeypadCoordinator.kt"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;->reportTouchEventsDevOnly(Landroid/view/View;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0005\u001a\n \u0004*\u0004\u0018\u00010\u00060\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "event",
        "Landroid/view/MotionEvent;",
        "onTouch"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt$reportTouchEventsDevOnly$1;->$screen:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    const-string p1, "event"

    .line 295
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 297
    :cond_0
    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt$reportTouchEventsDevOnly$1;->$screen:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->getMotionReleaseEventDevOnly()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    new-instance v1, Lcom/squareup/securetouch/MotionReleaseEventDevOnly;

    invoke-static {p2}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;->access$rawIntPoint(Landroid/view/MotionEvent;)Lcom/squareup/securetouch/SecureTouchPoint;

    move-result-object p2

    invoke-direct {v1, p2}, Lcom/squareup/securetouch/MotionReleaseEventDevOnly;-><init>(Lcom/squareup/securetouch/SecureTouchPoint;)V

    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 296
    :cond_1
    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt$reportTouchEventsDevOnly$1;->$screen:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->getMotionTouchEventDevOnly()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    new-instance v1, Lcom/squareup/securetouch/MotionTouchEventDevOnly;

    invoke-static {p2}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;->access$rawIntPoint(Landroid/view/MotionEvent;)Lcom/squareup/securetouch/SecureTouchPoint;

    move-result-object p2

    invoke-direct {v1, p2}, Lcom/squareup/securetouch/MotionTouchEventDevOnly;-><init>(Lcom/squareup/securetouch/SecureTouchPoint;)V

    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return v0
.end method
