.class public final Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AccessibleKeypadCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAccessibleKeypadCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AccessibleKeypadCoordinator.kt\ncom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator\n*L\n1#1,318:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0096\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001?BK\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0018\u0010(\u001a\u00020%2\u0006\u0010)\u001a\u00020\u00052\u0006\u0010\u0008\u001a\u00020\tH\u0002J\u0010\u0010*\u001a\u00020%2\u0006\u0010+\u001a\u00020\u0012H\u0016J\u0010\u0010,\u001a\u00020%2\u0006\u0010+\u001a\u00020\u0012H\u0002J\u0010\u0010-\u001a\u00020%2\u0006\u0010.\u001a\u00020\u0016H\u0002J\u0010\u0010/\u001a\u00020%2\u0006\u0010.\u001a\u00020\u0016H\u0002J\u0010\u00100\u001a\u00020%2\u0006\u0010)\u001a\u00020\u0005H\u0002J\u0008\u00101\u001a\u00020%H\u0002J\u0010\u00102\u001a\u00020%2\u0006\u0010)\u001a\u00020\u0005H\u0002J\u0010\u00103\u001a\u0002042\u0006\u0010)\u001a\u00020\u0005H\u0002J\u0018\u00105\u001a\u00020%2\u0006\u0010)\u001a\u00020\u00052\u0006\u00106\u001a\u000207H\u0002J\u000c\u00108\u001a\u00020%*\u00020\u0012H\u0002J\u001c\u00109\u001a\u00020%*\u00020\u00122\u0006\u0010:\u001a\u00020;2\u0006\u0010\u0008\u001a\u00020\tH\u0002J\u0016\u0010<\u001a\u00020;*\u00020\u00052\u0008\u0008\u0002\u0010=\u001a\u00020>H\u0003R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0015\u001a\u00020\u00168BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0019\u0010\u001a\u001a\u0004\u0008\u0017\u0010\u0018R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010#\u001a\u0008\u0012\u0004\u0012\u00020%0$X\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\'X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006@"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "mainThread",
        "Lio/reactivex/Scheduler;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "accessibilitySettings",
        "Lcom/squareup/accessibility/AccessibilitySettings;",
        "(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lio/reactivex/Scheduler;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/accessibility/AccessibilitySettings;)V",
        "accessibilityKeypadArea",
        "Landroid/view/View;",
        "cancelButton",
        "Landroid/widget/ImageView;",
        "centerOfScreen",
        "Lcom/squareup/securetouch/SecureTouchPoint;",
        "getCenterOfScreen",
        "()Lcom/squareup/securetouch/SecureTouchPoint;",
        "centerOfScreen$delegate",
        "Lkotlin/Lazy;",
        "digitZero",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "digitsOneThroughNine",
        "keypadTitle",
        "Lcom/squareup/marketfont/MarketTextView;",
        "resources",
        "Landroid/content/res/Resources;",
        "rootLayout",
        "rootLayoutChange",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "starGroup",
        "Lcom/squareup/ui/StarGroup;",
        "announceInstructions",
        "screen",
        "attach",
        "view",
        "bindViews",
        "displayDigitZero",
        "center",
        "displayDigitsOneThroughNine",
        "displayKeypad",
        "hideAllDigits",
        "sendLayout",
        "spinnerState",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "update",
        "buyerRes",
        "Lcom/squareup/util/Res;",
        "inflateInNightMode",
        "maybeAnnounceDigitsEntered",
        "digitsEntered",
        "",
        "toTitleResource",
        "forAudioInstructions",
        "",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private accessibilityKeypadArea:Landroid/view/View;

.field private final accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private cancelButton:Landroid/widget/ImageView;

.field private final centerOfScreen$delegate:Lkotlin/Lazy;

.field private digitZero:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private digitsOneThroughNine:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private keypadTitle:Lcom/squareup/marketfont/MarketTextView;

.field private final mainThread:Lio/reactivex/Scheduler;

.field private resources:Landroid/content/res/Resources;

.field private rootLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private final rootLayoutChange:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private starGroup:Lcom/squareup/ui/StarGroup;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lio/reactivex/Scheduler;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/accessibility/AccessibilitySettings;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            "Lcom/squareup/accessibility/AccessibilitySettings;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLocaleOverride"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassSpinner"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accessibilitySettings"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p3, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->mainThread:Lio/reactivex/Scheduler;

    iput-object p4, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iput-object p5, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    .line 68
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->rootLayoutChange:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 267
    new-instance p1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$centerOfScreen$2;

    invoke-direct {p1, p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$centerOfScreen$2;-><init>(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->centerOfScreen$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic access$announceInstructions(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 0

    .line 61
    invoke-direct {p0, p1, p2}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->announceInstructions(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V

    return-void
.end method

.method public static final synthetic access$displayKeypad(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)V
    .locals 0

    .line 61
    invoke-direct {p0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->displayKeypad(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)V

    return-void
.end method

.method public static final synthetic access$getBuyerLocaleOverride$p(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)Lcom/squareup/buyer/language/BuyerLocaleOverride;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-object p0
.end method

.method public static final synthetic access$getRootLayout$p(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 1

    .line 61
    iget-object p0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->rootLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez p0, :cond_0

    const-string v0, "rootLayout"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRootLayoutChange$p(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->rootLayoutChange:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$maybeAnnounceDigitsEntered(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;Landroid/view/View;ILcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 0

    .line 61
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->maybeAnnounceDigitsEntered(Landroid/view/View;ILcom/squareup/buyer/language/BuyerLocaleOverride;)V

    return-void
.end method

.method public static final synthetic access$sendLayout(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)V
    .locals 0

    .line 61
    invoke-direct {p0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->sendLayout(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)V

    return-void
.end method

.method public static final synthetic access$setRootLayout$p(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;Landroidx/constraintlayout/widget/ConstraintLayout;)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->rootLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    return-void
.end method

.method public static final synthetic access$spinnerState(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 0

    .line 61
    invoke-direct {p0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->spinnerState(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$update(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;Lcom/squareup/util/Res;)V
    .locals 0

    .line 61
    invoke-direct {p0, p1, p2}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->update(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;Lcom/squareup/util/Res;)V

    return-void
.end method

.method private final announceInstructions(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 2

    .line 184
    invoke-interface {p2}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->toTitleResource(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;Z)I

    move-result p1

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 185
    invoke-interface {p2}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerLocale()Ljava/util/Locale;

    move-result-object p2

    if-eqz p1, :cond_1

    invoke-virtual {p1, p2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    iget-object p2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->keypadTitle:Lcom/squareup/marketfont/MarketTextView;

    if-nez p2, :cond_0

    const-string v0, "keypadTitle"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p2, Landroid/view/View;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p2, p1}, Lcom/squareup/accessibility/Accessibility;->announceForAccessibility(Landroid/view/View;Ljava/lang/CharSequence;)V

    return-void

    .line 185
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 143
    sget v0, Lcom/squareup/securetouch/accessibility/impl/R$id;->secure_touch_accessible_layout:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    iput-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->rootLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 144
    sget v0, Lcom/squareup/securetouch/accessibility/impl/R$id;->accessible_keypad_bounding_box:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->accessibilityKeypadArea:Landroid/view/View;

    .line 145
    sget v0, Lcom/squareup/securetouch/accessibility/impl/R$id;->keypad_digits_1_through_9:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    iput-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->digitsOneThroughNine:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 146
    sget v0, Lcom/squareup/securetouch/accessibility/impl/R$id;->keypad_digit_0:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    iput-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->digitZero:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 147
    sget v0, Lcom/squareup/securetouch/accessibility/impl/R$id;->cancel_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->cancelButton:Landroid/widget/ImageView;

    .line 148
    sget v0, Lcom/squareup/securetouch/accessibility/impl/R$id;->accessible_keypad_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->keypadTitle:Lcom/squareup/marketfont/MarketTextView;

    .line 149
    sget v0, Lcom/squareup/securetouch/accessibility/impl/R$id;->star_group:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/StarGroup;

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    return-void
.end method

.method private final displayDigitZero(Lcom/squareup/securetouch/SecureTouchPoint;)V
    .locals 4

    .line 239
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->digitsOneThroughNine:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v0, :cond_0

    const-string v1, "digitsOneThroughNine"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setInvisible(Landroid/view/View;)V

    .line 242
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->digitZero:Landroidx/constraintlayout/widget/ConstraintLayout;

    const-string v1, "digitZero"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->getCenterOfScreen()Lcom/squareup/securetouch/SecureTouchPoint;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/squareup/securetouch/SecureTouchPoint;->minus(Lcom/squareup/securetouch/SecureTouchPoint;)Lcom/squareup/securetouch/SecureTouchPoint;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;->access$moveTo(Landroidx/constraintlayout/widget/ConstraintLayout;Lcom/squareup/securetouch/SecureTouchPoint;)V

    .line 244
    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->keypadTitle:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_2

    const-string v0, "keypadTitle"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->resources:Landroid/content/res/Resources;

    const-string v2, "resources"

    if-nez v0, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget v3, Lcom/squareup/securetouch/accessibility/impl/R$dimen;->text_occluded_color_alpha:I

    invoke-static {v0, v3}, Lcom/squareup/util/Views;->getFloatCompat(Landroid/content/res/Resources;I)F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setAlpha(F)V

    .line 245
    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    if-nez p1, :cond_4

    const-string v0, "starGroup"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->resources:Landroid/content/res/Resources;

    if-nez v0, :cond_5

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    sget v2, Lcom/squareup/securetouch/accessibility/impl/R$dimen;->text_occluded_color_alpha:I

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->getFloatCompat(Landroid/content/res/Resources;I)F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/StarGroup;->setAlpha(F)V

    .line 246
    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->digitZero:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez p1, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    return-void
.end method

.method private final displayDigitsOneThroughNine(Lcom/squareup/securetouch/SecureTouchPoint;)V
    .locals 4

    .line 250
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->digitZero:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v0, :cond_0

    const-string v1, "digitZero"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setInvisible(Landroid/view/View;)V

    .line 253
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->digitsOneThroughNine:Landroidx/constraintlayout/widget/ConstraintLayout;

    const-string v1, "digitsOneThroughNine"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->getCenterOfScreen()Lcom/squareup/securetouch/SecureTouchPoint;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/squareup/securetouch/SecureTouchPoint;->minus(Lcom/squareup/securetouch/SecureTouchPoint;)Lcom/squareup/securetouch/SecureTouchPoint;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;->access$moveTo(Landroidx/constraintlayout/widget/ConstraintLayout;Lcom/squareup/securetouch/SecureTouchPoint;)V

    .line 255
    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->keypadTitle:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_2

    const-string v0, "keypadTitle"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->resources:Landroid/content/res/Resources;

    const-string v2, "resources"

    if-nez v0, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget v3, Lcom/squareup/securetouch/accessibility/impl/R$dimen;->text_occluded_color_alpha:I

    invoke-static {v0, v3}, Lcom/squareup/util/Views;->getFloatCompat(Landroid/content/res/Resources;I)F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setAlpha(F)V

    .line 256
    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    if-nez p1, :cond_4

    const-string v0, "starGroup"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->resources:Landroid/content/res/Resources;

    if-nez v0, :cond_5

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    sget v2, Lcom/squareup/securetouch/accessibility/impl/R$dimen;->text_occluded_color_alpha:I

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->getFloatCompat(Landroid/content/res/Resources;I)F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/StarGroup;->setAlpha(F)V

    .line 257
    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->digitsOneThroughNine:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez p1, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    return-void
.end method

.method private final displayKeypad(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)V
    .locals 1

    .line 231
    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->getScreenData()Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;->getDisplayInputDigits()Lcom/squareup/securetouch/accessibility/InputDigits;

    move-result-object p1

    .line 232
    sget-object v0, Lcom/squareup/securetouch/accessibility/NoDigits;->INSTANCE:Lcom/squareup/securetouch/accessibility/NoDigits;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->hideAllDigits()V

    goto :goto_0

    .line 233
    :cond_0
    instance-of v0, p1, Lcom/squareup/securetouch/accessibility/DigitZero;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/securetouch/accessibility/DigitZero;

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/DigitZero;->getCenter()Lcom/squareup/securetouch/SecureTouchPoint;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->displayDigitZero(Lcom/squareup/securetouch/SecureTouchPoint;)V

    goto :goto_0

    .line 234
    :cond_1
    instance-of v0, p1, Lcom/squareup/securetouch/accessibility/DigitsOneThroughNine;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/securetouch/accessibility/DigitsOneThroughNine;

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/DigitsOneThroughNine;->getCenter()Lcom/squareup/securetouch/SecureTouchPoint;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->displayDigitsOneThroughNine(Lcom/squareup/securetouch/SecureTouchPoint;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private final getCenterOfScreen()Lcom/squareup/securetouch/SecureTouchPoint;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->centerOfScreen$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/securetouch/SecureTouchPoint;

    return-object v0
.end method

.method private final hideAllDigits()V
    .locals 4

    .line 261
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->keypadTitle:Lcom/squareup/marketfont/MarketTextView;

    if-nez v0, :cond_0

    const-string v1, "keypadTitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->resources:Landroid/content/res/Resources;

    const-string v2, "resources"

    if-nez v1, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v3, Lcom/squareup/securetouch/accessibility/impl/R$dimen;->text_color_alpha:I

    invoke-static {v1, v3}, Lcom/squareup/util/Views;->getFloatCompat(Landroid/content/res/Resources;I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setAlpha(F)V

    .line 262
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    if-nez v0, :cond_2

    const-string v1, "starGroup"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->resources:Landroid/content/res/Resources;

    if-nez v1, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget v2, Lcom/squareup/securetouch/accessibility/impl/R$dimen;->text_color_alpha:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->getFloatCompat(Landroid/content/res/Resources;I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/StarGroup;->setAlpha(F)V

    .line 263
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->digitsOneThroughNine:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v0, :cond_4

    const-string v1, "digitsOneThroughNine"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setInvisible(Landroid/view/View;)V

    .line 264
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->digitZero:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v0, :cond_5

    const-string v1, "digitZero"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setInvisible(Landroid/view/View;)V

    return-void
.end method

.method private final inflateInNightMode(Landroid/view/View;)V
    .locals 2

    .line 276
    sget v0, Lcom/squareup/securetouch/accessibility/impl/R$id;->screen_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 277
    sget v1, Lcom/squareup/securetouch/accessibility/impl/R$layout;->accessible_keypad:I

    invoke-static {p1, v1}, Lcom/squareup/noho/NightModeUtilsKt;->inflateViewInNightMode(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    .line 276
    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private final maybeAnnounceDigitsEntered(Landroid/view/View;ILcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 2

    .line 194
    invoke-interface {p3}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object p3

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 197
    sget p2, Lcom/squareup/securetouch/accessibility/impl/R$string;->accessible_keypad_single_digit_entered:I

    invoke-interface {p3, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    .line 196
    invoke-virtual {p1, p2}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    const/16 v0, 0xc

    const/4 v1, 0x2

    if-le v1, p2, :cond_1

    goto :goto_0

    :cond_1
    if-lt v0, p2, :cond_2

    .line 200
    sget v0, Lcom/squareup/securetouch/accessibility/impl/R$string;->accessible_keypad_multiple_digits_entered:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    const-string v0, "number"

    invoke-virtual {p3, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 202
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    .line 199
    invoke-virtual {p1, p2}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private final sendLayout(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)V
    .locals 6

    .line 153
    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->getOnKeypadActive()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 154
    new-instance v0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/Pair;

    .line 156
    sget-object v2, Lcom/squareup/securetouch/SecureKey;->Cancel:Lcom/squareup/securetouch/SecureKey;

    iget-object v3, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->cancelButton:Landroid/widget/ImageView;

    if-nez v3, :cond_0

    const-string v4, "cancelButton"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v3, Landroid/view/View;

    invoke-static {v3}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;->access$getGlobalVisibleRect(Landroid/view/View;)Lcom/squareup/securetouch/SecureTouchRect;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    .line 157
    sget-object v3, Lcom/squareup/securetouch/SecureKey;->AccessibilityKeypadArea:Lcom/squareup/securetouch/SecureKey;

    iget-object v4, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->accessibilityKeypadArea:Landroid/view/View;

    if-nez v4, :cond_1

    const-string v5, "accessibilityKeypadArea"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v4}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;->access$getGlobalVisibleRect(Landroid/view/View;)Lcom/squareup/securetouch/SecureTouchRect;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    aput-object v3, v1, v2

    .line 155
    invoke-static {v1}, Lkotlin/collections/MapsKt;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v1

    .line 159
    iget-object v2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->resources:Landroid/content/res/Resources;

    const-string v3, "resources"

    if-nez v2, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget v4, Lcom/squareup/securetouch/accessibility/impl/R$dimen;->accessible_keypad_margin:I

    invoke-static {v2, v4}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;->access$getDimensionInPx(Landroid/content/res/Resources;I)I

    move-result v2

    .line 160
    iget-object v4, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->resources:Landroid/content/res/Resources;

    if-nez v4, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget v3, Lcom/squareup/securetouch/accessibility/impl/R$dimen;->accessible_keypad_digit_radius:I

    invoke-static {v4, v3}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;->access$getDimensionInPx(Landroid/content/res/Resources;I)I

    move-result v3

    .line 154
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;-><init>(Ljava/util/Map;II)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final spinnerState(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 4

    .line 208
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->getScreenData()Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$WaitingForReader;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, p1, v1, v2, v3}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method private final toTitleResource(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;Z)I
    .locals 1

    .line 215
    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->getScreenData()Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object p1

    .line 216
    sget-object v0, Lcom/squareup/securetouch/PinFirstTry;->INSTANCE:Lcom/squareup/securetouch/PinFirstTry;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    .line 218
    sget p1, Lcom/squareup/securetouch/accessibility/impl/R$string;->accessible_keypad_first_try_instructions:I

    goto :goto_0

    .line 220
    :cond_0
    sget p1, Lcom/squareup/securetouch/accessibility/impl/R$string;->accessible_keypad_first_try_title:I

    goto :goto_0

    .line 223
    :cond_1
    sget-object p2, Lcom/squareup/securetouch/PinRetry;->INSTANCE:Lcom/squareup/securetouch/PinRetry;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    sget p1, Lcom/squareup/securetouch/accessibility/impl/R$string;->accessible_keypad_retry_title:I

    goto :goto_0

    .line 224
    :cond_2
    sget-object p2, Lcom/squareup/securetouch/PinLastTry;->INSTANCE:Lcom/squareup/securetouch/PinLastTry;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget p1, Lcom/squareup/securetouch/accessibility/impl/R$string;->accessible_keypad_last_try_title:I

    :goto_0
    return p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method static synthetic toTitleResource$default(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;ZILjava/lang/Object;)I
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 213
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->toTitleResource(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;Z)I

    move-result p0

    return p0
.end method

.method private final update(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;Lcom/squareup/util/Res;)V
    .locals 4

    .line 169
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->keypadTitle:Lcom/squareup/marketfont/MarketTextView;

    if-nez v0, :cond_0

    const-string v1, "keypadTitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p0, p1, v3, v1, v2}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->toTitleResource$default(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;ZILjava/lang/Object;)I

    move-result v1

    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object p2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->cancelButton:Landroid/widget/ImageView;

    if-nez p2, :cond_1

    const-string v0, "cancelButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p2, Landroid/view/View;

    invoke-static {p2, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;->access$reportTouchEventsDevOnly(Landroid/view/View;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)V

    .line 171
    iget-object p2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->accessibilityKeypadArea:Landroid/view/View;

    if-nez p2, :cond_2

    const-string v0, "accessibilityKeypadArea"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-static {p2, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;->access$reportTouchEventsDevOnly(Landroid/view/View;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)V

    .line 173
    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->getScreenData()Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;->getDigitsEntered()I

    move-result p1

    const/16 p2, 0xc

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 174
    iget-object p2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    const-string v0, "starGroup"

    if-nez p2, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p2, p1}, Lcom/squareup/ui/StarGroup;->setExpectedStarCount(I)V

    .line 175
    iget-object p2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    if-nez p2, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p2, p1}, Lcom/squareup/ui/StarGroup;->setStarCount(I)V

    .line 176
    iget-object p2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    if-nez p2, :cond_5

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    if-lez p1, :cond_6

    goto :goto_0

    :cond_6
    const/4 v3, 0x4

    :goto_0
    invoke-virtual {p2, v3}, Lcom/squareup/ui/StarGroup;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-direct {p0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->inflateInNightMode(Landroid/view/View;)V

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->bindViews(Landroid/view/View;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "glassSpinner.showOrHideSpinner(view.context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 98
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string/jumbo v1, "view.resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->resources:Landroid/content/res/Resources;

    .line 100
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$1;-><init>(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 104
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->screens:Lio/reactivex/Observable;

    iget-object v2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    invoke-virtual {v2}, Lcom/squareup/accessibility/AccessibilitySettings;->getTalkBackState()Lio/reactivex/Observable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 105
    sget-object v1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$2;->INSTANCE:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$2;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 106
    sget-object v1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$3;->INSTANCE:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$3;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->distinctUntilChanged(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "combineLatest(screens, a\u2026creenData.pinEntryState }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    new-instance v1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$4;

    invoke-direct {v1, p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$4;-><init>(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 111
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->screens:Lio/reactivex/Observable;

    iget-object v2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    invoke-virtual {v2}, Lcom/squareup/accessibility/AccessibilitySettings;->getTalkBackState()Lio/reactivex/Observable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 112
    sget-object v1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$5;->INSTANCE:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$5;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 113
    sget-object v1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$6;->INSTANCE:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$6;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->distinctUntilChanged(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 115
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->mainThread:Lio/reactivex/Scheduler;

    const-wide/16 v3, 0xfa

    .line 114
    invoke-virtual {v0, v3, v4, v1, v2}, Lio/reactivex/Observable;->debounce(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "combineLatest(screens, a\u2026NDS, mainThread\n        )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    new-instance v1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$7;

    invoke-direct {v1, p0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$7;-><init>(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 123
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->screens:Lio/reactivex/Observable;

    .line 124
    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    new-instance v2, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$8;

    invoke-direct {v2, p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$8;-><init>(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)V

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2(Lrx/functions/Func1;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 125
    sget-object v1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$9;->INSTANCE:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$9;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->distinctUntilChanged(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 126
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->mainThread:Lio/reactivex/Scheduler;

    const-wide/16 v3, 0x1e

    invoke-virtual {v0, v3, v4, v1, v2}, Lio/reactivex/Observable;->debounce(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens\n        .compose\u2026MILLISECONDS, mainThread)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    new-instance v1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$10;

    invoke-direct {v1, p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$10;-><init>(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 130
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 131
    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->screens:Lio/reactivex/Observable;

    .line 132
    iget-object v2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->rootLayoutChange:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v2, Lio/reactivex/Observable;

    .line 130
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 134
    sget-object v1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$11;->INSTANCE:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$11;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "combineLatest(\n        s\u2026 WaitingForKeypadLayout }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    new-instance v1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$12;

    invoke-direct {v1, p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$12;-><init>(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 137
    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->rootLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez p1, :cond_0

    const-string v0, "rootLayout"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$13;

    invoke-direct {v0, p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$13;-><init>(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)V

    check-cast v0, Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {p1, v0}, Landroidx/constraintlayout/widget/ConstraintLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    return-void
.end method
