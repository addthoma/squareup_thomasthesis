.class final Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSecureTouchWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3;->invoke(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/securetouch/SecureTouchState;",
        "-",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/securetouch/SecureTouchState;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "invoke",
        "com/squareup/securetouch/RealSecureTouchWorkflow$render$screen$3$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $event:Lcom/squareup/securetouch/SecureTouchKeyPressEvent;

.field final synthetic this$0:Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/SecureTouchKeyPressEvent;Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3$lambda$1;->$event:Lcom/squareup/securetouch/SecureTouchKeyPressEvent;

    iput-object p2, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3$lambda$1;->this$0:Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3$lambda$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/securetouch/SecureTouchState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3$lambda$1;->this$0:Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3;

    iget-object v0, v0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3;->this$0:Lcom/squareup/securetouch/RealSecureTouchWorkflow;

    iget-object v1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3$lambda$1;->this$0:Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3;

    iget-object v1, v1, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3;->$state$inlined:Lcom/squareup/securetouch/SecureTouchState;

    check-cast v1, Lcom/squareup/securetouch/UsingKeypad;

    iget-object v2, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3$lambda$1;->$event:Lcom/squareup/securetouch/SecureTouchKeyPressEvent;

    invoke-static {v0, v1, v2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->access$afterTouchOn(Lcom/squareup/securetouch/RealSecureTouchWorkflow;Lcom/squareup/securetouch/UsingKeypad;Lcom/squareup/securetouch/SecureTouchKeyPressEvent;)Lcom/squareup/securetouch/SecureTouchState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    .line 121
    new-instance v0, Lcom/squareup/securetouch/LogKeyPress;

    iget-object v1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3$lambda$1;->$event:Lcom/squareup/securetouch/SecureTouchKeyPressEvent;

    invoke-direct {v0, v1}, Lcom/squareup/securetouch/LogKeyPress;-><init>(Lcom/squareup/securetouch/SecureTouchKeyPressEvent;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method
