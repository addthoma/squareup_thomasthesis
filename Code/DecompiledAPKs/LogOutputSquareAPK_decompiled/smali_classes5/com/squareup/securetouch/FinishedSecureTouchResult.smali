.class public abstract Lcom/squareup/securetouch/FinishedSecureTouchResult;
.super Lcom/squareup/securetouch/SecureTouchResult;
.source "SecureTouchResult.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/FinishedSecureTouchResult$Completed;,
        Lcom/squareup/securetouch/FinishedSecureTouchResult$Failed;,
        Lcom/squareup/securetouch/FinishedSecureTouchResult$Cancelled;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/securetouch/FinishedSecureTouchResult;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "()V",
        "Cancelled",
        "Completed",
        "Failed",
        "Lcom/squareup/securetouch/FinishedSecureTouchResult$Completed;",
        "Lcom/squareup/securetouch/FinishedSecureTouchResult$Failed;",
        "Lcom/squareup/securetouch/FinishedSecureTouchResult$Cancelled;",
        "secure-touch-workflow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 6
    invoke-direct {p0, v0}, Lcom/squareup/securetouch/SecureTouchResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/squareup/securetouch/FinishedSecureTouchResult;-><init>()V

    return-void
.end method
