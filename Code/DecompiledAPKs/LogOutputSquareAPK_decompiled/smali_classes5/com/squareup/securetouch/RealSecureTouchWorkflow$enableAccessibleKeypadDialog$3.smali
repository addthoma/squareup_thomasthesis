.class final Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSecureTouchWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/RealSecureTouchWorkflow;->enableAccessibleKeypadDialog(Lcom/squareup/workflow/RenderContext;Lcom/squareup/securetouch/ShowingEnableAccessibleKeypadDialog;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen$EnableAccessibleKeypadDialogEvent;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "event",
        "Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen$EnableAccessibleKeypadDialogEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic $currentState:Lcom/squareup/securetouch/ShowingEnableAccessibleKeypadDialog;

.field final synthetic this$0:Lcom/squareup/securetouch/RealSecureTouchWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/RealSecureTouchWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/securetouch/ShowingEnableAccessibleKeypadDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$3;->this$0:Lcom/squareup/securetouch/RealSecureTouchWorkflow;

    iput-object p2, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$3;->$context:Lcom/squareup/workflow/RenderContext;

    iput-object p3, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$3;->$currentState:Lcom/squareup/securetouch/ShowingEnableAccessibleKeypadDialog;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen$EnableAccessibleKeypadDialogEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$3;->invoke(Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen$EnableAccessibleKeypadDialogEvent;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen$EnableAccessibleKeypadDialogEvent;)V
    .locals 4

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 298
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$3;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 300
    instance-of v1, p1, Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen$EnableAccessibleKeypadDialogEvent$HideDialog;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    iget-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$3;->this$0:Lcom/squareup/securetouch/RealSecureTouchWorkflow;

    new-instance v1, Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$3$1;

    invoke-direct {v1, p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$3$1;-><init>(Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$3;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v3, v1, v2, v3}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 308
    :cond_0
    instance-of p1, p1, Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen$EnableAccessibleKeypadDialogEvent$EnableAccessibleKeypad;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$3;->this$0:Lcom/squareup/securetouch/RealSecureTouchWorkflow;

    sget-object v1, Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$3$2;->INSTANCE:Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$3$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v3, v1, v2, v3}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    .line 298
    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void

    .line 308
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
