.class final Lcom/squareup/receiving/StandardReceiver$receivedResponseToSuccessOrFailure$1$1;
.super Ljava/lang/Object;
.source "StandardReceiver.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/receiving/StandardReceiver$receivedResponseToSuccessOrFailure$1;->apply(Lio/reactivex/Single;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u0002H\u0003 \u0004*\n\u0012\u0004\u0012\u0002H\u0003\u0018\u00010\u00020\u0002 \u0004*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u0002H\u0003 \u0004*\n\u0012\u0004\u0012\u0002H\u0003\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u0001\"\u0004\u0008\u0000\u0010\u00032\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "T",
        "kotlin.jvm.PlatformType",
        "received",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/receiving/StandardReceiver$receivedResponseToSuccessOrFailure$1;


# direct methods
.method constructor <init>(Lcom/squareup/receiving/StandardReceiver$receivedResponseToSuccessOrFailure$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/receiving/StandardReceiver$receivedResponseToSuccessOrFailure$1$1;->this$0:Lcom/squareup/receiving/StandardReceiver$receivedResponseToSuccessOrFailure$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/ReceivedResponse;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "+TT;>;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;>;"
        }
    .end annotation

    const-string v0, "received"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/receiving/StandardReceiver$receivedResponseToSuccessOrFailure$1$1;->this$0:Lcom/squareup/receiving/StandardReceiver$receivedResponseToSuccessOrFailure$1;

    iget-object v0, v0, Lcom/squareup/receiving/StandardReceiver$receivedResponseToSuccessOrFailure$1;->this$0:Lcom/squareup/receiving/StandardReceiver;

    invoke-static {v0}, Lcom/squareup/receiving/StandardReceiver;->access$getMainThreadEnforcer$p(Lcom/squareup/receiving/StandardReceiver;)Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 143
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz v0, :cond_0

    .line 148
    iget-object p1, p0, Lcom/squareup/receiving/StandardReceiver$receivedResponseToSuccessOrFailure$1$1;->this$0:Lcom/squareup/receiving/StandardReceiver$receivedResponseToSuccessOrFailure$1;

    iget-object p1, p1, Lcom/squareup/receiving/StandardReceiver$receivedResponseToSuccessOrFailure$1;->this$0:Lcom/squareup/receiving/StandardReceiver;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver;->getSessionExpiredHandler$public_release()Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;->onSessionExpired()V

    .line 149
    invoke-static {}, Lio/reactivex/Single;->never()Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 152
    :cond_0
    invoke-static {p1}, Lcom/squareup/receiving/StandardReceiverKt;->toSuccessOrFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/receiving/StandardReceiver$receivedResponseToSuccessOrFailure$1$1;->apply(Lcom/squareup/receiving/ReceivedResponse;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
