.class final Lcom/squareup/receiving/StandardReceiverRetrofit1$succeedOrFail$1;
.super Ljava/lang/Object;
.source "StandardReceiverRetrofit1.kt"

# interfaces
.implements Lrx/Observable$Transformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$Transformer<",
        "TT;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+TT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u0002H\u0003 \u0004*\n\u0012\u0004\u0012\u0002H\u0003\u0018\u00010\u00020\u0002 \u0004*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u0002H\u0003 \u0004*\n\u0012\u0004\u0012\u0002H\u0003\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u0001\"\u0004\u0008\u0000\u0010\u00032*\u0010\u0005\u001a&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u0001H\u0003H\u0003 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u0001H\u0003H\u0003\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Observable;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "T",
        "kotlin.jvm.PlatformType",
        "observable",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $isSuccessful:Lkotlin/jvm/functions/Function1;

.field final synthetic $this_succeedOrFail:Lcom/squareup/receiving/StandardReceiver;


# direct methods
.method constructor <init>(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/receiving/StandardReceiverRetrofit1$succeedOrFail$1;->$this_succeedOrFail:Lcom/squareup/receiving/StandardReceiver;

    iput-object p2, p0, Lcom/squareup/receiving/StandardReceiverRetrofit1$succeedOrFail$1;->$isSuccessful:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lrx/Observable;

    invoke-virtual {p0, p1}, Lcom/squareup/receiving/StandardReceiverRetrofit1$succeedOrFail$1;->call(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lrx/Observable;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "TT;>;)",
            "Lrx/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;>;"
        }
    .end annotation

    .line 18
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    .line 19
    invoke-virtual {p1}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object p1

    .line 20
    iget-object v0, p0, Lcom/squareup/receiving/StandardReceiverRetrofit1$succeedOrFail$1;->$isSuccessful:Lkotlin/jvm/functions/Function1;

    invoke-static {v0}, Lcom/squareup/receiving/ReceivedResponseRetrofit1;->receiveFromRetrofit1(Lkotlin/jvm/functions/Function1;)Lio/reactivex/SingleTransformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->compose(Lio/reactivex/SingleTransformer;)Lio/reactivex/Single;

    move-result-object p1

    .line 21
    iget-object v0, p0, Lcom/squareup/receiving/StandardReceiverRetrofit1$succeedOrFail$1;->$this_succeedOrFail:Lcom/squareup/receiving/StandardReceiver;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver;->receivedResponseToSuccessOrFailure()Lio/reactivex/SingleTransformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->compose(Lio/reactivex/SingleTransformer;)Lio/reactivex/Single;

    move-result-object p1

    .line 22
    check-cast p1, Lio/reactivex/SingleSource;

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
