.class final Lcom/squareup/receiving/StandardReceiverKt$retryExponentialBackoff$3;
.super Ljava/lang/Object;
.source "StandardReceiver.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/receiving/StandardReceiverKt;->retryExponentialBackoff(Lio/reactivex/Single;IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Ljava/lang/Throwable;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+TT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u0016\u0012\u0004\u0012\u0002H\u0002 \u0003*\n\u0012\u0004\u0012\u0002H\u0002\u0018\u00010\u00010\u0001\"\u0004\u0008\u0000\u0010\u00022\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "T",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/receiving/StandardReceiverKt$retryExponentialBackoff$3;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/receiving/StandardReceiverKt$retryExponentialBackoff$3;

    invoke-direct {v0}, Lcom/squareup/receiving/StandardReceiverKt$retryExponentialBackoff$3;-><init>()V

    sput-object v0, Lcom/squareup/receiving/StandardReceiverKt$retryExponentialBackoff$3;->INSTANCE:Lcom/squareup/receiving/StandardReceiverKt$retryExponentialBackoff$3;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Throwable;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    instance-of v0, p1, Lcom/squareup/receiving/ShowFailureRetryException;

    if-eqz v0, :cond_1

    .line 216
    check-cast p1, Lcom/squareup/receiving/ShowFailureRetryException;

    invoke-virtual {p1}, Lcom/squareup/receiving/ShowFailureRetryException;->getShowFailure()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.receiving.StandardReceiver.SuccessOrFailure.ShowFailure<T>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 218
    :cond_1
    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/squareup/receiving/StandardReceiverKt$retryExponentialBackoff$3;->apply(Ljava/lang/Throwable;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    return-object p1
.end method
