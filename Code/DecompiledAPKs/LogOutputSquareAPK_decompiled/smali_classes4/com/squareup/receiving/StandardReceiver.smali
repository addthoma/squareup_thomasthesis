.class public final Lcom/squareup/receiving/StandardReceiver;
.super Ljava/lang/Object;
.source "StandardReceiver.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;,
        Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;,
        Lcom/squareup/receiving/StandardReceiver$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u001b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0006\u0018\u0000 \u001b2\u00020\u0001:\u0003\u001b\u001c\u001dB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J$\u0010\t\u001a\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u000c0\u000b\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u000c0\r0\n\"\u0004\u0008\u0000\u0010\u000cJ]\u0010\u000e\u001a\u0014\u0012\u0004\u0012\u0002H\u000c\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u000c0\r0\n\"\u0008\u0008\u0000\u0010\u000c*\u00020\u00012\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00142\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u00020\u00180\u0017H\u0000\u00a2\u0006\u0004\u0008\u0019\u0010\u001aR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/receiving/StandardReceiver;",
        "",
        "mainThreadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "sessionExpiredHandler",
        "Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;",
        "(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;)V",
        "getSessionExpiredHandler$public_release",
        "()Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;",
        "receivedResponseToSuccessOrFailure",
        "Lio/reactivex/SingleTransformer;",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "T",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "succeedOrFailSingle",
        "retrofit",
        "Lretrofit2/Retrofit;",
        "type",
        "Ljava/lang/reflect/Type;",
        "annotations",
        "",
        "",
        "isSuccessful",
        "Lkotlin/Function1;",
        "",
        "succeedOrFailSingle$public_release",
        "(Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lkotlin/jvm/functions/Function1;)Lio/reactivex/SingleTransformer;",
        "Companion",
        "SessionExpiredHandler",
        "SuccessOrFailure",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/receiving/StandardReceiver$Companion;


# instance fields
.field private final mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final sessionExpiredHandler:Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/receiving/StandardReceiver$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/receiving/StandardReceiver$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/receiving/StandardReceiver;->Companion:Lcom/squareup/receiving/StandardReceiver$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;)V
    .locals 1

    const-string v0, "mainThreadEnforcer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionExpiredHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/receiving/StandardReceiver;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iput-object p2, p0, Lcom/squareup/receiving/StandardReceiver;->sessionExpiredHandler:Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;

    return-void
.end method

.method public static final synthetic access$getMainThreadEnforcer$p(Lcom/squareup/receiving/StandardReceiver;)Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/squareup/receiving/StandardReceiver;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-object p0
.end method

.method public static final filterSuccess()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/ObservableTransformer<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/receiving/StandardReceiver;->Companion:Lcom/squareup/receiving/StandardReceiver$Companion;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$Companion;->filterSuccess()Lio/reactivex/ObservableTransformer;

    move-result-object v0

    return-object v0
.end method

.method public static final rejectIfNot(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/receiving/StandardReceiver;->Companion:Lcom/squareup/receiving/StandardReceiver$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/receiving/StandardReceiver$Companion;->rejectIfNot(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final getSessionExpiredHandler$public_release()Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/receiving/StandardReceiver;->sessionExpiredHandler:Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;

    return-object v0
.end method

.method public final receivedResponseToSuccessOrFailure()Lio/reactivex/SingleTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/SingleTransformer<",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TT;>;",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;>;"
        }
    .end annotation

    .line 138
    new-instance v0, Lcom/squareup/receiving/StandardReceiver$receivedResponseToSuccessOrFailure$1;

    invoke-direct {v0, p0}, Lcom/squareup/receiving/StandardReceiver$receivedResponseToSuccessOrFailure$1;-><init>(Lcom/squareup/receiving/StandardReceiver;)V

    check-cast v0, Lio/reactivex/SingleTransformer;

    return-object v0
.end method

.method public final succeedOrFailSingle$public_release(Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lkotlin/jvm/functions/Function1;)Lio/reactivex/SingleTransformer;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lretrofit2/Retrofit;",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/SingleTransformer<",
            "TT;",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;>;"
        }
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotations"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isSuccessful"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    new-instance v0, Lcom/squareup/receiving/StandardReceiver$succeedOrFailSingle$1;

    move-object v1, v0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/receiving/StandardReceiver$succeedOrFailSingle$1;-><init>(Lcom/squareup/receiving/StandardReceiver;Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lio/reactivex/SingleTransformer;

    return-object v0
.end method
