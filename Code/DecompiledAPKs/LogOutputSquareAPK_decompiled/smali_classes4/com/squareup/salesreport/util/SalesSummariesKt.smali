.class public final Lcom/squareup/salesreport/util/SalesSummariesKt;
.super Ljava/lang/Object;
.source "SalesSummaries.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0004\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0000\u001a8\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\tH\u0000\u001a0\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e*\u00020\u00022\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "amountFromChartSalesSelection",
        "",
        "Lcom/squareup/customreport/data/SalesSummary;",
        "chartSalesSelection",
        "Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;",
        "textFromChartSalesSelection",
        "",
        "kotlin.jvm.PlatformType",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "numberFormatter",
        "",
        "truncatedSalesDetailsRows",
        "",
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final amountFromChartSalesSelection(Lcom/squareup/customreport/data/SalesSummary;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;)J
    .locals 3

    const-string v0, "$this$amountFromChartSalesSelection"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chartSalesSelection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    sget-object v0, Lcom/squareup/salesreport/util/SalesSummariesKt$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    const-wide/16 v1, 0x0

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/squareup/customreport/data/SalesSummary;->getSalesCount()J

    move-result-wide v1

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 37
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/customreport/data/SalesSummary;->getNetSales()Lcom/squareup/protos/common/Money;

    move-result-object p0

    if-eqz p0, :cond_3

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    goto :goto_0

    .line 36
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/customreport/data/SalesSummary;->getGrossSales()Lcom/squareup/protos/common/Money;

    move-result-object p0

    if-eqz p0, :cond_3

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    :cond_3
    :goto_0
    return-wide v1
.end method

.method public static final textFromChartSalesSelection(Lcom/squareup/customreport/data/SalesSummary;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/SalesSummary;",
            "Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    const-string v0, "$this$textFromChartSalesSelection"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chartSalesSelection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "numberFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    sget-object v0, Lcom/squareup/salesreport/util/SalesSummariesKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 p2, 0x3

    if-ne p1, p2, :cond_0

    .line 27
    invoke-virtual {p0}, Lcom/squareup/customreport/data/SalesSummary;->getSalesCount()J

    move-result-wide p0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-interface {p3, p0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 26
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/customreport/data/SalesSummary;->getNetSales()Lcom/squareup/protos/common/Money;

    move-result-object p0

    invoke-interface {p2, p0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_0

    .line 25
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/customreport/data/SalesSummary;->getGrossSales()Lcom/squareup/protos/common/Money;

    move-result-object p0

    invoke-interface {p2, p0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static final truncatedSalesDetailsRows(Lcom/squareup/customreport/data/SalesSummary;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)Ljava/util/List;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/SalesSummary;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    const-string v2, "$this$truncatedSalesDetailsRows"

    move-object/from16 v3, p0

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "moneyFormatter"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "settings"

    move-object/from16 v4, p2

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "features"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    new-instance v2, Lcom/squareup/salesreport/util/SalesSummariesKt$truncatedSalesDetailsRows$1;

    invoke-direct {v2, v0}, Lcom/squareup/salesreport/util/SalesSummariesKt$truncatedSalesDetailsRows$1;-><init>(Lcom/squareup/text/Formatter;)V

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    .line 49
    new-instance v15, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    .line 50
    new-instance v5, Lcom/squareup/util/ViewString$ResourceString;

    sget v6, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_gross_sales:I

    invoke-direct {v5, v6}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v6, v5

    check-cast v6, Lcom/squareup/util/ViewString;

    .line 51
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/customreport/data/SalesSummary;->getGrossSales()Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/squareup/salesreport/util/SalesSummariesKt$truncatedSalesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x70

    const/4 v14, 0x0

    move-object v5, v15

    .line 49
    invoke-direct/range {v5 .. v14}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v5, 0x0

    aput-object v15, v0, v5

    .line 55
    new-instance v5, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    .line 56
    new-instance v6, Lcom/squareup/util/ViewString$ResourceString;

    sget v7, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_refund:I

    invoke-direct {v6, v7}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object/from16 v17, v6

    check-cast v17, Lcom/squareup/util/ViewString;

    .line 57
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/customreport/data/SalesSummary;->getRefunds()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/squareup/salesreport/util/SalesSummariesKt$truncatedSalesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x70

    const/16 v25, 0x0

    move-object/from16 v16, v5

    .line 55
    invoke-direct/range {v16 .. v25}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v6, 0x1

    aput-object v5, v0, v6

    .line 61
    new-instance v5, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    .line 62
    new-instance v7, Lcom/squareup/util/ViewString$ResourceString;

    sget v8, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_discounts_and_comps:I

    invoke-direct {v7, v8}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v8, v7

    check-cast v8, Lcom/squareup/util/ViewString;

    .line 63
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/customreport/data/SalesSummary;->getDiscounts()Lcom/squareup/protos/common/Money;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/squareup/salesreport/util/SalesSummariesKt$truncatedSalesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v9

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v15, 0x70

    const/16 v16, 0x0

    move-object v7, v5

    .line 61
    invoke-direct/range {v7 .. v16}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v7, 0x2

    aput-object v5, v0, v7

    .line 67
    new-instance v5, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    .line 68
    new-instance v7, Lcom/squareup/util/ViewString$ResourceString;

    sget v8, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_net_sales:I

    invoke-direct {v7, v8}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v9, v7

    check-cast v9, Lcom/squareup/util/ViewString;

    .line 69
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/customreport/data/SalesSummary;->getNetSales()Lcom/squareup/protos/common/Money;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/squareup/salesreport/util/SalesSummariesKt$truncatedSalesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x70

    const/16 v17, 0x0

    move-object v8, v5

    .line 67
    invoke-direct/range {v8 .. v17}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v7, 0x3

    aput-object v5, v0, v7

    .line 73
    new-instance v5, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    .line 74
    new-instance v7, Lcom/squareup/util/ViewString$ResourceString;

    sget v8, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_tax:I

    invoke-direct {v7, v8}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v9, v7

    check-cast v9, Lcom/squareup/util/ViewString;

    .line 75
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/customreport/data/SalesSummary;->getTax()Lcom/squareup/protos/common/Money;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/squareup/salesreport/util/SalesSummariesKt$truncatedSalesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    move-object v8, v5

    .line 73
    invoke-direct/range {v8 .. v17}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v7, 0x4

    aput-object v5, v0, v7

    .line 79
    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->HIDE_TIPS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    const/4 v5, 0x0

    if-nez v1, :cond_0

    .line 80
    new-instance v1, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    .line 81
    new-instance v7, Lcom/squareup/util/ViewString$ResourceString;

    sget v8, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_tips:I

    invoke-direct {v7, v8}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v8, v7

    check-cast v8, Lcom/squareup/util/ViewString;

    .line 82
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/customreport/data/SalesSummary;->getTips()Lcom/squareup/protos/common/Money;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/squareup/salesreport/util/SalesSummariesKt$truncatedSalesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x70

    const/16 v16, 0x0

    move-object v7, v1

    .line 80
    invoke-direct/range {v7 .. v16}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    :cond_0
    move-object v1, v5

    :goto_0
    const/4 v7, 0x5

    aput-object v1, v0, v7

    const/4 v1, 0x6

    .line 89
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/settings/server/GiftCardSettings;->canUseGiftCards()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 90
    new-instance v4, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    .line 91
    new-instance v7, Lcom/squareup/util/ViewString$ResourceString;

    sget v8, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_gift_cards:I

    invoke-direct {v7, v8}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v8, v7

    check-cast v8, Lcom/squareup/util/ViewString;

    .line 92
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/customreport/data/SalesSummary;->getGiftCardSales()Lcom/squareup/protos/common/Money;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/squareup/salesreport/util/SalesSummariesKt$truncatedSalesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x70

    const/16 v16, 0x0

    move-object v7, v4

    .line 90
    invoke-direct/range {v7 .. v16}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_1

    :cond_1
    move-object v4, v5

    :goto_1
    aput-object v4, v0, v1

    const/4 v1, 0x7

    .line 99
    new-instance v4, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    .line 100
    new-instance v7, Lcom/squareup/util/ViewString$ResourceString;

    sget v8, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_refunds_by_amount:I

    invoke-direct {v7, v8}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v8, v7

    check-cast v8, Lcom/squareup/util/ViewString;

    .line 101
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/customreport/data/SalesSummary;->getCredit()Lcom/squareup/protos/common/Money;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/squareup/salesreport/util/SalesSummariesKt$truncatedSalesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x70

    const/16 v16, 0x0

    move-object v7, v4

    .line 99
    invoke-direct/range {v7 .. v16}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    aput-object v4, v0, v1

    const/16 v1, 0x8

    .line 105
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/customreport/data/SalesSummary;->getCashRounding()Lcom/squareup/protos/common/Money;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, v4, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    if-eqz v4, :cond_2

    invoke-static {v4}, Lcom/squareup/currency/CurrencyCodesKt;->getDoesSwedishRounding(Lcom/squareup/protos/common/CurrencyCode;)Z

    move-result v4

    if-ne v4, v6, :cond_2

    .line 106
    new-instance v4, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    .line 107
    new-instance v5, Lcom/squareup/util/ViewString$ResourceString;

    sget v6, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_cash_rounding:I

    invoke-direct {v5, v6}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v8, v5

    check-cast v8, Lcom/squareup/util/ViewString;

    .line 108
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/customreport/data/SalesSummary;->getCashRounding()Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/squareup/salesreport/util/SalesSummariesKt$truncatedSalesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x70

    const/16 v16, 0x0

    move-object v7, v4

    .line 106
    invoke-direct/range {v7 .. v16}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_2

    :cond_2
    move-object v4, v5

    :goto_2
    aput-object v4, v0, v1

    const/16 v1, 0x9

    .line 115
    new-instance v13, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    .line 116
    new-instance v4, Lcom/squareup/util/ViewString$ResourceString;

    sget v5, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_total:I

    invoke-direct {v4, v5}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v4, Lcom/squareup/util/ViewString;

    .line 117
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/customreport/data/SalesSummary;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/salesreport/util/SalesSummariesKt$truncatedSalesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x70

    const/4 v12, 0x0

    move-object v3, v13

    .line 115
    invoke-direct/range {v3 .. v12}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    aput-object v13, v0, v1

    .line 48
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOfNotNull([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
