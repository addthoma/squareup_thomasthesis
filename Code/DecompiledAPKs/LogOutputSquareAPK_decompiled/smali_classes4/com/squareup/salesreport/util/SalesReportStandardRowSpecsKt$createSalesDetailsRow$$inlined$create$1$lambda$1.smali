.class public final Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "TS;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator$bind$1\n+ 2 SalesReportStandardRowSpecs.kt\ncom/squareup/salesreport/util/SalesReportStandardRowSpecsKt\n*L\n1#1,87:1\n453#2,9:88\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0004\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0005\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "<anonymous parameter 0>",
        "",
        "item",
        "invoke",
        "(ILjava/lang/Object;)V",
        "com/squareup/cycler/StandardRowSpec$Creator$bind$1",
        "com/squareup/salesreport/util/SalesReportStandardRowSpecsKt$$special$$inlined$bind$8"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $glyph$inlined:Lcom/squareup/glyph/SquareGlyphView;

.field final synthetic $money$inlined:Landroid/widget/TextView;

.field final synthetic $name$inlined:Landroid/widget/TextView;

.field final synthetic $subValue$inlined:Landroid/widget/TextView;

.field final synthetic $this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

.field final synthetic this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1;


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/squareup/glyph/SquareGlyphView;Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    iput-object p2, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;->$subValue$inlined:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;->$name$inlined:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;->$money$inlined:Landroid/widget/TextView;

    iput-object p5, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;->$glyph$inlined:Lcom/squareup/glyph/SquareGlyphView;

    iput-object p6, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;->invoke(ILjava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;)V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    check-cast p2, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;

    .line 88
    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;->getBold()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 89
    :goto_0
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;->$subValue$inlined:Landroid/widget/TextView;

    const-string v1, "subValue"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;->getSubValue()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;->$subValue$inlined:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/marketfont/MarketUtils;->setTextViewTypeface(Landroid/widget/TextView;Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;->$subValue$inlined:Landroid/widget/TextView;

    .line 92
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1;

    iget-object v0, v0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1;->$resources$inlined:Landroid/content/res/Resources;

    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;->getSubValueColor()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1

    :cond_1
    move-object v1, p2

    check-cast v1, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;

    invoke-static {v1}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->access$getTextColor$p(Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;)I

    move-result v1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 91
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 95
    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1;

    iget-object v0, p1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1;->$resources$inlined:Landroid/content/res/Resources;

    move-object v1, p2

    check-cast v1, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;

    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;->$name$inlined:Landroid/widget/TextView;

    const-string p1, "name"

    invoke-static {v3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;->$money$inlined:Landroid/widget/TextView;

    const-string p1, "money"

    invoke-static {v4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1$lambda$1;->$glyph$inlined:Lcom/squareup/glyph/SquareGlyphView;

    const-string p1, "glyph"

    invoke-static {v5, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static/range {v0 .. v5}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->access$bindSalesDetailRow(Landroid/content/res/Resources;Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/squareup/glyph/SquareGlyphView;)V

    return-void
.end method
