.class public final Lcom/squareup/salesreport/util/RealBasicReportConfigPolicy;
.super Ljava/lang/Object;
.source "BasicReportConfigPolicy.kt"

# interfaces
.implements Lcom/squareup/salesreport/util/BasicReportConfigPolicy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/util/RealBasicReportConfigPolicy$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \t2\u00020\u0001:\u0001\tB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/salesreport/util/RealBasicReportConfigPolicy;",
        "Lcom/squareup/salesreport/util/BasicReportConfigPolicy;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "(Lcom/squareup/time/CurrentTime;)V",
        "earliestSelectableReportDate",
        "Lorg/threeten/bp/LocalDate;",
        "getEarliestSelectableReportDate",
        "()Lorg/threeten/bp/LocalDate;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/salesreport/util/RealBasicReportConfigPolicy$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MAXIMUM_NUMBERS_OF_DAY_BACK_BASIC_MODE:J = 0x5aL


# instance fields
.field private final currentTime:Lcom/squareup/time/CurrentTime;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/salesreport/util/RealBasicReportConfigPolicy$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/salesreport/util/RealBasicReportConfigPolicy$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/salesreport/util/RealBasicReportConfigPolicy;->Companion:Lcom/squareup/salesreport/util/RealBasicReportConfigPolicy$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/time/CurrentTime;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "currentTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/util/RealBasicReportConfigPolicy;->currentTime:Lcom/squareup/time/CurrentTime;

    return-void
.end method


# virtual methods
.method public getEarliestSelectableReportDate()Lorg/threeten/bp/LocalDate;
    .locals 3

    .line 21
    iget-object v0, p0, Lcom/squareup/salesreport/util/RealBasicReportConfigPolicy;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v0}, Lcom/squareup/time/CurrentTime;->localDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-wide/16 v1, 0x5a

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/LocalDate;->minusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-string v1, "currentTime.localDate().\u2026S_OF_DAY_BACK_BASIC_MODE)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
