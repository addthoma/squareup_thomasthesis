.class public final Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;
.super Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;
.source "SalesReportRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TruncatedSalesDetailsRow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0016\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001BT\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u0007\u0012\u0019\u0008\u0002\u0010\u000b\u001a\u0013\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000c\u00a2\u0006\u0002\u0008\u000f\u00a2\u0006\u0002\u0010\u0010J\t\u0010\u001c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0007H\u00c6\u0003J\t\u0010 \u001a\u00020\u0007H\u00c6\u0003J\t\u0010!\u001a\u00020\u0007H\u00c6\u0003J\u001a\u0010\"\u001a\u0013\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000c\u00a2\u0006\u0002\u0008\u000fH\u00c6\u0003J`\u0010#\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00072\u0008\u0008\u0002\u0010\t\u001a\u00020\u00072\u0008\u0008\u0002\u0010\n\u001a\u00020\u00072\u0019\u0008\u0002\u0010\u000b\u001a\u0013\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000c\u00a2\u0006\u0002\u0008\u000fH\u00c6\u0001J\u0013\u0010$\u001a\u00020\u00072\u0008\u0010%\u001a\u0004\u0018\u00010&H\u00d6\u0003J\t\u0010\'\u001a\u00020(H\u00d6\u0001J\t\u0010)\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R%\u0010\u000b\u001a\u0013\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000c\u00a2\u0006\u0002\u0008\u000fX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0014\u0010\t\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0012R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0014\u0010\n\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0012R\u0014\u0010\u0008\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0012\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;",
        "name",
        "Lcom/squareup/util/ViewString;",
        "money",
        "",
        "bold",
        "",
        "subRow",
        "hasSubRows",
        "showingSubRows",
        "clickHandler",
        "Lkotlin/Function1;",
        "Landroid/view/View;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;)V",
        "getBold",
        "()Z",
        "getClickHandler",
        "()Lkotlin/jvm/functions/Function1;",
        "getHasSubRows",
        "getMoney",
        "()Ljava/lang/String;",
        "getName",
        "()Lcom/squareup/util/ViewString;",
        "getShowingSubRows",
        "getSubRow",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bold:Z

.field private final clickHandler:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final hasSubRows:Z

.field private final money:Ljava/lang/String;

.field private final name:Lcom/squareup/util/ViewString;

.field private final showingSubRows:Z

.field private final subRow:Z


# direct methods
.method public constructor <init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ViewString;",
            "Ljava/lang/String;",
            "ZZZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    move-object v9, p0

    move-object v10, p1

    move-object v11, p2

    move-object/from16 v12, p7

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "money"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clickHandler"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    .line 85
    invoke-direct/range {v0 .. v8}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v10, v9, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->name:Lcom/squareup/util/ViewString;

    iput-object v11, v9, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->money:Ljava/lang/String;

    move/from16 v0, p3

    iput-boolean v0, v9, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->bold:Z

    move/from16 v0, p4

    iput-boolean v0, v9, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->subRow:Z

    move/from16 v0, p5

    iput-boolean v0, v9, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->hasSubRows:Z

    move/from16 v0, p6

    iput-boolean v0, v9, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->showingSubRows:Z

    iput-object v12, v9, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->clickHandler:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 10

    and-int/lit8 v0, p8, 0x10

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v7, 0x0

    goto :goto_0

    :cond_0
    move v7, p5

    :goto_0
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_1

    const/4 v8, 0x0

    goto :goto_1

    :cond_1
    move/from16 v8, p6

    :goto_1
    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_2

    .line 84
    sget-object v0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow$1;->INSTANCE:Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    move-object v9, v0

    goto :goto_2

    :cond_2
    move-object/from16 v9, p7

    :goto_2
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v2 .. v9}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getName()Lcom/squareup/util/ViewString;

    move-result-object p1

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getMoney()Ljava/lang/String;

    move-result-object p2

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getBold()Z

    move-result p3

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getSubRow()Z

    move-result p4

    :cond_3
    move v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getHasSubRows()Z

    move-result p5

    :cond_4
    move v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getShowingSubRows()Z

    move-result p6

    :cond_5
    move v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getClickHandler()Lkotlin/jvm/functions/Function1;

    move-result-object p7

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move p5, v0

    move p6, v1

    move p7, v2

    move p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->copy(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;)Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/util/ViewString;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getName()Lcom/squareup/util/ViewString;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getMoney()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getBold()Z

    move-result v0

    return v0
.end method

.method public final component4()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getSubRow()Z

    move-result v0

    return v0
.end method

.method public final component5()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getHasSubRows()Z

    move-result v0

    return v0
.end method

.method public final component6()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getShowingSubRows()Z

    move-result v0

    return v0
.end method

.method public final component7()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getClickHandler()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;)Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ViewString;",
            "Ljava/lang/String;",
            "ZZZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;"
        }
    .end annotation

    const-string v0, "name"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "money"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clickHandler"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    move-object v1, v0

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getName()Lcom/squareup/util/ViewString;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getName()Lcom/squareup/util/ViewString;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getMoney()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getMoney()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getBold()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getBold()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getSubRow()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getSubRow()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getHasSubRows()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getHasSubRows()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getShowingSubRows()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getShowingSubRows()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getClickHandler()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getClickHandler()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getBold()Z
    .locals 1

    .line 80
    iget-boolean v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->bold:Z

    return v0
.end method

.method public getClickHandler()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->clickHandler:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public getHasSubRows()Z
    .locals 1

    .line 82
    iget-boolean v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->hasSubRows:Z

    return v0
.end method

.method public getMoney()Ljava/lang/String;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->money:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Lcom/squareup/util/ViewString;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->name:Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public getShowingSubRows()Z
    .locals 1

    .line 83
    iget-boolean v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->showingSubRows:Z

    return v0
.end method

.method public getSubRow()Z
    .locals 1

    .line 81
    iget-boolean v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->subRow:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getName()Lcom/squareup/util/ViewString;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getMoney()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getBold()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getSubRow()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getHasSubRows()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getShowingSubRows()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getClickHandler()Lkotlin/jvm/functions/Function1;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TruncatedSalesDetailsRow(name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getName()Lcom/squareup/util/ViewString;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getMoney()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", bold="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getBold()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", subRow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getSubRow()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", hasSubRows="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getHasSubRows()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showingSubRows="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getShowingSubRows()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", clickHandler="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;->getClickHandler()Lkotlin/jvm/functions/Function1;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
