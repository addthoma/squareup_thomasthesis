.class Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;
.super Ljava/lang/Object;
.source "SharedPreferencesBundleStore.java"

# interfaces
.implements Lcom/squareup/persistentbundle/PersistentBundleStore;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/persistentbundle/SharedPreferencesBundleStore$PersistentBundleNotFoundException;
    }
.end annotation


# static fields
.field static final BUNDLE_FILE:Ljava/lang/String; = "persistent-bundles"

.field private static final KEY_BUNDLE_IDS:Ljava/lang/String; = "bundle-ids"

.field private static final KEY_VERSION_CODE:Ljava/lang/String; = "version-code"

.field private static final MAX_STORED_BUNDLE_COUNT:I = 0x3

.field private static final SEPARATOR:Ljava/lang/String; = ";"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final appContext:Landroid/app/Application;

.field private final bundlePreferences:Landroid/content/SharedPreferences;

.field private final clock:Lcom/squareup/util/Clock;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final fileThreadExecutor:Ljava/util/concurrent/Executor;

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;


# direct methods
.method constructor <init>(Landroid/app/Application;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/analytics/Analytics;Ljava/util/concurrent/Executor;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Clock;I)V
    .locals 0
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->appContext:Landroid/app/Application;

    .line 72
    iput-object p2, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 73
    iput-object p3, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->analytics:Lcom/squareup/analytics/Analytics;

    .line 74
    iput-object p4, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    .line 75
    iput-object p5, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->features:Lcom/squareup/settings/server/Features;

    .line 76
    iput-object p6, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->clock:Lcom/squareup/util/Clock;

    .line 77
    iget-object p1, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->appContext:Landroid/app/Application;

    const-string p2, "persistent-bundles"

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/app/Application;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->bundlePreferences:Landroid/content/SharedPreferences;

    .line 78
    invoke-direct {p0, p7}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->checkForUpgrade(I)V

    return-void
.end method

.method private checkForUpgrade(I)V
    .locals 3

    .line 177
    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->bundlePreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "version-code"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 179
    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->bundlePreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 180
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 181
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 182
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method private getIdsString(Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 173
    invoke-interface {p1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object p1

    const-string v0, ";"

    invoke-static {p1, v0}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getOrderedBundleIds()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 164
    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->bundlePreferences:Landroid/content/SharedPreferences;

    const-string v1, "bundle-ids"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 165
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0

    .line 168
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method private varargs logDurationAndMessage(Lcom/squareup/log/OhSnapLogger$EventType;JLjava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .line 188
    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, p2

    .line 189
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p4, p5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p4

    const/4 p5, 0x0

    aput-object p4, p3, p5

    const-string p4, " [Took %d ms]"

    invoke-static {p4, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 190
    iget-object p3, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    invoke-interface {p3, p1, p2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void
.end method

.method private logTimingEvent(Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;JI)V
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, p2

    .line 195
    iget-object p2, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance p3, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent;

    invoke-direct {p3, p1, v0, v1, p4}, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent;-><init>(Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;JI)V

    invoke-interface {p2, p3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;[B)V
    .locals 9

    .line 100
    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v4

    .line 101
    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BUNDLE_LOGGING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v7

    .line 103
    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->ON_SAVE:Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    array-length v3, p2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v6, 0x0

    aput-object v3, v2, v6

    const-string v3, "Saving %d byte Bundle"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 104
    sget-object v0, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;->START:Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    array-length v1, p2

    invoke-direct {p0, v0, v4, v5, v1}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->logTimingEvent(Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;JI)V

    .line 106
    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v8, Lcom/squareup/persistentbundle/-$$Lambda$SharedPreferencesBundleStore$86_y1Xs_35iTRZdWpO9MDHg93Qg;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p2

    move-object v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/persistentbundle/-$$Lambda$SharedPreferencesBundleStore$86_y1Xs_35iTRZdWpO9MDHg93Qg;-><init>(Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;[BJLjava/lang/String;Z)V

    invoke-interface {v0, v8}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public contains(Ljava/lang/String;)Z
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->bundlePreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public getAndRemove(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 8

    .line 82
    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v3

    .line 84
    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->bundlePreferences:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 87
    iget-object p1, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->CREATE:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v1, "Persistent Bundle was lost, return empty Bundle."

    invoke-interface {p1, v0, v1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 88
    new-instance p1, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore$PersistentBundleNotFoundException;

    invoke-direct {p1}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore$PersistentBundleNotFoundException;-><init>()V

    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 89
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    return-object p1

    :cond_0
    const/4 v1, 0x0

    .line 91
    invoke-static {v0, v1}, Lcom/squareup/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 92
    invoke-static {v0}, Lcom/squareup/persistentbundle/PersistentBundleUtil;->deserializeBundle([B)Landroid/os/Bundle;

    move-result-object v7

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->remove(Ljava/lang/String;)V

    .line 95
    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->RESUME:Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 p1, 0x1

    new-array v6, p1, [Ljava/lang/Object;

    array-length p1, v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, v1

    const-string v5, "Retrieved %d byte Bundle."

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->logDurationAndMessage(Lcom/squareup/log/OhSnapLogger$EventType;JLjava/lang/String;[Ljava/lang/Object;)V

    return-object v7
.end method

.method public synthetic lambda$add$0$SharedPreferencesBundleStore([BJLjava/lang/String;Z)V
    .locals 7

    const/4 v0, 0x0

    .line 109
    :try_start_0
    invoke-static {p1, v0}, Lcom/squareup/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    sget-object v2, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;->SERIALIZE:Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    array-length v3, p1

    invoke-direct {p0, v2, p2, p3, v3}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->logTimingEvent(Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;JI)V

    .line 117
    iget-object v2, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->bundlePreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 120
    invoke-direct {p0}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->getOrderedBundleIds()Ljava/util/List;

    move-result-object v3

    .line 121
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x3

    if-lt v4, v5, :cond_0

    .line 123
    iget-object v4, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v5, Lcom/squareup/log/OhSnapLogger$EventType;->ON_SAVE:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v6, "Persistent bundle was dropped"

    invoke-interface {v4, v5, v6}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 124
    invoke-interface {v3, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v2, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 126
    :cond_0
    invoke-interface {v3, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    invoke-interface {v2, p4, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p4

    .line 130
    invoke-direct {p0, v3}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->getIdsString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "bundle-ids"

    invoke-interface {p4, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p4

    .line 131
    invoke-interface {p4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 133
    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->ON_SAVE:Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 p4, 0x1

    new-array v6, p4, [Ljava/lang/Object;

    array-length p4, p1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    aput-object p4, v6, v0

    const-string v5, "Saved %d byte Bundle."

    move-object v1, p0

    move-wide v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->logDurationAndMessage(Lcom/squareup/log/OhSnapLogger$EventType;JLjava/lang/String;[Ljava/lang/Object;)V

    .line 134
    sget-object p4, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;->PERSIST:Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    array-length v0, p1

    invoke-direct {p0, p4, p2, p3, v0}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->logTimingEvent(Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;JI)V

    if-eqz p5, :cond_1

    .line 137
    iget-object p2, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object p3, Lcom/squareup/log/OhSnapLogger$EventType;->ON_SAVE:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-static {p1}, Lcom/squareup/persistentbundle/PersistentBundleUtil;->deserializeBundle([B)Landroid/os/Bundle;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p3, p1}, Lcom/squareup/log/OhSnapLogger;->logFull(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    :catch_0
    :cond_1
    return-void
.end method

.method public logBundleSize(JZ)V
    .locals 4

    if-eqz p3, :cond_0

    const-string p3, "saved"

    goto :goto_0

    :cond_0
    const-string p3, "not saved"

    .line 159
    :goto_0
    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->ON_SAVE:Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 160
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    aput-object p1, v2, v3

    const/4 p1, 0x1

    aput-object p3, v2, p1

    const-string p1, "Bundle was %s bytes and was %s to disk."

    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 159
    invoke-interface {v0, v1, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void
.end method

.method public remove(Ljava/lang/String;)V
    .locals 2

    .line 147
    invoke-virtual {p0, p1}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    invoke-direct {p0}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->getOrderedBundleIds()Ljava/util/List;

    move-result-object v0

    .line 149
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 150
    iget-object v1, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->bundlePreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 151
    invoke-interface {v1, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 152
    invoke-direct {p0, v0}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;->getIdsString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "bundle-ids"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 153
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method
