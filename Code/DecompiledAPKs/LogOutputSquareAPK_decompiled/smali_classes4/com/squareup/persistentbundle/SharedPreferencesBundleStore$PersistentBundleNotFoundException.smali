.class Lcom/squareup/persistentbundle/SharedPreferencesBundleStore$PersistentBundleNotFoundException;
.super Ljava/lang/RuntimeException;
.source "SharedPreferencesBundleStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PersistentBundleNotFoundException"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    const-string v0, "Persistent bundle was not found."

    .line 200
    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    return-void
.end method
