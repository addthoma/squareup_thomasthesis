.class Lcom/squareup/persistentbundle/PersistentBundleHolder;
.super Ljava/lang/Object;
.source "PersistentBundleHolder.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field static final BUNDLE_THRESHOLD_SIZE_BYTES:I = 0x32000

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/persistentbundle/PersistentBundleHolder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private isPersisted:Z

.field private persistentBundleStore:Lcom/squareup/persistentbundle/PersistentBundleStore;

.field private savedState:Landroid/os/Bundle;

.field private sizeInBytes:I

.field private final uuid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 112
    new-instance v0, Lcom/squareup/persistentbundle/PersistentBundleHolder$1;

    invoke-direct {v0}, Lcom/squareup/persistentbundle/PersistentBundleHolder$1;-><init>()V

    sput-object v0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->uuid:Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->sizeInBytes:I

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->isPersisted:Z

    .line 55
    iget-boolean v0, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->isPersisted:Z

    if-nez v0, :cond_1

    .line 56
    iget v0, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->sizeInBytes:I

    new-array v0, v0, [B

    .line 57
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 58
    invoke-static {v0}, Lcom/squareup/persistentbundle/PersistentBundleUtil;->deserializeBundle([B)Landroid/os/Bundle;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->savedState:Landroid/os/Bundle;

    :cond_1
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/squareup/persistentbundle/PersistentBundleHolder$1;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/squareup/persistentbundle/PersistentBundleHolder;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/squareup/persistentbundle/PersistentBundleStore;)V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->uuid:Ljava/lang/String;

    const/4 p1, 0x0

    .line 41
    iput-boolean p1, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->isPersisted:Z

    .line 42
    iput-object p2, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->savedState:Landroid/os/Bundle;

    .line 43
    iput-object p3, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->persistentBundleStore:Lcom/squareup/persistentbundle/PersistentBundleStore;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getSavedState()Landroid/os/Bundle;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->savedState:Landroid/os/Bundle;

    return-object v0
.end method

.method public getUuid()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->uuid:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .line 90
    iget-object p2, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->persistentBundleStore:Lcom/squareup/persistentbundle/PersistentBundleStore;

    const-string v0, "Must set a PersistentBundleStore."

    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 91
    iget-object p2, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->uuid:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 93
    iget-object p2, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->savedState:Landroid/os/Bundle;

    invoke-static {p2}, Lcom/squareup/persistentbundle/PersistentBundleUtil;->serializeBundle(Landroid/os/Bundle;)[B

    move-result-object p2

    .line 94
    array-length v0, p2

    iput v0, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->sizeInBytes:I

    .line 95
    iget v0, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->sizeInBytes:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 97
    iget v0, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->sizeInBytes:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const v3, 0x32000

    if-le v0, v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 99
    iput-boolean v1, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->isPersisted:Z

    .line 100
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 102
    iget-object p1, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->persistentBundleStore:Lcom/squareup/persistentbundle/PersistentBundleStore;

    iget-object v1, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->uuid:Ljava/lang/String;

    invoke-interface {p1, v1, p2}, Lcom/squareup/persistentbundle/PersistentBundleStore;->add(Ljava/lang/String;[B)V

    goto :goto_1

    .line 104
    :cond_1
    iput-boolean v2, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->isPersisted:Z

    .line 105
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 106
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 109
    :goto_1
    iget-object p1, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->persistentBundleStore:Lcom/squareup/persistentbundle/PersistentBundleStore;

    iget p2, p0, Lcom/squareup/persistentbundle/PersistentBundleHolder;->sizeInBytes:I

    int-to-long v1, p2

    invoke-interface {p1, v1, v2, v0}, Lcom/squareup/persistentbundle/PersistentBundleStore;->logBundleSize(JZ)V

    return-void
.end method
