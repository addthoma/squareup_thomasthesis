.class public final Lcom/squareup/quantity/PerUnitFormatter;
.super Ljava/lang/Object;
.source "PerUnitFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPerUnitFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PerUnitFormatter.kt\ncom/squareup/quantity/PerUnitFormatter\n*L\n1#1,404:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\r\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B#\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\u0008J\u0006\u0010\u0016\u001a\u00020\u0000J\u0006\u0010\u0017\u001a\u00020\u0018J\u001c\u0010\u0017\u001a\u00020\u00182\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u00042\u0008\u0008\u0002\u0010\u0019\u001a\u00020\rH\u0007J\u0008\u0010\u001a\u001a\u00020\rH\u0002J\u0008\u0010\u001b\u001a\u00020\rH\u0002J\u0006\u0010\u001c\u001a\u00020\u0000J\u0010\u0010\u000b\u001a\u00020\u00002\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\u000e\u0010\u001d\u001a\u00020\u00002\u0006\u0010\u0019\u001a\u00020\rJ\u0010\u0010\u000e\u001a\u00020\u00002\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fJ\u0018\u0010\u001e\u001a\u00020\u00002\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0012\u001a\u00020\u0013J\u0008\u0010\u001f\u001a\u00020 H\u0002J\u001c\u0010!\u001a\u0004\u0018\u00010\r2\u0008\u0010\"\u001a\u0004\u0018\u00010\r2\u0008\u0010#\u001a\u0004\u0018\u00010\rJ\u000e\u0010$\u001a\u00020\u00002\u0006\u0010\u0019\u001a\u00020\rJ\u000e\u0010%\u001a\u00020\r2\u0006\u0010#\u001a\u00020\rR\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "(Lcom/squareup/text/Formatter;Ljavax/inject/Provider;)V",
        "isSuffix",
        "",
        "money",
        "moneyUnitAbbreviation",
        "",
        "quantity",
        "Ljava/math/BigDecimal;",
        "quantityFormatter",
        "Lcom/squareup/quantity/BigDecimalFormatter;",
        "quantityPrecision",
        "",
        "quantityUnitAbbreviation",
        "useParentheses",
        "asSuffix",
        "format",
        "",
        "localizedUnitAbbreviation",
        "formattedMoney",
        "formattedQuantity",
        "inParentheses",
        "moneyUnit",
        "quantityAndPrecision",
        "resetFormatter",
        "",
        "trimUnitSuffix",
        "original",
        "unitAbbreviation",
        "unit",
        "unitSuffix",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private isSuffix:Z

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private money:Lcom/squareup/protos/common/Money;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private moneyUnitAbbreviation:Ljava/lang/String;

.field private quantity:Ljava/math/BigDecimal;

.field private quantityFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

.field private quantityPrecision:I

.field private quantityUnitAbbreviation:Ljava/lang/String;

.field private useParentheses:Z


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Ljavax/inject/Provider;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/quantity/PerUnitFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/quantity/PerUnitFormatter;->localeProvider:Ljavax/inject/Provider;

    const-string p1, ""

    .line 38
    iput-object p1, p0, Lcom/squareup/quantity/PerUnitFormatter;->moneyUnitAbbreviation:Ljava/lang/String;

    .line 39
    iput-object p1, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantityUnitAbbreviation:Ljava/lang/String;

    .line 45
    new-instance p1, Lcom/squareup/quantity/BigDecimalFormatter;

    iget-object v1, p0, Lcom/squareup/quantity/PerUnitFormatter;->localeProvider:Ljavax/inject/Provider;

    sget-object v2, Lcom/squareup/quantity/BigDecimalFormatter$Format;->ROUNDING_SCALE:Lcom/squareup/quantity/BigDecimalFormatter$Format;

    iget v3, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantityPrecision:I

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/quantity/BigDecimalFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/quantity/BigDecimalFormatter$Format;ILjava/math/RoundingMode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantityFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    return-void
.end method

.method public static synthetic format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const-string p2, ""

    .line 71
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method private final formattedMoney()Ljava/lang/String;
    .locals 3

    .line 360
    iget-object v0, p0, Lcom/squareup/quantity/PerUnitFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/quantity/PerUnitFormatter;->money:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 361
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 363
    iget-object v1, p0, Lcom/squareup/quantity/PerUnitFormatter;->moneyUnitAbbreviation:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 364
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/quantity/PerUnitFormatter;->moneyUnitAbbreviation:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/quantity/PerUnitFormatter;->unitSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 367
    :cond_0
    iget-boolean v1, p0, Lcom/squareup/quantity/PerUnitFormatter;->useParentheses:Z

    if-eqz v1, :cond_1

    .line 368
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x28

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x29

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method private final formattedQuantity()Ljava/lang/String;
    .locals 3

    .line 377
    iget-object v0, p0, Lcom/squareup/quantity/PerUnitFormatter;->money:Lcom/squareup/protos/common/Money;

    const-string v1, ""

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/quantity/PerUnitFormatter;->isSuffix:Z

    if-eqz v0, :cond_1

    .line 378
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " \u00d7\u00a0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 381
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantityFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    iget-object v2, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v1, v2}, Lcom/squareup/quantity/BigDecimalFormatter;->format(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 383
    iget-object v1, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantityUnitAbbreviation:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 384
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0xa0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantityUnitAbbreviation:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    return-object v0
.end method

.method private final resetFormatter()V
    .locals 9

    const/4 v0, 0x0

    .line 391
    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/common/Money;

    iput-object v1, p0, Lcom/squareup/quantity/PerUnitFormatter;->money:Lcom/squareup/protos/common/Money;

    .line 392
    check-cast v0, Ljava/math/BigDecimal;

    iput-object v0, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantity:Ljava/math/BigDecimal;

    const/4 v0, 0x0

    .line 393
    iput v0, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantityPrecision:I

    const-string v1, ""

    .line 395
    iput-object v1, p0, Lcom/squareup/quantity/PerUnitFormatter;->moneyUnitAbbreviation:Ljava/lang/String;

    .line 396
    iput-object v1, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantityUnitAbbreviation:Ljava/lang/String;

    .line 398
    iput-boolean v0, p0, Lcom/squareup/quantity/PerUnitFormatter;->useParentheses:Z

    .line 399
    iput-boolean v0, p0, Lcom/squareup/quantity/PerUnitFormatter;->isSuffix:Z

    .line 401
    new-instance v0, Lcom/squareup/quantity/BigDecimalFormatter;

    iget-object v3, p0, Lcom/squareup/quantity/PerUnitFormatter;->localeProvider:Ljavax/inject/Provider;

    sget-object v4, Lcom/squareup/quantity/BigDecimalFormatter$Format;->ROUNDING_SCALE:Lcom/squareup/quantity/BigDecimalFormatter$Format;

    iget v5, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantityPrecision:I

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/squareup/quantity/BigDecimalFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/quantity/BigDecimalFormatter$Format;ILjava/math/RoundingMode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantityFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    return-void
.end method


# virtual methods
.method public final asSuffix()Lcom/squareup/quantity/PerUnitFormatter;
    .locals 1

    const/4 v0, 0x1

    .line 355
    iput-boolean v0, p0, Lcom/squareup/quantity/PerUnitFormatter;->isSuffix:Z

    return-object p0
.end method

.method public final format()Ljava/lang/CharSequence;
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/squareup/quantity/PerUnitFormatter;->money:Lcom/squareup/protos/common/Money;

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/squareup/quantity/PerUnitFormatter;->formattedMoney()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantity:Ljava/math/BigDecimal;

    if-eqz v0, :cond_1

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/squareup/quantity/PerUnitFormatter;->formattedQuantity()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 164
    :cond_1
    invoke-direct {p0}, Lcom/squareup/quantity/PerUnitFormatter;->resetFormatter()V

    .line 166
    check-cast v1, Ljava/lang/CharSequence;

    return-object v1
.end method

.method public final format(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p0, p1, v0, v1, v0}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public final format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 1

    const-string v0, "localizedUnitAbbreviation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string p1, ""

    .line 74
    check-cast p1, Ljava/lang/CharSequence;

    return-object p1

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/squareup/quantity/PerUnitFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 78
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 80
    move-object v0, p2

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p2}, Lcom/squareup/quantity/PerUnitFormatter;->unitSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 84
    :cond_1
    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method public final inParentheses()Lcom/squareup/quantity/PerUnitFormatter;
    .locals 1

    const/4 v0, 0x1

    .line 337
    iput-boolean v0, p0, Lcom/squareup/quantity/PerUnitFormatter;->useParentheses:Z

    return-object p0
.end method

.method public final money(Lcom/squareup/protos/common/Money;)Lcom/squareup/quantity/PerUnitFormatter;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/quantity/PerUnitFormatter;->money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public final moneyUnit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;
    .locals 1

    const-string v0, "localizedUnitAbbreviation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 317
    iput-object p1, p0, Lcom/squareup/quantity/PerUnitFormatter;->moneyUnitAbbreviation:Ljava/lang/String;

    const-string p1, ""

    .line 318
    iput-object p1, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantityUnitAbbreviation:Ljava/lang/String;

    return-object p0
.end method

.method public final quantity(Ljava/math/BigDecimal;)Lcom/squareup/quantity/PerUnitFormatter;
    .locals 1

    if-eqz p1, :cond_0

    .line 218
    invoke-static {p1}, Lcom/squareup/util/BigDecimals;->decimalPart(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/squareup/quantity/PerUnitFormatter;->quantityAndPrecision(Ljava/math/BigDecimal;I)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    return-object p1
.end method

.method public final quantityAndPrecision(Ljava/math/BigDecimal;I)Lcom/squareup/quantity/PerUnitFormatter;
    .locals 7

    .line 261
    invoke-static {}, Lcom/squareup/quantity/ItemQuantities;->getALLOWED_QUANTITY_PRECISION_RANGE()Lkotlin/ranges/IntRange;

    move-result-object v0

    invoke-virtual {v0, p2}, Lkotlin/ranges/IntRange;->contains(I)Z

    move-result v0

    .line 262
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Quantity precision must be within "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/squareup/quantity/ItemQuantities;->getALLOWED_QUANTITY_PRECISION_RANGE()Lkotlin/ranges/IntRange;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 260
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 266
    iput-object p1, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantity:Ljava/math/BigDecimal;

    .line 268
    iget p1, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantityPrecision:I

    if-ne p1, p2, :cond_0

    return-object p0

    .line 271
    :cond_0
    iput p2, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantityPrecision:I

    .line 272
    new-instance p1, Lcom/squareup/quantity/BigDecimalFormatter;

    iget-object v1, p0, Lcom/squareup/quantity/PerUnitFormatter;->localeProvider:Ljavax/inject/Provider;

    sget-object v2, Lcom/squareup/quantity/BigDecimalFormatter$Format;->ROUNDING_SCALE:Lcom/squareup/quantity/BigDecimalFormatter$Format;

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p1

    move v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/quantity/BigDecimalFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/quantity/BigDecimalFormatter$Format;ILjava/math/RoundingMode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantityFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    return-object p0
.end method

.method public final trimUnitSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 98
    move-object v0, p2

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    return-object p1

    .line 101
    :cond_2
    invoke-virtual {p0, p2}, Lcom/squareup/quantity/PerUnitFormatter;->unitSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    if-eqz p1, :cond_3

    const/4 v0, 0x2

    const/4 v2, 0x0

    .line 102
    invoke-static {p1, p2, v1, v0, v2}, Lkotlin/text/StringsKt;->endsWith$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 103
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    sub-int/2addr v0, p2

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    const-string p2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_3
    return-object p1
.end method

.method public final unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;
    .locals 1

    const-string v0, "localizedUnitAbbreviation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 293
    iput-object p1, p0, Lcom/squareup/quantity/PerUnitFormatter;->moneyUnitAbbreviation:Ljava/lang/String;

    .line 294
    iput-object p1, p0, Lcom/squareup/quantity/PerUnitFormatter;->quantityUnitAbbreviation:Ljava/lang/String;

    return-object p0
.end method

.method public final unitSuffix(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "unitAbbreviation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
