.class public final Lcom/squareup/quantity/UnitDisplayDataKt;
.super Ljava/lang/Object;
.source "UnitDisplayData.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "getUnitDisplayData",
        "Lcom/squareup/quantity/UnitDisplayData;",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "res",
        "Lcom/squareup/util/Res;",
        "proto-utilities_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getUnitDisplayData(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/util/Res;)Lcom/squareup/quantity/UnitDisplayData;
    .locals 4

    const-string v0, "$this$getUnitDisplayData"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    new-instance v0, Lcom/squareup/quantity/UnitDisplayData;

    .line 12
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const-string v2, "measurementUnit"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result p0

    .line 11
    invoke-direct {v0, v1, p1, p0}, Lcom/squareup/quantity/UnitDisplayData;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method
