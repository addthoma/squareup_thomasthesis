.class final Lcom/squareup/protos/connect/v2/RiskEvaluation$ProtoAdapter_RiskEvaluation;
.super Lcom/squareup/wire/ProtoAdapter;
.source "RiskEvaluation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/RiskEvaluation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_RiskEvaluation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/RiskEvaluation;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 208
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/RiskEvaluation;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/RiskEvaluation;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 231
    new-instance v0, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;-><init>()V

    .line 232
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 233
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 240
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 238
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->reasons:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 237
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->numeric_score(Ljava/lang/Double;)Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;

    goto :goto_0

    .line 236
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->level(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;

    goto :goto_0

    .line 235
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->status(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;

    goto :goto_0

    .line 244
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 245
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->build()Lcom/squareup/protos/connect/v2/RiskEvaluation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 206
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/RiskEvaluation$ProtoAdapter_RiskEvaluation;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/RiskEvaluation;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/RiskEvaluation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 222
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/RiskEvaluation;->status:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 223
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/RiskEvaluation;->level:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 224
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/RiskEvaluation;->numeric_score:Ljava/lang/Double;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 225
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/RiskEvaluation;->reasons:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 226
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/RiskEvaluation;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 206
    check-cast p2, Lcom/squareup/protos/connect/v2/RiskEvaluation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/RiskEvaluation$ProtoAdapter_RiskEvaluation;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/RiskEvaluation;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/RiskEvaluation;)I
    .locals 4

    .line 213
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/RiskEvaluation;->status:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/RiskEvaluation;->level:Ljava/lang/String;

    const/4 v3, 0x2

    .line 214
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/RiskEvaluation;->numeric_score:Ljava/lang/Double;

    const/4 v3, 0x3

    .line 215
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 216
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/RiskEvaluation;->reasons:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 217
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/RiskEvaluation;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 206
    check-cast p1, Lcom/squareup/protos/connect/v2/RiskEvaluation;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/RiskEvaluation$ProtoAdapter_RiskEvaluation;->encodedSize(Lcom/squareup/protos/connect/v2/RiskEvaluation;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/RiskEvaluation;)Lcom/squareup/protos/connect/v2/RiskEvaluation;
    .locals 0

    .line 250
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/RiskEvaluation;->newBuilder()Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;

    move-result-object p1

    .line 251
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 252
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->build()Lcom/squareup/protos/connect/v2/RiskEvaluation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 206
    check-cast p1, Lcom/squareup/protos/connect/v2/RiskEvaluation;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/RiskEvaluation$ProtoAdapter_RiskEvaluation;->redact(Lcom/squareup/protos/connect/v2/RiskEvaluation;)Lcom/squareup/protos/connect/v2/RiskEvaluation;

    move-result-object p1

    return-object p1
.end method
