.class public final Lcom/squareup/protos/connect/v2/Payment;
.super Lcom/squareup/wire/Message;
.source "Payment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/Payment$ProtoAdapter_Payment;,
        Lcom/squareup/protos/connect/v2/Payment$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/Payment;",
        "Lcom/squareup/protos/connect/v2/Payment$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/Payment;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACCOUNT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_BUYER_EMAIL_ADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_CREATED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_CUSTOMER_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_DELAYED_UNTIL:Ljava/lang/String; = ""

.field public static final DEFAULT_DELAY_ACTION:Ljava/lang/String; = ""

.field public static final DEFAULT_DELAY_DURATION:Ljava/lang/String; = ""

.field public static final DEFAULT_EMPLOYEE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_LOCATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_ORDER_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_RECEIPT_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_RECEIPT_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_REFERENCE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_SOURCE_TYPE:Ljava/lang/String; = ""

.field public static final DEFAULT_STATEMENT_DESCRIPTION_IDENTIFIER:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Ljava/lang/String; = ""

.field public static final DEFAULT_TERMINAL_CHECKOUT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_UPDATED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_VERSION_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final account_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x18
    .end annotation
.end field

.field public final amount_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final app_processing_fee:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.AppProcessingFee#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x24
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/AppProcessingFee;",
            ">;"
        }
    .end annotation
.end field

.field public final approved_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x23
    .end annotation
.end field

.field public final balance_details:Lcom/squareup/protos/connect/v2/BalancePaymentDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.BalancePaymentDetails#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final billing_address:Lcom/squareup/protos/connect/v2/resources/Address;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Address#ADAPTER"
        redacted = true
        tag = 0x1c
    .end annotation
.end field

.field public final buyer_email_address:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1b
    .end annotation
.end field

.field public final capabilities:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x26
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final card_details:Lcom/squareup/protos/connect/v2/CardPaymentDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.CardPaymentDetails#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.CashPaymentDetails#ADAPTER"
        tag = 0x27
    .end annotation
.end field

.field public final created_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final customer_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1f
    .end annotation
.end field

.field public final delay_action:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2b
    .end annotation
.end field

.field public final delay_duration:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final delayed_until:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2c
    .end annotation
.end field

.field public final dispute_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x13
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final employee_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x25
    .end annotation
.end field

.field public final external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.ExternalPaymentDetails#ADAPTER"
        tag = 0x28
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final location_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xf
    .end annotation
.end field

.field public final note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1e
    .end annotation
.end field

.field public final order_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x10
    .end annotation
.end field

.field public final payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.PaymentFeesSpec#ADAPTER"
        tag = 0x21
    .end annotation
.end field

.field public final processing_fee:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.ProcessingFee#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x19
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/ProcessingFee;",
            ">;"
        }
    .end annotation
.end field

.field public final receipt_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x29
    .end annotation
.end field

.field public final receipt_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2a
    .end annotation
.end field

.field public final reference_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x11
    .end annotation
.end field

.field public final refund_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x12
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final refunded_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final risk_evaluation:Lcom/squareup/protos/connect/v2/RiskEvaluation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.RiskEvaluation#ADAPTER"
        tag = 0x15
    .end annotation
.end field

.field public final shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Address#ADAPTER"
        redacted = true
        tag = 0x1d
    .end annotation
.end field

.field public final source_type:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final statement_description_identifier:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x22
    .end annotation
.end field

.field public final status:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final tax_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x1a
    .end annotation
.end field

.field public final terminal_checkout_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x17
    .end annotation
.end field

.field public final tip_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final total_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x20
    .end annotation
.end field

.field public final updated_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3f2
    .end annotation
.end field

.field public final version_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3e8
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 29
    new-instance v0, Lcom/squareup/protos/connect/v2/Payment$ProtoAdapter_Payment;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/Payment$ProtoAdapter_Payment;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/Payment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 75
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/Payment;->DEFAULT_VERSION:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/Payment$Builder;Lokio/ByteString;)V
    .locals 1

    .line 622
    sget-object v0, Lcom/squareup/protos/connect/v2/Payment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 623
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->id:Ljava/lang/String;

    .line 624
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->created_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->created_at:Ljava/lang/String;

    .line 625
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->updated_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->updated_at:Ljava/lang/String;

    .line 626
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 627
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 628
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 629
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 630
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 631
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->approved_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->approved_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 632
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    .line 633
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->processing_fee:Ljava/util/List;

    const-string v0, "processing_fee"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->processing_fee:Ljava/util/List;

    .line 634
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->app_processing_fee:Ljava/util/List;

    const-string v0, "app_processing_fee"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->app_processing_fee:Ljava/util/List;

    .line 635
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->refunded_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->refunded_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 636
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->status:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->status:Ljava/lang/String;

    .line 637
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->delay_duration:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->delay_duration:Ljava/lang/String;

    .line 638
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->delay_action:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->delay_action:Ljava/lang/String;

    .line 639
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->delayed_until:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->delayed_until:Ljava/lang/String;

    .line 640
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->source_type:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->source_type:Ljava/lang/String;

    .line 641
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->card_details:Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->card_details:Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    .line 642
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->balance_details:Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->balance_details:Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    .line 643
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    .line 644
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    .line 645
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->location_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->location_id:Ljava/lang/String;

    .line 646
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->order_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->order_id:Ljava/lang/String;

    .line 647
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->reference_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->reference_id:Ljava/lang/String;

    .line 648
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->customer_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->customer_id:Ljava/lang/String;

    .line 649
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->employee_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->employee_id:Ljava/lang/String;

    .line 650
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->refund_ids:Ljava/util/List;

    const-string v0, "refund_ids"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->refund_ids:Ljava/util/List;

    .line 651
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->dispute_ids:Ljava/util/List;

    const-string v0, "dispute_ids"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->dispute_ids:Ljava/util/List;

    .line 652
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->risk_evaluation:Lcom/squareup/protos/connect/v2/RiskEvaluation;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->risk_evaluation:Lcom/squareup/protos/connect/v2/RiskEvaluation;

    .line 653
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->terminal_checkout_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->terminal_checkout_id:Ljava/lang/String;

    .line 654
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->account_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->account_id:Ljava/lang/String;

    .line 655
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->buyer_email_address:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->buyer_email_address:Ljava/lang/String;

    .line 656
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 657
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 658
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->note:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->note:Ljava/lang/String;

    .line 659
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->statement_description_identifier:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->statement_description_identifier:Ljava/lang/String;

    .line 660
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->capabilities:Ljava/util/List;

    const-string v0, "capabilities"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->capabilities:Ljava/util/List;

    .line 661
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->receipt_number:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->receipt_number:Ljava/lang/String;

    .line 662
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->receipt_url:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->receipt_url:Ljava/lang/String;

    .line 663
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->version_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/Payment;->version_token:Ljava/lang/String;

    .line 664
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/Payment$Builder;->version:Ljava/lang/Long;

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment;->version:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 719
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/Payment;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 720
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/Payment;

    .line 721
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/Payment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/Payment;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->id:Ljava/lang/String;

    .line 722
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->created_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->created_at:Ljava/lang/String;

    .line 723
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->updated_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->updated_at:Ljava/lang/String;

    .line 724
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 725
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 726
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 727
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 728
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 729
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->approved_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->approved_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 730
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    .line 731
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->processing_fee:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->processing_fee:Ljava/util/List;

    .line 732
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->app_processing_fee:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->app_processing_fee:Ljava/util/List;

    .line 733
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->refunded_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->refunded_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 734
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->status:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->status:Ljava/lang/String;

    .line 735
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->delay_duration:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->delay_duration:Ljava/lang/String;

    .line 736
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->delay_action:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->delay_action:Ljava/lang/String;

    .line 737
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->delayed_until:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->delayed_until:Ljava/lang/String;

    .line 738
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->source_type:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->source_type:Ljava/lang/String;

    .line 739
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->card_details:Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->card_details:Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    .line 740
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->balance_details:Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->balance_details:Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    .line 741
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    .line 742
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    .line 743
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->location_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->location_id:Ljava/lang/String;

    .line 744
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->order_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->order_id:Ljava/lang/String;

    .line 745
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->reference_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->reference_id:Ljava/lang/String;

    .line 746
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->customer_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->customer_id:Ljava/lang/String;

    .line 747
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->employee_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->employee_id:Ljava/lang/String;

    .line 748
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->refund_ids:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->refund_ids:Ljava/util/List;

    .line 749
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->dispute_ids:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->dispute_ids:Ljava/util/List;

    .line 750
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->risk_evaluation:Lcom/squareup/protos/connect/v2/RiskEvaluation;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->risk_evaluation:Lcom/squareup/protos/connect/v2/RiskEvaluation;

    .line 751
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->terminal_checkout_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->terminal_checkout_id:Ljava/lang/String;

    .line 752
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->account_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->account_id:Ljava/lang/String;

    .line 753
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->buyer_email_address:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->buyer_email_address:Ljava/lang/String;

    .line 754
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 755
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 756
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->note:Ljava/lang/String;

    .line 757
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->statement_description_identifier:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->statement_description_identifier:Ljava/lang/String;

    .line 758
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->capabilities:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->capabilities:Ljava/util/List;

    .line 759
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->receipt_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->receipt_number:Ljava/lang/String;

    .line 760
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->receipt_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->receipt_url:Ljava/lang/String;

    .line 761
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->version_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/Payment;->version_token:Ljava/lang/String;

    .line 762
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->version:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/Payment;->version:Ljava/lang/Long;

    .line 763
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 768
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_25

    .line 770
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/Payment;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 771
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 772
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 773
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->updated_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 774
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 775
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 776
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 777
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 778
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 779
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->approved_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 780
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/PaymentFeesSpec;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 781
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->processing_fee:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 782
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->app_processing_fee:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 783
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->refunded_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 784
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->status:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 785
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->delay_duration:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 786
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->delay_action:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 787
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->delayed_until:Ljava/lang/String;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 788
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->source_type:Ljava/lang/String;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 789
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->card_details:Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 790
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->balance_details:Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 791
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/CashPaymentDetails;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 792
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 793
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 794
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->order_id:Ljava/lang/String;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 795
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->reference_id:Ljava/lang/String;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 796
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->customer_id:Ljava/lang/String;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 797
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->employee_id:Ljava/lang/String;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 798
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->refund_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 799
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->dispute_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 800
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->risk_evaluation:Lcom/squareup/protos/connect/v2/RiskEvaluation;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/RiskEvaluation;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 801
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->terminal_checkout_id:Ljava/lang/String;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_1a
    const/4 v1, 0x0

    :goto_1a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 802
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->account_id:Ljava/lang/String;

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1b

    :cond_1b
    const/4 v1, 0x0

    :goto_1b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 803
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->buyer_email_address:Ljava/lang/String;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1c

    :cond_1c
    const/4 v1, 0x0

    :goto_1c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 804
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    if-eqz v1, :cond_1d

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Address;->hashCode()I

    move-result v1

    goto :goto_1d

    :cond_1d
    const/4 v1, 0x0

    :goto_1d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 805
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    if-eqz v1, :cond_1e

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Address;->hashCode()I

    move-result v1

    goto :goto_1e

    :cond_1e
    const/4 v1, 0x0

    :goto_1e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 806
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->note:Ljava/lang/String;

    if-eqz v1, :cond_1f

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1f

    :cond_1f
    const/4 v1, 0x0

    :goto_1f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 807
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->statement_description_identifier:Ljava/lang/String;

    if-eqz v1, :cond_20

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_20

    :cond_20
    const/4 v1, 0x0

    :goto_20
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 808
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->capabilities:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 809
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->receipt_number:Ljava/lang/String;

    if-eqz v1, :cond_21

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_21

    :cond_21
    const/4 v1, 0x0

    :goto_21
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 810
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->receipt_url:Ljava/lang/String;

    if-eqz v1, :cond_22

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_22

    :cond_22
    const/4 v1, 0x0

    :goto_22
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 811
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->version_token:Ljava/lang/String;

    if-eqz v1, :cond_23

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_23

    :cond_23
    const/4 v1, 0x0

    :goto_23
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 812
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->version:Ljava/lang/Long;

    if-eqz v1, :cond_24

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_24
    add-int/2addr v0, v2

    .line 813
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_25
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 2

    .line 669
    new-instance v0, Lcom/squareup/protos/connect/v2/Payment$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/Payment$Builder;-><init>()V

    .line 670
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->id:Ljava/lang/String;

    .line 671
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->created_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->created_at:Ljava/lang/String;

    .line 672
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->updated_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->updated_at:Ljava/lang/String;

    .line 673
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 674
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 675
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 676
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 677
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 678
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->approved_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->approved_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 679
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    .line 680
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->processing_fee:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->processing_fee:Ljava/util/List;

    .line 681
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->app_processing_fee:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->app_processing_fee:Ljava/util/List;

    .line 682
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->refunded_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->refunded_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 683
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->status:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->status:Ljava/lang/String;

    .line 684
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->delay_duration:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->delay_duration:Ljava/lang/String;

    .line 685
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->delay_action:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->delay_action:Ljava/lang/String;

    .line 686
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->delayed_until:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->delayed_until:Ljava/lang/String;

    .line 687
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->source_type:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->source_type:Ljava/lang/String;

    .line 688
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->card_details:Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->card_details:Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    .line 689
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->balance_details:Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->balance_details:Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    .line 690
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    .line 691
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    .line 692
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->location_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->location_id:Ljava/lang/String;

    .line 693
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->order_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->order_id:Ljava/lang/String;

    .line 694
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->reference_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->reference_id:Ljava/lang/String;

    .line 695
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->customer_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->customer_id:Ljava/lang/String;

    .line 696
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->employee_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->employee_id:Ljava/lang/String;

    .line 697
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->refund_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->refund_ids:Ljava/util/List;

    .line 698
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->dispute_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->dispute_ids:Ljava/util/List;

    .line 699
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->risk_evaluation:Lcom/squareup/protos/connect/v2/RiskEvaluation;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->risk_evaluation:Lcom/squareup/protos/connect/v2/RiskEvaluation;

    .line 700
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->terminal_checkout_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->terminal_checkout_id:Ljava/lang/String;

    .line 701
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->account_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->account_id:Ljava/lang/String;

    .line 702
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->buyer_email_address:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->buyer_email_address:Ljava/lang/String;

    .line 703
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 704
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 705
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->note:Ljava/lang/String;

    .line 706
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->statement_description_identifier:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->statement_description_identifier:Ljava/lang/String;

    .line 707
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->capabilities:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->capabilities:Ljava/util/List;

    .line 708
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->receipt_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->receipt_number:Ljava/lang/String;

    .line 709
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->receipt_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->receipt_url:Ljava/lang/String;

    .line 710
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->version_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->version_token:Ljava/lang/String;

    .line 711
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/Payment$Builder;->version:Ljava/lang/Long;

    .line 712
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/Payment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/Payment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/Payment;->newBuilder()Lcom/squareup/protos/connect/v2/Payment$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 820
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 821
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 822
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->created_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 823
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->updated_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", updated_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->updated_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 824
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", amount_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 825
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", tip_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 826
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", tax_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 827
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", total_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 828
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_7

    const-string v1, ", app_fee_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 829
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->approved_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_8

    const-string v1, ", approved_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->approved_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 830
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    if-eqz v1, :cond_9

    const-string v1, ", payment_fees_spec="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 831
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->processing_fee:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, ", processing_fee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->processing_fee:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 832
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->app_processing_fee:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, ", app_processing_fee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->app_processing_fee:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 833
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->refunded_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_c

    const-string v1, ", refunded_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->refunded_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 834
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->status:Ljava/lang/String;

    if-eqz v1, :cond_d

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 835
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->delay_duration:Ljava/lang/String;

    if-eqz v1, :cond_e

    const-string v1, ", delay_duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->delay_duration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 836
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->delay_action:Ljava/lang/String;

    if-eqz v1, :cond_f

    const-string v1, ", delay_action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->delay_action:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 837
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->delayed_until:Ljava/lang/String;

    if-eqz v1, :cond_10

    const-string v1, ", delayed_until="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->delayed_until:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 838
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->source_type:Ljava/lang/String;

    if-eqz v1, :cond_11

    const-string v1, ", source_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->source_type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 839
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->card_details:Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    if-eqz v1, :cond_12

    const-string v1, ", card_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->card_details:Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 840
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->balance_details:Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    if-eqz v1, :cond_13

    const-string v1, ", balance_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->balance_details:Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 841
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    if-eqz v1, :cond_14

    const-string v1, ", cash_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 842
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    if-eqz v1, :cond_15

    const-string v1, ", external_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 843
    :cond_15
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_16

    const-string v1, ", location_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->location_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 844
    :cond_16
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->order_id:Ljava/lang/String;

    if-eqz v1, :cond_17

    const-string v1, ", order_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->order_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 845
    :cond_17
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->reference_id:Ljava/lang/String;

    if-eqz v1, :cond_18

    const-string v1, ", reference_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->reference_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 846
    :cond_18
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->customer_id:Ljava/lang/String;

    if-eqz v1, :cond_19

    const-string v1, ", customer_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->customer_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 847
    :cond_19
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->employee_id:Ljava/lang/String;

    if-eqz v1, :cond_1a

    const-string v1, ", employee_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->employee_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 848
    :cond_1a
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->refund_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1b

    const-string v1, ", refund_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->refund_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 849
    :cond_1b
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->dispute_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1c

    const-string v1, ", dispute_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->dispute_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 850
    :cond_1c
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->risk_evaluation:Lcom/squareup/protos/connect/v2/RiskEvaluation;

    if-eqz v1, :cond_1d

    const-string v1, ", risk_evaluation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->risk_evaluation:Lcom/squareup/protos/connect/v2/RiskEvaluation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 851
    :cond_1d
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->terminal_checkout_id:Ljava/lang/String;

    if-eqz v1, :cond_1e

    const-string v1, ", terminal_checkout_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->terminal_checkout_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 852
    :cond_1e
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->account_id:Ljava/lang/String;

    if-eqz v1, :cond_1f

    const-string v1, ", account_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->account_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 853
    :cond_1f
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->buyer_email_address:Ljava/lang/String;

    if-eqz v1, :cond_20

    const-string v1, ", buyer_email_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 854
    :cond_20
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    if-eqz v1, :cond_21

    const-string v1, ", billing_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 855
    :cond_21
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    if-eqz v1, :cond_22

    const-string v1, ", shipping_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 856
    :cond_22
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->note:Ljava/lang/String;

    if-eqz v1, :cond_23

    const-string v1, ", note=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 857
    :cond_23
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->statement_description_identifier:Ljava/lang/String;

    if-eqz v1, :cond_24

    const-string v1, ", statement_description_identifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->statement_description_identifier:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 858
    :cond_24
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->capabilities:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_25

    const-string v1, ", capabilities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->capabilities:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 859
    :cond_25
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->receipt_number:Ljava/lang/String;

    if-eqz v1, :cond_26

    const-string v1, ", receipt_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->receipt_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 860
    :cond_26
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->receipt_url:Ljava/lang/String;

    if-eqz v1, :cond_27

    const-string v1, ", receipt_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->receipt_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 861
    :cond_27
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->version_token:Ljava/lang/String;

    if-eqz v1, :cond_28

    const-string v1, ", version_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->version_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 862
    :cond_28
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->version:Ljava/lang/Long;

    if-eqz v1, :cond_29

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/Payment;->version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_29
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Payment{"

    .line 863
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
