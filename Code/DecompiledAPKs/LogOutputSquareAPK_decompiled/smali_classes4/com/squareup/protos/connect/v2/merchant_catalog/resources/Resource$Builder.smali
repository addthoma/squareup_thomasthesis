.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Resource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public description:Ljava/lang/String;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 106
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;
    .locals 4

    .line 127
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;->description:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    move-result-object v0

    return-object v0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource$Builder;->name:Ljava/lang/String;

    return-object p0
.end method
