.class public final enum Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;
.super Ljava/lang/Enum;
.source "ExcludeStrategy.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy$ProtoAdapter_ExcludeStrategy;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum EXCLUDE_STRATEGY_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

.field public static final enum LEAST_EXPENSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

.field public static final enum MOST_EXPENSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 17
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    const/4 v1, 0x0

    const-string v2, "EXCLUDE_STRATEGY_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->EXCLUDE_STRATEGY_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    .line 28
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    const/4 v2, 0x1

    const-string v3, "LEAST_EXPENSIVE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->LEAST_EXPENSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    .line 39
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    const/4 v3, 0x2

    const-string v4, "MOST_EXPENSIVE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->MOST_EXPENSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    .line 16
    sget-object v4, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->EXCLUDE_STRATEGY_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->LEAST_EXPENSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->MOST_EXPENSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    .line 41
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy$ProtoAdapter_ExcludeStrategy;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy$ProtoAdapter_ExcludeStrategy;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 56
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->MOST_EXPENSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    return-object p0

    .line 55
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->LEAST_EXPENSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    return-object p0

    .line 54
    :cond_2
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->EXCLUDE_STRATEGY_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;
    .locals 1

    .line 16
    const-class v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 63
    iget v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->value:I

    return v0
.end method
