.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public abbreviation:Ljava/lang/String;

.field public available_electronically:Ljava/lang/Boolean;

.field public available_for_pickup:Ljava/lang/Boolean;

.field public available_online:Ljava/lang/Boolean;

.field public buyer_facing_name:Ljava/lang/String;

.field public category_id:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public ecom_available:Ljava/lang/Boolean;

.field public ecom_buy_button_text:Ljava/lang/String;

.field public ecom_image_uris:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public ecom_uri:Ljava/lang/String;

.field public ecom_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

.field public image_url:Ljava/lang/String;

.field public is_taxable:Ljava/lang/Boolean;

.field public item_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem;",
            ">;"
        }
    .end annotation
.end field

.field public label_color:Ljava/lang/String;

.field public modifier_list_info:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;

.field public online_store_data:Ljava/lang/String;

.field public ordinal:Ljava/lang/Integer;

.field public product_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemProductType;

.field public skip_modifier_screen:Ljava/lang/Boolean;

.field public tax_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public variations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field public visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogVisibility;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 545
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 546
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->tax_ids:Ljava/util/List;

    .line 547
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->modifier_list_info:Ljava/util/List;

    .line 548
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->variations:Ljava/util/List;

    .line 549
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->item_options:Ljava/util/List;

    .line 550
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->ecom_image_uris:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public abbreviation(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 574
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->abbreviation:Ljava/lang/String;

    return-object p0
.end method

.method public available_electronically(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 622
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->available_electronically:Ljava/lang/Boolean;

    return-object p0
.end method

.method public available_for_pickup(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 614
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->available_for_pickup:Ljava/lang/Boolean;

    return-object p0
.end method

.method public available_online(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 606
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->available_online:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;
    .locals 2

    .line 794
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 494
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    move-result-object v0

    return-object v0
.end method

.method public buyer_facing_name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 638
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->buyer_facing_name:Ljava/lang/String;

    return-object p0
.end method

.method public category_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 630
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->category_id:Ljava/lang/String;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 565
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public ecom_available(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 764
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->ecom_available:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ecom_buy_button_text(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 780
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->ecom_buy_button_text:Ljava/lang/String;

    return-object p0
.end method

.method public ecom_image_uris(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;"
        }
    .end annotation

    .line 755
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 756
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->ecom_image_uris:Ljava/util/List;

    return-object p0
.end method

.method public ecom_uri(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 747
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->ecom_uri:Ljava/lang/String;

    return-object p0
.end method

.method public ecom_visibility(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 788
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->ecom_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    return-object p0
.end method

.method public image_url(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 684
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->image_url:Ljava/lang/String;

    return-object p0
.end method

.method public is_taxable(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 590
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->is_taxable:Ljava/lang/Boolean;

    return-object p0
.end method

.method public item_options(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;"
        }
    .end annotation

    .line 738
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 739
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->item_options:Ljava/util/List;

    return-object p0
.end method

.method public label_color(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 582
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->label_color:Ljava/lang/String;

    return-object p0
.end method

.method public modifier_list_info(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;"
        }
    .end annotation

    .line 672
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 673
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->modifier_list_info:Ljava/util/List;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 557
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public online_store_data(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 772
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->online_store_data:Ljava/lang/String;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 646
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public product_type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemProductType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 708
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->product_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemProductType;

    return-object p0
.end method

.method public skip_modifier_screen(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 725
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->skip_modifier_screen:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tax_ids(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;"
        }
    .end annotation

    .line 658
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 659
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->tax_ids:Ljava/util/List;

    return-object p0
.end method

.method public variations(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;"
        }
    .end annotation

    .line 694
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 695
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->variations:Ljava/util/List;

    return-object p0
.end method

.method public visibility(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogVisibility;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;
    .locals 0

    .line 598
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogVisibility;

    return-object p0
.end method
