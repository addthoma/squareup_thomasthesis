.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;
.super Lcom/squareup/wire/Message;
.source "BatchUpsertCatalogObjectsResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$ProtoAdapter_BatchUpsertCatalogObjectsResponse;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_UPDATED_AT:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final errors:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Error#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field

.field public final id_mappings:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogIdMapping#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;",
            ">;"
        }
    .end annotation
.end field

.field public final objects:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogObject#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field public final updated_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$ProtoAdapter_BatchUpsertCatalogObjectsResponse;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$ProtoAdapter_BatchUpsertCatalogObjectsResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;",
            ">;)V"
        }
    .end annotation

    .line 73
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;-><init>(Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 78
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p5, "errors"

    .line 79
    invoke-static {p5, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->errors:Ljava/util/List;

    const-string p1, "objects"

    .line 80
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->objects:Ljava/util/List;

    .line 81
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->updated_at:Ljava/lang/String;

    const-string p1, "id_mappings"

    .line 82
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->id_mappings:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 99
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 100
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->errors:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->errors:Ljava/util/List;

    .line 102
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->objects:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->objects:Ljava/util/List;

    .line 103
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->updated_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->updated_at:Ljava/lang/String;

    .line 104
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->id_mappings:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->id_mappings:Ljava/util/List;

    .line 105
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 110
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->objects:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->updated_at:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->id_mappings:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 117
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;
    .locals 2

    .line 87
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;-><init>()V

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->errors:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->errors:Ljava/util/List;

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->objects:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->objects:Ljava/util/List;

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->updated_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->updated_at:Ljava/lang/String;

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->id_mappings:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->id_mappings:Ljava/util/List;

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", errors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->errors:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->objects:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", objects="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->objects:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 127
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->updated_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", updated_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->updated_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->id_mappings:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", id_mappings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->id_mappings:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BatchUpsertCatalogObjectsResponse{"

    .line 129
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
