.class public final Lcom/squareup/protos/connect/v2/CreatePaymentResponse;
.super Lcom/squareup/wire/Message;
.source "CreatePaymentResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/CreatePaymentResponse$ProtoAdapter_CreatePaymentResponse;,
        Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/CreatePaymentResponse;",
        "Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/CreatePaymentResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ENCRYPTED_EMV_DATA:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final encrypted_emv_data:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final errors:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Error#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field

.field public final payment:Lcom/squareup/protos/connect/v2/Payment;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.Payment#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$ProtoAdapter_CreatePaymentResponse;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$ProtoAdapter_CreatePaymentResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/connect/v2/Payment;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;",
            "Lcom/squareup/protos/connect/v2/Payment;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 70
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/connect/v2/Payment;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/connect/v2/Payment;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;",
            "Lcom/squareup/protos/connect/v2/Payment;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 75
    sget-object v0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p4, "errors"

    .line 76
    invoke-static {p4, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->errors:Ljava/util/List;

    .line 77
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->payment:Lcom/squareup/protos/connect/v2/Payment;

    .line 78
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->encrypted_emv_data:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 94
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 95
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->errors:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->errors:Ljava/util/List;

    .line 97
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->payment:Lcom/squareup/protos/connect/v2/Payment;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->payment:Lcom/squareup/protos/connect/v2/Payment;

    .line 98
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->encrypted_emv_data:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->encrypted_emv_data:Ljava/lang/String;

    .line 99
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 104
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->payment:Lcom/squareup/protos/connect/v2/Payment;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/Payment;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->encrypted_emv_data:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 110
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;
    .locals 2

    .line 83
    new-instance v0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;-><init>()V

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->errors:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->errors:Ljava/util/List;

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->payment:Lcom/squareup/protos/connect/v2/Payment;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->payment:Lcom/squareup/protos/connect/v2/Payment;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->encrypted_emv_data:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->encrypted_emv_data:Ljava/lang/String;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->newBuilder()Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", errors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->errors:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->payment:Lcom/squareup/protos/connect/v2/Payment;

    if-eqz v1, :cond_1

    const-string v1, ", payment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->payment:Lcom/squareup/protos/connect/v2/Payment;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 120
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->encrypted_emv_data:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", encrypted_emv_data=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CreatePaymentResponse{"

    .line 121
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
