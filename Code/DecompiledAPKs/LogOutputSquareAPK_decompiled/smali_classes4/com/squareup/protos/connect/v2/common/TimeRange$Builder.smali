.class public final Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TimeRange.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/common/TimeRange;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/common/TimeRange;",
        "Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public end_at:Ljava/lang/String;

.field public start_at:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 115
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/common/TimeRange;
    .locals 4

    .line 142
    new-instance v0, Lcom/squareup/protos/connect/v2/common/TimeRange;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;->start_at:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;->end_at:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/common/TimeRange;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;->build()Lcom/squareup/protos/connect/v2/common/TimeRange;

    move-result-object v0

    return-object v0
.end method

.method public end_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;->end_at:Ljava/lang/String;

    return-object p0
.end method

.method public start_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;->start_at:Ljava/lang/String;

    return-object p0
.end method
