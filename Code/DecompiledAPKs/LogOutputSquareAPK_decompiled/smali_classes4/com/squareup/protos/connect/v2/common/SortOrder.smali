.class public final enum Lcom/squareup/protos/connect/v2/common/SortOrder;
.super Ljava/lang/Enum;
.source "SortOrder.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/common/SortOrder$ProtoAdapter_SortOrder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/common/SortOrder;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/common/SortOrder;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/common/SortOrder;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ASC:Lcom/squareup/protos/connect/v2/common/SortOrder;

.field public static final enum DESC:Lcom/squareup/protos/connect/v2/common/SortOrder;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 19
    new-instance v0, Lcom/squareup/protos/connect/v2/common/SortOrder;

    const/4 v1, 0x0

    const-string v2, "DESC"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/common/SortOrder;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/SortOrder;->DESC:Lcom/squareup/protos/connect/v2/common/SortOrder;

    .line 24
    new-instance v0, Lcom/squareup/protos/connect/v2/common/SortOrder;

    const/4 v2, 0x1

    const-string v3, "ASC"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/common/SortOrder;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/SortOrder;->ASC:Lcom/squareup/protos/connect/v2/common/SortOrder;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/common/SortOrder;

    .line 15
    sget-object v3, Lcom/squareup/protos/connect/v2/common/SortOrder;->DESC:Lcom/squareup/protos/connect/v2/common/SortOrder;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/SortOrder;->ASC:Lcom/squareup/protos/connect/v2/common/SortOrder;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/connect/v2/common/SortOrder;->$VALUES:[Lcom/squareup/protos/connect/v2/common/SortOrder;

    .line 26
    new-instance v0, Lcom/squareup/protos/connect/v2/common/SortOrder$ProtoAdapter_SortOrder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/SortOrder$ProtoAdapter_SortOrder;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/SortOrder;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput p3, p0, Lcom/squareup/protos/connect/v2/common/SortOrder;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/common/SortOrder;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 40
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/common/SortOrder;->ASC:Lcom/squareup/protos/connect/v2/common/SortOrder;

    return-object p0

    .line 39
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/common/SortOrder;->DESC:Lcom/squareup/protos/connect/v2/common/SortOrder;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/SortOrder;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/protos/connect/v2/common/SortOrder;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/common/SortOrder;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/common/SortOrder;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/protos/connect/v2/common/SortOrder;->$VALUES:[Lcom/squareup/protos/connect/v2/common/SortOrder;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/common/SortOrder;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/common/SortOrder;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 47
    iget v0, p0, Lcom/squareup/protos/connect/v2/common/SortOrder;->value:I

    return v0
.end method
