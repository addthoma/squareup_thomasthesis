.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;
.super Lcom/squareup/wire/Message;
.source "CatalogDiscount.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$ProtoAdapter_CatalogDiscount;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APPLICATION_METHOD:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;

.field public static final DEFAULT_COMP_ORDINAL:Ljava/lang/Integer;

.field public static final DEFAULT_DISCOUNT_TYPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;

.field public static final DEFAULT_LABEL_COLOR:Ljava/lang/String; = ""

.field public static final DEFAULT_MODIFY_TAX_BASIS:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_PIN_REQUIRED:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final amount_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final application_method:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogDiscountApplicationMethod#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final comp_ordinal:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x8
    .end annotation
.end field

.field public final discount_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogDiscountType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final label_color:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final maximum_amount:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final modify_tax_basis:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogDiscountModifyTaxBasis#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final percentage:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final pin_required:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 26
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$ProtoAdapter_CatalogDiscount;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$ProtoAdapter_CatalogDiscount;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;->FIXED_PERCENTAGE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->DEFAULT_DISCOUNT_TYPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;

    const/4 v0, 0x0

    .line 36
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->DEFAULT_PIN_REQUIRED:Ljava/lang/Boolean;

    .line 40
    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;->MANUALLY_APPLIED:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;

    sput-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->DEFAULT_APPLICATION_METHOD:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;

    .line 42
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->DEFAULT_COMP_ORDINAL:Ljava/lang/Integer;

    .line 44
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;->MODIFY_TAX_BASIS:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->DEFAULT_MODIFY_TAX_BASIS:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;Ljava/lang/Integer;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;Lcom/squareup/protos/connect/v2/common/Money;)V
    .locals 12

    .line 169
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;Ljava/lang/Integer;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;Ljava/lang/Integer;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 177
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->name:Ljava/lang/String;

    .line 179
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->discount_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;

    .line 180
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->percentage:Ljava/lang/String;

    .line 181
    iput-object p4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 182
    iput-object p5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->pin_required:Ljava/lang/Boolean;

    .line 183
    iput-object p6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->label_color:Ljava/lang/String;

    .line 184
    iput-object p7, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->application_method:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;

    .line 185
    iput-object p8, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->comp_ordinal:Ljava/lang/Integer;

    .line 186
    iput-object p9, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->modify_tax_basis:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;

    .line 187
    iput-object p10, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->maximum_amount:Lcom/squareup/protos/connect/v2/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 210
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 211
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    .line 212
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->name:Ljava/lang/String;

    .line 213
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->discount_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->discount_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;

    .line 214
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->percentage:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->percentage:Ljava/lang/String;

    .line 215
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 216
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->pin_required:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->pin_required:Ljava/lang/Boolean;

    .line 217
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->label_color:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->label_color:Ljava/lang/String;

    .line 218
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->application_method:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->application_method:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;

    .line 219
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->comp_ordinal:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->comp_ordinal:Ljava/lang/Integer;

    .line 220
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->modify_tax_basis:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->modify_tax_basis:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;

    .line 221
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->maximum_amount:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->maximum_amount:Lcom/squareup/protos/connect/v2/common/Money;

    .line 222
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 227
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 229
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 230
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 231
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->discount_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 232
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 233
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 234
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->pin_required:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 235
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->label_color:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 236
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->application_method:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 237
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->comp_ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 238
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->modify_tax_basis:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 239
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->maximum_amount:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 240
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;
    .locals 2

    .line 192
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;-><init>()V

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->name:Ljava/lang/String;

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->discount_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->discount_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->percentage:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->percentage:Ljava/lang/String;

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->pin_required:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->pin_required:Ljava/lang/Boolean;

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->label_color:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->label_color:Ljava/lang/String;

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->application_method:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->application_method:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->comp_ordinal:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->comp_ordinal:Ljava/lang/Integer;

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->modify_tax_basis:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->modify_tax_basis:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->maximum_amount:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->maximum_amount:Lcom/squareup/protos/connect/v2/common/Money;

    .line 203
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 247
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->discount_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;

    if-eqz v1, :cond_1

    const-string v1, ", discount_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->discount_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 250
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->percentage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", amount_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 252
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->pin_required:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", pin_required="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->pin_required:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 253
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->label_color:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", label_color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->label_color:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->application_method:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;

    if-eqz v1, :cond_6

    const-string v1, ", application_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->application_method:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 255
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->comp_ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    const-string v1, ", comp_ordinal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->comp_ordinal:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 256
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->modify_tax_basis:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;

    if-eqz v1, :cond_8

    const-string v1, ", modify_tax_basis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->modify_tax_basis:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 257
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->maximum_amount:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_9

    const-string v1, ", maximum_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->maximum_amount:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CatalogDiscount{"

    .line 258
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
