.class public final Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AddedPaymentForTerminalCheckout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;",
        "Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public from:Lcom/squareup/protos/common/location/GeoLocation;

.field public payment_from_country_code:Lcom/squareup/protos/common/countries/Country;

.field public payment_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 112
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;
    .locals 5

    .line 132
    iget-object v0, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;->payment_id:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 135
    new-instance v1, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;->from:Lcom/squareup/protos/common/location/GeoLocation;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;->payment_from_country_code:Lcom/squareup/protos/common/countries/Country;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/location/GeoLocation;Lcom/squareup/protos/common/countries/Country;Lokio/ByteString;)V

    return-object v1

    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v0, 0x1

    const-string v2, "payment_id"

    aput-object v2, v1, v0

    .line 133
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;->build()Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;

    move-result-object v0

    return-object v0
.end method

.method public from(Lcom/squareup/protos/common/location/GeoLocation;)Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;->from:Lcom/squareup/protos/common/location/GeoLocation;

    return-object p0
.end method

.method public payment_from_country_code(Lcom/squareup/protos/common/countries/Country;)Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;->payment_from_country_code:Lcom/squareup/protos/common/countries/Country;

    return-object p0
.end method

.method public payment_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;->payment_id:Ljava/lang/String;

    return-object p0
.end method
