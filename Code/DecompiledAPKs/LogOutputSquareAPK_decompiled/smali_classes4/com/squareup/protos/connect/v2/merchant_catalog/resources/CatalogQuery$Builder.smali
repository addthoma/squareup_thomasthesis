.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

.field public exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

.field public filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

.field public item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

.field public items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

.field public items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

.field public items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

.field public prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

.field public pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

.field public range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

.field public sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

.field public text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 301
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;
    .locals 15

    .line 417
    new-instance v14, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    iget-object v7, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    iget-object v8, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    iget-object v9, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    iget-object v10, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    iget-object v11, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    iget-object v12, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v13

    move-object v0, v14

    invoke-direct/range {v0 .. v13}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;Lokio/ByteString;)V

    return-object v14
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 276
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    move-result-object v0

    return-object v0
.end method

.method public custom_attribute_usage(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;
    .locals 0

    .line 402
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    return-object p0
.end method

.method public exact_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;
    .locals 0

    .line 319
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    return-object p0
.end method

.method public filtered_items_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;
    .locals 0

    .line 394
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    return-object p0
.end method

.method public item_variations_for_item_option_values_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;
    .locals 0

    .line 386
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    return-object p0
.end method

.method public items_for_item_options_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;
    .locals 0

    .line 377
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    return-object p0
.end method

.method public items_for_modifier_list_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;
    .locals 0

    .line 369
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    return-object p0
.end method

.method public items_for_tax_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;
    .locals 0

    .line 360
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    return-object p0
.end method

.method public prefix_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;
    .locals 0

    .line 330
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    return-object p0
.end method

.method public pricing_rules_for_products_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;
    .locals 0

    .line 411
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    return-object p0
.end method

.method public range_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;
    .locals 0

    .line 340
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    return-object p0
.end method

.method public sorted_attribute_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;
    .locals 0

    .line 308
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    return-object p0
.end method

.method public text_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;
    .locals 0

    .line 352
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    return-object p0
.end method
