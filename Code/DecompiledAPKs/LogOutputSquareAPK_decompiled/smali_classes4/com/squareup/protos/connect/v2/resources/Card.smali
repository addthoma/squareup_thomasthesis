.class public final Lcom/squareup/protos/connect/v2/resources/Card;
.super Lcom/squareup/wire/Message;
.source "Card.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Card$ProtoAdapter_Card;,
        Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;,
        Lcom/squareup/protos/connect/v2/resources/Card$Type;,
        Lcom/squareup/protos/connect/v2/resources/Card$Brand;,
        Lcom/squareup/protos/connect/v2/resources/Card$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/resources/Card;",
        "Lcom/squareup/protos/connect/v2/resources/Card$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Card;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BIN:Ljava/lang/String; = ""

.field public static final DEFAULT_CARDHOLDER_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_CARD_BRAND:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

.field public static final DEFAULT_CARD_TYPE:Lcom/squareup/protos/connect/v2/resources/Card$Type;

.field public static final DEFAULT_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_EXP_MONTH:Ljava/lang/Long;

.field public static final DEFAULT_EXP_YEAR:Ljava/lang/Long;

.field public static final DEFAULT_FINGERPRINT:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_LAST_4:Ljava/lang/String; = ""

.field public static final DEFAULT_PREPAID_TYPE:Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;

.field public static final DEFAULT_REFERENCE_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final billing_address:Lcom/squareup/protos/connect/v2/resources/Address;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Address#ADAPTER"
        redacted = true
        tag = 0x7
    .end annotation
.end field

.field public final bin:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xf
    .end annotation
.end field

.field public final card_brand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Card$Brand#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final card_type:Lcom/squareup/protos/connect/v2/resources/Card$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Card$Type#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final cardholder_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x6
    .end annotation
.end field

.field public final enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final exp_month:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x4
    .end annotation
.end field

.field public final exp_year:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x5
    .end annotation
.end field

.field public final fingerprint:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final last_4:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final prepaid_type:Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Card$PrepaidType#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final reference_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 30
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Card$ProtoAdapter_Card;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Card$ProtoAdapter_Card;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 36
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->OTHER_BRAND:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card;->DEFAULT_CARD_BRAND:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    const-wide/16 v0, 0x0

    .line 40
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card;->DEFAULT_EXP_MONTH:Ljava/lang/Long;

    .line 42
    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card;->DEFAULT_EXP_YEAR:Ljava/lang/Long;

    const/4 v0, 0x1

    .line 50
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card;->DEFAULT_ENABLED:Ljava/lang/Boolean;

    .line 52
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Card$Type;->UNKNOWN_CARD_TYPE:Lcom/squareup/protos/connect/v2/resources/Card$Type;

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card;->DEFAULT_CARD_TYPE:Lcom/squareup/protos/connect/v2/resources/Card$Type;

    .line 54
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;->UNKNOWN_PREPAID_TYPE:Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card;->DEFAULT_PREPAID_TYPE:Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Card$Brand;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Address;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/connect/v2/resources/Card$Type;Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;Ljava/lang/String;)V
    .locals 15

    .line 212
    sget-object v14, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/protos/connect/v2/resources/Card;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Card$Brand;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Address;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/connect/v2/resources/Card$Type;Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Card$Brand;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Address;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/connect/v2/resources/Card$Type;Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 219
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p14}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->id:Ljava/lang/String;

    .line 221
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Card;->card_brand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 222
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/resources/Card;->last_4:Ljava/lang/String;

    .line 223
    iput-object p4, p0, Lcom/squareup/protos/connect/v2/resources/Card;->exp_month:Ljava/lang/Long;

    .line 224
    iput-object p5, p0, Lcom/squareup/protos/connect/v2/resources/Card;->exp_year:Ljava/lang/Long;

    .line 225
    iput-object p6, p0, Lcom/squareup/protos/connect/v2/resources/Card;->cardholder_name:Ljava/lang/String;

    .line 226
    iput-object p7, p0, Lcom/squareup/protos/connect/v2/resources/Card;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 227
    iput-object p8, p0, Lcom/squareup/protos/connect/v2/resources/Card;->fingerprint:Ljava/lang/String;

    .line 228
    iput-object p9, p0, Lcom/squareup/protos/connect/v2/resources/Card;->reference_id:Ljava/lang/String;

    .line 229
    iput-object p10, p0, Lcom/squareup/protos/connect/v2/resources/Card;->enabled:Ljava/lang/Boolean;

    .line 230
    iput-object p11, p0, Lcom/squareup/protos/connect/v2/resources/Card;->card_type:Lcom/squareup/protos/connect/v2/resources/Card$Type;

    .line 231
    iput-object p12, p0, Lcom/squareup/protos/connect/v2/resources/Card;->prepaid_type:Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;

    .line 232
    iput-object p13, p0, Lcom/squareup/protos/connect/v2/resources/Card;->bin:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 258
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/resources/Card;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 259
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/resources/Card;

    .line 260
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Card;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Card;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Card;->id:Ljava/lang/String;

    .line 261
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->card_brand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Card;->card_brand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 262
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->last_4:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Card;->last_4:Ljava/lang/String;

    .line 263
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->exp_month:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Card;->exp_month:Ljava/lang/Long;

    .line 264
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->exp_year:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Card;->exp_year:Ljava/lang/Long;

    .line 265
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->cardholder_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Card;->cardholder_name:Ljava/lang/String;

    .line 266
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Card;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 267
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->fingerprint:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Card;->fingerprint:Ljava/lang/String;

    .line 268
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->reference_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Card;->reference_id:Ljava/lang/String;

    .line 269
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Card;->enabled:Ljava/lang/Boolean;

    .line 270
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->card_type:Lcom/squareup/protos/connect/v2/resources/Card$Type;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Card;->card_type:Lcom/squareup/protos/connect/v2/resources/Card$Type;

    .line 271
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->prepaid_type:Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Card;->prepaid_type:Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;

    .line 272
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->bin:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/resources/Card;->bin:Ljava/lang/String;

    .line 273
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 278
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_d

    .line 280
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Card;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 281
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 282
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->card_brand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 283
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->last_4:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 284
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->exp_month:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 285
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->exp_year:Ljava/lang/Long;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 286
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->cardholder_name:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 287
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Address;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 288
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->fingerprint:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 289
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->reference_id:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 290
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 291
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->card_type:Lcom/squareup/protos/connect/v2/resources/Card$Type;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Card$Type;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 292
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->prepaid_type:Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 293
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->bin:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_c
    add-int/2addr v0, v2

    .line 294
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_d
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/resources/Card$Builder;
    .locals 2

    .line 237
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Card$Builder;-><init>()V

    .line 238
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->id:Ljava/lang/String;

    .line 239
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->card_brand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->card_brand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 240
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->last_4:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->last_4:Ljava/lang/String;

    .line 241
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->exp_month:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->exp_month:Ljava/lang/Long;

    .line 242
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->exp_year:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->exp_year:Ljava/lang/Long;

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->cardholder_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->cardholder_name:Ljava/lang/String;

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->fingerprint:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->fingerprint:Ljava/lang/String;

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->reference_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->reference_id:Ljava/lang/String;

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->enabled:Ljava/lang/Boolean;

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->card_type:Lcom/squareup/protos/connect/v2/resources/Card$Type;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->card_type:Lcom/squareup/protos/connect/v2/resources/Card$Type;

    .line 249
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->prepaid_type:Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->prepaid_type:Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;

    .line 250
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->bin:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->bin:Ljava/lang/String;

    .line 251
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Card;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Card;->newBuilder()Lcom/squareup/protos/connect/v2/resources/Card$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 301
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 302
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->card_brand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    if-eqz v1, :cond_1

    const-string v1, ", card_brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->card_brand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 304
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->last_4:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", last_4="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->last_4:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->exp_month:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", exp_month="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->exp_month:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 306
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->exp_year:Ljava/lang/Long;

    if-eqz v1, :cond_4

    const-string v1, ", exp_year="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->exp_year:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 307
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->cardholder_name:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", cardholder_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    if-eqz v1, :cond_6

    const-string v1, ", billing_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->fingerprint:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", fingerprint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->fingerprint:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->reference_id:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", reference_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->reference_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 312
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->card_type:Lcom/squareup/protos/connect/v2/resources/Card$Type;

    if-eqz v1, :cond_a

    const-string v1, ", card_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->card_type:Lcom/squareup/protos/connect/v2/resources/Card$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 313
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->prepaid_type:Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;

    if-eqz v1, :cond_b

    const-string v1, ", prepaid_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->prepaid_type:Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 314
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->bin:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", bin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Card;->bin:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Card{"

    .line 315
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
