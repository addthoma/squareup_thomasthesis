.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;
.super Lcom/squareup/wire/Message;
.source "CatalogQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ProtoAdapter_CatalogQuery;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogQuery$CustomAttributeUsage#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogQuery$Exact#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogQuery$FilteredItems#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogQuery$ItemVariationsForItemOptionValues#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogQuery$ItemsForItemOptions#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogQuery$ItemsForModifierList#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogQuery$ItemsForTax#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogQuery$Prefix#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogQuery$PricingRulesForProducts#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogQuery$Range#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogQuery$SortedAttribute#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogQuery$Text#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ProtoAdapter_CatalogQuery;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ProtoAdapter_CatalogQuery;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;)V
    .locals 14

    .line 172
    sget-object v13, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;Lokio/ByteString;)V
    .locals 1

    .line 182
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p13}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 183
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    .line 184
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    .line 185
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    .line 186
    iput-object p4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    .line 187
    iput-object p5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    .line 188
    iput-object p6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    .line 189
    iput-object p7, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    .line 190
    iput-object p8, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    .line 191
    iput-object p9, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    .line 192
    iput-object p10, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    .line 193
    iput-object p11, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    .line 194
    iput-object p12, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 219
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 220
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    .line 221
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    .line 222
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    .line 223
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    .line 224
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    .line 225
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    .line 226
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    .line 227
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    .line 228
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    .line 229
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    .line 230
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    .line 231
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    .line 232
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    .line 233
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 238
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_c

    .line 240
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 241
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 242
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 249
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 250
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 251
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 252
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;->hashCode()I

    move-result v2

    :cond_b
    add-int/2addr v0, v2

    .line 253
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_c
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;
    .locals 2

    .line 199
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;-><init>()V

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    .line 212
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 40
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 261
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    if-eqz v1, :cond_0

    const-string v1, ", sorted_attribute_query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 262
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    if-eqz v1, :cond_1

    const-string v1, ", exact_query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 263
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    if-eqz v1, :cond_2

    const-string v1, ", prefix_query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 264
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    if-eqz v1, :cond_3

    const-string v1, ", range_query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 265
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    if-eqz v1, :cond_4

    const-string v1, ", text_query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 266
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    if-eqz v1, :cond_5

    const-string v1, ", items_for_tax_query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 267
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    if-eqz v1, :cond_6

    const-string v1, ", items_for_modifier_list_query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 268
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    if-eqz v1, :cond_7

    const-string v1, ", items_for_item_options_query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 269
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    if-eqz v1, :cond_8

    const-string v1, ", item_variations_for_item_option_values_query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 270
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    if-eqz v1, :cond_9

    const-string v1, ", filtered_items_query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 271
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    if-eqz v1, :cond_a

    const-string v1, ", custom_attribute_usage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 272
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    if-eqz v1, :cond_b

    const-string v1, ", pricing_rules_for_products_query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_b
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CatalogQuery{"

    .line 273
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
