.class final Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$ProtoAdapter_SearchCatalogObjectsRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SearchCatalogObjectsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SearchCatalogObjectsRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 408
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 441
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;-><init>()V

    .line 442
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 443
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 462
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 460
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->include_counts(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    goto :goto_0

    .line 459
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->end_time(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    goto :goto_0

    .line 458
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    goto :goto_0

    .line 457
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    goto :goto_0

    .line 456
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->begin_time(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    goto :goto_0

    .line 455
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->include_related_objects(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    goto :goto_0

    .line 454
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->include_deleted_objects(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    goto :goto_0

    .line 448
    :pswitch_7
    :try_start_0
    iget-object v4, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->object_types:Ljava/util/List;

    sget-object v5, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 450
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 445
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->cursor(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    goto/16 :goto_0

    .line 466
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 467
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 406
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$ProtoAdapter_SearchCatalogObjectsRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 427
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->cursor:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 428
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->object_types:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 429
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->include_deleted_objects:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 430
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->include_related_objects:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 431
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->begin_time:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 432
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 433
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->limit:Ljava/lang/Integer;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 434
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->end_time:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 435
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->include_counts:Ljava/lang/Boolean;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 436
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 406
    check-cast p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$ProtoAdapter_SearchCatalogObjectsRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;)I
    .locals 4

    .line 413
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->cursor:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 414
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->object_types:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->include_deleted_objects:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 415
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->include_related_objects:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 416
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->begin_time:Ljava/lang/String;

    const/4 v3, 0x5

    .line 417
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    const/4 v3, 0x6

    .line 418
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->limit:Ljava/lang/Integer;

    const/4 v3, 0x7

    .line 419
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->end_time:Ljava/lang/String;

    const/16 v3, 0x8

    .line 420
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->include_counts:Ljava/lang/Boolean;

    const/16 v3, 0x9

    .line 421
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 422
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 406
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$ProtoAdapter_SearchCatalogObjectsRequest;->encodedSize(Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;
    .locals 2

    .line 472
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    move-result-object p1

    .line 473
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    .line 474
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 475
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 406
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$ProtoAdapter_SearchCatalogObjectsRequest;->redact(Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;

    move-result-object p1

    return-object p1
.end method
