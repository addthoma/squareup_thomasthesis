.class public final enum Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;
.super Ljava/lang/Enum;
.source "TaxCalculationPhase.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase$ProtoAdapter_TaxCalculationPhase;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum TAX_SUBTOTAL_PHASE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

.field public static final enum TAX_TOTAL_PHASE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 19
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const-string v3, "TAX_SUBTOTAL_PHASE"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;->TAX_SUBTOTAL_PHASE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

    .line 24
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

    const/4 v3, 0x1

    const-string v4, "TAX_TOTAL_PHASE"

    const/4 v5, 0x3

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;->TAX_TOTAL_PHASE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

    new-array v0, v1, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

    .line 15
    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;->TAX_SUBTOTAL_PHASE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;->TAX_TOTAL_PHASE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

    .line 26
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase$ProtoAdapter_TaxCalculationPhase;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase$ProtoAdapter_TaxCalculationPhase;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;
    .locals 1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 40
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;->TAX_TOTAL_PHASE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

    return-object p0

    .line 39
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;->TAX_SUBTOTAL_PHASE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 47
    iget v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;->value:I

    return v0
.end method
