.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogModifierList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public modifiers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;

.field public ordinal:Ljava/lang/Integer;

.field public selection_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 160
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 161
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;->modifiers:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;
    .locals 7

    .line 207
    new-instance v6, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;->ordinal:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;->selection_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;->modifiers:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;-><init>(Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 151
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    move-result-object v0

    return-object v0
.end method

.method public modifiers(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;"
        }
    .end annotation

    .line 200
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 201
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;->modifiers:Ljava/util/List;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public selection_type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList$Builder;->selection_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

    return-object p0
.end method
