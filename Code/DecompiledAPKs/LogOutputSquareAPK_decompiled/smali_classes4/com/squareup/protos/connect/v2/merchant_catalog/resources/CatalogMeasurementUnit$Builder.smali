.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogMeasurementUnit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

.field public precision:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;
    .locals 4

    .line 152
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->precision:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    move-result-object v0

    return-object v0
.end method

.method public measurement_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    return-object p0
.end method

.method public precision(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->precision:Ljava/lang/Integer;

    return-object p0
.end method
