.class public final enum Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;
.super Ljava/lang/Enum;
.source "AggregationStrategy.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy$ProtoAdapter_AggregationStrategy;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BASE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

.field public static final enum EXCLUSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

.field public static final enum STACKABLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

.field public static final enum UNKNOWN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 17
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->UNKNOWN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    .line 26
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    const/4 v2, 0x1

    const-string v3, "BASE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->BASE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    .line 34
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    const/4 v3, 0x2

    const-string v4, "STACKABLE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->STACKABLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    .line 39
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    const/4 v4, 0x3

    const-string v5, "EXCLUSIVE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->EXCLUSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    .line 13
    sget-object v5, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->UNKNOWN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->BASE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->STACKABLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->EXCLUSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    .line 41
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy$ProtoAdapter_AggregationStrategy;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy$ProtoAdapter_AggregationStrategy;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 57
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->EXCLUSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    return-object p0

    .line 56
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->STACKABLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    return-object p0

    .line 55
    :cond_2
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->BASE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    return-object p0

    .line 54
    :cond_3
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->UNKNOWN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 64
    iget v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->value:I

    return v0
.end method
