.class public final Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AdditionalRecipient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;",
        "Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public description:Ljava/lang/String;

.field public location_id:Ljava/lang/String;

.field public receivable_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 140
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;
    .locals 7

    .line 177
    new-instance v6, Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient$Builder;->location_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient$Builder;->description:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient$Builder;->receivable_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient$Builder;->build()Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;

    move-result-object v0

    return-object v0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public location_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient$Builder;->location_id:Ljava/lang/String;

    return-object p0
.end method

.method public receivable_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient$Builder;->receivable_id:Ljava/lang/String;

    return-object p0
.end method
