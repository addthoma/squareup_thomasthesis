.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogProductSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public all_products:Ljava/lang/Boolean;

.field public match_custom_amounts:Ljava/lang/Boolean;

.field public name:Ljava/lang/String;

.field public product_ids_all:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public product_ids_any:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public quantity_exact:Ljava/lang/Long;

.field public quantity_max:Ljava/lang/Long;

.field public quantity_min:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 261
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 262
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->product_ids_any:Ljava/util/List;

    .line 263
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->product_ids_all:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public all_products(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;
    .locals 0

    .line 357
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->all_products:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;
    .locals 11

    .line 373
    new-instance v10, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->product_ids_any:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->product_ids_all:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->quantity_exact:Ljava/lang/Long;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->quantity_min:Ljava/lang/Long;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->quantity_max:Ljava/lang/Long;

    iget-object v7, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->all_products:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->match_custom_amounts:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 244
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    move-result-object v0

    return-object v0
.end method

.method public match_custom_amounts(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;
    .locals 0

    .line 367
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->match_custom_amounts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;
    .locals 0

    .line 273
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public product_ids_all(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;"
        }
    .end annotation

    .line 308
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 309
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->product_ids_all:Ljava/util/List;

    return-object p0
.end method

.method public product_ids_any(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;"
        }
    .end annotation

    .line 292
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 293
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->product_ids_any:Ljava/util/List;

    return-object p0
.end method

.method public quantity_exact(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;
    .locals 0

    .line 322
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->quantity_exact:Ljava/lang/Long;

    return-object p0
.end method

.method public quantity_max(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;
    .locals 0

    .line 345
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->quantity_max:Ljava/lang/Long;

    return-object p0
.end method

.method public quantity_min(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;
    .locals 0

    .line 334
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->quantity_min:Ljava/lang/Long;

    return-object p0
.end method
