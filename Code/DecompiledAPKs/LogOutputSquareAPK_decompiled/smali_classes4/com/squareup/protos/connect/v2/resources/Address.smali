.class public final Lcom/squareup/protos/connect/v2/resources/Address;
.super Lcom/squareup/wire/Message;
.source "Address.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Address$ProtoAdapter_Address;,
        Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/resources/Address;",
        "Lcom/squareup/protos/connect/v2/resources/Address$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Address;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ADDRESS_LINE_1:Ljava/lang/String; = ""

.field public static final DEFAULT_ADDRESS_LINE_2:Ljava/lang/String; = ""

.field public static final DEFAULT_ADDRESS_LINE_3:Ljava/lang/String; = ""

.field public static final DEFAULT_ADMINISTRATIVE_DISTRICT_LEVEL_1:Ljava/lang/String; = ""

.field public static final DEFAULT_ADMINISTRATIVE_DISTRICT_LEVEL_2:Ljava/lang/String; = ""

.field public static final DEFAULT_ADMINISTRATIVE_DISTRICT_LEVEL_3:Ljava/lang/String; = ""

.field public static final DEFAULT_COUNTRY:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final DEFAULT_FIRST_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_LAST_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_LOCALITY:Ljava/lang/String; = ""

.field public static final DEFAULT_ORGANIZATION:Ljava/lang/String; = ""

.field public static final DEFAULT_POSTAL_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_SUBLOCALITY:Ljava/lang/String; = ""

.field public static final DEFAULT_SUBLOCALITY_2:Ljava/lang/String; = ""

.field public static final DEFAULT_SUBLOCALITY_3:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final address_line_1:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final address_line_2:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final address_line_3:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final administrative_district_level_1:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final administrative_district_level_2:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xe
    .end annotation
.end field

.field public final administrative_district_level_3:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xf
    .end annotation
.end field

.field public final country:Lcom/squareup/protos/connect/v2/resources/Country;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Country#ADAPTER"
        tag = 0x11
    .end annotation
.end field

.field public final first_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x12
    .end annotation
.end field

.field public final last_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x13
    .end annotation
.end field

.field public final locality:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final organization:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x14
    .end annotation
.end field

.field public final postal_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x10
    .end annotation
.end field

.field public final sublocality:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final sublocality_2:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final sublocality_3:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Address$ProtoAdapter_Address;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Address$ProtoAdapter_Address;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Address;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 49
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->ZZ:Lcom/squareup/protos/connect/v2/resources/Country;

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Address;->DEFAULT_COUNTRY:Lcom/squareup/protos/connect/v2/resources/Country;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Country;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    .line 223
    sget-object v16, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct/range {v0 .. v16}, Lcom/squareup/protos/connect/v2/resources/Address;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Country;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Country;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 3

    move-object v0, p0

    .line 231
    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Address;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p16

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 232
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_1:Ljava/lang/String;

    move-object v1, p2

    .line 233
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_2:Ljava/lang/String;

    move-object v1, p3

    .line 234
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_3:Ljava/lang/String;

    move-object v1, p4

    .line 235
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address;->locality:Ljava/lang/String;

    move-object v1, p5

    .line 236
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality:Ljava/lang/String;

    move-object v1, p6

    .line 237
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality_2:Ljava/lang/String;

    move-object v1, p7

    .line 238
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality_3:Ljava/lang/String;

    move-object v1, p8

    .line 239
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_1:Ljava/lang/String;

    move-object v1, p9

    .line 240
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_2:Ljava/lang/String;

    move-object v1, p10

    .line 241
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_3:Ljava/lang/String;

    move-object v1, p11

    .line 242
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address;->postal_code:Ljava/lang/String;

    move-object v1, p12

    .line 243
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    move-object/from16 v1, p13

    .line 244
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address;->first_name:Ljava/lang/String;

    move-object/from16 v1, p14

    .line 245
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address;->last_name:Ljava/lang/String;

    move-object/from16 v1, p15

    .line 246
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address;->organization:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 274
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/resources/Address;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 275
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/resources/Address;

    .line 276
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Address;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Address;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_1:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_1:Ljava/lang/String;

    .line 277
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_2:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_2:Ljava/lang/String;

    .line 278
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_3:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_3:Ljava/lang/String;

    .line 279
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->locality:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Address;->locality:Ljava/lang/String;

    .line 280
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality:Ljava/lang/String;

    .line 281
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality_2:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality_2:Ljava/lang/String;

    .line 282
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality_3:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality_3:Ljava/lang/String;

    .line 283
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_1:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_1:Ljava/lang/String;

    .line 284
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_2:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_2:Ljava/lang/String;

    .line 285
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_3:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_3:Ljava/lang/String;

    .line 286
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->postal_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Address;->postal_code:Ljava/lang/String;

    .line 287
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Address;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 288
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->first_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Address;->first_name:Ljava/lang/String;

    .line 289
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->last_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Address;->last_name:Ljava/lang/String;

    .line 290
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->organization:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/resources/Address;->organization:Ljava/lang/String;

    .line 291
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 296
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_f

    .line 298
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Address;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 299
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_1:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 300
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_2:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 301
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_3:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 302
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->locality:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 303
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 304
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality_2:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 305
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality_3:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 306
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_1:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 307
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_2:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 308
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_3:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 309
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->postal_code:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 310
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Country;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 311
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->first_name:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 312
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->last_name:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 313
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->organization:Ljava/lang/String;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_e
    add-int/2addr v0, v2

    .line 314
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_f
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    .locals 2

    .line 251
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Address$Builder;-><init>()V

    .line 252
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_1:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->address_line_1:Ljava/lang/String;

    .line 253
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_2:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->address_line_2:Ljava/lang/String;

    .line 254
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_3:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->address_line_3:Ljava/lang/String;

    .line 255
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->locality:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->locality:Ljava/lang/String;

    .line 256
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->sublocality:Ljava/lang/String;

    .line 257
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality_2:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->sublocality_2:Ljava/lang/String;

    .line 258
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality_3:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->sublocality_3:Ljava/lang/String;

    .line 259
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_1:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->administrative_district_level_1:Ljava/lang/String;

    .line 260
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_2:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->administrative_district_level_2:Ljava/lang/String;

    .line 261
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_3:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->administrative_district_level_3:Ljava/lang/String;

    .line 262
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->postal_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->postal_code:Ljava/lang/String;

    .line 263
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 264
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->first_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->first_name:Ljava/lang/String;

    .line 265
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->last_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->last_name:Ljava/lang/String;

    .line 266
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->organization:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->organization:Ljava/lang/String;

    .line 267
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Address;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Address;->newBuilder()Lcom/squareup/protos/connect/v2/resources/Address$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 321
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 322
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_1:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", address_line_1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_2:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", address_line_2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_3:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", address_line_3="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_3:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->locality:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", locality="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->locality:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 326
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", sublocality="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality_2:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", sublocality_2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality_2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality_3:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", sublocality_3="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->sublocality_3:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_1:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", administrative_district_level_1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_2:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", administrative_district_level_2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 331
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_3:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", administrative_district_level_3="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_3:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->postal_code:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", postal_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->postal_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 333
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    if-eqz v1, :cond_b

    const-string v1, ", country="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 334
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->first_name:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", first_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->first_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->last_name:Ljava/lang/String;

    if-eqz v1, :cond_d

    const-string v1, ", last_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->last_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->organization:Ljava/lang/String;

    if-eqz v1, :cond_e

    const-string v1, ", organization="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Address;->organization:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_e
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Address{"

    .line 337
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
