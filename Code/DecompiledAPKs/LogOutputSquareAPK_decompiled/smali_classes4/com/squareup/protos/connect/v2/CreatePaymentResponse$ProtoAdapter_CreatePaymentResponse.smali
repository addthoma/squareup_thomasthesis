.class final Lcom/squareup/protos/connect/v2/CreatePaymentResponse$ProtoAdapter_CreatePaymentResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CreatePaymentResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/CreatePaymentResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CreatePaymentResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/CreatePaymentResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 171
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/CreatePaymentResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 192
    new-instance v0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;-><init>()V

    .line 193
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 194
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 200
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 198
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->encrypted_emv_data(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;

    goto :goto_0

    .line 197
    :cond_1
    sget-object v3, Lcom/squareup/protos/connect/v2/Payment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/Payment;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->payment(Lcom/squareup/protos/connect/v2/Payment;)Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;

    goto :goto_0

    .line 196
    :cond_2
    iget-object v3, v0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->errors:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 204
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 205
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->build()Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 169
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$ProtoAdapter_CreatePaymentResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/CreatePaymentResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 184
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->errors:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 185
    sget-object v0, Lcom/squareup/protos/connect/v2/Payment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->payment:Lcom/squareup/protos/connect/v2/Payment;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 186
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->encrypted_emv_data:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 187
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 169
    check-cast p2, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$ProtoAdapter_CreatePaymentResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/CreatePaymentResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/CreatePaymentResponse;)I
    .locals 4

    .line 176
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->errors:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/connect/v2/Payment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->payment:Lcom/squareup/protos/connect/v2/Payment;

    const/4 v3, 0x2

    .line 177
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->encrypted_emv_data:Ljava/lang/String;

    const/4 v3, 0x3

    .line 178
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 169
    check-cast p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$ProtoAdapter_CreatePaymentResponse;->encodedSize(Lcom/squareup/protos/connect/v2/CreatePaymentResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/CreatePaymentResponse;)Lcom/squareup/protos/connect/v2/CreatePaymentResponse;
    .locals 2

    .line 210
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->newBuilder()Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;

    move-result-object p1

    .line 211
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->errors:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 212
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->payment:Lcom/squareup/protos/connect/v2/Payment;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/Payment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->payment:Lcom/squareup/protos/connect/v2/Payment;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/Payment;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->payment:Lcom/squareup/protos/connect/v2/Payment;

    :cond_0
    const/4 v0, 0x0

    .line 213
    iput-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->encrypted_emv_data:Ljava/lang/String;

    .line 214
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 215
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->build()Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 169
    check-cast p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$ProtoAdapter_CreatePaymentResponse;->redact(Lcom/squareup/protos/connect/v2/CreatePaymentResponse;)Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    move-result-object p1

    return-object p1
.end method
