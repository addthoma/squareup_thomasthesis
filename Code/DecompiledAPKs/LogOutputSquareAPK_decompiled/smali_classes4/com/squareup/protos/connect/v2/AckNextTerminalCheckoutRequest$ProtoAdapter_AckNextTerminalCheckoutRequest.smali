.class final Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$ProtoAdapter_AckNextTerminalCheckoutRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AckNextTerminalCheckoutRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AckNextTerminalCheckoutRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 173
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 195
    new-instance v0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;-><init>()V

    .line 196
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 197
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 210
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 203
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->ack_reason(Lcom/squareup/protos/connect/v2/AckReasons$AckReason;)Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 205
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 200
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->device_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;

    goto :goto_0

    .line 199
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->idempotency_key(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;

    goto :goto_0

    .line 214
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 215
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->build()Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 171
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$ProtoAdapter_AckNextTerminalCheckoutRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 187
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->idempotency_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 188
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->device_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 189
    sget-object v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 190
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 171
    check-cast p2, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$ProtoAdapter_AckNextTerminalCheckoutRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;)I
    .locals 4

    .line 178
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->idempotency_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->device_id:Ljava/lang/String;

    const/4 v3, 0x2

    .line 179
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    const/4 v3, 0x3

    .line 180
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 171
    check-cast p1, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$ProtoAdapter_AckNextTerminalCheckoutRequest;->encodedSize(Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;)Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;
    .locals 0

    .line 220
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->newBuilder()Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;

    move-result-object p1

    .line 221
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 222
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->build()Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 171
    check-cast p1, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$ProtoAdapter_AckNextTerminalCheckoutRequest;->redact(Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;)Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;

    move-result-object p1

    return-object p1
.end method
