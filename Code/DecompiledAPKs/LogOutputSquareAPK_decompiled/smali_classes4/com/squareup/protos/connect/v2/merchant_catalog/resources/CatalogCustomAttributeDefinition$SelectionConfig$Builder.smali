.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogCustomAttributeDefinition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allowed_selections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig$CustomAttributeSelection;",
            ">;"
        }
    .end annotation
.end field

.field public max_allowed_selections:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 815
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 816
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig$Builder;->allowed_selections:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public allowed_selections(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig$CustomAttributeSelection;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig$Builder;"
        }
    .end annotation

    .line 835
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 836
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig$Builder;->allowed_selections:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;
    .locals 4

    .line 842
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig$Builder;->max_allowed_selections:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig$Builder;->allowed_selections:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;-><init>(Ljava/lang/Integer;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 810
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;

    move-result-object v0

    return-object v0
.end method

.method public max_allowed_selections(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig$Builder;
    .locals 0

    .line 826
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig$Builder;->max_allowed_selections:Ljava/lang/Integer;

    return-object p0
.end method
