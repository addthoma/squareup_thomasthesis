.class public final enum Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;
.super Ljava/lang/Enum;
.source "CatalogQuickAmountsSettingsOption.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption$ProtoAdapter_CatalogQuickAmountsSettingsOption;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AUTO:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

.field public static final enum DISABLED:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

.field public static final enum MANUAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

.field public static final enum QUICK_AMOUNTS_SETTINGS_OPTION_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 16
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    const/4 v1, 0x0

    const-string v2, "QUICK_AMOUNTS_SETTINGS_OPTION_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->QUICK_AMOUNTS_SETTINGS_OPTION_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    .line 23
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    const/4 v2, 0x1

    const-string v3, "DISABLED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->DISABLED:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    .line 30
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    const/4 v3, 0x2

    const-string v4, "MANUAL"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->MANUAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    .line 37
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    const/4 v4, 0x3

    const-string v5, "AUTO"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->AUTO:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    .line 15
    sget-object v5, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->QUICK_AMOUNTS_SETTINGS_OPTION_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->DISABLED:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->MANUAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->AUTO:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    .line 39
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption$ProtoAdapter_CatalogQuickAmountsSettingsOption;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption$ProtoAdapter_CatalogQuickAmountsSettingsOption;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 44
    iput p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 55
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->AUTO:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    return-object p0

    .line 54
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->MANUAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    return-object p0

    .line 53
    :cond_2
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->DISABLED:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    return-object p0

    .line 52
    :cond_3
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->QUICK_AMOUNTS_SETTINGS_OPTION_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 62
    iget v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->value:I

    return v0
.end method
