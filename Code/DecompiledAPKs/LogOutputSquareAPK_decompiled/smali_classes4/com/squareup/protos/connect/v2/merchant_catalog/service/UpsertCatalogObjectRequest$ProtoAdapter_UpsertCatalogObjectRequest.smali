.class final Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$ProtoAdapter_UpsertCatalogObjectRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "UpsertCatalogObjectRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_UpsertCatalogObjectRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 161
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 180
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;-><init>()V

    .line 181
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 182
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 187
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 185
    :cond_0
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->object(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;

    goto :goto_0

    .line 184
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->idempotency_key(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;

    goto :goto_0

    .line 191
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 192
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 159
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$ProtoAdapter_UpsertCatalogObjectRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 173
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->idempotency_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 174
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 175
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 159
    check-cast p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$ProtoAdapter_UpsertCatalogObjectRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;)I
    .locals 4

    .line 166
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->idempotency_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    const/4 v3, 0x2

    .line 167
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 159
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$ProtoAdapter_UpsertCatalogObjectRequest;->encodedSize(Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;
    .locals 2

    .line 197
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;

    move-result-object p1

    .line 198
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    .line 199
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 200
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 159
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$ProtoAdapter_UpsertCatalogObjectRequest;->redact(Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;

    move-result-object p1

    return-object p1
.end method
