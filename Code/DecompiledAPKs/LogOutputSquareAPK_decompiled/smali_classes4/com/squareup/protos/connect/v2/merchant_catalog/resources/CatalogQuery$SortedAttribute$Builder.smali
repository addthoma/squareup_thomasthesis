.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attribute_name:Ljava/lang/String;

.field public initial_attribute_value:Ljava/lang/String;

.field public sort_order:Lcom/squareup/protos/connect/v2/common/SortOrder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 526
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public attribute_name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute$Builder;
    .locals 0

    .line 533
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute$Builder;->attribute_name:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;
    .locals 5

    .line 559
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute$Builder;->attribute_name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute$Builder;->initial_attribute_value:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute$Builder;->sort_order:Lcom/squareup/protos/connect/v2/common/SortOrder;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/SortOrder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 519
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    move-result-object v0

    return-object v0
.end method

.method public initial_attribute_value(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute$Builder;
    .locals 0

    .line 545
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute$Builder;->initial_attribute_value:Ljava/lang/String;

    return-object p0
.end method

.method public sort_order(Lcom/squareup/protos/connect/v2/common/SortOrder;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute$Builder;
    .locals 0

    .line 553
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute$Builder;->sort_order:Lcom/squareup/protos/connect/v2/common/SortOrder;

    return-object p0
.end method
