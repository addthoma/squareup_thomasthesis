.class public final Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SourceApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;",
        "Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public application_id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public product:Lcom/squareup/protos/connect/v2/inventory/resources/Product;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 133
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public application_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;->application_id:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;
    .locals 5

    .line 170
    new-instance v0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;->product:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;->application_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;->name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;-><init>(Lcom/squareup/protos/connect/v2/inventory/resources/Product;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;->build()Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;

    move-result-object v0

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public product(Lcom/squareup/protos/connect/v2/inventory/resources/Product;)Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;->product:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    return-object p0
.end method
