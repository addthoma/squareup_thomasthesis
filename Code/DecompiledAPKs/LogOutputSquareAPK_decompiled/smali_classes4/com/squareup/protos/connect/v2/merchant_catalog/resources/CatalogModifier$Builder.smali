.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogModifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public modifier_list_id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public on_by_default:Ljava/lang/Boolean;

.field public ordinal:Ljava/lang/Integer;

.field public price_money:Lcom/squareup/protos/connect/v2/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 163
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;
    .locals 8

    .line 211
    new-instance v7, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;->on_by_default:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;->ordinal:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;->modifier_list_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 152
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    move-result-object v0

    return-object v0
.end method

.method public modifier_list_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;->modifier_list_id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public on_by_default(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;->on_by_default:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier$Builder;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method
