.class final Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$ProtoAdapter_CancelTerminalCheckoutResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CancelTerminalCheckoutResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CancelTerminalCheckoutResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 137
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 157
    new-instance v0, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$Builder;-><init>()V

    .line 158
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 159
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 164
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 162
    :cond_0
    sget-object v3, Lcom/squareup/protos/connect/v2/TerminalCheckout;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/TerminalCheckout;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$Builder;->checkout(Lcom/squareup/protos/connect/v2/TerminalCheckout;)Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$Builder;

    goto :goto_0

    .line 161
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$Builder;->errors:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 168
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 169
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$Builder;->build()Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 135
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$ProtoAdapter_CancelTerminalCheckoutResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 150
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;->errors:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 151
    sget-object v0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;->checkout:Lcom/squareup/protos/connect/v2/TerminalCheckout;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 152
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 135
    check-cast p2, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$ProtoAdapter_CancelTerminalCheckoutResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;)I
    .locals 4

    .line 142
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;->errors:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;->checkout:Lcom/squareup/protos/connect/v2/TerminalCheckout;

    const/4 v3, 0x2

    .line 143
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 144
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 135
    check-cast p1, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$ProtoAdapter_CancelTerminalCheckoutResponse;->encodedSize(Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;)Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;
    .locals 2

    .line 174
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;->newBuilder()Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$Builder;

    move-result-object p1

    .line 175
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$Builder;->errors:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 176
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$Builder;->checkout:Lcom/squareup/protos/connect/v2/TerminalCheckout;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$Builder;->checkout:Lcom/squareup/protos/connect/v2/TerminalCheckout;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/TerminalCheckout;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$Builder;->checkout:Lcom/squareup/protos/connect/v2/TerminalCheckout;

    .line 177
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 178
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$Builder;->build()Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 135
    check-cast p1, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse$ProtoAdapter_CancelTerminalCheckoutResponse;->redact(Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;)Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;

    move-result-object p1

    return-object p1
.end method
