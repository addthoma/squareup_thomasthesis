.class public final enum Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;
.super Ljava/lang/Enum;
.source "MeasurementUnit.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Length"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length$ProtoAdapter_Length;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum IMPERIAL_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

.field public static final enum IMPERIAL_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

.field public static final enum IMPERIAL_MILE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

.field public static final enum IMPERIAL_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

.field public static final enum INVALID_LENGTH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

.field public static final enum METRIC_CENTIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

.field public static final enum METRIC_KILOMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

.field public static final enum METRIC_METER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

.field public static final enum METRIC_MILLIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 631
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    const/4 v1, 0x0

    const-string v2, "INVALID_LENGTH"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->INVALID_LENGTH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    .line 638
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    const/4 v2, 0x1

    const-string v3, "IMPERIAL_INCH"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    .line 645
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    const/4 v3, 0x4

    const/4 v4, 0x2

    const-string v5, "IMPERIAL_FOOT"

    invoke-direct {v0, v5, v4, v3}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    .line 652
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    const/4 v5, 0x5

    const/4 v6, 0x3

    const-string v7, "IMPERIAL_YARD"

    invoke-direct {v0, v7, v6, v5}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    .line 659
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    const/4 v7, 0x6

    const-string v8, "IMPERIAL_MILE"

    invoke-direct {v0, v8, v3, v7}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_MILE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    .line 666
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    const-string v8, "METRIC_MILLIMETER"

    invoke-direct {v0, v8, v5, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_MILLIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    .line 673
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    const/4 v8, 0x7

    const-string v9, "METRIC_CENTIMETER"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_CENTIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    .line 680
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    const-string v9, "METRIC_METER"

    invoke-direct {v0, v9, v8, v6}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_METER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    .line 687
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    const/16 v9, 0x8

    const-string v10, "METRIC_KILOMETER"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_KILOMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    .line 630
    sget-object v10, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->INVALID_LENGTH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_MILE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_MILLIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_CENTIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_METER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_KILOMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->$VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    .line 689
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length$ProtoAdapter_Length;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length$ProtoAdapter_Length;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 693
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 694
    iput p3, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 710
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_KILOMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    return-object p0

    .line 708
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_CENTIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    return-object p0

    .line 706
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_MILE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    return-object p0

    .line 705
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    return-object p0

    .line 704
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    return-object p0

    .line 709
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_METER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    return-object p0

    .line 707
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_MILLIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    return-object p0

    .line 703
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    return-object p0

    .line 702
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->INVALID_LENGTH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;
    .locals 1

    .line 630
    const-class v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;
    .locals 1

    .line 630
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->$VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 717
    iget v0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->value:I

    return v0
.end method
