.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeleteCatalogObjectResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public deleted_at:Ljava/lang/String;

.field public deleted_object_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 128
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 129
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse$Builder;->errors:Ljava/util/List;

    .line 130
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse$Builder;->deleted_object_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;
    .locals 5

    .line 169
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse$Builder;->errors:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse$Builder;->deleted_object_ids:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse$Builder;->deleted_at:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;-><init>(Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;

    move-result-object v0

    return-object v0
.end method

.method public deleted_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse$Builder;->deleted_at:Ljava/lang/String;

    return-object p0
.end method

.method public deleted_object_ids(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse$Builder;"
        }
    .end annotation

    .line 151
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse$Builder;->deleted_object_ids:Ljava/util/List;

    return-object p0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse$Builder;"
        }
    .end annotation

    .line 137
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method
