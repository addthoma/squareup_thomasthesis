.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogCustomAttributeValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public boolean_value:Ljava/lang/Boolean;

.field public custom_attribute_definition_id:Ljava/lang/String;

.field public int_value:Ljava/lang/Long;

.field public key:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public number_value:Ljava/lang/String;

.field public selection_id_values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public string_value:Ljava/lang/String;

.field public type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 239
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 240
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->selection_id_values:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public boolean_value(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->boolean_value:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;
    .locals 12

    .line 321
    new-instance v11, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->string_value:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->int_value:Ljava/lang/Long;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->custom_attribute_definition_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->number_value:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->boolean_value:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->selection_id_values:Ljava/util/List;

    iget-object v9, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->key:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 220
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;

    move-result-object v0

    return-object v0
.end method

.method public custom_attribute_definition_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->custom_attribute_definition_id:Ljava/lang/String;

    return-object p0
.end method

.method public int_value(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;
    .locals 0

    .line 263
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->int_value:Ljava/lang/Long;

    return-object p0
.end method

.method public key(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;
    .locals 0

    .line 315
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->key:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;
    .locals 0

    .line 247
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public number_value(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;
    .locals 0

    .line 289
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->number_value:Ljava/lang/String;

    return-object p0
.end method

.method public selection_id_values(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;"
        }
    .end annotation

    .line 305
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 306
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->selection_id_values:Ljava/util/List;

    return-object p0
.end method

.method public string_value(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;
    .locals 0

    .line 255
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->string_value:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;
    .locals 0

    .line 279
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    return-object p0
.end method
