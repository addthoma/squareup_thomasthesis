.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BatchRetrieveCatalogObjectsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field

.field public objects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field public related_objects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 120
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 121
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;->errors:Ljava/util/List;

    .line 122
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;->objects:Ljava/util/List;

    .line 123
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;->related_objects:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;
    .locals 5

    .line 155
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;->errors:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;->objects:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;->related_objects:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;

    move-result-object v0

    return-object v0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;"
        }
    .end annotation

    .line 130
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method

.method public objects(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;"
        }
    .end annotation

    .line 139
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;->objects:Ljava/util/List;

    return-object p0
.end method

.method public related_objects(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;"
        }
    .end annotation

    .line 148
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 149
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse$Builder;->related_objects:Ljava/util/List;

    return-object p0
.end method
