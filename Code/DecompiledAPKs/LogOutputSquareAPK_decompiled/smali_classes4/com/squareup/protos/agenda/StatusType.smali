.class public final enum Lcom/squareup/protos/agenda/StatusType;
.super Ljava/lang/Enum;
.source "StatusType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/agenda/StatusType$ProtoAdapter_StatusType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/agenda/StatusType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/agenda/StatusType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/agenda/StatusType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum APPLICATION_ERROR:Lcom/squareup/protos/agenda/StatusType;

.field public static final enum SUCCESS:Lcom/squareup/protos/agenda/StatusType;

.field public static final enum VALIDATION_ERROR:Lcom/squareup/protos/agenda/StatusType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 17
    new-instance v0, Lcom/squareup/protos/agenda/StatusType;

    const/4 v1, 0x0

    const-string v2, "SUCCESS"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/agenda/StatusType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/StatusType;->SUCCESS:Lcom/squareup/protos/agenda/StatusType;

    .line 22
    new-instance v0, Lcom/squareup/protos/agenda/StatusType;

    const/4 v2, 0x1

    const-string v3, "VALIDATION_ERROR"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/agenda/StatusType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/StatusType;->VALIDATION_ERROR:Lcom/squareup/protos/agenda/StatusType;

    .line 27
    new-instance v0, Lcom/squareup/protos/agenda/StatusType;

    const/4 v3, 0x2

    const-string v4, "APPLICATION_ERROR"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/agenda/StatusType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/StatusType;->APPLICATION_ERROR:Lcom/squareup/protos/agenda/StatusType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/agenda/StatusType;

    .line 13
    sget-object v4, Lcom/squareup/protos/agenda/StatusType;->SUCCESS:Lcom/squareup/protos/agenda/StatusType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/agenda/StatusType;->VALIDATION_ERROR:Lcom/squareup/protos/agenda/StatusType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/agenda/StatusType;->APPLICATION_ERROR:Lcom/squareup/protos/agenda/StatusType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/agenda/StatusType;->$VALUES:[Lcom/squareup/protos/agenda/StatusType;

    .line 29
    new-instance v0, Lcom/squareup/protos/agenda/StatusType$ProtoAdapter_StatusType;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/StatusType$ProtoAdapter_StatusType;-><init>()V

    sput-object v0, Lcom/squareup/protos/agenda/StatusType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput p3, p0, Lcom/squareup/protos/agenda/StatusType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/agenda/StatusType;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 44
    :cond_0
    sget-object p0, Lcom/squareup/protos/agenda/StatusType;->APPLICATION_ERROR:Lcom/squareup/protos/agenda/StatusType;

    return-object p0

    .line 43
    :cond_1
    sget-object p0, Lcom/squareup/protos/agenda/StatusType;->VALIDATION_ERROR:Lcom/squareup/protos/agenda/StatusType;

    return-object p0

    .line 42
    :cond_2
    sget-object p0, Lcom/squareup/protos/agenda/StatusType;->SUCCESS:Lcom/squareup/protos/agenda/StatusType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/agenda/StatusType;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/agenda/StatusType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/agenda/StatusType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/agenda/StatusType;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/agenda/StatusType;->$VALUES:[Lcom/squareup/protos/agenda/StatusType;

    invoke-virtual {v0}, [Lcom/squareup/protos/agenda/StatusType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/agenda/StatusType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 51
    iget v0, p0, Lcom/squareup/protos/agenda/StatusType;->value:I

    return v0
.end method
