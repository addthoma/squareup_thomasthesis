.class public final Lcom/squareup/protos/agenda/ClientInstallationIdentity;
.super Lcom/squareup/wire/Message;
.source "ClientInstallationIdentity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/agenda/ClientInstallationIdentity$ProtoAdapter_ClientInstallationIdentity;,
        Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/agenda/ClientInstallationIdentity;",
        "Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/agenda/ClientInstallationIdentity;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FULLY_QUALIFIED_APPLICATION_IDENTIFIER:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final device:Lcom/squareup/protos/common/client/Device;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.client.Device#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final fully_qualified_application_identifier:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final product:Lcom/squareup/protos/common/client/Product;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.client.Product#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/agenda/ClientInstallationIdentity$ProtoAdapter_ClientInstallationIdentity;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/ClientInstallationIdentity$ProtoAdapter_ClientInstallationIdentity;-><init>()V

    sput-object v0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/client/Product;Lcom/squareup/protos/common/client/Device;Ljava/lang/String;)V
    .locals 1

    .line 61
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/agenda/ClientInstallationIdentity;-><init>(Lcom/squareup/protos/common/client/Product;Lcom/squareup/protos/common/client/Device;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/client/Product;Lcom/squareup/protos/common/client/Device;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 66
    sget-object v0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 67
    iput-object p1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->product:Lcom/squareup/protos/common/client/Product;

    .line 68
    iput-object p2, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->device:Lcom/squareup/protos/common/client/Device;

    .line 69
    iput-object p3, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->fully_qualified_application_identifier:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 85
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 86
    :cond_1
    check-cast p1, Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->product:Lcom/squareup/protos/common/client/Product;

    iget-object v3, p1, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->product:Lcom/squareup/protos/common/client/Product;

    .line 88
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->device:Lcom/squareup/protos/common/client/Device;

    iget-object v3, p1, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->device:Lcom/squareup/protos/common/client/Device;

    .line 89
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->fully_qualified_application_identifier:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->fully_qualified_application_identifier:Ljava/lang/String;

    .line 90
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 95
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->product:Lcom/squareup/protos/common/client/Product;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/client/Product;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->device:Lcom/squareup/protos/common/client/Device;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/client/Device;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->fully_qualified_application_identifier:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 101
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;
    .locals 2

    .line 74
    new-instance v0, Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;-><init>()V

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->product:Lcom/squareup/protos/common/client/Product;

    iput-object v1, v0, Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;->product:Lcom/squareup/protos/common/client/Product;

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->device:Lcom/squareup/protos/common/client/Device;

    iput-object v1, v0, Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;->device:Lcom/squareup/protos/common/client/Device;

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->fully_qualified_application_identifier:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;->fully_qualified_application_identifier:Ljava/lang/String;

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->newBuilder()Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->product:Lcom/squareup/protos/common/client/Product;

    if-eqz v1, :cond_0

    const-string v1, ", product="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->product:Lcom/squareup/protos/common/client/Product;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->device:Lcom/squareup/protos/common/client/Device;

    if-eqz v1, :cond_1

    const-string v1, ", device="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->device:Lcom/squareup/protos/common/client/Device;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 111
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->fully_qualified_application_identifier:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", fully_qualified_application_identifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->fully_qualified_application_identifier:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ClientInstallationIdentity{"

    .line 112
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
