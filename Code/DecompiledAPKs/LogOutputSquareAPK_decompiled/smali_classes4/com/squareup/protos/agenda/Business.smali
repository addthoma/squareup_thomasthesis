.class public final Lcom/squareup/protos/agenda/Business;
.super Lcom/squareup/wire/Message;
.source "Business.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/agenda/Business$ProtoAdapter_Business;,
        Lcom/squareup/protos/agenda/Business$NoShowChargeType;,
        Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;,
        Lcom/squareup/protos/agenda/Business$LocationType;,
        Lcom/squareup/protos/agenda/Business$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/agenda/Business;",
        "Lcom/squareup/protos/agenda/Business$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/agenda/Business;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BANK_ACCOUNT_VERIFIED:Ljava/lang/Boolean;

.field public static final DEFAULT_BOOKING_SITES_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_BUYER_PREPAYMENT_MODE:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

.field public static final DEFAULT_CONFIRMATION_LEAD_TIME:Ljava/lang/Integer;

.field public static final DEFAULT_CONVERSATIONS_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_ELIGIBLE_FOR_RESERVE_WITH_GOOGLE:Ljava/lang/Boolean;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_IS_SOLE_PROPRIETOR:Ljava/lang/Boolean;

.field public static final DEFAULT_LOCATION_TYPE:Lcom/squareup/protos/agenda/Business$LocationType;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_NEEDS_MOBILE_ONBOARDING:Ljava/lang/Boolean;

.field public static final DEFAULT_NO_SHOW_CHARGE_TYPE:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

.field public static final DEFAULT_NO_SHOW_CUTOFF_TIME:Ljava/lang/Long;

.field public static final DEFAULT_NO_SHOW_PERCENTAGE:Ljava/lang/Integer;

.field public static final DEFAULT_NO_SHOW_PROTECTION_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_PHONE_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_REMINDER_EMAIL_LEAD_TIME:Ljava/lang/Integer;

.field public static final DEFAULT_REMINDER_SMS_LEAD_TIME:Ljava/lang/Integer;

.field public static final DEFAULT_RESERVE_WITH_GOOGLE_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_SEND_CONFIRMATION_EMAIL:Ljava/lang/Boolean;

.field public static final DEFAULT_SEND_CONFIRMATION_SMS:Ljava/lang/Boolean;

.field public static final DEFAULT_SEND_REMINDER_EMAIL:Ljava/lang/Boolean;

.field public static final DEFAULT_SEND_REMINDER_SMS:Ljava/lang/Boolean;

.field public static final DEFAULT_TIME_ZONE:Ljava/lang/String; = ""

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final address:Lcom/squareup/protos/common/location/GlobalAddress;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.location.GlobalAddress#ADAPTER"
        redacted = true
        tag = 0x7
    .end annotation
.end field

.field public final bank_account_verified:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xf
    .end annotation
.end field

.field public final booking_sites_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x10
    .end annotation
.end field

.field public final buyer_prepayment_mode:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.agenda.Business$BuyerPrepaymentMode#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final confirmation_lead_time:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x19
    .end annotation
.end field

.field public final conversations_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1a
    .end annotation
.end field

.field public final eligible_for_reserve_with_google:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x16
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final is_sole_proprietor:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1b
    .end annotation
.end field

.field public final location_type:Lcom/squareup/protos/agenda/Business$LocationType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.agenda.Business$LocationType#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final needs_mobile_onboarding:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final no_show_charge_type:Lcom/squareup/protos/agenda/Business$NoShowChargeType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.agenda.Business$NoShowChargeType#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final no_show_cutoff_time:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x8
    .end annotation
.end field

.field public final no_show_flat_fee:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final no_show_percentage:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xd
    .end annotation
.end field

.field public final no_show_protection_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final phone_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final reminder_email_lead_time:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x14
    .end annotation
.end field

.field public final reminder_sms_lead_time:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x13
    .end annotation
.end field

.field public final reserve_with_google_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x15
    .end annotation
.end field

.field public final send_confirmation_email:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x18
    .end annotation
.end field

.field public final send_confirmation_sms:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x17
    .end annotation
.end field

.field public final send_reminder_email:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x12
    .end annotation
.end field

.field public final send_reminder_sms:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x11
    .end annotation
.end field

.field public final time_zone:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 30
    new-instance v0, Lcom/squareup/protos/agenda/Business$ProtoAdapter_Business;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/Business$ProtoAdapter_Business;-><init>()V

    sput-object v0, Lcom/squareup/protos/agenda/Business;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 40
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/agenda/Business;->DEFAULT_NEEDS_MOBILE_ONBOARDING:Ljava/lang/Boolean;

    .line 42
    sget-object v2, Lcom/squareup/protos/agenda/Business$LocationType;->PREMISES:Lcom/squareup/protos/agenda/Business$LocationType;

    sput-object v2, Lcom/squareup/protos/agenda/Business;->DEFAULT_LOCATION_TYPE:Lcom/squareup/protos/agenda/Business$LocationType;

    const-wide/16 v2, 0x0

    .line 46
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    sput-object v2, Lcom/squareup/protos/agenda/Business;->DEFAULT_NO_SHOW_CUTOFF_TIME:Ljava/lang/Long;

    .line 50
    sput-object v0, Lcom/squareup/protos/agenda/Business;->DEFAULT_NO_SHOW_PROTECTION_ENABLED:Ljava/lang/Boolean;

    .line 52
    sget-object v2, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;->OFF:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    sput-object v2, Lcom/squareup/protos/agenda/Business;->DEFAULT_BUYER_PREPAYMENT_MODE:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    .line 54
    sget-object v2, Lcom/squareup/protos/agenda/Business$NoShowChargeType;->PERCENTAGE:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    sput-object v2, Lcom/squareup/protos/agenda/Business;->DEFAULT_NO_SHOW_CHARGE_TYPE:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    .line 56
    sput-object v1, Lcom/squareup/protos/agenda/Business;->DEFAULT_NO_SHOW_PERCENTAGE:Ljava/lang/Integer;

    .line 58
    sput-object v0, Lcom/squareup/protos/agenda/Business;->DEFAULT_BANK_ACCOUNT_VERIFIED:Ljava/lang/Boolean;

    .line 60
    sput-object v0, Lcom/squareup/protos/agenda/Business;->DEFAULT_BOOKING_SITES_ENABLED:Ljava/lang/Boolean;

    .line 62
    sput-object v0, Lcom/squareup/protos/agenda/Business;->DEFAULT_SEND_REMINDER_SMS:Ljava/lang/Boolean;

    .line 64
    sput-object v0, Lcom/squareup/protos/agenda/Business;->DEFAULT_SEND_REMINDER_EMAIL:Ljava/lang/Boolean;

    .line 66
    sput-object v1, Lcom/squareup/protos/agenda/Business;->DEFAULT_REMINDER_SMS_LEAD_TIME:Ljava/lang/Integer;

    .line 68
    sput-object v1, Lcom/squareup/protos/agenda/Business;->DEFAULT_REMINDER_EMAIL_LEAD_TIME:Ljava/lang/Integer;

    .line 70
    sput-object v0, Lcom/squareup/protos/agenda/Business;->DEFAULT_RESERVE_WITH_GOOGLE_ENABLED:Ljava/lang/Boolean;

    .line 72
    sput-object v0, Lcom/squareup/protos/agenda/Business;->DEFAULT_ELIGIBLE_FOR_RESERVE_WITH_GOOGLE:Ljava/lang/Boolean;

    .line 74
    sput-object v0, Lcom/squareup/protos/agenda/Business;->DEFAULT_SEND_CONFIRMATION_SMS:Ljava/lang/Boolean;

    .line 76
    sput-object v0, Lcom/squareup/protos/agenda/Business;->DEFAULT_SEND_CONFIRMATION_EMAIL:Ljava/lang/Boolean;

    .line 78
    sput-object v1, Lcom/squareup/protos/agenda/Business;->DEFAULT_CONFIRMATION_LEAD_TIME:Ljava/lang/Integer;

    .line 80
    sput-object v0, Lcom/squareup/protos/agenda/Business;->DEFAULT_CONVERSATIONS_ENABLED:Ljava/lang/Boolean;

    .line 82
    sput-object v0, Lcom/squareup/protos/agenda/Business;->DEFAULT_IS_SOLE_PROPRIETOR:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/agenda/Business$Builder;Lokio/ByteString;)V
    .locals 1

    .line 288
    sget-object v0, Lcom/squareup/protos/agenda/Business;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 289
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->id:Ljava/lang/String;

    .line 290
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->name:Ljava/lang/String;

    .line 291
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->phone_number:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->phone_number:Ljava/lang/String;

    .line 292
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->needs_mobile_onboarding:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->needs_mobile_onboarding:Ljava/lang/Boolean;

    .line 293
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->location_type:Lcom/squareup/protos/agenda/Business$LocationType;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->location_type:Lcom/squareup/protos/agenda/Business$LocationType;

    .line 294
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->time_zone:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->time_zone:Ljava/lang/String;

    .line 295
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 296
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->no_show_cutoff_time:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->no_show_cutoff_time:Ljava/lang/Long;

    .line 297
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->uid:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->uid:Ljava/lang/String;

    .line 298
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->no_show_protection_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->no_show_protection_enabled:Ljava/lang/Boolean;

    .line 299
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->buyer_prepayment_mode:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->buyer_prepayment_mode:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    .line 300
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->no_show_charge_type:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->no_show_charge_type:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    .line 301
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->no_show_percentage:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->no_show_percentage:Ljava/lang/Integer;

    .line 302
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->no_show_flat_fee:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->no_show_flat_fee:Lcom/squareup/protos/common/Money;

    .line 303
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->bank_account_verified:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->bank_account_verified:Ljava/lang/Boolean;

    .line 304
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->booking_sites_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->booking_sites_enabled:Ljava/lang/Boolean;

    .line 305
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->send_reminder_sms:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->send_reminder_sms:Ljava/lang/Boolean;

    .line 306
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->send_reminder_email:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->send_reminder_email:Ljava/lang/Boolean;

    .line 307
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->reminder_sms_lead_time:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->reminder_sms_lead_time:Ljava/lang/Integer;

    .line 308
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->reminder_email_lead_time:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->reminder_email_lead_time:Ljava/lang/Integer;

    .line 309
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->reserve_with_google_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->reserve_with_google_enabled:Ljava/lang/Boolean;

    .line 310
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->eligible_for_reserve_with_google:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->eligible_for_reserve_with_google:Ljava/lang/Boolean;

    .line 311
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->send_confirmation_sms:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->send_confirmation_sms:Ljava/lang/Boolean;

    .line 312
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->send_confirmation_email:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->send_confirmation_email:Ljava/lang/Boolean;

    .line 313
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->confirmation_lead_time:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->confirmation_lead_time:Ljava/lang/Integer;

    .line 314
    iget-object p2, p1, Lcom/squareup/protos/agenda/Business$Builder;->conversations_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/agenda/Business;->conversations_enabled:Ljava/lang/Boolean;

    .line 315
    iget-object p1, p1, Lcom/squareup/protos/agenda/Business$Builder;->is_sole_proprietor:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/squareup/protos/agenda/Business;->is_sole_proprietor:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 355
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/agenda/Business;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 356
    :cond_1
    check-cast p1, Lcom/squareup/protos/agenda/Business;

    .line 357
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/Business;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/agenda/Business;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->id:Ljava/lang/String;

    .line 358
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->name:Ljava/lang/String;

    .line 359
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->phone_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->phone_number:Ljava/lang/String;

    .line 360
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->needs_mobile_onboarding:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->needs_mobile_onboarding:Ljava/lang/Boolean;

    .line 361
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->location_type:Lcom/squareup/protos/agenda/Business$LocationType;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->location_type:Lcom/squareup/protos/agenda/Business$LocationType;

    .line 362
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->time_zone:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->time_zone:Ljava/lang/String;

    .line 363
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 364
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_cutoff_time:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->no_show_cutoff_time:Ljava/lang/Long;

    .line 365
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->uid:Ljava/lang/String;

    .line 366
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_protection_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->no_show_protection_enabled:Ljava/lang/Boolean;

    .line 367
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->buyer_prepayment_mode:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->buyer_prepayment_mode:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    .line 368
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_charge_type:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->no_show_charge_type:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    .line 369
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_percentage:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->no_show_percentage:Ljava/lang/Integer;

    .line 370
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_flat_fee:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->no_show_flat_fee:Lcom/squareup/protos/common/Money;

    .line 371
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->bank_account_verified:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->bank_account_verified:Ljava/lang/Boolean;

    .line 372
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->booking_sites_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->booking_sites_enabled:Ljava/lang/Boolean;

    .line 373
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_reminder_sms:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->send_reminder_sms:Ljava/lang/Boolean;

    .line 374
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_reminder_email:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->send_reminder_email:Ljava/lang/Boolean;

    .line 375
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->reminder_sms_lead_time:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->reminder_sms_lead_time:Ljava/lang/Integer;

    .line 376
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->reminder_email_lead_time:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->reminder_email_lead_time:Ljava/lang/Integer;

    .line 377
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->reserve_with_google_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->reserve_with_google_enabled:Ljava/lang/Boolean;

    .line 378
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->eligible_for_reserve_with_google:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->eligible_for_reserve_with_google:Ljava/lang/Boolean;

    .line 379
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_confirmation_sms:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->send_confirmation_sms:Ljava/lang/Boolean;

    .line 380
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_confirmation_email:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->send_confirmation_email:Ljava/lang/Boolean;

    .line 381
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->confirmation_lead_time:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->confirmation_lead_time:Ljava/lang/Integer;

    .line 382
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->conversations_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Business;->conversations_enabled:Ljava/lang/Boolean;

    .line 383
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->is_sole_proprietor:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/agenda/Business;->is_sole_proprietor:Ljava/lang/Boolean;

    .line 384
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 389
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1b

    .line 391
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/Business;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 392
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 393
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 394
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->phone_number:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 395
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->needs_mobile_onboarding:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 396
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->location_type:Lcom/squareup/protos/agenda/Business$LocationType;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/agenda/Business$LocationType;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 397
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->time_zone:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 398
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/location/GlobalAddress;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 399
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_cutoff_time:Ljava/lang/Long;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 400
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->uid:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 401
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_protection_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 402
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->buyer_prepayment_mode:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 403
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_charge_type:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/agenda/Business$NoShowChargeType;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 404
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_percentage:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 405
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_flat_fee:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 406
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->bank_account_verified:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 407
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->booking_sites_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 408
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_reminder_sms:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 409
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_reminder_email:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 410
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->reminder_sms_lead_time:Ljava/lang/Integer;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 411
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->reminder_email_lead_time:Ljava/lang/Integer;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 412
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->reserve_with_google_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 413
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->eligible_for_reserve_with_google:Ljava/lang/Boolean;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 414
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_confirmation_sms:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 415
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_confirmation_email:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 416
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->confirmation_lead_time:Ljava/lang/Integer;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 417
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->conversations_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 418
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->is_sole_proprietor:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_1a
    add-int/2addr v0, v2

    .line 419
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1b
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/agenda/Business$Builder;
    .locals 2

    .line 320
    new-instance v0, Lcom/squareup/protos/agenda/Business$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/Business$Builder;-><init>()V

    .line 321
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->id:Ljava/lang/String;

    .line 322
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->name:Ljava/lang/String;

    .line 323
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->phone_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->phone_number:Ljava/lang/String;

    .line 324
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->needs_mobile_onboarding:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->needs_mobile_onboarding:Ljava/lang/Boolean;

    .line 325
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->location_type:Lcom/squareup/protos/agenda/Business$LocationType;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->location_type:Lcom/squareup/protos/agenda/Business$LocationType;

    .line 326
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->time_zone:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->time_zone:Ljava/lang/String;

    .line 327
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 328
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_cutoff_time:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->no_show_cutoff_time:Ljava/lang/Long;

    .line 329
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->uid:Ljava/lang/String;

    .line 330
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_protection_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->no_show_protection_enabled:Ljava/lang/Boolean;

    .line 331
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->buyer_prepayment_mode:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->buyer_prepayment_mode:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    .line 332
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_charge_type:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->no_show_charge_type:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    .line 333
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_percentage:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->no_show_percentage:Ljava/lang/Integer;

    .line 334
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_flat_fee:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->no_show_flat_fee:Lcom/squareup/protos/common/Money;

    .line 335
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->bank_account_verified:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->bank_account_verified:Ljava/lang/Boolean;

    .line 336
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->booking_sites_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->booking_sites_enabled:Ljava/lang/Boolean;

    .line 337
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_reminder_sms:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->send_reminder_sms:Ljava/lang/Boolean;

    .line 338
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_reminder_email:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->send_reminder_email:Ljava/lang/Boolean;

    .line 339
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->reminder_sms_lead_time:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->reminder_sms_lead_time:Ljava/lang/Integer;

    .line 340
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->reminder_email_lead_time:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->reminder_email_lead_time:Ljava/lang/Integer;

    .line 341
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->reserve_with_google_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->reserve_with_google_enabled:Ljava/lang/Boolean;

    .line 342
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->eligible_for_reserve_with_google:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->eligible_for_reserve_with_google:Ljava/lang/Boolean;

    .line 343
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_confirmation_sms:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->send_confirmation_sms:Ljava/lang/Boolean;

    .line 344
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_confirmation_email:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->send_confirmation_email:Ljava/lang/Boolean;

    .line 345
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->confirmation_lead_time:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->confirmation_lead_time:Ljava/lang/Integer;

    .line 346
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->conversations_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->conversations_enabled:Ljava/lang/Boolean;

    .line 347
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->is_sole_proprietor:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Business$Builder;->is_sole_proprietor:Ljava/lang/Boolean;

    .line 348
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/Business;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/agenda/Business$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/Business;->newBuilder()Lcom/squareup/protos/agenda/Business$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 426
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 427
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 428
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->phone_number:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", phone_number=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 430
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->needs_mobile_onboarding:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", needs_mobile_onboarding="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->needs_mobile_onboarding:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 431
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->location_type:Lcom/squareup/protos/agenda/Business$LocationType;

    if-eqz v1, :cond_4

    const-string v1, ", location_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->location_type:Lcom/squareup/protos/agenda/Business$LocationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 432
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->time_zone:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", time_zone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->time_zone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 433
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v1, :cond_6

    const-string v1, ", address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_cutoff_time:Ljava/lang/Long;

    if-eqz v1, :cond_7

    const-string v1, ", no_show_cutoff_time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_cutoff_time:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 435
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->uid:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 436
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_protection_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", no_show_protection_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_protection_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 437
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->buyer_prepayment_mode:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    if-eqz v1, :cond_a

    const-string v1, ", buyer_prepayment_mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->buyer_prepayment_mode:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 438
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_charge_type:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    if-eqz v1, :cond_b

    const-string v1, ", no_show_charge_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_charge_type:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 439
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_percentage:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    const-string v1, ", no_show_percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_percentage:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 440
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_flat_fee:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_d

    const-string v1, ", no_show_flat_fee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->no_show_flat_fee:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 441
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->bank_account_verified:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    const-string v1, ", bank_account_verified="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->bank_account_verified:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 442
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->booking_sites_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    const-string v1, ", booking_sites_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->booking_sites_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 443
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_reminder_sms:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    const-string v1, ", send_reminder_sms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_reminder_sms:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 444
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_reminder_email:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    const-string v1, ", send_reminder_email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_reminder_email:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 445
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->reminder_sms_lead_time:Ljava/lang/Integer;

    if-eqz v1, :cond_12

    const-string v1, ", reminder_sms_lead_time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->reminder_sms_lead_time:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 446
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->reminder_email_lead_time:Ljava/lang/Integer;

    if-eqz v1, :cond_13

    const-string v1, ", reminder_email_lead_time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->reminder_email_lead_time:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 447
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->reserve_with_google_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    const-string v1, ", reserve_with_google_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->reserve_with_google_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 448
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->eligible_for_reserve_with_google:Ljava/lang/Boolean;

    if-eqz v1, :cond_15

    const-string v1, ", eligible_for_reserve_with_google="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->eligible_for_reserve_with_google:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 449
    :cond_15
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_confirmation_sms:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    const-string v1, ", send_confirmation_sms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_confirmation_sms:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 450
    :cond_16
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_confirmation_email:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    const-string v1, ", send_confirmation_email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->send_confirmation_email:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 451
    :cond_17
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->confirmation_lead_time:Ljava/lang/Integer;

    if-eqz v1, :cond_18

    const-string v1, ", confirmation_lead_time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->confirmation_lead_time:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 452
    :cond_18
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->conversations_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    const-string v1, ", conversations_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->conversations_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 453
    :cond_19
    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->is_sole_proprietor:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    const-string v1, ", is_sole_proprietor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Business;->is_sole_proprietor:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Business{"

    .line 454
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
