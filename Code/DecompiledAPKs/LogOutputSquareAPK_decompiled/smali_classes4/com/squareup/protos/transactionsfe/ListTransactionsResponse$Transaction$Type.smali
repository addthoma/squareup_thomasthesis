.class public final enum Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;
.super Ljava/lang/Enum;
.source "ListTransactionsResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DO_NOT_USE_TYPE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

.field public static final enum EXCHANGE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

.field public static final enum REFUND:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

.field public static final enum SALE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 525
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE_TYPE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->DO_NOT_USE_TYPE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    .line 530
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    const/4 v2, 0x1

    const-string v3, "SALE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->SALE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    .line 535
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    const/4 v3, 0x2

    const-string v4, "REFUND"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->REFUND:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    .line 540
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    const/4 v4, 0x3

    const-string v5, "EXCHANGE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->EXCHANGE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    .line 521
    sget-object v5, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->DO_NOT_USE_TYPE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->SALE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->REFUND:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->EXCHANGE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->$VALUES:[Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    .line 542
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 546
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 547
    iput p3, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 558
    :cond_0
    sget-object p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->EXCHANGE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    return-object p0

    .line 557
    :cond_1
    sget-object p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->REFUND:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    return-object p0

    .line 556
    :cond_2
    sget-object p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->SALE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    return-object p0

    .line 555
    :cond_3
    sget-object p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->DO_NOT_USE_TYPE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;
    .locals 1

    .line 521
    const-class v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;
    .locals 1

    .line 521
    sget-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->$VALUES:[Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    invoke-virtual {v0}, [Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 565
    iget v0, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->value:I

    return v0
.end method
