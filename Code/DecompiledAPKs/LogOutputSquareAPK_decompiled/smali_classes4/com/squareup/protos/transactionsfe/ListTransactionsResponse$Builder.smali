.class public final Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListTransactionsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

.field public transaction:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 112
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->transaction:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;
    .locals 4

    .line 131
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->transaction:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;

    move-result-object v0

    return-object v0
.end method

.method public next_page_request(Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    return-object p0
.end method

.method public transaction(Ljava/util/List;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;",
            ">;)",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;"
        }
    .end annotation

    .line 116
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->transaction:Ljava/util/List;

    return-object p0
.end method
