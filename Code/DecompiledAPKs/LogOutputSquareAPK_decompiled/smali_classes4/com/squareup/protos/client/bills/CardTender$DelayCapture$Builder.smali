.class public final Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender$DelayCapture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CardTender$DelayCapture;",
        "Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public reason:Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 877
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/CardTender$DelayCapture;
    .locals 3

    .line 887
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;->reason:Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/CardTender$DelayCapture;-><init>(Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 874
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    move-result-object v0

    return-object v0
.end method

.method public reason(Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;)Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;
    .locals 0

    .line 881
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;->reason:Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;

    return-object p0
.end method
