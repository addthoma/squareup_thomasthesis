.class public final Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;
.super Lcom/squareup/wire/Message;
.source "ManualMergeContactsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$ProtoAdapter_ManualMergeContactsRequest;,
        Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;",
        "Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_REQUEST_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.ContactSet#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.DuplicateContactTokenSet#ADAPTER"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyAccountMapping#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final request_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$ProtoAdapter_ManualMergeContactsRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$ProtoAdapter_ManualMergeContactsRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/ContactSet;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)V
    .locals 6

    .line 64
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;-><init>(Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/ContactSet;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/ContactSet;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Lokio/ByteString;)V
    .locals 1

    .line 70
    sget-object v0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 71
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    .line 72
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->request_token:Ljava/lang/String;

    .line 73
    iput-object p3, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    .line 74
    iput-object p4, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 91
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 92
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    .line 94
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->request_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->request_token:Ljava/lang/String;

    .line 95
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    .line 96
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 97
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 102
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->request_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/ContactSet;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 109
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;
    .locals 2

    .line 79
    new-instance v0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;-><init>()V

    .line 80
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->request_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->request_token:Ljava/lang/String;

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->newBuilder()Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    if-eqz v1, :cond_0

    const-string v1, ", duplicate_contact_token_set="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->request_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", request_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->request_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    if-eqz v1, :cond_2

    const-string v1, ", contact_set="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 120
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    if-eqz v1, :cond_3

    const-string v1, ", loyaltyAccountMapping="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ManualMergeContactsRequest{"

    .line 121
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
