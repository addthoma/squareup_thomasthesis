.class public final enum Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;
.super Ljava/lang/Enum;
.source "CardData.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData$A10Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EntryMethod"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod$ProtoAdapter_EntryMethod;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CONTACT:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

.field public static final enum CONTACTLESS:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

.field public static final enum DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

.field public static final enum SWIPED:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1658
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    const/4 v1, 0x0

    const-string v2, "DEFAULT_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    .line 1660
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    const/4 v2, 0x1

    const-string v3, "CONTACT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->CONTACT:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    .line 1662
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    const/4 v3, 0x2

    const-string v4, "CONTACTLESS"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    .line 1664
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    const/4 v4, 0x3

    const-string v5, "SWIPED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    .line 1657
    sget-object v5, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->CONTACT:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->$VALUES:[Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    .line 1666
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod$ProtoAdapter_EntryMethod;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod$ProtoAdapter_EntryMethod;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1670
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1671
    iput p3, p0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 1682
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    return-object p0

    .line 1681
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    return-object p0

    .line 1680
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->CONTACT:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    return-object p0

    .line 1679
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;
    .locals 1

    .line 1657
    const-class v0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;
    .locals 1

    .line 1657
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->$VALUES:[Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1689
    iget v0, p0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->value:I

    return v0
.end method
