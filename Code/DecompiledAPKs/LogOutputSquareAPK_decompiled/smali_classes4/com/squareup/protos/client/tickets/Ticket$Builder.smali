.class public final Lcom/squareup/protos/client/tickets/Ticket$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Ticket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tickets/Ticket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tickets/Ticket;",
        "Lcom/squareup/protos/client/tickets/Ticket$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cart:Lcom/squareup/protos/client/bills/Cart;

.field public client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

.field public close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

.field public closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;

.field public name:Ljava/lang/String;

.field public note:Ljava/lang/String;

.field public tender_id_deprecated:Ljava/lang/String;

.field public ticket_id_pair:Lcom/squareup/protos/client/IdPair;

.field public total_money:Lcom/squareup/protos/common/Money;

.field public transient_:Lcom/squareup/protos/client/tickets/Transient;

.field public vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 281
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/tickets/Ticket;
    .locals 14

    .line 397
    new-instance v13, Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    iget-object v3, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v4, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v6, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->note:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    iget-object v8, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v9, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->tender_id_deprecated:Ljava/lang/String;

    iget-object v10, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v11, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->transient_:Lcom/squareup/protos/client/tickets/Transient;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/client/tickets/Ticket;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/VectorClock;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/tickets/Transient;Lokio/ByteString;)V

    return-object v13
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 258
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object v0

    return-object v0
.end method

.method public cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/tickets/Ticket$Builder;
    .locals 0

    .line 313
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object p0
.end method

.method public client_updated_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/tickets/Ticket$Builder;
    .locals 0

    .line 332
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public close_event(Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;)Lcom/squareup/protos/client/tickets/Ticket$Builder;
    .locals 0

    .line 351
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    return-object p0
.end method

.method public closed_at_deprecated(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/tickets/Ticket$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 383
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/tickets/Ticket$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 324
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/protos/client/tickets/Ticket$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 343
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public tender_id_deprecated(Ljava/lang/String;)Lcom/squareup/protos/client/tickets/Ticket$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 374
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->tender_id_deprecated:Ljava/lang/String;

    return-object p0
.end method

.method public ticket_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/tickets/Ticket$Builder;
    .locals 0

    .line 291
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/tickets/Ticket$Builder;
    .locals 0

    .line 365
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->total_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public transient_(Lcom/squareup/protos/client/tickets/Transient;)Lcom/squareup/protos/client/tickets/Ticket$Builder;
    .locals 0

    .line 391
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->transient_:Lcom/squareup/protos/client/tickets/Transient;

    return-object p0
.end method

.method public vector_clock(Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/protos/client/tickets/Ticket$Builder;
    .locals 0

    .line 305
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    return-object p0
.end method
