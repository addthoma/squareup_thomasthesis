.class final Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$ProtoAdapter_ReportCoredumpRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReportCoredumpRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReportCoredumpRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 239
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 268
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;-><init>()V

    .line 269
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 270
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 294
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 292
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->user_agent(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;

    goto :goto_0

    .line 286
    :pswitch_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/tarkin/ChipType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/tarkin/ChipType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->chip_type(Lcom/squareup/protos/client/tarkin/ChipType;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 288
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 283
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;

    goto :goto_0

    .line 282
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->reader_serial_number(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;

    goto :goto_0

    .line 276
    :pswitch_4
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/tarkin/ReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/tarkin/ReaderType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->reader_type(Lcom/squareup/protos/client/tarkin/ReaderType;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 278
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 273
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->coredump_data(Lokio/ByteString;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;

    goto :goto_0

    .line 272
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->coredump_key(Lokio/ByteString;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;

    goto :goto_0

    .line 298
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 299
    invoke-virtual {v0}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->build()Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 237
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$ProtoAdapter_ReportCoredumpRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 256
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_key:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 257
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_data:Lokio/ByteString;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 258
    sget-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 259
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_serial_number:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 260
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 261
    sget-object v0, Lcom/squareup/protos/client/tarkin/ChipType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->chip_type:Lcom/squareup/protos/client/tarkin/ChipType;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 262
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->user_agent:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 263
    invoke-virtual {p2}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 237
    check-cast p2, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$ProtoAdapter_ReportCoredumpRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;)I
    .locals 4

    .line 244
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_key:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_data:Lokio/ByteString;

    const/4 v3, 0x2

    .line 245
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/tarkin/ReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    const/4 v3, 0x3

    .line 246
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_serial_number:Ljava/lang/String;

    const/4 v3, 0x4

    .line 247
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->merchant_token:Ljava/lang/String;

    const/4 v3, 0x5

    .line 248
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/tarkin/ChipType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->chip_type:Lcom/squareup/protos/client/tarkin/ChipType;

    const/4 v3, 0x6

    .line 249
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->user_agent:Ljava/lang/String;

    const/4 v3, 0x7

    .line 250
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 251
    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 237
    check-cast p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$ProtoAdapter_ReportCoredumpRequest;->encodedSize(Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;
    .locals 0

    .line 304
    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->newBuilder()Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;

    move-result-object p1

    .line 305
    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 306
    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->build()Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 237
    check-cast p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$ProtoAdapter_ReportCoredumpRequest;->redact(Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;

    move-result-object p1

    return-object p1
.end method
