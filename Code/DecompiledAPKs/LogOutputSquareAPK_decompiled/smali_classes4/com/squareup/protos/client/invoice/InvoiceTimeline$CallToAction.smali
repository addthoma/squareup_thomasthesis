.class public final Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;
.super Lcom/squareup/wire/Message;
.source "InvoiceTimeline.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceTimeline;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CallToAction"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$ProtoAdapter_CallToAction;,
        Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TARGET:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

.field public static final DEFAULT_TARGET_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_TEXT:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceTimeline$CallToActionLink#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final target:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceTimeline$CallToActionTarget#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final target_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final text:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 380
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$ProtoAdapter_CallToAction;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$ProtoAdapter_CallToAction;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 384
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->DO_NOT_USE:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->DEFAULT_TARGET:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;)V
    .locals 6

    .line 416
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;-><init>(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;Lokio/ByteString;)V
    .locals 1

    .line 421
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 422
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    .line 423
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->text:Ljava/lang/String;

    .line 424
    iput-object p3, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target_url:Ljava/lang/String;

    .line 425
    iput-object p4, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 442
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 443
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    .line 444
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    .line 445
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->text:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->text:Ljava/lang/String;

    .line 446
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target_url:Ljava/lang/String;

    .line 447
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    .line 448
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 453
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 455
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 456
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 457
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->text:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 458
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target_url:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 459
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 460
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;
    .locals 2

    .line 430
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;-><init>()V

    .line 431
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->target:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    .line 432
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->text:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->text:Ljava/lang/String;

    .line 433
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->target_url:Ljava/lang/String;

    .line 434
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    .line 435
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 379
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->newBuilder()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 467
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 468
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    if-eqz v1, :cond_0

    const-string v1, ", target="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 469
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->text:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 470
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target_url:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", target_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 471
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    if-eqz v1, :cond_3

    const-string v1, ", call_to_action_link="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CallToAction{"

    .line 472
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
