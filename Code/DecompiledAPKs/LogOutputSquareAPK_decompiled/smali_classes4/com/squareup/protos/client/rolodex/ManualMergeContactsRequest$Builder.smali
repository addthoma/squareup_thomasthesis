.class public final Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ManualMergeContactsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;",
        "Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

.field public duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

.field public loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

.field public request_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 133
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;
    .locals 7

    .line 166
    new-instance v6, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->request_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;-><init>(Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/ContactSet;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;

    move-result-object v0

    return-object v0
.end method

.method public contact_set(Lcom/squareup/protos/client/rolodex/ContactSet;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    return-object p0
.end method

.method public duplicate_contact_token_set(Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    return-object p0
.end method

.method public loyaltyAccountMapping(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    return-object p0
.end method

.method public request_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->request_token:Ljava/lang/String;

    return-object p0
.end method
