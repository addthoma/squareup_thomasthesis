.class public final Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;
.super Lcom/squareup/wire/Message;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ChildItemization"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$ProtoAdapter_ChildItemization;,
        Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;",
        "Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final ticket_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 3554
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$ProtoAdapter_ChildItemization;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$ProtoAdapter_ChildItemization;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V
    .locals 1

    .line 3571
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lokio/ByteString;)V
    .locals 1

    .line 3576
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 3577
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 3578
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 3593
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 3594
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    .line 3595
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 3596
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 3597
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 3602
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 3604
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 3605
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3606
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 3607
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;
    .locals 2

    .line 3583
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;-><init>()V

    .line 3584
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 3585
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 3586
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 3553
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 3614
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 3615
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", ticket_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3616
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_1

    const-string v1, ", child_itemization_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ChildItemization{"

    .line 3617
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
