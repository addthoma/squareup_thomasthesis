.class public final Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CheckIdentityVerificationStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

.field public pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

.field public response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;
    .locals 5

    .line 137
    new-instance v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;-><init>(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public error(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    const/4 p1, 0x0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    return-object p0
.end method

.method public pending(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    const/4 p1, 0x0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    return-object p0
.end method

.method public response(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    const/4 p1, 0x0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    return-object p0
.end method
