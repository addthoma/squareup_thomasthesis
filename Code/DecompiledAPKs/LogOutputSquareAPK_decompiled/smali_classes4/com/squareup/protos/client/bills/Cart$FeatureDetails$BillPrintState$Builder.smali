.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public read_only_printed:Ljava/lang/Boolean;

.field public write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;

.field public write_only_printed:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4657
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;
    .locals 5

    .line 4687
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->write_only_printed:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->read_only_printed:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;-><init>(Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4650
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    move-result-object v0

    return-object v0
.end method

.method public read_only_printed(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;
    .locals 0

    .line 4681
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->read_only_printed:Ljava/lang/Boolean;

    return-object p0
.end method

.method public write_only_client_printed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;
    .locals 0

    .line 4664
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public write_only_printed(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;
    .locals 0

    .line 4673
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->write_only_printed:Ljava/lang/Boolean;

    return-object p0
.end method
