.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public address:Lcom/squareup/protos/connect/v2/resources/Address;

.field public customer_id:Ljava/lang/String;

.field public display_name:Ljava/lang/String;

.field public email_address:Ljava/lang/String;

.field public phone_number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7200
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public address(Lcom/squareup/protos/connect/v2/resources/Address;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;
    .locals 0

    .line 7244
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;
    .locals 8

    .line 7250
    new-instance v7, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;->customer_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;->display_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;->email_address:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;->phone_number:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Address;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 7189
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    move-result-object v0

    return-object v0
.end method

.method public customer_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;
    .locals 0

    .line 7208
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;->customer_id:Ljava/lang/String;

    return-object p0
.end method

.method public display_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;
    .locals 0

    .line 7217
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;->display_name:Ljava/lang/String;

    return-object p0
.end method

.method public email_address(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;
    .locals 0

    .line 7226
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;->email_address:Ljava/lang/String;

    return-object p0
.end method

.method public phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;
    .locals 0

    .line 7235
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient$Builder;->phone_number:Ljava/lang/String;

    return-object p0
.end method
