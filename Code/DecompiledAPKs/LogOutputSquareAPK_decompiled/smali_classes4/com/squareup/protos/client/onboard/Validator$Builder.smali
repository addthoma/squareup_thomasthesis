.class public final Lcom/squareup/protos/client/onboard/Validator$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Validator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/Validator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/Validator;",
        "Lcom/squareup/protos/client/onboard/Validator$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public message:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 109
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/onboard/Validator;
    .locals 5

    .line 129
    new-instance v0, Lcom/squareup/protos/client/onboard/Validator;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Validator$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/Validator$Builder;->value:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/onboard/Validator$Builder;->message:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/onboard/Validator;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/Validator$Builder;->build()Lcom/squareup/protos/client/onboard/Validator;

    move-result-object v0

    return-object v0
.end method

.method public message(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Validator$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Validator$Builder;->message:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Validator$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Validator$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public value(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Validator$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Validator$Builder;->value:Ljava/lang/String;

    return-object p0
.end method
