.class final Lcom/squareup/protos/client/dialogue/GetConversationResponse$ProtoAdapter_GetConversationResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetConversationResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/GetConversationResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetConversationResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/dialogue/GetConversationResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 116
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/dialogue/GetConversationResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/dialogue/GetConversationResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 135
    new-instance v0, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;-><init>()V

    .line 136
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 137
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 142
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 140
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/dialogue/Conversation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/dialogue/Conversation;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->conversation(Lcom/squareup/protos/client/dialogue/Conversation;)Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;

    goto :goto_0

    .line 139
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;

    goto :goto_0

    .line 146
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 147
    invoke-virtual {v0}, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->build()Lcom/squareup/protos/client/dialogue/GetConversationResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 114
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/dialogue/GetConversationResponse$ProtoAdapter_GetConversationResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/dialogue/GetConversationResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/dialogue/GetConversationResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 128
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/dialogue/GetConversationResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 129
    sget-object v0, Lcom/squareup/protos/client/dialogue/Conversation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/dialogue/GetConversationResponse;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 130
    invoke-virtual {p2}, Lcom/squareup/protos/client/dialogue/GetConversationResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 114
    check-cast p2, Lcom/squareup/protos/client/dialogue/GetConversationResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/dialogue/GetConversationResponse$ProtoAdapter_GetConversationResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/dialogue/GetConversationResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/dialogue/GetConversationResponse;)I
    .locals 4

    .line 121
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/dialogue/GetConversationResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/dialogue/Conversation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/dialogue/GetConversationResponse;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    const/4 v3, 0x2

    .line 122
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/GetConversationResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 114
    check-cast p1, Lcom/squareup/protos/client/dialogue/GetConversationResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/dialogue/GetConversationResponse$ProtoAdapter_GetConversationResponse;->encodedSize(Lcom/squareup/protos/client/dialogue/GetConversationResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/dialogue/GetConversationResponse;)Lcom/squareup/protos/client/dialogue/GetConversationResponse;
    .locals 2

    .line 152
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/GetConversationResponse;->newBuilder()Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;

    move-result-object p1

    .line 153
    iget-object v0, p1, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Status;

    iput-object v0, p1, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 154
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/dialogue/Conversation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/dialogue/Conversation;

    iput-object v0, p1, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    .line 155
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 156
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->build()Lcom/squareup/protos/client/dialogue/GetConversationResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 114
    check-cast p1, Lcom/squareup/protos/client/dialogue/GetConversationResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/dialogue/GetConversationResponse$ProtoAdapter_GetConversationResponse;->redact(Lcom/squareup/protos/client/dialogue/GetConversationResponse;)Lcom/squareup/protos/client/dialogue/GetConversationResponse;

    move-result-object p1

    return-object p1
.end method
