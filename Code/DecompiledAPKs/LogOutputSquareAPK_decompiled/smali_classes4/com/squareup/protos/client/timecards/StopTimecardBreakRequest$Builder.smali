.class public final Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StopTimecardBreakRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;",
        "Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public authorization:Lcom/squareup/protos/client/timecards/Authorization;

.field public should_block_early_clockin:Ljava/lang/Boolean;

.field public timecard_break_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 121
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public authorization(Lcom/squareup/protos/client/timecards/Authorization;)Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;->authorization:Lcom/squareup/protos/client/timecards/Authorization;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;
    .locals 5

    .line 152
    new-instance v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;->timecard_break_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;->authorization:Lcom/squareup/protos/client/timecards/Authorization;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;->should_block_early_clockin:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/timecards/Authorization;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;->build()Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;

    move-result-object v0

    return-object v0
.end method

.method public should_block_early_clockin(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;->should_block_early_clockin:Ljava/lang/Boolean;

    return-object p0
.end method

.method public timecard_break_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;->timecard_break_token:Ljava/lang/String;

    return-object p0
.end method
