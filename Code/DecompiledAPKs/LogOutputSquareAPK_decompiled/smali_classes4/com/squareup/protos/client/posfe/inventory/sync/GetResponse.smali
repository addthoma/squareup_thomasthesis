.class public final Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;
.super Lcom/squareup/wire/Message;
.source "GetResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$ProtoAdapter_GetResponse;,
        Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;",
        "Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SERVER_VERSION:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final object:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.posfe.inventory.sync.InventoryObject#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;",
            ">;"
        }
    .end annotation
.end field

.field public final server_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 22
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$ProtoAdapter_GetResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$ProtoAdapter_GetResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 26
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->DEFAULT_SERVER_VERSION:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/Long;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;",
            ">;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .line 46
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;-><init>(Ljava/util/List;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;",
            ">;",
            "Ljava/lang/Long;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 50
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p3, "object"

    .line 51
    invoke-static {p3, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->object:Ljava/util/List;

    .line 52
    iput-object p2, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->server_version:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 67
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 68
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;

    .line 69
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->object:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->object:Ljava/util/List;

    .line 70
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->server_version:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->server_version:Ljava/lang/Long;

    .line 71
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 76
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 79
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->object:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 80
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->server_version:Ljava/lang/Long;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 81
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;
    .locals 2

    .line 57
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;-><init>()V

    .line 58
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->object:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;->object:Ljava/util/List;

    .line 59
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->server_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;->server_version:Ljava/lang/Long;

    .line 60
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->newBuilder()Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->object:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", object="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->object:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 90
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->server_version:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", server_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->server_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetResponse{"

    .line 91
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
