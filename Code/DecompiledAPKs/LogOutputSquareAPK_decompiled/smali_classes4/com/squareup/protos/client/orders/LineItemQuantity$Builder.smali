.class public final Lcom/squareup/protos/client/orders/LineItemQuantity$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LineItemQuantity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/LineItemQuantity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/LineItemQuantity;",
        "Lcom/squareup/protos/client/orders/LineItemQuantity$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public line_item_uid:Ljava/lang/String;

.field public quantity:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/orders/LineItemQuantity;
    .locals 4

    .line 109
    new-instance v0, Lcom/squareup/protos/client/orders/LineItemQuantity;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/LineItemQuantity$Builder;->line_item_uid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/orders/LineItemQuantity$Builder;->quantity:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/orders/LineItemQuantity;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/LineItemQuantity$Builder;->build()Lcom/squareup/protos/client/orders/LineItemQuantity;

    move-result-object v0

    return-object v0
.end method

.method public line_item_uid(Ljava/lang/String;)Lcom/squareup/protos/client/orders/LineItemQuantity$Builder;
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/protos/client/orders/LineItemQuantity$Builder;->line_item_uid:Ljava/lang/String;

    return-object p0
.end method

.method public quantity(Ljava/lang/String;)Lcom/squareup/protos/client/orders/LineItemQuantity$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/orders/LineItemQuantity$Builder;->quantity:Ljava/lang/String;

    return-object p0
.end method
