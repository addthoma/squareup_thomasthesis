.class public final Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;
.super Lcom/squareup/wire/Message;
.source "InvoiceDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$ProtoAdapter_InvoiceDisplayDetails;,
        Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;,
        Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;,
        Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DISPLAY_STATE:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

.field public static final DEFAULT_IS_ARCHIVED:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final automatic_reminder_instance:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceReminderInstance#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x13
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
            ">;"
        }
    .end annotation
.end field

.field public final bill_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final cancelled_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final created_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0xf
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0xe
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final deprecated_v1_refund:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.RefundV1#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RefundV1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceDisplayDetails$DisplayState#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final event:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceEvent#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xd
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceEvent;",
            ">;"
        }
    .end annotation
.end field

.field public final first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.YearMonthDay#ADAPTER"
        tag = 0x16
    .end annotation
.end field

.field public final invoice:Lcom/squareup/protos/client/invoice/Invoice;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.Invoice#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceTimeline#ADAPTER"
        tag = 0x11
    .end annotation
.end field

.field public final is_archived:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x17
    .end annotation
.end field

.field public final paid_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final payment_request_state:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceDisplayDetails$PaymentRequestState#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x14
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;",
            ">;"
        }
    .end annotation
.end field

.field public final payments:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceTenderDetails#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x15
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;",
            ">;"
        }
    .end annotation
.end field

.field public final refund:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceRefund#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x10
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceRefund;",
            ">;"
        }
    .end annotation
.end field

.field public final refunded_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final sent_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.SeriesDetails#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.ShippingAddress#ADAPTER"
        tag = 0x12
    .end annotation
.end field

.field public final sort_date:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final updated_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$ProtoAdapter_InvoiceDisplayDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$ProtoAdapter_InvoiceDisplayDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 38
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNKNOWN:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->DEFAULT_DISPLAY_STATE:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/4 v0, 0x0

    .line 40
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->DEFAULT_IS_ARCHIVED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;Lokio/ByteString;)V
    .locals 1

    .line 244
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 245
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    .line 246
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 247
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 248
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 249
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->paid_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->paid_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 250
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 251
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 252
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 253
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 254
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 255
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_v1_refund:Ljava/util/List;

    const-string v0, "deprecated_v1_refund"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_v1_refund:Ljava/util/List;

    .line 256
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    .line 257
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->event:Ljava/util/List;

    const-string v0, "event"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->event:Ljava/util/List;

    .line 258
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 259
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 260
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->refund:Ljava/util/List;

    const-string v0, "refund"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refund:Ljava/util/List;

    .line 261
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    .line 262
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

    .line 263
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->automatic_reminder_instance:Ljava/util/List;

    const-string v0, "automatic_reminder_instance"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->automatic_reminder_instance:Ljava/util/List;

    .line 264
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->payment_request_state:Ljava/util/List;

    const-string v0, "payment_request_state"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payment_request_state:Ljava/util/List;

    .line 265
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->payments:Ljava/util/List;

    const-string v0, "payments"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payments:Ljava/util/List;

    .line 266
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 267
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->is_archived:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->is_archived:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 303
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 304
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    .line 305
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    .line 306
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 307
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 308
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 309
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->paid_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->paid_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 310
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 311
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 312
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 313
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 314
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 315
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_v1_refund:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_v1_refund:Ljava/util/List;

    .line 316
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    .line 317
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->event:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->event:Ljava/util/List;

    .line 318
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 319
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 320
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refund:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refund:Ljava/util/List;

    .line 321
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    .line 322
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

    .line 323
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->automatic_reminder_instance:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->automatic_reminder_instance:Ljava/util/List;

    .line 324
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payment_request_state:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payment_request_state:Ljava/util/List;

    .line 325
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payments:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payments:Ljava/util/List;

    .line 326
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 327
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->is_archived:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->is_archived:Ljava/lang/Boolean;

    .line 328
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 333
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_11

    .line 335
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 336
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 337
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 338
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 339
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 340
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->paid_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 341
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 342
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 343
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 344
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 345
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 346
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_v1_refund:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 347
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/SeriesDetails;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 348
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->event:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 349
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 350
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 351
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refund:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 352
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 353
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/ShippingAddress;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 354
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->automatic_reminder_instance:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 355
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payment_request_state:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 356
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 357
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/YearMonthDay;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 358
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->is_archived:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_10
    add-int/2addr v0, v2

    .line 359
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_11
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 2

    .line 272
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;-><init>()V

    .line 273
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    .line 274
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 275
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 276
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 277
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->paid_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->paid_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 278
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 279
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 280
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 281
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 282
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 283
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_v1_refund:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_v1_refund:Ljava/util/List;

    .line 284
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    .line 285
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->event:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->event:Ljava/util/List;

    .line 286
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 287
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 288
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refund:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->refund:Ljava/util/List;

    .line 289
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    .line 290
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

    .line 291
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->automatic_reminder_instance:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->automatic_reminder_instance:Ljava/util/List;

    .line 292
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payment_request_state:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->payment_request_state:Ljava/util/List;

    .line 293
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payments:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->payments:Ljava/util/List;

    .line 294
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 295
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->is_archived:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->is_archived:Ljava/lang/Boolean;

    .line 296
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->newBuilder()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 366
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 367
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    if-eqz v1, :cond_0

    const-string v1, ", invoice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 368
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    const-string v1, ", sort_date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 369
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 370
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_3

    const-string v1, ", updated_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 371
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->paid_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    const-string v1, ", paid_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->paid_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 372
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_5

    const-string v1, ", cancelled_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 373
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_6

    const-string v1, ", refunded_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 374
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_7

    const-string v1, ", sent_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 375
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    if-eqz v1, :cond_8

    const-string v1, ", display_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 376
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_9

    const-string v1, ", bill_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 377
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_v1_refund:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, ", deprecated_v1_refund="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_v1_refund:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 378
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    if-eqz v1, :cond_b

    const-string v1, ", series_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 379
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->event:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, ", event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->event:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 380
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_d

    const-string v1, ", deprecated_last_viewed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 381
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_e

    const-string v1, ", deprecated_last_edited_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 382
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refund:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, ", refund="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refund:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 383
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    if-eqz v1, :cond_10

    const-string v1, ", invoice_timeline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 384
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

    if-eqz v1, :cond_11

    const-string v1, ", shipping_address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 385
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->automatic_reminder_instance:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_12

    const-string v1, ", automatic_reminder_instance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->automatic_reminder_instance:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 386
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payment_request_state:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_13

    const-string v1, ", payment_request_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payment_request_state:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 387
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_14

    const-string v1, ", payments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payments:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 388
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_15

    const-string v1, ", first_sent_date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 389
    :cond_15
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->is_archived:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    const-string v1, ", is_archived="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->is_archived:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_16
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InvoiceDisplayDetails{"

    .line 390
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
