.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Course"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$ProtoAdapter_Course;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ORDINAL:Ljava/lang/Integer;

.field public static final DEFAULT_STRAIGHT_FIRE:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final course_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final event:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$CoursingOptions$Course$Event#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;",
            ">;"
        }
    .end annotation
.end field

.field public final ordinal:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x2
    .end annotation
.end field

.field public final source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$SourceTicketInformation#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final straight_fire:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 3499
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$ProtoAdapter_Course;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$ProtoAdapter_Course;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 3503
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    .line 3505
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->DEFAULT_STRAIGHT_FIRE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;",
            ">;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;",
            ")V"
        }
    .end annotation

    .line 3558
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;",
            ">;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 3564
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 3565
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 3566
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->ordinal:Ljava/lang/Integer;

    .line 3567
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->straight_fire:Ljava/lang/Boolean;

    const-string p1, "event"

    .line 3568
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->event:Ljava/util/List;

    .line 3569
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 3587
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 3588
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    .line 3589
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 3590
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->ordinal:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->ordinal:Ljava/lang/Integer;

    .line 3591
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->straight_fire:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->straight_fire:Ljava/lang/Boolean;

    .line 3592
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->event:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->event:Ljava/util/List;

    .line 3593
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    .line 3594
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 3599
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 3601
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 3602
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3603
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3604
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->straight_fire:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3605
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->event:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3606
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 3607
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;
    .locals 2

    .line 3574
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;-><init>()V

    .line 3575
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 3576
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->ordinal:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->ordinal:Ljava/lang/Integer;

    .line 3577
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->straight_fire:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->straight_fire:Ljava/lang/Boolean;

    .line 3578
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->event:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->event:Ljava/util/List;

    .line 3579
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    .line 3580
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 3498
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 3614
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 3615
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", course_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3616
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", ordinal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->ordinal:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3617
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->straight_fire:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", straight_fire="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->straight_fire:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3618
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->event:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->event:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3619
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    if-eqz v1, :cond_4

    const-string v1, ", source_ticket_information="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Course{"

    .line 3620
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
