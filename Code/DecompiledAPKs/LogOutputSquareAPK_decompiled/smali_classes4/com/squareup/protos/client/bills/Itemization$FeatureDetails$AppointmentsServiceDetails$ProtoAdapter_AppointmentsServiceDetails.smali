.class final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$ProtoAdapter_AppointmentsServiceDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AppointmentsServiceDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 4918
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4950
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;-><init>()V

    .line 4951
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 4952
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 4970
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 4968
    :pswitch_0
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->resource_tokens:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4967
    :pswitch_1
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->intermissions:Ljava/util/List;

    sget-object v4, Lcom/squareup/api/items/Intermission;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4966
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->no_show_fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    goto :goto_0

    .line 4960
    :pswitch_3
    :try_start_0
    sget-object v4, Lcom/squareup/api/items/PricingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/PricingType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->pricing_type(Lcom/squareup/api/items/PricingType;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 4962
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 4957
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->transition_time_duration(Ljava/lang/Long;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    goto :goto_0

    .line 4956
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    goto :goto_0

    .line 4955
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->service_duration(Ljava/lang/Long;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    goto :goto_0

    .line 4954
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->service_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    goto :goto_0

    .line 4974
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 4975
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4916
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$ProtoAdapter_AppointmentsServiceDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4937
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->service_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4938
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->service_duration:Ljava/lang/Long;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4939
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->ordinal:Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4940
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->transition_time_duration:Ljava/lang/Long;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4941
    sget-object v0, Lcom/squareup/api/items/PricingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->pricing_type:Lcom/squareup/api/items/PricingType;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4942
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->no_show_fee_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4943
    sget-object v0, Lcom/squareup/api/items/Intermission;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->intermissions:Ljava/util/List;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4944
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->resource_tokens:Ljava/util/List;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4945
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4916
    check-cast p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$ProtoAdapter_AppointmentsServiceDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;)I
    .locals 4

    .line 4923
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->service_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->service_duration:Ljava/lang/Long;

    const/4 v3, 0x2

    .line 4924
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->ordinal:Ljava/lang/Integer;

    const/4 v3, 0x3

    .line 4925
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->transition_time_duration:Ljava/lang/Long;

    const/4 v3, 0x4

    .line 4926
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/PricingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->pricing_type:Lcom/squareup/api/items/PricingType;

    const/4 v3, 0x5

    .line 4927
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->no_show_fee_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x6

    .line 4928
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/Intermission;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 4929
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->intermissions:Ljava/util/List;

    const/4 v3, 0x7

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 4930
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->resource_tokens:Ljava/util/List;

    const/16 v3, 0x8

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4931
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 4916
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$ProtoAdapter_AppointmentsServiceDetails;->encodedSize(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;
    .locals 2

    .line 4980
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    move-result-object p1

    .line 4981
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->no_show_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->no_show_fee_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->no_show_fee_money:Lcom/squareup/protos/common/Money;

    .line 4982
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->intermissions:Ljava/util/List;

    sget-object v1, Lcom/squareup/api/items/Intermission;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 4983
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 4984
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 4916
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$ProtoAdapter_AppointmentsServiceDetails;->redact(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    move-result-object p1

    return-object p1
.end method
