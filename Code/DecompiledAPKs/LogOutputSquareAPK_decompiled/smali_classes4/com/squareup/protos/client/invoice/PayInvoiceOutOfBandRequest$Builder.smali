.class public final Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PayInvoiceOutOfBandRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;",
        "Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id_pair:Lcom/squareup/protos/client/IdPair;

.field public payment_amount:Lcom/squareup/protos/common/Money;

.field public send_email_to_recipients:Ljava/lang/Boolean;

.field public tender_note:Ljava/lang/String;

.field public tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

.field public version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 159
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;
    .locals 9

    .line 197
    new-instance v8, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->tender_note:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->send_email_to_recipients:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->version:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->payment_amount:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 146
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->build()Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;

    move-result-object v0

    return-object v0
.end method

.method public id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public payment_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->payment_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public send_email_to_recipients(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->send_email_to_recipients:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tender_note(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->tender_note:Ljava/lang/String;

    return-object p0
.end method

.method public tender_type(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;)Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    return-object p0
.end method

.method public version(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->version:Ljava/lang/String;

    return-object p0
.end method
