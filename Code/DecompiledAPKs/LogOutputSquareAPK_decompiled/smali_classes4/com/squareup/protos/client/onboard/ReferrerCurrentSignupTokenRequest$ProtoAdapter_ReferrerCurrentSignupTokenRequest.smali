.class final Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest$ProtoAdapter_ReferrerCurrentSignupTokenRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReferrerCurrentSignupTokenRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReferrerCurrentSignupTokenRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 71
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 87
    new-instance v0, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest$Builder;-><init>()V

    .line 88
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 89
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 92
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 96
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 97
    invoke-virtual {v0}, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest$Builder;->build()Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 69
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest$ProtoAdapter_ReferrerCurrentSignupTokenRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 82
    invoke-virtual {p2}, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 69
    check-cast p2, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest$ProtoAdapter_ReferrerCurrentSignupTokenRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;)I
    .locals 0

    .line 76
    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 69
    check-cast p1, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest$ProtoAdapter_ReferrerCurrentSignupTokenRequest;->encodedSize(Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;)Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;
    .locals 0

    .line 102
    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;->newBuilder()Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest$Builder;

    move-result-object p1

    .line 103
    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 104
    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest$Builder;->build()Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 69
    check-cast p1, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest$ProtoAdapter_ReferrerCurrentSignupTokenRequest;->redact(Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;)Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;

    move-result-object p1

    return-object p1
.end method
