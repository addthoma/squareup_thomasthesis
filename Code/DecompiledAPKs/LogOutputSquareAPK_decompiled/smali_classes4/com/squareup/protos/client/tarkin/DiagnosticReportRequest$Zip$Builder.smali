.class public final Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DiagnosticReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;",
        "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public archive:Lokio/ByteString;

.field public metadata:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 766
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 767
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;->metadata:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public archive(Lokio/ByteString;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;
    .locals 0

    .line 777
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;->archive:Lokio/ByteString;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;
    .locals 4

    .line 783
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;->metadata:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;->archive:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;-><init>(Ljava/util/List;Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 761
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;->build()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    move-result-object v0

    return-object v0
.end method

.method public metadata(Ljava/util/List;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;",
            ">;)",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;"
        }
    .end annotation

    .line 771
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 772
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;->metadata:Ljava/util/List;

    return-object p0
.end method
