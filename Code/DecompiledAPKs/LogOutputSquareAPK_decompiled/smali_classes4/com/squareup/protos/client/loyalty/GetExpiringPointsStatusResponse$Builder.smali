.class public final Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetExpiringPointsStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse;",
        "Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public status:Lcom/squareup/protos/client/Status;

.field public upcoming_deadlines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 101
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 102
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$Builder;->upcoming_deadlines:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse;
    .locals 4

    .line 122
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$Builder;->upcoming_deadlines:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public upcoming_deadlines(Ljava/util/List;)Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;",
            ">;)",
            "Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$Builder;"
        }
    .end annotation

    .line 115
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$Builder;->upcoming_deadlines:Ljava/util/List;

    return-object p0
.end method
