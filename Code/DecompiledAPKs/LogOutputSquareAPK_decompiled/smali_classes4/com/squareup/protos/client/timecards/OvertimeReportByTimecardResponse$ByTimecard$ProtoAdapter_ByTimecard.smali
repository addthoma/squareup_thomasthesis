.class final Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$ProtoAdapter_ByTimecard;
.super Lcom/squareup/wire/ProtoAdapter;
.source "OvertimeReportByTimecardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ByTimecard"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 254
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 275
    new-instance v0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;-><init>()V

    .line 276
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 277
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 283
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 281
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/timecards/Timecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/timecards/Timecard;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->timecard(Lcom/squareup/protos/client/timecards/Timecard;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;

    goto :goto_0

    .line 280
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/timecards/CalculationTotal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/timecards/CalculationTotal;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->calculation_total(Lcom/squareup/protos/client/timecards/CalculationTotal;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;

    goto :goto_0

    .line 279
    :cond_2
    sget-object v3, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->calculated_time_interval(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;

    goto :goto_0

    .line 287
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 288
    invoke-virtual {v0}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->build()Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 252
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$ProtoAdapter_ByTimecard;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 267
    sget-object v0, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 268
    sget-object v0, Lcom/squareup/protos/client/timecards/CalculationTotal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 269
    sget-object v0, Lcom/squareup/protos/client/timecards/Timecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 270
    invoke-virtual {p2}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 252
    check-cast p2, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$ProtoAdapter_ByTimecard;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;)I
    .locals 4

    .line 259
    sget-object v0, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/timecards/CalculationTotal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    const/4 v3, 0x2

    .line 260
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/timecards/Timecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    const/4 v3, 0x3

    .line 261
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 262
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 252
    check-cast p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$ProtoAdapter_ByTimecard;->encodedSize(Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;
    .locals 2

    .line 293
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->newBuilder()Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;

    move-result-object p1

    .line 294
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTimeInterval;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 295
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/timecards/CalculationTotal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/timecards/CalculationTotal;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    .line 296
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/timecards/Timecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/timecards/Timecard;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 297
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 298
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->build()Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 252
    check-cast p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$ProtoAdapter_ByTimecard;->redact(Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;

    move-result-object p1

    return-object p1
.end method
