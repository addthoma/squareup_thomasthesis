.class public final Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CommonPointOfSaleAttributes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;",
        "Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public business_day_token:Ljava/lang/String;

.field public receipt_token:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public transaction_number:Ljava/lang/String;

.field public virtual_register_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 152
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 153
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;->receipt_token:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;
    .locals 7

    .line 194
    new-instance v6, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;->virtual_register_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;->business_day_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;->transaction_number:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;->receipt_token:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 143
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;->build()Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;

    move-result-object v0

    return-object v0
.end method

.method public business_day_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;->business_day_token:Ljava/lang/String;

    return-object p0
.end method

.method public receipt_token(Ljava/util/List;)Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;"
        }
    .end annotation

    .line 187
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 188
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;->receipt_token:Ljava/util/List;

    return-object p0
.end method

.method public transaction_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;->transaction_number:Ljava/lang/String;

    return-object p0
.end method

.method public virtual_register_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;->virtual_register_token:Ljava/lang/String;

    return-object p0
.end method
