.class final Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$ProtoAdapter_UpdateCashDrawerShiftRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "UpdateCashDrawerShiftRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_UpdateCashDrawerShiftRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 259
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 288
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;-><init>()V

    .line 289
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 290
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 300
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 298
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->device_info(Lcom/squareup/protos/client/cashdrawers/DeviceInfo;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    goto :goto_0

    .line 297
    :pswitch_1
    iget-object v3, v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->events:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 296
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->expected_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    goto :goto_0

    .line 295
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->cash_drawer_shift_description(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    goto :goto_0

    .line 294
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    goto :goto_0

    .line 293
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    goto :goto_0

    .line 292
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->client_unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    goto :goto_0

    .line 304
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 305
    invoke-virtual {v0}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 257
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$ProtoAdapter_UpdateCashDrawerShiftRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 276
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 277
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 278
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 279
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->cash_drawer_shift_description:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 280
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 281
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->events:Ljava/util/List;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 282
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 283
    invoke-virtual {p2}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 257
    check-cast p2, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$ProtoAdapter_UpdateCashDrawerShiftRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;)I
    .locals 4

    .line 264
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    const/4 v3, 0x2

    .line 265
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    const/4 v3, 0x3

    .line 266
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->cash_drawer_shift_description:Ljava/lang/String;

    const/4 v3, 0x4

    .line 267
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x5

    .line 268
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 269
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->events:Ljava/util/List;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    const/4 v3, 0x7

    .line 270
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 271
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 257
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$ProtoAdapter_UpdateCashDrawerShiftRequest;->encodedSize(Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;
    .locals 2

    .line 310
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->newBuilder()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 311
    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->cash_drawer_shift_description:Ljava/lang/String;

    .line 312
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    .line 313
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->events:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 314
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 315
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 316
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 257
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$ProtoAdapter_UpdateCashDrawerShiftRequest;->redact(Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    move-result-object p1

    return-object p1
.end method
