.class public final Lcom/squareup/protos/client/bills/DiscountLineItem;
.super Lcom/squareup/wire/Message;
.source "DiscountLineItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/DiscountLineItem$ProtoAdapter_DiscountLineItem;,
        Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;,
        Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;,
        Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;,
        Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;,
        Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;,
        Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/DiscountLineItem;",
        "Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APPLICATION_METHOD:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

.field public static final DEFAULT_APPLICATION_SCOPE:Lcom/squareup/protos/client/bills/ApplicationScope;

.field public static final DEFAULT_DISCOUNT_QUANTITY:Ljava/lang/String; = ""

.field public static final DEFAULT_WRITE_ONLY_DELETED:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.DiscountLineItem$Amounts#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.DiscountLineItem$ApplicationMethod#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.ApplicationScope#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.DiscountLineItem$Configuration#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final created_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final discount_quantity:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.DiscountLineItem$DisplayDetails#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.DiscountLineItem$BackingDetails#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final write_only_deleted:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/protos/client/bills/DiscountLineItem$ProtoAdapter_DiscountLineItem;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$ProtoAdapter_DiscountLineItem;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 33
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem;->DEFAULT_WRITE_ONLY_DELETED:Ljava/lang/Boolean;

    .line 35
    sget-object v0, Lcom/squareup/protos/client/bills/ApplicationScope;->UNKNOWN:Lcom/squareup/protos/client/bills/ApplicationScope;

    sput-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem;->DEFAULT_APPLICATION_SCOPE:Lcom/squareup/protos/client/bills/ApplicationScope;

    .line 37
    sget-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;->UNKNOWN:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    sput-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem;->DEFAULT_APPLICATION_METHOD:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/ApplicationScope;Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;Ljava/lang/String;)V
    .locals 12

    .line 139
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/client/bills/DiscountLineItem;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/ApplicationScope;Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/ApplicationScope;Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 147
    sget-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 149
    iput-object p2, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 150
    iput-object p3, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    .line 151
    iput-object p4, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    .line 152
    iput-object p5, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    .line 153
    iput-object p6, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    .line 154
    iput-object p7, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_deleted:Ljava/lang/Boolean;

    .line 155
    iput-object p8, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    .line 156
    iput-object p9, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    .line 157
    iput-object p10, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_quantity:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 180
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 181
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 182
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/DiscountLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 183
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 184
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    .line 185
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    .line 186
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    .line 187
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    .line 188
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_deleted:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_deleted:Ljava/lang/Boolean;

    .line 189
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    .line 190
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    .line 191
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_quantity:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_quantity:Ljava/lang/String;

    .line 192
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 197
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 199
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_deleted:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ApplicationScope;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_quantity:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 210
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;
    .locals 2

    .line 162
    new-instance v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;-><init>()V

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 165
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    .line 166
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    .line 167
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    .line 169
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_deleted:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_deleted:Ljava/lang/Boolean;

    .line 170
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_quantity:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_quantity:Ljava/lang/String;

    .line 173
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem;->newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 218
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", discount_line_item_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 219
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 220
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    if-eqz v1, :cond_2

    const-string v1, ", configuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 221
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    if-eqz v1, :cond_3

    const-string v1, ", write_only_backing_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 222
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    if-eqz v1, :cond_4

    const-string v1, ", read_only_display_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 223
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    if-eqz v1, :cond_5

    const-string v1, ", amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 224
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_deleted:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", write_only_deleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_deleted:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 225
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    if-eqz v1, :cond_7

    const-string v1, ", application_scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 226
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    if-eqz v1, :cond_8

    const-string v1, ", application_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 227
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_quantity:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", discount_quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_quantity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DiscountLineItem{"

    .line 228
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
