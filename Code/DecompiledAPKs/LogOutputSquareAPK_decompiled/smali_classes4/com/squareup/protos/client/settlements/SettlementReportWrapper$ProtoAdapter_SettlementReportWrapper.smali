.class final Lcom/squareup/protos/client/settlements/SettlementReportWrapper$ProtoAdapter_SettlementReportWrapper;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SettlementReportWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettlementReportWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SettlementReportWrapper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 130
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/settlements/SettlementReportWrapper;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 149
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;-><init>()V

    .line 150
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 151
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 156
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 154
    :cond_0
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->send_date(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;

    goto :goto_0

    .line 153
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/settlements/SettlementReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/settlements/SettlementReport;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->settlement(Lcom/squareup/protos/client/settlements/SettlementReport;)Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;

    goto :goto_0

    .line 160
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 161
    invoke-virtual {v0}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 128
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$ProtoAdapter_SettlementReportWrapper;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 142
    sget-object v0, Lcom/squareup/protos/client/settlements/SettlementReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 143
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 144
    invoke-virtual {p2}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 128
    check-cast p2, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$ProtoAdapter_SettlementReportWrapper;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)I
    .locals 4

    .line 135
    sget-object v0, Lcom/squareup/protos/client/settlements/SettlementReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    const/4 v3, 0x3

    .line 136
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 128
    check-cast p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$ProtoAdapter_SettlementReportWrapper;->encodedSize(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)Lcom/squareup/protos/client/settlements/SettlementReportWrapper;
    .locals 2

    .line 166
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->newBuilder()Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;

    move-result-object p1

    .line 167
    iget-object v0, p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/settlements/SettlementReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/settlements/SettlementReport;

    iput-object v0, p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    .line 168
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->send_date:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->send_date:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->send_date:Lcom/squareup/protos/common/time/DateTime;

    .line 169
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 170
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 128
    check-cast p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$ProtoAdapter_SettlementReportWrapper;->redact(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    move-result-object p1

    return-object p1
.end method
