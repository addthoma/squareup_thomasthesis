.class public final Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;
.super Lcom/squareup/wire/Message;
.source "GetMetricsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/GetMetricsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Metric"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ProtoAdapter_Metric;,
        Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;,
        Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;,
        Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_METRIC_TYPE:Lcom/squareup/protos/client/invoice/MetricType;

.field private static final serialVersionUID:J


# instance fields
.field public final date_range:Lcom/squareup/protos/common/time/DateTimeInterval;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTimeInterval#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final metric_type:Lcom/squareup/protos/client/invoice/MetricType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.MetricType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.GetMetricsResponse$Metric$Value#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 119
    new-instance v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ProtoAdapter_Metric;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ProtoAdapter_Metric;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 123
    sget-object v0, Lcom/squareup/protos/client/invoice/MetricType;->METRIC_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/invoice/MetricType;

    sput-object v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->DEFAULT_METRIC_TYPE:Lcom/squareup/protos/client/invoice/MetricType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/invoice/MetricType;Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;)V
    .locals 1

    .line 144
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;-><init>(Lcom/squareup/protos/client/invoice/MetricType;Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/invoice/MetricType;Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;Lokio/ByteString;)V
    .locals 1

    .line 149
    sget-object v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    .line 151
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 152
    iput-object p3, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 168
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 169
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;

    .line 170
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    .line 171
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 172
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    .line 173
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 178
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 180
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 181
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/MetricType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTimeInterval;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 183
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 184
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;
    .locals 2

    .line 157
    new-instance v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;-><init>()V

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    .line 161
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->newBuilder()Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    if-eqz v1, :cond_0

    const-string v1, ", metric_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v1, :cond_1

    const-string v1, ", date_range="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 194
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    if-eqz v1, :cond_2

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Metric{"

    .line 195
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
