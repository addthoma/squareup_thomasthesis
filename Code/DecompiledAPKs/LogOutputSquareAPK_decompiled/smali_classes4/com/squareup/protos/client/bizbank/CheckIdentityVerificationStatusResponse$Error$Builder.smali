.class public final Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CheckIdentityVerificationStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public reason:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 599
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;
    .locals 3

    .line 612
    new-instance v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error$Builder;->reason:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 596
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error$Builder;->build()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    move-result-object v0

    return-object v0
.end method

.method public reason(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error$Builder;
    .locals 0

    .line 606
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error$Builder;->reason:Ljava/lang/String;

    return-object p0
.end method
