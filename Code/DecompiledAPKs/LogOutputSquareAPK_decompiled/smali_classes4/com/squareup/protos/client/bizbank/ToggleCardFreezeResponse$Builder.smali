.class public final Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ToggleCardFreezeResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse;",
        "Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse;
    .locals 4

    .line 119
    new-instance v0, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse$Builder;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse;

    move-result-object v0

    return-object v0
.end method

.method public card(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse$Builder;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
