.class public final Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ManualMergeContactsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse;",
        "Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public new_contact:Lcom/squareup/protos/client/rolodex/Contact;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 91
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse;
    .locals 4

    .line 106
    new-instance v0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse$Builder;->new_contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/rolodex/Contact;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse;

    move-result-object v0

    return-object v0
.end method

.method public new_contact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse$Builder;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse$Builder;->new_contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse$Builder;
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
