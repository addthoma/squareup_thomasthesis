.class public final Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BillEncryptionPayload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;",
        "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public receipt_option:Lcom/squareup/protos/client/ReceiptOption;

.field public sign_on_paper:Ljava/lang/Boolean;

.field public signature:Lcom/squareup/protos/client/Signature;

.field public tender_id:Lcom/squareup/protos/client/IdPair;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 391
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;
    .locals 7

    .line 419
    new-instance v6, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->tender_id:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->receipt_option:Lcom/squareup/protos/client/ReceiptOption;

    iget-object v3, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->signature:Lcom/squareup/protos/client/Signature;

    iget-object v4, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->sign_on_paper:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/ReceiptOption;Lcom/squareup/protos/client/Signature;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 382
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;

    move-result-object v0

    return-object v0
.end method

.method public receipt_option(Lcom/squareup/protos/client/ReceiptOption;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;
    .locals 0

    .line 400
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->receipt_option:Lcom/squareup/protos/client/ReceiptOption;

    return-object p0
.end method

.method public sign_on_paper(Ljava/lang/Boolean;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;
    .locals 0

    .line 413
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->sign_on_paper:Ljava/lang/Boolean;

    return-object p0
.end method

.method public signature(Lcom/squareup/protos/client/Signature;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;
    .locals 0

    .line 405
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->signature:Lcom/squareup/protos/client/Signature;

    return-object p0
.end method

.method public tender_id(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;
    .locals 0

    .line 395
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->tender_id:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method
