.class public final Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpdateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/UpdateOrderRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/UpdateOrderRequest;",
        "Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public client_support:Lcom/squareup/protos/client/orders/ClientSupport;

.field public fields_to_clear:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public order:Lcom/squareup/orders/model/Order;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 118
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 119
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->fields_to_clear:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/orders/UpdateOrderRequest;
    .locals 5

    .line 146
    new-instance v0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    iget-object v2, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->order:Lcom/squareup/orders/model/Order;

    iget-object v3, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->fields_to_clear:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/orders/UpdateOrderRequest;-><init>(Lcom/squareup/protos/client/orders/ClientSupport;Lcom/squareup/orders/model/Order;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->build()Lcom/squareup/protos/client/orders/UpdateOrderRequest;

    move-result-object v0

    return-object v0
.end method

.method public client_support(Lcom/squareup/protos/client/orders/ClientSupport;)Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    return-object p0
.end method

.method public fields_to_clear(Ljava/util/List;)Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;"
        }
    .end annotation

    .line 139
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->fields_to_clear:Ljava/util/List;

    return-object p0
.end method

.method public order(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->order:Lcom/squareup/orders/model/Order;

    return-object p0
.end method
