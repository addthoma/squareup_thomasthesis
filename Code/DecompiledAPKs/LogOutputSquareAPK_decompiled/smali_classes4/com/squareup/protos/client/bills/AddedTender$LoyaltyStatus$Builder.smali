.class public final Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AddedTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;",
        "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public is_enrolled:Ljava/lang/Boolean;

.field public punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

.field public type:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1055
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;
    .locals 5

    .line 1079
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->is_enrolled:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->type:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;-><init>(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1048
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->build()Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;

    move-result-object v0

    return-object v0
.end method

.method public is_enrolled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;
    .locals 0

    .line 1068
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->is_enrolled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public punch_status(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;)Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;
    .locals 0

    .line 1059
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;)Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;
    .locals 0

    .line 1073
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->type:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    return-object p0
.end method
