.class public final Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CheckIdentityVerificationStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public can_retry_idv:Ljava/lang/Boolean;

.field public idv_error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

.field public idv_needs_ssn:Ljava/lang/Boolean;

.field public idv_retries_remaining:Ljava/lang/Integer;

.field public idv_state:Lcom/squareup/protos/client/bizbank/IdvState;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 267
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;
    .locals 8

    .line 301
    new-instance v7, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->idv_needs_ssn:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->can_retry_idv:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->idv_retries_remaining:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->idv_error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;-><init>(Lcom/squareup/protos/client/bizbank/IdvState;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 256
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->build()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    move-result-object v0

    return-object v0
.end method

.method public can_retry_idv(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;
    .locals 0

    .line 281
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->can_retry_idv:Ljava/lang/Boolean;

    return-object p0
.end method

.method public idv_error(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;
    .locals 0

    .line 295
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->idv_error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    return-object p0
.end method

.method public idv_needs_ssn(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;
    .locals 0

    .line 276
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->idv_needs_ssn:Ljava/lang/Boolean;

    return-object p0
.end method

.method public idv_retries_remaining(Ljava/lang/Integer;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;
    .locals 0

    .line 286
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->idv_retries_remaining:Ljava/lang/Integer;

    return-object p0
.end method

.method public idv_state(Lcom/squareup/protos/client/bizbank/IdvState;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    return-object p0
.end method
