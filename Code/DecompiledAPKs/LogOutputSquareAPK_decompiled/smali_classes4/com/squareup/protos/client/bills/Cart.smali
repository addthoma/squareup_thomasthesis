.class public final Lcom/squareup/protos/client/bills/Cart;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$ProtoAdapter_Cart;,
        Lcom/squareup/protos/client/bills/Cart$AmountDetails;,
        Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails;,
        Lcom/squareup/protos/client/bills/Cart$Amounts;,
        Lcom/squareup/protos/client/bills/Cart$LineItems;,
        Lcom/squareup/protos/client/bills/Cart$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart;",
        "Lcom/squareup/protos/client/bills/Cart$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CREATED_BY_UNIT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_READ_ONLY_CUSTOM_NOTE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$AmountDetails#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$Amounts#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final created_by_unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$LineItems#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final read_only_custom_note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final return_line_items:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$ReturnLineItems#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$ProtoAdapter_Cart;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$ProtoAdapter_Cart;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Cart$LineItems;Lcom/squareup/protos/client/bills/Cart$Amounts;Lcom/squareup/protos/client/bills/Cart$FeatureDetails;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart$AmountDetails;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart$LineItems;",
            "Lcom/squareup/protos/client/bills/Cart$Amounts;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
            ">;",
            "Lcom/squareup/protos/client/bills/Cart$AmountDetails;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 120
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/Cart;-><init>(Lcom/squareup/protos/client/bills/Cart$LineItems;Lcom/squareup/protos/client/bills/Cart$Amounts;Lcom/squareup/protos/client/bills/Cart$FeatureDetails;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart$AmountDetails;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Cart$LineItems;Lcom/squareup/protos/client/bills/Cart$Amounts;Lcom/squareup/protos/client/bills/Cart$FeatureDetails;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart$AmountDetails;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart$LineItems;",
            "Lcom/squareup/protos/client/bills/Cart$Amounts;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
            ">;",
            "Lcom/squareup/protos/client/bills/Cart$AmountDetails;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 126
    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    .line 128
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 129
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    .line 130
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Cart;->read_only_custom_note:Ljava/lang/String;

    const-string p1, "return_line_items"

    .line 131
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    .line 132
    iput-object p6, p0, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    .line 133
    iput-object p7, p0, Lcom/squareup/protos/client/bills/Cart;->created_by_unit_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 153
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 154
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart;

    .line 155
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    .line 156
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 157
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->read_only_custom_note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart;->read_only_custom_note:Ljava/lang/String;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    .line 160
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->created_by_unit_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart;->created_by_unit_token:Ljava/lang/String;

    .line 162
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 167
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 169
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$LineItems;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$Amounts;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->read_only_custom_note:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->created_by_unit_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 177
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$Builder;
    .locals 2

    .line 138
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$Builder;-><init>()V

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$Builder;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$Builder;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$Builder;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->read_only_custom_note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$Builder;->read_only_custom_note:Ljava/lang/String;

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$Builder;->return_line_items:Ljava/util/List;

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$Builder;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->created_by_unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$Builder;->created_by_unit_token:Ljava/lang/String;

    .line 146
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart;->newBuilder()Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 185
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-eqz v1, :cond_0

    const-string v1, ", line_items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 186
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    if-eqz v1, :cond_1

    const-string v1, ", amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 187
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v1, :cond_2

    const-string v1, ", feature_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 188
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->read_only_custom_note:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", read_only_custom_note=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", return_line_items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 190
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    if-eqz v1, :cond_5

    const-string v1, ", amount_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 191
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->created_by_unit_token:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", created_by_unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->created_by_unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Cart{"

    .line 192
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
