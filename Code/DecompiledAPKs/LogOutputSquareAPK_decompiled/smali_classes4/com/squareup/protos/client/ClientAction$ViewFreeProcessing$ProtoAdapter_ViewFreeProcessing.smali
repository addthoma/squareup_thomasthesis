.class final Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing$ProtoAdapter_ViewFreeProcessing;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ClientAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ViewFreeProcessing"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 3900
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3915
    new-instance v0, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing$Builder;-><init>()V

    .line 3916
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 3917
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 3920
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 3924
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 3925
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3898
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing$ProtoAdapter_ViewFreeProcessing;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3910
    invoke-virtual {p2}, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3898
    check-cast p2, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing$ProtoAdapter_ViewFreeProcessing;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;)I
    .locals 0

    .line 3905
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 3898
    check-cast p1, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing$ProtoAdapter_ViewFreeProcessing;->encodedSize(Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;)Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;
    .locals 0

    .line 3930
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;->newBuilder()Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing$Builder;

    move-result-object p1

    .line 3931
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 3932
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 3898
    check-cast p1, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing$ProtoAdapter_ViewFreeProcessing;->redact(Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;)Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    move-result-object p1

    return-object p1
.end method
