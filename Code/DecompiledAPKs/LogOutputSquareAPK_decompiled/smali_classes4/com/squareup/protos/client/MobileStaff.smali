.class public final Lcom/squareup/protos/client/MobileStaff;
.super Lcom/squareup/wire/Message;
.source "MobileStaff.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/MobileStaff$ProtoAdapter_MobileStaff;,
        Lcom/squareup/protos/client/MobileStaff$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/MobileStaff;",
        "Lcom/squareup/protos/client/MobileStaff$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/MobileStaff;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FULL_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PHOTO_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_USER_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final full_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final photo_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final user_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/MobileStaff$ProtoAdapter_MobileStaff;

    invoke-direct {v0}, Lcom/squareup/protos/client/MobileStaff$ProtoAdapter_MobileStaff;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/MobileStaff;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 55
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/MobileStaff;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 60
    sget-object v0, Lcom/squareup/protos/client/MobileStaff;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 61
    iput-object p1, p0, Lcom/squareup/protos/client/MobileStaff;->user_token:Ljava/lang/String;

    .line 62
    iput-object p2, p0, Lcom/squareup/protos/client/MobileStaff;->photo_url:Ljava/lang/String;

    .line 63
    iput-object p3, p0, Lcom/squareup/protos/client/MobileStaff;->full_name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 79
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/MobileStaff;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 80
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/MobileStaff;

    .line 81
    invoke-virtual {p0}, Lcom/squareup/protos/client/MobileStaff;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/MobileStaff;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/MobileStaff;->user_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/MobileStaff;->user_token:Ljava/lang/String;

    .line 82
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/MobileStaff;->photo_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/MobileStaff;->photo_url:Ljava/lang/String;

    .line 83
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/MobileStaff;->full_name:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/MobileStaff;->full_name:Ljava/lang/String;

    .line 84
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 89
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/client/MobileStaff;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/MobileStaff;->user_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/MobileStaff;->photo_url:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/client/MobileStaff;->full_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 95
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/MobileStaff$Builder;
    .locals 2

    .line 68
    new-instance v0, Lcom/squareup/protos/client/MobileStaff$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/MobileStaff$Builder;-><init>()V

    .line 69
    iget-object v1, p0, Lcom/squareup/protos/client/MobileStaff;->user_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/MobileStaff$Builder;->user_token:Ljava/lang/String;

    .line 70
    iget-object v1, p0, Lcom/squareup/protos/client/MobileStaff;->photo_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/MobileStaff$Builder;->photo_url:Ljava/lang/String;

    .line 71
    iget-object v1, p0, Lcom/squareup/protos/client/MobileStaff;->full_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/MobileStaff$Builder;->full_name:Ljava/lang/String;

    .line 72
    invoke-virtual {p0}, Lcom/squareup/protos/client/MobileStaff;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/MobileStaff$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/MobileStaff;->newBuilder()Lcom/squareup/protos/client/MobileStaff$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/MobileStaff;->user_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", user_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/MobileStaff;->user_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/MobileStaff;->photo_url:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", photo_url=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/MobileStaff;->full_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", full_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "MobileStaff{"

    .line 106
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
