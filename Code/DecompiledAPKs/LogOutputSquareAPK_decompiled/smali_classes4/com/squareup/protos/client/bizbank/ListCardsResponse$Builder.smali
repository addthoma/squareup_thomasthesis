.class public final Lcom/squareup/protos/client/bizbank/ListCardsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListCardsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ListCardsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse;",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cards:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 96
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 97
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$Builder;->cards:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/ListCardsResponse;
    .locals 4

    .line 113
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$Builder;->cards:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bizbank/ListCardsResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/client/Status;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/ListCardsResponse;

    move-result-object v0

    return-object v0
.end method

.method public cards(Ljava/util/List;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ">;)",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$Builder;"
        }
    .end annotation

    .line 101
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$Builder;->cards:Ljava/util/List;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
