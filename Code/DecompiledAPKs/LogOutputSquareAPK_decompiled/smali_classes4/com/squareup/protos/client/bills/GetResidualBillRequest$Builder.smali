.class public final Lcom/squareup/protos/client/bills/GetResidualBillRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetResidualBillRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetResidualBillRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/GetResidualBillRequest;",
        "Lcom/squareup/protos/client/bills/GetResidualBillRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_server_token:Ljava/lang/String;

.field public bill_unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 110
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetResidualBillRequest$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillRequest$Builder;->bill_server_token:Ljava/lang/String;

    return-object p0
.end method

.method public bill_unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetResidualBillRequest$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillRequest$Builder;->bill_unit_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/GetResidualBillRequest;
    .locals 4

    .line 131
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillRequest$Builder;->bill_server_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/GetResidualBillRequest$Builder;->bill_unit_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/GetResidualBillRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillRequest$Builder;->build()Lcom/squareup/protos/client/bills/GetResidualBillRequest;

    move-result-object v0

    return-object v0
.end method
