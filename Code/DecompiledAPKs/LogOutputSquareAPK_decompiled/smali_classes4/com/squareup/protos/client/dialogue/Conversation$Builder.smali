.class public final Lcom/squareup/protos/client/dialogue/Conversation$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Conversation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/Conversation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/dialogue/Conversation;",
        "Lcom/squareup/protos/client/dialogue/Conversation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_merchant_response:Ljava/lang/Boolean;

.field public bill:Lcom/squareup/protos/client/bills/Bill;

.field public channel:Lcom/squareup/protos/client/dialogue/Channel;

.field public comments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/Comment;",
            ">;"
        }
    .end annotation
.end field

.field public contact:Lcom/squareup/protos/client/dialogue/Contact;

.field public created_at:Lcom/squareup/protos/common/time/DateTime;

.field public facets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

.field public messages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/Message;",
            ">;"
        }
    .end annotation
.end field

.field public opened:Ljava/lang/Boolean;

.field public payment_token:Ljava/lang/String;

.field public reference_token:Ljava/lang/String;

.field public sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 326
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 327
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->facets:Ljava/util/List;

    .line 328
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->comments:Ljava/util/List;

    .line 329
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->messages:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public allow_merchant_response(Ljava/lang/Boolean;)Lcom/squareup/protos/client/dialogue/Conversation$Builder;
    .locals 0

    .line 415
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->allow_merchant_response:Ljava/lang/Boolean;

    return-object p0
.end method

.method public bill(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/protos/client/dialogue/Conversation$Builder;
    .locals 0

    .line 448
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->bill:Lcom/squareup/protos/client/bills/Bill;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/dialogue/Conversation;
    .locals 18

    move-object/from16 v0, p0

    .line 454
    new-instance v17, Lcom/squareup/protos/client/dialogue/Conversation;

    iget-object v2, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->token:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v4, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->payment_token:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    iget-object v6, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->opened:Ljava/lang/Boolean;

    iget-object v7, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    iget-object v8, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->facets:Ljava/util/List;

    iget-object v9, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->comments:Ljava/util/List;

    iget-object v10, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v11, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->allow_merchant_response:Ljava/lang/Boolean;

    iget-object v12, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->reference_token:Ljava/lang/String;

    iget-object v13, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->channel:Lcom/squareup/protos/client/dialogue/Channel;

    iget-object v14, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->messages:Ljava/util/List;

    iget-object v15, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->bill:Lcom/squareup/protos/client/bills/Bill;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object/from16 v1, v17

    invoke-direct/range {v1 .. v16}, Lcom/squareup/protos/client/dialogue/Conversation;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/client/dialogue/Contact;Ljava/lang/Boolean;Lcom/squareup/protos/client/dialogue/Sentiment;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/client/dialogue/Channel;Ljava/util/List;Lcom/squareup/protos/client/bills/Bill;Lokio/ByteString;)V

    return-object v17
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 297
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->build()Lcom/squareup/protos/client/dialogue/Conversation;

    move-result-object v0

    return-object v0
.end method

.method public channel(Lcom/squareup/protos/client/dialogue/Channel;)Lcom/squareup/protos/client/dialogue/Conversation$Builder;
    .locals 0

    .line 431
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->channel:Lcom/squareup/protos/client/dialogue/Channel;

    return-object p0
.end method

.method public comments(Ljava/util/List;)Lcom/squareup/protos/client/dialogue/Conversation$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/Comment;",
            ">;)",
            "Lcom/squareup/protos/client/dialogue/Conversation$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 398
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 399
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->comments:Ljava/util/List;

    return-object p0
.end method

.method public contact(Lcom/squareup/protos/client/dialogue/Contact;)Lcom/squareup/protos/client/dialogue/Conversation$Builder;
    .locals 0

    .line 363
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    return-object p0
.end method

.method public created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/dialogue/Conversation$Builder;
    .locals 0

    .line 407
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public facets(Ljava/util/List;)Lcom/squareup/protos/client/dialogue/Conversation$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/dialogue/Conversation$Builder;"
        }
    .end annotation

    .line 388
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 389
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->facets:Ljava/util/List;

    return-object p0
.end method

.method public last_comment_created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/dialogue/Conversation$Builder;
    .locals 0

    .line 346
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public messages(Ljava/util/List;)Lcom/squareup/protos/client/dialogue/Conversation$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/Message;",
            ">;)",
            "Lcom/squareup/protos/client/dialogue/Conversation$Builder;"
        }
    .end annotation

    .line 439
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 440
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->messages:Ljava/util/List;

    return-object p0
.end method

.method public opened(Ljava/lang/Boolean;)Lcom/squareup/protos/client/dialogue/Conversation$Builder;
    .locals 0

    .line 371
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->opened:Ljava/lang/Boolean;

    return-object p0
.end method

.method public payment_token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/Conversation$Builder;
    .locals 0

    .line 355
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->payment_token:Ljava/lang/String;

    return-object p0
.end method

.method public reference_token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/Conversation$Builder;
    .locals 0

    .line 423
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->reference_token:Ljava/lang/String;

    return-object p0
.end method

.method public sentiment(Lcom/squareup/protos/client/dialogue/Sentiment;)Lcom/squareup/protos/client/dialogue/Conversation$Builder;
    .locals 0

    .line 379
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/Conversation$Builder;
    .locals 0

    .line 336
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
