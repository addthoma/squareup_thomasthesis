.class public final Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetInstantDepositFeeDetailsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;",
        "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public deposit_amount:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 81
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;
    .locals 3

    .line 94
    new-instance v0, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest$Builder;->deposit_amount:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;-><init>(Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest$Builder;->build()Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;

    move-result-object v0

    return-object v0
.end method

.method public deposit_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest$Builder;
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest$Builder;->deposit_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
