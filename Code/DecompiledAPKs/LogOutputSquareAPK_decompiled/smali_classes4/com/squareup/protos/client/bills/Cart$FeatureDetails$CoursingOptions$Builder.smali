.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public course:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 3482
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 3483
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;->course:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;
    .locals 3

    .line 3494
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;->course:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3479
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    move-result-object v0

    return-object v0
.end method

.method public course(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;"
        }
    .end annotation

    .line 3487
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 3488
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;->course:Ljava/util/List;

    return-object p0
.end method
