.class public final enum Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;
.super Ljava/lang/Enum;
.source "InvoiceDisplayDetails.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DisplayState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState$ProtoAdapter_DisplayState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CANCELLED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

.field public static final enum DRAFT:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

.field public static final enum FAILED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

.field public static final enum OVERDUE:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

.field public static final enum PAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

.field public static final enum PARTIALLY_PAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

.field public static final enum RECURRING:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

.field public static final enum REFUNDED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

.field public static final enum SCHEDULED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

.field public static final enum UNDELIVERED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

.field public static final enum UNPAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

.field public static final enum UNPUBLISHED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 647
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNKNOWN:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 652
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/4 v2, 0x1

    const-string v3, "DRAFT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->DRAFT:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 658
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/4 v3, 0x2

    const-string v4, "UNPAID"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNPAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 663
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/4 v4, 0x3

    const-string v5, "PAID"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->PAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 669
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/4 v5, 0x4

    const-string v6, "OVERDUE"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->OVERDUE:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 674
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/4 v6, 0x5

    const-string v7, "UNDELIVERED"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNDELIVERED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 679
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/4 v7, 0x6

    const-string v8, "CANCELLED"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->CANCELLED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 684
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/4 v8, 0x7

    const-string v9, "SCHEDULED"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->SCHEDULED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 689
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/16 v9, 0x8

    const-string v10, "REFUNDED"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->REFUNDED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 694
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/16 v10, 0x9

    const-string v11, "RECURRING"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->RECURRING:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 699
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/16 v11, 0xa

    const-string v12, "FAILED"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->FAILED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 704
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/16 v12, 0xb

    const-string v13, "PARTIALLY_PAID"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->PARTIALLY_PAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 709
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/16 v13, 0xc

    const-string v14, "UNPUBLISHED"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNPUBLISHED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/16 v0, 0xd

    new-array v0, v0, [Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 643
    sget-object v14, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNKNOWN:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    aput-object v14, v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->DRAFT:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNPAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->PAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->OVERDUE:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNDELIVERED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->CANCELLED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->SCHEDULED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->REFUNDED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->RECURRING:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->FAILED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->PARTIALLY_PAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNPUBLISHED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    aput-object v1, v0, v13

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->$VALUES:[Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 711
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState$ProtoAdapter_DisplayState;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState$ProtoAdapter_DisplayState;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 715
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 716
    iput p3, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 736
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNPUBLISHED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    return-object p0

    .line 735
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->PARTIALLY_PAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    return-object p0

    .line 734
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->FAILED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    return-object p0

    .line 733
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->RECURRING:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    return-object p0

    .line 732
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->REFUNDED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    return-object p0

    .line 731
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->SCHEDULED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    return-object p0

    .line 730
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->CANCELLED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    return-object p0

    .line 729
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNDELIVERED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    return-object p0

    .line 728
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->OVERDUE:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    return-object p0

    .line 727
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->PAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    return-object p0

    .line 726
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNPAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    return-object p0

    .line 725
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->DRAFT:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    return-object p0

    .line 724
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNKNOWN:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;
    .locals 1

    .line 643
    const-class v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;
    .locals 1

    .line 643
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->$VALUES:[Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 743
    iget v0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->value:I

    return v0
.end method
