.class public final Lcom/squareup/protos/client/bills/TipLineItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TipLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/TipLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/TipLineItem;",
        "Lcom/squareup/protos/client/bills/TipLineItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amounts:Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

.field public tender_server_token:Ljava/lang/String;

.field public tip_line_item_id_pair:Lcom/squareup/protos/client/IdPair;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 118
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amounts(Lcom/squareup/protos/client/bills/TipLineItem$Amounts;)Lcom/squareup/protos/client/bills/TipLineItem$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/client/bills/TipLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/TipLineItem;
    .locals 5

    .line 144
    new-instance v0, Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/TipLineItem$Builder;->tip_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/TipLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/TipLineItem$Builder;->tender_server_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/TipLineItem;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/TipLineItem$Amounts;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/TipLineItem$Builder;->build()Lcom/squareup/protos/client/bills/TipLineItem;

    move-result-object v0

    return-object v0
.end method

.method public tender_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/TipLineItem$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/client/bills/TipLineItem$Builder;->tender_server_token:Ljava/lang/String;

    return-object p0
.end method

.method public tip_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/TipLineItem$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/bills/TipLineItem$Builder;->tip_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method
