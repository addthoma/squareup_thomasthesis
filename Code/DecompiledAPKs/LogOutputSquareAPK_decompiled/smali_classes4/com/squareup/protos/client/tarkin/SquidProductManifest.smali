.class public final Lcom/squareup/protos/client/tarkin/SquidProductManifest;
.super Lcom/squareup/wire/Message;
.source "SquidProductManifest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tarkin/SquidProductManifest$ProtoAdapter_SquidProductManifest;,
        Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/tarkin/SquidProductManifest;",
        "Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tarkin/SquidProductManifest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COUNTRY_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_SPE_MANIFEST:Lokio/ByteString;

.field public static final DEFAULT_SQUID_MANIFEST:Lokio/ByteString;

.field public static final DEFAULT_UNIT_SERIAL_NUMBER:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final country_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final spe_manifest:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x3
    .end annotation
.end field

.field public final squid_manifest:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x2
    .end annotation
.end field

.field public final unit_serial_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/tarkin/SquidProductManifest$ProtoAdapter_SquidProductManifest;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/SquidProductManifest$ProtoAdapter_SquidProductManifest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 29
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->DEFAULT_SQUID_MANIFEST:Lokio/ByteString;

    .line 31
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->DEFAULT_SPE_MANIFEST:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;Ljava/lang/String;)V
    .locals 6

    .line 64
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/tarkin/SquidProductManifest;-><init>(Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 69
    sget-object v0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 70
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->unit_serial_number:Ljava/lang/String;

    .line 71
    iput-object p2, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->squid_manifest:Lokio/ByteString;

    .line 72
    iput-object p3, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->spe_manifest:Lokio/ByteString;

    .line 73
    iput-object p4, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->country_code:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 90
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 91
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->unit_serial_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->unit_serial_number:Ljava/lang/String;

    .line 93
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->squid_manifest:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->squid_manifest:Lokio/ByteString;

    .line 94
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->spe_manifest:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->spe_manifest:Lokio/ByteString;

    .line 95
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->country_code:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->country_code:Ljava/lang/String;

    .line 96
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 101
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->unit_serial_number:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->squid_manifest:Lokio/ByteString;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->spe_manifest:Lokio/ByteString;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->country_code:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 108
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;
    .locals 2

    .line 78
    new-instance v0, Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;-><init>()V

    .line 79
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->unit_serial_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;->unit_serial_number:Ljava/lang/String;

    .line 80
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->squid_manifest:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;->squid_manifest:Lokio/ByteString;

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->spe_manifest:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;->spe_manifest:Lokio/ByteString;

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->country_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;->country_code:Ljava/lang/String;

    .line 83
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->newBuilder()Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->unit_serial_number:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", unit_serial_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->unit_serial_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->squid_manifest:Lokio/ByteString;

    if-eqz v1, :cond_1

    const-string v1, ", squid_manifest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->squid_manifest:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 118
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->spe_manifest:Lokio/ByteString;

    if-eqz v1, :cond_2

    const-string v1, ", spe_manifest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->spe_manifest:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 119
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->country_code:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", country_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->country_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SquidProductManifest{"

    .line 120
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
