.class final Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$ProtoAdapter_DeleteGroupRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "DeleteGroupRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_DeleteGroupRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 95
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 112
    new-instance v0, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$Builder;-><init>()V

    .line 113
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 114
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 118
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 116
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$Builder;->group_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$Builder;

    goto :goto_0

    .line 122
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 123
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$ProtoAdapter_DeleteGroupRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 106
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;->group_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 107
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 93
    check-cast p2, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$ProtoAdapter_DeleteGroupRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;)I
    .locals 3

    .line 100
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;->group_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 101
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 93
    check-cast p1, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$ProtoAdapter_DeleteGroupRequest;->encodedSize(Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;)Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;
    .locals 0

    .line 128
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;->newBuilder()Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$Builder;

    move-result-object p1

    .line 129
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 130
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 93
    check-cast p1, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$ProtoAdapter_DeleteGroupRequest;->redact(Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;)Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;

    move-result-object p1

    return-object p1
.end method
