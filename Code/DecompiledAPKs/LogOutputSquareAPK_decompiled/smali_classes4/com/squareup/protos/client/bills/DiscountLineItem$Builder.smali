.class public final Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DiscountLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/DiscountLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/DiscountLineItem;",
        "Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

.field public application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

.field public application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

.field public configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

.field public created_at:Lcom/squareup/protos/client/ISO8601Date;

.field public discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

.field public discount_quantity:Ljava/lang/String;

.field public read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

.field public write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

.field public write_only_deleted:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 252
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amounts(Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;
    .locals 0

    .line 294
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    return-object p0
.end method

.method public application_method(Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;
    .locals 0

    .line 323
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    return-object p0
.end method

.method public application_scope(Lcom/squareup/protos/client/bills/ApplicationScope;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;
    .locals 0

    .line 315
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/DiscountLineItem;
    .locals 13

    .line 340
    new-instance v12, Lcom/squareup/protos/client/bills/DiscountLineItem;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_deleted:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    iget-object v9, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    iget-object v10, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_quantity:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/client/bills/DiscountLineItem;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/ApplicationScope;Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;Ljava/lang/String;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 231
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object v0

    return-object v0
.end method

.method public configuration(Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;
    .locals 0

    .line 273
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    return-object p0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;
    .locals 0

    .line 268
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public discount_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;
    .locals 0

    .line 259
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public discount_quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;
    .locals 0

    .line 334
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_quantity:Ljava/lang/String;

    return-object p0
.end method

.method public read_only_display_details(Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;
    .locals 0

    .line 289
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    return-object p0
.end method

.method public write_only_backing_details(Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;
    .locals 0

    .line 281
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    return-object p0
.end method

.method public write_only_deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;
    .locals 0

    .line 303
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_deleted:Ljava/lang/Boolean;

    return-object p0
.end method
