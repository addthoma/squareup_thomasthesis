.class public final Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ScheduleRecurringSeriesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest;",
        "Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public notify_recipients:Ljava/lang/Boolean;

.field public recurring_invoice:Lcom/squareup/protos/client/invoice/RecurringSeries;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest;
    .locals 4

    .line 110
    new-instance v0, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest$Builder;->recurring_invoice:Lcom/squareup/protos/client/invoice/RecurringSeries;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest$Builder;->notify_recipients:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest;-><init>(Lcom/squareup/protos/client/invoice/RecurringSeries;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest$Builder;->build()Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest;

    move-result-object v0

    return-object v0
.end method

.method public notify_recipients(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest$Builder;
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest$Builder;->notify_recipients:Ljava/lang/Boolean;

    return-object p0
.end method

.method public recurring_invoice(Lcom/squareup/protos/client/invoice/RecurringSeries;)Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest$Builder;
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest$Builder;->recurring_invoice:Lcom/squareup/protos/client/invoice/RecurringSeries;

    return-object p0
.end method
