.class public final Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MerchantOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/MerchantOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/MerchantOptions;",
        "Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public include_counts:Ljava/lang/Boolean;

.field public include_default_attribute_definitions:Ljava/lang/Boolean;

.field public include_messages_counts:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 117
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/MerchantOptions;
    .locals 5

    .line 143
    new-instance v0, Lcom/squareup/protos/client/rolodex/MerchantOptions;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;->include_counts:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;->include_messages_counts:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;->include_default_attribute_definitions:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/rolodex/MerchantOptions;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;->build()Lcom/squareup/protos/client/rolodex/MerchantOptions;

    move-result-object v0

    return-object v0
.end method

.method public include_counts(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;->include_counts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_default_attribute_definitions(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;->include_default_attribute_definitions:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_messages_counts(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;->include_messages_counts:Ljava/lang/Boolean;

    return-object p0
.end method
