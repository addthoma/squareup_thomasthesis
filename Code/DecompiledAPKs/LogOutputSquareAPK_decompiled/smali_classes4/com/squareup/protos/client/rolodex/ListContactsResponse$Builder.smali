.class public final Lcom/squareup/protos/client/rolodex/ListContactsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListContactsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ListContactsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ListContactsResponse;",
        "Lcom/squareup/protos/client/rolodex/ListContactsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public paging_key:Ljava/lang/String;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 112
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 113
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/ListContactsResponse$Builder;->contact:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ListContactsResponse;
    .locals 5

    .line 137
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListContactsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListContactsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/ListContactsResponse$Builder;->contact:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/ListContactsResponse$Builder;->paging_key:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/rolodex/ListContactsResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListContactsResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/ListContactsResponse;

    move-result-object v0

    return-object v0
.end method

.method public contact(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/ListContactsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/ListContactsResponse$Builder;"
        }
    .end annotation

    .line 122
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListContactsResponse$Builder;->contact:Ljava/util/List;

    return-object p0
.end method

.method public paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/ListContactsResponse$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListContactsResponse$Builder;->paging_key:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/ListContactsResponse$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListContactsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
