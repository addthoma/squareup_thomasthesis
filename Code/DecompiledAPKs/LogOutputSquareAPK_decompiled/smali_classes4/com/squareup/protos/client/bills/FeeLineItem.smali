.class public final Lcom/squareup/protos/client/bills/FeeLineItem;
.super Lcom/squareup/wire/Message;
.source "FeeLineItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/FeeLineItem$ProtoAdapter_FeeLineItem;,
        Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;,
        Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;,
        Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;,
        Lcom/squareup/protos/client/bills/FeeLineItem$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/FeeLineItem;",
        "Lcom/squareup/protos/client/bills/FeeLineItem$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_WRITE_ONLY_DELETED:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.FeeLineItem$Amounts#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final created_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.FeeLineItem$DisplayDetails#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.TranslatedName#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.FeeLineItem$BackingDetails#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final write_only_deleted:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/protos/client/bills/FeeLineItem$ProtoAdapter_FeeLineItem;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/FeeLineItem$ProtoAdapter_FeeLineItem;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/FeeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 33
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/FeeLineItem;->DEFAULT_WRITE_ONLY_DELETED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;Lcom/squareup/protos/client/bills/TranslatedName;Ljava/lang/Boolean;)V
    .locals 9

    .line 97
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/FeeLineItem;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;Lcom/squareup/protos/client/bills/TranslatedName;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;Lcom/squareup/protos/client/bills/TranslatedName;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 104
    sget-object v0, Lcom/squareup/protos/client/bills/FeeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 106
    iput-object p2, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 107
    iput-object p3, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    .line 108
    iput-object p4, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    .line 109
    iput-object p5, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    .line 110
    iput-object p6, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    .line 111
    iput-object p7, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 131
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/FeeLineItem;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 132
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 133
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/FeeLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/FeeLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 134
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 135
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    .line 136
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    .line 137
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    .line 138
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    .line 140
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 145
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 147
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/FeeLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/TranslatedName;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 155
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/FeeLineItem$Builder;
    .locals 2

    .line 116
    new-instance v0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;-><init>()V

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->write_only_deleted:Ljava/lang/Boolean;

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/FeeLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/FeeLineItem;->newBuilder()Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", fee_line_item_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 164
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 165
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    if-eqz v1, :cond_2

    const-string v1, ", write_only_backing_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 166
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    if-eqz v1, :cond_3

    const-string v1, ", read_only_display_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 167
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    if-eqz v1, :cond_4

    const-string v1, ", amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 168
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    if-eqz v1, :cond_5

    const-string v1, ", read_only_translated_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 169
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", write_only_deleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FeeLineItem{"

    .line 170
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
