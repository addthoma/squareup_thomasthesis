.class public final Lcom/squareup/protos/client/bills/RefundV1$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RefundV1.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/RefundV1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/RefundV1;",
        "Lcom/squareup/protos/client/bills/RefundV1$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amounts:Lcom/squareup/protos/client/bills/RefundV1$Amounts;

.field public created_at:Lcom/squareup/protos/client/ISO8601Date;

.field public creator_details:Lcom/squareup/protos/client/CreatorDetails;

.field public refund_id_pair:Lcom/squareup/protos/client/IdPair;

.field public refund_reason:Ljava/lang/String;

.field public state:Lcom/squareup/protos/client/bills/RefundV1$State;

.field public tender_id_pair:Lcom/squareup/protos/client/IdPair;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 180
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amounts(Lcom/squareup/protos/client/bills/RefundV1$Amounts;)Lcom/squareup/protos/client/bills/RefundV1$Builder;
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->amounts:Lcom/squareup/protos/client/bills/RefundV1$Amounts;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/RefundV1;
    .locals 10

    .line 232
    new-instance v9, Lcom/squareup/protos/client/bills/RefundV1;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->state:Lcom/squareup/protos/client/bills/RefundV1$State;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->refund_reason:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->amounts:Lcom/squareup/protos/client/bills/RefundV1$Amounts;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/RefundV1;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/RefundV1$State;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/client/bills/RefundV1$Amounts;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/CreatorDetails;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 165
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RefundV1$Builder;->build()Lcom/squareup/protos/client/bills/RefundV1;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/RefundV1$Builder;
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/RefundV1$Builder;
    .locals 0

    .line 226
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public refund_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/RefundV1$Builder;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public refund_reason(Ljava/lang/String;)Lcom/squareup/protos/client/bills/RefundV1$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->refund_reason:Ljava/lang/String;

    return-object p0
.end method

.method public state(Lcom/squareup/protos/client/bills/RefundV1$State;)Lcom/squareup/protos/client/bills/RefundV1$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->state:Lcom/squareup/protos/client/bills/RefundV1$State;

    return-object p0
.end method

.method public tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/RefundV1$Builder;
    .locals 0

    .line 218
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method
