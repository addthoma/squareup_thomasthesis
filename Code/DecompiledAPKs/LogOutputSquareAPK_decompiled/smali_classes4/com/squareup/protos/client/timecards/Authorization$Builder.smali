.class public final Lcom/squareup/protos/client/timecards/Authorization$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Authorization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/Authorization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/Authorization;",
        "Lcom/squareup/protos/client/timecards/Authorization$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public employee_token:Ljava/lang/String;

.field public using_team_passcode:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/Authorization;
    .locals 4

    .line 123
    new-instance v0, Lcom/squareup/protos/client/timecards/Authorization;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Authorization$Builder;->using_team_passcode:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/Authorization$Builder;->employee_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/timecards/Authorization;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/Authorization$Builder;->build()Lcom/squareup/protos/client/timecards/Authorization;

    move-result-object v0

    return-object v0
.end method

.method public employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/Authorization$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/Authorization$Builder;->employee_token:Ljava/lang/String;

    return-object p0
.end method

.method public using_team_passcode(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/Authorization$Builder;
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/Authorization$Builder;->using_team_passcode:Ljava/lang/Boolean;

    return-object p0
.end method
