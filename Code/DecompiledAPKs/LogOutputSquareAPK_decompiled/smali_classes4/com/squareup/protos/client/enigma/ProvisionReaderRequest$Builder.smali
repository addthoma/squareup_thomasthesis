.class public final Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ProvisionReaderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;",
        "Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public iksn:Ljava/lang/String;

.field public key_injection_cert:Ljava/lang/String;

.field public reader_type:Lcom/squareup/protos/client/enigma/ProvisionReaderType;

.field public signer_cert:Ljava/lang/String;

.field public terminal_cert:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 157
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;
    .locals 8

    .line 197
    new-instance v7, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->reader_type:Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    iget-object v2, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->signer_cert:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->terminal_cert:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->key_injection_cert:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->iksn:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;-><init>(Lcom/squareup/protos/client/enigma/ProvisionReaderType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 146
    invoke-virtual {p0}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->build()Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;

    move-result-object v0

    return-object v0
.end method

.method public iksn(Ljava/lang/String;)Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->iksn:Ljava/lang/String;

    return-object p0
.end method

.method public key_injection_cert(Ljava/lang/String;)Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->key_injection_cert:Ljava/lang/String;

    return-object p0
.end method

.method public reader_type(Lcom/squareup/protos/client/enigma/ProvisionReaderType;)Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->reader_type:Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    return-object p0
.end method

.method public signer_cert(Ljava/lang/String;)Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->signer_cert:Ljava/lang/String;

    return-object p0
.end method

.method public terminal_cert(Ljava/lang/String;)Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->terminal_cert:Ljava/lang/String;

    return-object p0
.end method
