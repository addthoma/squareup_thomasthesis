.class final Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$ProtoAdapter_CreateFirstDeviceCredentialResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CreateFirstDeviceCredentialResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CreateFirstDeviceCredentialResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 116
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 136
    new-instance v0, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$Builder;-><init>()V

    .line 137
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 138
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 143
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 141
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$Builder;->device_code(Ljava/lang/String;)Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$Builder;

    goto :goto_0

    .line 140
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$Builder;

    goto :goto_0

    .line 147
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 148
    invoke-virtual {v0}, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$Builder;->build()Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 114
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$ProtoAdapter_CreateFirstDeviceCredentialResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 129
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 130
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;->device_code:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 131
    invoke-virtual {p2}, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 114
    check-cast p2, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$ProtoAdapter_CreateFirstDeviceCredentialResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;)I
    .locals 4

    .line 121
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;->device_code:Ljava/lang/String;

    const/4 v3, 0x2

    .line 122
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    invoke-virtual {p1}, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 114
    check-cast p1, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$ProtoAdapter_CreateFirstDeviceCredentialResponse;->encodedSize(Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;)Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;
    .locals 2

    .line 153
    invoke-virtual {p1}, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;->newBuilder()Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$Builder;

    move-result-object p1

    .line 154
    iget-object v0, p1, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Status;

    iput-object v0, p1, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    :cond_0
    const/4 v0, 0x0

    .line 155
    iput-object v0, p1, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$Builder;->device_code:Ljava/lang/String;

    .line 156
    invoke-virtual {p1}, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 157
    invoke-virtual {p1}, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$Builder;->build()Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 114
    check-cast p1, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse$ProtoAdapter_CreateFirstDeviceCredentialResponse;->redact(Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;)Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialResponse;

    move-result-object p1

    return-object p1
.end method
