.class final Lcom/squareup/protos/client/bills/ModifierOptionLineItem$ProtoAdapter_ModifierOptionLineItem;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ModifierOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ModifierOptionLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ModifierOptionLineItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1237
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1264
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;-><init>()V

    .line 1265
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1266
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1275
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1273
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->feature_details(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    goto :goto_0

    .line 1272
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->hide_from_customer(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    goto :goto_0

    .line 1271
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->write_only_backing_details(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    goto :goto_0

    .line 1270
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->read_only_display_details(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    goto :goto_0

    .line 1269
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    goto :goto_0

    .line 1268
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->modifier_option_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    goto :goto_0

    .line 1279
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1280
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1235
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$ProtoAdapter_ModifierOptionLineItem;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1253
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1254
    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1255
    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1256
    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1257
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->hide_from_customer:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1258
    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1259
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1235
    check-cast p2, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$ProtoAdapter_ModifierOptionLineItem;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)I
    .locals 4

    .line 1242
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    const/4 v3, 0x2

    .line 1243
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    const/4 v3, 0x3

    .line 1244
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    const/4 v3, 0x4

    .line 1245
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->hide_from_customer:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 1246
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    const/4 v3, 0x6

    .line 1247
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1248
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1235
    check-cast p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$ProtoAdapter_ModifierOptionLineItem;->encodedSize(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem;
    .locals 2

    .line 1285
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object p1

    .line 1286
    iget-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 1287
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    .line 1288
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    .line 1289
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    .line 1290
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    .line 1291
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1292
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1235
    check-cast p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$ProtoAdapter_ModifierOptionLineItem;->redact(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    move-result-object p1

    return-object p1
.end method
