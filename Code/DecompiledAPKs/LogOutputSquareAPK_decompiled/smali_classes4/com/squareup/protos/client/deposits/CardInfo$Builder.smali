.class public final Lcom/squareup/protos/client/deposits/CardInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/deposits/CardInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/deposits/CardInfo;",
        "Lcom/squareup/protos/client/deposits/CardInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public instrument_token:Ljava/lang/String;

.field public pan_suffix:Ljava/lang/String;

.field public supports_instant_deposit:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 140
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public brand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/protos/client/deposits/CardInfo$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/CardInfo$Builder;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/deposits/CardInfo;
    .locals 7

    .line 177
    new-instance v6, Lcom/squareup/protos/client/deposits/CardInfo;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo$Builder;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v2, p0, Lcom/squareup/protos/client/deposits/CardInfo$Builder;->pan_suffix:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/deposits/CardInfo$Builder;->supports_instant_deposit:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/deposits/CardInfo$Builder;->instrument_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/deposits/CardInfo;-><init>(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/CardInfo$Builder;->build()Lcom/squareup/protos/client/deposits/CardInfo;

    move-result-object v0

    return-object v0
.end method

.method public instrument_token(Ljava/lang/String;)Lcom/squareup/protos/client/deposits/CardInfo$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/CardInfo$Builder;->instrument_token:Ljava/lang/String;

    return-object p0
.end method

.method public pan_suffix(Ljava/lang/String;)Lcom/squareup/protos/client/deposits/CardInfo$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/CardInfo$Builder;->pan_suffix:Ljava/lang/String;

    return-object p0
.end method

.method public supports_instant_deposit(Ljava/lang/Boolean;)Lcom/squareup/protos/client/deposits/CardInfo$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/CardInfo$Builder;->supports_instant_deposit:Ljava/lang/Boolean;

    return-object p0
.end method
