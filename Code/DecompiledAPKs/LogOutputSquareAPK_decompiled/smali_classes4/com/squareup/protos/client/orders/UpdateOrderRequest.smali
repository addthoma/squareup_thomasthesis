.class public final Lcom/squareup/protos/client/orders/UpdateOrderRequest;
.super Lcom/squareup/wire/Message;
.source "UpdateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/orders/UpdateOrderRequest$ProtoAdapter_UpdateOrderRequest;,
        Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/orders/UpdateOrderRequest;",
        "Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/orders/UpdateOrderRequest;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final client_support:Lcom/squareup/protos/client/orders/ClientSupport;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.ClientSupport#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final fields_to_clear:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final order:Lcom/squareup/orders/model/Order;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/orders/UpdateOrderRequest$ProtoAdapter_UpdateOrderRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/UpdateOrderRequest$ProtoAdapter_UpdateOrderRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/orders/ClientSupport;Lcom/squareup/orders/model/Order;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/orders/ClientSupport;",
            "Lcom/squareup/orders/model/Order;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 57
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/orders/UpdateOrderRequest;-><init>(Lcom/squareup/protos/client/orders/ClientSupport;Lcom/squareup/orders/model/Order;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/orders/ClientSupport;Lcom/squareup/orders/model/Order;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/orders/ClientSupport;",
            "Lcom/squareup/orders/model/Order;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 62
    sget-object v0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 63
    iput-object p1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    .line 64
    iput-object p2, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->order:Lcom/squareup/orders/model/Order;

    const-string p1, "fields_to_clear"

    .line 65
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->fields_to_clear:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 81
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/orders/UpdateOrderRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 82
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/orders/UpdateOrderRequest;

    .line 83
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    .line 84
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->order:Lcom/squareup/orders/model/Order;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->order:Lcom/squareup/orders/model/Order;

    .line 85
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->fields_to_clear:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->fields_to_clear:Ljava/util/List;

    .line 86
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 91
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/ClientSupport;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->order:Lcom/squareup/orders/model/Order;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->fields_to_clear:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;
    .locals 2

    .line 70
    new-instance v0, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;-><init>()V

    .line 71
    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->order:Lcom/squareup/orders/model/Order;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->order:Lcom/squareup/orders/model/Order;

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->fields_to_clear:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->fields_to_clear:Ljava/util/List;

    .line 74
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->newBuilder()Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    if-eqz v1, :cond_0

    const-string v1, ", client_support="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 106
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->order:Lcom/squareup/orders/model/Order;

    if-eqz v1, :cond_1

    const-string v1, ", order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->order:Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 107
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->fields_to_clear:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", fields_to_clear="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderRequest;->fields_to_clear:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "UpdateOrderRequest{"

    .line 108
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
