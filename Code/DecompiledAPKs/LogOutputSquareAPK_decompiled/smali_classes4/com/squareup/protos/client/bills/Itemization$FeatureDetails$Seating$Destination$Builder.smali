.class public final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public created_at:Lcom/squareup/protos/client/ISO8601Date;

.field public seat_id_pair:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;"
        }
    .end annotation
.end field

.field public type:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 4466
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 4467
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->seat_id_pair:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;
    .locals 5

    .line 4495
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->type:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->seat_id_pair:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;-><init>(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;Ljava/util/List;Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4459
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;
    .locals 0

    .line 4489
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public seat_id_pair(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;"
        }
    .end annotation

    .line 4480
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 4481
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->seat_id_pair:Ljava/util/List;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;
    .locals 0

    .line 4471
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->type:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

    return-object p0
.end method
