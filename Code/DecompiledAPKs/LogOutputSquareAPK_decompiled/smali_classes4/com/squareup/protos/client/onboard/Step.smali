.class public final Lcom/squareup/protos/client/onboard/Step;
.super Lcom/squareup/wire/Message;
.source "Step.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/onboard/Step$ProtoAdapter_Step;,
        Lcom/squareup/protos/client/onboard/Step$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/onboard/Step;",
        "Lcom/squareup/protos/client/onboard/Step$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/onboard/Step;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EVENT:Lcom/squareup/protos/client/onboard/TerminalEvent;

.field private static final serialVersionUID:J


# instance fields
.field public final event:Lcom/squareup/protos/client/onboard/TerminalEvent;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.onboard.TerminalEvent#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final panel:Lcom/squareup/protos/client/onboard/Panel;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.onboard.Panel#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/client/onboard/Step$ProtoAdapter_Step;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/Step$ProtoAdapter_Step;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/onboard/Step;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 24
    sget-object v0, Lcom/squareup/protos/client/onboard/TerminalEvent;->TERMINAL_EVENT_DO_NOT_USE:Lcom/squareup/protos/client/onboard/TerminalEvent;

    sput-object v0, Lcom/squareup/protos/client/onboard/Step;->DEFAULT_EVENT:Lcom/squareup/protos/client/onboard/TerminalEvent;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/onboard/Panel;Lcom/squareup/protos/client/onboard/TerminalEvent;)V
    .locals 1

    .line 39
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/onboard/Step;-><init>(Lcom/squareup/protos/client/onboard/Panel;Lcom/squareup/protos/client/onboard/TerminalEvent;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/onboard/Panel;Lcom/squareup/protos/client/onboard/TerminalEvent;Lokio/ByteString;)V
    .locals 1

    .line 43
    sget-object v0, Lcom/squareup/protos/client/onboard/Step;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 44
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p3

    const/4 v0, 0x1

    if-gt p3, v0, :cond_0

    .line 47
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Step;->panel:Lcom/squareup/protos/client/onboard/Panel;

    .line 48
    iput-object p2, p0, Lcom/squareup/protos/client/onboard/Step;->event:Lcom/squareup/protos/client/onboard/TerminalEvent;

    return-void

    .line 45
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of panel, event may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 63
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/onboard/Step;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 64
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/onboard/Step;

    .line 65
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/Step;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/Step;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Step;->panel:Lcom/squareup/protos/client/onboard/Panel;

    iget-object v3, p1, Lcom/squareup/protos/client/onboard/Step;->panel:Lcom/squareup/protos/client/onboard/Panel;

    .line 66
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Step;->event:Lcom/squareup/protos/client/onboard/TerminalEvent;

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/Step;->event:Lcom/squareup/protos/client/onboard/TerminalEvent;

    .line 67
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 72
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 74
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/Step;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Step;->panel:Lcom/squareup/protos/client/onboard/Panel;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/Panel;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Step;->event:Lcom/squareup/protos/client/onboard/TerminalEvent;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/TerminalEvent;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 77
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/onboard/Step$Builder;
    .locals 2

    .line 53
    new-instance v0, Lcom/squareup/protos/client/onboard/Step$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/Step$Builder;-><init>()V

    .line 54
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Step;->panel:Lcom/squareup/protos/client/onboard/Panel;

    iput-object v1, v0, Lcom/squareup/protos/client/onboard/Step$Builder;->panel:Lcom/squareup/protos/client/onboard/Panel;

    .line 55
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Step;->event:Lcom/squareup/protos/client/onboard/TerminalEvent;

    iput-object v1, v0, Lcom/squareup/protos/client/onboard/Step$Builder;->event:Lcom/squareup/protos/client/onboard/TerminalEvent;

    .line 56
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/Step;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/onboard/Step$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/Step;->newBuilder()Lcom/squareup/protos/client/onboard/Step$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Step;->panel:Lcom/squareup/protos/client/onboard/Panel;

    if-eqz v1, :cond_0

    const-string v1, ", panel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Step;->panel:Lcom/squareup/protos/client/onboard/Panel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Step;->event:Lcom/squareup/protos/client/onboard/TerminalEvent;

    if-eqz v1, :cond_1

    const-string v1, ", event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Step;->event:Lcom/squareup/protos/client/onboard/TerminalEvent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Step{"

    .line 87
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
