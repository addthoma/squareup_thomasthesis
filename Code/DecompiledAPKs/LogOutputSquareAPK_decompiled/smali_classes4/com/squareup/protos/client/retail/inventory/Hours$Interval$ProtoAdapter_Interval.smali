.class final Lcom/squareup/protos/client/retail/inventory/Hours$Interval$ProtoAdapter_Interval;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Hours.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/retail/inventory/Hours$Interval;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Interval"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/retail/inventory/Hours$Interval;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 220
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/retail/inventory/Hours$Interval;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 239
    new-instance v0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;-><init>()V

    .line 240
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 241
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 246
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 244
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->duration_minutes(Ljava/lang/Integer;)Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;

    goto :goto_0

    .line 243
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/time/LocalTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/LocalTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->start_time(Lcom/squareup/protos/common/time/LocalTime;)Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;

    goto :goto_0

    .line 250
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 251
    invoke-virtual {v0}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->build()Lcom/squareup/protos/client/retail/inventory/Hours$Interval;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 218
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$ProtoAdapter_Interval;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/retail/inventory/Hours$Interval;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/retail/inventory/Hours$Interval;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 232
    sget-object v0, Lcom/squareup/protos/common/time/LocalTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->start_time:Lcom/squareup/protos/common/time/LocalTime;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 233
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->duration_minutes:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 234
    invoke-virtual {p2}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 218
    check-cast p2, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$ProtoAdapter_Interval;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/retail/inventory/Hours$Interval;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/retail/inventory/Hours$Interval;)I
    .locals 4

    .line 225
    sget-object v0, Lcom/squareup/protos/common/time/LocalTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->start_time:Lcom/squareup/protos/common/time/LocalTime;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->duration_minutes:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 226
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    invoke-virtual {p1}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 218
    check-cast p1, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$ProtoAdapter_Interval;->encodedSize(Lcom/squareup/protos/client/retail/inventory/Hours$Interval;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/retail/inventory/Hours$Interval;)Lcom/squareup/protos/client/retail/inventory/Hours$Interval;
    .locals 2

    .line 256
    invoke-virtual {p1}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->newBuilder()Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;

    move-result-object p1

    .line 257
    iget-object v0, p1, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->start_time:Lcom/squareup/protos/common/time/LocalTime;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/time/LocalTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->start_time:Lcom/squareup/protos/common/time/LocalTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/LocalTime;

    iput-object v0, p1, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->start_time:Lcom/squareup/protos/common/time/LocalTime;

    .line 258
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 259
    invoke-virtual {p1}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->build()Lcom/squareup/protos/client/retail/inventory/Hours$Interval;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 218
    check-cast p1, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$ProtoAdapter_Interval;->redact(Lcom/squareup/protos/client/retail/inventory/Hours$Interval;)Lcom/squareup/protos/client/retail/inventory/Hours$Interval;

    move-result-object p1

    return-object p1
.end method
