.class public final Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;
.super Lcom/squareup/wire/Message;
.source "AccumulateLoyaltyStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$ProtoAdapter_AccumulateLoyaltyStatusRequest;,
        Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;",
        "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EMAIL_ADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_EMAIL_ADDRESS_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_ENROLL_INTENT:Ljava/lang/Boolean;

.field public static final DEFAULT_EXPECTED_POINT_ACCRUAL:Ljava/lang/Long;

.field public static final DEFAULT_PHONE_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_PHONE_NUMBER_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_REASON_FOR_POINT_ACCRUAL:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

.field public static final DEFAULT_TENDER_TYPE:Lcom/squareup/protos/client/bills/Tender$Type;

.field public static final DEFAULT_TRANSFER_FROM_EMAIL_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final bill_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final cart:Lcom/squareup/protos/client/bills/Cart;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final email_address:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x8
    .end annotation
.end field

.field public final email_address_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final enroll_intent:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final expected_point_accrual:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0xa
    .end annotation
.end field

.field public final newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyTermsOfService#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final phone_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final phone_number_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final reason_for_point_accrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyStatus$ReasonForPointAccrual#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final tender_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final tender_type:Lcom/squareup/protos/client/bills/Tender$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender$Type#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final transfer_from_email_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 26
    new-instance v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$ProtoAdapter_AccumulateLoyaltyStatusRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$ProtoAdapter_AccumulateLoyaltyStatusRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 32
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->DEFAULT_ENROLL_INTENT:Ljava/lang/Boolean;

    .line 34
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->UNKNOWN:Lcom/squareup/protos/client/bills/Tender$Type;

    sput-object v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->DEFAULT_TENDER_TYPE:Lcom/squareup/protos/client/bills/Tender$Type;

    const-wide/16 v0, 0x0

    .line 36
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->DEFAULT_EXPECTED_POINT_ACCRUAL:Ljava/lang/Long;

    .line 38
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_UNKNOWN:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    sput-object v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->DEFAULT_REASON_FOR_POINT_ACCRUAL:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/Tender$Type;Ljava/lang/Long;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 15

    .line 171
    sget-object v14, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/Tender$Type;Ljava/lang/Long;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/Tender$Type;Ljava/lang/Long;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 180
    sget-object v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p14}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 181
    invoke-static {p10, p11}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p14

    const/4 v0, 0x1

    if-gt p14, v0, :cond_1

    .line 184
    invoke-static {p12, p13}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p14

    if-gt p14, v0, :cond_0

    .line 187
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 188
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 189
    iput-object p3, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->transfer_from_email_token:Ljava/lang/String;

    .line 190
    iput-object p4, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->enroll_intent:Ljava/lang/Boolean;

    .line 191
    iput-object p5, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 192
    iput-object p6, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->expected_point_accrual:Ljava/lang/Long;

    .line 193
    iput-object p7, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->reason_for_point_accrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    .line 194
    iput-object p8, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 195
    iput-object p9, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    .line 196
    iput-object p10, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number:Ljava/lang/String;

    .line 197
    iput-object p11, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number_token:Ljava/lang/String;

    .line 198
    iput-object p12, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address:Ljava/lang/String;

    .line 199
    iput-object p13, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address_token:Ljava/lang/String;

    return-void

    .line 185
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of email_address, email_address_token may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 182
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of phone_number, phone_number_token may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 225
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 226
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;

    .line 227
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 228
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 229
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->transfer_from_email_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->transfer_from_email_token:Ljava/lang/String;

    .line 230
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->enroll_intent:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->enroll_intent:Ljava/lang/Boolean;

    .line 231
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 232
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->expected_point_accrual:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->expected_point_accrual:Ljava/lang/Long;

    .line 233
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->reason_for_point_accrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->reason_for_point_accrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    .line 234
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 235
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    .line 236
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number:Ljava/lang/String;

    .line 237
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number_token:Ljava/lang/String;

    .line 238
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address:Ljava/lang/String;

    .line 239
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address_token:Ljava/lang/String;

    .line 240
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 245
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_d

    .line 247
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 249
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 250
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->transfer_from_email_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 251
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->enroll_intent:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 252
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$Type;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 253
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->expected_point_accrual:Ljava/lang/Long;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 254
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->reason_for_point_accrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 255
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 256
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 257
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 258
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number_token:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 259
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 260
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address_token:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_c
    add-int/2addr v0, v2

    .line 261
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_d
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    .locals 2

    .line 204
    new-instance v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;-><init>()V

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->transfer_from_email_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->transfer_from_email_token:Ljava/lang/String;

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->enroll_intent:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->enroll_intent:Ljava/lang/Boolean;

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->expected_point_accrual:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->expected_point_accrual:Ljava/lang/Long;

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->reason_for_point_accrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->reason_for_point_accrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    .line 214
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->phone_number:Ljava/lang/String;

    .line 215
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->phone_number_token:Ljava/lang/String;

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->email_address:Ljava/lang/String;

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->email_address_token:Ljava/lang/String;

    .line 218
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->newBuilder()Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 268
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 269
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", tender_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 270
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_1

    const-string v1, ", cart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 271
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->transfer_from_email_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", transfer_from_email_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->transfer_from_email_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->enroll_intent:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", enroll_intent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->enroll_intent:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 273
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    if-eqz v1, :cond_4

    const-string v1, ", tender_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 274
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->expected_point_accrual:Ljava/lang/Long;

    if-eqz v1, :cond_5

    const-string v1, ", expected_point_accrual="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->expected_point_accrual:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 275
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->reason_for_point_accrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    if-eqz v1, :cond_6

    const-string v1, ", reason_for_point_accrual="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->reason_for_point_accrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 276
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_7

    const-string v1, ", bill_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 277
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    if-eqz v1, :cond_8

    const-string v1, ", newly_accepted_tos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 278
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", phone_number=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number_token:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", phone_number_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", email_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address_token:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", email_address_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AccumulateLoyaltyStatusRequest{"

    .line 282
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
