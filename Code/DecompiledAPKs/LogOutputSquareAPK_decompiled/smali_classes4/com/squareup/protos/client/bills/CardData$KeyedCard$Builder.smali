.class public final Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData$KeyedCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CardData$KeyedCard;",
        "Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card_number:Ljava/lang/String;

.field public cvv:Ljava/lang/String;

.field public expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

.field public postal_code:Ljava/lang/String;

.field public raw_card_number_utf8:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 512
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/CardData$KeyedCard;
    .locals 8

    .line 556
    new-instance v7, Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->cvv:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->postal_code:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->card_number:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->raw_card_number_utf8:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/CardData$KeyedCard;-><init>(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 501
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->build()Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    move-result-object v0

    return-object v0
.end method

.method public card_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;
    .locals 0

    .line 540
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->card_number:Ljava/lang/String;

    const/4 p1, 0x0

    .line 541
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->raw_card_number_utf8:Lokio/ByteString;

    return-object p0
.end method

.method public cvv(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;
    .locals 0

    .line 524
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->cvv:Ljava/lang/String;

    return-object p0
.end method

.method public expiry(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;
    .locals 0

    .line 516
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    return-object p0
.end method

.method public postal_code(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;
    .locals 0

    .line 532
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->postal_code:Ljava/lang/String;

    return-object p0
.end method

.method public raw_card_number_utf8(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;
    .locals 0

    .line 549
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->raw_card_number_utf8:Lokio/ByteString;

    const/4 p1, 0x0

    .line 550
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->card_number:Ljava/lang/String;

    return-object p0
.end method
