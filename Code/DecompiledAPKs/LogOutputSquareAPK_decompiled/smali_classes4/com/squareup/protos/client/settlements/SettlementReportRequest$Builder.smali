.class public final Lcom/squareup/protos/client/settlements/SettlementReportRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SettlementReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettlementReportRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/settlements/SettlementReportRequest;",
        "Lcom/squareup/protos/client/settlements/SettlementReportRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public request_params:Lcom/squareup/protos/client/settlements/RequestParams;

.field public settlement_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 96
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/settlements/SettlementReportRequest;
    .locals 4

    .line 114
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportRequest$Builder;->request_params:Lcom/squareup/protos/client/settlements/RequestParams;

    iget-object v2, p0, Lcom/squareup/protos/client/settlements/SettlementReportRequest$Builder;->settlement_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/settlements/SettlementReportRequest;-><init>(Lcom/squareup/protos/client/settlements/RequestParams;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReportRequest$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportRequest;

    move-result-object v0

    return-object v0
.end method

.method public request_params(Lcom/squareup/protos/client/settlements/RequestParams;)Lcom/squareup/protos/client/settlements/SettlementReportRequest$Builder;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportRequest$Builder;->request_params:Lcom/squareup/protos/client/settlements/RequestParams;

    return-object p0
.end method

.method public settlement_token(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettlementReportRequest$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportRequest$Builder;->settlement_token:Ljava/lang/String;

    return-object p0
.end method
