.class public final Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;
.super Lcom/squareup/wire/Message;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Configuration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelectedOptions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ProtoAdapter_SelectedOptions;,
        Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;,
        Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.DiningOptionLineItem#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final discount:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.DiscountLineItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public final fee:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.FeeLineItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public final item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemVariation#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$Configuration$SelectedOptions$ItemVariationDetails#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final modifier_option:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.ModifierOptionLineItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 640
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ProtoAdapter_SelectedOptions;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ProtoAdapter_SelectedOptions;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/ItemVariation;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;Lcom/squareup/protos/client/bills/DiningOptionLineItem;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/ItemVariation;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;",
            "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;",
            "Lcom/squareup/protos/client/bills/DiningOptionLineItem;",
            ")V"
        }
    .end annotation

    .line 703
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;-><init>(Lcom/squareup/api/items/ItemVariation;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;Lcom/squareup/protos/client/bills/DiningOptionLineItem;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/ItemVariation;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;Lcom/squareup/protos/client/bills/DiningOptionLineItem;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/ItemVariation;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;",
            "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;",
            "Lcom/squareup/protos/client/bills/DiningOptionLineItem;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 710
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 711
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;

    const-string p1, "modifier_option"

    .line 712
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->modifier_option:Ljava/util/List;

    const-string p1, "discount"

    .line 713
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    const-string p1, "fee"

    .line 714
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->fee:Ljava/util/List;

    .line 715
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    .line 716
    iput-object p6, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 735
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 736
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    .line 737
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;

    .line 738
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->modifier_option:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->modifier_option:Ljava/util/List;

    .line 739
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    .line 740
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->fee:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->fee:Ljava/util/List;

    .line 741
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    .line 742
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    .line 743
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 748
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 750
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 751
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemVariation;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 752
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->modifier_option:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 753
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 754
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->fee:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 755
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 756
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/DiningOptionLineItem;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 757
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;
    .locals 2

    .line 721
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;-><init>()V

    .line 722
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;

    .line 723
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->modifier_option:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->modifier_option:Ljava/util/List;

    .line 724
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->discount:Ljava/util/List;

    .line 725
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->fee:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->fee:Ljava/util/List;

    .line 726
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    .line 727
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    .line 728
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 639
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 764
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 765
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;

    if-eqz v1, :cond_0

    const-string v1, ", item_variation_deprecated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 766
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->modifier_option:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", modifier_option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->modifier_option:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 767
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", discount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 768
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->fee:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", fee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->fee:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 769
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    if-eqz v1, :cond_4

    const-string v1, ", item_variation_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 770
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    if-eqz v1, :cond_5

    const-string v1, ", dining_option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SelectedOptions{"

    .line 771
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
