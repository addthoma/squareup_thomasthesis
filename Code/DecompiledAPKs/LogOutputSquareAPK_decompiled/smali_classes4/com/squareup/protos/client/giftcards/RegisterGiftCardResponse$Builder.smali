.class public final Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RegisterGiftCardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
        "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

.field public preset:Lcom/squareup/protos/client/giftcards/Preset;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 116
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;
    .locals 5

    .line 147
    new-instance v0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;->gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

    iget-object v3, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;->preset:Lcom/squareup/protos/client/giftcards/Preset;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/client/giftcards/Preset;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;->build()Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    move-result-object v0

    return-object v0
.end method

.method public gift_card(Lcom/squareup/protos/client/giftcards/GiftCard;)Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;->gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

    return-object p0
.end method

.method public preset(Lcom/squareup/protos/client/giftcards/Preset;)Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;->preset:Lcom/squareup/protos/client/giftcards/Preset;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
