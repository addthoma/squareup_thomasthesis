.class public final Lcom/squareup/protos/client/settlements/SettlementReport$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SettlementReport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettlementReport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/settlements/SettlementReport;",
        "Lcom/squareup/protos/client/settlements/SettlementReport$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bank_account_suffix:Ljava/lang/String;

.field public bank_name:Ljava/lang/String;

.field public card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public created_at:Lcom/squareup/protos/common/time/DateTime;

.field public deposit_fee_money:Lcom/squareup/protos/common/Money;

.field public pan_suffix:Ljava/lang/String;

.field public settled_bill_entry_token_group:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;",
            ">;"
        }
    .end annotation
.end field

.field public settlement_money:Lcom/squareup/protos/common/Money;

.field public settlement_type:Lcom/squareup/protos/client/settlements/SettlementType;

.field public success:Ljava/lang/Boolean;

.field public token:Ljava/lang/String;

.field public total_collected_money:Lcom/squareup/protos/common/Money;

.field public total_fee_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 301
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 302
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->settled_bill_entry_token_group:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bank_account_suffix(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettlementReport$Builder;
    .locals 0

    .line 341
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->bank_account_suffix:Ljava/lang/String;

    return-object p0
.end method

.method public bank_name(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettlementReport$Builder;
    .locals 0

    .line 349
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->bank_name:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/settlements/SettlementReport;
    .locals 17

    move-object/from16 v0, p0

    .line 414
    new-instance v16, Lcom/squareup/protos/client/settlements/SettlementReport;

    iget-object v2, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->token:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->settlement_type:Lcom/squareup/protos/client/settlements/SettlementType;

    iget-object v4, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v5, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->success:Ljava/lang/Boolean;

    iget-object v6, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->bank_account_suffix:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->bank_name:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->total_collected_money:Lcom/squareup/protos/common/Money;

    iget-object v9, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->total_fee_money:Lcom/squareup/protos/common/Money;

    iget-object v10, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->deposit_fee_money:Lcom/squareup/protos/common/Money;

    iget-object v11, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->settlement_money:Lcom/squareup/protos/common/Money;

    iget-object v12, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v13, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->pan_suffix:Ljava/lang/String;

    iget-object v14, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->settled_bill_entry_token_group:Ljava/util/List;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v15

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/protos/client/settlements/SettlementReport;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/settlements/SettlementType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v16
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 274
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReport;

    move-result-object v0

    return-object v0
.end method

.method public card_brand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/protos/client/settlements/SettlementReport$Builder;
    .locals 0

    .line 389
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0
.end method

.method public created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/settlements/SettlementReport$Builder;
    .locals 0

    .line 325
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public deposit_fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/settlements/SettlementReport$Builder;
    .locals 0

    .line 373
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->deposit_fee_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public pan_suffix(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettlementReport$Builder;
    .locals 0

    .line 397
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->pan_suffix:Ljava/lang/String;

    return-object p0
.end method

.method public settled_bill_entry_token_group(Ljava/util/List;)Lcom/squareup/protos/client/settlements/SettlementReport$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;",
            ">;)",
            "Lcom/squareup/protos/client/settlements/SettlementReport$Builder;"
        }
    .end annotation

    .line 407
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 408
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->settled_bill_entry_token_group:Ljava/util/List;

    return-object p0
.end method

.method public settlement_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/settlements/SettlementReport$Builder;
    .locals 0

    .line 381
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->settlement_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public settlement_type(Lcom/squareup/protos/client/settlements/SettlementType;)Lcom/squareup/protos/client/settlements/SettlementReport$Builder;
    .locals 0

    .line 317
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->settlement_type:Lcom/squareup/protos/client/settlements/SettlementType;

    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/settlements/SettlementReport$Builder;
    .locals 0

    .line 333
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettlementReport$Builder;
    .locals 0

    .line 309
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->token:Ljava/lang/String;

    return-object p0
.end method

.method public total_collected_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/settlements/SettlementReport$Builder;
    .locals 0

    .line 357
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->total_collected_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public total_fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/settlements/SettlementReport$Builder;
    .locals 0

    .line 365
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->total_fee_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
