.class public final Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpdateLoyaltyAccountMappingRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest;",
        "Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public data:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

.field public loyalty_account_mapping_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 107
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest;
    .locals 4

    .line 129
    new-instance v0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest$Builder;->loyalty_account_mapping_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest$Builder;->data:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest;

    move-result-object v0

    return-object v0
.end method

.method public data(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest$Builder;->data:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    return-object p0
.end method

.method public loyalty_account_mapping_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest$Builder;->loyalty_account_mapping_token:Ljava/lang/String;

    return-object p0
.end method
