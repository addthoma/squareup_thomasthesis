.class public final Lcom/squareup/protos/client/AppVersionRange$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AppVersionRange.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/AppVersionRange;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/AppVersionRange;",
        "Lcom/squareup/protos/client/AppVersionRange$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public since:Ljava/lang/String;

.field public until:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/AppVersionRange;
    .locals 4

    .line 126
    new-instance v0, Lcom/squareup/protos/client/AppVersionRange;

    iget-object v1, p0, Lcom/squareup/protos/client/AppVersionRange$Builder;->since:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/AppVersionRange$Builder;->until:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/AppVersionRange;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/client/AppVersionRange$Builder;->build()Lcom/squareup/protos/client/AppVersionRange;

    move-result-object v0

    return-object v0
.end method

.method public since(Ljava/lang/String;)Lcom/squareup/protos/client/AppVersionRange$Builder;
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/protos/client/AppVersionRange$Builder;->since:Ljava/lang/String;

    return-object p0
.end method

.method public until(Ljava/lang/String;)Lcom/squareup/protos/client/AppVersionRange$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/AppVersionRange$Builder;->until:Ljava/lang/String;

    return-object p0
.end method
