.class public final Lcom/squareup/protos/client/Signature$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Signature.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/Signature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/Signature;",
        "Lcom/squareup/protos/client/Signature$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public data:Lokio/ByteString;

.field public type:Lcom/squareup/protos/client/Signature$DataType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 101
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/Signature;
    .locals 4

    .line 120
    new-instance v0, Lcom/squareup/protos/client/Signature;

    iget-object v1, p0, Lcom/squareup/protos/client/Signature$Builder;->type:Lcom/squareup/protos/client/Signature$DataType;

    iget-object v2, p0, Lcom/squareup/protos/client/Signature$Builder;->data:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/Signature;-><init>(Lcom/squareup/protos/client/Signature$DataType;Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/Signature$Builder;->build()Lcom/squareup/protos/client/Signature;

    move-result-object v0

    return-object v0
.end method

.method public data(Lokio/ByteString;)Lcom/squareup/protos/client/Signature$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/client/Signature$Builder;->data:Lokio/ByteString;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/Signature$DataType;)Lcom/squareup/protos/client/Signature$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/client/Signature$Builder;->type:Lcom/squareup/protos/client/Signature$DataType;

    return-object p0
.end method
