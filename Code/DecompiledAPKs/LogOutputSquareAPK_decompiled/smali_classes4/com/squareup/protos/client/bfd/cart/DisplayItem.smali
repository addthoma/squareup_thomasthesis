.class public final Lcom/squareup/protos/client/bfd/cart/DisplayItem;
.super Lcom/squareup/wire/Message;
.source "DisplayItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bfd/cart/DisplayItem$ProtoAdapter_DisplayItem;,
        Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bfd/cart/DisplayItem;",
        "Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bfd/cart/DisplayItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CLIENT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_IS_DISCOUNT_ROW:Ljava/lang/Boolean;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PRICE:Ljava/lang/String; = ""

.field public static final DEFAULT_QUANTITY:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final client_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final discounts:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bfd.cart.Discount#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bfd/cart/Discount;",
            ">;"
        }
    .end annotation
.end field

.field public final is_discount_row:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final price:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final quantity:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x7
    .end annotation
.end field

.field public final taxes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bfd.cart.Tax#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bfd/cart/Tax;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 23
    new-instance v0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$ProtoAdapter_DisplayItem;

    invoke-direct {v0}, Lcom/squareup/protos/client/bfd/cart/DisplayItem$ProtoAdapter_DisplayItem;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 35
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->DEFAULT_IS_DISCOUNT_ROW:Ljava/lang/Boolean;

    .line 37
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->DEFAULT_QUANTITY:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bfd/cart/Tax;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bfd/cart/Discount;",
            ">;)V"
        }
    .end annotation

    .line 91
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/bfd/cart/DisplayItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bfd/cart/Tax;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bfd/cart/Discount;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 97
    sget-object v0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 98
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->name:Ljava/lang/String;

    .line 99
    iput-object p2, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->price:Ljava/lang/String;

    .line 100
    iput-object p3, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->description:Ljava/lang/String;

    .line 101
    iput-object p4, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->client_id:Ljava/lang/String;

    const-string p1, "taxes"

    .line 102
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->taxes:Ljava/util/List;

    .line 103
    iput-object p6, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->is_discount_row:Ljava/lang/Boolean;

    .line 104
    iput-object p7, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->quantity:Ljava/lang/Integer;

    const-string p1, "discounts"

    .line 105
    invoke-static {p1, p8}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->discounts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 126
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bfd/cart/DisplayItem;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 127
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bfd/cart/DisplayItem;

    .line 128
    invoke-virtual {p0}, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->name:Ljava/lang/String;

    .line 129
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->price:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->price:Ljava/lang/String;

    .line 130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->description:Ljava/lang/String;

    .line 131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->client_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->client_id:Ljava/lang/String;

    .line 132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->taxes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->taxes:Ljava/util/List;

    .line 133
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->is_discount_row:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->is_discount_row:Ljava/lang/Boolean;

    .line 134
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->quantity:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->quantity:Ljava/lang/Integer;

    .line 135
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->discounts:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->discounts:Ljava/util/List;

    .line 136
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 141
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 143
    invoke-virtual {p0}, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->price:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->description:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->client_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->is_discount_row:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->quantity:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 152
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;
    .locals 2

    .line 110
    new-instance v0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;-><init>()V

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->name:Ljava/lang/String;

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->price:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->price:Ljava/lang/String;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->description:Ljava/lang/String;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->client_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->client_id:Ljava/lang/String;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->taxes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->taxes:Ljava/util/List;

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->is_discount_row:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->is_discount_row:Ljava/lang/Boolean;

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->quantity:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->quantity:Ljava/lang/Integer;

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->discounts:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->discounts:Ljava/util/List;

    .line 119
    invoke-virtual {p0}, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->newBuilder()Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->price:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", price="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->price:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->description:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->client_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", client_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->client_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->taxes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 165
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->is_discount_row:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", is_discount_row="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->is_discount_row:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 166
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->quantity:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->quantity:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 167
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, ", discounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->discounts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DisplayItem{"

    .line 168
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
