.class public final Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PaymentRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/PaymentRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

.field public fixed_amount:Lcom/squareup/protos/common/Money;

.field public instrument_token:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

.field public percentage_amount:Ljava/lang/Long;

.field public relative_due_on:Ljava/lang/Long;

.field public reminders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
            ">;"
        }
    .end annotation
.end field

.field public tipping_enabled:Ljava/lang/Boolean;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 255
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 256
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->reminders:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public amount_type(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/invoice/PaymentRequest;
    .locals 13

    .line 345
    new-instance v12, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    iget-object v4, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->instrument_token:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    iget-object v6, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    iget-object v7, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount:Ljava/lang/Long;

    iget-object v8, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->tipping_enabled:Ljava/lang/Boolean;

    iget-object v9, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->name:Ljava/lang/String;

    iget-object v10, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->reminders:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/client/invoice/PaymentRequest;-><init>(Ljava/lang/String;Ljava/lang/Long;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/String;Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Lcom/squareup/protos/common/Money;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 234
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->build()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    return-object v0
.end method

.method public fixed_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;
    .locals 0

    .line 305
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public instrument_token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;
    .locals 0

    .line 288
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->instrument_token:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;
    .locals 0

    .line 330
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public payment_method(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;
    .locals 0

    .line 280
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    return-object p0
.end method

.method public percentage_amount(Ljava/lang/Long;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;
    .locals 0

    .line 314
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount:Ljava/lang/Long;

    return-object p0
.end method

.method public relative_due_on(Ljava/lang/Long;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;
    .locals 0

    .line 272
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on:Ljava/lang/Long;

    return-object p0
.end method

.method public reminders(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;"
        }
    .end annotation

    .line 338
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 339
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->reminders:Ljava/util/List;

    return-object p0
.end method

.method public tipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;
    .locals 0

    .line 322
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->tipping_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;
    .locals 0

    .line 264
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
