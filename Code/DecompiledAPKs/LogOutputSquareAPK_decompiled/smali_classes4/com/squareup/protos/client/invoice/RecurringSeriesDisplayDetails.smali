.class public final Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;
.super Lcom/squareup/wire/Message;
.source "RecurringSeriesDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$ProtoAdapter_RecurringSeriesDisplayDetails;,
        Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;,
        Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;",
        "Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DISPLAY_STATE:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

.field private static final serialVersionUID:J


# instance fields
.field public final created_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.RecurringSeriesDisplayDetails$DisplayState#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final ended_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.RecurringSeries#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final sort_date:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final start_date:Lcom/squareup/protos/common/time/YearMonthDay;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.YearMonthDay#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final updated_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$ProtoAdapter_RecurringSeriesDisplayDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$ProtoAdapter_RecurringSeriesDisplayDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 31
    sget-object v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->UNUSED:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    sput-object v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->DEFAULT_DISPLAY_STATE:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/invoice/RecurringSeries;Lcom/squareup/protos/common/time/YearMonthDay;)V
    .locals 9

    .line 90
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;-><init>(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/invoice/RecurringSeries;Lcom/squareup/protos/common/time/YearMonthDay;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/invoice/RecurringSeries;Lcom/squareup/protos/common/time/YearMonthDay;Lokio/ByteString;)V
    .locals 1

    .line 97
    sget-object v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 98
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    .line 99
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 100
    iput-object p3, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 101
    iput-object p4, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 102
    iput-object p5, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 103
    iput-object p6, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    .line 104
    iput-object p7, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 124
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 125
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 128
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 129
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    .line 132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 133
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 138
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 140
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/RecurringSeries;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/YearMonthDay;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 148
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;
    .locals 2

    .line 109
    new-instance v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;-><init>()V

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->newBuilder()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    if-eqz v1, :cond_0

    const-string v1, ", display_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 157
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    const-string v1, ", sort_date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 158
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 159
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_3

    const-string v1, ", updated_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 160
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    const-string v1, ", ended_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 161
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    if-eqz v1, :cond_5

    const-string v1, ", recurring_series_template="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 162
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_6

    const-string v1, ", start_date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RecurringSeriesDisplayDetails{"

    .line 163
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
