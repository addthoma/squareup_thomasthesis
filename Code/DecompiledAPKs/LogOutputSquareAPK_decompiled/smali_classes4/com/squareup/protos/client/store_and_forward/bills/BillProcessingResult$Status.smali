.class public final enum Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;
.super Ljava/lang/Enum;
.source "BillProcessingResult.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status$ProtoAdapter_Status;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum COMPLETED:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

.field public static final enum FAILED:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

.field public static final enum PROCESSING:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

.field public static final enum STATUS_UNKNOWN:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

.field public static final enum UNPROCESSED:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 241
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    const/4 v1, 0x0

    const-string v2, "STATUS_UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->STATUS_UNKNOWN:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    .line 243
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    const/4 v2, 0x1

    const-string v3, "UNPROCESSED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->UNPROCESSED:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    .line 245
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    const/4 v3, 0x2

    const-string v4, "PROCESSING"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->PROCESSING:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    .line 250
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    const/4 v4, 0x3

    const-string v5, "COMPLETED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->COMPLETED:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    .line 255
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    const/4 v5, 0x4

    const-string v6, "FAILED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->FAILED:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    .line 237
    sget-object v6, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->STATUS_UNKNOWN:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->UNPROCESSED:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->PROCESSING:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->COMPLETED:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->FAILED:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->$VALUES:[Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    .line 257
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status$ProtoAdapter_Status;

    invoke-direct {v0}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status$ProtoAdapter_Status;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 261
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 262
    iput p3, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 274
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->FAILED:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    return-object p0

    .line 273
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->COMPLETED:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    return-object p0

    .line 272
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->PROCESSING:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    return-object p0

    .line 271
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->UNPROCESSED:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    return-object p0

    .line 270
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->STATUS_UNKNOWN:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;
    .locals 1

    .line 237
    const-class v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;
    .locals 1

    .line 237
    sget-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->$VALUES:[Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 281
    iget v0, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->value:I

    return v0
.end method
