.class public final Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;
.super Lcom/squareup/wire/Message;
.source "CancelInvoiceRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$ProtoAdapter_CancelInvoiceRequest;,
        Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;",
        "Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SEND_EMAIL_TO_RECIPIENTS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final send_email_to_recipients:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$ProtoAdapter_CancelInvoiceRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$ProtoAdapter_CancelInvoiceRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x1

    .line 26
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->DEFAULT_SEND_EMAIL_TO_RECIPIENTS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Boolean;)V
    .locals 1

    .line 44
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 49
    sget-object v0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 50
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 51
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 66
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 67
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;

    .line 68
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 69
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    .line 70
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 75
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 79
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 80
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;
    .locals 2

    .line 56
    new-instance v0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;-><init>()V

    .line 57
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 58
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;->send_email_to_recipients:Ljava/lang/Boolean;

    .line 59
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->newBuilder()Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", send_email_to_recipients="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CancelInvoiceRequest{"

    .line 90
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
