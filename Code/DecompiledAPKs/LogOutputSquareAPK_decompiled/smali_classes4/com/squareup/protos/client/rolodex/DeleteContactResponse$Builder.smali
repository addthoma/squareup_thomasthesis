.class public final Lcom/squareup/protos/client/rolodex/DeleteContactResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeleteContactResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/DeleteContactResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/DeleteContactResponse;",
        "Lcom/squareup/protos/client/rolodex/DeleteContactResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 81
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/DeleteContactResponse;
    .locals 3

    .line 94
    new-instance v0, Lcom/squareup/protos/client/rolodex/DeleteContactResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteContactResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/rolodex/DeleteContactResponse;-><init>(Lcom/squareup/protos/client/Status;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/DeleteContactResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/DeleteContactResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/DeleteContactResponse$Builder;
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/DeleteContactResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
