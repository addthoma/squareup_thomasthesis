.class public final enum Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;
.super Ljava/lang/Enum;
.source "ListCardsResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CardState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState$ProtoAdapter_CardState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final enum ACTIVE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final enum ACTIVE_PENDING_2FA:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CANCELLED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final enum CUSTOMIZATION_APPROVED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final enum CUSTOMIZATION_DENIED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final enum DESTROYED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final enum DISABLED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final enum ISSUED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final enum REJECTED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final enum REVIEW_PENDING:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final enum SHIPPED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final enum SUSPENDED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final enum TERMINATED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final enum UNDELIVERABLE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final enum VERIFIED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 276
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->UNKNOWN:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 281
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v2, 0x1

    const-string v3, "ACTIVE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 283
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v3, 0x2

    const-string v4, "DISABLED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->DISABLED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 285
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v4, 0x3

    const-string v5, "TERMINATED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->TERMINATED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 287
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v5, 0x4

    const-string v6, "SUSPENDED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->SUSPENDED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 289
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v6, 0x5

    const-string v7, "VERIFIED"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->VERIFIED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 291
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v7, 0x6

    const-string v8, "ISSUED"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ISSUED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 293
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v8, 0x7

    const-string v9, "UNDELIVERABLE"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->UNDELIVERABLE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 295
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/16 v9, 0x8

    const-string v10, "SHIPPED"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->SHIPPED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 297
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/16 v10, 0x9

    const-string v11, "CANCELLED"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->CANCELLED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 299
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/16 v11, 0xa

    const-string v12, "REJECTED"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->REJECTED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 301
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/16 v12, 0xb

    const-string v13, "DESTROYED"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->DESTROYED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 306
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/16 v13, 0xc

    const-string v14, "ACTIVE_PENDING_2FA"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE_PENDING_2FA:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 308
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/16 v14, 0xd

    const-string v15, "REVIEW_PENDING"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->REVIEW_PENDING:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 310
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/16 v15, 0xe

    const-string v14, "CUSTOMIZATION_APPROVED"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->CUSTOMIZATION_APPROVED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 312
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const-string v14, "CUSTOMIZATION_DENIED"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->CUSTOMIZATION_DENIED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 275
    sget-object v13, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->UNKNOWN:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->DISABLED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->TERMINATED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->SUSPENDED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->VERIFIED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ISSUED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->UNDELIVERABLE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->SHIPPED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->CANCELLED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->REJECTED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->DESTROYED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE_PENDING_2FA:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->REVIEW_PENDING:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->CUSTOMIZATION_APPROVED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->CUSTOMIZATION_DENIED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->$VALUES:[Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 314
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState$ProtoAdapter_CardState;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState$ProtoAdapter_CardState;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 318
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 319
    iput p3, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 342
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->CUSTOMIZATION_DENIED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0

    .line 341
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->CUSTOMIZATION_APPROVED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0

    .line 340
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->REVIEW_PENDING:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0

    .line 339
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE_PENDING_2FA:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0

    .line 338
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->DESTROYED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0

    .line 337
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->REJECTED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0

    .line 336
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->CANCELLED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0

    .line 335
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->SHIPPED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0

    .line 334
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->UNDELIVERABLE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0

    .line 333
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ISSUED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0

    .line 332
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->VERIFIED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0

    .line 331
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->SUSPENDED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0

    .line 330
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->TERMINATED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0

    .line 329
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->DISABLED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0

    .line 328
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0

    .line 327
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->UNKNOWN:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;
    .locals 1

    .line 275
    const-class v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;
    .locals 1

    .line 275
    sget-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->$VALUES:[Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 349
    iget v0, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->value:I

    return v0
.end method
