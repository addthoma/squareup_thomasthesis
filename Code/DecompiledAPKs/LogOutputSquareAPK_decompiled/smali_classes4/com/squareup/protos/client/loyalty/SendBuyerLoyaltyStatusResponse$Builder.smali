.class public final Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SendBuyerLoyaltyStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse;",
        "Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public response_code:Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse$SendBuyerLoyaltyStatusResponseCode;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse;
    .locals 4

    .line 112
    new-instance v0, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse$Builder;->response_code:Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse$SendBuyerLoyaltyStatusResponseCode;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse$SendBuyerLoyaltyStatusResponseCode;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public response_code(Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse$SendBuyerLoyaltyStatusResponseCode;)Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse$Builder;->response_code:Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse$SendBuyerLoyaltyStatusResponseCode;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
