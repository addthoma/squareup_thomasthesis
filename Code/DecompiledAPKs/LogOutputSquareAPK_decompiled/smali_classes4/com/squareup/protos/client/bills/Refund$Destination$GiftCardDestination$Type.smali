.class public final enum Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;
.super Ljava/lang/Enum;
.source "Refund.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DIGITAL:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

.field public static final enum PHYSICAL:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

.field public static final enum TYPE_UNKNOWN:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 795
    new-instance v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    const/4 v1, 0x0

    const-string v2, "TYPE_UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->TYPE_UNKNOWN:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    .line 800
    new-instance v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    const/4 v2, 0x1

    const-string v3, "PHYSICAL"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->PHYSICAL:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    .line 805
    new-instance v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    const/4 v3, 0x2

    const-string v4, "DIGITAL"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->DIGITAL:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    .line 794
    sget-object v4, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->TYPE_UNKNOWN:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->PHYSICAL:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->DIGITAL:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->$VALUES:[Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    .line 807
    new-instance v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 811
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 812
    iput p3, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 822
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->DIGITAL:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    return-object p0

    .line 821
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->PHYSICAL:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    return-object p0

    .line 820
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->TYPE_UNKNOWN:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;
    .locals 1

    .line 794
    const-class v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;
    .locals 1

    .line 794
    sget-object v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->$VALUES:[Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 829
    iget v0, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->value:I

    return v0
.end method
