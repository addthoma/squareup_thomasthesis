.class public final Lcom/squareup/protos/client/rolodex/GetMerchantResponse;
.super Lcom/squareup/wire/Message;
.source "GetMerchantResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/GetMerchantResponse$ProtoAdapter_GetMerchantResponse;,
        Lcom/squareup/protos/client/rolodex/GetMerchantResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/GetMerchantResponse;",
        "Lcom/squareup/protos/client/rolodex/GetMerchantResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/GetMerchantResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final merchant:Lcom/squareup/protos/client/rolodex/Merchant;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Merchant#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse$ProtoAdapter_GetMerchantResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/GetMerchantResponse$ProtoAdapter_GetMerchantResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/rolodex/Merchant;)V
    .locals 1

    .line 38
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/rolodex/Merchant;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/rolodex/Merchant;Lokio/ByteString;)V
    .locals 1

    .line 42
    sget-object v0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 43
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->status:Lcom/squareup/protos/client/Status;

    .line 44
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->merchant:Lcom/squareup/protos/client/rolodex/Merchant;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 59
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 60
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;

    .line 61
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->status:Lcom/squareup/protos/client/Status;

    .line 62
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->merchant:Lcom/squareup/protos/client/rolodex/Merchant;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->merchant:Lcom/squareup/protos/client/rolodex/Merchant;

    .line 63
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 68
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 70
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 71
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->merchant:Lcom/squareup/protos/client/rolodex/Merchant;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Merchant;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 73
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/GetMerchantResponse$Builder;
    .locals 2

    .line 49
    new-instance v0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/GetMerchantResponse$Builder;-><init>()V

    .line 50
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 51
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->merchant:Lcom/squareup/protos/client/rolodex/Merchant;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse$Builder;->merchant:Lcom/squareup/protos/client/rolodex/Merchant;

    .line 52
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/GetMerchantResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->newBuilder()Lcom/squareup/protos/client/rolodex/GetMerchantResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 82
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->merchant:Lcom/squareup/protos/client/rolodex/Merchant;

    if-eqz v1, :cond_1

    const-string v1, ", merchant="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->merchant:Lcom/squareup/protos/client/rolodex/Merchant;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetMerchantResponse{"

    .line 83
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
