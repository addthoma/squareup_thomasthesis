.class public final Lcom/squareup/protos/client/rolodex/ListOption$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListOption.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ListOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ListOption;",
        "Lcom/squareup/protos/client/rolodex/ListOption$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public event_types:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/EventType;",
            ">;"
        }
    .end annotation
.end field

.field public limit:Ljava/lang/Integer;

.field public paging_key:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 120
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 121
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/ListOption$Builder;->event_types:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ListOption;
    .locals 5

    .line 148
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListOption;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListOption$Builder;->paging_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/ListOption$Builder;->limit:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/ListOption$Builder;->event_types:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/rolodex/ListOption;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListOption$Builder;->build()Lcom/squareup/protos/client/rolodex/ListOption;

    move-result-object v0

    return-object v0
.end method

.method public event_types(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/ListOption$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/EventType;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/ListOption$Builder;"
        }
    .end annotation

    .line 141
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListOption$Builder;->event_types:Ljava/util/List;

    return-object p0
.end method

.method public limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/ListOption$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListOption$Builder;->limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/ListOption$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListOption$Builder;->paging_key:Ljava/lang/String;

    return-object p0
.end method
