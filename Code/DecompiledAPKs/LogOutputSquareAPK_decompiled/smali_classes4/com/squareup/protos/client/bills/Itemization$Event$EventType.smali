.class public final enum Lcom/squareup/protos/client/bills/Itemization$Event$EventType;
.super Ljava/lang/Enum;
.source "Itemization.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EventType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Itemization$Event$EventType$ProtoAdapter_EventType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/Itemization$Event$EventType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Itemization$Event$EventType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum COMP:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

.field public static final enum CREATION:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

.field public static final enum DELETE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

.field public static final enum DISCOUNT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

.field public static final enum FIRE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

.field public static final enum KDS_COMPLETE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

.field public static final enum KDS_UNCOMPLETE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

.field public static final enum REMOVE_DISCOUNT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

.field public static final enum SEND:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

.field public static final enum SPLIT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

.field public static final enum UNCOMP:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

.field public static final enum VOID:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 3320
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->UNKNOWN:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3322
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    const/4 v2, 0x1

    const-string v3, "CREATION"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->CREATION:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3333
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    const/4 v3, 0x2

    const-string v4, "COMP"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->COMP:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3342
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    const/4 v4, 0x3

    const-string v5, "VOID"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->VOID:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3348
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    const/4 v5, 0x4

    const-string v6, "UNCOMP"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->UNCOMP:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3356
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    const/4 v6, 0x5

    const-string v7, "DELETE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->DELETE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3369
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    const/4 v7, 0x6

    const-string v8, "SPLIT"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->SPLIT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3380
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    const/4 v8, 0x7

    const-string v9, "FIRE"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->FIRE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3387
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    const/16 v9, 0x8

    const-string v10, "KDS_COMPLETE"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->KDS_COMPLETE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3394
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    const/16 v10, 0x9

    const-string v11, "KDS_UNCOMPLETE"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->KDS_UNCOMPLETE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3400
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    const/16 v11, 0xa

    const-string v12, "SEND"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->SEND:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3405
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    const/16 v12, 0xb

    const-string v13, "DISCOUNT"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->DISCOUNT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3410
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    const/16 v13, 0xc

    const-string v14, "REMOVE_DISCOUNT"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->REMOVE_DISCOUNT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    const/16 v0, 0xd

    new-array v0, v0, [Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3319
    sget-object v14, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->UNKNOWN:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    aput-object v14, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->CREATION:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->COMP:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->VOID:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->UNCOMP:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->DELETE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->SPLIT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->FIRE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->KDS_COMPLETE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->KDS_UNCOMPLETE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->SEND:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->DISCOUNT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->REMOVE_DISCOUNT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    aput-object v1, v0, v13

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->$VALUES:[Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3412
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType$ProtoAdapter_EventType;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Event$EventType$ProtoAdapter_EventType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 3416
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3417
    iput p3, p0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/Itemization$Event$EventType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 3437
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->REMOVE_DISCOUNT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-object p0

    .line 3436
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->DISCOUNT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-object p0

    .line 3435
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->SEND:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-object p0

    .line 3434
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->KDS_UNCOMPLETE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-object p0

    .line 3433
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->KDS_COMPLETE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-object p0

    .line 3432
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->FIRE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-object p0

    .line 3431
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->SPLIT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-object p0

    .line 3430
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->DELETE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-object p0

    .line 3429
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->UNCOMP:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-object p0

    .line 3428
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->VOID:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-object p0

    .line 3427
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->COMP:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-object p0

    .line 3426
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->CREATION:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-object p0

    .line 3425
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->UNKNOWN:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event$EventType;
    .locals 1

    .line 3319
    const-class v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/Itemization$Event$EventType;
    .locals 1

    .line 3319
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->$VALUES:[Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 3444
    iget v0, p0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->value:I

    return v0
.end method
