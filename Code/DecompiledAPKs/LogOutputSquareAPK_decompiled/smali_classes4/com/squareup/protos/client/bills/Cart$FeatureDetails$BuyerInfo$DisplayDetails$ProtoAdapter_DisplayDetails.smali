.class final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$ProtoAdapter_DisplayDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_DisplayDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 3024
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3051
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;-><init>()V

    .line 3052
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 3053
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 3062
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 3060
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->loyalty_account_phone_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;

    goto :goto_0

    .line 3059
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->phone_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;

    goto :goto_0

    .line 3058
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;

    goto :goto_0

    .line 3057
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->email(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;

    goto :goto_0

    .line 3056
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->last_visit(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;

    goto :goto_0

    .line 3055
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->display_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;

    goto :goto_0

    .line 3066
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 3067
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3022
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$ProtoAdapter_DisplayDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3040
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->display_name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3041
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->last_visit:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3042
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->email:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3043
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_number:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3044
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_token:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3045
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->loyalty_account_phone_token:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3046
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3022
    check-cast p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$ProtoAdapter_DisplayDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;)I
    .locals 4

    .line 3029
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->display_name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->last_visit:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x2

    .line 3030
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->email:Ljava/lang/String;

    const/4 v3, 0x3

    .line 3031
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_number:Ljava/lang/String;

    const/4 v3, 0x4

    .line 3032
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_token:Ljava/lang/String;

    const/4 v3, 0x5

    .line 3033
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->loyalty_account_phone_token:Ljava/lang/String;

    const/4 v3, 0x6

    .line 3034
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3035
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 3022
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$ProtoAdapter_DisplayDetails;->encodedSize(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;
    .locals 3

    .line 3072
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 3073
    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->display_name:Ljava/lang/String;

    .line 3074
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->last_visit:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->last_visit:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->last_visit:Lcom/squareup/protos/client/ISO8601Date;

    .line 3075
    :cond_0
    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->email:Ljava/lang/String;

    .line 3076
    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->phone_number:Ljava/lang/String;

    .line 3077
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 3078
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 3022
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$ProtoAdapter_DisplayDetails;->redact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    move-result-object p1

    return-object p1
.end method
