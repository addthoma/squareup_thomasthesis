.class public final Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;
.super Lcom/squareup/wire/Message;
.source "GetResidualBillResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetResidualBillResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ResidualItemization"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$ProtoAdapter_ResidualItemization;,
        Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SOURCE_BILL_SERVER_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_SOURCE_ITEMIZATION_QUANTITY:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final itemization:Lcom/squareup/protos/client/bills/Itemization;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final source_bill_server_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final source_itemization_quantity:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 423
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$ProtoAdapter_ResidualItemization;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$ProtoAdapter_ResidualItemization;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization;)V
    .locals 6

    .line 486
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization;Lokio/ByteString;)V
    .locals 1

    .line 492
    sget-object v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 493
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_bill_server_token:Ljava/lang/String;

    .line 494
    iput-object p2, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    .line 495
    iput-object p3, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_quantity:Ljava/lang/String;

    .line 496
    iput-object p4, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 513
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 514
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;

    .line 515
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_bill_server_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_bill_server_token:Ljava/lang/String;

    .line 516
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    .line 517
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_quantity:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_quantity:Ljava/lang/String;

    .line 518
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    .line 519
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 524
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 526
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 527
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_bill_server_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 528
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 529
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_quantity:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 530
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 531
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;
    .locals 2

    .line 501
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;-><init>()V

    .line 502
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_bill_server_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;->source_bill_server_token:Ljava/lang/String;

    .line 503
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    .line 504
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_quantity:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;->source_itemization_quantity:Ljava/lang/String;

    .line 505
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    .line 506
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 422
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->newBuilder()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 538
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 539
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_bill_server_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", source_bill_server_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_bill_server_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 540
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_1

    const-string v1, ", source_itemization_token_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 541
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_quantity:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", source_itemization_quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_quantity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 542
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    if-eqz v1, :cond_3

    const-string v1, ", itemization="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ResidualItemization{"

    .line 543
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
