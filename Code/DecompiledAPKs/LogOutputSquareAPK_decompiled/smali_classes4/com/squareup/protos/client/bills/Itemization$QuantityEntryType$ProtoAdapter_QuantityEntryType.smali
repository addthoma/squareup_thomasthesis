.class final Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType$ProtoAdapter_QuantityEntryType;
.super Lcom/squareup/wire/EnumAdapter;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_QuantityEntryType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 481
    const-class v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;
    .locals 0

    .line 486
    invoke-static {p1}, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->fromValue(I)Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 479
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType$ProtoAdapter_QuantityEntryType;->fromValue(I)Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    move-result-object p1

    return-object p1
.end method
