.class public final Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;
.super Lcom/squareup/wire/Message;
.source "ListTendersAwaitingMerchantTipResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$ProtoAdapter_ListTendersAwaitingMerchantTipResponse;,
        Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;",
        "Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ERROR_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_ERROR_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_NEXT_REQUEST_BACKOFF_SECONDS:Ljava/lang/Integer;

.field public static final DEFAULT_SUCCESS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final error_message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final error_title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final next_request_backoff_seconds:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x5
    .end annotation
.end field

.field public final success:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final tender_awaiting_merchant_tip_list:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.paper_signature.PaperSignatureTender#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final tender_with_bill_id:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.paper_signature.TenderWithBillId#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/TenderWithBillId;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 28
    new-instance v0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$ProtoAdapter_ListTendersAwaitingMerchantTipResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$ProtoAdapter_ListTendersAwaitingMerchantTipResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 32
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    .line 38
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->DEFAULT_NEXT_REQUEST_BACKOFF_SECONDS:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/TenderWithBillId;",
            ">;)V"
        }
    .end annotation

    .line 113
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/TenderWithBillId;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 120
    sget-object v0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    .line 122
    iput-object p2, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    .line 123
    iput-object p3, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    const-string p1, "tender_awaiting_merchant_tip_list"

    .line 124
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    .line 125
    iput-object p5, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    const-string p1, "tender_with_bill_id"

    .line 126
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_with_bill_id:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 145
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 146
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;

    .line 147
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    .line 148
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    .line 149
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    .line 150
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    .line 151
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    .line 152
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_with_bill_id:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_with_bill_id:Ljava/util/List;

    .line 153
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 158
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 160
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 165
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 166
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_with_bill_id:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;
    .locals 2

    .line 131
    new-instance v0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;-><init>()V

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->success:Ljava/lang/Boolean;

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->error_message:Ljava/lang/String;

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->error_title:Ljava/lang/String;

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->next_request_backoff_seconds:Ljava/lang/Integer;

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_with_bill_id:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->tender_with_bill_id:Ljava/util/List;

    .line 138
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->newBuilder()Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 176
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", error_message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", error_title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", tender_awaiting_merchant_tip_list="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 179
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", next_request_backoff_seconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 180
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_with_bill_id:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", tender_with_bill_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_with_bill_id:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ListTendersAwaitingMerchantTipResponse{"

    .line 181
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
