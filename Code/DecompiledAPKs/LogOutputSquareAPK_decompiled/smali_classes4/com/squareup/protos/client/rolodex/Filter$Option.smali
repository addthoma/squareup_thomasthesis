.class public final Lcom/squareup/protos/client/rolodex/Filter$Option;
.super Lcom/squareup/wire/Message;
.source "Filter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Option"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/Filter$Option$ProtoAdapter_Option;,
        Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/Filter$Option;",
        "Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LABEL:Ljava/lang/String; = ""

.field public static final DEFAULT_VALUE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final label:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1245
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Option$ProtoAdapter_Option;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Filter$Option$ProtoAdapter_Option;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1272
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/rolodex/Filter$Option;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 1276
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1277
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    .line 1278
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1293
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1294
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 1295
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Filter$Option;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Filter$Option;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    .line 1296
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    .line 1297
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1302
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 1304
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Filter$Option;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1305
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1306
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 1307
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;
    .locals 2

    .line 1283
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;-><init>()V

    .line 1284
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;->value:Ljava/lang/String;

    .line 1285
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;->label:Ljava/lang/String;

    .line 1286
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Filter$Option;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1244
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Filter$Option;->newBuilder()Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1314
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1315
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1316
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Option{"

    .line 1317
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
