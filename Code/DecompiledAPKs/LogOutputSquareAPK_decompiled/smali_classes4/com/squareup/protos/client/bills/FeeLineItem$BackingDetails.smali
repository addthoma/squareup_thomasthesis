.class public final Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;
.super Lcom/squareup/wire/Message;
.source "FeeLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/FeeLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BackingDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$ProtoAdapter_BackingDetails;,
        Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;,
        Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;",
        "Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BACKING_TYPE:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;

.field private static final serialVersionUID:J


# instance fields
.field public final backing_type:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.FeeLineItem$BackingDetails$BackingType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final fee:Lcom/squareup/api/items/Fee;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Fee#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 250
    new-instance v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$ProtoAdapter_BackingDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$ProtoAdapter_BackingDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 254
    sget-object v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;->ITEMS_SERVICE_FEE:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;

    sput-object v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->DEFAULT_BACKING_TYPE:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/Fee;Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;)V
    .locals 1

    .line 276
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;-><init>(Lcom/squareup/api/items/Fee;Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/Fee;Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;Lokio/ByteString;)V
    .locals 1

    .line 280
    sget-object v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 281
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    .line 282
    iput-object p2, p0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 297
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 298
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    .line 299
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    .line 300
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;

    .line 301
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 306
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 308
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 309
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/api/items/Fee;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 310
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 311
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;
    .locals 2

    .line 287
    new-instance v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;-><init>()V

    .line 288
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;->fee:Lcom/squareup/api/items/Fee;

    .line 289
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;->backing_type:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;

    .line 290
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 249
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->newBuilder()Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 318
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 319
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    if-eqz v1, :cond_0

    const-string v1, ", fee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 320
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;

    if-eqz v1, :cond_1

    const-string v1, ", backing_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BackingDetails{"

    .line 321
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
