.class final Lcom/squareup/protos/client/invoice/PutInvoiceResponse$ProtoAdapter_PutInvoiceResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "PutInvoiceResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/PutInvoiceResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PutInvoiceResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/invoice/PutInvoiceResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 113
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/invoice/PutInvoiceResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/PutInvoiceResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 132
    new-instance v0, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;-><init>()V

    .line 133
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 134
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 139
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 137
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;->invoice(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;

    goto :goto_0

    .line 136
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;

    goto :goto_0

    .line 143
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 144
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;->build()Lcom/squareup/protos/client/invoice/PutInvoiceResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 111
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$ProtoAdapter_PutInvoiceResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/PutInvoiceResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/PutInvoiceResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 125
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/PutInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 126
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/PutInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 127
    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/PutInvoiceResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 111
    check-cast p2, Lcom/squareup/protos/client/invoice/PutInvoiceResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$ProtoAdapter_PutInvoiceResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/PutInvoiceResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/invoice/PutInvoiceResponse;)I
    .locals 4

    .line 118
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/PutInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/PutInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    const/4 v3, 0x2

    .line 119
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 120
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/PutInvoiceResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 111
    check-cast p1, Lcom/squareup/protos/client/invoice/PutInvoiceResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$ProtoAdapter_PutInvoiceResponse;->encodedSize(Lcom/squareup/protos/client/invoice/PutInvoiceResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/invoice/PutInvoiceResponse;)Lcom/squareup/protos/client/invoice/PutInvoiceResponse;
    .locals 2

    .line 149
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/PutInvoiceResponse;->newBuilder()Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;

    move-result-object p1

    .line 150
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Status;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 151
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    .line 152
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 153
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$Builder;->build()Lcom/squareup/protos/client/invoice/PutInvoiceResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 111
    check-cast p1, Lcom/squareup/protos/client/invoice/PutInvoiceResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/PutInvoiceResponse$ProtoAdapter_PutInvoiceResponse;->redact(Lcom/squareup/protos/client/invoice/PutInvoiceResponse;)Lcom/squareup/protos/client/invoice/PutInvoiceResponse;

    move-result-object p1

    return-object p1
.end method
