.class public final Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBalanceSummaryRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public include_recent_activity:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 85
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;
    .locals 3

    .line 100
    new-instance v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$Builder;->include_recent_activity:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$Builder;->build()Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;

    move-result-object v0

    return-object v0
.end method

.method public include_recent_activity(Ljava/lang/Boolean;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$Builder;
    .locals 0

    .line 94
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$Builder;->include_recent_activity:Ljava/lang/Boolean;

    return-object p0
.end method
