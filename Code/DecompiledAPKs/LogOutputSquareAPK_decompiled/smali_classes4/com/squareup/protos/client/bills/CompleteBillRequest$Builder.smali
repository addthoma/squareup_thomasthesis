.class public final Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CompleteBillRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CompleteBillRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CompleteBillRequest;",
        "Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amendment_token:Ljava/lang/String;

.field public bill_id_pair:Lcom/squareup/protos/client/IdPair;

.field public cart:Lcom/squareup/protos/client/bills/Cart;

.field public complete_bill_type:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

.field public dates:Lcom/squareup/protos/client/bills/Bill$Dates;

.field public is_amendable:Ljava/lang/Boolean;

.field public merchant:Lcom/squareup/protos/client/Merchant;

.field public removed_amending_tender_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;"
        }
    .end annotation
.end field

.field public square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

.field public tender:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/CompleteTender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 251
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 252
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->tender:Ljava/util/List;

    .line 253
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->removed_amending_tender_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public amendment_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;
    .locals 0

    .line 320
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->amendment_token:Ljava/lang/String;

    return-object p0
.end method

.method public bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;
    .locals 0

    .line 257
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/CompleteBillRequest;
    .locals 13

    .line 347
    new-instance v12, Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->tender:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->complete_bill_type:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iget-object v8, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->amendment_token:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->is_amendable:Ljava/lang/Boolean;

    iget-object v10, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->removed_amending_tender_ids:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/client/bills/CompleteBillRequest;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/bills/Bill$Dates;Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;Lcom/squareup/protos/client/bills/SquareProductAttributes;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 230
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->build()Lcom/squareup/protos/client/bills/CompleteBillRequest;

    move-result-object v0

    return-object v0
.end method

.method public cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;
    .locals 0

    .line 285
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object p0
.end method

.method public complete_bill_type(Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;
    .locals 0

    .line 298
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->complete_bill_type:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    return-object p0
.end method

.method public dates(Lcom/squareup/protos/client/bills/Bill$Dates;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;
    .locals 0

    .line 290
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    return-object p0
.end method

.method public is_amendable(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;
    .locals 0

    .line 329
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->is_amendable:Ljava/lang/Boolean;

    return-object p0
.end method

.method public merchant(Lcom/squareup/protos/client/Merchant;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;
    .locals 0

    .line 265
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    return-object p0
.end method

.method public removed_amending_tender_ids(Ljava/util/List;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;)",
            "Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;"
        }
    .end annotation

    .line 340
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 341
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->removed_amending_tender_ids:Ljava/util/List;

    return-object p0
.end method

.method public square_product_attributes(Lcom/squareup/protos/client/bills/SquareProductAttributes;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;
    .locals 0

    .line 308
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    return-object p0
.end method

.method public tender(Ljava/util/List;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/CompleteTender;",
            ">;)",
            "Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;"
        }
    .end annotation

    .line 274
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 275
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->tender:Ljava/util/List;

    return-object p0
.end method
