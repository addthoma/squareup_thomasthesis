.class public final Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;
.super Lcom/squareup/wire/Message;
.source "ReportCoredumpRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$ProtoAdapter_ReportCoredumpRequest;,
        Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;",
        "Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CHIP_TYPE:Lcom/squareup/protos/client/tarkin/ChipType;

.field public static final DEFAULT_COREDUMP_DATA:Lokio/ByteString;

.field public static final DEFAULT_COREDUMP_KEY:Lokio/ByteString;

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_READER_SERIAL_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_READER_TYPE:Lcom/squareup/protos/client/tarkin/ReaderType;

.field public static final DEFAULT_USER_AGENT:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final chip_type:Lcom/squareup/protos/client/tarkin/ChipType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tarkin.ChipType#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final coredump_data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x2
    .end annotation
.end field

.field public final coredump_key:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x1
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final reader_serial_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tarkin.ReaderType#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final user_agent:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$ProtoAdapter_ReportCoredumpRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$ProtoAdapter_ReportCoredumpRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 24
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->DEFAULT_COREDUMP_KEY:Lokio/ByteString;

    .line 26
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->DEFAULT_COREDUMP_DATA:Lokio/ByteString;

    .line 28
    sget-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->UNKNOWN:Lcom/squareup/protos/client/tarkin/ReaderType;

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->DEFAULT_READER_TYPE:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 34
    sget-object v0, Lcom/squareup/protos/client/tarkin/ChipType;->UNKNOWN_CHIP:Lcom/squareup/protos/client/tarkin/ChipType;

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->DEFAULT_CHIP_TYPE:Lcom/squareup/protos/client/tarkin/ChipType;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lokio/ByteString;Lcom/squareup/protos/client/tarkin/ReaderType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/tarkin/ChipType;Ljava/lang/String;)V
    .locals 9

    .line 93
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;-><init>(Lokio/ByteString;Lokio/ByteString;Lcom/squareup/protos/client/tarkin/ReaderType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/tarkin/ChipType;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lokio/ByteString;Lcom/squareup/protos/client/tarkin/ReaderType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/tarkin/ChipType;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 99
    sget-object v0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_key:Lokio/ByteString;

    .line 101
    iput-object p2, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_data:Lokio/ByteString;

    .line 102
    iput-object p3, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 103
    iput-object p4, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_serial_number:Ljava/lang/String;

    .line 104
    iput-object p5, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->merchant_token:Ljava/lang/String;

    .line 105
    iput-object p6, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->chip_type:Lcom/squareup/protos/client/tarkin/ChipType;

    .line 106
    iput-object p7, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->user_agent:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 126
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 127
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;

    .line 128
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_key:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_key:Lokio/ByteString;

    .line 129
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_data:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_data:Lokio/ByteString;

    .line 130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_serial_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_serial_number:Ljava/lang/String;

    .line 132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->merchant_token:Ljava/lang/String;

    .line 133
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->chip_type:Lcom/squareup/protos/client/tarkin/ChipType;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->chip_type:Lcom/squareup/protos/client/tarkin/ChipType;

    .line 134
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->user_agent:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->user_agent:Ljava/lang/String;

    .line 135
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 140
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 142
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_key:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_data:Lokio/ByteString;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/tarkin/ReaderType;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_serial_number:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->chip_type:Lcom/squareup/protos/client/tarkin/ChipType;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/tarkin/ChipType;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->user_agent:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 150
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;
    .locals 2

    .line 111
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;-><init>()V

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_key:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->coredump_key:Lokio/ByteString;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->coredump_data:Lokio/ByteString;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_serial_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->reader_serial_number:Ljava/lang/String;

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->merchant_token:Ljava/lang/String;

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->chip_type:Lcom/squareup/protos/client/tarkin/ChipType;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->chip_type:Lcom/squareup/protos/client/tarkin/ChipType;

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->user_agent:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->user_agent:Ljava/lang/String;

    .line 119
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->newBuilder()Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_key:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", coredump_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_key:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 159
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_data:Lokio/ByteString;

    if-eqz v1, :cond_1

    const-string v1, ", coredump_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->coredump_data:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 160
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    if-eqz v1, :cond_2

    const-string v1, ", reader_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 161
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_serial_number:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", reader_serial_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->reader_serial_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->chip_type:Lcom/squareup/protos/client/tarkin/ChipType;

    if-eqz v1, :cond_5

    const-string v1, ", chip_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->chip_type:Lcom/squareup/protos/client/tarkin/ChipType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 164
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->user_agent:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", user_agent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;->user_agent:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReportCoredumpRequest{"

    .line 165
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
