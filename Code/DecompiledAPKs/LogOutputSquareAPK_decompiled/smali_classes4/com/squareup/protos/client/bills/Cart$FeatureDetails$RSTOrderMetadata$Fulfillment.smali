.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Fulfillment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$ProtoAdapter_Fulfillment;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FULFILLMENT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_FULFILLMENT_TYPE_DISPLAY_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final fulfillment_entry:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;",
            ">;"
        }
    .end annotation
.end field

.field public final fulfillment_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final fulfillment_type_display_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5710
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$ProtoAdapter_Fulfillment;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$ProtoAdapter_Fulfillment;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;",
            ">;)V"
        }
    .end annotation

    .line 5747
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 5752
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 5753
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_id:Ljava/lang/String;

    .line 5754
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_type_display_name:Ljava/lang/String;

    const-string p1, "fulfillment_entry"

    .line 5755
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_entry:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 5771
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 5772
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;

    .line 5773
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_id:Ljava/lang/String;

    .line 5774
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_type_display_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_type_display_name:Ljava/lang/String;

    .line 5775
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_entry:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_entry:Ljava/util/List;

    .line 5776
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 5781
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 5783
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 5784
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5785
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_type_display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 5786
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_entry:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 5787
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;
    .locals 2

    .line 5760
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;-><init>()V

    .line 5761
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;->fulfillment_id:Ljava/lang/String;

    .line 5762
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_type_display_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;->fulfillment_type_display_name:Ljava/lang/String;

    .line 5763
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_entry:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;->fulfillment_entry:Ljava/util/List;

    .line 5764
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 5709
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 5794
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 5795
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", fulfillment_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5796
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_type_display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", fulfillment_type_display_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_type_display_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5797
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_entry:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", fulfillment_entry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;->fulfillment_entry:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Fulfillment{"

    .line 5798
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
