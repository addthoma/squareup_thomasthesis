.class public final Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DiningOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/DiningOptionLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/DiningOptionLineItem;",
        "Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

.field public backing_details:Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;

.field public dining_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 116
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public application_scope(Lcom/squareup/protos/client/bills/ApplicationScope;)Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    return-object p0
.end method

.method public backing_details(Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;->backing_details:Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/DiningOptionLineItem;
    .locals 5

    .line 142
    new-instance v0, Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;->dining_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;->backing_details:Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/DiningOptionLineItem;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;Lcom/squareup/protos/client/bills/ApplicationScope;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;->build()Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    move-result-object v0

    return-object v0
.end method

.method public dining_option_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;->dining_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method
