.class public final Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpsertContactRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/UpsertContactRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/UpsertContactRequest;",
        "Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact:Lcom/squareup/protos/client/rolodex/Contact;

.field public contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

.field public request_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 117
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/UpsertContactRequest;
    .locals 5

    .line 143
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpsertContactRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;->request_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;->contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/rolodex/UpsertContactRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/ContactOptions;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UpsertContactRequest;

    move-result-object v0

    return-object v0
.end method

.method public contact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object p0
.end method

.method public contact_options(Lcom/squareup/protos/client/rolodex/ContactOptions;)Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;->contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

    return-object p0
.end method

.method public request_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;->request_token:Ljava/lang/String;

    return-object p0
.end method
