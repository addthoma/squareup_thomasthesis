.class public final Lcom/squareup/protos/client/rolodex/Merchant$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Merchant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/Merchant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/Merchant;",
        "Lcom/squareup/protos/client/rolodex/Merchant$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attribute_schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;

.field public manual_customer_count:Ljava/lang/Integer;

.field public merchant_settings:Lcom/squareup/protos/client/rolodex/MerchantSettings;

.field public num_customers:Ljava/lang/Integer;

.field public unread_messages_count:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 145
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public attribute_schema(Lcom/squareup/protos/client/rolodex/AttributeSchema;)Lcom/squareup/protos/client/rolodex/Merchant$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->attribute_schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/Merchant;
    .locals 8

    .line 181
    new-instance v7, Lcom/squareup/protos/client/rolodex/Merchant;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->manual_customer_count:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->unread_messages_count:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->attribute_schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->num_customers:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->merchant_settings:Lcom/squareup/protos/client/rolodex/MerchantSettings;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/rolodex/Merchant;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/client/rolodex/AttributeSchema;Ljava/lang/Integer;Lcom/squareup/protos/client/rolodex/MerchantSettings;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 134
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->build()Lcom/squareup/protos/client/rolodex/Merchant;

    move-result-object v0

    return-object v0
.end method

.method public manual_customer_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/Merchant$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->manual_customer_count:Ljava/lang/Integer;

    return-object p0
.end method

.method public merchant_settings(Lcom/squareup/protos/client/rolodex/MerchantSettings;)Lcom/squareup/protos/client/rolodex/Merchant$Builder;
    .locals 0

    .line 175
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->merchant_settings:Lcom/squareup/protos/client/rolodex/MerchantSettings;

    return-object p0
.end method

.method public num_customers(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/Merchant$Builder;
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->num_customers:Ljava/lang/Integer;

    return-object p0
.end method

.method public unread_messages_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/Merchant$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->unread_messages_count:Ljava/lang/Integer;

    return-object p0
.end method
