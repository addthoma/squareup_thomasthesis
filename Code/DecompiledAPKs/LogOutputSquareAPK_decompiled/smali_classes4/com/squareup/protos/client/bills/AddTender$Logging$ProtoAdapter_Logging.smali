.class final Lcom/squareup/protos/client/bills/AddTender$Logging$ProtoAdapter_Logging;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AddTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddTender$Logging;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Logging"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/AddTender$Logging;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 328
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/AddTender$Logging;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/AddTender$Logging;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 345
    new-instance v0, Lcom/squareup/protos/client/bills/AddTender$Logging$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddTender$Logging$Builder;-><init>()V

    .line 346
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 347
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 351
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 349
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddTender$Logging$Builder;->manual_request_try_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/AddTender$Logging$Builder;

    goto :goto_0

    .line 355
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/AddTender$Logging$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 356
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/AddTender$Logging$Builder;->build()Lcom/squareup/protos/client/bills/AddTender$Logging;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 326
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddTender$Logging$ProtoAdapter_Logging;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/AddTender$Logging;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/AddTender$Logging;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 339
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTender$Logging;->manual_request_try_count:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 340
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/AddTender$Logging;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 326
    check-cast p2, Lcom/squareup/protos/client/bills/AddTender$Logging;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/AddTender$Logging$ProtoAdapter_Logging;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/AddTender$Logging;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/AddTender$Logging;)I
    .locals 3

    .line 333
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTender$Logging;->manual_request_try_count:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 334
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTender$Logging;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 326
    check-cast p1, Lcom/squareup/protos/client/bills/AddTender$Logging;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddTender$Logging$ProtoAdapter_Logging;->encodedSize(Lcom/squareup/protos/client/bills/AddTender$Logging;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/AddTender$Logging;)Lcom/squareup/protos/client/bills/AddTender$Logging;
    .locals 0

    .line 361
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTender$Logging;->newBuilder()Lcom/squareup/protos/client/bills/AddTender$Logging$Builder;

    move-result-object p1

    .line 362
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTender$Logging$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 363
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTender$Logging$Builder;->build()Lcom/squareup/protos/client/bills/AddTender$Logging;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 326
    check-cast p1, Lcom/squareup/protos/client/bills/AddTender$Logging;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddTender$Logging$ProtoAdapter_Logging;->redact(Lcom/squareup/protos/client/bills/AddTender$Logging;)Lcom/squareup/protos/client/bills/AddTender$Logging;

    move-result-object p1

    return-object p1
.end method
