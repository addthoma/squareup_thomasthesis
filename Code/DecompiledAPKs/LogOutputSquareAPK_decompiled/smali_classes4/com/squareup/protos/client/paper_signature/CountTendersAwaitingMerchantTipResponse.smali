.class public final Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;
.super Lcom/squareup/wire/Message;
.source "CountTendersAwaitingMerchantTipResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$ProtoAdapter_CountTendersAwaitingMerchantTipResponse;,
        Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;",
        "Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ERROR_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_ERROR_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_NEXT_REQUEST_BACKOFF_SECONDS:Ljava/lang/Integer;

.field public static final DEFAULT_SUCCESS:Ljava/lang/Boolean;

.field public static final DEFAULT_TENDER_AWAITING_MERCHANT_TIP_COUNT:Ljava/lang/Integer;

.field public static final DEFAULT_TENDER_AWAITING_MERCHANT_TIP_EXPIRING_SOON_AGE_SECONDS:Ljava/lang/Integer;

.field public static final DEFAULT_TENDER_AWAITING_MERCHANT_TIP_EXPIRING_SOON_COUNT:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final error_message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final error_title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final next_request_backoff_seconds:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x5
    .end annotation
.end field

.field public final success:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final tender_awaiting_merchant_tip_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final tender_awaiting_merchant_tip_expiring_soon_age_seconds:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x7
    .end annotation
.end field

.field public final tender_awaiting_merchant_tip_expiring_soon_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 22
    new-instance v0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$ProtoAdapter_CountTendersAwaitingMerchantTipResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$ProtoAdapter_CountTendersAwaitingMerchantTipResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 26
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    .line 32
    sput-object v1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->DEFAULT_TENDER_AWAITING_MERCHANT_TIP_COUNT:Ljava/lang/Integer;

    .line 34
    sput-object v1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->DEFAULT_NEXT_REQUEST_BACKOFF_SECONDS:Ljava/lang/Integer;

    .line 36
    sput-object v1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->DEFAULT_TENDER_AWAITING_MERCHANT_TIP_EXPIRING_SOON_COUNT:Ljava/lang/Integer;

    .line 38
    sput-object v1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->DEFAULT_TENDER_AWAITING_MERCHANT_TIP_EXPIRING_SOON_AGE_SECONDS:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 9

    .line 115
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 123
    sget-object v0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    .line 125
    iput-object p2, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    .line 126
    iput-object p3, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    .line 127
    iput-object p4, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_count:Ljava/lang/Integer;

    .line 128
    iput-object p5, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    .line 129
    iput-object p6, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_count:Ljava/lang/Integer;

    .line 130
    iput-object p7, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_age_seconds:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 150
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 151
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    .line 152
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    .line 153
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    .line 154
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    .line 155
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_count:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_count:Ljava/lang/Integer;

    .line 156
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    .line 157
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_count:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_count:Ljava/lang/Integer;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_age_seconds:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_age_seconds:Ljava/lang/Integer;

    .line 159
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 164
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 166
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 167
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 169
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_count:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_count:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_age_seconds:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 174
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;
    .locals 2

    .line 135
    new-instance v0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;-><init>()V

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->success:Ljava/lang/Boolean;

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->error_message:Ljava/lang/String;

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->error_title:Ljava/lang/String;

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_count:Ljava/lang/Integer;

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->next_request_backoff_seconds:Ljava/lang/Integer;

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_expiring_soon_count:Ljava/lang/Integer;

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_age_seconds:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_expiring_soon_age_seconds:Ljava/lang/Integer;

    .line 143
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->newBuilder()Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 183
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", error_message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", error_title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_count:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", tender_awaiting_merchant_tip_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 186
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", next_request_backoff_seconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 187
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_count:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    const-string v1, ", tender_awaiting_merchant_tip_expiring_soon_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 188
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_age_seconds:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    const-string v1, ", tender_awaiting_merchant_tip_expiring_soon_age_seconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_age_seconds:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CountTendersAwaitingMerchantTipResponse{"

    .line 189
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
