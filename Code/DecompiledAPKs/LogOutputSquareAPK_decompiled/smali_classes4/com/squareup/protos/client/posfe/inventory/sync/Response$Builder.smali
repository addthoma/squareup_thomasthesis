.class public final Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Response.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/posfe/inventory/sync/Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/Response;",
        "Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

.field public get_response:Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;

.field public put_response:Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 109
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/posfe/inventory/sync/Response;
    .locals 5

    .line 131
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    iget-object v2, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;->get_response:Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;

    iget-object v3, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;->put_response:Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/posfe/inventory/sync/Response;-><init>(Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/Response;

    move-result-object v0

    return-object v0
.end method

.method public error(Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;)Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    return-object p0
.end method

.method public get_response(Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;)Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;->get_response:Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;

    const/4 p1, 0x0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;->put_response:Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;

    return-object p0
.end method

.method public put_response(Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;)Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;->put_response:Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;

    const/4 p1, 0x0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;->get_response:Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;

    return-object p0
.end method
