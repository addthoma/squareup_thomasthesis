.class public final Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;
.super Lcom/squareup/wire/Message;
.source "ModifierOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConversationalMode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$ProtoAdapter_ConversationalMode;,
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;,
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final type:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 835
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$ProtoAdapter_ConversationalMode;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$ProtoAdapter_ConversationalMode;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 839
    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;->UNKNOWN:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    sput-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->DEFAULT_TYPE:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;)V
    .locals 1

    .line 851
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;-><init>(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;Lokio/ByteString;)V
    .locals 1

    .line 855
    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 856
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->type:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 870
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 871
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;

    .line 872
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->type:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->type:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    .line 873
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 878
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 880
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 881
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->type:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 882
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Builder;
    .locals 2

    .line 861
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Builder;-><init>()V

    .line 862
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->type:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Builder;->type:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    .line 863
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 834
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 889
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 890
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->type:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->type:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ConversationalMode{"

    .line 891
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
