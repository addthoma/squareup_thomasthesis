.class public final Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;
.super Lcom/squareup/wire/Message;
.source "SetEGiftOrderConfigurationRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$ProtoAdapter_SetEGiftOrderConfigurationRequest;,
        Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;",
        "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CLIENT_REQUEST_UUID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final client_request_uuid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final new_custom_egift_themes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.giftcards.EGiftTheme#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation
.end field

.field public final order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.giftcards.EGiftOrderConfiguration#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$ProtoAdapter_SetEGiftOrderConfigurationRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$ProtoAdapter_SetEGiftOrderConfigurationRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 54
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;-><init>(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 60
    sget-object v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 61
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    const-string p1, "new_custom_egift_themes"

    .line 62
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->new_custom_egift_themes:Ljava/util/List;

    .line 63
    iput-object p3, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->client_request_uuid:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 79
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 80
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;

    .line 81
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    .line 82
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->new_custom_egift_themes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->new_custom_egift_themes:Ljava/util/List;

    .line 83
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->client_request_uuid:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->client_request_uuid:Ljava/lang/String;

    .line 84
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 89
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->new_custom_egift_themes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->client_request_uuid:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 95
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;
    .locals 2

    .line 68
    new-instance v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;-><init>()V

    .line 69
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    .line 70
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->new_custom_egift_themes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->new_custom_egift_themes:Ljava/util/List;

    .line 71
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->client_request_uuid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->client_request_uuid:Ljava/lang/String;

    .line 72
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->newBuilder()Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    if-eqz v1, :cond_0

    const-string v1, ", order_configuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->new_custom_egift_themes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", new_custom_egift_themes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->new_custom_egift_themes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->client_request_uuid:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", client_request_uuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->client_request_uuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SetEGiftOrderConfigurationRequest{"

    .line 106
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
