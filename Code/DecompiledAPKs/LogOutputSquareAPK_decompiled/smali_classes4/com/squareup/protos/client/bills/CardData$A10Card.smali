.class public final Lcom/squareup/protos/client/bills/CardData$A10Card;
.super Lcom/squareup/wire/Message;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "A10Card"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardData$A10Card$ProtoAdapter_A10Card;,
        Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;,
        Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/CardData$A10Card;",
        "Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardData$A10Card;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ENCRYPTED_READER_DATA:Lokio/ByteString;

.field public static final DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

.field private static final serialVersionUID:J


# instance fields
.field public final encrypted_reader_data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final entry_method:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$A10Card$EntryMethod#ADAPTER"
        tag = 0x3
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1544
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$A10Card$ProtoAdapter_A10Card;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$A10Card$ProtoAdapter_A10Card;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$A10Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1548
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$A10Card;->DEFAULT_ENCRYPTED_READER_DATA:Lokio/ByteString;

    .line 1550
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$A10Card;->DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;)V
    .locals 1

    .line 1573
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/CardData$A10Card;-><init>(Lokio/ByteString;Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;Lokio/ByteString;)V
    .locals 1

    .line 1578
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$A10Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1579
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$A10Card;->encrypted_reader_data:Lokio/ByteString;

    .line 1580
    iput-object p2, p0, Lcom/squareup/protos/client/bills/CardData$A10Card;->entry_method:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1595
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/CardData$A10Card;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1596
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/CardData$A10Card;

    .line 1597
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$A10Card;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$A10Card;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$A10Card;->encrypted_reader_data:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData$A10Card;->encrypted_reader_data:Lokio/ByteString;

    .line 1598
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$A10Card;->entry_method:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardData$A10Card;->entry_method:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    .line 1599
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1604
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 1606
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$A10Card;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1607
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$A10Card;->encrypted_reader_data:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1608
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$A10Card;->entry_method:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 1609
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;
    .locals 2

    .line 1585
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;-><init>()V

    .line 1586
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$A10Card;->encrypted_reader_data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->encrypted_reader_data:Lokio/ByteString;

    .line 1587
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$A10Card;->entry_method:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    .line 1588
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$A10Card;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1543
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$A10Card;->newBuilder()Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1616
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1617
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$A10Card;->encrypted_reader_data:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", encrypted_reader_data=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1618
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$A10Card;->entry_method:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    if-eqz v1, :cond_1

    const-string v1, ", entry_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$A10Card;->entry_method:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "A10Card{"

    .line 1619
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
