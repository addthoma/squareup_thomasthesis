.class final Lcom/squareup/protos/client/orders/FulfillmentClientDetails$ProtoAdapter_FulfillmentClientDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "FulfillmentClientDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/FulfillmentClientDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FulfillmentClientDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/orders/FulfillmentClientDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 281
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/orders/FulfillmentClientDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 302
    new-instance v0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;-><init>()V

    .line 303
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 304
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 310
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 308
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->fulfillment_window_ends_at(Ljava/lang/String;)Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;

    goto :goto_0

    .line 307
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->courier(Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;)Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;

    goto :goto_0

    .line 306
    :cond_2
    iget-object v3, v0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->available_actions:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/orders/Action;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 314
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 315
    invoke-virtual {v0}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->build()Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 279
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$ProtoAdapter_FulfillmentClientDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/orders/FulfillmentClientDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 294
    sget-object v0, Lcom/squareup/protos/client/orders/Action;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->available_actions:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 295
    sget-object v0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 296
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->fulfillment_window_ends_at:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 297
    invoke-virtual {p2}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 279
    check-cast p2, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$ProtoAdapter_FulfillmentClientDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/orders/FulfillmentClientDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/orders/FulfillmentClientDetails;)I
    .locals 4

    .line 286
    sget-object v0, Lcom/squareup/protos/client/orders/Action;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->available_actions:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    const/4 v3, 0x2

    .line 287
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->fulfillment_window_ends_at:Ljava/lang/String;

    const/4 v3, 0x3

    .line 288
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 289
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 279
    check-cast p1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$ProtoAdapter_FulfillmentClientDetails;->encodedSize(Lcom/squareup/protos/client/orders/FulfillmentClientDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/orders/FulfillmentClientDetails;)Lcom/squareup/protos/client/orders/FulfillmentClientDetails;
    .locals 2

    .line 320
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->newBuilder()Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;

    move-result-object p1

    .line 321
    iget-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->available_actions:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/orders/Action;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 322
    iget-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    iput-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    .line 323
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 324
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->build()Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 279
    check-cast p1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$ProtoAdapter_FulfillmentClientDetails;->redact(Lcom/squareup/protos/client/orders/FulfillmentClientDetails;)Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    move-result-object p1

    return-object p1
.end method
