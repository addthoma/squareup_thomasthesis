.class public final Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AddTendersRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;",
        "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public is_canceled:Ljava/lang/Boolean;

.field public tender_id_pair:Lcom/squareup/protos/client/IdPair;

.field public total_charged_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 407
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;
    .locals 5

    .line 445
    new-instance v0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->is_canceled:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->total_charged_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 400
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;

    move-result-object v0

    return-object v0
.end method

.method public is_canceled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;
    .locals 0

    .line 426
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->is_canceled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;
    .locals 0

    .line 418
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public total_charged_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;
    .locals 0

    .line 439
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->total_charged_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
