.class public final Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BatchGetLoyaltyAccountsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;",
        "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public loyalty_account_tokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 105
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 106
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;->loyalty_account_tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;
    .locals 4

    .line 129
    new-instance v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;->loyalty_account_tokens:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;-><init>(Ljava/util/List;Lcom/squareup/protos/client/loyalty/LoyaltyOptions;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;

    move-result-object v0

    return-object v0
.end method

.method public loyalty_account_tokens(Ljava/util/List;)Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;"
        }
    .end annotation

    .line 113
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;->loyalty_account_tokens:Ljava/util/List;

    return-object p0
.end method

.method public options(Lcom/squareup/protos/client/loyalty/LoyaltyOptions;)Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    return-object p0
.end method
