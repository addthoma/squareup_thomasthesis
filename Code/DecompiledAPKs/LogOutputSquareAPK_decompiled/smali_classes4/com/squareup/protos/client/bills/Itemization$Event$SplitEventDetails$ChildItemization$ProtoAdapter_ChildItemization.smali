.class final Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$ProtoAdapter_ChildItemization;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ChildItemization"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 3646
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3665
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;-><init>()V

    .line 3666
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 3667
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 3672
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 3670
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->child_itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;

    goto :goto_0

    .line 3669
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->ticket_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;

    goto :goto_0

    .line 3676
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 3677
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3644
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$ProtoAdapter_ChildItemization;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3658
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3659
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3660
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3644
    check-cast p2, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$ProtoAdapter_ChildItemization;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;)I
    .locals 4

    .line 3651
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v3, 0x2

    .line 3652
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3653
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 3644
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$ProtoAdapter_ChildItemization;->encodedSize(Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;
    .locals 2

    .line 3682
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;

    move-result-object p1

    .line 3683
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 3684
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 3685
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 3686
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 3644
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$ProtoAdapter_ChildItemization;->redact(Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    move-result-object p1

    return-object p1
.end method
