.class public final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;
.super Lcom/squareup/wire/Message;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PricingEngineState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$ProtoAdapter_PricingEngineState;,
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final blacklisted_discount_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5204
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$ProtoAdapter_PricingEngineState;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$ProtoAdapter_PricingEngineState;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 5220
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 5224
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p2, "blacklisted_discount_ids"

    .line 5225
    invoke-static {p2, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->blacklisted_discount_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 5239
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 5240
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    .line 5241
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->blacklisted_discount_ids:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->blacklisted_discount_ids:Ljava/util/List;

    .line 5242
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 5247
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_0

    .line 5249
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 5250
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->blacklisted_discount_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 5251
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_0
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;
    .locals 2

    .line 5230
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;-><init>()V

    .line 5231
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->blacklisted_discount_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;->blacklisted_discount_ids:Ljava/util/List;

    .line 5232
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 5203
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 5258
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 5259
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->blacklisted_discount_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", blacklisted_discount_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->blacklisted_discount_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PricingEngineState{"

    .line 5260
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
