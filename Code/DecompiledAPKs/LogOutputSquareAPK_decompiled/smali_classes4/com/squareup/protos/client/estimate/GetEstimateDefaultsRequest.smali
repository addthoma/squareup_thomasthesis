.class public final Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest;
.super Lcom/squareup/wire/Message;
.source "GetEstimateDefaultsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest$ProtoAdapter_GetEstimateDefaultsRequest;,
        Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest;",
        "Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest$ProtoAdapter_GetEstimateDefaultsRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest$ProtoAdapter_GetEstimateDefaultsRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 23
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, v0}, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest;-><init>(Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;)V
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p1}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 40
    :cond_0
    instance-of v0, p1, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 41
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest;

    .line 42
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest$Builder;
    .locals 2

    .line 32
    new-instance v0, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest$Builder;-><init>()V

    .line 33
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest;->newBuilder()Lcom/squareup/protos/client/estimate/GetEstimateDefaultsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetEstimateDefaultsRequest{"

    .line 53
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
