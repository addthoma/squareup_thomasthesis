.class public final Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TransferLoyaltyAccountResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse;",
        "Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 81
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse;
    .locals 3

    .line 91
    new-instance v0, Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse;-><init>(Lcom/squareup/protos/client/Status;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse$Builder;
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
