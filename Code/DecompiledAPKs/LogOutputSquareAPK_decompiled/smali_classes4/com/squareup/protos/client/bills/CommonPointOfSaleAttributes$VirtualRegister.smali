.class public final Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;
.super Lcom/squareup/wire/Message;
.source "CommonPointOfSaleAttributes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VirtualRegister"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$ProtoAdapter_VirtualRegister;,
        Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;",
        "Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BUSINESS_DAY_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_TRANSACTION_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_VIRTUAL_REGISTER_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final business_day_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final receipt_token:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final transaction_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final virtual_register_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$ProtoAdapter_VirtualRegister;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$ProtoAdapter_VirtualRegister;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 84
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 89
    sget-object v0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 90
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->virtual_register_token:Ljava/lang/String;

    .line 91
    iput-object p2, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->business_day_token:Ljava/lang/String;

    .line 92
    iput-object p3, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->transaction_number:Ljava/lang/String;

    const-string p1, "receipt_token"

    .line 93
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->receipt_token:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 110
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 111
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->virtual_register_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->virtual_register_token:Ljava/lang/String;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->business_day_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->business_day_token:Ljava/lang/String;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->transaction_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->transaction_number:Ljava/lang/String;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->receipt_token:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->receipt_token:Ljava/util/List;

    .line 116
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 121
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 123
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->virtual_register_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->business_day_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->transaction_number:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->receipt_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;
    .locals 2

    .line 98
    new-instance v0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;-><init>()V

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->virtual_register_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;->virtual_register_token:Ljava/lang/String;

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->business_day_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;->business_day_token:Ljava/lang/String;

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->transaction_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;->transaction_number:Ljava/lang/String;

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->receipt_token:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;->receipt_token:Ljava/util/List;

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->newBuilder()Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->virtual_register_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", virtual_register_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->virtual_register_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->business_day_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", business_day_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->business_day_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->transaction_number:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", transaction_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->transaction_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->receipt_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", receipt_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;->receipt_token:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "VirtualRegister{"

    .line 140
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
