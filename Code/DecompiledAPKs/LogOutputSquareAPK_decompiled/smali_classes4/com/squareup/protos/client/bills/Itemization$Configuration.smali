.class public final Lcom/squareup/protos/client/bills/Itemization$Configuration;
.super Lcom/squareup/wire/Message;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Configuration"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Itemization$Configuration$ProtoAdapter_Configuration;,
        Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;,
        Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;,
        Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration;",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Itemization$Configuration;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BACKING_TYPE:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

.field private static final serialVersionUID:J


# instance fields
.field public final backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$Configuration$BackingType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final category:Lcom/squareup/api/items/MenuCategory;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.MenuCategory#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final item_variation_price_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$Configuration$SelectedOptions#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 492
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$ProtoAdapter_Configuration;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$ProtoAdapter_Configuration;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 496
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->UNKNOWN:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->DEFAULT_BACKING_TYPE:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;Lcom/squareup/protos/common/Money;Lcom/squareup/api/items/MenuCategory;)V
    .locals 6

    .line 533
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/Itemization$Configuration;-><init>(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;Lcom/squareup/protos/common/Money;Lcom/squareup/api/items/MenuCategory;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;Lcom/squareup/protos/common/Money;Lcom/squareup/api/items/MenuCategory;Lokio/ByteString;)V
    .locals 1

    .line 538
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 539
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    .line 540
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    .line 541
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    .line 542
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->category:Lcom/squareup/api/items/MenuCategory;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 559
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 560
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;

    .line 561
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Configuration;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    .line 562
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    .line 563
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    .line 564
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->category:Lcom/squareup/api/items/MenuCategory;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->category:Lcom/squareup/api/items/MenuCategory;

    .line 565
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 570
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 572
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Configuration;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 573
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 574
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 575
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 576
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->category:Lcom/squareup/api/items/MenuCategory;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/api/items/MenuCategory;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 577
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;
    .locals 2

    .line 547
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;-><init>()V

    .line 548
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    .line 549
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    .line 550
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    .line 551
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->category:Lcom/squareup/api/items/MenuCategory;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->category:Lcom/squareup/api/items/MenuCategory;

    .line 552
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Configuration;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 491
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Configuration;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 584
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 585
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-eqz v1, :cond_0

    const-string v1, ", selected_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 586
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    if-eqz v1, :cond_1

    const-string v1, ", backing_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 587
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", item_variation_price_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 588
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->category:Lcom/squareup/api/items/MenuCategory;

    if-eqz v1, :cond_3

    const-string v1, ", category="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->category:Lcom/squareup/api/items/MenuCategory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Configuration{"

    .line 589
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
