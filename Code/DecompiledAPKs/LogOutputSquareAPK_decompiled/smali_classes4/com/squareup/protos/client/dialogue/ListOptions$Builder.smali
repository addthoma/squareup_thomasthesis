.class public final Lcom/squareup/protos/client/dialogue/ListOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/ListOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/dialogue/ListOptions;",
        "Lcom/squareup/protos/client/dialogue/ListOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public limit:Ljava/lang/Integer;

.field public offset:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/dialogue/ListOptions;
    .locals 4

    .line 120
    new-instance v0, Lcom/squareup/protos/client/dialogue/ListOptions;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListOptions$Builder;->offset:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/protos/client/dialogue/ListOptions$Builder;->limit:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/dialogue/ListOptions;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/ListOptions$Builder;->build()Lcom/squareup/protos/client/dialogue/ListOptions;

    move-result-object v0

    return-object v0
.end method

.method public limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/dialogue/ListOptions$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ListOptions$Builder;->limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public offset(Ljava/lang/Long;)Lcom/squareup/protos/client/dialogue/ListOptions$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ListOptions$Builder;->offset:Ljava/lang/Long;

    return-object p0
.end method
