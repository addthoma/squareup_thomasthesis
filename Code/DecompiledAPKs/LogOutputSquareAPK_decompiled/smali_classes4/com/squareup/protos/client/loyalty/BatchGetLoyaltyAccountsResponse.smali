.class public final Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;
.super Lcom/squareup/wire/Message;
.source "BatchGetLoyaltyAccountsResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$ProtoAdapter_BatchGetLoyaltyAccountsResponse;,
        Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;",
        "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

.field private static final serialVersionUID:J


# instance fields
.field public final errors:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyRequestError#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
            ">;"
        }
    .end annotation
.end field

.field public final loyaltyAccounts:Ljava/util/Map;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyAccount#ADAPTER"
        keyAdapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccount;",
            ">;"
        }
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyRequestStatus#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$ProtoAdapter_BatchGetLoyaltyAccountsResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$ProtoAdapter_BatchGetLoyaltyAccountsResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    sput-object v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->DEFAULT_STATUS:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccount;",
            ">;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
            ">;)V"
        }
    .end annotation

    .line 67
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;-><init>(Ljava/util/Map;Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccount;",
            ">;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 72
    sget-object v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p4, "loyaltyAccounts"

    .line 73
    invoke-static {p4, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->loyaltyAccounts:Ljava/util/Map;

    .line 74
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    const-string p1, "errors"

    .line 75
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->errors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 91
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 92
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->loyaltyAccounts:Ljava/util/Map;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->loyaltyAccounts:Ljava/util/Map;

    .line 94
    invoke-interface {v1, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    .line 95
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->errors:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->errors:Ljava/util/List;

    .line 96
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 101
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->loyaltyAccounts:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;
    .locals 2

    .line 80
    new-instance v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;-><init>()V

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->loyaltyAccounts:Ljava/util/Map;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;->loyaltyAccounts:Ljava/util/Map;

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->errors:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;->errors:Ljava/util/List;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->newBuilder()Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->loyaltyAccounts:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", loyaltyAccounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->loyaltyAccounts:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    if-eqz v1, :cond_1

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 117
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", errors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->errors:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BatchGetLoyaltyAccountsResponse{"

    .line 118
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
