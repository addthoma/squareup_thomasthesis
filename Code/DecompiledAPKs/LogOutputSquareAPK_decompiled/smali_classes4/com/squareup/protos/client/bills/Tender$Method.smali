.class public final Lcom/squareup/protos/client/bills/Tender$Method;
.super Lcom/squareup/wire/Message;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Method"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Tender$Method$ProtoAdapter_Method;,
        Lcom/squareup/protos/client/bills/Tender$Method$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Tender$Method;",
        "Lcom/squareup/protos/client/bills/Tender$Method$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Tender$Method;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final card_tender:Lcom/squareup/protos/client/bills/CardTender;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final cash_tender:Lcom/squareup/protos/client/bills/CashTender;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CashTender#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final external_tender:Lcom/squareup/protos/client/bills/ExternalTender;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.ExternalTender#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.NoSaleTender#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final other_tender:Lcom/squareup/protos/client/bills/OtherTender;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.OtherTender#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.GenericTender#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.SquareCapitalTender#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.WalletTender#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.ZeroAmountTender#ADAPTER"
        tag = 0x8
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 869
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Method$ProtoAdapter_Method;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Method$ProtoAdapter_Method;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$Method;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/CardTender;Lcom/squareup/protos/client/bills/CashTender;Lcom/squareup/protos/client/bills/NoSaleTender;Lcom/squareup/protos/client/bills/OtherTender;Lcom/squareup/protos/client/bills/WalletTender;Lcom/squareup/protos/client/bills/GenericTender;Lcom/squareup/protos/client/bills/ZeroAmountTender;Lcom/squareup/protos/client/bills/ExternalTender;Lcom/squareup/protos/client/bills/SquareCapitalTender;)V
    .locals 11

    .line 931
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/bills/Tender$Method;-><init>(Lcom/squareup/protos/client/bills/CardTender;Lcom/squareup/protos/client/bills/CashTender;Lcom/squareup/protos/client/bills/NoSaleTender;Lcom/squareup/protos/client/bills/OtherTender;Lcom/squareup/protos/client/bills/WalletTender;Lcom/squareup/protos/client/bills/GenericTender;Lcom/squareup/protos/client/bills/ZeroAmountTender;Lcom/squareup/protos/client/bills/ExternalTender;Lcom/squareup/protos/client/bills/SquareCapitalTender;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/CardTender;Lcom/squareup/protos/client/bills/CashTender;Lcom/squareup/protos/client/bills/NoSaleTender;Lcom/squareup/protos/client/bills/OtherTender;Lcom/squareup/protos/client/bills/WalletTender;Lcom/squareup/protos/client/bills/GenericTender;Lcom/squareup/protos/client/bills/ZeroAmountTender;Lcom/squareup/protos/client/bills/ExternalTender;Lcom/squareup/protos/client/bills/SquareCapitalTender;Lokio/ByteString;)V
    .locals 1

    .line 939
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Method;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 940
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    .line 941
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender$Method;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    .line 942
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Tender$Method;->no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;

    .line 943
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    .line 944
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Tender$Method;->wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;

    .line 945
    iput-object p6, p0, Lcom/squareup/protos/client/bills/Tender$Method;->read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;

    .line 946
    iput-object p7, p0, Lcom/squareup/protos/client/bills/Tender$Method;->zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;

    .line 947
    iput-object p8, p0, Lcom/squareup/protos/client/bills/Tender$Method;->external_tender:Lcom/squareup/protos/client/bills/ExternalTender;

    .line 948
    iput-object p9, p0, Lcom/squareup/protos/client/bills/Tender$Method;->square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 970
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Tender$Method;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 971
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Tender$Method;

    .line 972
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$Method;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$Method;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    .line 973
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$Method;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    .line 974
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$Method;->no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;

    .line 975
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    .line 976
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$Method;->wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;

    .line 977
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$Method;->read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;

    .line 978
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$Method;->zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;

    .line 979
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->external_tender:Lcom/squareup/protos/client/bills/ExternalTender;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$Method;->external_tender:Lcom/squareup/protos/client/bills/ExternalTender;

    .line 980
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender$Method;->square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;

    .line 981
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 986
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 988
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$Method;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 989
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 990
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CashTender;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 991
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/NoSaleTender;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 992
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/OtherTender;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 993
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/WalletTender;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 994
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/GenericTender;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 995
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ZeroAmountTender;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 996
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->external_tender:Lcom/squareup/protos/client/bills/ExternalTender;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ExternalTender;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 997
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/SquareCapitalTender;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 998
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Tender$Method$Builder;
    .locals 2

    .line 953
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;-><init>()V

    .line 954
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    .line 955
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    .line 956
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;

    .line 957
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    .line 958
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;

    .line 959
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;

    .line 960
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;

    .line 961
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->external_tender:Lcom/squareup/protos/client/bills/ExternalTender;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->external_tender:Lcom/squareup/protos/client/bills/ExternalTender;

    .line 962
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;

    .line 963
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$Method;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 868
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$Method;->newBuilder()Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1005
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1006
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    if-eqz v1, :cond_0

    const-string v1, ", card_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1007
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    if-eqz v1, :cond_1

    const-string v1, ", cash_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1008
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;

    if-eqz v1, :cond_2

    const-string v1, ", no_sale_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1009
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    if-eqz v1, :cond_3

    const-string v1, ", other_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1010
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;

    if-eqz v1, :cond_4

    const-string v1, ", wallet_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1011
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;

    if-eqz v1, :cond_5

    const-string v1, ", read_only_generic_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1012
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;

    if-eqz v1, :cond_6

    const-string v1, ", zero_amount_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1013
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->external_tender:Lcom/squareup/protos/client/bills/ExternalTender;

    if-eqz v1, :cond_7

    const-string v1, ", external_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->external_tender:Lcom/squareup/protos/client/bills/ExternalTender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1014
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;

    if-eqz v1, :cond_8

    const-string v1, ", square_capital_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method;->square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Method{"

    .line 1015
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
