.class public final Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SetDepositScheduleRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;",
        "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public day_of_week_deposit_settings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
            ">;"
        }
    .end annotation
.end field

.field public enable_daily_same_day_settlement:Ljava/lang/Boolean;

.field public pause_settlement:Ljava/lang/Boolean;

.field public settle_if_optional:Ljava/lang/Boolean;

.field public timezone:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 169
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 170
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->day_of_week_deposit_settings:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;
    .locals 8

    .line 225
    new-instance v7, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->timezone:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->day_of_week_deposit_settings:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->enable_daily_same_day_settlement:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->settle_if_optional:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->pause_settlement:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 158
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->build()Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;

    move-result-object v0

    return-object v0
.end method

.method public day_of_week_deposit_settings(Ljava/util/List;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
            ">;)",
            "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;"
        }
    .end annotation

    .line 189
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 190
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->day_of_week_deposit_settings:Ljava/util/List;

    return-object p0
.end method

.method public enable_daily_same_day_settlement(Ljava/lang/Boolean;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->enable_daily_same_day_settlement:Ljava/lang/Boolean;

    return-object p0
.end method

.method public pause_settlement(Ljava/lang/Boolean;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->pause_settlement:Ljava/lang/Boolean;

    return-object p0
.end method

.method public settle_if_optional(Ljava/lang/Boolean;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;
    .locals 0

    .line 209
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->settle_if_optional:Ljava/lang/Boolean;

    return-object p0
.end method

.method public timezone(Ljava/lang/String;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->timezone:Ljava/lang/String;

    return-object p0
.end method
