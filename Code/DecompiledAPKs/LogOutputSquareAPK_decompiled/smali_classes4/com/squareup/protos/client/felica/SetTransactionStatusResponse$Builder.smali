.class public final Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SetTransactionStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;",
        "Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public connection_id:Ljava/lang/String;

.field public error:Lcom/squareup/protos/client/felica/Error;

.field public status:Lcom/squareup/protos/client/felica/Status;

.field public transaction_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 126
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;
    .locals 7

    .line 151
    new-instance v6, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;->connection_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;->transaction_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;->error:Lcom/squareup/protos/client/felica/Error;

    iget-object v4, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;->status:Lcom/squareup/protos/client/felica/Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/felica/Error;Lcom/squareup/protos/client/felica/Status;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;->build()Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public connection_id(Ljava/lang/String;)Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;->connection_id:Ljava/lang/String;

    return-object p0
.end method

.method public error(Lcom/squareup/protos/client/felica/Error;)Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;->error:Lcom/squareup/protos/client/felica/Error;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/felica/Status;)Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;->status:Lcom/squareup/protos/client/felica/Status;

    return-object p0
.end method

.method public transaction_id(Ljava/lang/String;)Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;->transaction_id:Ljava/lang/String;

    return-object p0
.end method
