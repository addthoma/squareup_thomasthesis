.class public final Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InstantDepositDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/deposits/InstantDepositDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/deposits/InstantDepositDetails;",
        "Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public eligibility_blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

.field public eligibility_status:Lcom/squareup/protos/client/Status;

.field public max_deposit_amount:Lcom/squareup/protos/common/Money;

.field public show_price_change_modal:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 139
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/deposits/InstantDepositDetails;
    .locals 7

    .line 177
    new-instance v6, Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->eligibility_blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    iget-object v2, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->eligibility_status:Lcom/squareup/protos/client/Status;

    iget-object v3, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->max_deposit_amount:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->show_price_change_modal:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/deposits/InstantDepositDetails;-><init>(Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;Lcom/squareup/protos/client/Status;Lcom/squareup/protos/common/Money;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->build()Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    move-result-object v0

    return-object v0
.end method

.method public eligibility_blocker(Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;)Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->eligibility_blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    return-object p0
.end method

.method public eligibility_status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->eligibility_status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public max_deposit_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->max_deposit_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public show_price_change_modal(Ljava/lang/Boolean;)Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->show_price_change_modal:Ljava/lang/Boolean;

    return-object p0
.end method
