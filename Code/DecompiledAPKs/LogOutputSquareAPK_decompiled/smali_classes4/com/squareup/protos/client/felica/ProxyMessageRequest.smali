.class public final Lcom/squareup/protos/client/felica/ProxyMessageRequest;
.super Lcom/squareup/wire/Message;
.source "ProxyMessageRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/felica/ProxyMessageRequest$ProtoAdapter_ProxyMessageRequest;,
        Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/felica/ProxyMessageRequest;",
        "Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/felica/ProxyMessageRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CONNECTION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_PACKET_DATA:Lokio/ByteString;

.field public static final DEFAULT_TRANSACTION_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final connection_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final packet_data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final transaction_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/client/felica/ProxyMessageRequest$ProtoAdapter_ProxyMessageRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/felica/ProxyMessageRequest$ProtoAdapter_ProxyMessageRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->DEFAULT_PACKET_DATA:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 50
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/felica/ProxyMessageRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;)V
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 56
    iput-object p1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->connection_id:Ljava/lang/String;

    .line 57
    iput-object p2, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->transaction_id:Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->packet_data:Lokio/ByteString;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 74
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/felica/ProxyMessageRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 75
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/felica/ProxyMessageRequest;

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->connection_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->connection_id:Ljava/lang/String;

    .line 77
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->transaction_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->transaction_id:Ljava/lang/String;

    .line 78
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->packet_data:Lokio/ByteString;

    iget-object p1, p1, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->packet_data:Lokio/ByteString;

    .line 79
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 84
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->connection_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->transaction_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->packet_data:Lokio/ByteString;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 90
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->connection_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;->connection_id:Ljava/lang/String;

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->transaction_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;->transaction_id:Ljava/lang/String;

    .line 66
    iget-object v1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->packet_data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;->packet_data:Lokio/ByteString;

    .line 67
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->newBuilder()Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->connection_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", connection_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->connection_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->transaction_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", transaction_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->transaction_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;->packet_data:Lokio/ByteString;

    if-eqz v1, :cond_2

    const-string v1, ", packet_data=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ProxyMessageRequest{"

    .line 101
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
