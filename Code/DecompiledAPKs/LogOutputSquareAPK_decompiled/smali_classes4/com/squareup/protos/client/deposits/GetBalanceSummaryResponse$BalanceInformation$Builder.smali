.class public final Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBalanceSummaryResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public available_balance:Lcom/squareup/protos/common/Money;

.field public pending_balance:Lcom/squareup/protos/common/Money;

.field public total_balance:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 582
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public available_balance(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;
    .locals 0

    .line 597
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;->available_balance:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;
    .locals 5

    .line 611
    new-instance v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;->total_balance:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;->available_balance:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;->pending_balance:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 575
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;->build()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    move-result-object v0

    return-object v0
.end method

.method public pending_balance(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;
    .locals 0

    .line 605
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;->pending_balance:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public total_balance(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;
    .locals 0

    .line 589
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;->total_balance:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
