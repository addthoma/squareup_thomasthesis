.class public final Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardActivityEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/CardActivityEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
        "Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public activity_type:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

.field public amount:Lcom/squareup/protos/common/Money;

.field public description:Ljava/lang/String;

.field public image_url:Ljava/lang/String;

.field public is_personal_expense:Ljava/lang/Boolean;

.field public occurred_at:Lcom/squareup/protos/client/ISO8601Date;

.field public token:Ljava/lang/String;

.field public transaction_state:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

.field public transaction_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 228
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public activity_type(Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;)Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;
    .locals 0

    .line 267
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->activity_type:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    return-object p0
.end method

.method public amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;
    .locals 0

    .line 259
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bizbank/CardActivityEvent;
    .locals 12

    .line 302
    new-instance v11, Lcom/squareup/protos/client/bizbank/CardActivityEvent;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->description:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->amount:Lcom/squareup/protos/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->activity_type:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    iget-object v6, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->transaction_state:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    iget-object v7, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->is_personal_expense:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->transaction_token:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->image_url:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/bizbank/CardActivityEvent;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 209
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->build()Lcom/squareup/protos/client/bizbank/CardActivityEvent;

    move-result-object v0

    return-object v0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;
    .locals 0

    .line 251
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public image_url(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;
    .locals 0

    .line 296
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->image_url:Ljava/lang/String;

    return-object p0
.end method

.method public is_personal_expense(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;
    .locals 0

    .line 280
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->is_personal_expense:Ljava/lang/Boolean;

    return-object p0
.end method

.method public occurred_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;
    .locals 0

    .line 235
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->token:Ljava/lang/String;

    return-object p0
.end method

.method public transaction_state(Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;)Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;
    .locals 0

    .line 272
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->transaction_state:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    return-object p0
.end method

.method public transaction_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;
    .locals 0

    .line 288
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->transaction_token:Ljava/lang/String;

    return-object p0
.end method
