.class final Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$ProtoAdapter_InventoryCount;
.super Lcom/squareup/wire/ProtoAdapter;
.source "InventoryCount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_InventoryCount"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 208
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 233
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;-><init>()V

    .line 234
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 235
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 257
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 255
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->updated_at(Ljava/lang/Long;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;

    goto :goto_0

    .line 249
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->inventory_type(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 251
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 246
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->product_identifier(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;

    goto :goto_0

    .line 245
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->current_count(Ljava/lang/Long;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;

    goto :goto_0

    .line 239
    :cond_4
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->state(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 241
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 261
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 262
    invoke-virtual {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 206
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$ProtoAdapter_InventoryCount;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 223
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 224
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->current_count:Ljava/lang/Long;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 225
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 226
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 227
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->updated_at:Ljava/lang/Long;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 228
    invoke-virtual {p2}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 206
    check-cast p2, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$ProtoAdapter_InventoryCount;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;)I
    .locals 4

    .line 213
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->current_count:Ljava/lang/Long;

    const/4 v3, 0x2

    .line 214
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    const/4 v3, 0x3

    .line 215
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    const/4 v3, 0x4

    .line 216
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->updated_at:Ljava/lang/Long;

    const/4 v3, 0x5

    .line 217
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 218
    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 206
    check-cast p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$ProtoAdapter_InventoryCount;->encodedSize(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;
    .locals 2

    .line 267
    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->newBuilder()Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;

    move-result-object p1

    .line 268
    iget-object v0, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    iput-object v0, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    .line 269
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 270
    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 206
    check-cast p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$ProtoAdapter_InventoryCount;->redact(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    move-result-object p1

    return-object p1
.end method
