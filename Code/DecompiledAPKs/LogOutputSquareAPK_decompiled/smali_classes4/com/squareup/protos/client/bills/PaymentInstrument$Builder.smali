.class public final Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PaymentInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/PaymentInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/PaymentInstrument;",
        "Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bitcoin_lightning_instrument_hackweek:Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;

.field public card_data:Lcom/squareup/protos/client/bills/CardData;

.field public stored_instrument:Lcom/squareup/protos/client/bills/StoredInstrument;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 126
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bitcoin_lightning_instrument_hackweek(Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;)Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->bitcoin_lightning_instrument_hackweek:Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;

    const/4 p1, 0x0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->stored_instrument:Lcom/squareup/protos/client/bills/StoredInstrument;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/PaymentInstrument;
    .locals 5

    .line 168
    new-instance v0, Lcom/squareup/protos/client/bills/PaymentInstrument;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->stored_instrument:Lcom/squareup/protos/client/bills/StoredInstrument;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->bitcoin_lightning_instrument_hackweek:Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/PaymentInstrument;-><init>(Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/StoredInstrument;Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 119
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->build()Lcom/squareup/protos/client/bills/PaymentInstrument;

    move-result-object v0

    return-object v0
.end method

.method public card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    const/4 p1, 0x0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->stored_instrument:Lcom/squareup/protos/client/bills/StoredInstrument;

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->bitcoin_lightning_instrument_hackweek:Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;

    return-object p0
.end method

.method public stored_instrument(Lcom/squareup/protos/client/bills/StoredInstrument;)Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->stored_instrument:Lcom/squareup/protos/client/bills/StoredInstrument;

    const/4 p1, 0x0

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->bitcoin_lightning_instrument_hackweek:Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;

    return-object p0
.end method
