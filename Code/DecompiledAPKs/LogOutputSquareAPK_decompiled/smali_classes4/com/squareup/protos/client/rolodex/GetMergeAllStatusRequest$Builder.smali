.class public final Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetMergeAllStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;",
        "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public job_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 85
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;
    .locals 3

    .line 98
    new-instance v0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;->job_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;

    move-result-object v0

    return-object v0
.end method

.method public job_id(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;->job_id:Ljava/lang/String;

    return-object p0
.end method
