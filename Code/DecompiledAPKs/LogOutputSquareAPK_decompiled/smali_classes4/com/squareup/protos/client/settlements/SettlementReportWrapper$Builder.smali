.class public final Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SettlementReportWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettlementReportWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
        "Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public send_date:Lcom/squareup/protos/common/time/DateTime;

.field public settlement:Lcom/squareup/protos/client/settlements/SettlementReport;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 103
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/settlements/SettlementReportWrapper;
    .locals 4

    .line 124
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    iget-object v2, p0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->send_date:Lcom/squareup/protos/common/time/DateTime;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;-><init>(Lcom/squareup/protos/client/settlements/SettlementReport;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    move-result-object v0

    return-object v0
.end method

.method public send_date(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->send_date:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public settlement(Lcom/squareup/protos/client/settlements/SettlementReport;)Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    return-object p0
.end method
