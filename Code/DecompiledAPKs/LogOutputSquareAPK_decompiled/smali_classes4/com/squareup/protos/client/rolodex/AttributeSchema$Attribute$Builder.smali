.class public final Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AttributeSchema.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

.field public fallback_value:Ljava/lang/String;

.field public is_visible_in_profile:Ljava/lang/Boolean;

.field public key:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

.field public was_boolean_ever_set:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 739
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 10

    .line 794
    new-instance v9, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->fallback_value:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object v6, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->is_visible_in_profile:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->was_boolean_ever_set:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 724
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    return-object v0
.end method

.method public data(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;
    .locals 0

    .line 772
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    return-object p0
.end method

.method public fallback_value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;
    .locals 0

    .line 767
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->fallback_value:Ljava/lang/String;

    return-object p0
.end method

.method public is_visible_in_profile(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;
    .locals 0

    .line 780
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->is_visible_in_profile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public key(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;
    .locals 0

    .line 746
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->key:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;
    .locals 0

    .line 754
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;
    .locals 0

    .line 759
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    return-object p0
.end method

.method public was_boolean_ever_set(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;
    .locals 0

    .line 788
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->was_boolean_ever_set:Ljava/lang/Boolean;

    return-object p0
.end method
