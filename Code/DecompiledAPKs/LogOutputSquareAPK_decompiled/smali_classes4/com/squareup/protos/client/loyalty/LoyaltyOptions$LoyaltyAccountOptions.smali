.class public final Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;
.super Lcom/squareup/wire/Message;
.source "LoyaltyOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/LoyaltyOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoyaltyAccountOptions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$ProtoAdapter_LoyaltyAccountOptions;,
        Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;",
        "Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FORCE_CONTACT_CREATION:Ljava/lang/Boolean;

.field public static final DEFAULT_INCLUDE_ACTIVE_MAPPINGS:Ljava/lang/Boolean;

.field public static final DEFAULT_INCLUDE_CONTACT:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final force_contact_creation:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final include_active_mappings:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final include_contact:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 133
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$ProtoAdapter_LoyaltyAccountOptions;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$ProtoAdapter_LoyaltyAccountOptions;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 137
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->DEFAULT_INCLUDE_ACTIVE_MAPPINGS:Ljava/lang/Boolean;

    .line 139
    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->DEFAULT_INCLUDE_CONTACT:Ljava/lang/Boolean;

    .line 141
    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->DEFAULT_FORCE_CONTACT_CREATION:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 1

    .line 175
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 180
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->include_active_mappings:Ljava/lang/Boolean;

    .line 182
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->include_contact:Ljava/lang/Boolean;

    .line 183
    iput-object p3, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->force_contact_creation:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 199
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 200
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;

    .line 201
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->include_active_mappings:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->include_active_mappings:Ljava/lang/Boolean;

    .line 202
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->include_contact:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->include_contact:Ljava/lang/Boolean;

    .line 203
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->force_contact_creation:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->force_contact_creation:Ljava/lang/Boolean;

    .line 204
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 209
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 211
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->include_active_mappings:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->include_contact:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 214
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->force_contact_creation:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 215
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;
    .locals 2

    .line 188
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;-><init>()V

    .line 189
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->include_active_mappings:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->include_active_mappings:Ljava/lang/Boolean;

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->include_contact:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->include_contact:Ljava/lang/Boolean;

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->force_contact_creation:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->force_contact_creation:Ljava/lang/Boolean;

    .line 192
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 132
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->newBuilder()Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 222
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 223
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->include_active_mappings:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", include_active_mappings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->include_active_mappings:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 224
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->include_contact:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", include_contact="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->include_contact:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 225
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->force_contact_creation:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", force_contact_creation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;->force_contact_creation:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LoyaltyAccountOptions{"

    .line 226
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
