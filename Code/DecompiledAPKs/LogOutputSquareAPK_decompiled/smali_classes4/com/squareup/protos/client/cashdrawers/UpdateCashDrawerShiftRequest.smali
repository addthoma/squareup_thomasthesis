.class public final Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;
.super Lcom/squareup/wire/Message;
.source "UpdateCashDrawerShiftRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$ProtoAdapter_UpdateCashDrawerShiftRequest;,
        Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;",
        "Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CASH_DRAWER_SHIFT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_CLIENT_CASH_DRAWER_SHIFT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CLIENT_UNIQUE_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_MERCHANT_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final cash_drawer_shift_description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final client_cash_drawer_shift_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final client_unique_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.DeviceInfo#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final events:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.CashDrawerShiftEvent#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;"
        }
    .end annotation
.end field

.field public final expected_cash_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final merchant_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$ProtoAdapter_UpdateCashDrawerShiftRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$ProtoAdapter_UpdateCashDrawerShiftRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/List;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;",
            "Lcom/squareup/protos/client/cashdrawers/DeviceInfo;",
            ")V"
        }
    .end annotation

    .line 102
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/List;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/List;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;",
            "Lcom/squareup/protos/client/cashdrawers/DeviceInfo;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 109
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 110
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    .line 111
    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    .line 112
    iput-object p3, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    .line 113
    iput-object p4, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->cash_drawer_shift_description:Ljava/lang/String;

    .line 114
    iput-object p5, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    const-string p1, "events"

    .line 115
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->events:Ljava/util/List;

    .line 116
    iput-object p7, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 136
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 137
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    .line 138
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    .line 140
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    .line 141
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->cash_drawer_shift_description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->cash_drawer_shift_description:Ljava/lang/String;

    .line 142
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    .line 143
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->events:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->events:Ljava/util/List;

    .line 144
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 145
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 150
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 152
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->cash_drawer_shift_description:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->events:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 160
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;
    .locals 2

    .line 121
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;-><init>()V

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->client_unique_key:Ljava/lang/String;

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->merchant_id:Ljava/lang/String;

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->cash_drawer_shift_description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->cash_drawer_shift_description:Ljava/lang/String;

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->events:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->events:Ljava/util/List;

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->newBuilder()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 167
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", client_unique_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", merchant_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", client_cash_drawer_shift_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->cash_drawer_shift_description:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", cash_drawer_shift_description=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", expected_cash_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 173
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->events:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", events="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->events:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 174
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v1, :cond_6

    const-string v1, ", device_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "UpdateCashDrawerShiftRequest{"

    .line 175
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
