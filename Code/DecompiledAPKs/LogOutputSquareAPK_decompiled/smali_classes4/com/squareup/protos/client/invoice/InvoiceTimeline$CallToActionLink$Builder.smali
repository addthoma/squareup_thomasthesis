.class public final Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InvoiceTimeline.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public link_type:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

.field public link_value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 261
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;
    .locals 4

    .line 276
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;->link_type:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;->link_value:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;-><init>(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 256
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    move-result-object v0

    return-object v0
.end method

.method public link_type(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;
    .locals 0

    .line 265
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;->link_type:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

    return-object p0
.end method

.method public link_value(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;
    .locals 0

    .line 270
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;->link_value:Ljava/lang/String;

    return-object p0
.end method
