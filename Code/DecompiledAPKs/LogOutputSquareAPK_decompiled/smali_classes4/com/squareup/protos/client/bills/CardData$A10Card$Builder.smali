.class public final Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData$A10Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CardData$A10Card;",
        "Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public encrypted_reader_data:Lokio/ByteString;

.field public entry_method:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1627
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/CardData$A10Card;
    .locals 4

    .line 1649
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$A10Card;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->encrypted_reader_data:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/CardData$A10Card;-><init>(Lokio/ByteString;Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1622
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$A10Card;

    move-result-object v0

    return-object v0
.end method

.method public encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;
    .locals 0

    .line 1634
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->encrypted_reader_data:Lokio/ByteString;

    return-object p0
.end method

.method public entry_method(Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;)Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1643
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    return-object p0
.end method
