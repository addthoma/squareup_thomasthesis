.class final Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ProtoAdapter_SplitEventDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SplitEventDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 3693
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3710
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;-><init>()V

    .line 3711
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 3712
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 3716
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 3714
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;->child_itemization:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3720
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 3721
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3691
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ProtoAdapter_SplitEventDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3704
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;->child_itemization:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3705
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3691
    check-cast p2, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ProtoAdapter_SplitEventDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;)I
    .locals 3

    .line 3698
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;->child_itemization:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 3699
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 3691
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ProtoAdapter_SplitEventDetails;->encodedSize(Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;
    .locals 2

    .line 3726
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;

    move-result-object p1

    .line 3727
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;->child_itemization:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 3728
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 3729
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 3691
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ProtoAdapter_SplitEventDetails;->redact(Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    move-result-object p1

    return-object p1
.end method
