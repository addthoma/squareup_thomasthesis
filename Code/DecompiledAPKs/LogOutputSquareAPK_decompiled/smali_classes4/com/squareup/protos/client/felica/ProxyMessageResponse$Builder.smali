.class public final Lcom/squareup/protos/client/felica/ProxyMessageResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ProxyMessageResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/felica/ProxyMessageResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/felica/ProxyMessageResponse;",
        "Lcom/squareup/protos/client/felica/ProxyMessageResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public connection_id:Ljava/lang/String;

.field public error:Lcom/squareup/protos/client/felica/Error;

.field public packet_data:Lokio/ByteString;

.field public transaction_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 127
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/felica/ProxyMessageResponse;
    .locals 7

    .line 152
    new-instance v6, Lcom/squareup/protos/client/felica/ProxyMessageResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/felica/ProxyMessageResponse$Builder;->connection_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/felica/ProxyMessageResponse$Builder;->transaction_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/felica/ProxyMessageResponse$Builder;->packet_data:Lokio/ByteString;

    iget-object v4, p0, Lcom/squareup/protos/client/felica/ProxyMessageResponse$Builder;->error:Lcom/squareup/protos/client/felica/Error;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/felica/ProxyMessageResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Lcom/squareup/protos/client/felica/Error;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/ProxyMessageResponse$Builder;->build()Lcom/squareup/protos/client/felica/ProxyMessageResponse;

    move-result-object v0

    return-object v0
.end method

.method public connection_id(Ljava/lang/String;)Lcom/squareup/protos/client/felica/ProxyMessageResponse$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/felica/ProxyMessageResponse$Builder;->connection_id:Ljava/lang/String;

    return-object p0
.end method

.method public error(Lcom/squareup/protos/client/felica/Error;)Lcom/squareup/protos/client/felica/ProxyMessageResponse$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/client/felica/ProxyMessageResponse$Builder;->error:Lcom/squareup/protos/client/felica/Error;

    return-object p0
.end method

.method public packet_data(Lokio/ByteString;)Lcom/squareup/protos/client/felica/ProxyMessageResponse$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/client/felica/ProxyMessageResponse$Builder;->packet_data:Lokio/ByteString;

    return-object p0
.end method

.method public transaction_id(Ljava/lang/String;)Lcom/squareup/protos/client/felica/ProxyMessageResponse$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/felica/ProxyMessageResponse$Builder;->transaction_id:Ljava/lang/String;

    return-object p0
.end method
