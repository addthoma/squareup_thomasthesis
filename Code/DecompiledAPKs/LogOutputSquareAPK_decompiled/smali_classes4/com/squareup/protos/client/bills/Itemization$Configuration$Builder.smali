.class public final Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Configuration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration;",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

.field public category:Lcom/squareup/api/items/MenuCategory;

.field public item_variation_price_money:Lcom/squareup/protos/common/Money;

.field public selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 601
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public backing_type(Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;)Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;
    .locals 0

    .line 610
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/Itemization$Configuration;
    .locals 7

    .line 635
    new-instance v6, Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->category:Lcom/squareup/api/items/MenuCategory;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/Itemization$Configuration;-><init>(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;Lcom/squareup/protos/common/Money;Lcom/squareup/api/items/MenuCategory;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 592
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public category(Lcom/squareup/api/items/MenuCategory;)Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;
    .locals 0

    .line 629
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->category:Lcom/squareup/api/items/MenuCategory;

    return-object p0
.end method

.method public item_variation_price_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;
    .locals 0

    .line 621
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public selected_options(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;)Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;
    .locals 0

    .line 605
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    return-object p0
.end method
