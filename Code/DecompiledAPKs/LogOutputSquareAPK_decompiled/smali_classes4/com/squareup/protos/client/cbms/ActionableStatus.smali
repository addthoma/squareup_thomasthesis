.class public final enum Lcom/squareup/protos/client/cbms/ActionableStatus;
.super Ljava/lang/Enum;
.source "ActionableStatus.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/cbms/ActionableStatus$ProtoAdapter_ActionableStatus;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/cbms/ActionableStatus;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/cbms/ActionableStatus;

.field public static final enum ACTIONABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/cbms/ActionableStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum NOT_ACTIONABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

.field public static final enum NO_ACTION_DISPUTED_AMOUNT_TOO_LOW:Lcom/squareup/protos/client/cbms/ActionableStatus;

.field public static final enum NO_ACTION_REFUNDED:Lcom/squareup/protos/client/cbms/ActionableStatus;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/cbms/ActionableStatus;

.field public static final enum VIEWABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 11
    new-instance v0, Lcom/squareup/protos/client/cbms/ActionableStatus;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/cbms/ActionableStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/ActionableStatus;->UNKNOWN:Lcom/squareup/protos/client/cbms/ActionableStatus;

    .line 13
    new-instance v0, Lcom/squareup/protos/client/cbms/ActionableStatus;

    const/4 v2, 0x1

    const-string v3, "ACTIONABLE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/cbms/ActionableStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/ActionableStatus;->ACTIONABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

    .line 15
    new-instance v0, Lcom/squareup/protos/client/cbms/ActionableStatus;

    const/4 v3, 0x2

    const-string v4, "NOT_ACTIONABLE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/cbms/ActionableStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/ActionableStatus;->NOT_ACTIONABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

    .line 17
    new-instance v0, Lcom/squareup/protos/client/cbms/ActionableStatus;

    const/4 v4, 0x3

    const-string v5, "NO_ACTION_DISPUTED_AMOUNT_TOO_LOW"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/cbms/ActionableStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/ActionableStatus;->NO_ACTION_DISPUTED_AMOUNT_TOO_LOW:Lcom/squareup/protos/client/cbms/ActionableStatus;

    .line 19
    new-instance v0, Lcom/squareup/protos/client/cbms/ActionableStatus;

    const/4 v5, 0x4

    const-string v6, "NO_ACTION_REFUNDED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/cbms/ActionableStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/ActionableStatus;->NO_ACTION_REFUNDED:Lcom/squareup/protos/client/cbms/ActionableStatus;

    .line 24
    new-instance v0, Lcom/squareup/protos/client/cbms/ActionableStatus;

    const/4 v6, 0x5

    const-string v7, "VIEWABLE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/cbms/ActionableStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/ActionableStatus;->VIEWABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/cbms/ActionableStatus;

    .line 10
    sget-object v7, Lcom/squareup/protos/client/cbms/ActionableStatus;->UNKNOWN:Lcom/squareup/protos/client/cbms/ActionableStatus;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->ACTIONABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->NOT_ACTIONABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->NO_ACTION_DISPUTED_AMOUNT_TOO_LOW:Lcom/squareup/protos/client/cbms/ActionableStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->NO_ACTION_REFUNDED:Lcom/squareup/protos/client/cbms/ActionableStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->VIEWABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/cbms/ActionableStatus;->$VALUES:[Lcom/squareup/protos/client/cbms/ActionableStatus;

    .line 26
    new-instance v0, Lcom/squareup/protos/client/cbms/ActionableStatus$ProtoAdapter_ActionableStatus;

    invoke-direct {v0}, Lcom/squareup/protos/client/cbms/ActionableStatus$ProtoAdapter_ActionableStatus;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/cbms/ActionableStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput p3, p0, Lcom/squareup/protos/client/cbms/ActionableStatus;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/cbms/ActionableStatus;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 44
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/cbms/ActionableStatus;->VIEWABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

    return-object p0

    .line 43
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/cbms/ActionableStatus;->NO_ACTION_REFUNDED:Lcom/squareup/protos/client/cbms/ActionableStatus;

    return-object p0

    .line 42
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/cbms/ActionableStatus;->NO_ACTION_DISPUTED_AMOUNT_TOO_LOW:Lcom/squareup/protos/client/cbms/ActionableStatus;

    return-object p0

    .line 41
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/cbms/ActionableStatus;->NOT_ACTIONABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

    return-object p0

    .line 40
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/cbms/ActionableStatus;->ACTIONABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

    return-object p0

    .line 39
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/cbms/ActionableStatus;->UNKNOWN:Lcom/squareup/protos/client/cbms/ActionableStatus;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/cbms/ActionableStatus;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/client/cbms/ActionableStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/cbms/ActionableStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/cbms/ActionableStatus;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/client/cbms/ActionableStatus;->$VALUES:[Lcom/squareup/protos/client/cbms/ActionableStatus;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/cbms/ActionableStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/cbms/ActionableStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 51
    iget v0, p0, Lcom/squareup/protos/client/cbms/ActionableStatus;->value:I

    return v0
.end method
