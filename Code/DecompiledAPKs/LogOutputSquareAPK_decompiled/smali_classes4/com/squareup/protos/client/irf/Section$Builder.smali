.class public final Lcom/squareup/protos/client/irf/Section$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Section.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/irf/Section;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/irf/Section;",
        "Lcom/squareup/protos/client/irf/Section$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public description:Ljava/lang/String;

.field public field:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Field;",
            ">;"
        }
    .end annotation
.end field

.field public header:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 109
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 110
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/irf/Section$Builder;->field:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/irf/Section;
    .locals 5

    .line 131
    new-instance v0, Lcom/squareup/protos/client/irf/Section;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Section$Builder;->header:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/irf/Section$Builder;->description:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/irf/Section$Builder;->field:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/irf/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/Section$Builder;->build()Lcom/squareup/protos/client/irf/Section;

    move-result-object v0

    return-object v0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/client/irf/Section$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Section$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public field(Ljava/util/List;)Lcom/squareup/protos/client/irf/Section$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Field;",
            ">;)",
            "Lcom/squareup/protos/client/irf/Section$Builder;"
        }
    .end annotation

    .line 124
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Section$Builder;->field:Ljava/util/List;

    return-object p0
.end method

.method public header(Ljava/lang/String;)Lcom/squareup/protos/client/irf/Section$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Section$Builder;->header:Ljava/lang/String;

    return-object p0
.end method
