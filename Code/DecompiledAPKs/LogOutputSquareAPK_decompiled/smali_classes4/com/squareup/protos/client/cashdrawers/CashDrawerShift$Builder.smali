.class public final Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CashDrawerShift.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
        "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public business_day_id:Ljava/lang/String;

.field public cash_paid_in_money:Lcom/squareup/protos/common/Money;

.field public cash_paid_out_money:Lcom/squareup/protos/common/Money;

.field public cash_payment_money:Lcom/squareup/protos/common/Money;

.field public cash_refunds_money:Lcom/squareup/protos/common/Money;

.field public client_cash_drawer_shift_id:Ljava/lang/String;

.field public client_virtual_register_id:Ljava/lang/String;

.field public closed_at:Lcom/squareup/protos/client/ISO8601Date;

.field public closed_cash_money:Lcom/squareup/protos/common/Money;

.field public closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

.field public closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

.field public closing_employee_id:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public employee_id:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public ended_at:Lcom/squareup/protos/client/ISO8601Date;

.field public ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

.field public ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

.field public ending_employee_id:Ljava/lang/String;

.field public event_data_omitted:Ljava/lang/Boolean;

.field public events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;"
        }
    .end annotation
.end field

.field public expected_cash_money:Lcom/squareup/protos/common/Money;

.field public merchant_id:Ljava/lang/String;

.field public opened_at:Lcom/squareup/protos/client/ISO8601Date;

.field public opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

.field public opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

.field public opening_employee_id:Ljava/lang/String;

.field public starting_cash_money:Lcom/squareup/protos/common/Money;

.field public state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 507
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 508
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->employee_id:Ljava/util/List;

    .line 509
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->events:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
    .locals 2

    .line 707
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;-><init>(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 450
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object v0

    return-object v0
.end method

.method public business_day_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 645
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->business_day_id:Ljava/lang/String;

    return-object p0
.end method

.method public cash_paid_in_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 605
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public cash_paid_out_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 613
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public cash_payment_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 589
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_payment_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public cash_refunds_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 597
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 524
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    return-object p0
.end method

.method public client_virtual_register_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 640
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->client_virtual_register_id:Ljava/lang/String;

    return-object p0
.end method

.method public closed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 576
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public closed_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 629
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_cash_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public closing_device_info(Lcom/squareup/protos/client/cashdrawers/DeviceInfo;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 669
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    return-object p0
.end method

.method public closing_employee(Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 693
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    return-object p0
.end method

.method public closing_employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 561
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_employee_id:Ljava/lang/String;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 546
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public employee_id(Ljava/util/List;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;"
        }
    .end annotation

    .line 537
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 538
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->employee_id:Ljava/util/List;

    return-object p0
.end method

.method public ended_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 571
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public ending_device_info(Lcom/squareup/protos/client/cashdrawers/DeviceInfo;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 661
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    return-object p0
.end method

.method public ending_employee(Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 685
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    return-object p0
.end method

.method public ending_employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 556
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_employee_id:Ljava/lang/String;

    return-object p0
.end method

.method public event_data_omitted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 701
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->event_data_omitted:Ljava/lang/Boolean;

    return-object p0
.end method

.method public events(Ljava/util/List;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;)",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;"
        }
    .end annotation

    .line 634
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 635
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->events:Ljava/util/List;

    return-object p0
.end method

.method public expected_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 621
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 516
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->merchant_id:Ljava/lang/String;

    return-object p0
.end method

.method public opened_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 566
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public opening_device_info(Lcom/squareup/protos/client/cashdrawers/DeviceInfo;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 653
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    return-object p0
.end method

.method public opening_employee(Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 677
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    return-object p0
.end method

.method public opening_employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 551
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_employee_id:Ljava/lang/String;

    return-object p0
.end method

.method public starting_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 581
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->starting_cash_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public state(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 529
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    return-object p0
.end method
