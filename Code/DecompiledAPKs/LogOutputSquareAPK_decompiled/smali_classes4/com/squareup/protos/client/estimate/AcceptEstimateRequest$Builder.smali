.class public final Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AcceptEstimateRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;",
        "Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public package_token:Ljava/lang/String;

.field public send_email_to_recipients:Ljava/lang/Boolean;

.field public token_pair:Lcom/squareup/protos/client/IdPair;

.field public version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 133
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;
    .locals 7

    .line 164
    new-instance v6, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;->token_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;->version:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;->send_email_to_recipients:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;->package_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;->build()Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;

    move-result-object v0

    return-object v0
.end method

.method public package_token(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;->package_token:Ljava/lang/String;

    return-object p0
.end method

.method public send_email_to_recipients(Ljava/lang/Boolean;)Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;->send_email_to_recipients:Ljava/lang/Boolean;

    return-object p0
.end method

.method public token_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;->token_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public version(Ljava/lang/Integer;)Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;->version:Ljava/lang/Integer;

    return-object p0
.end method
