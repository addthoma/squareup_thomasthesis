.class public final Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetFileAttachmentMetadataResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;",
        "Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attachment:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 92
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public attachment(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;->attachment:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;
    .locals 4

    .line 107
    new-instance v0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;->attachment:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;->build()Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
