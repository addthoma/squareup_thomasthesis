.class public final Lcom/squareup/protos/client/bills/CardData$R4Card;
.super Lcom/squareup/wire/Message;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "R4Card"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardData$R4Card$ProtoAdapter_R4Card;,
        Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/CardData$R4Card;",
        "Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardData$R4Card;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ENCRYPTED_SWIPE_DATA:Lokio/ByteString;

.field private static final serialVersionUID:J


# instance fields
.field public final encrypted_swipe_data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1143
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$R4Card$ProtoAdapter_R4Card;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$R4Card$ProtoAdapter_R4Card;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$R4Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1147
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$R4Card;->DEFAULT_ENCRYPTED_SWIPE_DATA:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;)V
    .locals 1

    .line 1160
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/client/bills/CardData$R4Card;-><init>(Lokio/ByteString;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lokio/ByteString;)V
    .locals 1

    .line 1164
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$R4Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1165
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$R4Card;->encrypted_swipe_data:Lokio/ByteString;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1179
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/CardData$R4Card;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1180
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/CardData$R4Card;

    .line 1181
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$R4Card;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$R4Card;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$R4Card;->encrypted_swipe_data:Lokio/ByteString;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardData$R4Card;->encrypted_swipe_data:Lokio/ByteString;

    .line 1182
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 1187
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 1189
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$R4Card;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1190
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$R4Card;->encrypted_swipe_data:Lokio/ByteString;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 1191
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;
    .locals 2

    .line 1170
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;-><init>()V

    .line 1171
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$R4Card;->encrypted_swipe_data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;->encrypted_swipe_data:Lokio/ByteString;

    .line 1172
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$R4Card;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1142
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$R4Card;->newBuilder()Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1198
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1199
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$R4Card;->encrypted_swipe_data:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", encrypted_swipe_data=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "R4Card{"

    .line 1200
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
