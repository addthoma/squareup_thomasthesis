.class public final Lcom/squareup/protos/client/bills/CardTender$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CardTender;",
        "Lcom/squareup/protos/client/bills/CardTender$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public account_type:Lcom/squareup/protos/client/bills/CardTender$AccountType;

.field public allows_reauthorization_with_changes:Ljava/lang/Boolean;

.field public card:Lcom/squareup/protos/client/bills/CardTender$Card;

.field public emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

.field public read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

.field public read_only_delay_capture:Lcom/squareup/protos/client/bills/CardTender$DelayCapture;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 168
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public account_type(Lcom/squareup/protos/client/bills/CardTender$AccountType;)Lcom/squareup/protos/client/bills/CardTender$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Builder;->account_type:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    return-object p0
.end method

.method public allows_reauthorization_with_changes(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/CardTender$Builder;
    .locals 0

    .line 212
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Builder;->allows_reauthorization_with_changes:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/CardTender;
    .locals 9

    .line 218
    new-instance v8, Lcom/squareup/protos/client/bills/CardTender;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Builder;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CardTender$Builder;->read_only_delay_capture:Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/CardTender$Builder;->read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/CardTender$Builder;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/CardTender$Builder;->account_type:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/CardTender$Builder;->allows_reauthorization_with_changes:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/CardTender;-><init>(Lcom/squareup/protos/client/bills/CardTender$Card;Lcom/squareup/protos/client/bills/CardTender$DelayCapture;Lcom/squareup/protos/client/bills/CardTender$Authorization;Lcom/squareup/protos/client/bills/CardTender$Emv;Lcom/squareup/protos/client/bills/CardTender$AccountType;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 155
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Builder;->build()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object v0

    return-object v0
.end method

.method public card(Lcom/squareup/protos/client/bills/CardTender$Card;)Lcom/squareup/protos/client/bills/CardTender$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Builder;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    return-object p0
.end method

.method public emv(Lcom/squareup/protos/client/bills/CardTender$Emv;)Lcom/squareup/protos/client/bills/CardTender$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Builder;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    return-object p0
.end method

.method public read_only_authorization(Lcom/squareup/protos/client/bills/CardTender$Authorization;)Lcom/squareup/protos/client/bills/CardTender$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Builder;->read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

    return-object p0
.end method

.method public read_only_delay_capture(Lcom/squareup/protos/client/bills/CardTender$DelayCapture;)Lcom/squareup/protos/client/bills/CardTender$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Builder;->read_only_delay_capture:Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    return-object p0
.end method
