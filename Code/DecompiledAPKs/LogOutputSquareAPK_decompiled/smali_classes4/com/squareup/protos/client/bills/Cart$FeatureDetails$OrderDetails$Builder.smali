.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public fulfillment_creation_details:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;",
            ">;"
        }
    .end annotation
.end field

.field public order_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 6650
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 6651
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$Builder;->fulfillment_creation_details:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;
    .locals 4

    .line 6677
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$Builder;->order_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$Builder;->fulfillment_creation_details:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;-><init>(Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 6645
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    move-result-object v0

    return-object v0
.end method

.method public fulfillment_creation_details(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$Builder;"
        }
    .end annotation

    .line 6670
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 6671
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$Builder;->fulfillment_creation_details:Ljava/util/List;

    return-object p0
.end method

.method public order_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$Builder;
    .locals 0

    .line 6658
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$Builder;->order_id:Ljava/lang/String;

    return-object p0
.end method
