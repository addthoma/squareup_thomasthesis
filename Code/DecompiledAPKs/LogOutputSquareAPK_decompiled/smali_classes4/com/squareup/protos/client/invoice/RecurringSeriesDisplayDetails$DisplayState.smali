.class public final enum Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;
.super Ljava/lang/Enum;
.source "RecurringSeriesDisplayDetails.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DisplayState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState$ProtoAdapter_DisplayState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

.field public static final enum ACTIVE:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DRAFT:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

.field public static final enum ENDED:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

.field public static final enum UNUSED:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 238
    new-instance v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    const/4 v1, 0x0

    const-string v2, "UNUSED"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->UNUSED:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    .line 240
    new-instance v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    const/4 v2, 0x1

    const-string v3, "DRAFT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->DRAFT:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    .line 242
    new-instance v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    const/4 v3, 0x2

    const-string v4, "ACTIVE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ACTIVE:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    .line 244
    new-instance v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    const/4 v4, 0x3

    const-string v5, "ENDED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ENDED:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    .line 237
    sget-object v5, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->UNUSED:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->DRAFT:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ACTIVE:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ENDED:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->$VALUES:[Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    .line 246
    new-instance v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState$ProtoAdapter_DisplayState;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState$ProtoAdapter_DisplayState;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 250
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 251
    iput p3, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 262
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ENDED:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    return-object p0

    .line 261
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ACTIVE:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    return-object p0

    .line 260
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->DRAFT:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    return-object p0

    .line 259
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->UNUSED:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;
    .locals 1

    .line 237
    const-class v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;
    .locals 1

    .line 237
    sget-object v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->$VALUES:[Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 269
    iget v0, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->value:I

    return v0
.end method
