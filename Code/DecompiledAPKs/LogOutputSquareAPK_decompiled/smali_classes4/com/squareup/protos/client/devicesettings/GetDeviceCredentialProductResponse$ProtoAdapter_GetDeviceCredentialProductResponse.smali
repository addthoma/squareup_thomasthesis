.class final Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$ProtoAdapter_GetDeviceCredentialProductResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetDeviceCredentialProductResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetDeviceCredentialProductResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 96
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 114
    new-instance v0, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$Builder;-><init>()V

    .line 115
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 116
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 127
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 120
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/Device$Product;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/Device$Product;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$Builder;->product(Lcom/squareup/protos/client/Device$Product;)Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 122
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 131
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 132
    invoke-virtual {v0}, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$Builder;->build()Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 94
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$ProtoAdapter_GetDeviceCredentialProductResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 108
    sget-object v0, Lcom/squareup/protos/client/Device$Product;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;->product:Lcom/squareup/protos/client/Device$Product;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 109
    invoke-virtual {p2}, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 94
    check-cast p2, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$ProtoAdapter_GetDeviceCredentialProductResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;)I
    .locals 3

    .line 101
    sget-object v0, Lcom/squareup/protos/client/Device$Product;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;->product:Lcom/squareup/protos/client/Device$Product;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 102
    invoke-virtual {p1}, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 94
    check-cast p1, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$ProtoAdapter_GetDeviceCredentialProductResponse;->encodedSize(Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;)Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;
    .locals 0

    .line 137
    invoke-virtual {p1}, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;->newBuilder()Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$Builder;

    move-result-object p1

    .line 138
    invoke-virtual {p1}, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 139
    invoke-virtual {p1}, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$Builder;->build()Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 94
    check-cast p1, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$ProtoAdapter_GetDeviceCredentialProductResponse;->redact(Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;)Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;

    move-result-object p1

    return-object p1
.end method
