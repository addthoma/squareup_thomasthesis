.class public final Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;
.super Lcom/squareup/wire/Message;
.source "SubmitTipAndSettleBatchResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse$ProtoAdapter_SubmitTipAndSettleBatchResponse;,
        Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;",
        "Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ERROR_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_ERROR_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_SUCCESS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final error_message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final error_title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final success:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse$ProtoAdapter_SubmitTipAndSettleBatchResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse$ProtoAdapter_SubmitTipAndSettleBatchResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 64
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 69
    sget-object v0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 70
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->success:Ljava/lang/Boolean;

    .line 71
    iput-object p2, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->error_message:Ljava/lang/String;

    .line 72
    iput-object p3, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->error_title:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 88
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 89
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->success:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->success:Ljava/lang/Boolean;

    .line 91
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->error_message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->error_message:Ljava/lang/String;

    .line 92
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->error_title:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->error_title:Ljava/lang/String;

    .line 93
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 98
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->error_title:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 104
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse$Builder;
    .locals 2

    .line 77
    new-instance v0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse$Builder;-><init>()V

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->success:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse$Builder;->success:Ljava/lang/Boolean;

    .line 79
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->error_message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse$Builder;->error_message:Ljava/lang/String;

    .line 80
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->error_title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse$Builder;->error_title:Ljava/lang/String;

    .line 81
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->newBuilder()Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->success:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", error_message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->error_message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->error_title:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", error_title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->error_title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SubmitTipAndSettleBatchResponse{"

    .line 115
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
