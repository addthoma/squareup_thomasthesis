.class final Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$ProtoAdapter_LoyaltyStatus;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AddedTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_LoyaltyStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1135
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1156
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;-><init>()V

    .line 1157
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1158
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 1171
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1164
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->type(Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;)Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 1166
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1161
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->is_enrolled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;

    goto :goto_0

    .line 1160
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->punch_status(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;)Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;

    goto :goto_0

    .line 1175
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1176
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->build()Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1133
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$ProtoAdapter_LoyaltyStatus;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1148
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1149
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->is_enrolled:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1150
    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->type:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1151
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1133
    check-cast p2, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$ProtoAdapter_LoyaltyStatus;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;)I
    .locals 4

    .line 1140
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->is_enrolled:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 1141
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->type:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    const/4 v3, 0x3

    .line 1142
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1143
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1133
    check-cast p1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$ProtoAdapter_LoyaltyStatus;->encodedSize(Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;)Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;
    .locals 2

    .line 1181
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->newBuilder()Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;

    move-result-object p1

    .line 1182
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    .line 1183
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1184
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->build()Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1133
    check-cast p1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$ProtoAdapter_LoyaltyStatus;->redact(Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;)Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;

    move-result-object p1

    return-object p1
.end method
