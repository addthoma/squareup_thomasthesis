.class public final Lcom/squareup/protos/client/coupons/CouponCondition$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CouponCondition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/coupons/CouponCondition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/coupons/CouponCondition;",
        "Lcom/squareup/protos/client/coupons/CouponCondition$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public item_constraint:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/ItemConstraint;",
            ">;"
        }
    .end annotation
.end field

.field public min_spend_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 101
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 102
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/coupons/CouponCondition$Builder;->item_constraint:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/coupons/CouponCondition;
    .locals 4

    .line 125
    new-instance v0, Lcom/squareup/protos/client/coupons/CouponCondition;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponCondition$Builder;->item_constraint:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/coupons/CouponCondition$Builder;->min_spend_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/coupons/CouponCondition;-><init>(Ljava/util/List;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/CouponCondition$Builder;->build()Lcom/squareup/protos/client/coupons/CouponCondition;

    move-result-object v0

    return-object v0
.end method

.method public item_constraint(Ljava/util/List;)Lcom/squareup/protos/client/coupons/CouponCondition$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/ItemConstraint;",
            ">;)",
            "Lcom/squareup/protos/client/coupons/CouponCondition$Builder;"
        }
    .end annotation

    .line 110
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/CouponCondition$Builder;->item_constraint:Ljava/util/List;

    return-object p0
.end method

.method public min_spend_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/coupons/CouponCondition$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/CouponCondition$Builder;->min_spend_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
