.class public final Lcom/squareup/protos/client/coupons/Coupon;
.super Lcom/squareup/wire/Message;
.source "Coupon.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/coupons/Coupon$ProtoAdapter_Coupon;,
        Lcom/squareup/protos/client/coupons/Coupon$Reason;,
        Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;,
        Lcom/squareup/protos/client/coupons/Coupon$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "Lcom/squareup/protos/client/coupons/Coupon$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_COUPON_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_IMAGE_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_ITEM_CONSTRAINT_TYPE:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

.field public static final DEFAULT_REASON:Lcom/squareup/protos/client/coupons/Coupon$Reason;

.field private static final serialVersionUID:J


# instance fields
.field public final code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final condition:Lcom/squareup/protos/client/coupons/CouponCondition;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.coupons.CouponCondition#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final coupon_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final created_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final discount:Lcom/squareup/api/items/Discount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Discount#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final expires_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final image_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.coupons.Coupon$ItemConstraintType#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.coupons.LoyaltyPoints#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final reason:Lcom/squareup/protos/client/coupons/Coupon$Reason;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.coupons.Coupon$Reason#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final reward:Lcom/squareup/protos/client/coupons/CouponReward;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.coupons.CouponReward#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/coupons/Coupon$ProtoAdapter_Coupon;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/Coupon$ProtoAdapter_Coupon;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/coupons/Coupon;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->UNKNOWN:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    sput-object v0, Lcom/squareup/protos/client/coupons/Coupon;->DEFAULT_ITEM_CONSTRAINT_TYPE:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    .line 34
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->UNKNOWN_REASON:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    sput-object v0, Lcom/squareup/protos/client/coupons/Coupon;->DEFAULT_REASON:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/items/Discount;Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;Ljava/lang/String;Lcom/squareup/protos/client/coupons/CouponCondition;Lcom/squareup/protos/client/coupons/CouponReward;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/coupons/LoyaltyPoints;Lcom/squareup/protos/client/coupons/Coupon$Reason;Ljava/lang/String;)V
    .locals 13

    .line 134
    sget-object v12, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/client/coupons/Coupon;-><init>(Ljava/lang/String;Lcom/squareup/api/items/Discount;Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;Ljava/lang/String;Lcom/squareup/protos/client/coupons/CouponCondition;Lcom/squareup/protos/client/coupons/CouponReward;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/coupons/LoyaltyPoints;Lcom/squareup/protos/client/coupons/Coupon$Reason;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/items/Discount;Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;Ljava/lang/String;Lcom/squareup/protos/client/coupons/CouponCondition;Lcom/squareup/protos/client/coupons/CouponReward;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/coupons/LoyaltyPoints;Lcom/squareup/protos/client/coupons/Coupon$Reason;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 141
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p12}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    .line 143
    iput-object p2, p0, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    .line 144
    iput-object p3, p0, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    .line 145
    iput-object p4, p0, Lcom/squareup/protos/client/coupons/Coupon;->image_url:Ljava/lang/String;

    .line 146
    iput-object p5, p0, Lcom/squareup/protos/client/coupons/Coupon;->condition:Lcom/squareup/protos/client/coupons/CouponCondition;

    .line 147
    iput-object p6, p0, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    .line 148
    iput-object p7, p0, Lcom/squareup/protos/client/coupons/Coupon;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 149
    iput-object p8, p0, Lcom/squareup/protos/client/coupons/Coupon;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 150
    iput-object p9, p0, Lcom/squareup/protos/client/coupons/Coupon;->loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    .line 151
    iput-object p10, p0, Lcom/squareup/protos/client/coupons/Coupon;->reason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    .line 152
    iput-object p11, p0, Lcom/squareup/protos/client/coupons/Coupon;->code:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 176
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/coupons/Coupon;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 177
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/coupons/Coupon;

    .line 178
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/Coupon;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/Coupon;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    .line 179
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    .line 180
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    .line 181
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->image_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/Coupon;->image_url:Ljava/lang/String;

    .line 182
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->condition:Lcom/squareup/protos/client/coupons/CouponCondition;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/Coupon;->condition:Lcom/squareup/protos/client/coupons/CouponCondition;

    .line 183
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    .line 184
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/Coupon;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 185
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/Coupon;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 186
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/Coupon;->loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    .line 187
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->reason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/Coupon;->reason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    .line 188
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->code:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/coupons/Coupon;->code:Ljava/lang/String;

    .line 189
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 194
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_b

    .line 196
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/Coupon;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/items/Discount;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->image_url:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->condition:Lcom/squareup/protos/client/coupons/CouponCondition;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/coupons/CouponCondition;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/coupons/CouponReward;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/coupons/LoyaltyPoints;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->reason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/client/coupons/Coupon$Reason;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->code:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    .line 208
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_b
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/coupons/Coupon$Builder;
    .locals 2

    .line 157
    new-instance v0, Lcom/squareup/protos/client/coupons/Coupon$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/Coupon$Builder;-><init>()V

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->coupon_id:Ljava/lang/String;

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->discount:Lcom/squareup/api/items/Discount;

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->image_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->image_url:Ljava/lang/String;

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->condition:Lcom/squareup/protos/client/coupons/CouponCondition;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->condition:Lcom/squareup/protos/client/coupons/CouponCondition;

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 165
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 166
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    .line 167
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->reason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->reason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->code:Ljava/lang/String;

    .line 169
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/Coupon;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/Coupon;->newBuilder()Lcom/squareup/protos/client/coupons/Coupon$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", coupon_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    if-eqz v1, :cond_1

    const-string v1, ", discount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 218
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    if-eqz v1, :cond_2

    const-string v1, ", item_constraint_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 219
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->image_url:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", image_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->image_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->condition:Lcom/squareup/protos/client/coupons/CouponCondition;

    if-eqz v1, :cond_4

    const-string v1, ", condition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->condition:Lcom/squareup/protos/client/coupons/CouponCondition;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 221
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    if-eqz v1, :cond_5

    const-string v1, ", reward="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 222
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_6

    const-string v1, ", expires_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 223
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_7

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 224
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    if-eqz v1, :cond_8

    const-string v1, ", loyalty_points="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 225
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->reason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    if-eqz v1, :cond_9

    const-string v1, ", reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->reason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 226
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->code:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon;->code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Coupon{"

    .line 227
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
