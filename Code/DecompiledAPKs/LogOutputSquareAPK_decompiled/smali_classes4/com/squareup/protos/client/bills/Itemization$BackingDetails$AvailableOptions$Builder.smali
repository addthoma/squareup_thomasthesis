.class public final Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;",
        "Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public item_variation:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field public modifier_list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 2110
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 2111
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->item_variation:Ljava/util/List;

    .line 2112
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->modifier_list:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;
    .locals 4

    .line 2135
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->item_variation:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->modifier_list:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;-><init>(Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2105
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    move-result-object v0

    return-object v0
.end method

.method public item_variation(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemVariation;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;"
        }
    .end annotation

    .line 2119
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 2120
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->item_variation:Ljava/util/List;

    return-object p0
.end method

.method public modifier_list(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;"
        }
    .end annotation

    .line 2128
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 2129
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->modifier_list:Ljava/util/List;

    return-object p0
.end method
