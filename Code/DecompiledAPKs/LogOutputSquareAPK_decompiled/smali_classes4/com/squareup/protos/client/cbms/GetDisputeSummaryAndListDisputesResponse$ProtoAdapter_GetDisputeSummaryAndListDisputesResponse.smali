.class final Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$ProtoAdapter_GetDisputeSummaryAndListDisputesResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetDisputeSummaryAndListDisputesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetDisputeSummaryAndListDisputesResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 248
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 280
    new-instance v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;-><init>()V

    .line 281
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 282
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 293
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 291
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->resolved_dispute_cursor(Ljava/lang/Integer;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;

    goto :goto_0

    .line 290
    :pswitch_1
    iget-object v3, v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->resolved_dispute:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/cbms/DisputedPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 289
    :pswitch_2
    iget-object v3, v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->unresolved_dispute:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/cbms/DisputedPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 288
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_covered_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;

    goto :goto_0

    .line 287
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_disputed_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;

    goto :goto_0

    .line 286
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_disputes_count(Ljava/lang/Long;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;

    goto :goto_0

    .line 285
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->actionable_disputes_count(Ljava/lang/Long;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;

    goto :goto_0

    .line 284
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;

    goto :goto_0

    .line 297
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 298
    invoke-virtual {v0}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->build()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 246
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$ProtoAdapter_GetDisputeSummaryAndListDisputesResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 267
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 268
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->actionable_disputes_count:Ljava/lang/Long;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 269
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputes_count:Ljava/lang/Long;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 270
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputed_amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 271
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_covered_amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 272
    sget-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unresolved_dispute:Ljava/util/List;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 273
    sget-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute:Ljava/util/List;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 274
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute_cursor:Ljava/lang/Integer;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 275
    invoke-virtual {p2}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 246
    check-cast p2, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$ProtoAdapter_GetDisputeSummaryAndListDisputesResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;)I
    .locals 4

    .line 253
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->actionable_disputes_count:Ljava/lang/Long;

    const/4 v3, 0x2

    .line 254
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputes_count:Ljava/lang/Long;

    const/4 v3, 0x3

    .line 255
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputed_amount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x4

    .line 256
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_covered_amount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x5

    .line 257
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 258
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unresolved_dispute:Ljava/util/List;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 259
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute:Ljava/util/List;

    const/4 v3, 0x7

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute_cursor:Ljava/lang/Integer;

    const/16 v3, 0x8

    .line 260
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    invoke-virtual {p1}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 246
    check-cast p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$ProtoAdapter_GetDisputeSummaryAndListDisputesResponse;->encodedSize(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;
    .locals 2

    .line 304
    invoke-virtual {p1}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->newBuilder()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;

    move-result-object p1

    .line 305
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Status;

    iput-object v0, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 306
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_disputed_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_disputed_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_disputed_amount:Lcom/squareup/protos/common/Money;

    .line 307
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_covered_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_covered_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_covered_amount:Lcom/squareup/protos/common/Money;

    .line 308
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->unresolved_dispute:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 309
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->resolved_dispute:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 310
    invoke-virtual {p1}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 311
    invoke-virtual {p1}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->build()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 246
    check-cast p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$ProtoAdapter_GetDisputeSummaryAndListDisputesResponse;->redact(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object p1

    return-object p1
.end method
