.class public final Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;
.super Lcom/squareup/wire/Message;
.source "GetLoyaltyReportResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AggregateCount"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$ProtoAdapter_AggregateCount;,
        Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COHORT_TYPE:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

.field public static final DEFAULT_COUNT:Ljava/lang/Double;

.field private static final serialVersionUID:J


# instance fields
.field public final cohort_type:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.GetLoyaltyReportResponse$AggregateCohortType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final count:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#DOUBLE"
        tag = 0x2
    .end annotation
.end field

.field public final money_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 448
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$ProtoAdapter_AggregateCount;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$ProtoAdapter_AggregateCount;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 452
    sget-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->COHORT_ALL:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    sput-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->DEFAULT_COHORT_TYPE:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    const-wide/16 v0, 0x0

    .line 454
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->DEFAULT_COUNT:Ljava/lang/Double;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;Ljava/lang/Double;Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 475
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;-><init>(Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;Ljava/lang/Double;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;Ljava/lang/Double;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 480
    sget-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 481
    invoke-static {p2, p3}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p4

    const/4 v0, 0x1

    if-gt p4, v0, :cond_0

    .line 484
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->cohort_type:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    .line 485
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->count:Ljava/lang/Double;

    .line 486
    iput-object p3, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->money_amount:Lcom/squareup/protos/common/Money;

    return-void

    .line 482
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of count, money_amount may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 502
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 503
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;

    .line 504
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->cohort_type:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->cohort_type:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    .line 505
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->count:Ljava/lang/Double;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->count:Ljava/lang/Double;

    .line 506
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->money_amount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->money_amount:Lcom/squareup/protos/common/Money;

    .line 507
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 512
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 514
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 515
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->cohort_type:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 516
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->count:Ljava/lang/Double;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Double;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 517
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->money_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 518
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;
    .locals 2

    .line 491
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;-><init>()V

    .line 492
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->cohort_type:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;->cohort_type:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    .line 493
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->count:Ljava/lang/Double;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;->count:Ljava/lang/Double;

    .line 494
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->money_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;->money_amount:Lcom/squareup/protos/common/Money;

    .line 495
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 447
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->newBuilder()Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 525
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 526
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->cohort_type:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    if-eqz v1, :cond_0

    const-string v1, ", cohort_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->cohort_type:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 527
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->count:Ljava/lang/Double;

    if-eqz v1, :cond_1

    const-string v1, ", count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->count:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 528
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->money_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", money_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->money_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AggregateCount{"

    .line 529
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
