.class final Lcom/squareup/protos/client/bills/Refund$Destination$ProtoAdapter_Destination;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Refund.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Refund$Destination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Destination"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Refund$Destination;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 905
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Refund$Destination;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Refund$Destination;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 922
    new-instance v0, Lcom/squareup/protos/client/bills/Refund$Destination$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Refund$Destination$Builder;-><init>()V

    .line 923
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 924
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 928
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 926
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Refund$Destination$Builder;->gift_card_destination(Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;)Lcom/squareup/protos/client/bills/Refund$Destination$Builder;

    goto :goto_0

    .line 932
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Refund$Destination$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 933
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Refund$Destination$Builder;->build()Lcom/squareup/protos/client/bills/Refund$Destination;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 903
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Refund$Destination$ProtoAdapter_Destination;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Refund$Destination;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Refund$Destination;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 916
    sget-object v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Refund$Destination;->gift_card_destination:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 917
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Refund$Destination;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 903
    check-cast p2, Lcom/squareup/protos/client/bills/Refund$Destination;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Refund$Destination$ProtoAdapter_Destination;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Refund$Destination;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Refund$Destination;)I
    .locals 3

    .line 910
    sget-object v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Refund$Destination;->gift_card_destination:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 911
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Refund$Destination;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 903
    check-cast p1, Lcom/squareup/protos/client/bills/Refund$Destination;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Refund$Destination$ProtoAdapter_Destination;->encodedSize(Lcom/squareup/protos/client/bills/Refund$Destination;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Refund$Destination;)Lcom/squareup/protos/client/bills/Refund$Destination;
    .locals 2

    .line 938
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Refund$Destination;->newBuilder()Lcom/squareup/protos/client/bills/Refund$Destination$Builder;

    move-result-object p1

    .line 939
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Refund$Destination$Builder;->gift_card_destination:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Refund$Destination$Builder;->gift_card_destination:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Refund$Destination$Builder;->gift_card_destination:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;

    .line 940
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Refund$Destination$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 941
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Refund$Destination$Builder;->build()Lcom/squareup/protos/client/bills/Refund$Destination;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 903
    check-cast p1, Lcom/squareup/protos/client/bills/Refund$Destination;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Refund$Destination$ProtoAdapter_Destination;->redact(Lcom/squareup/protos/client/bills/Refund$Destination;)Lcom/squareup/protos/client/bills/Refund$Destination;

    move-result-object p1

    return-object p1
.end method
