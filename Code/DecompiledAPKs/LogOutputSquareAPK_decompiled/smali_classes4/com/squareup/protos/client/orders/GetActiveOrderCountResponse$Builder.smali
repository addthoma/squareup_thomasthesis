.class public final Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetActiveOrderCountResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;",
        "Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public count:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 83
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;
    .locals 3

    .line 96
    new-instance v0, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$Builder;->count:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;-><init>(Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 80
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$Builder;->build()Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;

    move-result-object v0

    return-object v0
.end method

.method public count(Ljava/lang/Integer;)Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$Builder;
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$Builder;->count:Ljava/lang/Integer;

    return-object p0
.end method
