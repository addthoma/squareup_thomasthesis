.class public final enum Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;
.super Ljava/lang/Enum;
.source "BillProcessingResult.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BILL:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

.field public static final enum LEGACY_PAYMENT:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

.field public static final enum TYPE_UNKNOWN:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 194
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    const/4 v1, 0x0

    const-string v2, "TYPE_UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->TYPE_UNKNOWN:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    .line 196
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    const/4 v2, 0x1

    const-string v3, "LEGACY_PAYMENT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->LEGACY_PAYMENT:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    .line 198
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    const/4 v3, 0x2

    const-string v4, "BILL"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->BILL:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    .line 190
    sget-object v4, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->TYPE_UNKNOWN:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->LEGACY_PAYMENT:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->BILL:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->$VALUES:[Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    .line 200
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 204
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 205
    iput p3, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 215
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->BILL:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    return-object p0

    .line 214
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->LEGACY_PAYMENT:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    return-object p0

    .line 213
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->TYPE_UNKNOWN:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;
    .locals 1

    .line 190
    const-class v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;
    .locals 1

    .line 190
    sget-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->$VALUES:[Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 222
    iget v0, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->value:I

    return v0
.end method
