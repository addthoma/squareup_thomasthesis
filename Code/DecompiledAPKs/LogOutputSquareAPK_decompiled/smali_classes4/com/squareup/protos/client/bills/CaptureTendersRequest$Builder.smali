.class public final Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CaptureTendersRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CaptureTendersRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CaptureTendersRequest;",
        "Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_id_pair:Lcom/squareup/protos/client/IdPair;

.field public cart:Lcom/squareup/protos/client/bills/Cart;

.field public dates:Lcom/squareup/protos/client/bills/Bill$Dates;

.field public merchant:Lcom/squareup/protos/client/Merchant;

.field public square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

.field public tender_group_token:Ljava/lang/String;

.field public tenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/CompleteTender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 193
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 194
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->tenders:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/CaptureTendersRequest;
    .locals 10

    .line 263
    new-instance v9, Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->tenders:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->tender_group_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/CaptureTendersRequest;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;Lcom/squareup/protos/client/bills/Bill$Dates;Lcom/squareup/protos/client/bills/SquareProductAttributes;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 178
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->build()Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    move-result-object v0

    return-object v0
.end method

.method public cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;
    .locals 0

    .line 247
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object p0
.end method

.method public dates(Lcom/squareup/protos/client/bills/Bill$Dates;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;
    .locals 0

    .line 226
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    return-object p0
.end method

.method public merchant(Lcom/squareup/protos/client/Merchant;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;
    .locals 0

    .line 207
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    return-object p0
.end method

.method public square_product_attributes(Lcom/squareup/protos/client/bills/SquareProductAttributes;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    return-object p0
.end method

.method public tender_group_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;
    .locals 0

    .line 257
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->tender_group_token:Ljava/lang/String;

    return-object p0
.end method

.method public tenders(Ljava/util/List;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/CompleteTender;",
            ">;)",
            "Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;"
        }
    .end annotation

    .line 216
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 217
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->tenders:Ljava/util/List;

    return-object p0
.end method
