.class final Lcom/squareup/protos/client/devicesettings/TenderSettings$ProtoAdapter_TenderSettings;
.super Lcom/squareup/wire/ProtoAdapter;
.source "TenderSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/devicesettings/TenderSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_TenderSettings"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 372
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/devicesettings/TenderSettings;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 393
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;-><init>()V

    .line 394
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 395
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 401
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 399
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 398
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 397
    :cond_2
    iget-object v3, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 405
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 406
    invoke-virtual {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->build()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 370
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$ProtoAdapter_TenderSettings;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/devicesettings/TenderSettings;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 385
    sget-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 386
    sget-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 387
    sget-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 388
    invoke-virtual {p2}, Lcom/squareup/protos/client/devicesettings/TenderSettings;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 370
    check-cast p2, Lcom/squareup/protos/client/devicesettings/TenderSettings;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/devicesettings/TenderSettings$ProtoAdapter_TenderSettings;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/devicesettings/TenderSettings;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/devicesettings/TenderSettings;)I
    .locals 4

    .line 377
    sget-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 378
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 379
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 380
    invoke-virtual {p1}, Lcom/squareup/protos/client/devicesettings/TenderSettings;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 370
    check-cast p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$ProtoAdapter_TenderSettings;->encodedSize(Lcom/squareup/protos/client/devicesettings/TenderSettings;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 2

    .line 411
    invoke-virtual {p1}, Lcom/squareup/protos/client/devicesettings/TenderSettings;->newBuilder()Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    move-result-object p1

    .line 412
    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 413
    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 414
    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 415
    invoke-virtual {p1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 416
    invoke-virtual {p1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->build()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 370
    check-cast p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$ProtoAdapter_TenderSettings;->redact(Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p1

    return-object p1
.end method
