.class public final Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;",
        "Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public modifier_list:Lcom/squareup/api/items/ItemModifierList;

.field public modifier_option:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemModifierOption;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 2213
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 2214
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->modifier_option:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;
    .locals 4

    .line 2230
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->modifier_option:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;-><init>(Lcom/squareup/api/items/ItemModifierList;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2208
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;

    move-result-object v0

    return-object v0
.end method

.method public modifier_list(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;
    .locals 0

    .line 2218
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    return-object p0
.end method

.method public modifier_option(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemModifierOption;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;"
        }
    .end annotation

    .line 2223
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 2224
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->modifier_option:Ljava/util/List;

    return-object p0
.end method
