.class public final Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListBreaksForOpenTimecardsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;",
        "Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public employee_token:Ljava/lang/String;

.field public include_closed_breaks:Ljava/lang/Boolean;

.field public limit:Ljava/lang/Integer;

.field public merchant_token:Ljava/lang/String;

.field public pagination_token:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 168
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;
    .locals 9

    .line 208
    new-instance v8, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->employee_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->limit:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->pagination_token:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->include_closed_breaks:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 155
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->build()Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;

    move-result-object v0

    return-object v0
.end method

.method public employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->employee_token:Ljava/lang/String;

    return-object p0
.end method

.method public include_closed_breaks(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;
    .locals 0

    .line 202
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->include_closed_breaks:Ljava/lang/Boolean;

    return-object p0
.end method

.method public limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;
    .locals 0

    .line 192
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public pagination_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->pagination_token:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
