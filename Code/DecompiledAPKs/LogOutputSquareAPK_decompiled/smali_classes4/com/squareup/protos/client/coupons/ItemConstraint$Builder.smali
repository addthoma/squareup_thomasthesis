.class public final Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ItemConstraint.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/coupons/ItemConstraint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/coupons/ItemConstraint;",
        "Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public constraint_id:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public quantity:Ljava/lang/Integer;

.field public type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 122
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 123
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;->constraint_id:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/coupons/ItemConstraint;
    .locals 5

    .line 154
    new-instance v0, Lcom/squareup/protos/client/coupons/ItemConstraint;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;->constraint_id:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;->quantity:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;->type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/coupons/ItemConstraint;-><init>(Ljava/util/List;Ljava/lang/Integer;Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 115
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;->build()Lcom/squareup/protos/client/coupons/ItemConstraint;

    move-result-object v0

    return-object v0
.end method

.method public constraint_id(Ljava/util/List;)Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;"
        }
    .end annotation

    .line 130
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;->constraint_id:Ljava/util/List;

    return-object p0
.end method

.method public quantity(Ljava/lang/Integer;)Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;->quantity:Ljava/lang/Integer;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;)Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;->type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    return-object p0
.end method
