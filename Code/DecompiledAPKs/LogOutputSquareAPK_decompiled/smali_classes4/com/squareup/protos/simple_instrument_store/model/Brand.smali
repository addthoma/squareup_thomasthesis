.class public final enum Lcom/squareup/protos/simple_instrument_store/model/Brand;
.super Ljava/lang/Enum;
.source "Brand.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/simple_instrument_store/model/Brand$ProtoAdapter_Brand;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/simple_instrument_store/model/Brand;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/simple_instrument_store/model/Brand;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALIPAY:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final enum AMEX:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final enum CASH_APP:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final enum DISCOVER:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final enum DISCOVER_DINERS:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final enum EBT:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final enum EFTPOS:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final enum FELICA:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final enum INTERAC:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final enum INVALID:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final enum JCB:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final enum MASTERCARD:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final enum SQUARE_CAPITAL_CARD:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final enum SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final enum UNION_PAY:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final enum UNKNOWN:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final enum VISA:Lcom/squareup/protos/simple_instrument_store/model/Brand;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 16
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/4 v1, 0x0

    const-string v2, "INVALID"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->INVALID:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 18
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/4 v2, 0x1

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->UNKNOWN:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 20
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/4 v3, 0x2

    const-string v4, "VISA"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->VISA:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 22
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/4 v4, 0x3

    const-string v5, "MASTERCARD"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->MASTERCARD:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 24
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/4 v5, 0x4

    const-string v6, "AMEX"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->AMEX:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 26
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/4 v6, 0x5

    const-string v7, "DISCOVER"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->DISCOVER:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 28
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/4 v7, 0x6

    const-string v8, "DISCOVER_DINERS"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->DISCOVER_DINERS:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 30
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/4 v8, 0x7

    const-string v9, "JCB"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->JCB:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 32
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/16 v9, 0x8

    const-string v10, "UNION_PAY"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->UNION_PAY:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 34
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/16 v10, 0x9

    const-string v11, "SQUARE_GIFT_CARD_V2"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 36
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/16 v11, 0xa

    const-string v12, "INTERAC"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->INTERAC:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 38
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/16 v12, 0xb

    const-string v13, "SQUARE_CAPITAL_CARD"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 40
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/16 v13, 0xc

    const-string v14, "EFTPOS"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->EFTPOS:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 42
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/16 v14, 0xd

    const-string v15, "FELICA"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->FELICA:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 44
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/16 v15, 0xe

    const-string v14, "ALIPAY"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->ALIPAY:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 46
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const-string v14, "CASH_APP"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->CASH_APP:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 48
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const-string v13, "EBT"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/simple_instrument_store/model/Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->EBT:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/16 v0, 0x11

    new-array v0, v0, [Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 15
    sget-object v13, Lcom/squareup/protos/simple_instrument_store/model/Brand;->INVALID:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/model/Brand;->UNKNOWN:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/model/Brand;->VISA:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/model/Brand;->MASTERCARD:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/model/Brand;->AMEX:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/model/Brand;->DISCOVER:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/model/Brand;->DISCOVER_DINERS:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/model/Brand;->JCB:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/model/Brand;->UNION_PAY:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/model/Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/model/Brand;->INTERAC:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/model/Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/model/Brand;->EFTPOS:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/model/Brand;->FELICA:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/model/Brand;->ALIPAY:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/model/Brand;->CASH_APP:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/model/Brand;->EBT:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->$VALUES:[Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 50
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/Brand$ProtoAdapter_Brand;

    invoke-direct {v0}, Lcom/squareup/protos/simple_instrument_store/model/Brand$ProtoAdapter_Brand;-><init>()V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 55
    iput p3, p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/simple_instrument_store/model/Brand;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 79
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->EBT:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    .line 78
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->CASH_APP:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    .line 77
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->ALIPAY:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    .line 76
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->FELICA:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    .line 75
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->EFTPOS:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    .line 74
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    .line 73
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->INTERAC:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    .line 72
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    .line 71
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->UNION_PAY:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    .line 70
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->JCB:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    .line 69
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->DISCOVER_DINERS:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    .line 68
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->DISCOVER:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    .line 67
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->AMEX:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    .line 66
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->MASTERCARD:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    .line 65
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->VISA:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    .line 64
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->UNKNOWN:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    .line 63
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->INVALID:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/simple_instrument_store/model/Brand;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/simple_instrument_store/model/Brand;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->$VALUES:[Lcom/squareup/protos/simple_instrument_store/model/Brand;

    invoke-virtual {v0}, [Lcom/squareup/protos/simple_instrument_store/model/Brand;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 86
    iget v0, p0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->value:I

    return v0
.end method
