.class public final Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Merchant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;",
        "Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public business_name:Ljava/lang/String;

.field public country:Lcom/squareup/protos/connect/v2/resources/Country;

.field public currency:Lcom/squareup/protos/connect/v2/common/Currency;

.field public id:Ljava/lang/String;

.field public language_code:Ljava/lang/String;

.field public status:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 184
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;
    .locals 9

    .line 237
    new-instance v8, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;

    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->business_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    iget-object v4, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->language_code:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    iget-object v6, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->status:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Country;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Currency;Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 171
    invoke-virtual {p0}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->build()Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;

    move-result-object v0

    return-object v0
.end method

.method public business_name(Ljava/lang/String;)Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->business_name:Ljava/lang/String;

    return-object p0
.end method

.method public country(Lcom/squareup/protos/connect/v2/resources/Country;)Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;
    .locals 0

    .line 207
    iput-object p1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0
.end method

.method public currency(Lcom/squareup/protos/connect/v2/common/Currency;)Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public language_code(Ljava/lang/String;)Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->language_code:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;)Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;
    .locals 0

    .line 231
    iput-object p1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->status:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;

    return-object p0
.end method
