.class public final Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SignalValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/checklist/signal/SignalValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/checklist/signal/SignalValue;",
        "Lcom/squareup/protos/checklist/signal/SignalValue$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public ext_added_a_location:Ljava/lang/Boolean;

.field public ext_added_authorized_representative:Ljava/lang/Boolean;

.field public ext_added_mobile_staff:Ljava/lang/Boolean;

.field public ext_added_more_next_steps:Ljava/lang/Boolean;

.field public ext_allowed_reactivation:Ljava/lang/Boolean;

.field public ext_apps_with_device_code_usages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public ext_bank_account_link_progress:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

.field public ext_card_payment_success_count:Ljava/lang/Integer;

.field public ext_cardless_free_trial_features:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/Feature;",
            ">;"
        }
    .end annotation
.end field

.field public ext_completed_action_item:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public ext_completed_product_interests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public ext_created_an_item:Ljava/lang/Boolean;

.field public ext_created_bar_code_label:Ljava/lang/Boolean;

.field public ext_customized_receipts:Ljava/lang/Boolean;

.field public ext_dismissed_action_item:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public ext_dismissed_action_item_time:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/ActionItemDismissal;",
            ">;"
        }
    .end annotation
.end field

.field public ext_downloaded_the_invoices_app:Ljava/lang/Boolean;

.field public ext_enable_pickup_and_delivery:Ljava/lang/Boolean;

.field public ext_enabled_item_for_sale:Ljava/lang/Boolean;

.field public ext_enabled_two_factor_auth:Ljava/lang/Boolean;

.field public ext_engaged_customers:Ljava/lang/Boolean;

.field public ext_has_a_device_usage:Ljava/lang/Boolean;

.field public ext_has_paid_for_features:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/Feature;",
            ">;"
        }
    .end annotation
.end field

.field public ext_has_retail_app_usage:Ljava/lang/Boolean;

.field public ext_imported_inventory:Ljava/lang/Boolean;

.field public ext_managed_employees:Ljava/lang/Boolean;

.field public ext_next_step:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/common/NextStep;",
            ">;"
        }
    .end annotation
.end field

.field public ext_published_item:Ljava/lang/Boolean;

.field public ext_published_merchant_profile:Ljava/lang/Boolean;

.field public ext_recommended_products:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public ext_referral_activated:Ljava/lang/Boolean;

.field public ext_register_activation_state:Lcom/squareup/protos/checklist/signal/ActivationStatus;

.field public ext_register_activation_state_history:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/ActivationStatus;",
            ">;"
        }
    .end annotation
.end field

.field public ext_reviewed_business_details:Ljava/lang/Boolean;

.field public ext_saved_a_menu_item:Ljava/lang/Boolean;

.field public ext_saw_secure_profile:Ljava/lang/Boolean;

.field public ext_sent_a_device_code:Ljava/lang/Boolean;

.field public ext_sent_an_invoice:Ljava/lang/Boolean;

.field public ext_set_close_of_day:Ljava/lang/Boolean;

.field public ext_set_up_a_floor_plan:Ljava/lang/Boolean;

.field public ext_set_up_an_rst_grid_layout:Ljava/lang/Boolean;

.field public ext_set_up_taxes:Ljava/lang/Boolean;

.field public ext_signup_business_category:Ljava/lang/String;

.field public ext_signup_business_sub_category:Ljava/lang/String;

.field public ext_signup_egift_card:Ljava/lang/Boolean;

.field public ext_signup_estimated_employees:Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;

.field public ext_signup_estimated_revenue:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

.field public ext_signup_estimated_usage:Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

.field public ext_signup_variant:Ljava/lang/String;

.field public ext_started_big_commerce_retail:Ljava/lang/Boolean;

.field public ext_subscribed_features:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/Feature;",
            ">;"
        }
    .end annotation
.end field

.field public ext_took_a_card_payment:Ljava/lang/Boolean;

.field public ext_took_a_cash_payment:Ljava/lang/Boolean;

.field public ext_took_a_cnp_payment:Ljava/lang/Boolean;

.field public ext_updated_secure_profile:Ljava/lang/Boolean;

.field public ext_viewed_business_fulfillment_options:Ljava/lang/Boolean;

.field public ext_viewed_customer_feedback:Ljava/lang/Boolean;

.field public ext_viewed_discounts:Ljava/lang/Boolean;

.field public ext_viewed_invoice_settings:Ljava/lang/Boolean;

.field public ext_viewed_online_store_selector:Ljava/lang/Boolean;

.field public ext_viewed_online_store_setup:Ljava/lang/Boolean;

.field public ext_viewed_other_services:Ljava/lang/Boolean;

.field public ext_viewed_reports:Ljava/lang/Boolean;

.field public ext_viewed_software_overview:Ljava/lang/Boolean;

.field public ext_viewed_stand_page:Ljava/lang/Boolean;

.field public ext_viewed_vendors:Ljava/lang/Boolean;

.field public ext_viewed_weebly_ost:Ljava/lang/Boolean;

.field public ext_visited_seller_community:Ljava/lang/Boolean;

.field public ext_visited_square_shop:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1302
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 1303
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_dismissed_action_item:Ljava/util/List;

    .line 1304
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_dismissed_action_item_time:Ljava/util/List;

    .line 1305
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_recommended_products:Ljava/util/List;

    .line 1306
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_subscribed_features:Ljava/util/List;

    .line 1307
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_cardless_free_trial_features:Ljava/util/List;

    .line 1308
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_completed_product_interests:Ljava/util/List;

    .line 1309
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_has_paid_for_features:Ljava/util/List;

    .line 1310
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_register_activation_state_history:Ljava/util/List;

    .line 1311
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_completed_action_item:Ljava/util/List;

    .line 1312
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_apps_with_device_code_usages:Ljava/util/List;

    .line 1313
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_next_step:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/checklist/signal/SignalValue;
    .locals 2

    .line 1685
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalValue;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/checklist/signal/SignalValue;-><init>(Lcom/squareup/protos/checklist/signal/SignalValue$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1163
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->build()Lcom/squareup/protos/checklist/signal/SignalValue;

    move-result-object v0

    return-object v0
.end method

.method public ext_added_a_location(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1466
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_added_a_location:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_added_authorized_representative(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1618
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_added_authorized_representative:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_added_mobile_staff(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1376
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_added_mobile_staff:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_added_more_next_steps(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1381
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_added_more_next_steps:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_allowed_reactivation(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1356
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_allowed_reactivation:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_apps_with_device_code_usages(Ljava/util/List;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/checklist/signal/SignalValue$Builder;"
        }
    .end annotation

    .line 1591
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1592
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_apps_with_device_code_usages:Ljava/util/List;

    return-object p0
.end method

.method public ext_bank_account_link_progress(Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1343
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_bank_account_link_progress:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    return-object p0
.end method

.method public ext_card_payment_success_count(Ljava/lang/Integer;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1392
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_card_payment_success_count:Ljava/lang/Integer;

    return-object p0
.end method

.method public ext_cardless_free_trial_features(Ljava/util/List;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/Feature;",
            ">;)",
            "Lcom/squareup/protos/checklist/signal/SignalValue$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1520
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1521
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_cardless_free_trial_features:Ljava/util/List;

    return-object p0
.end method

.method public ext_completed_action_item(Ljava/util/List;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/checklist/signal/SignalValue$Builder;"
        }
    .end annotation

    .line 1585
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1586
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_completed_action_item:Ljava/util/List;

    return-object p0
.end method

.method public ext_completed_product_interests(Ljava/util/List;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/checklist/signal/SignalValue$Builder;"
        }
    .end annotation

    .line 1526
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1527
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_completed_product_interests:Ljava/util/List;

    return-object p0
.end method

.method public ext_created_an_item(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1337
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_created_an_item:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_created_bar_code_label(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1668
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_created_bar_code_label:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_customized_receipts(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1481
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_customized_receipts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_dismissed_action_item(Ljava/util/List;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/checklist/signal/SignalValue$Builder;"
        }
    .end annotation

    .line 1386
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1387
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_dismissed_action_item:Ljava/util/List;

    return-object p0
.end method

.method public ext_dismissed_action_item_time(Ljava/util/List;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/ActionItemDismissal;",
            ">;)",
            "Lcom/squareup/protos/checklist/signal/SignalValue$Builder;"
        }
    .end annotation

    .line 1450
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1451
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_dismissed_action_item_time:Ljava/util/List;

    return-object p0
.end method

.method public ext_downloaded_the_invoices_app(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1658
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_downloaded_the_invoices_app:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_enable_pickup_and_delivery(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1673
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_enable_pickup_and_delivery:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_enabled_item_for_sale(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1402
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_enabled_item_for_sale:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_enabled_two_factor_auth(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1418
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_enabled_two_factor_auth:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_engaged_customers(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1491
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_engaged_customers:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_has_a_device_usage(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1361
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_has_a_device_usage:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_has_paid_for_features(Ljava/util/List;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/Feature;",
            ">;)",
            "Lcom/squareup/protos/checklist/signal/SignalValue$Builder;"
        }
    .end annotation

    .line 1542
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1543
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_has_paid_for_features:Ljava/util/List;

    return-object p0
.end method

.method public ext_has_retail_app_usage(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1537
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_has_retail_app_usage:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_imported_inventory(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1663
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_imported_inventory:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_managed_employees(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1471
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_managed_employees:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_next_step(Ljava/util/List;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/common/NextStep;",
            ">;)",
            "Lcom/squareup/protos/checklist/signal/SignalValue$Builder;"
        }
    .end annotation

    .line 1678
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1679
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_next_step:Ljava/util/List;

    return-object p0
.end method

.method public ext_published_item(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1397
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_published_item:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_published_merchant_profile(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1366
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_published_merchant_profile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_recommended_products(Ljava/util/List;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/checklist/signal/SignalValue$Builder;"
        }
    .end annotation

    .line 1506
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1507
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_recommended_products:Ljava/util/List;

    return-object p0
.end method

.method public ext_referral_activated(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1413
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_referral_activated:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_register_activation_state(Lcom/squareup/protos/checklist/signal/ActivationStatus;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1555
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_register_activation_state:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    return-object p0
.end method

.method public ext_register_activation_state_history(Ljava/util/List;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/ActivationStatus;",
            ">;)",
            "Lcom/squareup/protos/checklist/signal/SignalValue$Builder;"
        }
    .end annotation

    .line 1549
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1550
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_register_activation_state_history:Ljava/util/List;

    return-object p0
.end method

.method public ext_reviewed_business_details(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1461
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_reviewed_business_details:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_saved_a_menu_item(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1580
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_saved_a_menu_item:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_saw_secure_profile(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1560
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_saw_secure_profile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_sent_a_device_code(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1456
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_sent_a_device_code:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_sent_an_invoice(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1332
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_sent_an_invoice:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_set_close_of_day(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1532
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_set_close_of_day:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_set_up_a_floor_plan(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1570
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_set_up_a_floor_plan:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_set_up_an_rst_grid_layout(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1565
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_set_up_an_rst_grid_layout:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_set_up_taxes(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1575
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_set_up_taxes:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_signup_business_category(Ljava/lang/String;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1423
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_business_category:Ljava/lang/String;

    return-object p0
.end method

.method public ext_signup_business_sub_category(Ljava/lang/String;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1428
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_business_sub_category:Ljava/lang/String;

    return-object p0
.end method

.method public ext_signup_egift_card(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1602
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_egift_card:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_signup_estimated_employees(Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1439
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_estimated_employees:Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;

    return-object p0
.end method

.method public ext_signup_estimated_revenue(Lcom/squareup/protos/checklist/signal/EstimatedRevenue;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1433
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_estimated_revenue:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    return-object p0
.end method

.method public ext_signup_estimated_usage(Lcom/squareup/protos/checklist/signal/EstimatedUsageType;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1444
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_estimated_usage:Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

    return-object p0
.end method

.method public ext_signup_variant(Ljava/lang/String;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1597
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_variant:Ljava/lang/String;

    return-object p0
.end method

.method public ext_started_big_commerce_retail(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1648
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_started_big_commerce_retail:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_subscribed_features(Ljava/util/List;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/Feature;",
            ">;)",
            "Lcom/squareup/protos/checklist/signal/SignalValue$Builder;"
        }
    .end annotation

    .line 1512
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1513
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_subscribed_features:Ljava/util/List;

    return-object p0
.end method

.method public ext_took_a_card_payment(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1327
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_took_a_card_payment:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_took_a_cash_payment(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1322
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_took_a_cash_payment:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_took_a_cnp_payment(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1348
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_took_a_cnp_payment:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_updated_secure_profile(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1612
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_updated_secure_profile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_viewed_business_fulfillment_options(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1408
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_business_fulfillment_options:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_viewed_customer_feedback(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1476
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_customer_feedback:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_viewed_discounts(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1623
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_discounts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_viewed_invoice_settings(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1653
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_invoice_settings:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_viewed_online_store_selector(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1643
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_online_store_selector:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_viewed_online_store_setup(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1638
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_online_store_setup:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_viewed_other_services(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1486
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_other_services:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_viewed_reports(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1317
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_reports:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_viewed_software_overview(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1607
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_software_overview:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_viewed_stand_page(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1371
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_stand_page:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_viewed_vendors(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1628
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_vendors:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_viewed_weebly_ost(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1633
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_weebly_ost:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_visited_seller_community(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1496
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_visited_seller_community:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ext_visited_square_shop(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 0

    .line 1501
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_visited_square_shop:Ljava/lang/Boolean;

    return-object p0
.end method
