.class public final enum Lcom/squareup/protos/checklist/service/Variant;
.super Ljava/lang/Enum;
.source "Variant.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/checklist/service/Variant$ProtoAdapter_Variant;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/checklist/service/Variant;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/checklist/service/Variant;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/checklist/service/Variant;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum APPOINTMENTS:Lcom/squareup/protos/checklist/service/Variant;

.field public static final enum DEFAULT:Lcom/squareup/protos/checklist/service/Variant;

.field public static final enum ECOMMERCE:Lcom/squareup/protos/checklist/service/Variant;

.field public static final enum INVOICES:Lcom/squareup/protos/checklist/service/Variant;

.field public static final enum REGISTER_POS:Lcom/squareup/protos/checklist/service/Variant;

.field public static final enum RETAIL:Lcom/squareup/protos/checklist/service/Variant;

.field public static final enum RST:Lcom/squareup/protos/checklist/service/Variant;

.field public static final enum TERMINAL:Lcom/squareup/protos/checklist/service/Variant;

.field public static final enum UNKNOWN_VARIANT_DO_NOT_USE:Lcom/squareup/protos/checklist/service/Variant;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 11
    new-instance v0, Lcom/squareup/protos/checklist/service/Variant;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_VARIANT_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/checklist/service/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/service/Variant;->UNKNOWN_VARIANT_DO_NOT_USE:Lcom/squareup/protos/checklist/service/Variant;

    .line 13
    new-instance v0, Lcom/squareup/protos/checklist/service/Variant;

    const/4 v2, 0x1

    const-string v3, "REGISTER_POS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/checklist/service/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/service/Variant;->REGISTER_POS:Lcom/squareup/protos/checklist/service/Variant;

    .line 15
    new-instance v0, Lcom/squareup/protos/checklist/service/Variant;

    const/4 v3, 0x2

    const-string v4, "RETAIL"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/checklist/service/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/service/Variant;->RETAIL:Lcom/squareup/protos/checklist/service/Variant;

    .line 17
    new-instance v0, Lcom/squareup/protos/checklist/service/Variant;

    const/4 v4, 0x3

    const-string v5, "RST"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/checklist/service/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/service/Variant;->RST:Lcom/squareup/protos/checklist/service/Variant;

    .line 19
    new-instance v0, Lcom/squareup/protos/checklist/service/Variant;

    const/4 v5, 0x4

    const-string v6, "TERMINAL"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/checklist/service/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/service/Variant;->TERMINAL:Lcom/squareup/protos/checklist/service/Variant;

    .line 21
    new-instance v0, Lcom/squareup/protos/checklist/service/Variant;

    const/4 v6, 0x5

    const-string v7, "DEFAULT"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/checklist/service/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/service/Variant;->DEFAULT:Lcom/squareup/protos/checklist/service/Variant;

    .line 23
    new-instance v0, Lcom/squareup/protos/checklist/service/Variant;

    const/4 v7, 0x6

    const-string v8, "ECOMMERCE"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/checklist/service/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/service/Variant;->ECOMMERCE:Lcom/squareup/protos/checklist/service/Variant;

    .line 25
    new-instance v0, Lcom/squareup/protos/checklist/service/Variant;

    const/4 v8, 0x7

    const-string v9, "INVOICES"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/checklist/service/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/service/Variant;->INVOICES:Lcom/squareup/protos/checklist/service/Variant;

    .line 27
    new-instance v0, Lcom/squareup/protos/checklist/service/Variant;

    const/16 v9, 0x8

    const-string v10, "APPOINTMENTS"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/checklist/service/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/service/Variant;->APPOINTMENTS:Lcom/squareup/protos/checklist/service/Variant;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/protos/checklist/service/Variant;

    .line 10
    sget-object v10, Lcom/squareup/protos/checklist/service/Variant;->UNKNOWN_VARIANT_DO_NOT_USE:Lcom/squareup/protos/checklist/service/Variant;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/protos/checklist/service/Variant;->REGISTER_POS:Lcom/squareup/protos/checklist/service/Variant;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/service/Variant;->RETAIL:Lcom/squareup/protos/checklist/service/Variant;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/checklist/service/Variant;->RST:Lcom/squareup/protos/checklist/service/Variant;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/checklist/service/Variant;->TERMINAL:Lcom/squareup/protos/checklist/service/Variant;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/checklist/service/Variant;->DEFAULT:Lcom/squareup/protos/checklist/service/Variant;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/checklist/service/Variant;->ECOMMERCE:Lcom/squareup/protos/checklist/service/Variant;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/checklist/service/Variant;->INVOICES:Lcom/squareup/protos/checklist/service/Variant;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/checklist/service/Variant;->APPOINTMENTS:Lcom/squareup/protos/checklist/service/Variant;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/protos/checklist/service/Variant;->$VALUES:[Lcom/squareup/protos/checklist/service/Variant;

    .line 29
    new-instance v0, Lcom/squareup/protos/checklist/service/Variant$ProtoAdapter_Variant;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/service/Variant$ProtoAdapter_Variant;-><init>()V

    sput-object v0, Lcom/squareup/protos/checklist/service/Variant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput p3, p0, Lcom/squareup/protos/checklist/service/Variant;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/checklist/service/Variant;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 50
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/checklist/service/Variant;->APPOINTMENTS:Lcom/squareup/protos/checklist/service/Variant;

    return-object p0

    .line 49
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/checklist/service/Variant;->INVOICES:Lcom/squareup/protos/checklist/service/Variant;

    return-object p0

    .line 48
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/checklist/service/Variant;->ECOMMERCE:Lcom/squareup/protos/checklist/service/Variant;

    return-object p0

    .line 47
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/checklist/service/Variant;->DEFAULT:Lcom/squareup/protos/checklist/service/Variant;

    return-object p0

    .line 46
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/checklist/service/Variant;->TERMINAL:Lcom/squareup/protos/checklist/service/Variant;

    return-object p0

    .line 45
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/checklist/service/Variant;->RST:Lcom/squareup/protos/checklist/service/Variant;

    return-object p0

    .line 44
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/checklist/service/Variant;->RETAIL:Lcom/squareup/protos/checklist/service/Variant;

    return-object p0

    .line 43
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/checklist/service/Variant;->REGISTER_POS:Lcom/squareup/protos/checklist/service/Variant;

    return-object p0

    .line 42
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/checklist/service/Variant;->UNKNOWN_VARIANT_DO_NOT_USE:Lcom/squareup/protos/checklist/service/Variant;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/checklist/service/Variant;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/checklist/service/Variant;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/checklist/service/Variant;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/checklist/service/Variant;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/checklist/service/Variant;->$VALUES:[Lcom/squareup/protos/checklist/service/Variant;

    invoke-virtual {v0}, [Lcom/squareup/protos/checklist/service/Variant;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/checklist/service/Variant;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 57
    iget v0, p0, Lcom/squareup/protos/checklist/service/Variant;->value:I

    return v0
.end method
