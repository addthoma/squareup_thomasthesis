.class public final Lcom/squareup/protos/checklist/service/CompleteActionItemResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CompleteActionItemResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/checklist/service/CompleteActionItemResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/checklist/service/CompleteActionItemResponse;",
        "Lcom/squareup/protos/checklist/service/CompleteActionItemResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public status:Lcom/squareup/protos/checklist/service/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 77
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/checklist/service/CompleteActionItemResponse;
    .locals 3

    .line 87
    new-instance v0, Lcom/squareup/protos/checklist/service/CompleteActionItemResponse;

    iget-object v1, p0, Lcom/squareup/protos/checklist/service/CompleteActionItemResponse$Builder;->status:Lcom/squareup/protos/checklist/service/Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/checklist/service/CompleteActionItemResponse;-><init>(Lcom/squareup/protos/checklist/service/Status;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 74
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/service/CompleteActionItemResponse$Builder;->build()Lcom/squareup/protos/checklist/service/CompleteActionItemResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/checklist/service/Status;)Lcom/squareup/protos/checklist/service/CompleteActionItemResponse$Builder;
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/protos/checklist/service/CompleteActionItemResponse$Builder;->status:Lcom/squareup/protos/checklist/service/Status;

    return-object p0
.end method
