.class public final enum Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;
.super Ljava/lang/Enum;
.source "ReverseSweepBizbankMoneyResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Error"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error$ProtoAdapter_Error;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum INSUFFICIENT_FUNDS:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

.field public static final enum MERCHANT_NOT_BACKFILLED_ON_BOOKS:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

.field public static final enum REVERSE_SWEEP_ERROR_UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 20
    new-instance v0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    const/4 v1, 0x0

    const-string v2, "REVERSE_SWEEP_ERROR_UNKNOWN_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->REVERSE_SWEEP_ERROR_UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    .line 27
    new-instance v0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    const/4 v2, 0x1

    const-string v3, "INSUFFICIENT_FUNDS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->INSUFFICIENT_FUNDS:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    .line 32
    new-instance v0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    const/4 v3, 0x2

    const-string v4, "MERCHANT_NOT_BACKFILLED_ON_BOOKS"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->MERCHANT_NOT_BACKFILLED_ON_BOOKS:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    .line 19
    sget-object v4, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->REVERSE_SWEEP_ERROR_UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->INSUFFICIENT_FUNDS:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->MERCHANT_NOT_BACKFILLED_ON_BOOKS:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->$VALUES:[Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    .line 34
    new-instance v0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error$ProtoAdapter_Error;

    invoke-direct {v0}, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error$ProtoAdapter_Error;-><init>()V

    sput-object v0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput p3, p0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 49
    :cond_0
    sget-object p0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->MERCHANT_NOT_BACKFILLED_ON_BOOKS:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    return-object p0

    .line 48
    :cond_1
    sget-object p0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->INSUFFICIENT_FUNDS:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    return-object p0

    .line 47
    :cond_2
    sget-object p0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->REVERSE_SWEEP_ERROR_UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;
    .locals 1

    .line 19
    const-class v0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->$VALUES:[Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    invoke-virtual {v0}, [Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 56
    iget v0, p0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->value:I

    return v0
.end method
