.class public final enum Lcom/squareup/protos/devicesettings/ProductType;
.super Ljava/lang/Enum;
.source "ProductType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/devicesettings/ProductType$ProtoAdapter_ProductType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/devicesettings/ProductType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/devicesettings/ProductType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/devicesettings/ProductType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum KDS:Lcom/squareup/protos/devicesettings/ProductType;

.field public static final enum PAY_SDK:Lcom/squareup/protos/devicesettings/ProductType;

.field public static final enum REGISTER:Lcom/squareup/protos/devicesettings/ProductType;

.field public static final enum RESTAURANT:Lcom/squareup/protos/devicesettings/ProductType;

.field public static final enum RETAIL:Lcom/squareup/protos/devicesettings/ProductType;

.field public static final enum TERMINAL_API:Lcom/squareup/protos/devicesettings/ProductType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/devicesettings/ProductType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 11
    new-instance v0, Lcom/squareup/protos/devicesettings/ProductType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/devicesettings/ProductType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/devicesettings/ProductType;->UNKNOWN:Lcom/squareup/protos/devicesettings/ProductType;

    .line 13
    new-instance v0, Lcom/squareup/protos/devicesettings/ProductType;

    const/4 v3, 0x2

    const-string v4, "REGISTER"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/devicesettings/ProductType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/devicesettings/ProductType;->REGISTER:Lcom/squareup/protos/devicesettings/ProductType;

    .line 15
    new-instance v0, Lcom/squareup/protos/devicesettings/ProductType;

    const/4 v4, 0x3

    const-string v5, "RESTAURANT"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/devicesettings/ProductType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/devicesettings/ProductType;->RESTAURANT:Lcom/squareup/protos/devicesettings/ProductType;

    .line 17
    new-instance v0, Lcom/squareup/protos/devicesettings/ProductType;

    const/4 v5, 0x4

    const-string v6, "RETAIL"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/devicesettings/ProductType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/devicesettings/ProductType;->RETAIL:Lcom/squareup/protos/devicesettings/ProductType;

    .line 19
    new-instance v0, Lcom/squareup/protos/devicesettings/ProductType;

    const/4 v6, 0x5

    const-string v7, "PAY_SDK"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/devicesettings/ProductType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/devicesettings/ProductType;->PAY_SDK:Lcom/squareup/protos/devicesettings/ProductType;

    .line 21
    new-instance v0, Lcom/squareup/protos/devicesettings/ProductType;

    const/4 v7, 0x6

    const-string v8, "TERMINAL_API"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/protos/devicesettings/ProductType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/devicesettings/ProductType;->TERMINAL_API:Lcom/squareup/protos/devicesettings/ProductType;

    .line 23
    new-instance v0, Lcom/squareup/protos/devicesettings/ProductType;

    const/4 v8, 0x7

    const-string v9, "KDS"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/protos/devicesettings/ProductType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/devicesettings/ProductType;->KDS:Lcom/squareup/protos/devicesettings/ProductType;

    new-array v0, v8, [Lcom/squareup/protos/devicesettings/ProductType;

    .line 10
    sget-object v8, Lcom/squareup/protos/devicesettings/ProductType;->UNKNOWN:Lcom/squareup/protos/devicesettings/ProductType;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/devicesettings/ProductType;->REGISTER:Lcom/squareup/protos/devicesettings/ProductType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/devicesettings/ProductType;->RESTAURANT:Lcom/squareup/protos/devicesettings/ProductType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/devicesettings/ProductType;->RETAIL:Lcom/squareup/protos/devicesettings/ProductType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/devicesettings/ProductType;->PAY_SDK:Lcom/squareup/protos/devicesettings/ProductType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/devicesettings/ProductType;->TERMINAL_API:Lcom/squareup/protos/devicesettings/ProductType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/devicesettings/ProductType;->KDS:Lcom/squareup/protos/devicesettings/ProductType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/devicesettings/ProductType;->$VALUES:[Lcom/squareup/protos/devicesettings/ProductType;

    .line 25
    new-instance v0, Lcom/squareup/protos/devicesettings/ProductType$ProtoAdapter_ProductType;

    invoke-direct {v0}, Lcom/squareup/protos/devicesettings/ProductType$ProtoAdapter_ProductType;-><init>()V

    sput-object v0, Lcom/squareup/protos/devicesettings/ProductType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    iput p3, p0, Lcom/squareup/protos/devicesettings/ProductType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/devicesettings/ProductType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 44
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/devicesettings/ProductType;->KDS:Lcom/squareup/protos/devicesettings/ProductType;

    return-object p0

    .line 43
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/devicesettings/ProductType;->TERMINAL_API:Lcom/squareup/protos/devicesettings/ProductType;

    return-object p0

    .line 42
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/devicesettings/ProductType;->PAY_SDK:Lcom/squareup/protos/devicesettings/ProductType;

    return-object p0

    .line 41
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/devicesettings/ProductType;->RETAIL:Lcom/squareup/protos/devicesettings/ProductType;

    return-object p0

    .line 40
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/devicesettings/ProductType;->RESTAURANT:Lcom/squareup/protos/devicesettings/ProductType;

    return-object p0

    .line 39
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/devicesettings/ProductType;->REGISTER:Lcom/squareup/protos/devicesettings/ProductType;

    return-object p0

    .line 38
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/devicesettings/ProductType;->UNKNOWN:Lcom/squareup/protos/devicesettings/ProductType;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/devicesettings/ProductType;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/devicesettings/ProductType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/devicesettings/ProductType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/devicesettings/ProductType;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/devicesettings/ProductType;->$VALUES:[Lcom/squareup/protos/devicesettings/ProductType;

    invoke-virtual {v0}, [Lcom/squareup/protos/devicesettings/ProductType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/devicesettings/ProductType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 51
    iget v0, p0, Lcom/squareup/protos/devicesettings/ProductType;->value:I

    return v0
.end method
