.class public final Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GbAccountDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bank_accounts/GbAccountDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bank_accounts/GbAccountDetails;",
        "Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bic:Ljava/lang/String;

.field public supports_bacs:Ljava/lang/Boolean;

.field public supports_direct_debit:Ljava/lang/Boolean;

.field public supports_faster_payments:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 140
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bic(Ljava/lang/String;)Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->bic:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/bank_accounts/GbAccountDetails;
    .locals 7

    .line 178
    new-instance v6, Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->supports_bacs:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->supports_direct_debit:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->supports_faster_payments:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->bic:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/bank_accounts/GbAccountDetails;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->build()Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    move-result-object v0

    return-object v0
.end method

.method public supports_bacs(Ljava/lang/Boolean;)Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->supports_bacs:Ljava/lang/Boolean;

    return-object p0
.end method

.method public supports_direct_debit(Ljava/lang/Boolean;)Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->supports_direct_debit:Ljava/lang/Boolean;

    return-object p0
.end method

.method public supports_faster_payments(Ljava/lang/Boolean;)Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->supports_faster_payments:Ljava/lang/Boolean;

    return-object p0
.end method
