.class final Lcom/squareup/protos/bank_accounts/FrAccount$ProtoAdapter_FrAccount;
.super Lcom/squareup/wire/ProtoAdapter;
.source "FrAccount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bank_accounts/FrAccount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FrAccount"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/bank_accounts/FrAccount;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 154
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/bank_accounts/FrAccount;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bank_accounts/FrAccount;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 175
    new-instance v0, Lcom/squareup/protos/bank_accounts/FrAccount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bank_accounts/FrAccount$Builder;-><init>()V

    .line 176
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 177
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 183
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 181
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bank_accounts/FrAccount$Builder;->swift_bic(Ljava/lang/String;)Lcom/squareup/protos/bank_accounts/FrAccount$Builder;

    goto :goto_0

    .line 180
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bank_accounts/FrAccount$Builder;->branch_code(Ljava/lang/String;)Lcom/squareup/protos/bank_accounts/FrAccount$Builder;

    goto :goto_0

    .line 179
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bank_accounts/FrAccount$Builder;->bank_code(Ljava/lang/String;)Lcom/squareup/protos/bank_accounts/FrAccount$Builder;

    goto :goto_0

    .line 187
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/bank_accounts/FrAccount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 188
    invoke-virtual {v0}, Lcom/squareup/protos/bank_accounts/FrAccount$Builder;->build()Lcom/squareup/protos/bank_accounts/FrAccount;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 152
    invoke-virtual {p0, p1}, Lcom/squareup/protos/bank_accounts/FrAccount$ProtoAdapter_FrAccount;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bank_accounts/FrAccount;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bank_accounts/FrAccount;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 167
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bank_accounts/FrAccount;->bank_code:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 168
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bank_accounts/FrAccount;->branch_code:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 169
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bank_accounts/FrAccount;->swift_bic:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 170
    invoke-virtual {p2}, Lcom/squareup/protos/bank_accounts/FrAccount;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 152
    check-cast p2, Lcom/squareup/protos/bank_accounts/FrAccount;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/bank_accounts/FrAccount$ProtoAdapter_FrAccount;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bank_accounts/FrAccount;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/bank_accounts/FrAccount;)I
    .locals 4

    .line 159
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bank_accounts/FrAccount;->bank_code:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bank_accounts/FrAccount;->branch_code:Ljava/lang/String;

    const/4 v3, 0x2

    .line 160
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bank_accounts/FrAccount;->swift_bic:Ljava/lang/String;

    const/4 v3, 0x3

    .line 161
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 162
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/FrAccount;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 152
    check-cast p1, Lcom/squareup/protos/bank_accounts/FrAccount;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bank_accounts/FrAccount$ProtoAdapter_FrAccount;->encodedSize(Lcom/squareup/protos/bank_accounts/FrAccount;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/bank_accounts/FrAccount;)Lcom/squareup/protos/bank_accounts/FrAccount;
    .locals 0

    .line 193
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/FrAccount;->newBuilder()Lcom/squareup/protos/bank_accounts/FrAccount$Builder;

    move-result-object p1

    .line 194
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/FrAccount$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 195
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/FrAccount$Builder;->build()Lcom/squareup/protos/bank_accounts/FrAccount;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 152
    check-cast p1, Lcom/squareup/protos/bank_accounts/FrAccount;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bank_accounts/FrAccount$ProtoAdapter_FrAccount;->redact(Lcom/squareup/protos/bank_accounts/FrAccount;)Lcom/squareup/protos/bank_accounts/FrAccount;

    move-result-object p1

    return-object p1
.end method
