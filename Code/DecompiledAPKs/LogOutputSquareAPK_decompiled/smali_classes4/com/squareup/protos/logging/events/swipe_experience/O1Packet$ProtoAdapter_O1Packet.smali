.class final Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$ProtoAdapter_O1Packet;
.super Lcom/squareup/wire/ProtoAdapter;
.source "O1Packet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_O1Packet"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 178
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 201
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;-><init>()V

    .line 202
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 203
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 210
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 208
    :cond_0
    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_flash_error(Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;)Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;

    goto :goto_0

    .line 207
    :cond_1
    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_general_error(Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;)Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;

    goto :goto_0

    .line 206
    :cond_2
    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_successful_swipe(Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;)Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;

    goto :goto_0

    .line 205
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->total_length(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;

    goto :goto_0

    .line 214
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 215
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 176
    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$ProtoAdapter_O1Packet;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 192
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->total_length:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 193
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 194
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 195
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 196
    invoke-virtual {p2}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 176
    check-cast p2, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$ProtoAdapter_O1Packet;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;)I
    .locals 4

    .line 183
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->total_length:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    const/4 v3, 0x2

    .line 184
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    const/4 v3, 0x3

    .line 185
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    const/4 v3, 0x4

    .line 186
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 176
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$ProtoAdapter_O1Packet;->encodedSize(Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;)Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;
    .locals 2

    .line 220
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;

    move-result-object p1

    .line 221
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    iput-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    .line 222
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    iput-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    .line 223
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    iput-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    .line 224
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 225
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 176
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$ProtoAdapter_O1Packet;->redact(Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;)Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    move-result-object p1

    return-object p1
.end method
