.class final Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$ProtoAdapter_R4CardData;
.super Lcom/squareup/wire/ProtoAdapter;
.source "R4CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_R4CardData"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 757
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 794
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;-><init>()V

    .line 795
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 796
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 810
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 808
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->name_not_blank(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;

    goto :goto_0

    .line 807
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->name_length(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;

    goto :goto_0

    .line 806
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track2_result(Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;

    goto :goto_0

    .line 805
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track1_result(Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;

    goto :goto_0

    .line 804
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track2_len(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;

    goto :goto_0

    .line 803
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track1_len(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;

    goto :goto_0

    .line 802
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->bit_period_end_of_swipe(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;

    goto :goto_0

    .line 801
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->bit_period_bit_140(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;

    goto :goto_0

    .line 800
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->bit_period_bwd(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;

    goto :goto_0

    .line 799
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->entropy(Ljava/lang/Long;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;

    goto/16 :goto_0

    .line 798
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->counter(Ljava/lang/Long;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;

    goto/16 :goto_0

    .line 814
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 815
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 755
    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$ProtoAdapter_R4CardData;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 778
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->counter:Ljava/lang/Long;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 779
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->entropy:Ljava/lang/Long;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 780
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bwd:Ljava/lang/Integer;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 781
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bit_140:Ljava/lang/Integer;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 782
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_end_of_swipe:Ljava/lang/Integer;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 783
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_len:Ljava/lang/Integer;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 784
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_len:Ljava/lang/Integer;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 785
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 786
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 787
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_length:Ljava/lang/Integer;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 788
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_not_blank:Ljava/lang/Boolean;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 789
    invoke-virtual {p2}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 755
    check-cast p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$ProtoAdapter_R4CardData;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;)I
    .locals 4

    .line 762
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->counter:Ljava/lang/Long;

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->entropy:Ljava/lang/Long;

    const/4 v3, 0x4

    .line 763
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bwd:Ljava/lang/Integer;

    const/4 v3, 0x5

    .line 764
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bit_140:Ljava/lang/Integer;

    const/4 v3, 0x6

    .line 765
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_end_of_swipe:Ljava/lang/Integer;

    const/4 v3, 0x7

    .line 766
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_len:Ljava/lang/Integer;

    const/16 v3, 0x8

    .line 767
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_len:Ljava/lang/Integer;

    const/16 v3, 0x9

    .line 768
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    const/16 v3, 0xa

    .line 769
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    const/16 v3, 0xb

    .line 770
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_length:Ljava/lang/Integer;

    const/16 v3, 0xc

    .line 771
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_not_blank:Ljava/lang/Boolean;

    const/16 v3, 0xd

    .line 772
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 773
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 755
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$ProtoAdapter_R4CardData;->encodedSize(Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;
    .locals 2

    .line 820
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;

    move-result-object p1

    .line 821
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    iput-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    .line 822
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    iput-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    .line 823
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 824
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 755
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$ProtoAdapter_R4CardData;->redact(Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    move-result-object p1

    return-object p1
.end method
