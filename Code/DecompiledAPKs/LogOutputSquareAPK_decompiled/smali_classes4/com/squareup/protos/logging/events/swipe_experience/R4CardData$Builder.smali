.class public final Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "R4CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;",
        "Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bit_period_bit_140:Ljava/lang/Integer;

.field public bit_period_bwd:Ljava/lang/Integer;

.field public bit_period_end_of_swipe:Ljava/lang/Integer;

.field public counter:Ljava/lang/Long;

.field public entropy:Ljava/lang/Long;

.field public name_length:Ljava/lang/Integer;

.field public name_not_blank:Ljava/lang/Boolean;

.field public track1_len:Ljava/lang/Integer;

.field public track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

.field public track2_len:Ljava/lang/Integer;

.field public track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 299
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bit_period_bit_140(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;
    .locals 0

    .line 351
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->bit_period_bit_140:Ljava/lang/Integer;

    return-object p0
.end method

.method public bit_period_bwd(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;
    .locals 0

    .line 341
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->bit_period_bwd:Ljava/lang/Integer;

    return-object p0
.end method

.method public bit_period_end_of_swipe(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;
    .locals 0

    .line 361
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->bit_period_end_of_swipe:Ljava/lang/Integer;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;
    .locals 14

    .line 421
    new-instance v13, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->counter:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->entropy:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->bit_period_bwd:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->bit_period_bit_140:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->bit_period_end_of_swipe:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track1_len:Ljava/lang/Integer;

    iget-object v7, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track2_len:Ljava/lang/Integer;

    iget-object v8, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    iget-object v9, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    iget-object v10, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->name_length:Ljava/lang/Integer;

    iget-object v11, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->name_not_blank:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;Ljava/lang/Integer;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v13
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 276
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    move-result-object v0

    return-object v0
.end method

.method public counter(Ljava/lang/Long;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;
    .locals 0

    .line 311
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->counter:Ljava/lang/Long;

    return-object p0
.end method

.method public entropy(Ljava/lang/Long;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;
    .locals 0

    .line 322
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->entropy:Ljava/lang/Long;

    return-object p0
.end method

.method public name_length(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;
    .locals 0

    .line 407
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->name_length:Ljava/lang/Integer;

    return-object p0
.end method

.method public name_not_blank(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;
    .locals 0

    .line 415
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->name_not_blank:Ljava/lang/Boolean;

    return-object p0
.end method

.method public track1_len(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;
    .locals 0

    .line 371
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track1_len:Ljava/lang/Integer;

    return-object p0
.end method

.method public track1_result(Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;
    .locals 0

    .line 389
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    return-object p0
.end method

.method public track2_len(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;
    .locals 0

    .line 380
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track2_len:Ljava/lang/Integer;

    return-object p0
.end method

.method public track2_result(Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;
    .locals 0

    .line 398
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    return-object p0
.end method
