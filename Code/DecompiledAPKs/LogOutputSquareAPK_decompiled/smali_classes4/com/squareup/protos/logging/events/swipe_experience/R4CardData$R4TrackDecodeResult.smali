.class public final Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;
.super Lcom/squareup/wire/Message;
.source "R4CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "R4TrackDecodeResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$ProtoAdapter_R4TrackDecodeResult;,
        Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;",
        "Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CARD_DATA_TOO_LONG:Ljava/lang/Boolean;

.field public static final DEFAULT_CARD_DATA_TOO_SHORT:Ljava/lang/Boolean;

.field public static final DEFAULT_LRC_FAILURE:Ljava/lang/Boolean;

.field public static final DEFAULT_MISSING_END_SENTINEL:Ljava/lang/Boolean;

.field public static final DEFAULT_MISSING_START_SENTINEL:Ljava/lang/Boolean;

.field public static final DEFAULT_NO_ZEROS_DETECTED:Ljava/lang/Boolean;

.field public static final DEFAULT_PARITY_ERROR:Ljava/lang/Boolean;

.field public static final DEFAULT_TRACK_NOT_PRESENT:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final card_data_too_long:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final card_data_too_short:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final lrc_failure:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final missing_end_sentinel:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final missing_start_sentinel:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final no_zeros_detected:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final parity_error:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final track_not_present:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 426
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$ProtoAdapter_R4TrackDecodeResult;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$ProtoAdapter_R4TrackDecodeResult;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 430
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->DEFAULT_TRACK_NOT_PRESENT:Ljava/lang/Boolean;

    .line 432
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->DEFAULT_NO_ZEROS_DETECTED:Ljava/lang/Boolean;

    .line 434
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->DEFAULT_CARD_DATA_TOO_LONG:Ljava/lang/Boolean;

    .line 436
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->DEFAULT_CARD_DATA_TOO_SHORT:Ljava/lang/Boolean;

    .line 438
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->DEFAULT_MISSING_START_SENTINEL:Ljava/lang/Boolean;

    .line 440
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->DEFAULT_MISSING_END_SENTINEL:Ljava/lang/Boolean;

    .line 442
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->DEFAULT_PARITY_ERROR:Ljava/lang/Boolean;

    .line 444
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->DEFAULT_LRC_FAILURE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 10

    .line 521
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 528
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 529
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->track_not_present:Ljava/lang/Boolean;

    .line 530
    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->no_zeros_detected:Ljava/lang/Boolean;

    .line 531
    iput-object p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_long:Ljava/lang/Boolean;

    .line 532
    iput-object p4, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_short:Ljava/lang/Boolean;

    .line 533
    iput-object p5, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_start_sentinel:Ljava/lang/Boolean;

    .line 534
    iput-object p6, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_end_sentinel:Ljava/lang/Boolean;

    .line 535
    iput-object p7, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->parity_error:Ljava/lang/Boolean;

    .line 536
    iput-object p8, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->lrc_failure:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 557
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 558
    :cond_1
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    .line 559
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->track_not_present:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->track_not_present:Ljava/lang/Boolean;

    .line 560
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->no_zeros_detected:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->no_zeros_detected:Ljava/lang/Boolean;

    .line 561
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_long:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_long:Ljava/lang/Boolean;

    .line 562
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_short:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_short:Ljava/lang/Boolean;

    .line 563
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_start_sentinel:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_start_sentinel:Ljava/lang/Boolean;

    .line 564
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_end_sentinel:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_end_sentinel:Ljava/lang/Boolean;

    .line 565
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->parity_error:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->parity_error:Ljava/lang/Boolean;

    .line 566
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->lrc_failure:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->lrc_failure:Ljava/lang/Boolean;

    .line 567
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 572
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 574
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 575
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->track_not_present:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 576
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->no_zeros_detected:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 577
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_long:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 578
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_short:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 579
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_start_sentinel:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 580
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_end_sentinel:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 581
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->parity_error:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 582
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->lrc_failure:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 583
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;
    .locals 2

    .line 541
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;-><init>()V

    .line 542
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->track_not_present:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->track_not_present:Ljava/lang/Boolean;

    .line 543
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->no_zeros_detected:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->no_zeros_detected:Ljava/lang/Boolean;

    .line 544
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_long:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->card_data_too_long:Ljava/lang/Boolean;

    .line 545
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_short:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->card_data_too_short:Ljava/lang/Boolean;

    .line 546
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_start_sentinel:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->missing_start_sentinel:Ljava/lang/Boolean;

    .line 547
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_end_sentinel:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->missing_end_sentinel:Ljava/lang/Boolean;

    .line 548
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->parity_error:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->parity_error:Ljava/lang/Boolean;

    .line 549
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->lrc_failure:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->lrc_failure:Ljava/lang/Boolean;

    .line 550
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 425
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 590
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 591
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->track_not_present:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", track_not_present="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->track_not_present:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 592
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->no_zeros_detected:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", no_zeros_detected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->no_zeros_detected:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 593
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_long:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", card_data_too_long="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_long:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 594
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_short:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", card_data_too_short="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_short:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 595
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_start_sentinel:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", missing_start_sentinel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_start_sentinel:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 596
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_end_sentinel:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", missing_end_sentinel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_end_sentinel:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 597
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->parity_error:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", parity_error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->parity_error:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 598
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->lrc_failure:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", lrc_failure="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->lrc_failure:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "R4TrackDecodeResult{"

    .line 599
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
