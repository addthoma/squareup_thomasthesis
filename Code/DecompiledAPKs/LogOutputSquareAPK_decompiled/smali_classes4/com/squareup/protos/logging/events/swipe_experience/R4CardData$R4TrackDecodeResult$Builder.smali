.class public final Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "R4CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;",
        "Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card_data_too_long:Ljava/lang/Boolean;

.field public card_data_too_short:Ljava/lang/Boolean;

.field public lrc_failure:Ljava/lang/Boolean;

.field public missing_end_sentinel:Ljava/lang/Boolean;

.field public missing_start_sentinel:Ljava/lang/Boolean;

.field public no_zeros_detected:Ljava/lang/Boolean;

.field public parity_error:Ljava/lang/Boolean;

.field public track_not_present:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 619
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;
    .locals 11

    .line 688
    new-instance v10, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->track_not_present:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->no_zeros_detected:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->card_data_too_long:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->card_data_too_short:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->missing_start_sentinel:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->missing_end_sentinel:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->parity_error:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->lrc_failure:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 602
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    move-result-object v0

    return-object v0
.end method

.method public card_data_too_long(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;
    .locals 0

    .line 642
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->card_data_too_long:Ljava/lang/Boolean;

    return-object p0
.end method

.method public card_data_too_short(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;
    .locals 0

    .line 650
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->card_data_too_short:Ljava/lang/Boolean;

    return-object p0
.end method

.method public lrc_failure(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;
    .locals 0

    .line 682
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->lrc_failure:Ljava/lang/Boolean;

    return-object p0
.end method

.method public missing_end_sentinel(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;
    .locals 0

    .line 666
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->missing_end_sentinel:Ljava/lang/Boolean;

    return-object p0
.end method

.method public missing_start_sentinel(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;
    .locals 0

    .line 658
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->missing_start_sentinel:Ljava/lang/Boolean;

    return-object p0
.end method

.method public no_zeros_detected(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;
    .locals 0

    .line 634
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->no_zeros_detected:Ljava/lang/Boolean;

    return-object p0
.end method

.method public parity_error(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;
    .locals 0

    .line 674
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->parity_error:Ljava/lang/Boolean;

    return-object p0
.end method

.method public track_not_present(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;
    .locals 0

    .line 626
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->track_not_present:Ljava/lang/Boolean;

    return-object p0
.end method
