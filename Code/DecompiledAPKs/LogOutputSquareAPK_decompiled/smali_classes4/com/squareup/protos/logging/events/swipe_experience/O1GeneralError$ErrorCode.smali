.class public final enum Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;
.super Ljava/lang/Enum;
.source "O1GeneralError.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ErrorCode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode$ProtoAdapter_ErrorCode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CARD_DATA_FAILED_LRC_CHECK:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

.field public static final enum CARD_DATA_FAILED_PARITY_CHECK:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

.field public static final enum CARD_DATA_TOO_LONG:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

.field public static final enum CARD_DATA_TOO_SHORT:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

.field public static final enum END_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

.field public static final enum NO_LEADING_ZEROS_DETECTED:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

.field public static final enum OTHER_INTERNAL_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

.field public static final enum START_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

.field public static final enum SWIPE_COUNTER_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 290
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    const/4 v1, 0x0

    const-string v2, "CARD_DATA_TOO_LONG"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_TOO_LONG:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    .line 295
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    const/4 v2, 0x1

    const-string v3, "CARD_DATA_TOO_SHORT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_TOO_SHORT:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    .line 300
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    const/4 v3, 0x2

    const-string v4, "START_SENTINEL_NOT_FOUND"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->START_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    .line 305
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    const/4 v4, 0x3

    const-string v5, "END_SENTINEL_NOT_FOUND"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->END_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    .line 310
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    const/4 v5, 0x4

    const-string v6, "CARD_DATA_FAILED_PARITY_CHECK"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_FAILED_PARITY_CHECK:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    .line 315
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    const/4 v6, 0x5

    const-string v7, "CARD_DATA_FAILED_LRC_CHECK"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_FAILED_LRC_CHECK:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    .line 321
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    const/4 v7, 0x6

    const-string v8, "SWIPE_COUNTER_ERROR"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->SWIPE_COUNTER_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    .line 327
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    const/4 v8, 0x7

    const-string v9, "NO_LEADING_ZEROS_DETECTED"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->NO_LEADING_ZEROS_DETECTED:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    .line 332
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    const/16 v9, 0x8

    const-string v10, "OTHER_INTERNAL_ERROR"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->OTHER_INTERNAL_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    .line 286
    sget-object v10, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_TOO_LONG:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_TOO_SHORT:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->START_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->END_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_FAILED_PARITY_CHECK:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_FAILED_LRC_CHECK:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->SWIPE_COUNTER_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->NO_LEADING_ZEROS_DETECTED:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->OTHER_INTERNAL_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    .line 334
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode$ProtoAdapter_ErrorCode;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode$ProtoAdapter_ErrorCode;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 338
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 339
    iput p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 355
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->OTHER_INTERNAL_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    .line 354
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->NO_LEADING_ZEROS_DETECTED:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    .line 353
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->SWIPE_COUNTER_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    .line 352
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_FAILED_LRC_CHECK:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    .line 351
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_FAILED_PARITY_CHECK:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    .line 350
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->END_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    .line 349
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->START_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    .line 348
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_TOO_SHORT:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    .line 347
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_TOO_LONG:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;
    .locals 1

    .line 286
    const-class v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;
    .locals 1

    .line 286
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    invoke-virtual {v0}, [Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 362
    iget v0, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->value:I

    return v0
.end method
