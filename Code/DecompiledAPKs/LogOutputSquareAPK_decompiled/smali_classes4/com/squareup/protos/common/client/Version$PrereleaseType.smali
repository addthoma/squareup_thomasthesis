.class public final enum Lcom/squareup/protos/common/client/Version$PrereleaseType;
.super Ljava/lang/Enum;
.source "Version.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/client/Version;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PrereleaseType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/client/Version$PrereleaseType$ProtoAdapter_PrereleaseType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/common/client/Version$PrereleaseType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/common/client/Version$PrereleaseType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/client/Version$PrereleaseType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALPHA:Lcom/squareup/protos/common/client/Version$PrereleaseType;

.field public static final enum BETA:Lcom/squareup/protos/common/client/Version$PrereleaseType;

.field public static final enum PRERELEASE:Lcom/squareup/protos/common/client/Version$PrereleaseType;

.field public static final enum RELEASE_CANDIDATE:Lcom/squareup/protos/common/client/Version$PrereleaseType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 220
    new-instance v0, Lcom/squareup/protos/common/client/Version$PrereleaseType;

    const/4 v1, 0x0

    const-string v2, "ALPHA"

    const/16 v3, 0xa

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/protos/common/client/Version$PrereleaseType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Version$PrereleaseType;->ALPHA:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    .line 222
    new-instance v0, Lcom/squareup/protos/common/client/Version$PrereleaseType;

    const/4 v2, 0x1

    const-string v3, "BETA"

    const/16 v4, 0x14

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/protos/common/client/Version$PrereleaseType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Version$PrereleaseType;->BETA:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    .line 224
    new-instance v0, Lcom/squareup/protos/common/client/Version$PrereleaseType;

    const/4 v3, 0x2

    const-string v4, "PRERELEASE"

    const/16 v5, 0x1e

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/protos/common/client/Version$PrereleaseType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Version$PrereleaseType;->PRERELEASE:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    .line 226
    new-instance v0, Lcom/squareup/protos/common/client/Version$PrereleaseType;

    const/4 v4, 0x3

    const-string v5, "RELEASE_CANDIDATE"

    const/16 v6, 0x28

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/protos/common/client/Version$PrereleaseType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Version$PrereleaseType;->RELEASE_CANDIDATE:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/common/client/Version$PrereleaseType;

    .line 219
    sget-object v5, Lcom/squareup/protos/common/client/Version$PrereleaseType;->ALPHA:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/common/client/Version$PrereleaseType;->BETA:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/client/Version$PrereleaseType;->PRERELEASE:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/common/client/Version$PrereleaseType;->RELEASE_CANDIDATE:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/common/client/Version$PrereleaseType;->$VALUES:[Lcom/squareup/protos/common/client/Version$PrereleaseType;

    .line 228
    new-instance v0, Lcom/squareup/protos/common/client/Version$PrereleaseType$ProtoAdapter_PrereleaseType;

    invoke-direct {v0}, Lcom/squareup/protos/common/client/Version$PrereleaseType$ProtoAdapter_PrereleaseType;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/client/Version$PrereleaseType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 232
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 233
    iput p3, p0, Lcom/squareup/protos/common/client/Version$PrereleaseType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/common/client/Version$PrereleaseType;
    .locals 1

    const/16 v0, 0xa

    if-eq p0, v0, :cond_3

    const/16 v0, 0x14

    if-eq p0, v0, :cond_2

    const/16 v0, 0x1e

    if-eq p0, v0, :cond_1

    const/16 v0, 0x28

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 244
    :cond_0
    sget-object p0, Lcom/squareup/protos/common/client/Version$PrereleaseType;->RELEASE_CANDIDATE:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    return-object p0

    .line 243
    :cond_1
    sget-object p0, Lcom/squareup/protos/common/client/Version$PrereleaseType;->PRERELEASE:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    return-object p0

    .line 242
    :cond_2
    sget-object p0, Lcom/squareup/protos/common/client/Version$PrereleaseType;->BETA:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    return-object p0

    .line 241
    :cond_3
    sget-object p0, Lcom/squareup/protos/common/client/Version$PrereleaseType;->ALPHA:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/client/Version$PrereleaseType;
    .locals 1

    .line 219
    const-class v0, Lcom/squareup/protos/common/client/Version$PrereleaseType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/common/client/Version$PrereleaseType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/common/client/Version$PrereleaseType;
    .locals 1

    .line 219
    sget-object v0, Lcom/squareup/protos/common/client/Version$PrereleaseType;->$VALUES:[Lcom/squareup/protos/common/client/Version$PrereleaseType;

    invoke-virtual {v0}, [Lcom/squareup/protos/common/client/Version$PrereleaseType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/common/client/Version$PrereleaseType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 251
    iget v0, p0, Lcom/squareup/protos/common/client/Version$PrereleaseType;->value:I

    return v0
.end method
