.class final Lcom/squareup/protos/common/CurrencyCode$ProtoAdapter_CurrencyCode;
.super Lcom/squareup/wire/EnumAdapter;
.source "CurrencyCode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/CurrencyCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CurrencyCode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/common/CurrencyCode;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 583
    const-class v0, Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 588
    invoke-static {p1}, Lcom/squareup/protos/common/CurrencyCode;->fromValue(I)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 581
    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/CurrencyCode$ProtoAdapter_CurrencyCode;->fromValue(I)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    return-object p1
.end method
