.class public final Lcom/squareup/protos/common/RGBAColor$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RGBAColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/RGBAColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/RGBAColor;",
        "Lcom/squareup/protos/common/RGBAColor$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/Double;

.field public b:Ljava/lang/Double;

.field public g:Ljava/lang/Double;

.field public r:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 128
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Double;)Lcom/squareup/protos/common/RGBAColor$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/common/RGBAColor$Builder;->a:Ljava/lang/Double;

    return-object p0
.end method

.method public b(Ljava/lang/Double;)Lcom/squareup/protos/common/RGBAColor$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/common/RGBAColor$Builder;->b:Ljava/lang/Double;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/common/RGBAColor;
    .locals 7

    .line 153
    new-instance v6, Lcom/squareup/protos/common/RGBAColor;

    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor$Builder;->r:Ljava/lang/Double;

    iget-object v2, p0, Lcom/squareup/protos/common/RGBAColor$Builder;->g:Ljava/lang/Double;

    iget-object v3, p0, Lcom/squareup/protos/common/RGBAColor$Builder;->b:Ljava/lang/Double;

    iget-object v4, p0, Lcom/squareup/protos/common/RGBAColor$Builder;->a:Ljava/lang/Double;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/common/RGBAColor;-><init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 119
    invoke-virtual {p0}, Lcom/squareup/protos/common/RGBAColor$Builder;->build()Lcom/squareup/protos/common/RGBAColor;

    move-result-object v0

    return-object v0
.end method

.method public g(Ljava/lang/Double;)Lcom/squareup/protos/common/RGBAColor$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/common/RGBAColor$Builder;->g:Ljava/lang/Double;

    return-object p0
.end method

.method public r(Ljava/lang/Double;)Lcom/squareup/protos/common/RGBAColor$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/common/RGBAColor$Builder;->r:Ljava/lang/Double;

    return-object p0
.end method
