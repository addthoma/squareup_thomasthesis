.class public final enum Lcom/squareup/protos/common/client/Product$BuildType;
.super Ljava/lang/Enum;
.source "Product.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/client/Product;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BuildType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/client/Product$BuildType$ProtoAdapter_BuildType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/common/client/Product$BuildType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/common/client/Product$BuildType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/client/Product$BuildType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALPHA:Lcom/squareup/protos/common/client/Product$BuildType;

.field public static final enum BETA:Lcom/squareup/protos/common/client/Product$BuildType;

.field public static final enum DEVELOPMENT:Lcom/squareup/protos/common/client/Product$BuildType;

.field public static final enum DOGFOOD:Lcom/squareup/protos/common/client/Product$BuildType;

.field public static final enum RELEASE:Lcom/squareup/protos/common/client/Product$BuildType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 271
    new-instance v0, Lcom/squareup/protos/common/client/Product$BuildType;

    const/4 v1, 0x0

    const-string v2, "DEVELOPMENT"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/common/client/Product$BuildType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Product$BuildType;->DEVELOPMENT:Lcom/squareup/protos/common/client/Product$BuildType;

    .line 273
    new-instance v0, Lcom/squareup/protos/common/client/Product$BuildType;

    const/4 v2, 0x1

    const-string v3, "ALPHA"

    const/16 v4, 0xa

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/protos/common/client/Product$BuildType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Product$BuildType;->ALPHA:Lcom/squareup/protos/common/client/Product$BuildType;

    .line 275
    new-instance v0, Lcom/squareup/protos/common/client/Product$BuildType;

    const/4 v3, 0x2

    const-string v4, "DOGFOOD"

    const/16 v5, 0xf

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/protos/common/client/Product$BuildType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Product$BuildType;->DOGFOOD:Lcom/squareup/protos/common/client/Product$BuildType;

    .line 277
    new-instance v0, Lcom/squareup/protos/common/client/Product$BuildType;

    const/4 v4, 0x3

    const-string v5, "BETA"

    const/16 v6, 0x14

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/protos/common/client/Product$BuildType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Product$BuildType;->BETA:Lcom/squareup/protos/common/client/Product$BuildType;

    .line 279
    new-instance v0, Lcom/squareup/protos/common/client/Product$BuildType;

    const/4 v5, 0x4

    const-string v6, "RELEASE"

    const/16 v7, 0x1e

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/protos/common/client/Product$BuildType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Product$BuildType;->RELEASE:Lcom/squareup/protos/common/client/Product$BuildType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/common/client/Product$BuildType;

    .line 270
    sget-object v6, Lcom/squareup/protos/common/client/Product$BuildType;->DEVELOPMENT:Lcom/squareup/protos/common/client/Product$BuildType;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/common/client/Product$BuildType;->ALPHA:Lcom/squareup/protos/common/client/Product$BuildType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/client/Product$BuildType;->DOGFOOD:Lcom/squareup/protos/common/client/Product$BuildType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/common/client/Product$BuildType;->BETA:Lcom/squareup/protos/common/client/Product$BuildType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/common/client/Product$BuildType;->RELEASE:Lcom/squareup/protos/common/client/Product$BuildType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/common/client/Product$BuildType;->$VALUES:[Lcom/squareup/protos/common/client/Product$BuildType;

    .line 281
    new-instance v0, Lcom/squareup/protos/common/client/Product$BuildType$ProtoAdapter_BuildType;

    invoke-direct {v0}, Lcom/squareup/protos/common/client/Product$BuildType$ProtoAdapter_BuildType;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/client/Product$BuildType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 285
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 286
    iput p3, p0, Lcom/squareup/protos/common/client/Product$BuildType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/common/client/Product$BuildType;
    .locals 1

    if-eqz p0, :cond_4

    const/16 v0, 0xa

    if-eq p0, v0, :cond_3

    const/16 v0, 0xf

    if-eq p0, v0, :cond_2

    const/16 v0, 0x14

    if-eq p0, v0, :cond_1

    const/16 v0, 0x1e

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 298
    :cond_0
    sget-object p0, Lcom/squareup/protos/common/client/Product$BuildType;->RELEASE:Lcom/squareup/protos/common/client/Product$BuildType;

    return-object p0

    .line 297
    :cond_1
    sget-object p0, Lcom/squareup/protos/common/client/Product$BuildType;->BETA:Lcom/squareup/protos/common/client/Product$BuildType;

    return-object p0

    .line 296
    :cond_2
    sget-object p0, Lcom/squareup/protos/common/client/Product$BuildType;->DOGFOOD:Lcom/squareup/protos/common/client/Product$BuildType;

    return-object p0

    .line 295
    :cond_3
    sget-object p0, Lcom/squareup/protos/common/client/Product$BuildType;->ALPHA:Lcom/squareup/protos/common/client/Product$BuildType;

    return-object p0

    .line 294
    :cond_4
    sget-object p0, Lcom/squareup/protos/common/client/Product$BuildType;->DEVELOPMENT:Lcom/squareup/protos/common/client/Product$BuildType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/client/Product$BuildType;
    .locals 1

    .line 270
    const-class v0, Lcom/squareup/protos/common/client/Product$BuildType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/common/client/Product$BuildType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/common/client/Product$BuildType;
    .locals 1

    .line 270
    sget-object v0, Lcom/squareup/protos/common/client/Product$BuildType;->$VALUES:[Lcom/squareup/protos/common/client/Product$BuildType;

    invoke-virtual {v0}, [Lcom/squareup/protos/common/client/Product$BuildType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/common/client/Product$BuildType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 305
    iget v0, p0, Lcom/squareup/protos/common/client/Product$BuildType;->value:I

    return v0
.end method
