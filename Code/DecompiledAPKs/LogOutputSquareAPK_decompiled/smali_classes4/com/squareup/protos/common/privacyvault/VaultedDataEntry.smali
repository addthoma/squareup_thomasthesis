.class public final Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;
.super Lcom/squareup/wire/Message;
.source "VaultedDataEntry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$ProtoAdapter_VaultedDataEntry;,
        Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;",
        "Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final entity:Lcom/squareup/protos/privacyvault/model/Entity;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.privacyvault.model.Entity#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final vaulted_fields:Ljava/util/Map;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.privacyvault.service.Protected#ADAPTER"
        keyAdapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/privacyvault/service/Protected;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$ProtoAdapter_VaultedDataEntry;

    invoke-direct {v0}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$ProtoAdapter_VaultedDataEntry;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/privacyvault/model/Entity;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/privacyvault/model/Entity;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/privacyvault/service/Protected;",
            ">;)V"
        }
    .end annotation

    .line 46
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;-><init>(Lcom/squareup/protos/privacyvault/model/Entity;Ljava/util/Map;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/privacyvault/model/Entity;Ljava/util/Map;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/privacyvault/model/Entity;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/privacyvault/service/Protected;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 51
    sget-object v0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 52
    iput-object p1, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->entity:Lcom/squareup/protos/privacyvault/model/Entity;

    const-string/jumbo p1, "vaulted_fields"

    .line 53
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->vaulted_fields:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 68
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 69
    :cond_1
    check-cast p1, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;

    .line 70
    invoke-virtual {p0}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->entity:Lcom/squareup/protos/privacyvault/model/Entity;

    iget-object v3, p1, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->entity:Lcom/squareup/protos/privacyvault/model/Entity;

    .line 71
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->vaulted_fields:Ljava/util/Map;

    iget-object p1, p1, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->vaulted_fields:Ljava/util/Map;

    .line 72
    invoke-interface {v1, p1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 77
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 80
    iget-object v1, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->entity:Lcom/squareup/protos/privacyvault/model/Entity;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/privacyvault/model/Entity;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->vaulted_fields:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;
    .locals 2

    .line 58
    new-instance v0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;-><init>()V

    .line 59
    iget-object v1, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->entity:Lcom/squareup/protos/privacyvault/model/Entity;

    iput-object v1, v0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->entity:Lcom/squareup/protos/privacyvault/model/Entity;

    .line 60
    iget-object v1, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->vaulted_fields:Ljava/util/Map;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->vaulted_fields:Ljava/util/Map;

    .line 61
    invoke-virtual {p0}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->newBuilder()Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->entity:Lcom/squareup/protos/privacyvault/model/Entity;

    if-eqz v1, :cond_0

    const-string v1, ", entity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->entity:Lcom/squareup/protos/privacyvault/model/Entity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 91
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->vaulted_fields:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", vaulted_fields="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->vaulted_fields:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "VaultedDataEntry{"

    .line 92
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
