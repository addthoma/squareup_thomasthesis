.class public final Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BalanceActivityEntityView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/BalanceActivityEntityView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/deposits/BalanceActivityEntityView;",
        "Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public instrument:Lcom/squareup/protos/deposits/InstrumentView;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/deposits/BalanceActivityEntityView;
    .locals 4

    .line 128
    new-instance v0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;->instrument:Lcom/squareup/protos/deposits/InstrumentView;

    iget-object v2, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;->unit_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/deposits/BalanceActivityEntityView;-><init>(Lcom/squareup/protos/deposits/InstrumentView;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;->build()Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    move-result-object v0

    return-object v0
.end method

.method public instrument(Lcom/squareup/protos/deposits/InstrumentView;)Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;->instrument:Lcom/squareup/protos/deposits/InstrumentView;

    const/4 p1, 0x0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;->unit_token:Ljava/lang/String;

    const/4 p1, 0x0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;->instrument:Lcom/squareup/protos/deposits/InstrumentView;

    return-object p0
.end method
