.class final Lcom/squareup/protos/deposits/SweepFailureReason$ProtoAdapter_SweepFailureReason;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SweepFailureReason.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/SweepFailureReason;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SweepFailureReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/deposits/SweepFailureReason;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 244
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/deposits/SweepFailureReason;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/deposits/SweepFailureReason;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 269
    new-instance v0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;-><init>()V

    .line 270
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 271
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 314
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 307
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->wallet_pull_failure(Lcom/squareup/protos/connect/v2/resources/Error$Code;)Lcom/squareup/protos/deposits/SweepFailureReason$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 309
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 299
    :cond_1
    :try_start_1
    sget-object v4, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->bookkeeper_push_failure(Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;)Lcom/squareup/protos/deposits/SweepFailureReason$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 301
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 291
    :cond_2
    :try_start_2
    sget-object v4, Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->bookkeeper_pull_failure(Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;)Lcom/squareup/protos/deposits/SweepFailureReason$Builder;
    :try_end_2
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v4

    .line 293
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 283
    :cond_3
    :try_start_3
    sget-object v4, Lcom/squareup/protos/teller/FailureReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/teller/FailureReason;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->teller_failure(Lcom/squareup/protos/teller/FailureReason;)Lcom/squareup/protos/deposits/SweepFailureReason$Builder;
    :try_end_3
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_3
    move-exception v4

    .line 285
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 275
    :cond_4
    :try_start_4
    sget-object v4, Lcom/squareup/protos/payments/common/PushMoneyError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/payments/common/PushMoneyError;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->esperanto_failure(Lcom/squareup/protos/payments/common/PushMoneyError;)Lcom/squareup/protos/deposits/SweepFailureReason$Builder;
    :try_end_4
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v4

    .line 277
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 318
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 319
    invoke-virtual {v0}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->build()Lcom/squareup/protos/deposits/SweepFailureReason;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 242
    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/SweepFailureReason$ProtoAdapter_SweepFailureReason;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/deposits/SweepFailureReason;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/deposits/SweepFailureReason;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 259
    sget-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/SweepFailureReason;->esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 260
    sget-object v0, Lcom/squareup/protos/teller/FailureReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/SweepFailureReason;->teller_failure:Lcom/squareup/protos/teller/FailureReason;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 261
    sget-object v0, Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 262
    sget-object v0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 263
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/SweepFailureReason;->wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 264
    invoke-virtual {p2}, Lcom/squareup/protos/deposits/SweepFailureReason;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 242
    check-cast p2, Lcom/squareup/protos/deposits/SweepFailureReason;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/deposits/SweepFailureReason$ProtoAdapter_SweepFailureReason;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/deposits/SweepFailureReason;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/deposits/SweepFailureReason;)I
    .locals 4

    .line 249
    sget-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/SweepFailureReason;->esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/teller/FailureReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/SweepFailureReason;->teller_failure:Lcom/squareup/protos/teller/FailureReason;

    const/4 v3, 0x2

    .line 250
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    const/4 v3, 0x3

    .line 251
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    const/4 v3, 0x4

    .line 252
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/SweepFailureReason;->wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/4 v3, 0x5

    .line 253
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 254
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/SweepFailureReason;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 242
    check-cast p1, Lcom/squareup/protos/deposits/SweepFailureReason;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/SweepFailureReason$ProtoAdapter_SweepFailureReason;->encodedSize(Lcom/squareup/protos/deposits/SweepFailureReason;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/deposits/SweepFailureReason;)Lcom/squareup/protos/deposits/SweepFailureReason;
    .locals 0

    .line 324
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/SweepFailureReason;->newBuilder()Lcom/squareup/protos/deposits/SweepFailureReason$Builder;

    move-result-object p1

    .line 325
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 326
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->build()Lcom/squareup/protos/deposits/SweepFailureReason;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 242
    check-cast p1, Lcom/squareup/protos/deposits/SweepFailureReason;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/SweepFailureReason$ProtoAdapter_SweepFailureReason;->redact(Lcom/squareup/protos/deposits/SweepFailureReason;)Lcom/squareup/protos/deposits/SweepFailureReason;

    move-result-object p1

    return-object p1
.end method
