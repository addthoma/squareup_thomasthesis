.class public final Lcom/squareup/protos/deposits/InstrumentView$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InstrumentView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/InstrumentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/deposits/InstrumentView;",
        "Lcom/squareup/protos/deposits/InstrumentView$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bank_account_number_suffix:Ljava/lang/String;

.field public bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

.field public card_pan_suffix:Ljava/lang/String;

.field public card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

.field public instrument_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 164
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bank_account_number_suffix(Ljava/lang/String;)Lcom/squareup/protos/deposits/InstrumentView$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 190
    iput-object p1, p0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->bank_account_number_suffix:Ljava/lang/String;

    const/4 p1, 0x0

    .line 191
    iput-object p1, p0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->card_pan_suffix:Ljava/lang/String;

    return-object p0
.end method

.method public bank_account_summary(Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;)Lcom/squareup/protos/deposits/InstrumentView$Builder;
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    const/4 p1, 0x0

    .line 209
    iput-object p1, p0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/deposits/InstrumentView;
    .locals 8

    .line 215
    new-instance v7, Lcom/squareup/protos/deposits/InstrumentView;

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->instrument_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->card_pan_suffix:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->bank_account_number_suffix:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    iget-object v5, p0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/deposits/InstrumentView;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/deposits/InstrumentView$CardSummary;Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 153
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/InstrumentView$Builder;->build()Lcom/squareup/protos/deposits/InstrumentView;

    move-result-object v0

    return-object v0
.end method

.method public card_pan_suffix(Ljava/lang/String;)Lcom/squareup/protos/deposits/InstrumentView$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 180
    iput-object p1, p0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->card_pan_suffix:Ljava/lang/String;

    const/4 p1, 0x0

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->bank_account_number_suffix:Ljava/lang/String;

    return-object p0
.end method

.method public card_summary(Lcom/squareup/protos/deposits/InstrumentView$CardSummary;)Lcom/squareup/protos/deposits/InstrumentView$Builder;
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    const/4 p1, 0x0

    .line 200
    iput-object p1, p0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    return-object p0
.end method

.method public instrument_token(Ljava/lang/String;)Lcom/squareup/protos/deposits/InstrumentView$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->instrument_token:Ljava/lang/String;

    return-object p0
.end method
