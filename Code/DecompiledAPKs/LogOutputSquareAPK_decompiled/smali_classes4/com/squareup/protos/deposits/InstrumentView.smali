.class public final Lcom/squareup/protos/deposits/InstrumentView;
.super Lcom/squareup/wire/Message;
.source "InstrumentView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/deposits/InstrumentView$ProtoAdapter_InstrumentView;,
        Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;,
        Lcom/squareup/protos/deposits/InstrumentView$CardSummary;,
        Lcom/squareup/protos/deposits/InstrumentView$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/deposits/InstrumentView;",
        "Lcom/squareup/protos/deposits/InstrumentView$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/deposits/InstrumentView;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BANK_ACCOUNT_NUMBER_SUFFIX:Ljava/lang/String; = ""

.field public static final DEFAULT_CARD_PAN_SUFFIX:Ljava/lang/String; = ""

.field public static final DEFAULT_INSTRUMENT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final bank_account_number_suffix:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.deposits.InstrumentView$BankAccountSummary#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final card_pan_suffix:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.deposits.InstrumentView$CardSummary#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final instrument_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/deposits/InstrumentView$ProtoAdapter_InstrumentView;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/InstrumentView$ProtoAdapter_InstrumentView;-><init>()V

    sput-object v0, Lcom/squareup/protos/deposits/InstrumentView;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/deposits/InstrumentView$CardSummary;Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;)V
    .locals 7

    .line 82
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/deposits/InstrumentView;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/deposits/InstrumentView$CardSummary;Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/deposits/InstrumentView$CardSummary;Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;Lokio/ByteString;)V
    .locals 1

    .line 88
    sget-object v0, Lcom/squareup/protos/deposits/InstrumentView;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 89
    invoke-static {p2, p3}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p6

    const/4 v0, 0x1

    if-gt p6, v0, :cond_1

    .line 92
    invoke-static {p4, p5}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p6

    if-gt p6, v0, :cond_0

    .line 95
    iput-object p1, p0, Lcom/squareup/protos/deposits/InstrumentView;->instrument_token:Ljava/lang/String;

    .line 96
    iput-object p2, p0, Lcom/squareup/protos/deposits/InstrumentView;->card_pan_suffix:Ljava/lang/String;

    .line 97
    iput-object p3, p0, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_number_suffix:Ljava/lang/String;

    .line 98
    iput-object p4, p0, Lcom/squareup/protos/deposits/InstrumentView;->card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    .line 99
    iput-object p5, p0, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    return-void

    .line 93
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of card_summary, bank_account_summary may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 90
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of card_pan_suffix, bank_account_number_suffix may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 117
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/deposits/InstrumentView;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 118
    :cond_1
    check-cast p1, Lcom/squareup/protos/deposits/InstrumentView;

    .line 119
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/InstrumentView;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/deposits/InstrumentView;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->instrument_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/deposits/InstrumentView;->instrument_token:Ljava/lang/String;

    .line 120
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->card_pan_suffix:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/deposits/InstrumentView;->card_pan_suffix:Ljava/lang/String;

    .line 121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_number_suffix:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_number_suffix:Ljava/lang/String;

    .line 122
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    iget-object v3, p1, Lcom/squareup/protos/deposits/InstrumentView;->card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    .line 123
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    iget-object p1, p1, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    .line 124
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 129
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/InstrumentView;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->instrument_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->card_pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_number_suffix:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 137
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/deposits/InstrumentView$Builder;
    .locals 2

    .line 104
    new-instance v0, Lcom/squareup/protos/deposits/InstrumentView$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/InstrumentView$Builder;-><init>()V

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->instrument_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->instrument_token:Ljava/lang/String;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->card_pan_suffix:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->card_pan_suffix:Ljava/lang/String;

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_number_suffix:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->bank_account_number_suffix:Ljava/lang/String;

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    iput-object v1, v0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    iput-object v1, v0, Lcom/squareup/protos/deposits/InstrumentView$Builder;->bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/InstrumentView;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/deposits/InstrumentView$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/InstrumentView;->newBuilder()Lcom/squareup/protos/deposits/InstrumentView$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->instrument_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", instrument_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->instrument_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->card_pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", card_pan_suffix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->card_pan_suffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_number_suffix:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", bank_account_number_suffix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_number_suffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    if-eqz v1, :cond_3

    const-string v1, ", card_summary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 149
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    if-eqz v1, :cond_4

    const-string v1, ", bank_account_summary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InstrumentView{"

    .line 150
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
