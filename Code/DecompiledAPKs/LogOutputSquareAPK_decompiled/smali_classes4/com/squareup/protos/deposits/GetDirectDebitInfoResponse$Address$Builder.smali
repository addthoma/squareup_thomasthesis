.class public final Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetDirectDebitInfoResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;",
        "Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public address:Lcom/squareup/protos/common/location/GlobalAddress;

.field public recipient:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 384
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;
    .locals 0

    .line 399
    iput-object p1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;
    .locals 4

    .line 405
    new-instance v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;->recipient:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 379
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;->build()Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    move-result-object v0

    return-object v0
.end method

.method public recipient(Ljava/lang/String;)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;
    .locals 0

    .line 391
    iput-object p1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;->recipient:Ljava/lang/String;

    return-object p0
.end method
