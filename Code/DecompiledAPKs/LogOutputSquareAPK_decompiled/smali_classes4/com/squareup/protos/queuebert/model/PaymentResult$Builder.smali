.class public final Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PaymentResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/queuebert/model/PaymentResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/queuebert/model/PaymentResult;",
        "Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error_message:Ljava/lang/String;

.field public error_title:Ljava/lang/String;

.field public payment_id:Ljava/lang/String;

.field public status:Lcom/squareup/protos/queuebert/model/PaymentStatus;

.field public unique_key:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 150
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/queuebert/model/PaymentResult;
    .locals 8

    .line 189
    new-instance v7, Lcom/squareup/protos/queuebert/model/PaymentResult;

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->unique_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->payment_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->status:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    iget-object v4, p0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->error_title:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->error_message:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/queuebert/model/PaymentResult;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/queuebert/model/PaymentStatus;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 139
    invoke-virtual {p0}, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->build()Lcom/squareup/protos/queuebert/model/PaymentResult;

    move-result-object v0

    return-object v0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public error_title(Ljava/lang/String;)Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->error_title:Ljava/lang/String;

    return-object p0
.end method

.method public payment_id(Ljava/lang/String;)Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->payment_id:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/queuebert/model/PaymentStatus;)Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->status:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    return-object p0
.end method

.method public unique_key(Ljava/lang/String;)Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;
    .locals 0

    .line 157
    iput-object p1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->unique_key:Ljava/lang/String;

    return-object p0
.end method
