.class public final Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetSubscriptionInvitationResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;",
        "Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public invitation:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;
    .locals 3

    .line 88
    new-instance v0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$Builder;->invitation:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;-><init>(Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$Builder;->build()Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;

    move-result-object v0

    return-object v0
.end method

.method public invitation(Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;)Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$Builder;
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$Builder;->invitation:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    return-object p0
.end method
