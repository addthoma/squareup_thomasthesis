.class public final Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SubscribeResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/postoffice/sms/SubscribeResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/postoffice/sms/SubscribeResponse;",
        "Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public status:Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 81
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/postoffice/sms/SubscribeResponse;
    .locals 3

    .line 91
    new-instance v0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse;

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Builder;->status:Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/postoffice/sms/SubscribeResponse;-><init>(Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Builder;->build()Lcom/squareup/protos/postoffice/sms/SubscribeResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;)Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Builder;
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Builder;->status:Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    return-object p0
.end method
