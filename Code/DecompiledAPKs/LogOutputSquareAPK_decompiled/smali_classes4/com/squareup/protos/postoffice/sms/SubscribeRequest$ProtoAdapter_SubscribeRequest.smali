.class final Lcom/squareup/protos/postoffice/sms/SubscribeRequest$ProtoAdapter_SubscribeRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SubscribeRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/postoffice/sms/SubscribeRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SubscribeRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/postoffice/sms/SubscribeRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 292
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/postoffice/sms/SubscribeRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 315
    new-instance v0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;-><init>()V

    .line 316
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 317
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 324
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 322
    :cond_0
    sget-object v3, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->metadata(Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;)Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;

    goto :goto_0

    .line 321
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->invitation_id(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;

    goto :goto_0

    .line 320
    :cond_2
    sget-object v3, Lcom/squareup/protos/postoffice/sms/Subscriber;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/postoffice/sms/Subscriber;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->subscriber(Lcom/squareup/protos/postoffice/sms/Subscriber;)Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;

    goto :goto_0

    .line 319
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;

    goto :goto_0

    .line 328
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 329
    invoke-virtual {v0}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->build()Lcom/squareup/protos/postoffice/sms/SubscribeRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 290
    invoke-virtual {p0, p1}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$ProtoAdapter_SubscribeRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/postoffice/sms/SubscribeRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/postoffice/sms/SubscribeRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 306
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 307
    sget-object v0, Lcom/squareup/protos/postoffice/sms/Subscriber;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 308
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;->invitation_id:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 309
    sget-object v0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;->metadata:Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 310
    invoke-virtual {p2}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 290
    check-cast p2, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$ProtoAdapter_SubscribeRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/postoffice/sms/SubscribeRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/postoffice/sms/SubscribeRequest;)I
    .locals 4

    .line 297
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/postoffice/sms/Subscriber;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    const/4 v3, 0x2

    .line 298
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;->invitation_id:Ljava/lang/String;

    const/4 v3, 0x3

    .line 299
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;->metadata:Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;

    const/4 v3, 0x4

    .line 300
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 301
    invoke-virtual {p1}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 290
    check-cast p1, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$ProtoAdapter_SubscribeRequest;->encodedSize(Lcom/squareup/protos/postoffice/sms/SubscribeRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/postoffice/sms/SubscribeRequest;)Lcom/squareup/protos/postoffice/sms/SubscribeRequest;
    .locals 2

    .line 334
    invoke-virtual {p1}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;->newBuilder()Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;

    move-result-object p1

    .line 335
    iget-object v0, p1, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/postoffice/sms/Subscriber;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/postoffice/sms/Subscriber;

    iput-object v0, p1, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    .line 336
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->metadata:Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->metadata:Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;

    iput-object v0, p1, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->metadata:Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;

    .line 337
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 338
    invoke-virtual {p1}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->build()Lcom/squareup/protos/postoffice/sms/SubscribeRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 290
    check-cast p1, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$ProtoAdapter_SubscribeRequest;->redact(Lcom/squareup/protos/postoffice/sms/SubscribeRequest;)Lcom/squareup/protos/postoffice/sms/SubscribeRequest;

    move-result-object p1

    return-object p1
.end method
