.class public final Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SubscribeRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/postoffice/sms/SubscribeRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/postoffice/sms/SubscribeRequest;",
        "Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public invitation_id:Ljava/lang/String;

.field public merchant_token:Ljava/lang/String;

.field public metadata:Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;

.field public subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 122
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/postoffice/sms/SubscribeRequest;
    .locals 7

    .line 147
    new-instance v6, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    iget-object v3, p0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->invitation_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->metadata:Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/postoffice/sms/Subscriber;Ljava/lang/String;Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->build()Lcom/squareup/protos/postoffice/sms/SubscribeRequest;

    move-result-object v0

    return-object v0
.end method

.method public invitation_id(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->invitation_id:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public metadata(Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;)Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->metadata:Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;

    return-object p0
.end method

.method public subscriber(Lcom/squareup/protos/postoffice/sms/Subscriber;)Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Builder;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    return-object p0
.end method
