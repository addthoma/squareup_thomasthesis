.class public final Lcom/squareup/protos/precog/ProductInterestWeight$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ProductInterestWeight.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/precog/ProductInterestWeight;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/precog/ProductInterestWeight;",
        "Lcom/squareup/protos/precog/ProductInterestWeight$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public weight:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 83
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/precog/ProductInterestWeight;
    .locals 3

    .line 96
    new-instance v0, Lcom/squareup/protos/precog/ProductInterestWeight;

    iget-object v1, p0, Lcom/squareup/protos/precog/ProductInterestWeight$Builder;->weight:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/precog/ProductInterestWeight;-><init>(Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 80
    invoke-virtual {p0}, Lcom/squareup/protos/precog/ProductInterestWeight$Builder;->build()Lcom/squareup/protos/precog/ProductInterestWeight;

    move-result-object v0

    return-object v0
.end method

.method public weight(Ljava/lang/Integer;)Lcom/squareup/protos/precog/ProductInterestWeight$Builder;
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/squareup/protos/precog/ProductInterestWeight$Builder;->weight:Ljava/lang/Integer;

    return-object p0
.end method
