.class public final Lcom/squareup/protos/multipass/common/TwoFactorDetails;
.super Lcom/squareup/wire/Message;
.source "TwoFactorDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/multipass/common/TwoFactorDetails$ProtoAdapter_TwoFactorDetails;,
        Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CREATED_AT:Ljava/lang/Long;

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_SET_TRUSTED_DEVICE:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final created_at:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x5
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.common.GoogleauthTwoFactor#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final set_trusted_device:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.common.SmsTwoFactor#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.common.WebauthnTwoFactor#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$ProtoAdapter_TwoFactorDetails;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$ProtoAdapter_TwoFactorDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->DEFAULT_SET_TRUSTED_DEVICE:Ljava/lang/Boolean;

    const-wide/16 v0, 0x0

    .line 33
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->DEFAULT_CREATED_AT:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/squareup/protos/multipass/common/SmsTwoFactor;Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;)V
    .locals 8

    .line 82
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/multipass/common/TwoFactorDetails;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/squareup/protos/multipass/common/SmsTwoFactor;Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/squareup/protos/multipass/common/SmsTwoFactor;Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;Lokio/ByteString;)V
    .locals 1

    .line 88
    sget-object v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 89
    invoke-static {p4, p5, p6}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p7

    const/4 v0, 0x1

    if-gt p7, v0, :cond_0

    .line 92
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->description:Ljava/lang/String;

    .line 93
    iput-object p2, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->set_trusted_device:Ljava/lang/Boolean;

    .line 94
    iput-object p3, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->created_at:Ljava/lang/Long;

    .line 95
    iput-object p4, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    .line 96
    iput-object p5, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    .line 97
    iput-object p6, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    return-void

    .line 90
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of sms, googleauth, webauthn may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 116
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 117
    :cond_1
    check-cast p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->description:Ljava/lang/String;

    .line 119
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->set_trusted_device:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->set_trusted_device:Ljava/lang/Boolean;

    .line 120
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->created_at:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->created_at:Ljava/lang/Long;

    .line 121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    iget-object v3, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    .line 122
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    iget-object v3, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    .line 123
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    iget-object p1, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    .line 124
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 129
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->description:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->set_trusted_device:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->created_at:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 138
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;
    .locals 2

    .line 102
    new-instance v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;-><init>()V

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->description:Ljava/lang/String;

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->set_trusted_device:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->set_trusted_device:Ljava/lang/Boolean;

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->created_at:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->created_at:Ljava/lang/Long;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    iput-object v1, v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    iput-object v1, v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    iput-object v1, v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->newBuilder()Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->description:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->set_trusted_device:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", set_trusted_device="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->set_trusted_device:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 148
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->created_at:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->created_at:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 149
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    if-eqz v1, :cond_3

    const-string v1, ", sms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 150
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    if-eqz v1, :cond_4

    const-string v1, ", googleauth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 151
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    if-eqz v1, :cond_5

    const-string v1, ", webauthn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "TwoFactorDetails{"

    .line 152
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
