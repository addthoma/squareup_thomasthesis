.class public final Lcom/squareup/protos/multipass/mobile/Alert$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Alert.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/mobile/Alert;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/multipass/mobile/Alert;",
        "Lcom/squareup/protos/multipass/mobile/Alert$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public message:Ljava/lang/String;

.field public other_buttons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/mobile/AlertButton;",
            ">;"
        }
    .end annotation
.end field

.field public primary_button:Lcom/squareup/protos/multipass/mobile/AlertButton;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 140
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 141
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/multipass/mobile/Alert$Builder;->other_buttons:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/multipass/mobile/Alert;
    .locals 7

    .line 179
    new-instance v6, Lcom/squareup/protos/multipass/mobile/Alert;

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert$Builder;->title:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/multipass/mobile/Alert$Builder;->message:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/multipass/mobile/Alert$Builder;->primary_button:Lcom/squareup/protos/multipass/mobile/AlertButton;

    iget-object v4, p0, Lcom/squareup/protos/multipass/mobile/Alert$Builder;->other_buttons:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/multipass/mobile/Alert;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/multipass/mobile/AlertButton;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/Alert$Builder;->build()Lcom/squareup/protos/multipass/mobile/Alert;

    move-result-object v0

    return-object v0
.end method

.method public message(Ljava/lang/String;)Lcom/squareup/protos/multipass/mobile/Alert$Builder;
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/Alert$Builder;->message:Ljava/lang/String;

    return-object p0
.end method

.method public other_buttons(Ljava/util/List;)Lcom/squareup/protos/multipass/mobile/Alert$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/mobile/AlertButton;",
            ">;)",
            "Lcom/squareup/protos/multipass/mobile/Alert$Builder;"
        }
    .end annotation

    .line 172
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/Alert$Builder;->other_buttons:Ljava/util/List;

    return-object p0
.end method

.method public primary_button(Lcom/squareup/protos/multipass/mobile/AlertButton;)Lcom/squareup/protos/multipass/mobile/Alert$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/Alert$Builder;->primary_button:Lcom/squareup/protos/multipass/mobile/AlertButton;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/protos/multipass/mobile/Alert$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/Alert$Builder;->title:Ljava/lang/String;

    return-object p0
.end method
