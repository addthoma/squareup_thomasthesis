.class public final Lcom/squareup/protos/multipass/service/OneTimeKey$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OneTimeKey.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/service/OneTimeKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/multipass/service/OneTimeKey;",
        "Lcom/squareup/protos/multipass/service/OneTimeKey$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public value:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 88
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/multipass/service/OneTimeKey;
    .locals 3

    .line 101
    new-instance v0, Lcom/squareup/protos/multipass/service/OneTimeKey;

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/OneTimeKey$Builder;->value:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/multipass/service/OneTimeKey;-><init>(Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 85
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/OneTimeKey$Builder;->build()Lcom/squareup/protos/multipass/service/OneTimeKey;

    move-result-object v0

    return-object v0
.end method

.method public value(Lokio/ByteString;)Lcom/squareup/protos/multipass/service/OneTimeKey$Builder;
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/OneTimeKey$Builder;->value:Lokio/ByteString;

    return-object p0
.end method
