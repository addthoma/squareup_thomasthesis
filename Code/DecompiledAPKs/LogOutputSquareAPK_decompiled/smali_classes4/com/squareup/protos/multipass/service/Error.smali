.class public final enum Lcom/squareup/protos/multipass/service/Error;
.super Ljava/lang/Enum;
.source "Error.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/multipass/service/Error$ProtoAdapter_Error;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/multipass/service/Error;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/multipass/service/Error;

.field public static final enum ACCESS_TOKEN_EXPIRED:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum ACCESS_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum ACCESS_TOKEN_REVOKED:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum ACCOUNT_ALREADY_CLAIMED:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum ACCOUNT_LOCKED:Lcom/squareup/protos/multipass/service/Error;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/multipass/service/Error;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALIAS_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum ALIAS_IN_USE:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum APPLICATION_NOT_ALLOWED:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum CAPTCHA_REQUIRED:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum CAPTCHA_RESPONSE_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum CREDENTIAL_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum DEVICE_CREDENTIAL_ALREADY_PAIRED:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum DEVICE_CREDENTIAL_EXPECTED_REGISTER_PRODUCT:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum DEVICE_CREDENTIAL_EXPECTED_RESTAURANT_PRODUCT:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum DEVICE_CREDENTIAL_EXPECTED_RETAIL_PRODUCT:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum DEVICE_CREDENTIAL_LIMIT_EXCEEDED:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum DEVICE_CREDENTIAL_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum DEVICE_DETAILS_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum EMAIL_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum FORWARD_TO_MASTER:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum IP_ADDRESS_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum MERCHANT_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum NEW_EMAIL_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum NEW_EMAIL_TAKEN:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum NOT_FOUND:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum NO_ERROR:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum OAUTH_SESSION_DISABLED:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum OTK_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum PASSWORD_COMMON:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum PASSWORD_COMPROMISED:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum PASSWORD_FAILS_REQUIREMENTS:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum PASSWORD_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum PASSWORD_REUSE:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum PERSON_TOKEN_INVALID_CASH_USER:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum PHONE_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum REDIRECT_URL_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum REPLICATION_LAG:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum SESSION_COOKIE_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum SESSION_ID_ASSUMED_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum SESSION_ID_EXPIRED:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum SESSION_ID_IDLE:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum SESSION_ID_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum SESSION_ID_INVALID_SELECTION:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum SESSION_ID_TERMINATED:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum SESSION_SECONDARY_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum TEMPORARY_LOCKOUT:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum UNIT_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum USER_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum VERIFICATION_CODE_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum VERIFICATION_CODE_NOT_SENT:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum VERIFICATION_METHOD_INVALID:Lcom/squareup/protos/multipass/service/Error;

.field public static final enum VERIFICATION_REQUIRED:Lcom/squareup/protos/multipass/service/Error;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 19
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const/4 v1, 0x0

    const-string v2, "NO_ERROR"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->NO_ERROR:Lcom/squareup/protos/multipass/service/Error;

    .line 24
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const/4 v2, 0x1

    const-string v3, "TEMPORARY_LOCKOUT"

    const/16 v4, 0x3e8

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->TEMPORARY_LOCKOUT:Lcom/squareup/protos/multipass/service/Error;

    .line 29
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const/4 v3, 0x2

    const-string v4, "EMAIL_INVALID"

    const/16 v5, 0x3e9

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->EMAIL_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 34
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const/4 v4, 0x3

    const-string v5, "USER_TOKEN_INVALID"

    const/16 v6, 0x3ea

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->USER_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 39
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const/4 v5, 0x4

    const-string v6, "NEW_EMAIL_INVALID"

    const/16 v7, 0x3eb

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->NEW_EMAIL_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 44
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const/4 v6, 0x5

    const-string v7, "NEW_EMAIL_TAKEN"

    const/16 v8, 0x3ec

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->NEW_EMAIL_TAKEN:Lcom/squareup/protos/multipass/service/Error;

    .line 49
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const/4 v7, 0x6

    const-string v8, "PHONE_INVALID"

    const/16 v9, 0x3ed

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->PHONE_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 54
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const/4 v8, 0x7

    const-string v9, "CAPTCHA_REQUIRED"

    const/16 v10, 0x3ee

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->CAPTCHA_REQUIRED:Lcom/squareup/protos/multipass/service/Error;

    .line 59
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const/16 v9, 0x8

    const-string v10, "CAPTCHA_RESPONSE_INVALID"

    const/16 v11, 0x3ef

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->CAPTCHA_RESPONSE_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 64
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const/16 v10, 0x9

    const-string v11, "FORWARD_TO_MASTER"

    const/16 v12, 0x3f0

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->FORWARD_TO_MASTER:Lcom/squareup/protos/multipass/service/Error;

    .line 69
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const/16 v11, 0xa

    const-string v12, "VERIFICATION_REQUIRED"

    const/16 v13, 0x3f1

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->VERIFICATION_REQUIRED:Lcom/squareup/protos/multipass/service/Error;

    .line 74
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const/16 v12, 0xb

    const-string v13, "VERIFICATION_METHOD_INVALID"

    const/16 v14, 0x3f2

    invoke-direct {v0, v13, v12, v14}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->VERIFICATION_METHOD_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 79
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const/16 v13, 0xc

    const-string v14, "VERIFICATION_CODE_NOT_SENT"

    const/16 v15, 0x3f3

    invoke-direct {v0, v14, v13, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->VERIFICATION_CODE_NOT_SENT:Lcom/squareup/protos/multipass/service/Error;

    .line 84
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const/16 v14, 0xd

    const-string v15, "ACCOUNT_LOCKED"

    const/16 v13, 0x3f4

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->ACCOUNT_LOCKED:Lcom/squareup/protos/multipass/service/Error;

    .line 89
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const/16 v13, 0xe

    const-string v15, "ALIAS_IN_USE"

    const/16 v14, 0x3f5

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->ALIAS_IN_USE:Lcom/squareup/protos/multipass/service/Error;

    .line 94
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v14, "ALIAS_INVALID"

    const/16 v15, 0xf

    const/16 v13, 0x3f6

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->ALIAS_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 99
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "ACCOUNT_ALREADY_CLAIMED"

    const/16 v14, 0x10

    const/16 v15, 0x3f7

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->ACCOUNT_ALREADY_CLAIMED:Lcom/squareup/protos/multipass/service/Error;

    .line 104
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "PASSWORD_INVALID"

    const/16 v14, 0x11

    const/16 v15, 0x7d0

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->PASSWORD_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 109
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "VERIFICATION_CODE_INVALID"

    const/16 v14, 0x12

    const/16 v15, 0x7d1

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->VERIFICATION_CODE_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 114
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "PASSWORD_REUSE"

    const/16 v14, 0x13

    const/16 v15, 0x7d2

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->PASSWORD_REUSE:Lcom/squareup/protos/multipass/service/Error;

    .line 119
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "PASSWORD_COMMON"

    const/16 v14, 0x14

    const/16 v15, 0x7d3

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->PASSWORD_COMMON:Lcom/squareup/protos/multipass/service/Error;

    .line 124
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "PASSWORD_COMPROMISED"

    const/16 v14, 0x15

    const/16 v15, 0x7d4

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->PASSWORD_COMPROMISED:Lcom/squareup/protos/multipass/service/Error;

    .line 129
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "PASSWORD_FAILS_REQUIREMENTS"

    const/16 v14, 0x16

    const/16 v15, 0x7d5

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->PASSWORD_FAILS_REQUIREMENTS:Lcom/squareup/protos/multipass/service/Error;

    .line 134
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "SESSION_ID_INVALID"

    const/16 v14, 0x17

    const/16 v15, 0xbb8

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 139
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "SESSION_ID_EXPIRED"

    const/16 v14, 0x18

    const/16 v15, 0xbb9

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_EXPIRED:Lcom/squareup/protos/multipass/service/Error;

    .line 144
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "SESSION_ID_TERMINATED"

    const/16 v14, 0x19

    const/16 v15, 0xbba

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_TERMINATED:Lcom/squareup/protos/multipass/service/Error;

    .line 149
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "SESSION_ID_IDLE"

    const/16 v14, 0x1a

    const/16 v15, 0xbbb

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_IDLE:Lcom/squareup/protos/multipass/service/Error;

    .line 154
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "SESSION_ID_INVALID_SELECTION"

    const/16 v14, 0x1b

    const/16 v15, 0xbbc

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_INVALID_SELECTION:Lcom/squareup/protos/multipass/service/Error;

    .line 159
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "SESSION_ID_ASSUMED_INVALID"

    const/16 v14, 0x1c

    const/16 v15, 0xbbd

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_ASSUMED_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 164
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "SESSION_SECONDARY_TOKEN_INVALID"

    const/16 v14, 0x1d

    const/16 v15, 0xbbe

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->SESSION_SECONDARY_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 169
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "SESSION_COOKIE_INVALID"

    const/16 v14, 0x1e

    const/16 v15, 0xbbf

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->SESSION_COOKIE_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 174
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "ACCESS_TOKEN_INVALID"

    const/16 v14, 0x1f

    const/16 v15, 0xbc0

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->ACCESS_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 179
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "OAUTH_SESSION_DISABLED"

    const/16 v14, 0x20

    const/16 v15, 0xbc1

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->OAUTH_SESSION_DISABLED:Lcom/squareup/protos/multipass/service/Error;

    .line 184
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "TOKEN_INVALID"

    const/16 v14, 0x21

    const/16 v15, 0xbc2

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 189
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "IP_ADDRESS_INVALID"

    const/16 v14, 0x22

    const/16 v15, 0xbc3

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->IP_ADDRESS_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 194
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "ACCESS_TOKEN_EXPIRED"

    const/16 v14, 0x23

    const/16 v15, 0xbc4

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->ACCESS_TOKEN_EXPIRED:Lcom/squareup/protos/multipass/service/Error;

    .line 199
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "ACCESS_TOKEN_REVOKED"

    const/16 v14, 0x24

    const/16 v15, 0xbc5

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->ACCESS_TOKEN_REVOKED:Lcom/squareup/protos/multipass/service/Error;

    .line 204
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "DEVICE_DETAILS_INVALID"

    const/16 v14, 0x25

    const/16 v15, 0xfa0

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->DEVICE_DETAILS_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 209
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "DEVICE_CREDENTIAL_TOKEN_INVALID"

    const/16 v14, 0x26

    const/16 v15, 0xfa1

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 214
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "DEVICE_CREDENTIAL_LIMIT_EXCEEDED"

    const/16 v14, 0x27

    const/16 v15, 0xfa2

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_LIMIT_EXCEEDED:Lcom/squareup/protos/multipass/service/Error;

    .line 220
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "DEVICE_CREDENTIAL_EXPECTED_REGISTER_PRODUCT"

    const/16 v14, 0x28

    const/16 v15, 0xfa3

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_EXPECTED_REGISTER_PRODUCT:Lcom/squareup/protos/multipass/service/Error;

    .line 226
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "DEVICE_CREDENTIAL_EXPECTED_RETAIL_PRODUCT"

    const/16 v14, 0x29

    const/16 v15, 0xfa4

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_EXPECTED_RETAIL_PRODUCT:Lcom/squareup/protos/multipass/service/Error;

    .line 232
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "DEVICE_CREDENTIAL_EXPECTED_RESTAURANT_PRODUCT"

    const/16 v14, 0x2a

    const/16 v15, 0xfa5

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_EXPECTED_RESTAURANT_PRODUCT:Lcom/squareup/protos/multipass/service/Error;

    .line 237
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "DEVICE_CREDENTIAL_ALREADY_PAIRED"

    const/16 v14, 0x2b

    const/16 v15, 0xfa6

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_ALREADY_PAIRED:Lcom/squareup/protos/multipass/service/Error;

    .line 242
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "OTK_INVALID"

    const/16 v14, 0x2c

    const/16 v15, 0x1388

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->OTK_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 247
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "REPLICATION_LAG"

    const/16 v14, 0x2d

    const/16 v15, 0x1770

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->REPLICATION_LAG:Lcom/squareup/protos/multipass/service/Error;

    .line 252
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "MERCHANT_INVALID"

    const/16 v14, 0x2e

    const/16 v15, 0x1b58

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->MERCHANT_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 257
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "UNIT_INVALID"

    const/16 v14, 0x2f

    const/16 v15, 0x1b59

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->UNIT_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 262
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "APPLICATION_NOT_ALLOWED"

    const/16 v14, 0x30

    const/16 v15, 0x1b5a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->APPLICATION_NOT_ALLOWED:Lcom/squareup/protos/multipass/service/Error;

    .line 267
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "REDIRECT_URL_INVALID"

    const/16 v14, 0x31

    const/16 v15, 0x1b5b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->REDIRECT_URL_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 272
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "CREDENTIAL_TOKEN_INVALID"

    const/16 v14, 0x32

    const/16 v15, 0x1f40

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->CREDENTIAL_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    .line 277
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "PERSON_TOKEN_INVALID_CASH_USER"

    const/16 v14, 0x33

    const/16 v15, 0x1f41

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->PERSON_TOKEN_INVALID_CASH_USER:Lcom/squareup/protos/multipass/service/Error;

    .line 282
    new-instance v0, Lcom/squareup/protos/multipass/service/Error;

    const-string v13, "NOT_FOUND"

    const/16 v14, 0x34

    const/16 v15, 0x2328

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/multipass/service/Error;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->NOT_FOUND:Lcom/squareup/protos/multipass/service/Error;

    const/16 v0, 0x35

    new-array v0, v0, [Lcom/squareup/protos/multipass/service/Error;

    .line 15
    sget-object v13, Lcom/squareup/protos/multipass/service/Error;->NO_ERROR:Lcom/squareup/protos/multipass/service/Error;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->TEMPORARY_LOCKOUT:Lcom/squareup/protos/multipass/service/Error;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->EMAIL_INVALID:Lcom/squareup/protos/multipass/service/Error;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->USER_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->NEW_EMAIL_INVALID:Lcom/squareup/protos/multipass/service/Error;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->NEW_EMAIL_TAKEN:Lcom/squareup/protos/multipass/service/Error;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->PHONE_INVALID:Lcom/squareup/protos/multipass/service/Error;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->CAPTCHA_REQUIRED:Lcom/squareup/protos/multipass/service/Error;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->CAPTCHA_RESPONSE_INVALID:Lcom/squareup/protos/multipass/service/Error;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->FORWARD_TO_MASTER:Lcom/squareup/protos/multipass/service/Error;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->VERIFICATION_REQUIRED:Lcom/squareup/protos/multipass/service/Error;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->VERIFICATION_METHOD_INVALID:Lcom/squareup/protos/multipass/service/Error;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->VERIFICATION_CODE_NOT_SENT:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->ACCOUNT_LOCKED:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->ALIAS_IN_USE:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->ALIAS_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->ACCOUNT_ALREADY_CLAIMED:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->PASSWORD_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->VERIFICATION_CODE_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->PASSWORD_REUSE:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->PASSWORD_COMMON:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->PASSWORD_COMPROMISED:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->PASSWORD_FAILS_REQUIREMENTS:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_EXPIRED:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_TERMINATED:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_IDLE:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_INVALID_SELECTION:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_ASSUMED_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->SESSION_SECONDARY_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->SESSION_COOKIE_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->ACCESS_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->OAUTH_SESSION_DISABLED:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->IP_ADDRESS_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->ACCESS_TOKEN_EXPIRED:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->ACCESS_TOKEN_REVOKED:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->DEVICE_DETAILS_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_LIMIT_EXCEEDED:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_EXPECTED_REGISTER_PRODUCT:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_EXPECTED_RETAIL_PRODUCT:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_EXPECTED_RESTAURANT_PRODUCT:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_ALREADY_PAIRED:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->OTK_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->REPLICATION_LAG:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->MERCHANT_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->UNIT_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->APPLICATION_NOT_ALLOWED:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->REDIRECT_URL_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->CREDENTIAL_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->PERSON_TOKEN_INVALID_CASH_USER:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/service/Error;->NOT_FOUND:Lcom/squareup/protos/multipass/service/Error;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->$VALUES:[Lcom/squareup/protos/multipass/service/Error;

    .line 284
    new-instance v0, Lcom/squareup/protos/multipass/service/Error$ProtoAdapter_Error;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/Error$ProtoAdapter_Error;-><init>()V

    sput-object v0, Lcom/squareup/protos/multipass/service/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 288
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 289
    iput p3, p0, Lcom/squareup/protos/multipass/service/Error;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/multipass/service/Error;
    .locals 1

    if-eqz p0, :cond_5

    const/16 v0, 0x1388

    if-eq p0, v0, :cond_4

    const/16 v0, 0x1770

    if-eq p0, v0, :cond_3

    const/16 v0, 0x2328

    if-eq p0, v0, :cond_2

    const/16 v0, 0x1f40

    if-eq p0, v0, :cond_1

    const/16 v0, 0x1f41

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_0

    packed-switch p0, :pswitch_data_1

    packed-switch p0, :pswitch_data_2

    packed-switch p0, :pswitch_data_3

    packed-switch p0, :pswitch_data_4

    const/4 p0, 0x0

    return-object p0

    .line 346
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->REDIRECT_URL_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 345
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->APPLICATION_NOT_ALLOWED:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 344
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->UNIT_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 343
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->MERCHANT_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 340
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_ALREADY_PAIRED:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 339
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_EXPECTED_RESTAURANT_PRODUCT:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 338
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_EXPECTED_RETAIL_PRODUCT:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 337
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_EXPECTED_REGISTER_PRODUCT:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 336
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_LIMIT_EXCEEDED:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 335
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->DEVICE_CREDENTIAL_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 334
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->DEVICE_DETAILS_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 333
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->ACCESS_TOKEN_REVOKED:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 332
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->ACCESS_TOKEN_EXPIRED:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 331
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->IP_ADDRESS_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 330
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 329
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->OAUTH_SESSION_DISABLED:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 328
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->ACCESS_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 327
    :pswitch_11
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->SESSION_COOKIE_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 326
    :pswitch_12
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->SESSION_SECONDARY_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 325
    :pswitch_13
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_ASSUMED_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 324
    :pswitch_14
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_INVALID_SELECTION:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 323
    :pswitch_15
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_IDLE:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 322
    :pswitch_16
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_TERMINATED:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 321
    :pswitch_17
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_EXPIRED:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 320
    :pswitch_18
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->SESSION_ID_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 319
    :pswitch_19
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->PASSWORD_FAILS_REQUIREMENTS:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 318
    :pswitch_1a
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->PASSWORD_COMPROMISED:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 317
    :pswitch_1b
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->PASSWORD_COMMON:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 316
    :pswitch_1c
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->PASSWORD_REUSE:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 315
    :pswitch_1d
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->VERIFICATION_CODE_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 314
    :pswitch_1e
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->PASSWORD_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 313
    :pswitch_1f
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->ACCOUNT_ALREADY_CLAIMED:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 312
    :pswitch_20
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->ALIAS_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 311
    :pswitch_21
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->ALIAS_IN_USE:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 310
    :pswitch_22
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->ACCOUNT_LOCKED:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 309
    :pswitch_23
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->VERIFICATION_CODE_NOT_SENT:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 308
    :pswitch_24
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->VERIFICATION_METHOD_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 307
    :pswitch_25
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->VERIFICATION_REQUIRED:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 306
    :pswitch_26
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->FORWARD_TO_MASTER:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 305
    :pswitch_27
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->CAPTCHA_RESPONSE_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 304
    :pswitch_28
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->CAPTCHA_REQUIRED:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 303
    :pswitch_29
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->PHONE_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 302
    :pswitch_2a
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->NEW_EMAIL_TAKEN:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 301
    :pswitch_2b
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->NEW_EMAIL_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 300
    :pswitch_2c
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->USER_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 299
    :pswitch_2d
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->EMAIL_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 298
    :pswitch_2e
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->TEMPORARY_LOCKOUT:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 348
    :cond_0
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->PERSON_TOKEN_INVALID_CASH_USER:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 347
    :cond_1
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->CREDENTIAL_TOKEN_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 349
    :cond_2
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->NOT_FOUND:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 342
    :cond_3
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->REPLICATION_LAG:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 341
    :cond_4
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->OTK_INVALID:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    .line 297
    :cond_5
    sget-object p0, Lcom/squareup/protos/multipass/service/Error;->NO_ERROR:Lcom/squareup/protos/multipass/service/Error;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7d0
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0xbb8
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0xfa0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1b58
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/multipass/service/Error;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/protos/multipass/service/Error;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/multipass/service/Error;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/multipass/service/Error;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/protos/multipass/service/Error;->$VALUES:[Lcom/squareup/protos/multipass/service/Error;

    invoke-virtual {v0}, [Lcom/squareup/protos/multipass/service/Error;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/multipass/service/Error;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 356
    iget v0, p0, Lcom/squareup/protos/multipass/service/Error;->value:I

    return v0
.end method
