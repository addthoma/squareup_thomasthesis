.class public final Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TwoFactorDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/common/TwoFactorDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public created_at:Ljava/lang/Long;

.field public description:Ljava/lang/String;

.field public googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

.field public set_trusted_device:Ljava/lang/Boolean;

.field public sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

.field public webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 168
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/multipass/common/TwoFactorDetails;
    .locals 9

    .line 218
    new-instance v8, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->description:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->set_trusted_device:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->created_at:Ljava/lang/Long;

    iget-object v4, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    iget-object v5, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    iget-object v6, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/multipass/common/TwoFactorDetails;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/squareup/protos/multipass/common/SmsTwoFactor;Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 155
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->build()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Ljava/lang/Long;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->created_at:Ljava/lang/Long;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;
    .locals 0

    .line 175
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public googleauth(Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    const/4 p1, 0x0

    .line 204
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    return-object p0
.end method

.method public set_trusted_device(Ljava/lang/Boolean;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->set_trusted_device:Ljava/lang/Boolean;

    return-object p0
.end method

.method public sms(Lcom/squareup/protos/multipass/common/SmsTwoFactor;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    const/4 p1, 0x0

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    return-object p0
.end method

.method public webauthn(Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    const/4 p1, 0x0

    .line 211
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    .line 212
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    return-object p0
.end method
