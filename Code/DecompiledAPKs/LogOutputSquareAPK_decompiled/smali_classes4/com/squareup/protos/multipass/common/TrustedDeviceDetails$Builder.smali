.class public final Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TrustedDeviceDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
        "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public expiry:Ljava/lang/Long;

.field public person_token:Ljava/lang/String;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 127
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;
    .locals 5

    .line 158
    new-instance v0, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;->expiry:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;->person_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 120
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;->build()Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    move-result-object v0

    return-object v0
.end method

.method public expiry(Ljava/lang/Long;)Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;->expiry:Ljava/lang/Long;

    return-object p0
.end method

.method public person_token(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;->person_token:Ljava/lang/String;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
