.class public final Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;
.super Lcom/squareup/wire/Message;
.source "WebauthnTwoFactor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$ProtoAdapter_WebauthnTwoFactor;,
        Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;",
        "Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CHALLENGE:Lokio/ByteString;

.field public static final DEFAULT_CREDENTIAL_ID:Lokio/ByteString;

.field public static final DEFAULT_ENCRYPTED_DATA:Lokio/ByteString;

.field public static final DEFAULT_PERSON_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_PUBLIC_KEY_CREDENTIAL:Ljava/lang/String; = ""

.field public static final DEFAULT_SIGNATURE:Lokio/ByteString;

.field public static final DEFAULT_SIGNATURE_COUNT:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final challenge:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x6
    .end annotation
.end field

.field public final credential_id:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final encrypted_data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final person_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final public_key_credential:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x5
    .end annotation
.end field

.field public final signature:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final signature_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        redacted = true
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 21
    new-instance v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$ProtoAdapter_WebauthnTwoFactor;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$ProtoAdapter_WebauthnTwoFactor;-><init>()V

    sput-object v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 25
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->DEFAULT_CREDENTIAL_ID:Lokio/ByteString;

    const-wide/16 v0, 0x0

    .line 27
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->DEFAULT_SIGNATURE_COUNT:Ljava/lang/Long;

    .line 29
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->DEFAULT_SIGNATURE:Lokio/ByteString;

    .line 31
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->DEFAULT_ENCRYPTED_DATA:Lokio/ByteString;

    .line 35
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->DEFAULT_CHALLENGE:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Ljava/lang/Long;Lokio/ByteString;Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;Ljava/lang/String;)V
    .locals 9

    .line 113
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;-><init>(Lokio/ByteString;Ljava/lang/Long;Lokio/ByteString;Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Ljava/lang/Long;Lokio/ByteString;Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 119
    sget-object v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->credential_id:Lokio/ByteString;

    .line 121
    iput-object p2, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->signature_count:Ljava/lang/Long;

    .line 122
    iput-object p3, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->signature:Lokio/ByteString;

    .line 123
    iput-object p4, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->encrypted_data:Lokio/ByteString;

    .line 124
    iput-object p5, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->public_key_credential:Ljava/lang/String;

    .line 125
    iput-object p6, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->challenge:Lokio/ByteString;

    .line 126
    iput-object p7, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->person_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 146
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 147
    :cond_1
    check-cast p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    .line 148
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->credential_id:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->credential_id:Lokio/ByteString;

    .line 149
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->signature_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->signature_count:Ljava/lang/Long;

    .line 150
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->signature:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->signature:Lokio/ByteString;

    .line 151
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->encrypted_data:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->encrypted_data:Lokio/ByteString;

    .line 152
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->public_key_credential:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->public_key_credential:Ljava/lang/String;

    .line 153
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->challenge:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->challenge:Lokio/ByteString;

    .line 154
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->person_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->person_token:Ljava/lang/String;

    .line 155
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 160
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 162
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->credential_id:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->signature_count:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 165
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->signature:Lokio/ByteString;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 166
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->encrypted_data:Lokio/ByteString;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 167
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->public_key_credential:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->challenge:Lokio/ByteString;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 169
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->person_token:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 170
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;
    .locals 2

    .line 131
    new-instance v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;-><init>()V

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->credential_id:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->credential_id:Lokio/ByteString;

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->signature_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->signature_count:Ljava/lang/Long;

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->signature:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->signature:Lokio/ByteString;

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->encrypted_data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->encrypted_data:Lokio/ByteString;

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->public_key_credential:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->public_key_credential:Ljava/lang/String;

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->challenge:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->challenge:Lokio/ByteString;

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->person_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->person_token:Ljava/lang/String;

    .line 139
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->newBuilder()Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->credential_id:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", credential_id=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->signature_count:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", signature_count=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->signature:Lokio/ByteString;

    if-eqz v1, :cond_2

    const-string v1, ", signature=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->encrypted_data:Lokio/ByteString;

    if-eqz v1, :cond_3

    const-string v1, ", encrypted_data=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->public_key_credential:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", public_key_credential=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->challenge:Lokio/ByteString;

    if-eqz v1, :cond_5

    const-string v1, ", challenge=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->person_token:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", person_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->person_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "WebauthnTwoFactor{"

    .line 185
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
