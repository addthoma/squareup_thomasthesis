.class public final Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EnrollTwoFactorResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;",
        "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public complete:Ljava/lang/Boolean;

.field public error_message:Ljava/lang/String;

.field public error_title:Ljava/lang/String;

.field public session_token:Ljava/lang/String;

.field public trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

.field public two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 178
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;
    .locals 9

    .line 233
    new-instance v8, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;

    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->complete:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->session_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    iget-object v4, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    iget-object v5, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->error_title:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->error_message:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 165
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->build()Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;

    move-result-object v0

    return-object v0
.end method

.method public complete(Ljava/lang/Boolean;)Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->complete:Ljava/lang/Boolean;

    return-object p0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public error_title(Ljava/lang/String;)Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->error_title:Ljava/lang/String;

    return-object p0
.end method

.method public session_token(Ljava/lang/String;)Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->session_token:Ljava/lang/String;

    return-object p0
.end method

.method public trusted_device_details(Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;)Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    return-object p0
.end method

.method public two_factor_details(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    return-object p0
.end method
