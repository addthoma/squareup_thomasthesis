.class public final Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpgradeSessionTwoFactorRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;",
        "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 82
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;
    .locals 3

    .line 95
    new-instance v0, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;

    iget-object v1, p0, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest$Builder;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;-><init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest$Builder;->build()Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;

    move-result-object v0

    return-object v0
.end method

.method public two_factor_details(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest$Builder;
    .locals 0

    .line 89
    iput-object p1, p0, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest$Builder;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    return-object p0
.end method
