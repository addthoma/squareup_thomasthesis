.class public final Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SendInstallmentsSMSRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest;",
        "Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cart:Lcom/squareup/protos/client/bills/Cart;

.field public idempotence_token:Ljava/lang/String;

.field public phone_number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 113
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest;
    .locals 5

    .line 136
    new-instance v0, Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest;

    iget-object v1, p0, Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;->idempotence_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v3, p0, Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;->phone_number:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;->build()Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest;

    move-result-object v0

    return-object v0
.end method

.method public cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object p0
.end method

.method public idempotence_token(Ljava/lang/String;)Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;->idempotence_token:Ljava/lang/String;

    return-object p0
.end method

.method public phone_number(Ljava/lang/String;)Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;->phone_number:Ljava/lang/String;

    return-object p0
.end method
