.class final Lcom/squareup/protos/capital/external/business/models/Customer$ProtoAdapter_Customer;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Customer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/external/business/models/Customer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Customer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/capital/external/business/models/Customer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 372
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/capital/external/business/models/Customer;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/external/business/models/Customer;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 409
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;-><init>()V

    .line 410
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 411
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 425
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 423
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_latest_closed_plan(Lcom/squareup/protos/capital/external/business/models/PreviewPlan;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;

    goto :goto_0

    .line 422
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_financing_request(Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;

    goto :goto_0

    .line 421
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_offer_flow(Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;

    goto :goto_0

    .line 420
    :pswitch_3
    iget-object v3, v0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->eligibilities:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/capital/external/business/models/Eligibility;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 419
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_latest_completed_plan(Lcom/squareup/protos/capital/external/business/models/PreviewPlan;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;

    goto :goto_0

    .line 418
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_active_plan(Lcom/squareup/protos/capital/external/business/models/PreviewPlan;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;

    goto :goto_0

    .line 417
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_financing_request_in_review(Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;

    goto :goto_0

    .line 416
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_main_offer(Lcom/squareup/protos/capital/external/business/models/PreviewOffer;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;

    goto :goto_0

    .line 415
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;

    goto :goto_0

    .line 414
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->partner_id(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;

    goto/16 :goto_0

    .line 413
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->id(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;

    goto/16 :goto_0

    .line 429
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 430
    invoke-virtual {v0}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->build()Lcom/squareup/protos/capital/external/business/models/Customer;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 370
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/external/business/models/Customer$ProtoAdapter_Customer;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/external/business/models/Customer;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/external/business/models/Customer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 393
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/Customer;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 394
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/Customer;->partner_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 395
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/Customer;->created_at:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 396
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 397
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 398
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 399
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 400
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 401
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/Eligibility;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/Customer;->eligibilities:Ljava/util/List;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 402
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 403
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 404
    invoke-virtual {p2}, Lcom/squareup/protos/capital/external/business/models/Customer;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 370
    check-cast p2, Lcom/squareup/protos/capital/external/business/models/Customer;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/capital/external/business/models/Customer$ProtoAdapter_Customer;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/external/business/models/Customer;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/capital/external/business/models/Customer;)I
    .locals 4

    .line 377
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->partner_id:Ljava/lang/String;

    const/4 v3, 0x2

    .line 378
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->created_at:Lcom/squareup/protos/common/time/DateTime;

    const/4 v3, 0x3

    .line 379
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    const/4 v3, 0x4

    .line 380
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    const/4 v3, 0x5

    .line 381
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    const/4 v3, 0x6

    .line 382
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    const/4 v3, 0x7

    .line 383
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    const/16 v3, 0xb

    .line 384
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/external/business/models/Eligibility;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 385
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->eligibilities:Ljava/util/List;

    const/16 v3, 0x8

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    const/16 v3, 0x9

    .line 386
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    const/16 v3, 0xa

    .line 387
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 388
    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/models/Customer;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 370
    check-cast p1, Lcom/squareup/protos/capital/external/business/models/Customer;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/external/business/models/Customer$ProtoAdapter_Customer;->encodedSize(Lcom/squareup/protos/capital/external/business/models/Customer;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/capital/external/business/models/Customer;)Lcom/squareup/protos/capital/external/business/models/Customer;
    .locals 2

    .line 435
    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/models/Customer;->newBuilder()Lcom/squareup/protos/capital/external/business/models/Customer$Builder;

    move-result-object p1

    .line 436
    iget-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 437
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    iput-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    .line 438
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    iput-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    .line 439
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iput-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    .line 440
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iput-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    .line 441
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iput-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    .line 442
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->eligibilities:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/capital/external/business/models/Eligibility;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 443
    iget-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    iput-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    .line 444
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    iput-object v0, p1, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    .line 445
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 446
    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->build()Lcom/squareup/protos/capital/external/business/models/Customer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 370
    check-cast p1, Lcom/squareup/protos/capital/external/business/models/Customer;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/external/business/models/Customer$ProtoAdapter_Customer;->redact(Lcom/squareup/protos/capital/external/business/models/Customer;)Lcom/squareup/protos/capital/external/business/models/Customer;

    move-result-object p1

    return-object p1
.end method
