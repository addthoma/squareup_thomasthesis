.class public final Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Amounts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Amounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Amounts;",
        "Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

.field public detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

.field public detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

.field public pending_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 130
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public base_amounts(Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/capital/servicing/plan/models/Amounts;
    .locals 7

    .line 163
    new-instance v6, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    iget-object v3, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    iget-object v4, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    move-result-object v0

    return-object v0
.end method

.method public detail_amounts(Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    return-object p0
.end method

.method public detail_amounts_pending_applied(Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;
    .locals 0

    .line 157
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    return-object p0
.end method

.method public pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
