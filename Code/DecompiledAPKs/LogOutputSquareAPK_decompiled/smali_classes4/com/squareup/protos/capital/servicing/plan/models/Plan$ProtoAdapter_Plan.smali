.class final Lcom/squareup/protos/capital/servicing/plan/models/Plan$ProtoAdapter_Plan;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Plan.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Plan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Plan"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Plan;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1100
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/Plan;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1171
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;-><init>()V

    .line 1172
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1173
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    packed-switch v3, :pswitch_data_1

    .line 1211
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1209
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->retail_installments(Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto :goto_0

    .line 1208
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->mca(Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto :goto_0

    .line 1207
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_portion_of_sales(Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto :goto_0

    .line 1206
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_fixed_payments(Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto :goto_0

    .line 1205
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->percentage_complete(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto :goto_0

    .line 1204
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->amounts(Lcom/squareup/protos/capital/servicing/plan/models/Amounts;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto :goto_0

    .line 1203
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->product_name(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto :goto_0

    .line 1202
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->allocated_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto :goto_0

    .line 1201
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->is_originated(Ljava/lang/Boolean;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto :goto_0

    .line 1200
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->is_prepayable(Ljava/lang/Boolean;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto/16 :goto_0

    .line 1199
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->legacy_id(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto/16 :goto_0

    .line 1198
    :pswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->activated_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto/16 :goto_0

    .line 1197
    :pswitch_c
    iget-object v3, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->support_resources:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1196
    :pswitch_d
    iget-object v3, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->documents:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/capital/servicing/plan/models/Document;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1195
    :pswitch_e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->is_cancelable(Ljava/lang/Boolean;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto/16 :goto_0

    .line 1194
    :pswitch_f
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->default_status(Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto/16 :goto_0

    .line 1193
    :pswitch_10
    iget-object v3, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->tags:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/capital/servicing/plan/models/Tag;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1192
    :pswitch_11
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/Fund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->fund(Lcom/squareup/protos/capital/servicing/plan/models/Fund;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto/16 :goto_0

    .line 1191
    :pswitch_12
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/Investor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->investor(Lcom/squareup/protos/capital/servicing/plan/models/Investor;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto/16 :goto_0

    .line 1190
    :pswitch_13
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->contract_id(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto/16 :goto_0

    .line 1184
    :pswitch_14
    :try_start_0
    sget-object v4, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->status(Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v4

    .line 1186
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 1181
    :pswitch_15
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->closed_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto/16 :goto_0

    .line 1180
    :pswitch_16
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->cancelable_expired_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto/16 :goto_0

    .line 1179
    :pswitch_17
    iget-object v3, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->payers:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1178
    :pswitch_18
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->updated_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto/16 :goto_0

    .line 1177
    :pswitch_19
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->created_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto/16 :goto_0

    .line 1176
    :pswitch_1a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->version(Ljava/lang/Integer;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto/16 :goto_0

    .line 1175
    :pswitch_1b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->id(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    goto/16 :goto_0

    .line 1215
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1216
    invoke-virtual {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/Plan;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x96
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1098
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$ProtoAdapter_Plan;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/Plan;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/Plan;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1138
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1139
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->version:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1140
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->created_at:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1141
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->updated_at:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1142
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->payers:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1143
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->cancelable_expired_at:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1144
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->closed_at:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1145
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1146
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->contract_id:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1147
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Investor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1148
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Fund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1149
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Tag;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->tags:Ljava/util/List;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1150
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1151
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_cancelable:Ljava/lang/Boolean;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1152
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->documents:Ljava/util/List;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1153
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->support_resources:Ljava/util/List;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1154
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->activated_at:Ljava/lang/String;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1155
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->legacy_id:Ljava/lang/String;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1156
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_prepayable:Ljava/lang/Boolean;

    const/16 v2, 0x13

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1157
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_originated:Ljava/lang/Boolean;

    const/16 v2, 0x14

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1158
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->allocated_at:Ljava/lang/String;

    const/16 v2, 0x15

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1159
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->product_name:Ljava/lang/String;

    const/16 v2, 0x16

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1160
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    const/16 v2, 0x17

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1161
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->percentage_complete:Ljava/lang/String;

    const/16 v2, 0x18

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1162
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    const/16 v2, 0x96

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1163
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    const/16 v2, 0x97

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1164
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    const/16 v2, 0x98

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1165
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    const/16 v2, 0x99

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1166
    invoke-virtual {p2}, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1098
    check-cast p2, Lcom/squareup/protos/capital/servicing/plan/models/Plan;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$ProtoAdapter_Plan;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/Plan;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)I
    .locals 4

    .line 1105
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->version:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 1106
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->created_at:Ljava/lang/String;

    const/4 v3, 0x3

    .line 1107
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->updated_at:Ljava/lang/String;

    const/4 v3, 0x4

    .line 1108
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1109
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->payers:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->cancelable_expired_at:Ljava/lang/String;

    const/4 v3, 0x6

    .line 1110
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->closed_at:Ljava/lang/String;

    const/4 v3, 0x7

    .line 1111
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    const/16 v3, 0x8

    .line 1112
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->contract_id:Ljava/lang/String;

    const/16 v3, 0x9

    .line 1113
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Investor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    const/16 v3, 0xa

    .line 1114
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Fund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    const/16 v3, 0xb

    .line 1115
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Tag;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1116
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->tags:Ljava/util/List;

    const/16 v3, 0xc

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    const/16 v3, 0xd

    .line 1117
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_cancelable:Ljava/lang/Boolean;

    const/16 v3, 0xe

    .line 1118
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Document;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1119
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->documents:Ljava/util/List;

    const/16 v3, 0xf

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1120
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->support_resources:Ljava/util/List;

    const/16 v3, 0x10

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->activated_at:Ljava/lang/String;

    const/16 v3, 0x11

    .line 1121
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->legacy_id:Ljava/lang/String;

    const/16 v3, 0x12

    .line 1122
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_prepayable:Ljava/lang/Boolean;

    const/16 v3, 0x13

    .line 1123
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_originated:Ljava/lang/Boolean;

    const/16 v3, 0x14

    .line 1124
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->allocated_at:Ljava/lang/String;

    const/16 v3, 0x15

    .line 1125
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->product_name:Ljava/lang/String;

    const/16 v3, 0x16

    .line 1126
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    const/16 v3, 0x17

    .line 1127
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->percentage_complete:Ljava/lang/String;

    const/16 v3, 0x18

    .line 1128
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    const/16 v3, 0x96

    .line 1129
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    const/16 v3, 0x97

    .line 1130
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    const/16 v3, 0x98

    .line 1131
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    const/16 v3, 0x99

    .line 1132
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1133
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1098
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$ProtoAdapter_Plan;->encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Lcom/squareup/protos/capital/servicing/plan/models/Plan;
    .locals 2

    .line 1221
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    move-result-object p1

    .line 1222
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->payers:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1223
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Investor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    .line 1224
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Fund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    .line 1225
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->tags:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Tag;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1226
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    .line 1227
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->documents:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Document;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1228
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->support_resources:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1229
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    .line 1230
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    .line 1231
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    .line 1232
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    .line 1233
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    .line 1234
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1235
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/Plan;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1098
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$ProtoAdapter_Plan;->redact(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Lcom/squareup/protos/capital/servicing/plan/models/Plan;

    move-result-object p1

    return-object p1
.end method
