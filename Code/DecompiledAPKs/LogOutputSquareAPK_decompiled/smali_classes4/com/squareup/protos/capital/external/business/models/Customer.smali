.class public final Lcom/squareup/protos/capital/external/business/models/Customer;
.super Lcom/squareup/wire/Message;
.source "Customer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/external/business/models/Customer$ProtoAdapter_Customer;,
        Lcom/squareup/protos/capital/external/business/models/Customer$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/external/business/models/Customer;",
        "Lcom/squareup/protos/capital/external/business/models/Customer$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/external/business/models/Customer;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_PARTNER_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final created_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final eligibilities:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.external.business.models.Eligibility#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/external/business/models/Eligibility;",
            ">;"
        }
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final partner_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.external.business.models.PreviewPlan#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.external.business.models.PreviewFinancingRequest#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.external.business.models.PreviewFinancingRequest#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.external.business.models.PreviewPlan#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.external.business.models.PreviewPlan#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.external.business.models.PreviewOffer#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.external.business.models.PreviewOfferFlow#ADAPTER"
        tag = 0x9
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/Customer$ProtoAdapter_Customer;

    invoke-direct {v0}, Lcom/squareup/protos/capital/external/business/models/Customer$ProtoAdapter_Customer;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/external/business/models/Customer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/capital/external/business/models/PreviewOffer;Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;Lcom/squareup/protos/capital/external/business/models/PreviewPlan;Lcom/squareup/protos/capital/external/business/models/PreviewPlan;Lcom/squareup/protos/capital/external/business/models/PreviewPlan;Ljava/util/List;Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Lcom/squareup/protos/capital/external/business/models/PreviewOffer;",
            "Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;",
            "Lcom/squareup/protos/capital/external/business/models/PreviewPlan;",
            "Lcom/squareup/protos/capital/external/business/models/PreviewPlan;",
            "Lcom/squareup/protos/capital/external/business/models/PreviewPlan;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/external/business/models/Eligibility;",
            ">;",
            "Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;",
            "Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;",
            ")V"
        }
    .end annotation

    .line 142
    sget-object v12, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/capital/external/business/models/Customer;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/capital/external/business/models/PreviewOffer;Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;Lcom/squareup/protos/capital/external/business/models/PreviewPlan;Lcom/squareup/protos/capital/external/business/models/PreviewPlan;Lcom/squareup/protos/capital/external/business/models/PreviewPlan;Ljava/util/List;Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/capital/external/business/models/PreviewOffer;Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;Lcom/squareup/protos/capital/external/business/models/PreviewPlan;Lcom/squareup/protos/capital/external/business/models/PreviewPlan;Lcom/squareup/protos/capital/external/business/models/PreviewPlan;Ljava/util/List;Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Lcom/squareup/protos/capital/external/business/models/PreviewOffer;",
            "Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;",
            "Lcom/squareup/protos/capital/external/business/models/PreviewPlan;",
            "Lcom/squareup/protos/capital/external/business/models/PreviewPlan;",
            "Lcom/squareup/protos/capital/external/business/models/PreviewPlan;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/external/business/models/Eligibility;",
            ">;",
            "Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;",
            "Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 151
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/Customer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p12}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->id:Ljava/lang/String;

    .line 153
    iput-object p2, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->partner_id:Ljava/lang/String;

    .line 154
    iput-object p3, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 155
    iput-object p4, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    .line 156
    iput-object p5, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    .line 157
    iput-object p6, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    .line 158
    iput-object p7, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    .line 159
    iput-object p8, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    const-string p1, "eligibilities"

    .line 160
    invoke-static {p1, p9}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->eligibilities:Ljava/util/List;

    .line 161
    iput-object p10, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    .line 162
    iput-object p11, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 186
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/external/business/models/Customer;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 187
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/external/business/models/Customer;

    .line 188
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/Customer;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/models/Customer;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->id:Ljava/lang/String;

    .line 189
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->partner_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->partner_id:Ljava/lang/String;

    .line 190
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 191
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    iget-object v3, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    .line 192
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    iget-object v3, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    .line 193
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iget-object v3, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    .line 194
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iget-object v3, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    .line 195
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iget-object v3, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    .line 196
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->eligibilities:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->eligibilities:Ljava/util/List;

    .line 197
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    iget-object v3, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    .line 198
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    .line 199
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 204
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 206
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/Customer;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->partner_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 214
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 215
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->eligibilities:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 218
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/external/business/models/Customer$Builder;
    .locals 2

    .line 167
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;-><init>()V

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->id:Ljava/lang/String;

    .line 169
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->partner_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->partner_id:Ljava/lang/String;

    .line 170
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->eligibilities:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->eligibilities:Ljava/util/List;

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    .line 179
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/Customer;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/Customer;->newBuilder()Lcom/squareup/protos/capital/external/business/models/Customer$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 226
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->partner_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", partner_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->partner_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_2

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 229
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    if-eqz v1, :cond_3

    const-string v1, ", preview_main_offer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 230
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    if-eqz v1, :cond_4

    const-string v1, ", preview_financing_request_in_review="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 231
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    if-eqz v1, :cond_5

    const-string v1, ", preview_active_plan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 232
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    if-eqz v1, :cond_6

    const-string v1, ", preview_latest_completed_plan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 233
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    if-eqz v1, :cond_7

    const-string v1, ", preview_latest_closed_plan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 234
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->eligibilities:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, ", eligibilities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->eligibilities:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 235
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    if-eqz v1, :cond_9

    const-string v1, ", preview_offer_flow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 236
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    if-eqz v1, :cond_a

    const-string v1, ", preview_financing_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Customer{"

    .line 237
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
