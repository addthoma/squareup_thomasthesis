.class public final Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;
.super Lcom/squareup/wire/Message;
.source "RepaymentTerms.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FixedPayments"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$ProtoAdapter_FixedPayments;,
        Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;,
        Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;",
        "Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PAYMENT_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_PAYMENT_FREQUENCY:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

.field private static final serialVersionUID:J


# instance fields
.field public final final_payment_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final payment_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3
    .end annotation
.end field

.field public final payment_frequency:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.RepaymentTerms$FixedPayments$PaymentFrequency#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final payment_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 184
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$ProtoAdapter_FixedPayments;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$ProtoAdapter_FixedPayments;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 188
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->DEFAULT_PAYMENT_COUNT:Ljava/lang/Long;

    .line 190
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;->PF_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->DEFAULT_PAYMENT_FREQUENCY:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/Long;Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;)V
    .locals 6

    .line 218
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/Long;Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/Long;Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;Lokio/ByteString;)V
    .locals 1

    .line 223
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 224
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_money:Lcom/squareup/protos/common/Money;

    .line 225
    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->final_payment_money:Lcom/squareup/protos/common/Money;

    .line 226
    iput-object p3, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_count:Ljava/lang/Long;

    .line 227
    iput-object p4, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_frequency:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 244
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 245
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    .line 246
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_money:Lcom/squareup/protos/common/Money;

    .line 247
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->final_payment_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->final_payment_money:Lcom/squareup/protos/common/Money;

    .line 248
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_count:Ljava/lang/Long;

    .line 249
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_frequency:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_frequency:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

    .line 250
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 255
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 257
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 258
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 259
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->final_payment_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 260
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_count:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 261
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_frequency:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 262
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;
    .locals 2

    .line 232
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;-><init>()V

    .line 233
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->payment_money:Lcom/squareup/protos/common/Money;

    .line 234
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->final_payment_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->final_payment_money:Lcom/squareup/protos/common/Money;

    .line 235
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->payment_count:Ljava/lang/Long;

    .line 236
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_frequency:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->payment_frequency:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

    .line 237
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 183
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 269
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 270
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    const-string v1, ", payment_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 271
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->final_payment_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", final_payment_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->final_payment_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 272
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_count:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", payment_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 273
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_frequency:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

    if-eqz v1, :cond_3

    const-string v1, ", payment_frequency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_frequency:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FixedPayments{"

    .line 274
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
