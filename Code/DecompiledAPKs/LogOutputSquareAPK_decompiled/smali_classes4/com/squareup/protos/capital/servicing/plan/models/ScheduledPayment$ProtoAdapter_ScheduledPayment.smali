.class final Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$ProtoAdapter_ScheduledPayment;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ScheduledPayment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ScheduledPayment"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 364
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 397
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;-><init>()V

    .line 398
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 399
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 418
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 416
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_late_fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;

    goto :goto_0

    .line 415
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_past_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;

    goto :goto_0

    .line 414
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;

    goto :goto_0

    .line 413
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_non_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;

    goto :goto_0

    .line 412
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->late_fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;

    goto :goto_0

    .line 411
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->late_fee_applied_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;

    goto :goto_0

    .line 410
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->debit_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;

    goto :goto_0

    .line 409
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->debited_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;

    goto :goto_0

    .line 403
    :pswitch_8
    :try_start_0
    sget-object v4, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->status(Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 405
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 422
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 423
    invoke-virtual {v0}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 362
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$ProtoAdapter_ScheduledPayment;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 383
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->status:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 384
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debited_at:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 385
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debit_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 386
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_applied_at:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 387
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 388
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_non_due_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 389
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_due_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 390
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_past_due_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 391
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_late_fee_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 392
    invoke-virtual {p2}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 362
    check-cast p2, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$ProtoAdapter_ScheduledPayment;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;)I
    .locals 4

    .line 369
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->status:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debited_at:Ljava/lang/String;

    const/4 v3, 0x2

    .line 370
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debit_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x3

    .line 371
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_applied_at:Ljava/lang/String;

    const/4 v3, 0x4

    .line 372
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x5

    .line 373
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_non_due_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x6

    .line 374
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_due_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x7

    .line 375
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_past_due_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x8

    .line 376
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_late_fee_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x9

    .line 377
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 378
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 362
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$ProtoAdapter_ScheduledPayment;->encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;
    .locals 2

    .line 428
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;

    move-result-object p1

    .line 429
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->debit_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->debit_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->debit_money:Lcom/squareup/protos/common/Money;

    .line 430
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->late_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->late_fee_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->late_fee_money:Lcom/squareup/protos/common/Money;

    .line 431
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_non_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_non_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_non_due_money:Lcom/squareup/protos/common/Money;

    .line 432
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_due_money:Lcom/squareup/protos/common/Money;

    .line 433
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_past_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_past_due_money:Lcom/squareup/protos/common/Money;

    .line 434
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_late_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_late_fee_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_late_fee_money:Lcom/squareup/protos/common/Money;

    .line 435
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 436
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 362
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$ProtoAdapter_ScheduledPayment;->redact(Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;

    move-result-object p1

    return-object p1
.end method
