.class public final Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RepaymentTerms.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;",
        "Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public hold_rate_bps:Ljava/lang/String;

.field public minimum_payment_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 113
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;
    .locals 4

    .line 133
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;->hold_rate_bps:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;->minimum_payment_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    move-result-object v0

    return-object v0
.end method

.method public hold_rate_bps(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;->hold_rate_bps:Ljava/lang/String;

    return-object p0
.end method

.method public minimum_payment_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;->minimum_payment_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
