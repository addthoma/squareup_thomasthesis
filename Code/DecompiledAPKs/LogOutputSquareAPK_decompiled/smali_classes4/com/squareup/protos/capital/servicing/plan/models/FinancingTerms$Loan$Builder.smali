.class public final Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FinancingTerms.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;",
        "Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public apr_bps:Ljava/lang/String;

.field public deposit_money:Lcom/squareup/protos/common/Money;

.field public fee_amount_bps:Ljava/lang/String;

.field public fee_money:Lcom/squareup/protos/common/Money;

.field public financed_money:Lcom/squareup/protos/common/Money;

.field public late_fee_bps:Ljava/lang/String;

.field public outstanding_balance_money:Lcom/squareup/protos/common/Money;

.field public post_maturity_sweeper_bps:Ljava/lang/String;

.field public total_owed_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 236
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public apr_bps(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;
    .locals 0

    .line 285
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->apr_bps:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;
    .locals 12

    .line 315
    new-instance v11, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->financed_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->fee_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->deposit_money:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->total_owed_money:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->apr_bps:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->late_fee_bps:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->post_maturity_sweeper_bps:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->fee_amount_bps:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 217
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    move-result-object v0

    return-object v0
.end method

.method public deposit_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;
    .locals 0

    .line 261
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->deposit_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public fee_amount_bps(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;
    .locals 0

    .line 309
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->fee_amount_bps:Ljava/lang/String;

    return-object p0
.end method

.method public fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;
    .locals 0

    .line 251
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->fee_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public financed_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->financed_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public late_fee_bps(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;
    .locals 0

    .line 293
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->late_fee_bps:Ljava/lang/String;

    return-object p0
.end method

.method public outstanding_balance_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public post_maturity_sweeper_bps(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;
    .locals 0

    .line 301
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->post_maturity_sweeper_bps:Ljava/lang/String;

    return-object p0
.end method

.method public total_owed_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;
    .locals 0

    .line 277
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->total_owed_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
