.class public final enum Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;
.super Ljava/lang/Enum;
.source "ScheduledPayment.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status$ProtoAdapter_Status;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CANCELED:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

.field public static final enum COMPLETED:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

.field public static final enum DUE:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

.field public static final enum PAST_DUE:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

.field public static final enum PENDING:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

.field public static final enum SCHEDULED:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

.field public static final enum S_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 307
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    const/4 v1, 0x0

    const-string v2, "S_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->S_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    .line 309
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    const/4 v2, 0x1

    const-string v3, "SCHEDULED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->SCHEDULED:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    .line 311
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    const/4 v3, 0x2

    const-string v4, "COMPLETED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->COMPLETED:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    .line 313
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    const/4 v4, 0x3

    const-string v5, "DUE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->DUE:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    .line 315
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    const/4 v5, 0x4

    const-string v6, "PAST_DUE"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->PAST_DUE:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    .line 317
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    const/4 v6, 0x5

    const-string v7, "CANCELED"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->CANCELED:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    .line 319
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    const/4 v7, 0x6

    const-string v8, "PENDING"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->PENDING:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    .line 306
    sget-object v8, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->S_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->SCHEDULED:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->COMPLETED:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->DUE:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->PAST_DUE:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->CANCELED:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->PENDING:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->$VALUES:[Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    .line 321
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status$ProtoAdapter_Status;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status$ProtoAdapter_Status;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 325
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 326
    iput p3, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 340
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->PENDING:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    return-object p0

    .line 339
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->CANCELED:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    return-object p0

    .line 338
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->PAST_DUE:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    return-object p0

    .line 337
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->DUE:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    return-object p0

    .line 336
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->COMPLETED:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    return-object p0

    .line 335
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->SCHEDULED:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    return-object p0

    .line 334
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->S_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;
    .locals 1

    .line 306
    const-class v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;
    .locals 1

    .line 306
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->$VALUES:[Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    invoke-virtual {v0}, [Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 347
    iget v0, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->value:I

    return v0
.end method
