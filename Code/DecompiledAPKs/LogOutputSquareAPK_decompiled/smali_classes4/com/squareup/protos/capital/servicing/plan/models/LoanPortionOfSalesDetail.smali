.class public final Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;
.super Lcom/squareup/wire/Message;
.source "LoanPortionOfSalesDetail.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$ProtoAdapter_LoanPortionOfSalesDetail;,
        Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;",
        "Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PAST_DUE_STARTED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_PAYMENT_PERIOD_ENDED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE_COMPLETE:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE_COMPLETE_INCLUDING_PENDING:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final due_and_past_due_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final due_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.FinancingTerms$Loan#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final paid_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x12
    .end annotation
.end field

.field public final past_due_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final past_due_started_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final payment_period_ended_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final pending_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final percentage_complete:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final percentage_complete_including_pending:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x16
    .end annotation
.end field

.field public final period_paid_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x11
    .end annotation
.end field

.field public final period_remaining_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final period_required_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x15
    .end annotation
.end field

.field public final remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x14
    .end annotation
.end field

.field public final remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x13
    .end annotation
.end field

.field public final remaining_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.RepaymentTerms$PortionOfSales#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final total_due_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xe
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$ProtoAdapter_LoanPortionOfSalesDetail;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$ProtoAdapter_LoanPortionOfSalesDetail;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;Lokio/ByteString;)V
    .locals 1

    .line 223
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 224
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    .line 225
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    .line 226
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->due_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_money:Lcom/squareup/protos/common/Money;

    .line 227
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_started_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_started_at:Ljava/lang/String;

    .line 228
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    .line 229
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->paid_money:Lcom/squareup/protos/common/Money;

    .line 230
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    .line 231
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->pending_money:Lcom/squareup/protos/common/Money;

    .line 232
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->payment_period_ended_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->payment_period_ended_at:Ljava/lang/String;

    .line 233
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->percentage_complete:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete:Ljava/lang/String;

    .line 234
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_required_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_required_money:Lcom/squareup/protos/common/Money;

    .line 235
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_paid_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_paid_money:Lcom/squareup/protos/common/Money;

    .line 236
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_remaining_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_money:Lcom/squareup/protos/common/Money;

    .line 237
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->total_due_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    .line 238
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    .line 239
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    .line 240
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 241
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 242
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 243
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;

    .line 244
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    .line 245
    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->percentage_complete_including_pending:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete_including_pending:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 280
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 281
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    .line 282
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    .line 283
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    .line 284
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_money:Lcom/squareup/protos/common/Money;

    .line 285
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_started_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_started_at:Ljava/lang/String;

    .line 286
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    .line 287
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->paid_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->paid_money:Lcom/squareup/protos/common/Money;

    .line 288
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    .line 289
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->pending_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->pending_money:Lcom/squareup/protos/common/Money;

    .line 290
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->payment_period_ended_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->payment_period_ended_at:Ljava/lang/String;

    .line 291
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete:Ljava/lang/String;

    .line 292
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_required_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_required_money:Lcom/squareup/protos/common/Money;

    .line 293
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_paid_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_paid_money:Lcom/squareup/protos/common/Money;

    .line 294
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_money:Lcom/squareup/protos/common/Money;

    .line 295
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    .line 296
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    .line 297
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    .line 298
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 299
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 300
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 301
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;

    .line 302
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    .line 303
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete_including_pending:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete_including_pending:Ljava/lang/String;

    .line 304
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 309
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_16

    .line 311
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 312
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 313
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 314
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 315
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_started_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 316
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 317
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->paid_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 318
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 319
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 320
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->payment_period_ended_at:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 321
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 322
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_required_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 323
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_paid_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 324
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 325
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 326
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 327
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 328
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 329
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 330
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 331
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 332
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 333
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete_including_pending:Ljava/lang/String;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_15
    add-int/2addr v0, v2

    .line 334
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_16
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 2

    .line 250
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;-><init>()V

    .line 251
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    .line 252
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    .line 253
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->due_money:Lcom/squareup/protos/common/Money;

    .line 254
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_started_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_started_at:Ljava/lang/String;

    .line 255
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    .line 256
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->paid_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    .line 257
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    .line 258
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->pending_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    .line 259
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->payment_period_ended_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->payment_period_ended_at:Ljava/lang/String;

    .line 260
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->percentage_complete:Ljava/lang/String;

    .line 261
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_required_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_required_money:Lcom/squareup/protos/common/Money;

    .line 262
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_paid_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_paid_money:Lcom/squareup/protos/common/Money;

    .line 263
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_remaining_money:Lcom/squareup/protos/common/Money;

    .line 264
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->total_due_money:Lcom/squareup/protos/common/Money;

    .line 265
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    .line 266
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    .line 267
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 268
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 269
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 270
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;

    .line 271
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    .line 272
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete_including_pending:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->percentage_complete_including_pending:Ljava/lang/String;

    .line 273
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 341
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 342
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    if-eqz v1, :cond_0

    const-string v1, ", financing_terms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 343
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    if-eqz v1, :cond_1

    const-string v1, ", repayment_terms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 344
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", due_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 345
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_started_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", past_due_started_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_started_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", past_due_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 347
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->paid_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", paid_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->paid_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 348
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", remaining_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 349
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    const-string v1, ", pending_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 350
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->payment_period_ended_at:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", payment_period_ended_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->payment_period_ended_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", percentage_complete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_required_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_a

    const-string v1, ", period_required_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_required_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 353
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_paid_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_b

    const-string v1, ", period_paid_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_paid_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 354
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_c

    const-string v1, ", period_remaining_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 355
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_d

    const-string v1, ", total_due_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 356
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_e

    const-string v1, ", due_and_past_due_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 357
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_f

    const-string v1, ", remaining_excluding_past_due_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 358
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_10

    const-string v1, ", period_remaining_excluding_pending_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 359
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_11

    const-string v1, ", past_due_excluding_pending_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 360
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_12

    const-string v1, ", remaining_excluding_pending_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 361
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_13

    const-string v1, ", remaining_excluding_past_due_and_period_remaining_and_pending_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 362
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_14

    const-string v1, ", remaining_excluding_past_due_and_pending_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 363
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete_including_pending:Ljava/lang/String;

    if-eqz v1, :cond_15

    const-string v1, ", percentage_complete_including_pending="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete_including_pending:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_15
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LoanPortionOfSalesDetail{"

    .line 364
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
