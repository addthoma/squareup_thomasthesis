.class final Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType$ProtoAdapter_DocumentType;
.super Lcom/squareup/wire/EnumAdapter;
.source "Document.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_DocumentType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 175
    const-class v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;
    .locals 0

    .line 180
    invoke-static {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->fromValue(I)Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 173
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType$ProtoAdapter_DocumentType;->fromValue(I)Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    move-result-object p1

    return-object p1
.end method
