.class public final Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetAllNotificationSettingsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse;",
        "Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public notification_group:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/squareup/protos/invoice/v2/common/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 95
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 96
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse$Builder;->notification_group:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse;
    .locals 4

    .line 112
    new-instance v0, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse;

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse$Builder;->status:Lcom/squareup/protos/invoice/v2/common/Status;

    iget-object v2, p0, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse$Builder;->notification_group:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse;-><init>(Lcom/squareup/protos/invoice/v2/common/Status;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse$Builder;->build()Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse;

    move-result-object v0

    return-object v0
.end method

.method public notification_group(Ljava/util/List;)Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup;",
            ">;)",
            "Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse$Builder;"
        }
    .end annotation

    .line 105
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse$Builder;->notification_group:Ljava/util/List;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/invoice/v2/common/Status;)Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse$Builder;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse$Builder;->status:Lcom/squareup/protos/invoice/v2/common/Status;

    return-object p0
.end method
