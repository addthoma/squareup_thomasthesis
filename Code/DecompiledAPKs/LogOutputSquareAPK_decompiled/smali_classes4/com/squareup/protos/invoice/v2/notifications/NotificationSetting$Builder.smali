.class public final Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NotificationSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;",
        "Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public category:Ljava/lang/String;

.field public category_description:Ljava/lang/String;

.field public category_label:Ljava/lang/String;

.field public email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

.field public push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 150
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;
    .locals 8

    .line 189
    new-instance v7, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->category:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->category_label:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->category_description:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    iget-object v5, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 139
    invoke-virtual {p0}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->build()Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;

    move-result-object v0

    return-object v0
.end method

.method public category(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;
    .locals 0

    .line 157
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->category:Ljava/lang/String;

    return-object p0
.end method

.method public category_description(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->category_description:Ljava/lang/String;

    return-object p0
.end method

.method public category_label(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->category_label:Ljava/lang/String;

    return-object p0
.end method

.method public email_setting(Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    return-object p0
.end method

.method public push_setting(Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    return-object p0
.end method
