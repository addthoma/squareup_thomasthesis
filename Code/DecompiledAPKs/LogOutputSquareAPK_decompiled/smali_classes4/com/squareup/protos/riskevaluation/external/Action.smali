.class public final enum Lcom/squareup/protos/riskevaluation/external/Action;
.super Ljava/lang/Enum;
.source "Action.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/riskevaluation/external/Action$ProtoAdapter_Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/riskevaluation/external/Action;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/riskevaluation/external/Action;

.field public static final enum ACCEPT_AND_ALERT:Lcom/squareup/protos/riskevaluation/external/Action;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/riskevaluation/external/Action;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DECLINE:Lcom/squareup/protos/riskevaluation/external/Action;

.field public static final enum DECLINE_FAILED_AND_ALERT:Lcom/squareup/protos/riskevaluation/external/Action;

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/riskevaluation/external/Action;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 14
    new-instance v0, Lcom/squareup/protos/riskevaluation/external/Action;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/riskevaluation/external/Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/riskevaluation/external/Action;->DO_NOT_USE:Lcom/squareup/protos/riskevaluation/external/Action;

    .line 19
    new-instance v0, Lcom/squareup/protos/riskevaluation/external/Action;

    const/4 v2, 0x1

    const-string v3, "DECLINE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/riskevaluation/external/Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/riskevaluation/external/Action;->DECLINE:Lcom/squareup/protos/riskevaluation/external/Action;

    .line 24
    new-instance v0, Lcom/squareup/protos/riskevaluation/external/Action;

    const/4 v3, 0x2

    const-string v4, "ACCEPT_AND_ALERT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/riskevaluation/external/Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/riskevaluation/external/Action;->ACCEPT_AND_ALERT:Lcom/squareup/protos/riskevaluation/external/Action;

    .line 29
    new-instance v0, Lcom/squareup/protos/riskevaluation/external/Action;

    const/4 v4, 0x3

    const-string v5, "DECLINE_FAILED_AND_ALERT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/riskevaluation/external/Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/riskevaluation/external/Action;->DECLINE_FAILED_AND_ALERT:Lcom/squareup/protos/riskevaluation/external/Action;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/riskevaluation/external/Action;

    .line 13
    sget-object v5, Lcom/squareup/protos/riskevaluation/external/Action;->DO_NOT_USE:Lcom/squareup/protos/riskevaluation/external/Action;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/riskevaluation/external/Action;->DECLINE:Lcom/squareup/protos/riskevaluation/external/Action;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/riskevaluation/external/Action;->ACCEPT_AND_ALERT:Lcom/squareup/protos/riskevaluation/external/Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/riskevaluation/external/Action;->DECLINE_FAILED_AND_ALERT:Lcom/squareup/protos/riskevaluation/external/Action;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/riskevaluation/external/Action;->$VALUES:[Lcom/squareup/protos/riskevaluation/external/Action;

    .line 31
    new-instance v0, Lcom/squareup/protos/riskevaluation/external/Action$ProtoAdapter_Action;

    invoke-direct {v0}, Lcom/squareup/protos/riskevaluation/external/Action$ProtoAdapter_Action;-><init>()V

    sput-object v0, Lcom/squareup/protos/riskevaluation/external/Action;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 36
    iput p3, p0, Lcom/squareup/protos/riskevaluation/external/Action;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/riskevaluation/external/Action;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 47
    :cond_0
    sget-object p0, Lcom/squareup/protos/riskevaluation/external/Action;->DECLINE_FAILED_AND_ALERT:Lcom/squareup/protos/riskevaluation/external/Action;

    return-object p0

    .line 46
    :cond_1
    sget-object p0, Lcom/squareup/protos/riskevaluation/external/Action;->ACCEPT_AND_ALERT:Lcom/squareup/protos/riskevaluation/external/Action;

    return-object p0

    .line 45
    :cond_2
    sget-object p0, Lcom/squareup/protos/riskevaluation/external/Action;->DECLINE:Lcom/squareup/protos/riskevaluation/external/Action;

    return-object p0

    .line 44
    :cond_3
    sget-object p0, Lcom/squareup/protos/riskevaluation/external/Action;->DO_NOT_USE:Lcom/squareup/protos/riskevaluation/external/Action;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/riskevaluation/external/Action;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/riskevaluation/external/Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/riskevaluation/external/Action;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/riskevaluation/external/Action;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/riskevaluation/external/Action;->$VALUES:[Lcom/squareup/protos/riskevaluation/external/Action;

    invoke-virtual {v0}, [Lcom/squareup/protos/riskevaluation/external/Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/riskevaluation/external/Action;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 54
    iget v0, p0, Lcom/squareup/protos/riskevaluation/external/Action;->value:I

    return v0
.end method
