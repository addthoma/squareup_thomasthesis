.class public final Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Application.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/eventstream/v1/Application$Version;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/eventstream/v1/Application$Version;",
        "Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public build_id:Ljava/lang/String;

.field public major:Ljava/lang/Integer;

.field public minor:Ljava/lang/Integer;

.field public prerelease:Ljava/lang/Integer;

.field public prerelease_type:Ljava/lang/String;

.field public revision:Ljava/lang/Integer;

.field public version_code:Ljava/lang/String;

.field public version_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 398
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/eventstream/v1/Application$Version;
    .locals 11

    .line 461
    new-instance v10, Lcom/squareup/protos/eventstream/v1/Application$Version;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->major:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->minor:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->revision:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->prerelease_type:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->prerelease:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->build_id:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->version_name:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->version_code:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/eventstream/v1/Application$Version;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 381
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->build()Lcom/squareup/protos/eventstream/v1/Application$Version;

    move-result-object v0

    return-object v0
.end method

.method public build_id(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;
    .locals 0

    .line 439
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->build_id:Ljava/lang/String;

    return-object p0
.end method

.method public major(Ljava/lang/Integer;)Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 403
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->major:Ljava/lang/Integer;

    return-object p0
.end method

.method public minor(Ljava/lang/Integer;)Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 409
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->minor:Ljava/lang/Integer;

    return-object p0
.end method

.method public prerelease(Ljava/lang/Integer;)Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 431
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->prerelease:Ljava/lang/Integer;

    return-object p0
.end method

.method public prerelease_type(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 425
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->prerelease_type:Ljava/lang/String;

    return-object p0
.end method

.method public revision(Ljava/lang/Integer;)Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 415
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->revision:Ljava/lang/Integer;

    return-object p0
.end method

.method public version_code(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;
    .locals 0

    .line 455
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->version_code:Ljava/lang/String;

    return-object p0
.end method

.method public version_name(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;
    .locals 0

    .line 447
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->version_name:Ljava/lang/String;

    return-object p0
.end method
