.class public final Lcom/squareup/protos/eventstream/v1/Subject$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Subject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/eventstream/v1/Subject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/eventstream/v1/Subject;",
        "Lcom/squareup/protos/eventstream/v1/Subject$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public account_country_code:Ljava/lang/String;

.field public anonymized_user_token:Ljava/lang/String;

.field public employment_token:Ljava/lang/String;

.field public merchant_token:Ljava/lang/String;

.field public payment_token:Ljava/lang/String;

.field public person_token:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;

.field public user_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 218
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public account_country_code(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 243
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->account_country_code:Ljava/lang/String;

    return-object p0
.end method

.method public anonymized_user_token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->anonymized_user_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/eventstream/v1/Subject;
    .locals 11

    .line 289
    new-instance v10, Lcom/squareup/protos/eventstream/v1/Subject;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->user_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->anonymized_user_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->account_country_code:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->merchant_token:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->unit_token:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->person_token:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->employment_token:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->payment_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/eventstream/v1/Subject;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 201
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->build()Lcom/squareup/protos/eventstream/v1/Subject;

    move-result-object v0

    return-object v0
.end method

.method public employment_token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;
    .locals 0

    .line 275
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->employment_token:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;
    .locals 0

    .line 251
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public payment_token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;
    .locals 0

    .line 283
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->payment_token:Ljava/lang/String;

    return-object p0
.end method

.method public person_token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;
    .locals 0

    .line 267
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->person_token:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;
    .locals 0

    .line 259
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method

.method public user_token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 226
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->user_token:Ljava/lang/String;

    return-object p0
.end method
