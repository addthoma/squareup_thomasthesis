.class public final enum Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;
.super Ljava/lang/Enum;
.source "GetUnifiedActivitiesRequest.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActivityStateCategory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory$ProtoAdapter_ActivityStateCategory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum COMPLETED:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

.field public static final enum PENDING:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 189
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "PENDING"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;->PENDING:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    .line 194
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    const/4 v3, 0x2

    const-string v4, "COMPLETED"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;->COMPLETED:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    new-array v0, v3, [Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    .line 185
    sget-object v3, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;->PENDING:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;->COMPLETED:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;->$VALUES:[Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    .line 196
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory$ProtoAdapter_ActivityStateCategory;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory$ProtoAdapter_ActivityStateCategory;-><init>()V

    sput-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 200
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 201
    iput p3, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 210
    :cond_0
    sget-object p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;->COMPLETED:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    return-object p0

    .line 209
    :cond_1
    sget-object p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;->PENDING:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;
    .locals 1

    .line 185
    const-class v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;
    .locals 1

    .line 185
    sget-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;->$VALUES:[Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    invoke-virtual {v0}, [Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 217
    iget v0, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;->value:I

    return v0
.end method
