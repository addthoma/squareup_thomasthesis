.class public final Lcom/squareup/protos/bizbank/UnifiedActivityDetails;
.super Lcom/squareup/wire/Message;
.source "UnifiedActivityDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/bizbank/UnifiedActivityDetails$ProtoAdapter_UnifiedActivityDetails;,
        Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;,
        Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails;",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/bizbank/UnifiedActivityDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_IS_PERSONAL_EXPENSE:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final gross_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final is_personal_expense:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final modifiers:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bizbank.UnifiedActivityDetails$Modifier#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
            ">;"
        }
    .end annotation
.end field

.field public final net_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$ProtoAdapter_UnifiedActivityDetails;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$ProtoAdapter_UnifiedActivityDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->DEFAULT_IS_PERSONAL_EXPENSE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            ")V"
        }
    .end annotation

    .line 81
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 86
    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 87
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->description:Ljava/lang/String;

    .line 88
    iput-object p2, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->is_personal_expense:Ljava/lang/Boolean;

    const-string p1, "modifiers"

    .line 89
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->modifiers:Ljava/util/List;

    .line 90
    iput-object p4, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->gross_amount:Lcom/squareup/protos/common/Money;

    .line 91
    iput-object p5, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->net_amount:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 109
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 110
    :cond_1
    check-cast p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->description:Ljava/lang/String;

    .line 112
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->is_personal_expense:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->is_personal_expense:Ljava/lang/Boolean;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->modifiers:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->modifiers:Ljava/util/List;

    .line 114
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->gross_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->gross_amount:Lcom/squareup/protos/common/Money;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->net_amount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->net_amount:Lcom/squareup/protos/common/Money;

    .line 116
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 121
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 123
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->description:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->is_personal_expense:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->modifiers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->gross_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->net_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 129
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;
    .locals 2

    .line 96
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;-><init>()V

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->description:Ljava/lang/String;

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->is_personal_expense:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->is_personal_expense:Ljava/lang/Boolean;

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->modifiers:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->modifiers:Ljava/util/List;

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->gross_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->gross_amount:Lcom/squareup/protos/common/Money;

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->net_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->net_amount:Lcom/squareup/protos/common/Money;

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->newBuilder()Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->description:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->is_personal_expense:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", is_personal_expense="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->is_personal_expense:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 139
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->modifiers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", modifiers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->modifiers:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 140
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->gross_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", gross_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->gross_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 141
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->net_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", net_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->net_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "UnifiedActivityDetails{"

    .line 142
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
