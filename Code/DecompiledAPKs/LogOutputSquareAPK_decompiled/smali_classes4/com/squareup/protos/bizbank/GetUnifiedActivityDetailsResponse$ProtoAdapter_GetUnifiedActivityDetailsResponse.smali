.class final Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$ProtoAdapter_GetUnifiedActivityDetailsResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetUnifiedActivityDetailsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetUnifiedActivityDetailsResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 270
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 290
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;-><init>()V

    .line 291
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 292
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 297
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 295
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;->error_message(Ljava/lang/String;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;

    goto :goto_0

    .line 294
    :cond_1
    sget-object v3, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;->response(Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;

    goto :goto_0

    .line 301
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 302
    invoke-virtual {v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;->build()Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 268
    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$ProtoAdapter_GetUnifiedActivityDetailsResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 283
    sget-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;->response:Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 284
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;->error_message:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 285
    invoke-virtual {p2}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 268
    check-cast p2, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$ProtoAdapter_GetUnifiedActivityDetailsResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;)I
    .locals 4

    .line 275
    sget-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;->response:Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;->error_message:Ljava/lang/String;

    const/4 v3, 0x2

    .line 276
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 277
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 268
    check-cast p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$ProtoAdapter_GetUnifiedActivityDetailsResponse;->encodedSize(Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;
    .locals 2

    .line 307
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;->newBuilder()Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;

    move-result-object p1

    .line 308
    iget-object v0, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;->response:Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;->response:Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    iput-object v0, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;->response:Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    .line 309
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 310
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;->build()Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 268
    check-cast p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$ProtoAdapter_GetUnifiedActivityDetailsResponse;->redact(Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;

    move-result-object p1

    return-object p1
.end method
