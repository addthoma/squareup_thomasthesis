.class public final Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetUnifiedActivitiesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response;",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public has_hidden_balance_activity_transactions:Ljava/lang/Boolean;

.field public pagination_token:Ljava/lang/String;

.field public unified_activities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 221
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 222
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response$Builder;->unified_activities:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response;
    .locals 5

    .line 255
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response$Builder;->unified_activities:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response$Builder;->pagination_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response$Builder;->has_hidden_balance_activity_transactions:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 214
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response$Builder;->build()Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response;

    move-result-object v0

    return-object v0
.end method

.method public has_hidden_balance_activity_transactions(Ljava/lang/Boolean;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response$Builder;
    .locals 0

    .line 249
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response$Builder;->has_hidden_balance_activity_transactions:Ljava/lang/Boolean;

    return-object p0
.end method

.method public pagination_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response$Builder;
    .locals 0

    .line 238
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response$Builder;->pagination_token:Ljava/lang/String;

    return-object p0
.end method

.method public unified_activities(Ljava/util/List;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivity;",
            ">;)",
            "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response$Builder;"
        }
    .end annotation

    .line 229
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 230
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response$Builder;->unified_activities:Ljava/util/List;

    return-object p0
.end method
