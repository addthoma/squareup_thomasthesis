.class public final Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetUnifiedActivitiesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse;",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error_message:Ljava/lang/String;

.field public response:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse;
    .locals 4

    .line 115
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Builder;->response:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Builder;->error_message:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse;-><init>(Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Builder;->build()Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse;

    move-result-object v0

    return-object v0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Builder;->error_message:Ljava/lang/String;

    const/4 p1, 0x0

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Builder;->response:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response;

    return-object p0
.end method

.method public response(Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Builder;
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Builder;->response:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Response;

    const/4 p1, 0x0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method
