.class public final Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UnifiedActivityDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/UnifiedActivityDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails;",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public description:Ljava/lang/String;

.field public gross_amount:Lcom/squareup/protos/common/Money;

.field public is_personal_expense:Ljava/lang/Boolean;

.field public modifiers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
            ">;"
        }
    .end annotation
.end field

.field public net_amount:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 156
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 157
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->modifiers:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;
    .locals 8

    .line 203
    new-instance v7, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->description:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->is_personal_expense:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->modifiers:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->gross_amount:Lcom/squareup/protos/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->net_amount:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 145
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->build()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object v0

    return-object v0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public gross_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->gross_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public is_personal_expense(Ljava/lang/Boolean;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->is_personal_expense:Ljava/lang/Boolean;

    return-object p0
.end method

.method public modifiers(Ljava/util/List;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
            ">;)",
            "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;"
        }
    .end annotation

    .line 180
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->modifiers:Ljava/util/List;

    return-object p0
.end method

.method public net_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->net_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
