.class public final enum Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;
.super Ljava/lang/Enum;
.source "UnifiedActivityDetails.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ModifierType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType$ProtoAdapter_ModifierType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ATM_WITHDRAWAL_FEE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

.field public static final enum CASHBACK:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

.field public static final enum COMMUNITY_REWARD:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

.field public static final enum INSTANT_TRANSFER_FEE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 340
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->DO_NOT_USE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    .line 342
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    const/4 v2, 0x1

    const-string v3, "COMMUNITY_REWARD"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->COMMUNITY_REWARD:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    .line 344
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    const/4 v3, 0x2

    const-string v4, "ATM_WITHDRAWAL_FEE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->ATM_WITHDRAWAL_FEE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    .line 346
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    const/4 v4, 0x3

    const-string v5, "INSTANT_TRANSFER_FEE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->INSTANT_TRANSFER_FEE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    .line 348
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    const/4 v5, 0x4

    const-string v6, "CASHBACK"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->CASHBACK:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    .line 339
    sget-object v6, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->DO_NOT_USE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->COMMUNITY_REWARD:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->ATM_WITHDRAWAL_FEE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->INSTANT_TRANSFER_FEE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->CASHBACK:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->$VALUES:[Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    .line 350
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType$ProtoAdapter_ModifierType;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType$ProtoAdapter_ModifierType;-><init>()V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 354
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 355
    iput p3, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 367
    :cond_0
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->CASHBACK:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    return-object p0

    .line 366
    :cond_1
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->INSTANT_TRANSFER_FEE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    return-object p0

    .line 365
    :cond_2
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->ATM_WITHDRAWAL_FEE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    return-object p0

    .line 364
    :cond_3
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->COMMUNITY_REWARD:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    return-object p0

    .line 363
    :cond_4
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->DO_NOT_USE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;
    .locals 1

    .line 339
    const-class v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;
    .locals 1

    .line 339
    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->$VALUES:[Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    invoke-virtual {v0}, [Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 374
    iget v0, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->value:I

    return v0
.end method
