.class public final Lcom/squareup/protos/bizbank/PrivateCardData$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PrivateCardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/PrivateCardData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/PrivateCardData;",
        "Lcom/squareup/protos/bizbank/PrivateCardData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cvc:Ljava/lang/String;

.field public expiration:Lcom/squareup/protos/common/time/YearMonth;

.field public full_pan:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 124
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/bizbank/PrivateCardData;
    .locals 5

    .line 153
    new-instance v0, Lcom/squareup/protos/bizbank/PrivateCardData;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->full_pan:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->cvc:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->expiration:Lcom/squareup/protos/common/time/YearMonth;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/bizbank/PrivateCardData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonth;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->build()Lcom/squareup/protos/bizbank/PrivateCardData;

    move-result-object v0

    return-object v0
.end method

.method public cvc(Ljava/lang/String;)Lcom/squareup/protos/bizbank/PrivateCardData$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->cvc:Ljava/lang/String;

    return-object p0
.end method

.method public expiration(Lcom/squareup/protos/common/time/YearMonth;)Lcom/squareup/protos/bizbank/PrivateCardData$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->expiration:Lcom/squareup/protos/common/time/YearMonth;

    return-object p0
.end method

.method public full_pan(Ljava/lang/String;)Lcom/squareup/protos/bizbank/PrivateCardData$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->full_pan:Ljava/lang/String;

    return-object p0
.end method
