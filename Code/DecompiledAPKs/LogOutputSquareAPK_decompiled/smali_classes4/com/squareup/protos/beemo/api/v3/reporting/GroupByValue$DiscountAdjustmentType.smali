.class public final enum Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;
.super Ljava/lang/Enum;
.source "GroupByValue.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DiscountAdjustmentType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType$ProtoAdapter_DiscountAdjustmentType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum COMP:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

.field public static final enum COUPON:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

.field public static final enum DISCOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

.field public static final enum NO_DISCOUNT_ADJUSTMENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

.field public static final enum REWARD:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 4858
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    const/4 v1, 0x0

    const-string v2, "DISCOUNT"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->DISCOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    .line 4860
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    const/4 v2, 0x1

    const-string v3, "REWARD"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->REWARD:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    .line 4862
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    const/4 v3, 0x2

    const-string v4, "COUPON"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->COUPON:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    .line 4864
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    const/4 v4, 0x3

    const-string v5, "COMP"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->COMP:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    .line 4866
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    const/4 v5, 0x4

    const-string v6, "NO_DISCOUNT_ADJUSTMENT_TYPE"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->NO_DISCOUNT_ADJUSTMENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    .line 4857
    sget-object v6, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->DISCOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->REWARD:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->COUPON:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->COMP:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->NO_DISCOUNT_ADJUSTMENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    .line 4868
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType$ProtoAdapter_DiscountAdjustmentType;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType$ProtoAdapter_DiscountAdjustmentType;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 4872
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 4873
    iput p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 4885
    :cond_0
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->NO_DISCOUNT_ADJUSTMENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    return-object p0

    .line 4884
    :cond_1
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->COMP:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    return-object p0

    .line 4883
    :cond_2
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->COUPON:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    return-object p0

    .line 4882
    :cond_3
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->REWARD:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    return-object p0

    .line 4881
    :cond_4
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->DISCOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;
    .locals 1

    .line 4857
    const-class v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;
    .locals 1

    .line 4857
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 4892
    iget v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->value:I

    return v0
.end method
