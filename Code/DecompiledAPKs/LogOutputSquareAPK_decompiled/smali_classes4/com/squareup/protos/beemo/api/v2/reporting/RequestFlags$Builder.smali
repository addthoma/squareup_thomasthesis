.class public final Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RequestFlags.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;",
        "Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_partial_fanouts:Ljava/lang/Boolean;

.field public fetch_contact_tokens:Ljava/lang/Boolean;

.field public force_include_future_effective_at_adjustment_fees:Ljava/lang/Boolean;

.field public ignore_items_service_failures:Ljava/lang/Boolean;

.field public skip_cost_of_goods_sold:Ljava/lang/Boolean;

.field public skip_ledger_fees:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 181
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public allow_partial_fanouts(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;
    .locals 0

    .line 231
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->allow_partial_fanouts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;
    .locals 9

    .line 237
    new-instance v8, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->skip_ledger_fees:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->skip_cost_of_goods_sold:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->ignore_items_service_failures:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->force_include_future_effective_at_adjustment_fees:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->fetch_contact_tokens:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->allow_partial_fanouts:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 168
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->build()Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    move-result-object v0

    return-object v0
.end method

.method public fetch_contact_tokens(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->fetch_contact_tokens:Ljava/lang/Boolean;

    return-object p0
.end method

.method public force_include_future_effective_at_adjustment_fees(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->force_include_future_effective_at_adjustment_fees:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ignore_items_service_failures(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->ignore_items_service_failures:Ljava/lang/Boolean;

    return-object p0
.end method

.method public skip_cost_of_goods_sold(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->skip_cost_of_goods_sold:Ljava/lang/Boolean;

    return-object p0
.end method

.method public skip_ledger_fees(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->skip_ledger_fees:Ljava/lang/Boolean;

    return-object p0
.end method
