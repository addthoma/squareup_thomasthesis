.class public final Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CustomReportResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_event_token:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public custom_report:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
            ">;"
        }
    .end annotation
.end field

.field public sales_inside_range_detail:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;",
            ">;"
        }
    .end annotation
.end field

.field public sales_outside_range_detail:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 136
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 137
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->custom_report:Ljava/util/List;

    .line 138
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->bill_event_token:Ljava/util/List;

    .line 139
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->sales_inside_range_detail:Ljava/util/List;

    .line 140
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->sales_outside_range_detail:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bill_event_token(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;"
        }
    .end annotation

    .line 153
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 154
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->bill_event_token:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;
    .locals 7

    .line 180
    new-instance v6, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->custom_report:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->bill_event_token:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->sales_inside_range_detail:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->sales_outside_range_detail:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 127
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    move-result-object v0

    return-object v0
.end method

.method public custom_report(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;"
        }
    .end annotation

    .line 144
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->custom_report:Ljava/util/List;

    return-object p0
.end method

.method public sales_inside_range_detail(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;"
        }
    .end annotation

    .line 163
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->sales_inside_range_detail:Ljava/util/List;

    return-object p0
.end method

.method public sales_outside_range_detail(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;"
        }
    .end annotation

    .line 173
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->sales_outside_range_detail:Ljava/util/List;

    return-object p0
.end method
