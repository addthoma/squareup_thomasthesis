.class final Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$ProtoAdapter_ThirdPartyFeeAmountDetail;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Aggregate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ThirdPartyFeeAmountDetail"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1836
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1856
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;-><init>()V

    .line 1857
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1858
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 1863
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1861
    :cond_0
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->third_party_fee_entity(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;

    goto :goto_0

    .line 1860
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;

    goto :goto_0

    .line 1867
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1868
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1834
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$ProtoAdapter_ThirdPartyFeeAmountDetail;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1849
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;->amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1850
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;->third_party_fee_entity:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1851
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1834
    check-cast p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$ProtoAdapter_ThirdPartyFeeAmountDetail;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;)I
    .locals 4

    .line 1841
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;->amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;->third_party_fee_entity:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    const/4 v3, 0x2

    .line 1842
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1843
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1834
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$ProtoAdapter_ThirdPartyFeeAmountDetail;->encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;
    .locals 2

    .line 1873
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;

    move-result-object p1

    .line 1874
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->amount:Lcom/squareup/protos/common/Money;

    .line 1875
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->third_party_fee_entity:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->third_party_fee_entity:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->third_party_fee_entity:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    .line 1876
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1877
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1834
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$ProtoAdapter_ThirdPartyFeeAmountDetail;->redact(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;

    move-result-object p1

    return-object p1
.end method
