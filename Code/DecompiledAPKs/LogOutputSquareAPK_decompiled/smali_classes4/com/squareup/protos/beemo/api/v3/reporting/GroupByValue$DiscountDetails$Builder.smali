.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

.field public rate:Ljava/lang/String;

.field public single_quantity_applied_money:Lcom/squareup/protos/common/Money;

.field public type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5622
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;
    .locals 7

    .line 5653
    new-instance v6, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;->type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;->rate:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 5613
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;

    move-result-object v0

    return-object v0
.end method

.method public discount(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;
    .locals 0

    .line 5629
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    return-object p0
.end method

.method public rate(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;
    .locals 0

    .line 5642
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;->rate:Ljava/lang/String;

    return-object p0
.end method

.method public single_quantity_applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;
    .locals 0

    .line 5647
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;
    .locals 0

    .line 5634
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;->type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    return-object p0
.end method
