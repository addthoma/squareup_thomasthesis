.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;
.super Lcom/squareup/wire/Message;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Item"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$ProtoAdapter_Item;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ABBREVIATION:Ljava/lang/String; = ""

.field public static final DEFAULT_IMAGE_URL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final abbreviation:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final icon_color:Lcom/squareup/protos/common/RGBAColor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.RGBAColor#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final image_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.translation_types.NameOrTranslationType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 2056
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$ProtoAdapter_Item;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$ProtoAdapter_Item;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Ljava/lang/String;Lcom/squareup/protos/common/RGBAColor;Ljava/lang/String;)V
    .locals 6

    .line 2106
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Ljava/lang/String;Lcom/squareup/protos/common/RGBAColor;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Ljava/lang/String;Lcom/squareup/protos/common/RGBAColor;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 2111
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 2112
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    .line 2113
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->image_url:Ljava/lang/String;

    .line 2114
    iput-object p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->icon_color:Lcom/squareup/protos/common/RGBAColor;

    .line 2115
    iput-object p4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->abbreviation:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 2132
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2133
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    .line 2134
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    .line 2135
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->image_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->image_url:Ljava/lang/String;

    .line 2136
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->icon_color:Lcom/squareup/protos/common/RGBAColor;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->icon_color:Lcom/squareup/protos/common/RGBAColor;

    .line 2137
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->abbreviation:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->abbreviation:Ljava/lang/String;

    .line 2138
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 2143
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 2145
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 2146
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2147
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->image_url:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2148
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->icon_color:Lcom/squareup/protos/common/RGBAColor;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/RGBAColor;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2149
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->abbreviation:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 2150
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;
    .locals 2

    .line 2120
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;-><init>()V

    .line 2121
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    .line 2122
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->image_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;->image_url:Ljava/lang/String;

    .line 2123
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->icon_color:Lcom/squareup/protos/common/RGBAColor;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;->icon_color:Lcom/squareup/protos/common/RGBAColor;

    .line 2124
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->abbreviation:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;->abbreviation:Ljava/lang/String;

    .line 2125
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 2055
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2157
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2158
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    if-eqz v1, :cond_0

    const-string v1, ", name_or_translation_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2159
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->image_url:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", image_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->image_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2160
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->icon_color:Lcom/squareup/protos/common/RGBAColor;

    if-eqz v1, :cond_2

    const-string v1, ", icon_color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->icon_color:Lcom/squareup/protos/common/RGBAColor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2161
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->abbreviation:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", abbreviation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->abbreviation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Item{"

    .line 2162
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
