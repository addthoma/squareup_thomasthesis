.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public note:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6356
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;
    .locals 3

    .line 6366
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote$Builder;->note:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 6353
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    move-result-object v0

    return-object v0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote$Builder;
    .locals 0

    .line 6360
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote$Builder;->note:Ljava/lang/String;

    return-object p0
.end method
