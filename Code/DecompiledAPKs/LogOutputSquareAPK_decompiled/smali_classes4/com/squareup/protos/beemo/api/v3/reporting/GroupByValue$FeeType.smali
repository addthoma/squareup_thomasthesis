.class public final enum Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;
.super Ljava/lang/Enum;
.source "GroupByValue.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FeeType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType$ProtoAdapter_FeeType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DEFAULT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

.field public static final enum DIPPED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

.field public static final enum KEYED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

.field public static final enum NO_FEE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

.field public static final enum ON_FILE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

.field public static final enum SWIPED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

.field public static final enum TAPPED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 3587
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "SWIPED"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->SWIPED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    .line 3592
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    const/4 v3, 0x2

    const-string v4, "KEYED"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->KEYED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    .line 3597
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    const/4 v4, 0x3

    const-string v5, "DEFAULT"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->DEFAULT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    .line 3602
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    const/4 v5, 0x4

    const-string v6, "NO_FEE"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->NO_FEE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    .line 3607
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    const/4 v6, 0x5

    const-string v7, "TAPPED"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->TAPPED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    .line 3612
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    const/4 v7, 0x6

    const-string v8, "DIPPED"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->DIPPED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    .line 3617
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    const/4 v8, 0x7

    const-string v9, "ON_FILE"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->ON_FILE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    new-array v0, v8, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    .line 3583
    sget-object v8, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->SWIPED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->KEYED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->DEFAULT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->NO_FEE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->TAPPED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->DIPPED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->ON_FILE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    .line 3619
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType$ProtoAdapter_FeeType;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType$ProtoAdapter_FeeType;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 3623
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3624
    iput p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 3638
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->ON_FILE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    return-object p0

    .line 3637
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->DIPPED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    return-object p0

    .line 3636
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->TAPPED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    return-object p0

    .line 3635
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->NO_FEE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    return-object p0

    .line 3634
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->DEFAULT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    return-object p0

    .line 3633
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->KEYED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    return-object p0

    .line 3632
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->SWIPED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;
    .locals 1

    .line 3583
    const-class v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;
    .locals 1

    .line 3583
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 3645
    iget v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->value:I

    return v0
.end method
