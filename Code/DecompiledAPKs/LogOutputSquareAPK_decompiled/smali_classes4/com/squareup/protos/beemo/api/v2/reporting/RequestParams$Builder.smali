.class public final Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RequestParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;",
        "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

.field public begin_time:Lcom/squareup/protos/common/time/DateTime;

.field public creator_token:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public end_time:Lcom/squareup/protos/common/time/DateTime;

.field public event_based:Ljava/lang/Boolean;

.field public merchant_token:Ljava/lang/String;

.field public request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

.field public selected_bill_token:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public selected_payment_token:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public sort_param:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v2/reporting/SortParams;",
            ">;"
        }
    .end annotation
.end field

.field public start_of_day_offset_minutes:Ljava/lang/Integer;

.field public subunit_merchant_token:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

.field public tz_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 339
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 340
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->creator_token:Ljava/util/List;

    .line 341
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->selected_payment_token:Ljava/util/List;

    .line 342
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->sort_param:Ljava/util/List;

    .line 343
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->subunit_merchant_token:Ljava/util/List;

    .line 344
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->selected_bill_token:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public beemo_internal_request_flags(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
    .locals 0

    .line 482
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    return-object p0
.end method

.method public begin_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
    .locals 0

    .line 362
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;
    .locals 18

    move-object/from16 v0, p0

    .line 488
    new-instance v17, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    iget-object v2, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->merchant_token:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v4, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->end_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v5, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->creator_token:Ljava/util/List;

    iget-object v6, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->selected_payment_token:Ljava/util/List;

    iget-object v7, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->sort_param:Ljava/util/List;

    iget-object v8, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    iget-object v9, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->subunit_merchant_token:Ljava/util/List;

    iget-object v10, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->tz_name:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->event_based:Ljava/lang/Boolean;

    iget-object v12, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->selected_bill_token:Ljava/util/List;

    iget-object v13, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    iget-object v14, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->start_of_day_offset_minutes:Ljava/lang/Integer;

    iget-object v15, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object/from16 v1, v17

    invoke-direct/range {v1 .. v16}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;Ljava/lang/Integer;Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;Lokio/ByteString;)V

    return-object v17
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 310
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->build()Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    move-result-object v0

    return-object v0
.end method

.method public creator_token(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;"
        }
    .end annotation

    .line 382
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 383
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->creator_token:Ljava/util/List;

    return-object p0
.end method

.method public end_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
    .locals 0

    .line 370
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->end_time:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public event_based(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
    .locals 0

    .line 448
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->event_based:Ljava/lang/Boolean;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
    .locals 0

    .line 352
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public request_flags(Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
    .locals 0

    .line 468
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    return-object p0
.end method

.method public selected_bill_token(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;"
        }
    .end annotation

    .line 458
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 459
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->selected_bill_token:Ljava/util/List;

    return-object p0
.end method

.method public selected_payment_token(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;"
        }
    .end annotation

    .line 391
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 392
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->selected_payment_token:Ljava/util/List;

    return-object p0
.end method

.method public sort_param(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v2/reporting/SortParams;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;"
        }
    .end annotation

    .line 400
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 401
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->sort_param:Ljava/util/List;

    return-object p0
.end method

.method public start_of_day_offset_minutes(Ljava/lang/Integer;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
    .locals 0

    .line 476
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->start_of_day_offset_minutes:Ljava/lang/Integer;

    return-object p0
.end method

.method public subunit_merchant_token(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;"
        }
    .end annotation

    .line 426
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 427
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->subunit_merchant_token:Ljava/util/List;

    return-object p0
.end method

.method public time_filter(Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
    .locals 0

    .line 415
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    return-object p0
.end method

.method public tz_name(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
    .locals 0

    .line 437
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->tz_name:Ljava/lang/String;

    return-object p0
.end method
