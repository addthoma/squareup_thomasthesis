.class public final Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TimeWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;",
        "Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public begin_time:Lcom/squareup/protos/common/time/DateTime;

.field public end_time:Lcom/squareup/protos/common/time/DateTime;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public begin_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow$Builder;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;
    .locals 4

    .line 146
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow$Builder;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow$Builder;->end_time:Lcom/squareup/protos/common/time/DateTime;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;-><init>(Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow$Builder;->build()Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    move-result-object v0

    return-object v0
.end method

.method public end_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow$Builder;->end_time:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method
