.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;
.super Lcom/squareup/wire/Message;
.source "GroupingType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$ProtoAdapter_GroupingType;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_GROUP_BY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final DEFAULT_REPORTING_GROUP_CONFIG_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final reporting_group_config_details:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.ReportingGroupConfigDetails#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final reporting_group_config_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$ProtoAdapter_GroupingType;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$ProtoAdapter_GroupingType;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->NONE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->DEFAULT_GROUP_BY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Ljava/lang/String;Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;)V
    .locals 1

    .line 55
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Ljava/lang/String;Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Ljava/lang/String;Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;Lokio/ByteString;)V
    .locals 1

    .line 60
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 61
    invoke-static {p2, p3}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p4

    const/4 v0, 0x1

    if-gt p4, v0, :cond_0

    .line 64
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 65
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->reporting_group_config_id:Ljava/lang/String;

    .line 66
    iput-object p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->reporting_group_config_details:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    return-void

    .line 62
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of reporting_group_config_id, reporting_group_config_details may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 82
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 83
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 85
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->reporting_group_config_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->reporting_group_config_id:Ljava/lang/String;

    .line 86
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->reporting_group_config_details:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->reporting_group_config_details:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    .line 87
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 92
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->reporting_group_config_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->reporting_group_config_details:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 98
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;
    .locals 2

    .line 71
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;-><init>()V

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->reporting_group_config_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;->reporting_group_config_id:Ljava/lang/String;

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->reporting_group_config_details:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;->reporting_group_config_details:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    if-eqz v1, :cond_0

    const-string v1, ", group_by_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->reporting_group_config_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", reporting_group_config_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->reporting_group_config_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->reporting_group_config_details:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    if-eqz v1, :cond_2

    const-string v1, ", reporting_group_config_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->reporting_group_config_details:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GroupingType{"

    .line 109
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
