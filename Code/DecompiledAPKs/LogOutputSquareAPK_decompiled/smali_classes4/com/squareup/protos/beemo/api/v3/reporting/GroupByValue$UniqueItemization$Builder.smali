.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public discount_details:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;",
            ">;"
        }
    .end annotation
.end field

.field public item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

.field public item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

.field public item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

.field public modifier_details:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;",
            ">;"
        }
    .end annotation
.end field

.field public tax_details:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;",
            ">;"
        }
    .end annotation
.end field

.field public unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 5901
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 5902
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->modifier_details:Ljava/util/List;

    .line 5903
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->tax_details:Ljava/util/List;

    .line 5904
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->discount_details:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;
    .locals 10

    .line 5947
    new-instance v9, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    iget-object v4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    iget-object v5, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->modifier_details:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->tax_details:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->discount_details:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 5886
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    move-result-object v0

    return-object v0
.end method

.method public discount_details(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;"
        }
    .end annotation

    .line 5940
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 5941
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->discount_details:Ljava/util/List;

    return-object p0
.end method

.method public item(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;
    .locals 0

    .line 5913
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    return-object p0
.end method

.method public item_category(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;
    .locals 0

    .line 5908
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    return-object p0
.end method

.method public item_variation(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;
    .locals 0

    .line 5918
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    return-object p0
.end method

.method public modifier_details(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;"
        }
    .end annotation

    .line 5928
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 5929
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->modifier_details:Ljava/util/List;

    return-object p0
.end method

.method public tax_details(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;"
        }
    .end annotation

    .line 5934
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 5935
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->tax_details:Ljava/util/List;

    return-object p0
.end method

.method public unit_price(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;
    .locals 0

    .line 5923
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    return-object p0
.end method
