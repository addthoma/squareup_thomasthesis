.class public final Lcom/squareup/protos/jedi/service/StartSessionRequest;
.super Lcom/squareup/wire/Message;
.source "StartSessionRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/jedi/service/StartSessionRequest$ProtoAdapter_StartSessionRequest;,
        Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/jedi/service/StartSessionRequest;",
        "Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/jedi/service/StartSessionRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ANONYMOUS_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_CLIENT:Ljava/lang/String; = ""

.field public static final DEFAULT_COMPONENT_LIBRARY_VERSION:Ljava/lang/String; = ""

.field public static final DEFAULT_COUNTRY_CODE:Lcom/squareup/protos/common/countries/Country;

.field public static final DEFAULT_DEVICE_TYPE:Lcom/squareup/protos/jedi/service/DeviceType;

.field public static final DEFAULT_EMPLOYMENT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_LANGUAGE_CODE:Lcom/squareup/protos/common/languages/Language;

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_PERSON_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_UNIT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final anonymous_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final client:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final component_library_version:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final country_code:Lcom/squareup/protos/common/countries/Country;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.countries.Country#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final device_type:Lcom/squareup/protos/jedi/service/DeviceType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.jedi.service.DeviceType#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final employment_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final language_code:Lcom/squareup/protos/common/languages/Language;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.languages.Language#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final person_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/jedi/service/StartSessionRequest$ProtoAdapter_StartSessionRequest;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/StartSessionRequest$ProtoAdapter_StartSessionRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/protos/common/countries/Country;->US:Lcom/squareup/protos/common/countries/Country;

    sput-object v0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->DEFAULT_COUNTRY_CODE:Lcom/squareup/protos/common/countries/Country;

    .line 34
    sget-object v0, Lcom/squareup/protos/common/languages/Language;->EN:Lcom/squareup/protos/common/languages/Language;

    sput-object v0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->DEFAULT_LANGUAGE_CODE:Lcom/squareup/protos/common/languages/Language;

    .line 44
    sget-object v0, Lcom/squareup/protos/jedi/service/DeviceType;->PHONE:Lcom/squareup/protos/jedi/service/DeviceType;

    sput-object v0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->DEFAULT_DEVICE_TYPE:Lcom/squareup/protos/jedi/service/DeviceType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/countries/Country;Lcom/squareup/protos/common/languages/Language;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/jedi/service/DeviceType;)V
    .locals 12

    .line 109
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/jedi/service/StartSessionRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/countries/Country;Lcom/squareup/protos/common/languages/Language;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/jedi/service/DeviceType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/countries/Country;Lcom/squareup/protos/common/languages/Language;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/jedi/service/DeviceType;Lokio/ByteString;)V
    .locals 1

    .line 116
    sget-object v0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->employment_token:Ljava/lang/String;

    .line 118
    iput-object p2, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->anonymous_token:Ljava/lang/String;

    .line 119
    iput-object p3, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->unit_token:Ljava/lang/String;

    .line 120
    iput-object p4, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->country_code:Lcom/squareup/protos/common/countries/Country;

    .line 121
    iput-object p5, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->language_code:Lcom/squareup/protos/common/languages/Language;

    .line 122
    iput-object p6, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->merchant_token:Ljava/lang/String;

    .line 123
    iput-object p7, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->person_token:Ljava/lang/String;

    .line 124
    iput-object p8, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->client:Ljava/lang/String;

    .line 125
    iput-object p9, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->component_library_version:Ljava/lang/String;

    .line 126
    iput-object p10, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->device_type:Lcom/squareup/protos/jedi/service/DeviceType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 149
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/jedi/service/StartSessionRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 150
    :cond_1
    check-cast p1, Lcom/squareup/protos/jedi/service/StartSessionRequest;

    .line 151
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/StartSessionRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/jedi/service/StartSessionRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->employment_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/StartSessionRequest;->employment_token:Ljava/lang/String;

    .line 152
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->anonymous_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/StartSessionRequest;->anonymous_token:Ljava/lang/String;

    .line 153
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/StartSessionRequest;->unit_token:Ljava/lang/String;

    .line 154
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->country_code:Lcom/squareup/protos/common/countries/Country;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/StartSessionRequest;->country_code:Lcom/squareup/protos/common/countries/Country;

    .line 155
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->language_code:Lcom/squareup/protos/common/languages/Language;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/StartSessionRequest;->language_code:Lcom/squareup/protos/common/languages/Language;

    .line 156
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/StartSessionRequest;->merchant_token:Ljava/lang/String;

    .line 157
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->person_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/StartSessionRequest;->person_token:Ljava/lang/String;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->client:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/StartSessionRequest;->client:Ljava/lang/String;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->component_library_version:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/StartSessionRequest;->component_library_version:Ljava/lang/String;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->device_type:Lcom/squareup/protos/jedi/service/DeviceType;

    iget-object p1, p1, Lcom/squareup/protos/jedi/service/StartSessionRequest;->device_type:Lcom/squareup/protos/jedi/service/DeviceType;

    .line 161
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 166
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 168
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/StartSessionRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 169
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->employment_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->anonymous_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->country_code:Lcom/squareup/protos/common/countries/Country;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/countries/Country;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->language_code:Lcom/squareup/protos/common/languages/Language;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/languages/Language;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->person_token:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->client:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->component_library_version:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->device_type:Lcom/squareup/protos/jedi/service/DeviceType;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/jedi/service/DeviceType;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 179
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;
    .locals 2

    .line 131
    new-instance v0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;-><init>()V

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->employment_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->employment_token:Ljava/lang/String;

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->anonymous_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->anonymous_token:Ljava/lang/String;

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->unit_token:Ljava/lang/String;

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->country_code:Lcom/squareup/protos/common/countries/Country;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->country_code:Lcom/squareup/protos/common/countries/Country;

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->language_code:Lcom/squareup/protos/common/languages/Language;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->language_code:Lcom/squareup/protos/common/languages/Language;

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->merchant_token:Ljava/lang/String;

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->person_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->person_token:Ljava/lang/String;

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->client:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->client:Ljava/lang/String;

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->component_library_version:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->component_library_version:Ljava/lang/String;

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->device_type:Lcom/squareup/protos/jedi/service/DeviceType;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->device_type:Lcom/squareup/protos/jedi/service/DeviceType;

    .line 142
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/StartSessionRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/StartSessionRequest;->newBuilder()Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 187
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->employment_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", employment_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->employment_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->anonymous_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", anonymous_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->anonymous_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->country_code:Lcom/squareup/protos/common/countries/Country;

    if-eqz v1, :cond_3

    const-string v1, ", country_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->country_code:Lcom/squareup/protos/common/countries/Country;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 191
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->language_code:Lcom/squareup/protos/common/languages/Language;

    if-eqz v1, :cond_4

    const-string v1, ", language_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->language_code:Lcom/squareup/protos/common/languages/Language;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 192
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->person_token:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", person_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->person_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->client:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", client="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->client:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->component_library_version:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", component_library_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->component_library_version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->device_type:Lcom/squareup/protos/jedi/service/DeviceType;

    if-eqz v1, :cond_9

    const-string v1, ", device_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest;->device_type:Lcom/squareup/protos/jedi/service/DeviceType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "StartSessionRequest{"

    .line 197
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
