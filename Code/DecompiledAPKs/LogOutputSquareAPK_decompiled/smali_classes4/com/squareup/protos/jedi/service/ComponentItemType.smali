.class public final enum Lcom/squareup/protos/jedi/service/ComponentItemType;
.super Ljava/lang/Enum;
.source "ComponentItemType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/jedi/service/ComponentItemType$ProtoAdapter_ComponentItemType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/jedi/service/ComponentItemType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/jedi/service/ComponentItemType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/jedi/service/ComponentItemType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum TEXT_ITEM:Lcom/squareup/protos/jedi/service/ComponentItemType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 11
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentItemType;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "TEXT_ITEM"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/protos/jedi/service/ComponentItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentItemType;->TEXT_ITEM:Lcom/squareup/protos/jedi/service/ComponentItemType;

    new-array v0, v1, [Lcom/squareup/protos/jedi/service/ComponentItemType;

    .line 10
    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentItemType;->TEXT_ITEM:Lcom/squareup/protos/jedi/service/ComponentItemType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentItemType;->$VALUES:[Lcom/squareup/protos/jedi/service/ComponentItemType;

    .line 13
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentItemType$ProtoAdapter_ComponentItemType;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/ComponentItemType$ProtoAdapter_ComponentItemType;-><init>()V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentItemType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    iput p3, p0, Lcom/squareup/protos/jedi/service/ComponentItemType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/jedi/service/ComponentItemType;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 26
    :cond_0
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentItemType;->TEXT_ITEM:Lcom/squareup/protos/jedi/service/ComponentItemType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/ComponentItemType;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/jedi/service/ComponentItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/jedi/service/ComponentItemType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/jedi/service/ComponentItemType;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/jedi/service/ComponentItemType;->$VALUES:[Lcom/squareup/protos/jedi/service/ComponentItemType;

    invoke-virtual {v0}, [Lcom/squareup/protos/jedi/service/ComponentItemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/jedi/service/ComponentItemType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 33
    iget v0, p0, Lcom/squareup/protos/jedi/service/ComponentItemType;->value:I

    return v0
.end method
