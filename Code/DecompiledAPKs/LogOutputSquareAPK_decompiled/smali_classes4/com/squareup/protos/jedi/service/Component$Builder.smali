.class public final Lcom/squareup/protos/jedi/service/Component$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Component.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/jedi/service/Component;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/jedi/service/Component;",
        "Lcom/squareup/protos/jedi/service/Component$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/jedi/service/ComponentItem;",
            ">;"
        }
    .end annotation
.end field

.field public kind:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public name:Ljava/lang/String;

.field public parameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 126
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 127
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/jedi/service/Component$Builder;->parameters:Ljava/util/Map;

    .line 128
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/jedi/service/Component$Builder;->items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/jedi/service/Component;
    .locals 7

    .line 155
    new-instance v6, Lcom/squareup/protos/jedi/service/Component;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Component$Builder;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    iget-object v2, p0, Lcom/squareup/protos/jedi/service/Component$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/jedi/service/Component$Builder;->parameters:Ljava/util/Map;

    iget-object v4, p0, Lcom/squareup/protos/jedi/service/Component$Builder;->items:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/jedi/service/Component;-><init>(Lcom/squareup/protos/jedi/service/ComponentKind;Ljava/lang/String;Ljava/util/Map;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/Component$Builder;->build()Lcom/squareup/protos/jedi/service/Component;

    move-result-object v0

    return-object v0
.end method

.method public items(Ljava/util/List;)Lcom/squareup/protos/jedi/service/Component$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/jedi/service/ComponentItem;",
            ">;)",
            "Lcom/squareup/protos/jedi/service/Component$Builder;"
        }
    .end annotation

    .line 148
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 149
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/Component$Builder;->items:Ljava/util/List;

    return-object p0
.end method

.method public kind(Lcom/squareup/protos/jedi/service/ComponentKind;)Lcom/squareup/protos/jedi/service/Component$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/Component$Builder;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/Component$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/Component$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public parameters(Ljava/util/Map;)Lcom/squareup/protos/jedi/service/Component$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/jedi/service/Component$Builder;"
        }
    .end annotation

    .line 142
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/Map;)V

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/Component$Builder;->parameters:Ljava/util/Map;

    return-object p0
.end method
