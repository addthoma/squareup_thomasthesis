.class public final enum Lcom/squareup/protos/jedi/service/ComponentKind;
.super Ljava/lang/Enum;
.source "ComponentKind.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/jedi/service/ComponentKind$ProtoAdapter_ComponentKind;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/jedi/service/ComponentKind;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/jedi/service/ComponentKind;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ANSWER:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum BACK:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum BANNER:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum BUTTON:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum BUTTON_BASE:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum CALL_TO_ACTION:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum DATE_PICKER:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum DETAIL_ROW:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum FEEDBACK:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum HEADLINE:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum HERO_HEADLINE:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum ICON:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum INLINE_BANNER:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum INPUT_CONFIRMATION:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum INSTANT_ANSWER:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum LINK:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum MULTI_SELECT_LIST:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum ORDER_DETAIL:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum ORDER_TRACKING_STATUS:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum PARAGRAPH:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum PHONE_SUPPORT_INFO:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum PROGRESS_BAR:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum RESOLUTION:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum ROW:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum SEARCH_BAR:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum SECTION_HEADER:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum SEPARATOR:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum SINGLE_SELECT_LIST:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum TEXT_FIELD:Lcom/squareup/protos/jedi/service/ComponentKind;

.field public static final enum TILE:Lcom/squareup/protos/jedi/service/ComponentKind;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 14
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const/4 v1, 0x4

    const-string v2, "TEXT_FIELD"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->TEXT_FIELD:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 16
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const/4 v2, 0x5

    const/4 v3, 0x1

    const-string v4, "HEADLINE"

    invoke-direct {v0, v4, v3, v2}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->HEADLINE:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 18
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v4, 0x9

    const/4 v5, 0x2

    const-string v6, "LINK"

    invoke-direct {v0, v6, v5, v4}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->LINK:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 20
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v6, 0xa

    const/4 v7, 0x3

    const-string v8, "INPUT_CONFIRMATION"

    invoke-direct {v0, v8, v7, v6}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->INPUT_CONFIRMATION:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 22
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v8, 0xc

    const-string v9, "BACK"

    invoke-direct {v0, v9, v1, v8}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->BACK:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 24
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v9, 0xe

    const-string v10, "BANNER"

    invoke-direct {v0, v10, v2, v9}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->BANNER:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 26
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v10, 0xf

    const/4 v11, 0x6

    const-string v12, "INSTANT_ANSWER"

    invoke-direct {v0, v12, v11, v10}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->INSTANT_ANSWER:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 28
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v12, 0x13

    const/4 v13, 0x7

    const-string v14, "PARAGRAPH"

    invoke-direct {v0, v14, v13, v12}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->PARAGRAPH:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 33
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v14, 0x15

    const/16 v15, 0x8

    const-string v2, "PROGRESS_BAR"

    invoke-direct {v0, v2, v15, v14}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->PROGRESS_BAR:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 35
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v2, "SEARCH_BAR"

    const/16 v1, 0x16

    invoke-direct {v0, v2, v4, v1}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->SEARCH_BAR:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 40
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "ROW"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v6, v2}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->ROW:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 45
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "SINGLE_SELECT_LIST"

    const/16 v2, 0xb

    const/16 v6, 0x19

    invoke-direct {v0, v1, v2, v6}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->SINGLE_SELECT_LIST:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 50
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "MULTI_SELECT_LIST"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v8, v2}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->MULTI_SELECT_LIST:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 55
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "DATE_PICKER"

    const/16 v2, 0xd

    const/16 v6, 0x1b

    invoke-direct {v0, v1, v2, v6}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->DATE_PICKER:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 57
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "SECTION_HEADER"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v9, v2}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->SECTION_HEADER:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 59
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "BUTTON_BASE"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v10, v2}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->BUTTON_BASE:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 61
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "ICON"

    const/16 v2, 0x10

    const/16 v6, 0x1e

    invoke-direct {v0, v1, v2, v6}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->ICON:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 67
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "BUTTON"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->BUTTON:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 72
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "TILE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2, v5}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->TILE:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 77
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "ANSWER"

    invoke-direct {v0, v1, v12, v7}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->ANSWER:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 82
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "HERO_HEADLINE"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2, v11}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->HERO_HEADLINE:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 87
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "FEEDBACK"

    invoke-direct {v0, v1, v14, v13}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->FEEDBACK:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 92
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "CALL_TO_ACTION"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2, v15}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->CALL_TO_ACTION:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 97
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "RESOLUTION"

    const/16 v2, 0x17

    const/16 v6, 0xb

    invoke-direct {v0, v1, v2, v6}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->RESOLUTION:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 102
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "PHONE_SUPPORT_INFO"

    const/16 v2, 0x18

    const/16 v6, 0xd

    invoke-direct {v0, v1, v2, v6}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->PHONE_SUPPORT_INFO:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 107
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "ORDER_DETAIL"

    const/16 v2, 0x19

    const/16 v6, 0x10

    invoke-direct {v0, v1, v2, v6}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->ORDER_DETAIL:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 112
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "ORDER_TRACKING_STATUS"

    const/16 v2, 0x1a

    const/16 v6, 0x11

    invoke-direct {v0, v1, v2, v6}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->ORDER_TRACKING_STATUS:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 117
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "DETAIL_ROW"

    const/16 v2, 0x1b

    const/16 v6, 0x12

    invoke-direct {v0, v1, v2, v6}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->DETAIL_ROW:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 122
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "SEPARATOR"

    const/16 v2, 0x1c

    const/16 v6, 0x14

    invoke-direct {v0, v1, v2, v6}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->SEPARATOR:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 127
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    const-string v1, "INLINE_BANNER"

    const/16 v2, 0x1d

    const/16 v6, 0x17

    invoke-direct {v0, v1, v2, v6}, Lcom/squareup/protos/jedi/service/ComponentKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->INLINE_BANNER:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v0, 0x1e

    new-array v0, v0, [Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 10
    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->TEXT_FIELD:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->HEADLINE:Lcom/squareup/protos/jedi/service/ComponentKind;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->LINK:Lcom/squareup/protos/jedi/service/ComponentKind;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->INPUT_CONFIRMATION:Lcom/squareup/protos/jedi/service/ComponentKind;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->BACK:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->BANNER:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->INSTANT_ANSWER:Lcom/squareup/protos/jedi/service/ComponentKind;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->PARAGRAPH:Lcom/squareup/protos/jedi/service/ComponentKind;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->PROGRESS_BAR:Lcom/squareup/protos/jedi/service/ComponentKind;

    aput-object v1, v0, v15

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->SEARCH_BAR:Lcom/squareup/protos/jedi/service/ComponentKind;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->ROW:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->SINGLE_SELECT_LIST:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->MULTI_SELECT_LIST:Lcom/squareup/protos/jedi/service/ComponentKind;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->DATE_PICKER:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->SECTION_HEADER:Lcom/squareup/protos/jedi/service/ComponentKind;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->BUTTON_BASE:Lcom/squareup/protos/jedi/service/ComponentKind;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->ICON:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->BUTTON:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->TILE:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->ANSWER:Lcom/squareup/protos/jedi/service/ComponentKind;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->HERO_HEADLINE:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->FEEDBACK:Lcom/squareup/protos/jedi/service/ComponentKind;

    aput-object v1, v0, v14

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->CALL_TO_ACTION:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->RESOLUTION:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->PHONE_SUPPORT_INFO:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->ORDER_DETAIL:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->ORDER_TRACKING_STATUS:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->DETAIL_ROW:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->SEPARATOR:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->INLINE_BANNER:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->$VALUES:[Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 129
    new-instance v0, Lcom/squareup/protos/jedi/service/ComponentKind$ProtoAdapter_ComponentKind;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/ComponentKind$ProtoAdapter_ComponentKind;-><init>()V

    sput-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 133
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 134
    iput p3, p0, Lcom/squareup/protos/jedi/service/ComponentKind;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/jedi/service/ComponentKind;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 158
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->ICON:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 157
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->BUTTON_BASE:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 156
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->SECTION_HEADER:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 155
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->DATE_PICKER:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 154
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->MULTI_SELECT_LIST:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 153
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->SINGLE_SELECT_LIST:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 152
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->ROW:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 171
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->INLINE_BANNER:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 151
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->SEARCH_BAR:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 150
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->PROGRESS_BAR:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 170
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->SEPARATOR:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 149
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->PARAGRAPH:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 169
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->DETAIL_ROW:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 168
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->ORDER_TRACKING_STATUS:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 167
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->ORDER_DETAIL:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 148
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->INSTANT_ANSWER:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 147
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->BANNER:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 166
    :pswitch_11
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->PHONE_SUPPORT_INFO:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 146
    :pswitch_12
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->BACK:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 165
    :pswitch_13
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->RESOLUTION:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 145
    :pswitch_14
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->INPUT_CONFIRMATION:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 144
    :pswitch_15
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->LINK:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 164
    :pswitch_16
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->CALL_TO_ACTION:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 163
    :pswitch_17
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->FEEDBACK:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 162
    :pswitch_18
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->HERO_HEADLINE:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 143
    :pswitch_19
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->HEADLINE:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 142
    :pswitch_1a
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->TEXT_FIELD:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 161
    :pswitch_1b
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->ANSWER:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 160
    :pswitch_1c
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->TILE:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    .line 159
    :pswitch_1d
    sget-object p0, Lcom/squareup/protos/jedi/service/ComponentKind;->BUTTON:Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/ComponentKind;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/jedi/service/ComponentKind;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->$VALUES:[Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {v0}, [Lcom/squareup/protos/jedi/service/ComponentKind;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/jedi/service/ComponentKind;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 178
    iget v0, p0, Lcom/squareup/protos/jedi/service/ComponentKind;->value:I

    return v0
.end method
