.class final Lcom/squareup/progress/ProgressBinder$bindProgressDialogs$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ProgressBinder.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/progress/ProgressBinder;->bindProgressDialogs()Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lio/reactivex/Observable<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/progress/Progress$ScreenData;",
        "Lcom/squareup/progress/Progress$ProgressComplete;",
        ">;>;",
        "Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00060\u0001R\u00020\u00022\u0018\u0010\u0003\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;",
        "Lcom/squareup/progress/ProgressBinder;",
        "screen",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/progress/Progress$ScreenData;",
        "Lcom/squareup/progress/Progress$ProgressComplete;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/progress/ProgressBinder;


# direct methods
.method constructor <init>(Lcom/squareup/progress/ProgressBinder;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/progress/ProgressBinder$bindProgressDialogs$1;->this$0:Lcom/squareup/progress/ProgressBinder;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lio/reactivex/Observable;)Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/progress/Progress$ScreenData;",
            "Lcom/squareup/progress/Progress$ProgressComplete;",
            ">;>;)",
            "Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    new-instance v0, Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;

    iget-object v1, p0, Lcom/squareup/progress/ProgressBinder$bindProgressDialogs$1;->this$0:Lcom/squareup/progress/ProgressBinder;

    invoke-direct {v0, v1, p1}, Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;-><init>(Lcom/squareup/progress/ProgressBinder;Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lio/reactivex/Observable;

    invoke-virtual {p0, p1}, Lcom/squareup/progress/ProgressBinder$bindProgressDialogs$1;->invoke(Lio/reactivex/Observable;)Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;

    move-result-object p1

    return-object p1
.end method
