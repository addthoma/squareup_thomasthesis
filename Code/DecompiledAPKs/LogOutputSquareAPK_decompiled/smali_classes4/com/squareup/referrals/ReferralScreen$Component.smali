.class public interface abstract Lcom/squareup/referrals/ReferralScreen$Component;
.super Ljava/lang/Object;
.source "ReferralScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/referrals/ReferralScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract referralCoordinator()Lcom/squareup/referrals/ReferralCoordinator;
.end method

.method public abstract runner()Lcom/squareup/referrals/ReferralRunner;
.end method
