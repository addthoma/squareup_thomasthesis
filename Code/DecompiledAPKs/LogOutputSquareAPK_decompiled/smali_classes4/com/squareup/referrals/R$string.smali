.class public final Lcom/squareup/referrals/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/referrals/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final loading:I = 0x7f120ee7

.field public static final referral_balance_title:I = 0x7f121617

.field public static final referral_earn_rewards:I = 0x7f121618

.field public static final referral_email_subject:I = 0x7f121619

.field public static final referral_email_text:I = 0x7f12161a

.field public static final referral_get_free_processing:I = 0x7f12161b

.field public static final referral_later:I = 0x7f12161c

.field public static final referral_message:I = 0x7f12161d

.field public static final referral_message_rewards:I = 0x7f12161e

.field public static final referral_process_failure:I = 0x7f12161f

.field public static final referral_send_with:I = 0x7f121620

.field public static final referral_share_link:I = 0x7f121621

.field public static final referral_title:I = 0x7f121622

.field public static final referral_title_rewards:I = 0x7f121623

.field public static final referral_url_template:I = 0x7f121624

.field public static final referral_use_by:I = 0x7f121625


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
