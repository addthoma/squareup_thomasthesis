.class public final Lcom/squareup/referrals/ReferralRunner;
.super Ljava/lang/Object;
.source "ReferralRunner.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/referrals/ReferralRunner$ReferralData;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final dateFormat:Ljava/text/DateFormat;

.field private final httpAndHttps:Ljava/util/regex/Pattern;

.field private final locale:Ljava/util/Locale;

.field private final longMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final referralCall:Lio/reactivex/observables/ConnectableObservable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/observables/ConnectableObservable<",
            "Lcom/squareup/referrals/ReferralRunner$ReferralData;",
            ">;"
        }
    .end annotation
.end field

.field private final referralListener:Lcom/squareup/referrals/ReferralListener;

.field private final res:Lcom/squareup/util/Res;

.field private final shortMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/server/referral/ReferralService;Ljava/text/DateFormat;Lcom/squareup/analytics/Analytics;Ljava/util/Locale;Lio/reactivex/Scheduler;Lcom/squareup/referrals/ReferralListener;Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;)V
    .locals 1
    .param p8    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/server/referral/ReferralService;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/analytics/Analytics;",
            "Ljava/util/Locale;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/referrals/ReferralListener;",
            "Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "^https?://"

    .line 47
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/referrals/ReferralRunner;->httpAndHttps:Ljava/util/regex/Pattern;

    .line 56
    iput-object p1, p0, Lcom/squareup/referrals/ReferralRunner;->res:Lcom/squareup/util/Res;

    .line 57
    iput-object p2, p0, Lcom/squareup/referrals/ReferralRunner;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    .line 58
    iput-object p3, p0, Lcom/squareup/referrals/ReferralRunner;->longMoneyFormatter:Lcom/squareup/text/Formatter;

    .line 59
    iput-object p5, p0, Lcom/squareup/referrals/ReferralRunner;->dateFormat:Ljava/text/DateFormat;

    .line 60
    iput-object p6, p0, Lcom/squareup/referrals/ReferralRunner;->analytics:Lcom/squareup/analytics/Analytics;

    .line 61
    iput-object p7, p0, Lcom/squareup/referrals/ReferralRunner;->locale:Ljava/util/Locale;

    .line 62
    iput-object p9, p0, Lcom/squareup/referrals/ReferralRunner;->referralListener:Lcom/squareup/referrals/ReferralListener;

    .line 64
    new-instance p1, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest$Builder;-><init>()V

    .line 67
    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest$Builder;->build()Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;

    move-result-object p1

    invoke-interface {p4, p1}, Lcom/squareup/server/referral/ReferralService;->referral(Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 68
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 69
    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    .line 70
    invoke-virtual {p10}, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;->showMultipleRewardsCopyBehavior()Lio/reactivex/Observable;

    move-result-object p2

    new-instance p3, Lcom/squareup/referrals/-$$Lambda$ReferralRunner$VK3_ecyGPmJd44aiq57KddEdTAQ;

    invoke-direct {p3, p0}, Lcom/squareup/referrals/-$$Lambda$ReferralRunner$VK3_ecyGPmJd44aiq57KddEdTAQ;-><init>(Lcom/squareup/referrals/ReferralRunner;)V

    .line 65
    invoke-static {p1, p2, p3}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    .line 73
    invoke-virtual {p1, p8}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/referrals/-$$Lambda$ReferralRunner$OIkG-IKwmaVlRA_fS0RfbeVtuQ0;

    invoke-direct {p2, p9}, Lcom/squareup/referrals/-$$Lambda$ReferralRunner$OIkG-IKwmaVlRA_fS0RfbeVtuQ0;-><init>(Lcom/squareup/referrals/ReferralListener;)V

    .line 74
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    const/4 p2, 0x1

    .line 79
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/referrals/ReferralRunner;->referralCall:Lio/reactivex/observables/ConnectableObservable;

    return-void
.end method

.method private generateReferralUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/referrals/R$string;->referral_url_template:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "domain"

    .line 175
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v0, "slug"

    .line 176
    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 177
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 178
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic lambda$VK3_ecyGPmJd44aiq57KddEdTAQ(Lcom/squareup/referrals/ReferralRunner;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Z)Lcom/squareup/referrals/ReferralRunner$ReferralData;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/squareup/referrals/ReferralRunner;->toReferralData(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Z)Lcom/squareup/referrals/ReferralRunner$ReferralData;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$new$0(Lcom/squareup/referrals/ReferralListener;Lcom/squareup/referrals/ReferralRunner$ReferralData;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 75
    iget-boolean p1, p1, Lcom/squareup/referrals/ReferralRunner$ReferralData;->hasError:Z

    if-eqz p1, :cond_0

    .line 76
    invoke-interface {p0}, Lcom/squareup/referrals/ReferralListener;->onLoadFailure()V

    :cond_0
    return-void
.end method

.method private toReferralData(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Z)Lcom/squareup/referrals/ReferralRunner$ReferralData;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;",
            ">;Z)",
            "Lcom/squareup/referrals/ReferralRunner$ReferralData;"
        }
    .end annotation

    .line 94
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;

    if-nez p1, :cond_0

    .line 97
    invoke-static {}, Lcom/squareup/referrals/ReferralRunner$ReferralData;->error()Lcom/squareup/referrals/ReferralRunner$ReferralData;

    move-result-object p1

    return-object p1

    .line 100
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;->free_processing_balance:Lcom/squareup/protos/client/onboard/FreeProcessingBalance;

    .line 102
    new-instance v1, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;-><init>(Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;)V

    if-eqz v0, :cond_1

    .line 103
    iget-object v2, v0, Lcom/squareup/protos/client/onboard/FreeProcessingBalance;->free_processing:Lcom/squareup/protos/common/Money;

    invoke-static {v2}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 104
    iget-object v2, p0, Lcom/squareup/referrals/ReferralRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/referrals/R$string;->referral_use_by:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/referrals/ReferralRunner;->dateFormat:Ljava/text/DateFormat;

    iget-object v4, v0, Lcom/squareup/protos/client/onboard/FreeProcessingBalance;->expires_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v5, p0, Lcom/squareup/referrals/ReferralRunner;->locale:Ljava/util/Locale;

    .line 105
    invoke-static {v4, v5}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "expired_at"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 106
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    .line 107
    invoke-virtual {v1, v2}, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->setExpiresAt(Ljava/lang/CharSequence;)Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/referrals/ReferralRunner;->longMoneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/FreeProcessingBalance;->free_processing:Lcom/squareup/protos/common/Money;

    .line 108
    invoke-interface {v3, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->setBalance(Ljava/lang/CharSequence;)Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;

    .line 110
    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;->current_signup_token:Lcom/squareup/protos/client/onboard/SignupToken;

    .line 111
    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p1, Lcom/squareup/protos/client/onboard/SignupToken;->free_processing_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v2, "referral_money"

    if-eqz p2, :cond_2

    .line 114
    iget-object p2, p0, Lcom/squareup/referrals/ReferralRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/referrals/R$string;->referral_title_rewards:I

    .line 115
    invoke-interface {p2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->setFreeProcessingTitle(Ljava/lang/CharSequence;)Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;

    move-result-object p2

    iget-object v3, p0, Lcom/squareup/referrals/ReferralRunner;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/referrals/R$string;->referral_message_rewards:I

    .line 116
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 117
    invoke-virtual {v3, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 116
    invoke-virtual {p2, v0}, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->setFreeProcessingNote(Ljava/lang/CharSequence;)Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;

    goto :goto_0

    .line 120
    :cond_2
    iget-object p2, p0, Lcom/squareup/referrals/ReferralRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/referrals/R$string;->referral_title:I

    .line 121
    invoke-interface {p2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->setFreeProcessingTitle(Ljava/lang/CharSequence;)Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;

    move-result-object p2

    iget-object v3, p0, Lcom/squareup/referrals/ReferralRunner;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/referrals/R$string;->referral_message:I

    .line 122
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 123
    invoke-virtual {v3, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v2, p1, Lcom/squareup/protos/client/onboard/SignupToken;->free_processing_days:Ljava/lang/Integer;

    .line 124
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "expiration_days"

    invoke-virtual {v0, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 122
    invoke-virtual {p2, v0}, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->setFreeProcessingNote(Ljava/lang/CharSequence;)Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;

    .line 128
    :goto_0
    iget-object p1, p1, Lcom/squareup/protos/client/onboard/SignupToken;->referral_url:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/referrals/ReferralRunner;->prettyUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->setPrettyUrl(Ljava/lang/String;)Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;

    move-result-object p1

    .line 129
    invoke-virtual {p1}, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->build()Lcom/squareup/referrals/ReferralRunner$ReferralData;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public launchReferralIntent(Landroid/app/Activity;Lcom/squareup/referrals/ReferralRunner$ReferralData;)V
    .locals 7

    .line 143
    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->REFERRAL_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 144
    invoke-static {p2}, Lcom/squareup/referrals/ReferralRunner$ReferralData;->access$000(Lcom/squareup/referrals/ReferralRunner$ReferralData;)Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;->current_signup_token:Lcom/squareup/protos/client/onboard/SignupToken;

    .line 145
    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p2, Lcom/squareup/protos/client/onboard/SignupToken;->free_processing_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 146
    iget-object v1, p0, Lcom/squareup/referrals/ReferralRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/referrals/R$string;->referral_email_subject:I

    .line 147
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "referral_money"

    .line 148
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 149
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 150
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 151
    iget-object v3, p0, Lcom/squareup/referrals/ReferralRunner;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/referrals/R$string;->referral_send_with:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 153
    iget-object v4, p2, Lcom/squareup/protos/client/onboard/SignupToken;->referral_url:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    .line 154
    iget-object v5, p0, Lcom/squareup/referrals/ReferralRunner;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/referrals/R$string;->referral_email_text:I

    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 155
    invoke-virtual {v5, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object p2, p2, Lcom/squareup/protos/client/onboard/SignupToken;->slug:Ljava/lang/String;

    .line 156
    invoke-direct {p0, v4, p2}, Lcom/squareup/referrals/ReferralRunner;->generateReferralUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string v2, "referral_url"

    invoke-virtual {v0, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 157
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    .line 159
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.intent.action.SEND"

    .line 160
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "text/plain"

    .line 161
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "android.intent.extra.TEXT"

    .line 162
    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p2, "android.intent.extra.SUBJECT"

    .line 163
    invoke-virtual {v0, p2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 164
    invoke-static {v0, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner;->referralCall:Lio/reactivex/observables/ConnectableObservable;

    invoke-virtual {v0}, Lio/reactivex/observables/ConnectableObservable;->connect()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method onUserDismissed()V
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner;->referralListener:Lcom/squareup/referrals/ReferralListener;

    invoke-interface {v0}, Lcom/squareup/referrals/ReferralListener;->onUserDismissed()V

    return-void
.end method

.method prettyUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner;->httpAndHttps:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public referralViewData()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/referrals/ReferralRunner$ReferralData;",
            ">;"
        }
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner;->referralCall:Lio/reactivex/observables/ConnectableObservable;

    return-object v0
.end method
