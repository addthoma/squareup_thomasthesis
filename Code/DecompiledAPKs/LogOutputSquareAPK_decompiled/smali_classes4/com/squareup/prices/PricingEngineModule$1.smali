.class final Lcom/squareup/prices/PricingEngineModule$1;
.super Ljava/lang/Object;
.source "PricingEngineModule.java"

# interfaces
.implements Lcom/squareup/shared/catalog/logging/Clock;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/prices/PricingEngineModule;->providePricingEngine(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;)Lcom/squareup/shared/pricing/engine/PricingEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$clock:Lcom/squareup/util/Clock;


# direct methods
.method constructor <init>(Lcom/squareup/util/Clock;)V
    .locals 0

    .line 19
    iput-object p1, p0, Lcom/squareup/prices/PricingEngineModule$1;->val$clock:Lcom/squareup/util/Clock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getElapsedRealtime()J
    .locals 2

    .line 21
    iget-object v0, p0, Lcom/squareup/prices/PricingEngineModule$1;->val$clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getUptimeMillis()J
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/prices/PricingEngineModule$1;->val$clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide v0

    return-wide v0
.end method
