.class abstract enum Lcom/squareup/pushmessages/PushMessageOp;
.super Ljava/lang/Enum;
.source "PushMessageOp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/pushmessages/PushMessageOp$ApsOldFormat;,
        Lcom/squareup/pushmessages/PushMessageOp$Alert;,
        Lcom/squareup/pushmessages/PushMessageOp$Aps;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/pushmessages/PushMessageOp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/pushmessages/PushMessageOp;

.field public static final enum CHECKOUT_CREATED:Lcom/squareup/pushmessages/PushMessageOp;

.field public static final enum CLIENT_LEDGER_UPLOAD:Lcom/squareup/pushmessages/PushMessageOp;

.field private static final EXTRA_APS:Ljava/lang/String; = "aps"

.field private static final EXTRA_DC:Ljava/lang/String; = "dc"

.field private static final EXTRA_URL:Ljava/lang/String; = "url"

.field public static final enum INVENTORY_AVAILABILITY:Lcom/squareup/pushmessages/PushMessageOp;

.field private static final LEDGER_END_TIME_STAMP:Ljava/lang/String; = "client-ledger-end-time-stamp"

.field private static final LEDGER_START_TIME_STAMP:Ljava/lang/String; = "client-ledger-start-time-stamp"

.field public static final enum ORDERS_UPDATED:Lcom/squareup/pushmessages/PushMessageOp;

.field public static final enum PUSH_NOTIFICATION:Lcom/squareup/pushmessages/PushMessageOp;

.field public static final enum PUSH_NOTIFICATION_WITH_LINK:Lcom/squareup/pushmessages/PushMessageOp;

.field public static final enum SUPPORT_MESSAGING_NOTIFICATION:Lcom/squareup/pushmessages/PushMessageOp;

.field public static final enum SYNC_APPOINTMENTS:Lcom/squareup/pushmessages/PushMessageOp;

.field public static final enum SYNC_ESTIMATE_DEFAULTS:Lcom/squareup/pushmessages/PushMessageOp;

.field public static final enum SYNC_INVOICES_METRICS:Lcom/squareup/pushmessages/PushMessageOp;

.field public static final enum SYNC_INVOICES_UNIT_SETTINGS:Lcom/squareup/pushmessages/PushMessageOp;

.field public static final enum SYNC_ITEMS:Lcom/squareup/pushmessages/PushMessageOp;

.field public static final enum SYNC_MESSAGES:Lcom/squareup/pushmessages/PushMessageOp;

.field public static final enum SYNC_TICKETS:Lcom/squareup/pushmessages/PushMessageOp;


# instance fields
.field final op:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 31
    new-instance v0, Lcom/squareup/pushmessages/PushMessageOp$1;

    const/4 v1, 0x0

    const-string v2, "PUSH_NOTIFICATION"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/pushmessages/PushMessageOp$1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pushmessages/PushMessageOp;->PUSH_NOTIFICATION:Lcom/squareup/pushmessages/PushMessageOp;

    .line 39
    new-instance v0, Lcom/squareup/pushmessages/PushMessageOp$2;

    const/4 v2, 0x1

    const-string v3, "PUSH_NOTIFICATION_WITH_LINK"

    const-string v4, "deep-link"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/pushmessages/PushMessageOp$2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pushmessages/PushMessageOp;->PUSH_NOTIFICATION_WITH_LINK:Lcom/squareup/pushmessages/PushMessageOp;

    .line 56
    new-instance v0, Lcom/squareup/pushmessages/PushMessageOp$3;

    const/4 v3, 0x2

    const-string v4, "SUPPORT_MESSAGING_NOTIFICATION"

    const-string v5, "support-messaging"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/pushmessages/PushMessageOp$3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pushmessages/PushMessageOp;->SUPPORT_MESSAGING_NOTIFICATION:Lcom/squareup/pushmessages/PushMessageOp;

    .line 63
    new-instance v0, Lcom/squareup/pushmessages/PushMessageOp$4;

    const/4 v4, 0x3

    const-string v5, "SYNC_APPOINTMENTS"

    const-string v6, "sync-appointments"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/pushmessages/PushMessageOp$4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pushmessages/PushMessageOp;->SYNC_APPOINTMENTS:Lcom/squareup/pushmessages/PushMessageOp;

    .line 69
    new-instance v0, Lcom/squareup/pushmessages/PushMessageOp$5;

    const/4 v5, 0x4

    const-string v6, "SYNC_ITEMS"

    const-string v7, "sync-items"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/pushmessages/PushMessageOp$5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pushmessages/PushMessageOp;->SYNC_ITEMS:Lcom/squareup/pushmessages/PushMessageOp;

    .line 75
    new-instance v0, Lcom/squareup/pushmessages/PushMessageOp$6;

    const/4 v6, 0x5

    const-string v7, "SYNC_TICKETS"

    const-string v8, "ot-update"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/pushmessages/PushMessageOp$6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pushmessages/PushMessageOp;->SYNC_TICKETS:Lcom/squareup/pushmessages/PushMessageOp;

    .line 81
    new-instance v0, Lcom/squareup/pushmessages/PushMessageOp$7;

    const/4 v7, 0x6

    const-string v8, "SYNC_INVOICES_UNIT_SETTINGS"

    const-string v9, "invoices_unit_settings_updated"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/pushmessages/PushMessageOp$7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pushmessages/PushMessageOp;->SYNC_INVOICES_UNIT_SETTINGS:Lcom/squareup/pushmessages/PushMessageOp;

    .line 87
    new-instance v0, Lcom/squareup/pushmessages/PushMessageOp$8;

    const/4 v8, 0x7

    const-string v9, "SYNC_INVOICES_METRICS"

    const-string v10, "invoices_metrics_updated"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/pushmessages/PushMessageOp$8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pushmessages/PushMessageOp;->SYNC_INVOICES_METRICS:Lcom/squareup/pushmessages/PushMessageOp;

    .line 93
    new-instance v0, Lcom/squareup/pushmessages/PushMessageOp$9;

    const/16 v9, 0x8

    const-string v10, "SYNC_ESTIMATE_DEFAULTS"

    const-string v11, "invoices_estimate_defaults_updated"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/pushmessages/PushMessageOp$9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pushmessages/PushMessageOp;->SYNC_ESTIMATE_DEFAULTS:Lcom/squareup/pushmessages/PushMessageOp;

    .line 99
    new-instance v0, Lcom/squareup/pushmessages/PushMessageOp$10;

    const/16 v10, 0x9

    const-string v11, "ORDERS_UPDATED"

    const-string v12, "order-updated"

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/pushmessages/PushMessageOp$10;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pushmessages/PushMessageOp;->ORDERS_UPDATED:Lcom/squareup/pushmessages/PushMessageOp;

    .line 105
    new-instance v0, Lcom/squareup/pushmessages/PushMessageOp$11;

    const/16 v11, 0xa

    const-string v12, "CHECKOUT_CREATED"

    const-string v13, "checkout-created"

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/pushmessages/PushMessageOp$11;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pushmessages/PushMessageOp;->CHECKOUT_CREATED:Lcom/squareup/pushmessages/PushMessageOp;

    .line 111
    new-instance v0, Lcom/squareup/pushmessages/PushMessageOp$12;

    const/16 v12, 0xb

    const-string v13, "INVENTORY_AVAILABILITY"

    const-string v14, "sync-sales-limits"

    invoke-direct {v0, v13, v12, v14}, Lcom/squareup/pushmessages/PushMessageOp$12;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pushmessages/PushMessageOp;->INVENTORY_AVAILABILITY:Lcom/squareup/pushmessages/PushMessageOp;

    .line 121
    new-instance v0, Lcom/squareup/pushmessages/PushMessageOp$13;

    const/16 v13, 0xc

    const-string v14, "SYNC_MESSAGES"

    const-string v15, "sync-messages"

    invoke-direct {v0, v14, v13, v15}, Lcom/squareup/pushmessages/PushMessageOp$13;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pushmessages/PushMessageOp;->SYNC_MESSAGES:Lcom/squareup/pushmessages/PushMessageOp;

    .line 127
    new-instance v0, Lcom/squareup/pushmessages/PushMessageOp$14;

    const/16 v14, 0xd

    const-string v15, "CLIENT_LEDGER_UPLOAD"

    const-string v13, "client-upload-ledger"

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/pushmessages/PushMessageOp$14;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pushmessages/PushMessageOp;->CLIENT_LEDGER_UPLOAD:Lcom/squareup/pushmessages/PushMessageOp;

    const/16 v0, 0xe

    new-array v0, v0, [Lcom/squareup/pushmessages/PushMessageOp;

    .line 28
    sget-object v13, Lcom/squareup/pushmessages/PushMessageOp;->PUSH_NOTIFICATION:Lcom/squareup/pushmessages/PushMessageOp;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/pushmessages/PushMessageOp;->PUSH_NOTIFICATION_WITH_LINK:Lcom/squareup/pushmessages/PushMessageOp;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/pushmessages/PushMessageOp;->SUPPORT_MESSAGING_NOTIFICATION:Lcom/squareup/pushmessages/PushMessageOp;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/pushmessages/PushMessageOp;->SYNC_APPOINTMENTS:Lcom/squareup/pushmessages/PushMessageOp;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/pushmessages/PushMessageOp;->SYNC_ITEMS:Lcom/squareup/pushmessages/PushMessageOp;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/pushmessages/PushMessageOp;->SYNC_TICKETS:Lcom/squareup/pushmessages/PushMessageOp;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/pushmessages/PushMessageOp;->SYNC_INVOICES_UNIT_SETTINGS:Lcom/squareup/pushmessages/PushMessageOp;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/pushmessages/PushMessageOp;->SYNC_INVOICES_METRICS:Lcom/squareup/pushmessages/PushMessageOp;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/pushmessages/PushMessageOp;->SYNC_ESTIMATE_DEFAULTS:Lcom/squareup/pushmessages/PushMessageOp;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/pushmessages/PushMessageOp;->ORDERS_UPDATED:Lcom/squareup/pushmessages/PushMessageOp;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/pushmessages/PushMessageOp;->CHECKOUT_CREATED:Lcom/squareup/pushmessages/PushMessageOp;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/pushmessages/PushMessageOp;->INVENTORY_AVAILABILITY:Lcom/squareup/pushmessages/PushMessageOp;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/pushmessages/PushMessageOp;->SYNC_MESSAGES:Lcom/squareup/pushmessages/PushMessageOp;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/pushmessages/PushMessageOp;->CLIENT_LEDGER_UPLOAD:Lcom/squareup/pushmessages/PushMessageOp;

    aput-object v1, v0, v14

    sput-object v0, Lcom/squareup/pushmessages/PushMessageOp;->$VALUES:[Lcom/squareup/pushmessages/PushMessageOp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 143
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 144
    iput-object p3, p0, Lcom/squareup/pushmessages/PushMessageOp;->op:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/pushmessages/PushMessageOp$1;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/pushmessages/PushMessageOp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Ljava/util/Map;)Lcom/squareup/pushmessages/PushMessageOp$Alert;
    .locals 0

    .line 28
    invoke-static {p0}, Lcom/squareup/pushmessages/PushMessageOp;->obtainAlert(Ljava/util/Map;)Lcom/squareup/pushmessages/PushMessageOp$Alert;

    move-result-object p0

    return-object p0
.end method

.method private static findOp(Ljava/lang/String;)Lcom/squareup/pushmessages/PushMessageOp;
    .locals 5

    .line 154
    invoke-static {}, Lcom/squareup/pushmessages/PushMessageOp;->values()[Lcom/squareup/pushmessages/PushMessageOp;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 155
    iget-object v4, v3, Lcom/squareup/pushmessages/PushMessageOp;->op:Ljava/lang/String;

    invoke-static {v4, p0}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method static fromParams(Ljava/util/Map;)Lcom/squareup/pushmessages/PushMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/pushmessages/PushMessage;"
        }
    .end annotation

    .line 148
    invoke-static {p0}, Lcom/squareup/pushmessages/PushMessageOps;->getOpKey(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 149
    invoke-static {v0}, Lcom/squareup/pushmessages/PushMessageOp;->findOp(Ljava/lang/String;)Lcom/squareup/pushmessages/PushMessageOp;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 150
    :cond_0
    invoke-virtual {v0, p0}, Lcom/squareup/pushmessages/PushMessageOp;->createMessage(Ljava/util/Map;)Lcom/squareup/pushmessages/PushMessage;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private static obtainAlert(Ljava/util/Map;)Lcom/squareup/pushmessages/PushMessageOp$Alert;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/pushmessages/PushMessageOp$Alert;"
        }
    .end annotation

    const-string v0, "aps"

    .line 163
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 167
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/squareup/gson/GsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object v1

    const-class v2, Lcom/squareup/pushmessages/PushMessageOp$Aps;

    invoke-virtual {v1, p0, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/pushmessages/PushMessageOp$Aps;

    .line 168
    iget-object p0, v1, Lcom/squareup/pushmessages/PushMessageOp$Aps;->alert:Lcom/squareup/pushmessages/PushMessageOp$Alert;
    :try_end_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    .line 170
    :catch_0
    invoke-static {}, Lcom/squareup/gson/GsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object v1

    const-class v2, Lcom/squareup/pushmessages/PushMessageOp$ApsOldFormat;

    invoke-virtual {v1, p0, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/pushmessages/PushMessageOp$ApsOldFormat;

    .line 171
    new-instance v1, Lcom/squareup/pushmessages/PushMessageOp$Alert;

    invoke-direct {v1, v0}, Lcom/squareup/pushmessages/PushMessageOp$Alert;-><init>(Lcom/squareup/pushmessages/PushMessageOp$1;)V

    .line 172
    iget-object p0, p0, Lcom/squareup/pushmessages/PushMessageOp$ApsOldFormat;->alert:Ljava/lang/String;

    iput-object p0, v1, Lcom/squareup/pushmessages/PushMessageOp$Alert;->title:Ljava/lang/String;

    return-object v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/pushmessages/PushMessageOp;
    .locals 1

    .line 28
    const-class v0, Lcom/squareup/pushmessages/PushMessageOp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/pushmessages/PushMessageOp;

    return-object p0
.end method

.method public static values()[Lcom/squareup/pushmessages/PushMessageOp;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/pushmessages/PushMessageOp;->$VALUES:[Lcom/squareup/pushmessages/PushMessageOp;

    invoke-virtual {v0}, [Lcom/squareup/pushmessages/PushMessageOp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/pushmessages/PushMessageOp;

    return-object v0
.end method


# virtual methods
.method abstract createMessage(Ljava/util/Map;)Lcom/squareup/pushmessages/PushMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/pushmessages/PushMessage;"
        }
    .end annotation
.end method
