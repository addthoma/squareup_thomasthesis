.class final Lcom/squareup/printer/epson/EpsonPrinter$epsonPrinterSdk$2;
.super Lkotlin/jvm/internal/Lambda;
.source "EpsonPrinter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/printer/epson/EpsonPrinter;-><init>(Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/printer/epson/EpsonPrinterConnection;Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;Lcom/squareup/util/Clock;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/squareup/printer/epson/EpsonPrinterSdk;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/printer/epson/EpsonPrinterSdk;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/printer/epson/EpsonPrinter;


# direct methods
.method constructor <init>(Lcom/squareup/printer/epson/EpsonPrinter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinter$epsonPrinterSdk$2;->this$0:Lcom/squareup/printer/epson/EpsonPrinter;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/squareup/printer/epson/EpsonPrinterSdk;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinter$epsonPrinterSdk$2;->this$0:Lcom/squareup/printer/epson/EpsonPrinter;

    invoke-static {v0}, Lcom/squareup/printer/epson/EpsonPrinter;->access$getEpsonPrinterSdkFactory$p(Lcom/squareup/printer/epson/EpsonPrinter;)Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;

    move-result-object v0

    .line 34
    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinter$epsonPrinterSdk$2;->this$0:Lcom/squareup/printer/epson/EpsonPrinter;

    invoke-static {v1}, Lcom/squareup/printer/epson/EpsonPrinter;->access$getEpsonPrinterInfo$p(Lcom/squareup/printer/epson/EpsonPrinter;)Lcom/squareup/printer/epson/EpsonPrinterInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonPrinterInfo;->getEpsonPrinterAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;->getPrinterCode()I

    move-result v1

    const/4 v2, 0x0

    .line 33
    invoke-interface {v0, v1, v2}, Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;->create(II)Lcom/squareup/printer/epson/EpsonPrinterSdk;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/printer/epson/EpsonPrinter$epsonPrinterSdk$2;->invoke()Lcom/squareup/printer/epson/EpsonPrinterSdk;

    move-result-object v0

    return-object v0
.end method
