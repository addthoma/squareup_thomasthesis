.class public final Lcom/squareup/printer/epson/RealEpsonPrinterScouts_Factory;
.super Ljava/lang/Object;
.source "RealEpsonPrinterScouts_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/printer/epson/RealEpsonPrinterScouts;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/EpsonTcpPrinterScout;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/EpsonUsbPrinterScout;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/EpsonTcpPrinterScout;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/EpsonUsbPrinterScout;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/printer/epson/RealEpsonPrinterScouts_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/printer/epson/RealEpsonPrinterScouts_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/squareup/printer/epson/RealEpsonPrinterScouts_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/printer/epson/RealEpsonPrinterScouts_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/EpsonTcpPrinterScout;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/EpsonUsbPrinterScout;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/printer/epson/RealEpsonPrinterScouts_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/printer/epson/RealEpsonPrinterScouts_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/printer/epson/RealEpsonPrinterScouts_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/print/EpsonTcpPrinterScout;Lcom/squareup/print/EpsonUsbPrinterScout;Lcom/squareup/settings/server/Features;)Lcom/squareup/printer/epson/RealEpsonPrinterScouts;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/printer/epson/RealEpsonPrinterScouts;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/printer/epson/RealEpsonPrinterScouts;-><init>(Lcom/squareup/print/EpsonTcpPrinterScout;Lcom/squareup/print/EpsonUsbPrinterScout;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/printer/epson/RealEpsonPrinterScouts;
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/squareup/printer/epson/RealEpsonPrinterScouts_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/EpsonTcpPrinterScout;

    iget-object v1, p0, Lcom/squareup/printer/epson/RealEpsonPrinterScouts_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/EpsonUsbPrinterScout;

    iget-object v2, p0, Lcom/squareup/printer/epson/RealEpsonPrinterScouts_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2}, Lcom/squareup/printer/epson/RealEpsonPrinterScouts_Factory;->newInstance(Lcom/squareup/print/EpsonTcpPrinterScout;Lcom/squareup/print/EpsonUsbPrinterScout;Lcom/squareup/settings/server/Features;)Lcom/squareup/printer/epson/RealEpsonPrinterScouts;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/printer/epson/RealEpsonPrinterScouts_Factory;->get()Lcom/squareup/printer/epson/RealEpsonPrinterScouts;

    move-result-object v0

    return-object v0
.end method
