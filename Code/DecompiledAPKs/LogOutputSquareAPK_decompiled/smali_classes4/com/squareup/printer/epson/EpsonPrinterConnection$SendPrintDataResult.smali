.class public final Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;
.super Ljava/lang/Object;
.source "EpsonPrinterConnection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/printer/epson/EpsonPrinterConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SendPrintDataResult"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0015\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u0007\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00c6\u0003J\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000eJ\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000eJD\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u00072\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u0007H\u00c6\u0001\u00a2\u0006\u0002\u0010\u001bJ\u0013\u0010\u001c\u001a\u00020\u001d2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001f\u001a\u00020\u0007H\u00d6\u0001J\t\u0010 \u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0015\u0010\u0008\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\n\n\u0002\u0010\u000f\u001a\u0004\u0008\r\u0010\u000eR\u0015\u0010\t\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\n\n\u0002\u0010\u000f\u001a\u0004\u0008\u0010\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;",
        "",
        "printResult",
        "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
        "epsonPrinterStatus",
        "",
        "attempts",
        "",
        "callbackCode",
        "epsonExceptionCode",
        "(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;)V",
        "getAttempts",
        "()I",
        "getCallbackCode",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "getEpsonExceptionCode",
        "getEpsonPrinterStatus",
        "()Ljava/lang/String;",
        "getPrintResult",
        "()Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;)Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final attempts:I

.field private final callbackCode:Ljava/lang/Integer;

.field private final epsonExceptionCode:Ljava/lang/Integer;

.field private final epsonPrinterStatus:Ljava/lang/String;

.field private final printResult:Lcom/squareup/print/PrintJob$PrintAttempt$Result;


# direct methods
.method public constructor <init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1

    const-string v0, "printResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "epsonPrinterStatus"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->printResult:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    iput-object p2, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->epsonPrinterStatus:Ljava/lang/String;

    iput p3, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->attempts:I

    iput-object p4, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->callbackCode:Ljava/lang/Integer;

    iput-object p5, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->epsonExceptionCode:Ljava/lang/Integer;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p7, p6, 0x4

    if-eqz p7, :cond_0

    const/4 p3, -0x1

    const/4 v3, -0x1

    goto :goto_0

    :cond_0
    move v3, p3

    :goto_0
    and-int/lit8 p3, p6, 0x8

    const/4 p7, 0x0

    if-eqz p3, :cond_1

    .line 244
    move-object p4, p7

    check-cast p4, Ljava/lang/Integer;

    :cond_1
    move-object v4, p4

    and-int/lit8 p3, p6, 0x10

    if-eqz p3, :cond_2

    .line 245
    move-object p5, p7

    check-cast p5, Ljava/lang/Integer;

    :cond_2
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;-><init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;Lcom/squareup/print/PrintJob$PrintAttempt$Result;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;ILjava/lang/Object;)Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->printResult:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->epsonPrinterStatus:Ljava/lang/String;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->attempts:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->callbackCode:Ljava/lang/Integer;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->epsonExceptionCode:Ljava/lang/Integer;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->copy(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;)Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/print/PrintJob$PrintAttempt$Result;
    .locals 1

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->printResult:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->epsonPrinterStatus:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->attempts:I

    return v0
.end method

.method public final component4()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->callbackCode:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component5()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->epsonExceptionCode:Ljava/lang/Integer;

    return-object v0
.end method

.method public final copy(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;)Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;
    .locals 7

    const-string v0, "printResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "epsonPrinterStatus"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;-><init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->printResult:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    iget-object v1, p1, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->printResult:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->epsonPrinterStatus:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->epsonPrinterStatus:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->attempts:I

    iget v1, p1, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->attempts:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->callbackCode:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->callbackCode:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->epsonExceptionCode:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->epsonExceptionCode:Ljava/lang/Integer;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAttempts()I
    .locals 1

    .line 243
    iget v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->attempts:I

    return v0
.end method

.method public final getCallbackCode()Ljava/lang/Integer;
    .locals 1

    .line 244
    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->callbackCode:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getEpsonExceptionCode()Ljava/lang/Integer;
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->epsonExceptionCode:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getEpsonPrinterStatus()Ljava/lang/String;
    .locals 1

    .line 242
    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->epsonPrinterStatus:Ljava/lang/String;

    return-object v0
.end method

.method public final getPrintResult()Lcom/squareup/print/PrintJob$PrintAttempt$Result;
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->printResult:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->printResult:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->epsonPrinterStatus:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->attempts:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->callbackCode:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->epsonExceptionCode:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SendPrintDataResult(printResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->printResult:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", epsonPrinterStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->epsonPrinterStatus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", attempts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->attempts:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", callbackCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->callbackCode:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", epsonExceptionCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->epsonExceptionCode:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
