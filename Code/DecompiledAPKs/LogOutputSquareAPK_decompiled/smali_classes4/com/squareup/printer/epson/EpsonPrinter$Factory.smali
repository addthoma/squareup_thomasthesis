.class public final Lcom/squareup/printer/epson/EpsonPrinter$Factory;
.super Ljava/lang/Object;
.source "EpsonPrinter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/printer/epson/EpsonPrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/printer/epson/EpsonPrinter$Factory;",
        "",
        "epsonPrinterConnection",
        "Lcom/squareup/printer/epson/EpsonPrinterConnection;",
        "epsonPrinterSdkFactory",
        "Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "(Lcom/squareup/printer/epson/EpsonPrinterConnection;Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;Lcom/squareup/util/Clock;)V",
        "create",
        "Lcom/squareup/printer/epson/EpsonPrinter;",
        "epsonPrinterInfo",
        "Lcom/squareup/printer/epson/EpsonPrinterInfo;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final epsonPrinterConnection:Lcom/squareup/printer/epson/EpsonPrinterConnection;

.field private final epsonPrinterSdkFactory:Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;


# direct methods
.method public constructor <init>(Lcom/squareup/printer/epson/EpsonPrinterConnection;Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;Lcom/squareup/util/Clock;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "epsonPrinterConnection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "epsonPrinterSdkFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinter$Factory;->epsonPrinterConnection:Lcom/squareup/printer/epson/EpsonPrinterConnection;

    iput-object p2, p0, Lcom/squareup/printer/epson/EpsonPrinter$Factory;->epsonPrinterSdkFactory:Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;

    iput-object p3, p0, Lcom/squareup/printer/epson/EpsonPrinter$Factory;->clock:Lcom/squareup/util/Clock;

    return-void
.end method


# virtual methods
.method public final create(Lcom/squareup/printer/epson/EpsonPrinterInfo;)Lcom/squareup/printer/epson/EpsonPrinter;
    .locals 4

    const-string v0, "epsonPrinterInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinter;

    .line 115
    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinter$Factory;->epsonPrinterConnection:Lcom/squareup/printer/epson/EpsonPrinterConnection;

    .line 116
    iget-object v2, p0, Lcom/squareup/printer/epson/EpsonPrinter$Factory;->epsonPrinterSdkFactory:Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;

    .line 117
    iget-object v3, p0, Lcom/squareup/printer/epson/EpsonPrinter$Factory;->clock:Lcom/squareup/util/Clock;

    .line 113
    invoke-direct {v0, p1, v1, v2, v3}, Lcom/squareup/printer/epson/EpsonPrinter;-><init>(Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/printer/epson/EpsonPrinterConnection;Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;Lcom/squareup/util/Clock;)V

    return-object v0
.end method
