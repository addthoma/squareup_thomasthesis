.class public interface abstract Lcom/squareup/printer/epson/EpsonPrinterSdk;
.super Ljava/lang/Object;
.source "EpsonPrinterSdk.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u0006\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&JX\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\u00052\u0006\u0010\u000c\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0005H&J\u0018\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00052\u0006\u0010\u0015\u001a\u00020\u0005H&J\u0010\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u0018H&J(\u0010\u0019\u001a\u00020\u00032\u0006\u0010\u001a\u001a\u00020\u00052\u0006\u0010\u001b\u001a\u00020\u00052\u0006\u0010\u001c\u001a\u00020\u00052\u0006\u0010\u001d\u001a\u00020\u0005H&J\u0008\u0010\u001e\u001a\u00020\u0003H&J\u0008\u0010\u001f\u001a\u00020\u0003H&J\u0018\u0010 \u001a\u00020\u00032\u0006\u0010!\u001a\u00020\u00182\u0006\u0010\"\u001a\u00020\u0005H&J\u0008\u0010#\u001a\u00020\u0003H&J\u0008\u0010$\u001a\u00020%H&J\u0010\u0010&\u001a\u00020\u00032\u0006\u0010\"\u001a\u00020\u0005H&JW\u0010\'\u001a\u00020\u00032M\u0010(\u001aI\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\u000c\u0008*\u0012\u0008\u0008+\u0012\u0004\u0008\u0008(,\u0012\u0013\u0012\u00110%\u00a2\u0006\u000c\u0008*\u0012\u0008\u0008+\u0012\u0004\u0008\u0008(-\u0012\u0015\u0012\u0013\u0018\u00010\u0018\u00a2\u0006\u000c\u0008*\u0012\u0008\u0008+\u0012\u0004\u0008\u0008(.\u0012\u0004\u0012\u00020\u00030)H&\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/printer/epson/EpsonPrinterSdk;",
        "",
        "addCut",
        "",
        "type",
        "",
        "addImage",
        "data",
        "Landroid/graphics/Bitmap;",
        "x",
        "y",
        "width",
        "height",
        "color",
        "mode",
        "halftone",
        "brightness",
        "",
        "compress",
        "addPulse",
        "drawer",
        "time",
        "addText",
        "text",
        "",
        "addTextStyle",
        "reverse",
        "underline",
        "bold",
        "textColor",
        "clearCommandBuffer",
        "clearReceiveEventListener",
        "connect",
        "target",
        "timeout",
        "disconnect",
        "getPrinterStatusInfo",
        "Lcom/squareup/printer/epson/PrinterStatusInfo;",
        "sendData",
        "setReceiveEventListener",
        "receiveEventListener",
        "Lkotlin/Function3;",
        "Lkotlin/ParameterName;",
        "name",
        "code",
        "printerStatusInfo",
        "printJobId",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract addCut(I)V
.end method

.method public abstract addImage(Landroid/graphics/Bitmap;IIIIIIIDI)V
.end method

.method public abstract addPulse(II)V
.end method

.method public abstract addText(Ljava/lang/String;)V
.end method

.method public abstract addTextStyle(IIII)V
.end method

.method public abstract clearCommandBuffer()V
.end method

.method public abstract clearReceiveEventListener()V
.end method

.method public abstract connect(Ljava/lang/String;I)V
.end method

.method public abstract disconnect()V
.end method

.method public abstract getPrinterStatusInfo()Lcom/squareup/printer/epson/PrinterStatusInfo;
.end method

.method public abstract sendData(I)V
.end method

.method public abstract setReceiveEventListener(Lkotlin/jvm/functions/Function3;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Lcom/squareup/printer/epson/PrinterStatusInfo;",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation
.end method
