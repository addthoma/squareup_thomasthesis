.class public final Lcom/squareup/printer/epson/RealEpsonDiscoverer_Factory;
.super Ljava/lang/Object;
.source "RealEpsonDiscoverer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/printer/epson/RealEpsonDiscoverer;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/printer/epson/EpsonPrinter$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/printer/epson/EpsonPrinter$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p3, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/printer/epson/RealEpsonDiscoverer_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/printer/epson/EpsonPrinter$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/printer/epson/RealEpsonDiscoverer_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/printer/epson/RealEpsonDiscoverer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/printer/epson/EpsonPrinter$Factory;Landroid/app/Application;Lcom/squareup/settings/server/Features;)Lcom/squareup/printer/epson/RealEpsonDiscoverer;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/printer/epson/RealEpsonDiscoverer;-><init>(Lcom/squareup/printer/epson/EpsonPrinter$Factory;Landroid/app/Application;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/printer/epson/RealEpsonDiscoverer;
    .locals 3

    .line 29
    iget-object v0, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/printer/epson/EpsonPrinter$Factory;

    iget-object v1, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Application;

    iget-object v2, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2}, Lcom/squareup/printer/epson/RealEpsonDiscoverer_Factory;->newInstance(Lcom/squareup/printer/epson/EpsonPrinter$Factory;Landroid/app/Application;Lcom/squareup/settings/server/Features;)Lcom/squareup/printer/epson/RealEpsonDiscoverer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/printer/epson/RealEpsonDiscoverer_Factory;->get()Lcom/squareup/printer/epson/RealEpsonDiscoverer;

    move-result-object v0

    return-object v0
.end method
