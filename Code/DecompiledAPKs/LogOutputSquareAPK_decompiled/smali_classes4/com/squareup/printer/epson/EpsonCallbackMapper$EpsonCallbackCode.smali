.class public final enum Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;
.super Ljava/lang/Enum;
.source "EpsonCallbackMapper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/printer/epson/EpsonCallbackMapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EpsonCallbackCode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008.\u0008\u0087\u0001\u0018\u0000 02\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u00010B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018j\u0002\u0008\u0019j\u0002\u0008\u001aj\u0002\u0008\u001bj\u0002\u0008\u001cj\u0002\u0008\u001dj\u0002\u0008\u001ej\u0002\u0008\u001fj\u0002\u0008 j\u0002\u0008!j\u0002\u0008\"j\u0002\u0008#j\u0002\u0008$j\u0002\u0008%j\u0002\u0008&j\u0002\u0008\'j\u0002\u0008(j\u0002\u0008)j\u0002\u0008*j\u0002\u0008+j\u0002\u0008,j\u0002\u0008-j\u0002\u0008.j\u0002\u0008/\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;",
        "",
        "code",
        "",
        "(Ljava/lang/String;II)V",
        "getCode",
        "()I",
        "CODE_SUCCESS",
        "CODE_ERR_TIMEOUT",
        "CODE_ERR_NOT_FOUND",
        "CODE_ERR_AUTORECOVER",
        "CODE_ERR_COVER_OPEN",
        "CODE_ERR_CUTTER",
        "CODE_ERR_MECHANICAL",
        "CODE_ERR_EMPTY",
        "CODE_ERR_UNRECOVERABLE",
        "CODE_ERR_SYSTEM",
        "CODE_ERR_PORT",
        "CODE_ERR_INVALID_WINDOW",
        "CODE_ERR_JOB_NOT_FOUND",
        "CODE_PRINTING",
        "CODE_ERR_SPOOLER",
        "CODE_ERR_BATTERY_LOW",
        "CODE_ERR_TOO_MANY_REQUESTS",
        "CODE_ERR_REQUEST_ENTITY_TOO_LARGE",
        "CODE_CANCELED",
        "CODE_ERR_NO_MICR_DATA",
        "CODE_ERR_ILLEGAL_LENGTH",
        "CODE_ERR_NO_MAGNETIC_DATA",
        "CODE_ERR_RECOGNITION",
        "CODE_ERR_READ",
        "CODE_ERR_NOISE_DETECTED",
        "CODE_ERR_PAPER_JAM",
        "CODE_ERR_PAPER_PULLED_OUT",
        "CODE_ERR_CANCEL_FAILED",
        "CODE_ERR_PAPER_TYPE",
        "CODE_ERR_WAIT_INSERTION",
        "CODE_ERR_ILLEGAL",
        "CODE_ERR_INSERTED",
        "CODE_ERR_WAIT_REMOVAL",
        "CODE_ERR_DEVICE_BUSY",
        "CODE_ERR_IN_USE",
        "CODE_ERR_CONNECT",
        "CODE_ERR_DISCONNECT",
        "CODE_ERR_MEMORY",
        "CODE_ERR_PROCESSING",
        "CODE_ERR_PARAM",
        "CODE_ERR_FAILURE",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_CANCELED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_AUTORECOVER:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_BATTERY_LOW:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_CANCEL_FAILED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_CONNECT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_COVER_OPEN:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_CUTTER:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_DEVICE_BUSY:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_DISCONNECT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_EMPTY:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_FAILURE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_ILLEGAL:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_ILLEGAL_LENGTH:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_INSERTED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_INVALID_WINDOW:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_IN_USE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_JOB_NOT_FOUND:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_MECHANICAL:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_MEMORY:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_NOISE_DETECTED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_NOT_FOUND:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_NO_MAGNETIC_DATA:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_NO_MICR_DATA:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_PAPER_JAM:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_PAPER_PULLED_OUT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_PAPER_TYPE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_PARAM:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_PORT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_PROCESSING:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_READ:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_RECOGNITION:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_REQUEST_ENTITY_TOO_LARGE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_SPOOLER:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_SYSTEM:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_TIMEOUT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_TOO_MANY_REQUESTS:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_UNRECOVERABLE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_WAIT_INSERTION:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_ERR_WAIT_REMOVAL:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_PRINTING:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final enum CODE_SUCCESS:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

.field public static final Companion:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode$Companion;


# instance fields
.field private final code:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x29

    new-array v0, v0, [Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/4 v2, 0x0

    const-string v3, "CODE_SUCCESS"

    .line 138
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_SUCCESS:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/4 v2, 0x1

    const-string v3, "CODE_ERR_TIMEOUT"

    .line 139
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_TIMEOUT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/4 v2, 0x2

    const-string v3, "CODE_ERR_NOT_FOUND"

    .line 140
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_NOT_FOUND:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/4 v2, 0x3

    const-string v3, "CODE_ERR_AUTORECOVER"

    .line 141
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_AUTORECOVER:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/4 v2, 0x4

    const-string v3, "CODE_ERR_COVER_OPEN"

    .line 142
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_COVER_OPEN:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/4 v2, 0x5

    const-string v3, "CODE_ERR_CUTTER"

    .line 143
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_CUTTER:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/4 v2, 0x6

    const-string v3, "CODE_ERR_MECHANICAL"

    .line 144
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_MECHANICAL:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/4 v2, 0x7

    const-string v3, "CODE_ERR_EMPTY"

    .line 145
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_EMPTY:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x8

    const-string v3, "CODE_ERR_UNRECOVERABLE"

    .line 146
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_UNRECOVERABLE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x9

    const-string v3, "CODE_ERR_SYSTEM"

    .line 147
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_SYSTEM:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0xa

    const-string v3, "CODE_ERR_PORT"

    .line 148
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_PORT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0xb

    const-string v3, "CODE_ERR_INVALID_WINDOW"

    .line 149
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_INVALID_WINDOW:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0xc

    const-string v3, "CODE_ERR_JOB_NOT_FOUND"

    .line 150
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_JOB_NOT_FOUND:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0xd

    const-string v3, "CODE_PRINTING"

    .line 151
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_PRINTING:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0xe

    const-string v3, "CODE_ERR_SPOOLER"

    .line 152
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_SPOOLER:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_BATTERY_LOW"

    const/16 v3, 0xf

    const/16 v4, 0xf

    .line 153
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_BATTERY_LOW:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_TOO_MANY_REQUESTS"

    const/16 v3, 0x10

    const/16 v4, 0x10

    .line 154
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_TOO_MANY_REQUESTS:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_REQUEST_ENTITY_TOO_LARGE"

    const/16 v3, 0x11

    const/16 v4, 0x11

    .line 155
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_REQUEST_ENTITY_TOO_LARGE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_CANCELED"

    const/16 v3, 0x12

    const/16 v4, 0x12

    .line 156
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_CANCELED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_NO_MICR_DATA"

    const/16 v3, 0x13

    const/16 v4, 0x13

    .line 157
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_NO_MICR_DATA:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_ILLEGAL_LENGTH"

    const/16 v3, 0x14

    const/16 v4, 0x14

    .line 158
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_ILLEGAL_LENGTH:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_NO_MAGNETIC_DATA"

    const/16 v3, 0x15

    const/16 v4, 0x15

    .line 159
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_NO_MAGNETIC_DATA:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_RECOGNITION"

    const/16 v3, 0x16

    const/16 v4, 0x16

    .line 160
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_RECOGNITION:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_READ"

    const/16 v3, 0x17

    const/16 v4, 0x17

    .line 161
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_READ:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_NOISE_DETECTED"

    const/16 v3, 0x18

    const/16 v4, 0x18

    .line 162
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_NOISE_DETECTED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_PAPER_JAM"

    const/16 v3, 0x19

    const/16 v4, 0x19

    .line 163
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_PAPER_JAM:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_PAPER_PULLED_OUT"

    const/16 v3, 0x1a

    const/16 v4, 0x1a

    .line 164
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_PAPER_PULLED_OUT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_CANCEL_FAILED"

    const/16 v3, 0x1b

    const/16 v4, 0x1b

    .line 165
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_CANCEL_FAILED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_PAPER_TYPE"

    const/16 v3, 0x1c

    const/16 v4, 0x1c

    .line 166
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_PAPER_TYPE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_WAIT_INSERTION"

    const/16 v3, 0x1d

    const/16 v4, 0x1d

    .line 167
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_WAIT_INSERTION:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_ILLEGAL"

    const/16 v3, 0x1e

    const/16 v4, 0x1e

    .line 168
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_ILLEGAL:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_INSERTED"

    const/16 v3, 0x1f

    const/16 v4, 0x1f

    .line 169
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_INSERTED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_WAIT_REMOVAL"

    const/16 v3, 0x20

    const/16 v4, 0x20

    .line 170
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_WAIT_REMOVAL:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_DEVICE_BUSY"

    const/16 v3, 0x21

    const/16 v4, 0x21

    .line 171
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_DEVICE_BUSY:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_IN_USE"

    const/16 v3, 0x22

    const/16 v4, 0x22

    .line 172
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_IN_USE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_CONNECT"

    const/16 v3, 0x23

    const/16 v4, 0x23

    .line 173
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_CONNECT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_DISCONNECT"

    const/16 v3, 0x24

    const/16 v4, 0x24

    .line 174
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_DISCONNECT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_MEMORY"

    const/16 v3, 0x25

    const/16 v4, 0x25

    .line 175
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_MEMORY:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_PROCESSING"

    const/16 v3, 0x26

    const/16 v4, 0x26

    .line 176
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_PROCESSING:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_PARAM"

    const/16 v3, 0x27

    const/16 v4, 0x27

    .line 177
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_PARAM:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const-string v2, "CODE_ERR_FAILURE"

    const/16 v3, 0x28

    const/16 v4, 0xff

    .line 178
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_FAILURE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->$VALUES:[Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    new-instance v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->Companion:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 137
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->code:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;
    .locals 1

    const-class v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;
    .locals 1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->$VALUES:[Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v0}, [Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    return-object v0
.end method


# virtual methods
.method public final getCode()I
    .locals 1

    .line 137
    iget v0, p0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->code:I

    return v0
.end method
