.class public final Lcom/squareup/printer/epson/RealEpsonPrinterSdkKt;
.super Ljava/lang/Object;
.source "RealEpsonPrinterSdk.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "toPrinterStatusInfo",
        "Lcom/squareup/printer/epson/PrinterStatusInfo;",
        "Lcom/epson/epos2/printer/PrinterStatusInfo;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$toPrinterStatusInfo(Lcom/epson/epos2/printer/PrinterStatusInfo;)Lcom/squareup/printer/epson/PrinterStatusInfo;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/printer/epson/RealEpsonPrinterSdkKt;->toPrinterStatusInfo(Lcom/epson/epos2/printer/PrinterStatusInfo;)Lcom/squareup/printer/epson/PrinterStatusInfo;

    move-result-object p0

    return-object p0
.end method

.method private static final toPrinterStatusInfo(Lcom/epson/epos2/printer/PrinterStatusInfo;)Lcom/squareup/printer/epson/PrinterStatusInfo;
    .locals 15

    .line 111
    new-instance v14, Lcom/squareup/printer/epson/PrinterStatusInfo;

    .line 112
    invoke-virtual {p0}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getConnection()I

    move-result v1

    .line 113
    invoke-virtual {p0}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getOnline()I

    move-result v2

    .line 114
    invoke-virtual {p0}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getCoverOpen()I

    move-result v3

    .line 115
    invoke-virtual {p0}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getPaper()I

    move-result v4

    .line 116
    invoke-virtual {p0}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getPaperFeed()I

    move-result v5

    .line 117
    invoke-virtual {p0}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getPanelSwitch()I

    move-result v6

    .line 118
    invoke-virtual {p0}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getWaitOnline()I

    move-result v7

    .line 119
    invoke-virtual {p0}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getDrawer()I

    move-result v8

    .line 120
    invoke-virtual {p0}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getErrorStatus()I

    move-result v9

    .line 121
    invoke-virtual {p0}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getAutoRecoverError()I

    move-result v10

    .line 122
    invoke-virtual {p0}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getBuzzer()I

    move-result v11

    .line 123
    invoke-virtual {p0}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getAdapter()I

    move-result v12

    .line 124
    invoke-virtual {p0}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getBatteryLevel()I

    move-result v13

    move-object v0, v14

    .line 111
    invoke-direct/range {v0 .. v13}, Lcom/squareup/printer/epson/PrinterStatusInfo;-><init>(IIIIIIIIIIIII)V

    return-object v14
.end method
