.class Lcom/squareup/payment/Order$SensitiveValues;
.super Ljava/lang/Object;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SensitiveValues"
.end annotation


# instance fields
.field card:Lcom/squareup/Card;

.field iso8601TimeStamp:Ljava/lang/String;

.field uniqueClientId:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 430
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method copy()Lcom/squareup/payment/Order$SensitiveValues;
    .locals 2

    .line 444
    new-instance v0, Lcom/squareup/payment/Order$SensitiveValues;

    invoke-direct {v0}, Lcom/squareup/payment/Order$SensitiveValues;-><init>()V

    .line 445
    iget-object v1, p0, Lcom/squareup/payment/Order$SensitiveValues;->card:Lcom/squareup/Card;

    iput-object v1, v0, Lcom/squareup/payment/Order$SensitiveValues;->card:Lcom/squareup/Card;

    .line 446
    iget-object v1, p0, Lcom/squareup/payment/Order$SensitiveValues;->uniqueClientId:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/payment/Order$SensitiveValues;->uniqueClientId:Ljava/lang/String;

    .line 447
    iget-object v1, p0, Lcom/squareup/payment/Order$SensitiveValues;->iso8601TimeStamp:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/payment/Order$SensitiveValues;->iso8601TimeStamp:Ljava/lang/String;

    return-object v0
.end method
