.class final Lcom/squareup/payment/PaymentReceipt$VoidReceipt;
.super Lcom/squareup/payment/PaymentReceipt;
.source "PaymentReceipt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/PaymentReceipt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "VoidReceipt"
.end annotation


# direct methods
.method private constructor <init>(Lcom/squareup/payment/PaymentReceipt$Builder;)V
    .locals 1

    const/4 v0, 0x0

    .line 293
    invoke-direct {p0, p1, v0}, Lcom/squareup/payment/PaymentReceipt;-><init>(Lcom/squareup/payment/PaymentReceipt$Builder;Lcom/squareup/payment/PaymentReceipt$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/PaymentReceipt$Builder;Lcom/squareup/payment/PaymentReceipt$1;)V
    .locals 0

    .line 290
    invoke-direct {p0, p1}, Lcom/squareup/payment/PaymentReceipt$VoidReceipt;-><init>(Lcom/squareup/payment/PaymentReceipt$Builder;)V

    return-void
.end method


# virtual methods
.method public decline()V
    .locals 0

    return-void
.end method

.method public email(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public emailDefault()V
    .locals 0

    return-void
.end method

.method public sms(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public smsDefault()V
    .locals 0

    return-void
.end method
