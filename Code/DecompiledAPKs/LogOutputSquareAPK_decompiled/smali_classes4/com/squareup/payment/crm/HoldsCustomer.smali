.class public interface abstract Lcom/squareup/payment/crm/HoldsCustomer;
.super Ljava/lang/Object;
.source "HoldsCustomer.java"


# virtual methods
.method public abstract getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;
.end method

.method public abstract getCustomerId()Ljava/lang/String;
.end method

.method public abstract getCustomerInstrumentDetails()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;
.end method

.method public abstract hasCustomer()Z
.end method

.method public abstract onCustomerChanged()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;)V"
        }
    .end annotation
.end method
