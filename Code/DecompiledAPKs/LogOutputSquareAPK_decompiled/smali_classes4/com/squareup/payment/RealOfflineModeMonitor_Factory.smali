.class public final Lcom/squareup/payment/RealOfflineModeMonitor_Factory;
.super Ljava/lang/Object;
.source "RealOfflineModeMonitor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/RealOfflineModeMonitor;",
        ">;"
    }
.end annotation


# instance fields
.field private final connectivityCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/check/ConnectivityCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final eventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final executorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final queueServiceStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardKeysProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/StoreAndForwardKeys;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLazyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/StoreAndForwardKeys;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/check/ConnectivityCheck;",
            ">;)V"
        }
    .end annotation

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->eventSinkProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p2, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p3, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p4, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->executorProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p5, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p6, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->storeAndForwardKeysProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p7, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->transactionLazyProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p8, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->queueServiceStarterProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p9, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->connectivityCheckProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/RealOfflineModeMonitor_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/StoreAndForwardKeys;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/check/ConnectivityCheck;",
            ">;)",
            "Lcom/squareup/payment/RealOfflineModeMonitor_Factory;"
        }
    .end annotation

    .line 76
    new-instance v10, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/payment/offline/StoreAndForwardKeys;Ldagger/Lazy;Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/connectivity/check/ConnectivityCheck;)Lcom/squareup/payment/RealOfflineModeMonitor;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/payment/offline/StoreAndForwardKeys;",
            "Ldagger/Lazy<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Lcom/squareup/queue/QueueServiceStarter;",
            "Lcom/squareup/connectivity/check/ConnectivityCheck;",
            ")",
            "Lcom/squareup/payment/RealOfflineModeMonitor;"
        }
    .end annotation

    .line 84
    new-instance v10, Lcom/squareup/payment/RealOfflineModeMonitor;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/payment/RealOfflineModeMonitor;-><init>(Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/payment/offline/StoreAndForwardKeys;Ldagger/Lazy;Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/connectivity/check/ConnectivityCheck;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/payment/RealOfflineModeMonitor;
    .locals 10

    .line 65
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->eventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/badbus/BadEventSink;

    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->executorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->storeAndForwardKeysProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/offline/StoreAndForwardKeys;

    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->transactionLazyProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v7

    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->queueServiceStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/queue/QueueServiceStarter;

    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->connectivityCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/connectivity/check/ConnectivityCheck;

    invoke-static/range {v1 .. v9}, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->newInstance(Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/payment/offline/StoreAndForwardKeys;Ldagger/Lazy;Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/connectivity/check/ConnectivityCheck;)Lcom/squareup/payment/RealOfflineModeMonitor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/payment/RealOfflineModeMonitor_Factory;->get()Lcom/squareup/payment/RealOfflineModeMonitor;

    move-result-object v0

    return-object v0
.end method
