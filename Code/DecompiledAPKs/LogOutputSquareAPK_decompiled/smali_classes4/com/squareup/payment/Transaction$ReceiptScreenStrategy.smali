.class public final enum Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;
.super Ljava/lang/Enum;
.source "Transaction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReceiptScreenStrategy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

.field public static final enum READ_FROM_PAYMENT:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

.field public static final enum SHOW_RECEIPT_SCREEN:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

.field public static final enum SKIP_RECEIPT_SCREEN:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 264
    new-instance v0, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    const/4 v1, 0x0

    const-string v2, "SHOW_RECEIPT_SCREEN"

    invoke-direct {v0, v2, v1}, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->SHOW_RECEIPT_SCREEN:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    new-instance v0, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    const/4 v2, 0x1

    const-string v3, "SKIP_RECEIPT_SCREEN"

    invoke-direct {v0, v3, v2}, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->SKIP_RECEIPT_SCREEN:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    new-instance v0, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    const/4 v3, 0x2

    const-string v4, "READ_FROM_PAYMENT"

    invoke-direct {v0, v4, v3}, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->READ_FROM_PAYMENT:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    .line 263
    sget-object v4, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->SHOW_RECEIPT_SCREEN:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->SKIP_RECEIPT_SCREEN:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->READ_FROM_PAYMENT:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->$VALUES:[Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 263
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;
    .locals 1

    .line 263
    const-class v0, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    return-object p0
.end method

.method public static values()[Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;
    .locals 1

    .line 263
    sget-object v0, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->$VALUES:[Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    invoke-virtual {v0}, [Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    return-object v0
.end method
