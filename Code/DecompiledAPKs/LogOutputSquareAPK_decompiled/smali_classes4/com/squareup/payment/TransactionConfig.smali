.class public final Lcom/squareup/payment/TransactionConfig;
.super Ljava/lang/Object;
.source "TransactionConfig.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/TransactionConfig$Builder;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u001c\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001+BI\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000c\u0012\u0006\u0010\u000e\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\u000fJ\t\u0010\u001d\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\tH\u00c6\u0003J\t\u0010!\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\"\u001a\u00020\u000cH\u00c6\u0003J\t\u0010#\u001a\u00020\u000cH\u00c6\u0003J\t\u0010$\u001a\u00020\u000cH\u00c6\u0003J]\u0010%\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000cH\u00c6\u0001J\u0013\u0010&\u001a\u00020\u000c2\u0008\u0010\'\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010(\u001a\u00020)H\u00d6\u0001J\t\u0010*\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\n\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u000e\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\r\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0015R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0013R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0015R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/payment/TransactionConfig;",
        "",
        "amount",
        "",
        "note",
        "",
        "receiptScreenStrategy",
        "Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;",
        "tipSettings",
        "Lcom/squareup/settings/server/TipSettings;",
        "apiClientId",
        "skipSignature",
        "",
        "delayCapture",
        "clearTaxes",
        "(JLjava/lang/String;Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;Lcom/squareup/settings/server/TipSettings;Ljava/lang/String;ZZZ)V",
        "getAmount",
        "()J",
        "getApiClientId",
        "()Ljava/lang/String;",
        "getClearTaxes",
        "()Z",
        "getDelayCapture",
        "getNote",
        "getReceiptScreenStrategy",
        "()Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;",
        "getSkipSignature",
        "getTipSettings",
        "()Lcom/squareup/settings/server/TipSettings;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "Builder",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amount:J

.field private final apiClientId:Ljava/lang/String;

.field private final clearTaxes:Z

.field private final delayCapture:Z

.field private final note:Ljava/lang/String;

.field private final receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

.field private final skipSignature:Z

.field private final tipSettings:Lcom/squareup/settings/server/TipSettings;


# direct methods
.method public constructor <init>(JLjava/lang/String;Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;Lcom/squareup/settings/server/TipSettings;Ljava/lang/String;ZZZ)V
    .locals 1

    const-string v0, "receiptScreenStrategy"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiClientId"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/squareup/payment/TransactionConfig;->amount:J

    iput-object p3, p0, Lcom/squareup/payment/TransactionConfig;->note:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/payment/TransactionConfig;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    iput-object p5, p0, Lcom/squareup/payment/TransactionConfig;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    iput-object p6, p0, Lcom/squareup/payment/TransactionConfig;->apiClientId:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/squareup/payment/TransactionConfig;->skipSignature:Z

    iput-boolean p8, p0, Lcom/squareup/payment/TransactionConfig;->delayCapture:Z

    iput-boolean p9, p0, Lcom/squareup/payment/TransactionConfig;->clearTaxes:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/payment/TransactionConfig;JLjava/lang/String;Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;Lcom/squareup/settings/server/TipSettings;Ljava/lang/String;ZZZILjava/lang/Object;)Lcom/squareup/payment/TransactionConfig;
    .locals 10

    move-object v0, p0

    move/from16 v1, p10

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-wide v2, v0, Lcom/squareup/payment/TransactionConfig;->amount:J

    goto :goto_0

    :cond_0
    move-wide v2, p1

    :goto_0
    and-int/lit8 v4, v1, 0x2

    if-eqz v4, :cond_1

    iget-object v4, v0, Lcom/squareup/payment/TransactionConfig;->note:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v4, p3

    :goto_1
    and-int/lit8 v5, v1, 0x4

    if-eqz v5, :cond_2

    iget-object v5, v0, Lcom/squareup/payment/TransactionConfig;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    goto :goto_2

    :cond_2
    move-object v5, p4

    :goto_2
    and-int/lit8 v6, v1, 0x8

    if-eqz v6, :cond_3

    iget-object v6, v0, Lcom/squareup/payment/TransactionConfig;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    goto :goto_3

    :cond_3
    move-object v6, p5

    :goto_3
    and-int/lit8 v7, v1, 0x10

    if-eqz v7, :cond_4

    iget-object v7, v0, Lcom/squareup/payment/TransactionConfig;->apiClientId:Ljava/lang/String;

    goto :goto_4

    :cond_4
    move-object/from16 v7, p6

    :goto_4
    and-int/lit8 v8, v1, 0x20

    if-eqz v8, :cond_5

    iget-boolean v8, v0, Lcom/squareup/payment/TransactionConfig;->skipSignature:Z

    goto :goto_5

    :cond_5
    move/from16 v8, p7

    :goto_5
    and-int/lit8 v9, v1, 0x40

    if-eqz v9, :cond_6

    iget-boolean v9, v0, Lcom/squareup/payment/TransactionConfig;->delayCapture:Z

    goto :goto_6

    :cond_6
    move/from16 v9, p8

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-boolean v1, v0, Lcom/squareup/payment/TransactionConfig;->clearTaxes:Z

    goto :goto_7

    :cond_7
    move/from16 v1, p9

    :goto_7
    move-wide p1, v2

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object/from16 p6, v7

    move/from16 p7, v8

    move/from16 p8, v9

    move/from16 p9, v1

    invoke-virtual/range {p0 .. p9}, Lcom/squareup/payment/TransactionConfig;->copy(JLjava/lang/String;Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;Lcom/squareup/settings/server/TipSettings;Ljava/lang/String;ZZZ)Lcom/squareup/payment/TransactionConfig;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/payment/TransactionConfig;->amount:J

    return-wide v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/payment/TransactionConfig;->note:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;
    .locals 1

    iget-object v0, p0, Lcom/squareup/payment/TransactionConfig;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    return-object v0
.end method

.method public final component4()Lcom/squareup/settings/server/TipSettings;
    .locals 1

    iget-object v0, p0, Lcom/squareup/payment/TransactionConfig;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/payment/TransactionConfig;->apiClientId:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/payment/TransactionConfig;->skipSignature:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/payment/TransactionConfig;->delayCapture:Z

    return v0
.end method

.method public final component8()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/payment/TransactionConfig;->clearTaxes:Z

    return v0
.end method

.method public final copy(JLjava/lang/String;Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;Lcom/squareup/settings/server/TipSettings;Ljava/lang/String;ZZZ)Lcom/squareup/payment/TransactionConfig;
    .locals 11

    const-string v0, "receiptScreenStrategy"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiClientId"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/payment/TransactionConfig;

    move-object v1, v0

    move-wide v2, p1

    move-object v4, p3

    move-object/from16 v6, p5

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    invoke-direct/range {v1 .. v10}, Lcom/squareup/payment/TransactionConfig;-><init>(JLjava/lang/String;Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;Lcom/squareup/settings/server/TipSettings;Ljava/lang/String;ZZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/payment/TransactionConfig;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/payment/TransactionConfig;

    iget-wide v0, p0, Lcom/squareup/payment/TransactionConfig;->amount:J

    iget-wide v2, p1, Lcom/squareup/payment/TransactionConfig;->amount:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/TransactionConfig;->note:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/payment/TransactionConfig;->note:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/TransactionConfig;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    iget-object v1, p1, Lcom/squareup/payment/TransactionConfig;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/TransactionConfig;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    iget-object v1, p1, Lcom/squareup/payment/TransactionConfig;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/TransactionConfig;->apiClientId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/payment/TransactionConfig;->apiClientId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/TransactionConfig;->skipSignature:Z

    iget-boolean v1, p1, Lcom/squareup/payment/TransactionConfig;->skipSignature:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/TransactionConfig;->delayCapture:Z

    iget-boolean v1, p1, Lcom/squareup/payment/TransactionConfig;->delayCapture:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/TransactionConfig;->clearTaxes:Z

    iget-boolean p1, p1, Lcom/squareup/payment/TransactionConfig;->clearTaxes:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmount()J
    .locals 2

    .line 8
    iget-wide v0, p0, Lcom/squareup/payment/TransactionConfig;->amount:J

    return-wide v0
.end method

.method public final getApiClientId()Ljava/lang/String;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/payment/TransactionConfig;->apiClientId:Ljava/lang/String;

    return-object v0
.end method

.method public final getClearTaxes()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/squareup/payment/TransactionConfig;->clearTaxes:Z

    return v0
.end method

.method public final getDelayCapture()Z
    .locals 1

    .line 14
    iget-boolean v0, p0, Lcom/squareup/payment/TransactionConfig;->delayCapture:Z

    return v0
.end method

.method public final getNote()Ljava/lang/String;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/payment/TransactionConfig;->note:Ljava/lang/String;

    return-object v0
.end method

.method public final getReceiptScreenStrategy()Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/payment/TransactionConfig;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    return-object v0
.end method

.method public final getSkipSignature()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/squareup/payment/TransactionConfig;->skipSignature:Z

    return v0
.end method

.method public final getTipSettings()Lcom/squareup/settings/server/TipSettings;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/payment/TransactionConfig;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-wide v0, p0, Lcom/squareup/payment/TransactionConfig;->amount:J

    invoke-static {v0, v1}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/payment/TransactionConfig;->note:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/payment/TransactionConfig;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/payment/TransactionConfig;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/payment/TransactionConfig;->apiClientId:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/payment/TransactionConfig;->skipSignature:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/payment/TransactionConfig;->delayCapture:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/payment/TransactionConfig;->clearTaxes:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransactionConfig(amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/payment/TransactionConfig;->amount:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/payment/TransactionConfig;->note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", receiptScreenStrategy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/payment/TransactionConfig;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tipSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/payment/TransactionConfig;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", apiClientId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/payment/TransactionConfig;->apiClientId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", skipSignature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/payment/TransactionConfig;->skipSignature:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", delayCapture="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/payment/TransactionConfig;->delayCapture:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", clearTaxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/payment/TransactionConfig;->clearTaxes:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
