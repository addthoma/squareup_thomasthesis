.class public Lcom/squareup/payment/Obfuscated;
.super Ljava/lang/Object;
.source "Obfuscated.java"


# static fields
.field private static final ASTERISKS:Ljava/util/regex/Pattern;


# instance fields
.field private final id:Ljava/lang/String;

.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "\\*{4,}"

    .line 12
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/Obfuscated;->ASTERISKS:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/payment/Obfuscated;->id:Ljava/lang/String;

    if-nez p2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 19
    :cond_0
    sget-object p1, Lcom/squareup/payment/Obfuscated;->ASTERISKS:Ljava/util/regex/Pattern;

    const-string v0, "***"

    invoke-static {p2, p1, v0}, Lcom/squareup/util/Strings;->replaceAll(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/payment/Obfuscated;->value:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/payment/Obfuscated;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/payment/Obfuscated;->value:Ljava/lang/String;

    return-object v0
.end method
