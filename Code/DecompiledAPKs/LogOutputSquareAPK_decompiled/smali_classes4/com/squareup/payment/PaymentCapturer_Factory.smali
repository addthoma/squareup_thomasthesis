.class public final Lcom/squareup/payment/PaymentCapturer_Factory;
.super Ljava/lang/Object;
.source "PaymentCapturer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/PaymentCapturer;",
        ">;"
    }
.end annotation


# instance fields
.field private final autoCaptureControlProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            ">;"
        }
    .end annotation
.end field

.field private final completedCaptureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final paymentAccuracyLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketAutoIdentifiersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/TicketAutoIdentifiers;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/TicketAutoIdentifiers;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/payment/PaymentCapturer_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/payment/PaymentCapturer_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/payment/PaymentCapturer_Factory;->autoCaptureControlProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/payment/PaymentCapturer_Factory;->ticketAutoIdentifiersProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/payment/PaymentCapturer_Factory;->completedCaptureProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/payment/PaymentCapturer_Factory;->paymentAccuracyLoggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/PaymentCapturer_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/TicketAutoIdentifiers;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ">;)",
            "Lcom/squareup/payment/PaymentCapturer_Factory;"
        }
    .end annotation

    .line 57
    new-instance v7, Lcom/squareup/payment/PaymentCapturer_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/PaymentCapturer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/print/PrinterStations;Lcom/squareup/autocapture/AutoCaptureControl;Lcom/squareup/print/TicketAutoIdentifiers;Lcom/squareup/settings/LocalSetting;Lcom/squareup/payment/PaymentAccuracyLogger;)Lcom/squareup/payment/PaymentCapturer;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            "Lcom/squareup/print/TicketAutoIdentifiers;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ")",
            "Lcom/squareup/payment/PaymentCapturer;"
        }
    .end annotation

    .line 64
    new-instance v7, Lcom/squareup/payment/PaymentCapturer;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/PaymentCapturer;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/print/PrinterStations;Lcom/squareup/autocapture/AutoCaptureControl;Lcom/squareup/print/TicketAutoIdentifiers;Lcom/squareup/settings/LocalSetting;Lcom/squareup/payment/PaymentAccuracyLogger;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/payment/PaymentCapturer;
    .locals 7

    .line 48
    iget-object v0, p0, Lcom/squareup/payment/PaymentCapturer_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/payment/PaymentCapturer_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/print/PrinterStations;

    iget-object v0, p0, Lcom/squareup/payment/PaymentCapturer_Factory;->autoCaptureControlProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/autocapture/AutoCaptureControl;

    iget-object v0, p0, Lcom/squareup/payment/PaymentCapturer_Factory;->ticketAutoIdentifiersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/print/TicketAutoIdentifiers;

    iget-object v0, p0, Lcom/squareup/payment/PaymentCapturer_Factory;->completedCaptureProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/payment/PaymentCapturer_Factory;->paymentAccuracyLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/PaymentAccuracyLogger;

    invoke-static/range {v1 .. v6}, Lcom/squareup/payment/PaymentCapturer_Factory;->newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/print/PrinterStations;Lcom/squareup/autocapture/AutoCaptureControl;Lcom/squareup/print/TicketAutoIdentifiers;Lcom/squareup/settings/LocalSetting;Lcom/squareup/payment/PaymentAccuracyLogger;)Lcom/squareup/payment/PaymentCapturer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/payment/PaymentCapturer_Factory;->get()Lcom/squareup/payment/PaymentCapturer;

    move-result-object v0

    return-object v0
.end method
