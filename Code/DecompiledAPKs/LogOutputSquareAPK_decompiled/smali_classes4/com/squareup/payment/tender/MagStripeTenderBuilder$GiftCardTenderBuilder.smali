.class public Lcom/squareup/payment/tender/MagStripeTenderBuilder$GiftCardTenderBuilder;
.super Lcom/squareup/payment/tender/MagStripeTenderBuilder;
.source "MagStripeTenderBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/tender/MagStripeTenderBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GiftCardTenderBuilder"
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/settings/server/SwipeChipCardsSettings;Ljava/lang/String;)V
    .locals 0

    .line 65
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/payment/tender/MagStripeTenderBuilder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/settings/server/SwipeChipCardsSettings;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/payment/tender/BaseTender;
    .locals 1

    .line 62
    invoke-virtual {p0}, Lcom/squareup/payment/tender/MagStripeTenderBuilder$GiftCardTenderBuilder;->build()Lcom/squareup/payment/tender/MagStripeTender;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/squareup/payment/tender/MagStripeTender;
    .locals 1

    .line 81
    new-instance v0, Lcom/squareup/payment/tender/MagStripeTender;

    invoke-direct {v0, p0}, Lcom/squareup/payment/tender/MagStripeTender;-><init>(Lcom/squareup/payment/tender/MagStripeTenderBuilder;)V

    return-object v0
.end method

.method public getMaxAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/payment/tender/MagStripeTenderBuilder$GiftCardTenderBuilder;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getMinAmount()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 73
    iget-object v0, p0, Lcom/squareup/payment/tender/MagStripeTenderBuilder$GiftCardTenderBuilder;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v1, 0x1

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public isGiftCard()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
