.class Lcom/squareup/payment/BillPaymentOnlineStrategy;
.super Ljava/lang/Object;
.source "BillPaymentOnlineStrategy.java"

# interfaces
.implements Lcom/squareup/payment/BillPayment$BillPaymentStrategy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;
    }
.end annotation


# instance fields
.field private final apiClientId:Ljava/lang/String;

.field private final backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

.field private final billCreationService:Lcom/squareup/server/bills/BillCreationService;


# direct methods
.method private constructor <init>(Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;Ljava/lang/String;)V
    .locals 1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p1}, Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;->access$000(Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;)Lcom/squareup/server/bills/BillCreationService;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/BillPaymentOnlineStrategy;->billCreationService:Lcom/squareup/server/bills/BillCreationService;

    .line 23
    invoke-static {p1}, Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;->access$100(Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;)Lcom/squareup/payment/BackgroundCaptor;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/BillPaymentOnlineStrategy;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    .line 24
    iput-object p2, p0, Lcom/squareup/payment/BillPaymentOnlineStrategy;->apiClientId:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;Ljava/lang/String;Lcom/squareup/payment/BillPaymentOnlineStrategy$1;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2}, Lcom/squareup/payment/BillPaymentOnlineStrategy;-><init>(Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;Ljava/lang/String;)V

    return-void
.end method

.method private enqueueFlushedTenderTasks(Lcom/squareup/payment/BillPayment;)V
    .locals 3

    .line 68
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->getFlushedTenders()Ljava/util/Set;

    move-result-object v0

    .line 69
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/tender/BaseTender;

    .line 71
    invoke-virtual {v1}, Lcom/squareup/payment/tender/BaseTender;->enqueueSignature()V

    .line 76
    invoke-virtual {v1}, Lcom/squareup/payment/tender/BaseTender;->enqueueReceipt()V

    .line 81
    invoke-virtual {p1, v1}, Lcom/squareup/payment/BillPayment;->isLastAddedTender(Lcom/squareup/payment/tender/BaseTender;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 82
    invoke-virtual {v1}, Lcom/squareup/payment/tender/BaseTender;->enqueueAttachContactTask()V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public addTenders(Lcom/squareup/protos/client/bills/AddTendersRequest;)Lcom/squareup/protos/client/bills/AddTendersResponse;
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOnlineStrategy;->billCreationService:Lcom/squareup/server/bills/BillCreationService;

    iget-object v1, p0, Lcom/squareup/payment/BillPaymentOnlineStrategy;->apiClientId:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/server/bills/ApiClientId;->clientIdOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/squareup/server/bills/BillCreationService;->addTenders(Lcom/squareup/protos/client/bills/AddTendersRequest;Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddTendersResponse;

    move-result-object p1

    return-object p1
.end method

.method public canExitStrategy()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public doCapture(Lcom/squareup/payment/BillPayment;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 44
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->isStoreAndForward()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOnlineStrategy;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/payment/BackgroundCaptor;->addCapture(Lcom/squareup/payment/RequiresAuthorization;Z)V

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPaymentOnlineStrategy;->enqueueFlushedTenderTasks(Lcom/squareup/payment/BillPayment;)V

    return-void

    .line 45
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot perform online capture with offline tenders"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public enqueueAttachContactTask(Lcom/squareup/payment/tender/BaseTender;)V
    .locals 0

    .line 60
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->enqueueAttachContactTask()V

    return-void
.end method

.method public enqueueLastReceipt(Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;)V
    .locals 0

    .line 53
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->isStoreAndForward()Z

    move-result p1

    if-nez p1, :cond_0

    .line 56
    invoke-virtual {p2}, Lcom/squareup/payment/tender/BaseTender;->enqueueReceipt()V

    return-void

    .line 54
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot enqueue online receipt with offline tenders"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public hasOfflineTenders()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onDropTender(Lcom/squareup/payment/tender/BaseTender;)V
    .locals 0

    return-void
.end method
