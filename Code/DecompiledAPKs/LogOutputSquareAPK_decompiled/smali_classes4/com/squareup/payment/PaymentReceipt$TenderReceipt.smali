.class public final Lcom/squareup/payment/PaymentReceipt$TenderReceipt;
.super Lcom/squareup/payment/PaymentReceipt;
.source "PaymentReceipt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/PaymentReceipt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TenderReceipt"
.end annotation


# instance fields
.field private final payment:Lcom/squareup/payment/BillPayment;

.field private final tender:Lcom/squareup/payment/tender/BaseTender;


# direct methods
.method private constructor <init>(Lcom/squareup/payment/PaymentReceipt$Builder;)V
    .locals 1

    const/4 v0, 0x0

    .line 354
    invoke-direct {p0, p1, v0}, Lcom/squareup/payment/PaymentReceipt;-><init>(Lcom/squareup/payment/PaymentReceipt$Builder;Lcom/squareup/payment/PaymentReceipt$1;)V

    .line 355
    invoke-static {p1}, Lcom/squareup/payment/PaymentReceipt$Builder;->access$500(Lcom/squareup/payment/PaymentReceipt$Builder;)Lcom/squareup/payment/Payment;

    move-result-object p1

    check-cast p1, Lcom/squareup/payment/BillPayment;

    iput-object p1, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->payment:Lcom/squareup/payment/BillPayment;

    .line 356
    iget-object p1, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->payment:Lcom/squareup/payment/BillPayment;

    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/PaymentReceipt$Builder;Lcom/squareup/payment/PaymentReceipt$1;)V
    .locals 0

    .line 348
    invoke-direct {p0, p1}, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;-><init>(Lcom/squareup/payment/PaymentReceipt$Builder;)V

    return-void
.end method


# virtual methods
.method public asReturnsChange()Lcom/squareup/payment/tender/BaseTender$ReturnsChange;
    .locals 2

    .line 397
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    instance-of v1, v0, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public decline()V
    .locals 2

    .line 360
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->declineReceipt()V

    .line 361
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->payment:Lcom/squareup/payment/BillPayment;

    iget-object v1, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/BillPayment;->enqueueReceiptIfComplete(Lcom/squareup/payment/tender/BaseTender;)V

    return-void
.end method

.method public email(Ljava/lang/String;)V
    .locals 2

    .line 375
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    const-string v1, "email"

    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/BaseTender;->emailReceipt(Ljava/lang/String;)V

    .line 376
    iget-object p1, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->payment:Lcom/squareup/payment/BillPayment;

    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {p1, v0}, Lcom/squareup/payment/BillPayment;->enqueueReceiptIfComplete(Lcom/squareup/payment/tender/BaseTender;)V

    return-void
.end method

.method public emailDefault()V
    .locals 2

    .line 380
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {p0}, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->defaultEmailId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/tender/BaseTender;->emailReceiptById(Ljava/lang/String;)V

    .line 381
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->payment:Lcom/squareup/payment/BillPayment;

    iget-object v1, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/BillPayment;->enqueueReceiptIfComplete(Lcom/squareup/payment/tender/BaseTender;)V

    return-void
.end method

.method public getRemainingAmountDue()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 415
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->payment:Lcom/squareup/payment/BillPayment;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->payment:Lcom/squareup/payment/BillPayment;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getRemainingBalance()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 409
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getRemainingBalanceMinusTip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getRemainingBalanceText(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 401
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/BaseTender;->getRemainingBalanceTextForReceipt(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getRemainingBalanceTextForHud()Ljava/lang/String;
    .locals 1

    .line 405
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getRemainingBalanceTextForHud()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTenderAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 419
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getTenderIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 423
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    return-object v0
.end method

.method public getTenderType()Lcom/squareup/protos/client/bills/Tender$Type;
    .locals 1

    .line 427
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getTenderType()Lcom/squareup/protos/client/bills/Tender$Type;

    move-result-object v0

    return-object v0
.end method

.method public isCard()Z
    .locals 2

    .line 392
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->payment:Lcom/squareup/payment/BillPayment;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    .line 393
    instance-of v0, v0, Lcom/squareup/payment/tender/BaseCardTender;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->payment:Lcom/squareup/payment/BillPayment;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getCard()Lcom/squareup/Card;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public isReceiptDeferred()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public sms(Ljava/lang/String;)V
    .locals 2

    .line 365
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    const-string v1, "phone"

    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/BaseTender;->smsReceipt(Ljava/lang/String;)V

    .line 366
    iget-object p1, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->payment:Lcom/squareup/payment/BillPayment;

    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {p1, v0}, Lcom/squareup/payment/BillPayment;->enqueueReceiptIfComplete(Lcom/squareup/payment/tender/BaseTender;)V

    return-void
.end method

.method public smsDefault()V
    .locals 2

    .line 370
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {p0}, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->defaultSmsId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/tender/BaseTender;->smsReceiptById(Ljava/lang/String;)V

    .line 371
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->payment:Lcom/squareup/payment/BillPayment;

    iget-object v1, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/BillPayment;->enqueueReceiptIfComplete(Lcom/squareup/payment/tender/BaseTender;)V

    return-void
.end method

.method public updateBuyerLanguage(Ljava/util/Locale;)V
    .locals 1

    .line 431
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/BaseTender;->setBuyerSelectedLanguage(Ljava/util/Locale;)V

    return-void
.end method
