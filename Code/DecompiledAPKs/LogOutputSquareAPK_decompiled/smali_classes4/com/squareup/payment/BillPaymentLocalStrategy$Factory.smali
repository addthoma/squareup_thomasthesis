.class Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;
.super Ljava/lang/Object;
.source "BillPaymentLocalStrategy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/BillPaymentLocalStrategy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Factory"
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final localPaymentsQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final merchantToken:Ljava/lang/String;

.field private final taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;


# direct methods
.method constructor <init>(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/retrofit/RetrofitQueue;Ljava/lang/String;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    iput-object p1, p0, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 186
    iput-object p2, p0, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;->localPaymentsQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 187
    iput-object p3, p0, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;->merchantToken:Ljava/lang/String;

    .line 188
    iput-object p4, p0, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 0

    .line 175
    iget-object p0, p0, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 0

    .line 175
    iget-object p0, p0, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;->localPaymentsQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;)Ljava/lang/String;
    .locals 0

    .line 175
    iget-object p0, p0, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;->merchantToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 175
    iget-object p0, p0, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method


# virtual methods
.method create()Lcom/squareup/payment/BillPaymentLocalStrategy;
    .locals 2

    .line 192
    new-instance v0, Lcom/squareup/payment/BillPaymentLocalStrategy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/BillPaymentLocalStrategy;-><init>(Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;Lcom/squareup/payment/BillPaymentLocalStrategy$1;)V

    return-object v0
.end method
