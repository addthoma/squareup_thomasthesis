.class public Lcom/squareup/payment/AutoVoidStatusBarNotifier;
.super Ljava/lang/Object;
.source "AutoVoidStatusBarNotifier.java"

# interfaces
.implements Lcom/squareup/notifications/AutoVoidNotifier;


# instance fields
.field private final context:Landroid/content/Context;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationManager:Landroid/app/NotificationManager;

.field private final notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Landroid/app/Application;Lcom/squareup/notification/NotificationWrapper;Landroid/app/NotificationManager;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/notification/NotificationWrapper;",
            "Landroid/app/NotificationManager;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->context:Landroid/content/Context;

    .line 33
    iput-object p2, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    .line 34
    iput-object p3, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->notificationManager:Landroid/app/NotificationManager;

    .line 35
    iput-object p4, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->res:Lcom/squareup/util/Res;

    .line 36
    iput-object p5, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method private showNotification(ILcom/squareup/protos/common/Money;)V
    .locals 3

    .line 66
    iget-object v0, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->res:Lcom/squareup/util/Res;

    .line 67
    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v0, "amount"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 69
    iget-object p2, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->context:Landroid/content/Context;

    invoke-static {p2}, Lcom/squareup/ui/PaymentActivity;->createPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object p2

    .line 71
    iget-object v0, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    iget-object v1, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->context:Landroid/content/Context;

    sget-object v2, Lcom/squareup/notification/Channels;->PAYMENTS:Lcom/squareup/notification/Channels;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/notification/NotificationWrapper;->getNotificationBuilder(Landroid/content/Context;Lcom/squareup/notification/Channel;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x2

    .line 72
    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setPriority(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/notification/R$drawable;->notification_square_error:I

    .line 73
    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setSmallIcon(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/payment/notifiers/impl/R$string;->auto_void_title:I

    .line 74
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/payment/notifiers/impl/R$string;->auto_void_title:I

    .line 75
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    .line 76
    invoke-virtual {v0, p1}, Landroidx/core/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    new-instance v1, Landroidx/core/app/NotificationCompat$BigTextStyle;

    invoke-direct {v1}, Landroidx/core/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 77
    invoke-virtual {v1, p1}, Landroidx/core/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$BigTextStyle;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroidx/core/app/NotificationCompat$Builder;->setStyle(Landroidx/core/app/NotificationCompat$Style;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    .line 78
    invoke-virtual {p1, p2}, Landroidx/core/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    .line 79
    invoke-virtual {p1}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    .line 81
    iget-object p2, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->notificationManager:Landroid/app/NotificationManager;

    sget v0, Lcom/squareup/payment/notifiers/impl/R$id;->notification_auto_void:I

    invoke-virtual {p2, v0, p1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method


# virtual methods
.method public notifyDanglingAuthVoidedAfterCrash(Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 40
    sget v0, Lcom/squareup/payment/notifiers/impl/R$string;->auto_void_body_crash:I

    invoke-direct {p0, v0, p1}, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->showNotification(ILcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public notifyDanglingMiryo()V
    .locals 5

    .line 49
    iget-object v0, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/payment/notifiers/impl/R$string;->dangling_miryo_payment:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/squareup/ui/PaymentActivity;->createPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 52
    iget-object v2, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    iget-object v3, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->context:Landroid/content/Context;

    sget-object v4, Lcom/squareup/notification/Channels;->PAYMENTS:Lcom/squareup/notification/Channels;

    invoke-virtual {v2, v3, v4}, Lcom/squareup/notification/NotificationWrapper;->getNotificationBuilder(Landroid/content/Context;Lcom/squareup/notification/Channel;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v2

    const/4 v3, 0x2

    .line 53
    invoke-virtual {v2, v3}, Landroidx/core/app/NotificationCompat$Builder;->setPriority(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v2

    sget v3, Lcom/squareup/notification/R$drawable;->notification_square_error:I

    .line 54
    invoke-virtual {v2, v3}, Landroidx/core/app/NotificationCompat$Builder;->setSmallIcon(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/payment/notifiers/impl/R$string;->auto_void_title:I

    .line 55
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroidx/core/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/payment/notifiers/impl/R$string;->auto_void_title:I

    .line 56
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroidx/core/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v2

    .line 57
    invoke-virtual {v2, v0}, Landroidx/core/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v2

    new-instance v3, Landroidx/core/app/NotificationCompat$BigTextStyle;

    invoke-direct {v3}, Landroidx/core/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 58
    invoke-virtual {v3, v0}, Landroidx/core/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$BigTextStyle;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroidx/core/app/NotificationCompat$Builder;->setStyle(Landroidx/core/app/NotificationCompat$Style;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    .line 59
    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->notificationManager:Landroid/app/NotificationManager;

    sget v2, Lcom/squareup/payment/notifiers/impl/R$id;->notification_auto_void:I

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method public notifyVoidOnTimeout(Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 44
    sget v0, Lcom/squareup/payment/notifiers/impl/R$string;->auto_void_body_timeout:I

    invoke-direct {p0, v0, p1}, Lcom/squareup/payment/AutoVoidStatusBarNotifier;->showNotification(ILcom/squareup/protos/common/Money;)V

    return-void
.end method
