.class public Lcom/squareup/payment/Transaction;
.super Ljava/lang/Object;
.source "Transaction.java"

# interfaces
.implements Lcom/squareup/badbus/BadBusRegistrant;
.implements Lcom/squareup/payment/crm/HoldsCustomer;
.implements Lcom/squareup/payment/ReceiptForLastPayment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;,
        Lcom/squareup/payment/Transaction$Change;,
        Lcom/squareup/payment/Transaction$TransactionBundler;
    }
.end annotation


# static fields
.field private static final API_CLIENT_ID_BUNDLE_KEY:Ljava/lang/String; = "API_CLIENT_ID"

.field private static final HAD_PAYMENT:Ljava/lang/String; = "HAD_PAYMENT"

.field private static final HAS_INVOICE_PAYMENT:Ljava/lang/String; = "HAS_INVOICE_PAYMENT"

.field private static final RECEIPT_STRATEGY_BUNDLE_KEY:Ljava/lang/String; = "SKIP_RECEIPT_SCREEN_STRATEGY"

.field private static final SKIP_SIGNATURE_BUNDLE_KEY:Ljava/lang/String; = "SKIP_SIGNATURE"

.field private static final TICKET_PROTO_KEY:Ljava/lang/String; = "TICKET_PROTO_KEY"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private apiClientId:Ljava/lang/String;

.field private final availableDiscountsStore:Lcom/squareup/payment/AvailableDiscountsStore;

.field private final billPaymentFactory:Lcom/squareup/payment/BillPayment$Factory;

.field private final bus:Lcom/squareup/badbus/BadEventSink;

.field private canStillNameCart:Z

.field private cartChanges:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/payment/OrderEntryEvents$CartChanged;",
            ">;"
        }
    .end annotation
.end field

.field private final currency:Lcom/squareup/protos/common/CurrencyCode;

.field private currentTicket:Lcom/squareup/tickets/OpenTicket;

.field private delayCapture:Z

.field private final diningOptionCache:Lcom/squareup/ui/main/DiningOptionCache;

.field final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;

.field private ignoreTransactionLimits:Z

.field private final invoicePaymentFactory:Lcom/squareup/payment/InvoicePayment$Factory;

.field private final isReaderSdk:Z

.field private lastCapturedAmount:Lcom/squareup/protos/common/Money;

.field private lostPayment:Z

.field private final mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private networkRequestsModifier:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

.field private nextAvailableDiscounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;"
        }
    .end annotation
.end field

.field private nextAvailableTaxRules:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;"
        }
    .end annotation
.end field

.field private nextAvailableTaxes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end field

.field private final nextDiscountsKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;>;"
        }
    .end annotation
.end field

.field private final nextTaxRulesKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;>;"
        }
    .end annotation
.end field

.field private final nextTaxesKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;>;"
        }
    .end annotation
.end field

.field private numSplitsTotal:J

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

.field private order:Lcom/squareup/payment/Order;

.field private orderCreated:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/payment/Order;",
            ">;"
        }
    .end annotation
.end field

.field private final orderCustomerChanged:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation
.end field

.field private final orderKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/payment/Order;",
            ">;"
        }
    .end annotation
.end field

.field private paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

.field private final paymentFlowTaskProviderFactory:Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;

.field private paymentInFlight:Lcom/squareup/payment/Payment;

.field private receiptForLastPayment:Lcom/squareup/payment/PaymentReceipt;

.field private receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

.field final res:Lcom/squareup/util/Res;

.field private final retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private shouldPreserveCustomerOnNextReset:Z

.field private skipSignature:Z

.field private startedFromChargeButton:Z

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final tickets:Lcom/squareup/tickets/Tickets;

.field private final ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

.field private final tipSettingsKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/settings/server/TipSettings;",
            ">;"
        }
    .end annotation
.end field

.field private tipSettingsOverride:Lcom/squareup/settings/server/TipSettings;

.field private final transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

.field private final userId:Ljava/lang/String;

.field final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method constructor <init>(Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/protos/common/CurrencyCode;Ljava/lang/String;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/BillPayment$Factory;Lcom/squareup/payment/InvoicePayment$Factory;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/tickets/Tickets;Lcom/squareup/util/Res;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/ui/main/DiningOptionCache;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;ZLcom/squareup/BundleKey;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/payment/AvailableDiscountsStore;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;Lcom/squareup/settings/server/Features;)V
    .locals 3
    .param p25    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/payment/Order;",
            ">;",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;>;",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;>;",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;>;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Ljava/lang/String;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/payment/BillPayment$Factory;",
            "Lcom/squareup/payment/InvoicePayment$Factory;",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            "Lcom/squareup/tickets/Tickets;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            "Lcom/squareup/log/OhSnapLogger;",
            "Lcom/squareup/ui/main/DiningOptionCache;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Z",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/settings/server/TipSettings;",
            ">;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/payment/AvailableDiscountsStore;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v1, 0x0

    .line 307
    iput-wide v1, v0, Lcom/squareup/payment/Transaction;->numSplitsTotal:J

    .line 325
    sget-object v1, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->READ_FROM_PAYMENT:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    iput-object v1, v0, Lcom/squareup/payment/Transaction;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    .line 347
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/payment/Transaction;->orderCustomerChanged:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 353
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/payment/Transaction;->nextAvailableTaxes:Ljava/util/Map;

    .line 362
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/payment/Transaction;->nextAvailableTaxRules:Ljava/util/List;

    .line 368
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/payment/Transaction;->nextAvailableDiscounts:Ljava/util/List;

    const/4 v1, 0x1

    .line 383
    iput-boolean v1, v0, Lcom/squareup/payment/Transaction;->canStillNameCart:Z

    const/4 v1, 0x0

    .line 386
    iput-boolean v1, v0, Lcom/squareup/payment/Transaction;->shouldPreserveCustomerOnNextReset:Z

    .line 391
    iput-boolean v1, v0, Lcom/squareup/payment/Transaction;->ignoreTransactionLimits:Z

    .line 399
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/payment/Transaction;->cartChanges:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 400
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/payment/Transaction;->orderCreated:Lcom/jakewharton/rxrelay/PublishRelay;

    move-object v1, p1

    .line 417
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->orderKey:Lcom/squareup/BundleKey;

    move-object/from16 v1, p22

    .line 418
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->tipSettingsKey:Lcom/squareup/BundleKey;

    move-object v1, p2

    .line 419
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->nextTaxesKey:Lcom/squareup/BundleKey;

    move-object v1, p3

    .line 420
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->nextTaxRulesKey:Lcom/squareup/BundleKey;

    move-object v1, p4

    .line 421
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->nextDiscountsKey:Lcom/squareup/BundleKey;

    move-object v1, p6

    .line 422
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->currency:Lcom/squareup/protos/common/CurrencyCode;

    move-object v1, p5

    .line 423
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->bus:Lcom/squareup/badbus/BadEventSink;

    move-object/from16 v1, p25

    .line 424
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-object v1, p8

    .line 425
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p11

    .line 426
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    move-object v1, p7

    .line 427
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->userId:Ljava/lang/String;

    move-object v1, p9

    .line 428
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->billPaymentFactory:Lcom/squareup/payment/BillPayment$Factory;

    move-object v1, p10

    .line 429
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->invoicePaymentFactory:Lcom/squareup/payment/InvoicePayment$Factory;

    move-object v1, p12

    .line 430
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->tickets:Lcom/squareup/tickets/Tickets;

    move-object/from16 v1, p13

    .line 431
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->res:Lcom/squareup/util/Res;

    move-object/from16 v1, p14

    .line 432
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    move-object/from16 v1, p15

    .line 433
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    move-object/from16 v1, p16

    .line 434
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->diningOptionCache:Lcom/squareup/ui/main/DiningOptionCache;

    move-object/from16 v1, p17

    .line 435
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->analytics:Lcom/squareup/analytics/Analytics;

    move-object/from16 v1, p18

    .line 436
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    move-object/from16 v1, p19

    .line 437
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    move-object/from16 v1, p20

    .line 438
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    move/from16 v1, p21

    .line 439
    iput-boolean v1, v0, Lcom/squareup/payment/Transaction;->isReaderSdk:Z

    move-object/from16 v1, p23

    .line 440
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-object/from16 v1, p24

    .line 441
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->availableDiscountsStore:Lcom/squareup/payment/AvailableDiscountsStore;

    move-object/from16 v1, p26

    .line 442
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->paymentFlowTaskProviderFactory:Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;

    move-object/from16 v1, p27

    .line 443
    iput-object v1, v0, Lcom/squareup/payment/Transaction;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/Order;
    .locals 0

    .line 188
    iget-object p0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    return-object p0
.end method

.method static synthetic access$002(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    return-object p1
.end method

.method static synthetic access$100(Lcom/squareup/payment/Transaction;)Lcom/squareup/BundleKey;
    .locals 0

    .line 188
    iget-object p0, p0, Lcom/squareup/payment/Transaction;->orderKey:Lcom/squareup/BundleKey;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/payment/Transaction;)Ljava/util/List;
    .locals 0

    .line 188
    iget-object p0, p0, Lcom/squareup/payment/Transaction;->nextAvailableTaxRules:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$1002(Lcom/squareup/payment/Transaction;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/payment/Transaction;->nextAvailableTaxRules:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/squareup/payment/Transaction;)Lcom/squareup/BundleKey;
    .locals 0

    .line 188
    iget-object p0, p0, Lcom/squareup/payment/Transaction;->nextTaxRulesKey:Lcom/squareup/BundleKey;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/payment/Transaction;)Ljava/util/List;
    .locals 0

    .line 188
    iget-object p0, p0, Lcom/squareup/payment/Transaction;->nextAvailableDiscounts:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$1202(Lcom/squareup/payment/Transaction;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/payment/Transaction;->nextAvailableDiscounts:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/squareup/payment/Transaction;)Lcom/squareup/BundleKey;
    .locals 0

    .line 188
    iget-object p0, p0, Lcom/squareup/payment/Transaction;->nextDiscountsKey:Lcom/squareup/BundleKey;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/Payment;
    .locals 0

    .line 188
    iget-object p0, p0, Lcom/squareup/payment/Transaction;->paymentInFlight:Lcom/squareup/payment/Payment;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/squareup/payment/Transaction;)Lcom/squareup/tickets/OpenTicket;
    .locals 0

    .line 188
    iget-object p0, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    return-object p0
.end method

.method static synthetic access$1502(Lcom/squareup/payment/Transaction;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/OpenTicket;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    return-object p1
.end method

.method static synthetic access$1602(Lcom/squareup/payment/Transaction;Z)Z
    .locals 0

    .line 188
    iput-boolean p1, p0, Lcom/squareup/payment/Transaction;->lostPayment:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/squareup/payment/Transaction;)V
    .locals 0

    .line 188
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->verifyUser()V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/payment/Transaction;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 188
    iget-object p0, p0, Lcom/squareup/payment/Transaction;->orderCreated:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/payment/Transaction;)Lcom/squareup/settings/server/TipSettings;
    .locals 0

    .line 188
    iget-object p0, p0, Lcom/squareup/payment/Transaction;->tipSettingsOverride:Lcom/squareup/settings/server/TipSettings;

    return-object p0
.end method

.method static synthetic access$302(Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/TipSettings;)Lcom/squareup/settings/server/TipSettings;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/payment/Transaction;->tipSettingsOverride:Lcom/squareup/settings/server/TipSettings;

    return-object p1
.end method

.method static synthetic access$400(Lcom/squareup/payment/Transaction;)Lcom/squareup/BundleKey;
    .locals 0

    .line 188
    iget-object p0, p0, Lcom/squareup/payment/Transaction;->tipSettingsKey:Lcom/squareup/BundleKey;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;
    .locals 0

    .line 188
    iget-object p0, p0, Lcom/squareup/payment/Transaction;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    return-object p0
.end method

.method static synthetic access$502(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;)Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/payment/Transaction;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    return-object p1
.end method

.method static synthetic access$600(Lcom/squareup/payment/Transaction;)Z
    .locals 0

    .line 188
    iget-boolean p0, p0, Lcom/squareup/payment/Transaction;->skipSignature:Z

    return p0
.end method

.method static synthetic access$602(Lcom/squareup/payment/Transaction;Z)Z
    .locals 0

    .line 188
    iput-boolean p1, p0, Lcom/squareup/payment/Transaction;->skipSignature:Z

    return p1
.end method

.method static synthetic access$700(Lcom/squareup/payment/Transaction;)Ljava/lang/String;
    .locals 0

    .line 188
    iget-object p0, p0, Lcom/squareup/payment/Transaction;->apiClientId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$702(Lcom/squareup/payment/Transaction;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/payment/Transaction;->apiClientId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/squareup/payment/Transaction;)Ljava/util/Map;
    .locals 0

    .line 188
    iget-object p0, p0, Lcom/squareup/payment/Transaction;->nextAvailableTaxes:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$802(Lcom/squareup/payment/Transaction;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/payment/Transaction;->nextAvailableTaxes:Ljava/util/Map;

    return-object p1
.end method

.method static synthetic access$900(Lcom/squareup/payment/Transaction;)Lcom/squareup/BundleKey;
    .locals 0

    .line 188
    iget-object p0, p0, Lcom/squareup/payment/Transaction;->nextTaxesKey:Lcom/squareup/BundleKey;

    return-object p0
.end method

.method private assertSameUser()V
    .locals 4

    .line 2222
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    .line 2227
    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getUserId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/Transaction;->userId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 2228
    :cond_0
    new-instance v1, Ljava/lang/AssertionError;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2229
    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getUserId()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/squareup/payment/Transaction;->userId:Ljava/lang/String;

    aput-object v3, v2, v0

    const-string v0, "Wrong user, expected %s, got %s"

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private beginTransactionIfCartEmpty()V
    .locals 1

    .line 1053
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1054
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    invoke-interface {v0}, Lcom/squareup/ui/main/TransactionMetrics;->beginTransaction()V

    :cond_0
    return-void
.end method

.method private buildKeypadItem()Lcom/squareup/checkout/CartItem;
    .locals 5

    .line 2191
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->currency:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v1, 0x0

    .line 2192
    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/Transaction;->nextAvailableTaxes:Ljava/util/Map;

    .line 2194
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getAvailableTaxRules()Ljava/util/List;

    move-result-object v2

    .line 2195
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v3

    .line 2196
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getCurrentDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v4

    .line 2191
    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/payment/CustomAmountItemsKt;->buildCustomAmountItem(Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/CartItem;

    move-result-object v0

    return-object v0
.end method

.method private buildOrderForTicketWithoutLockingItems(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/payment/Order;
    .locals 11

    .line 2594
    iget-object v1, p0, Lcom/squareup/payment/Transaction;->userId:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/payment/Transaction;->currency:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/payment/Transaction;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 2595
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getRoundingType()Lcom/squareup/calc/constants/RoundingType;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/payment/Transaction;->nextAvailableTaxes:Ljava/util/Map;

    iget-object v5, p0, Lcom/squareup/payment/Transaction;->nextAvailableTaxRules:Ljava/util/List;

    iget-object v0, p0, Lcom/squareup/payment/Transaction;->diningOptionCache:Lcom/squareup/ui/main/DiningOptionCache;

    .line 2596
    invoke-virtual {v0}, Lcom/squareup/ui/main/DiningOptionCache;->getDefaultDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v6

    iget-object v7, p0, Lcom/squareup/payment/Transaction;->res:Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/payment/Transaction;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 2597
    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidEnabled()Z

    move-result v8

    iget-object v0, p0, Lcom/squareup/payment/Transaction;->availableDiscountsStore:Lcom/squareup/payment/AvailableDiscountsStore;

    invoke-virtual {v0}, Lcom/squareup/payment/AvailableDiscountsStore;->isDiscountApplicationIdEnabled()Z

    move-result v9

    const/4 v10, 0x0

    move-object v0, p1

    .line 2594
    invoke-static/range {v0 .. v10}, Lcom/squareup/payment/OrderProtoConversions;->forTicketFromCartProto(Lcom/squareup/tickets/OpenTicket;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/calc/constants/RoundingType;Ljava/util/Map;Ljava/util/List;Lcom/squareup/checkout/DiningOption;Lcom/squareup/util/Res;ZZZ)Lcom/squareup/payment/Order;

    move-result-object p1

    return-object p1
.end method

.method private canAddToLastItem(Lcom/squareup/checkout/CartItem;)Z
    .locals 1

    .line 1094
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1095
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 1096
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private clearPaymentInFlight()V
    .locals 1

    .line 1826
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-static {v0}, Lcom/squareup/util/ThreadEnforcerExtensionsKt;->logIfNotThread(Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    const/4 v0, 0x0

    .line 1827
    iput-object v0, p0, Lcom/squareup/payment/Transaction;->paymentInFlight:Lcom/squareup/payment/Payment;

    .line 1828
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->reset()V

    return-void
.end method

.method private concludeReset()V
    .locals 2

    .line 1810
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->buildKeypadItem()Lcom/squareup/checkout/CartItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order;->pushItem(Lcom/squareup/checkout/CartItem;)V

    const/4 v0, 0x0

    .line 1811
    invoke-direct {p0, v0}, Lcom/squareup/payment/Transaction;->postEverythingChanged(Z)V

    .line 1812
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->orderCustomerChanged:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->onCustomerChanged()Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private doCommitKeypadItem()Z
    .locals 1

    .line 1073
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasEmptyKeypadItem()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 1075
    :cond_0
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->buildKeypadItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/payment/Transaction;->pushItem(Lcom/squareup/checkout/CartItem;)V

    const/4 v0, 0x1

    return v0
.end method

.method private eligibleForReset()Z
    .locals 1

    .line 1864
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private getKeypadPriceAmount()J
    .locals 2

    .line 852
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->peekItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method private getOrderWithoutKeypadItem()Lcom/squareup/payment/Order;
    .locals 1

    .line 2702
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasEmptyKeypadItem()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->popItem()Lcom/squareup/checkout/CartItem;

    .line 2703
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    return-object v0
.end method

.method private getPaymentConfig()Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;
    .locals 6

    .line 1988
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    if-nez v0, :cond_0

    .line 1989
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 1990
    new-instance v1, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    iget-object v5, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 1991
    invoke-static {v3, v4, v5}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3, v0}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    iput-object v1, p0, Lcom/squareup/payment/Transaction;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    .line 1994
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    return-object v0
.end method

.method private hasTaxableItems()Z
    .locals 3

    .line 813
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    .line 814
    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 815
    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private hasTipSettingsOverride()Z
    .locals 1

    .line 921
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->tipSettingsOverride:Lcom/squareup/settings/server/TipSettings;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isDiscountApplicationIdEnabled()Z
    .locals 1

    .line 2753
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->availableDiscountsStore:Lcom/squareup/payment/AvailableDiscountsStore;

    invoke-virtual {v0}, Lcom/squareup/payment/AvailableDiscountsStore;->isDiscountApplicationIdEnabled()Z

    move-result v0

    return v0
.end method

.method private isTaxDisabledForItem(Lcom/squareup/checkout/CartItem;Lcom/squareup/checkout/Tax;)Z
    .locals 2

    .line 1579
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isCustomItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1580
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getAvailableTaxRules()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getCurrentDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    invoke-static {p2, p1, v0}, Lcom/squareup/payment/TransactionTaxesKt;->isDisabledForCustomItem(Lcom/squareup/checkout/Tax;Ljava/util/List;Lcom/squareup/checkout/DiningOption;)Z

    move-result p1

    return p1

    .line 1582
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getAvailableTaxRules()Ljava/util/List;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    .line 1583
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getCurrentDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v1

    .line 1582
    invoke-static {p2, v0, p1, v1}, Lcom/squareup/payment/TransactionTaxesKt;->isDisabledForNonCustomItem(Lcom/squareup/checkout/Tax;Ljava/util/List;Ljava/lang/String;Lcom/squareup/checkout/DiningOption;)Z

    move-result p1

    return p1
.end method

.method static synthetic lambda$startAppointmentsTransaction$1(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;Lcom/squareup/payment/Order;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 943
    invoke-virtual {p1, p0}, Lcom/squareup/payment/Order;->setAppointmentDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;)V

    return-void
.end method

.method static synthetic lambda$startInvoiceTransaction$0(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;Lcom/squareup/payment/Order;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 937
    invoke-virtual {p1, p0}, Lcom/squareup/payment/Order;->setInvoiceDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;)V

    return-void
.end method

.method private peekItem()Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 2200
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->peekItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    return-object v0
.end method

.method private popItem()Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 2204
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->popItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    return-object v0
.end method

.method private postAddedItem(ZLcom/squareup/checkout/CartItem;)V
    .locals 0

    .line 2250
    invoke-static {p1, p2}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemAdded(ZLcom/squareup/checkout/CartItem;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    return-void
.end method

.method private postAddedOrEditedItem(Lcom/squareup/checkout/CartItem;Lcom/squareup/checkout/CartItem;Z)V
    .locals 0

    .line 2262
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2263
    invoke-direct {p0, p3, p2}, Lcom/squareup/payment/Transaction;->postAddedItem(ZLcom/squareup/checkout/CartItem;)V

    goto :goto_0

    .line 2265
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/payment/Transaction;->postEditedItem(Lcom/squareup/checkout/CartItem;)V

    :goto_0
    return-void
.end method

.method private postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 5

    .line 2274
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->invalidate()V

    .line 2279
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/32 v2, 0x7fffffff

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2280
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->WARNING:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v1, "Cart exceeds Integer.MAX_VALUE, resetting"

    invoke-interface {p1, v0, v1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 2281
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->reset()V

    return-void

    .line 2285
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->bus:Lcom/squareup/badbus/BadEventSink;

    invoke-interface {v0, p1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    .line 2286
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->cartChanges:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private postDiningOptionChanged()V
    .locals 1

    .line 2242
    invoke-static {}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->diningOptionChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    return-void
.end method

.method private postEditedItem(Lcom/squareup/checkout/CartItem;)V
    .locals 0

    .line 2254
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemEdited(Lcom/squareup/checkout/CartItem;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    return-void
.end method

.method private postEverythingChanged(Z)V
    .locals 0

    .line 2234
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->everythingChanged(Z)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    return-void
.end method

.method private postRemovedItem(Lcom/squareup/checkout/CartItem;)V
    .locals 0

    .line 2258
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemRemoved(Lcom/squareup/checkout/CartItem;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    return-void
.end method

.method private postReturnChanged()V
    .locals 1

    .line 2270
    invoke-static {}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->returnChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    return-void
.end method

.method private prepareForReset()V
    .locals 1

    .line 1820
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->clearPaymentInFlight()V

    const/4 v0, 0x0

    .line 1821
    iput-boolean v0, p0, Lcom/squareup/payment/Transaction;->lostPayment:Z

    const/4 v0, 0x0

    .line 1822
    iput-object v0, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    return-void
.end method

.method private pushItem(Lcom/squareup/checkout/CartItem;)V
    .locals 1

    .line 2208
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->pushItem(Lcom/squareup/checkout/CartItem;)V

    return-void
.end method

.method private resetAndCreateOrderFromCart(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/rolodex/Contact;Lio/reactivex/functions/Consumer;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Lio/reactivex/functions/Consumer<",
            "Lcom/squareup/payment/Order;",
            ">;)V"
        }
    .end annotation

    .line 977
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 978
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->updateCurrentTicketBeforeReset()V

    .line 982
    :cond_0
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->prepareForReset()V

    .line 984
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 986
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getRoundingType()Lcom/squareup/calc/constants/RoundingType;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/Transaction;->res:Lcom/squareup/util/Res;

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/squareup/payment/Transaction;->userId:Ljava/lang/String;

    new-instance v7, Ljava/util/LinkedHashMap;

    invoke-direct {v7}, Ljava/util/LinkedHashMap;-><init>()V

    iget-object v0, p0, Lcom/squareup/payment/Transaction;->availableDiscountsStore:Lcom/squareup/payment/AvailableDiscountsStore;

    .line 992
    invoke-virtual {v0}, Lcom/squareup/payment/AvailableDiscountsStore;->isDiscountApplicationIdEnabled()Z

    move-result v8

    move-object v1, p1

    .line 984
    invoke-static/range {v1 .. v8}, Lcom/squareup/payment/OrderProtoConversions;->makeOrderFromCartProto(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/calc/constants/RoundingType;Lcom/squareup/util/Res;ZZLjava/lang/String;Ljava/util/Map;Z)Lcom/squareup/payment/Order;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    .line 995
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->orderCreated:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 996
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v0}, Lcom/squareup/payment/Order;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 998
    :try_start_0
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-interface {p3, p1}, Lio/reactivex/functions/Consumer;->accept(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1003
    :catch_0
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->concludeReset()V

    return-void
.end method

.method private resetUniqueKey()V
    .locals 1

    .line 1869
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->invalidate()V

    return-void
.end method

.method private returnAllUnusedCoupons()V
    .locals 6

    .line 1922
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getCart()Lcom/squareup/checkout/Cart;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getCart()Lcom/squareup/checkout/Cart;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart;->getItems()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 1926
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    const/4 v2, 0x0

    .line 1928
    iget-object v3, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v3}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/CartItem;

    .line 1929
    iget-object v4, v4, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    iget-object v5, v1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v2, 0x1

    :cond_3
    if-nez v2, :cond_1

    .line 1937
    invoke-direct {p0, v1}, Lcom/squareup/payment/Transaction;->returnCouponIfEligible(Lcom/squareup/checkout/Discount;)V

    goto :goto_0

    :cond_4
    :goto_1
    return-void
.end method

.method private returnCouponIfEligible(Lcom/squareup/checkout/Discount;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 1950
    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->isCoupon()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->LOYALTY:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->getCouponReason()Lcom/squareup/protos/client/coupons/Coupon$Reason;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/coupons/Coupon$Reason;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1951
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/queue/crm/ReturnCouponTask;

    invoke-direct {v1, p1}, Lcom/squareup/queue/crm/ReturnCouponTask;-><init>(Lcom/squareup/checkout/Discount;)V

    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private returnCouponIfEligible(Ljava/lang/String;)V
    .locals 1

    .line 1959
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/Discount;

    .line 1960
    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->returnCouponIfEligible(Lcom/squareup/checkout/Discount;)V

    return-void
.end method

.method private startBillPayment(Z)Lcom/squareup/payment/BillPayment;
    .locals 9

    .line 1976
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->assertSameUser()V

    .line 1977
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v3

    .line 1978
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->billPaymentFactory:Lcom/squareup/payment/BillPayment$Factory;

    .line 1979
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v2

    iget-object v4, p0, Lcom/squareup/payment/Transaction;->apiClientId:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/squareup/payment/Transaction;->delayCapture:Z

    iget-object v6, p0, Lcom/squareup/payment/Transaction;->networkRequestsModifier:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

    iget-object v1, p0, Lcom/squareup/payment/Transaction;->paymentFlowTaskProviderFactory:Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;

    .line 1981
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->getPaymentConfig()Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;->create(Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)Lcom/squareup/payment/PaymentFlowTaskProvider;

    move-result-object v7

    .line 1982
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->getPaymentConfig()Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    move-result-object v8

    move v1, p1

    .line 1979
    invoke-virtual/range {v0 .. v8}, Lcom/squareup/payment/BillPayment$Factory;->create(ZLcom/squareup/payment/Order;Lcom/squareup/settings/server/TipSettings;Ljava/lang/String;ZLcom/squareup/checkoutflow/services/NetworkRequestModifier;Lcom/squareup/payment/PaymentFlowTaskProvider;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)Lcom/squareup/payment/BillPayment;

    move-result-object p1

    .line 1983
    iput-object p1, p0, Lcom/squareup/payment/Transaction;->paymentInFlight:Lcom/squareup/payment/Payment;

    return-object p1
.end method

.method private verifyUser()V
    .locals 7

    .line 1877
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->userId:Ljava/lang/String;

    .line 1878
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getUserId()Ljava/lang/String;

    move-result-object v1

    .line 1879
    iget-object v2, p0, Lcom/squareup/payment/Transaction;->paymentInFlight:Lcom/squareup/payment/Payment;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    .line 1880
    :cond_0
    invoke-virtual {v2}, Lcom/squareup/payment/Payment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/OrderSnapshot;->getUserId()Ljava/lang/String;

    move-result-object v2

    .line 1883
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v2, :cond_1

    .line 1884
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    return-void

    :cond_2
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x2

    if-eqz v2, :cond_4

    .line 1890
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    goto :goto_1

    .line 1891
    :cond_3
    new-instance v0, Ljava/lang/AssertionError;

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v4

    aput-object v2, v5, v3

    const-string v1, "Inconsistent next (%s) and in-flight (%s)"

    .line 1892
    invoke-static {v1, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_4
    :goto_1
    new-array v2, v5, [Ljava/lang/Object;

    aput-object v1, v2, v4

    aput-object v0, v2, v3

    const-string v0, "User changed, abandoning this payment. User was %s, is %s"

    .line 1895
    invoke-static {v0, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1897
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->reset()V

    return-void
.end method


# virtual methods
.method public addCoupon(Lcom/squareup/protos/client/coupons/Coupon;Z)V
    .locals 2

    .line 1209
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    const-string v1, "coupon"

    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/payment/Order;->addCoupon(Lcom/squareup/protos/client/coupons/Coupon;Z)V

    const/4 p1, 0x1

    .line 1210
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged(Z)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    return-void
.end method

.method public addDiscount(Lcom/squareup/checkout/Discount;)V
    .locals 1

    const/4 v0, 0x0

    .line 1254
    invoke-virtual {p0, p1, v0}, Lcom/squareup/payment/Transaction;->addDiscount(Lcom/squareup/checkout/Discount;Lcom/squareup/protos/client/Employee;)V

    return-void
.end method

.method public addDiscount(Lcom/squareup/checkout/Discount;Lcom/squareup/protos/client/Employee;)V
    .locals 1

    .line 1258
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->beginTransactionIfCartEmpty()V

    .line 1259
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/squareup/payment/Order;->addDiscount(Lcom/squareup/checkout/Discount;Lcom/squareup/protos/client/Employee;)V

    .line 1260
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasExactlyOneDiscount()Z

    move-result p1

    const/4 p2, 0x1

    if-eqz p1, :cond_0

    .line 1261
    sget-object p1, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->ADD:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    invoke-static {p2, p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged(ZLcom/squareup/payment/OrderEntryEvents$ItemChangeType;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    goto :goto_0

    .line 1263
    :cond_0
    sget-object p1, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->EDIT:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    invoke-static {p2, p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged(ZLcom/squareup/payment/OrderEntryEvents$ItemChangeType;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    :goto_0
    return-void
.end method

.method public addOrderItem(Lcom/squareup/checkout/CartItem;)V
    .locals 5

    .line 1104
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->beginTransactionIfCartEmpty()V

    .line 1107
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->doCommitKeypadItem()Z

    .line 1110
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->popItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    .line 1111
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v1

    .line 1114
    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->canAddToLastItem(Lcom/squareup/checkout/CartItem;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1115
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->popItem()Lcom/squareup/checkout/CartItem;

    move-result-object v2

    .line 1116
    invoke-virtual {p1, v2}, Lcom/squareup/checkout/CartItem;->isSameLibraryItem(Lcom/squareup/checkout/CartItem;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1117
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    iget-object v3, p1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v1, v3}, Lcom/squareup/checkout/CartItem$Builder;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v1

    .line 1118
    iget-object v3, p0, Lcom/squareup/payment/Transaction;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v3}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v3

    .line 1119
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v4

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    iget-object v2, v2, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    .line 1120
    invoke-virtual {p1, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/squareup/checkout/CartItem$Builder;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/payment/Transaction;->res:Lcom/squareup/util/Res;

    .line 1124
    invoke-virtual {v3, v2}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProto(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/checkout/util/ItemizationEvents;->creationEvent(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/squareup/checkout/CartItem$Builder;->events(Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 1125
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    goto :goto_0

    .line 1127
    :cond_0
    invoke-direct {p0, v2}, Lcom/squareup/payment/Transaction;->pushItem(Lcom/squareup/checkout/CartItem;)V

    .line 1131
    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->pushItem(Lcom/squareup/checkout/CartItem;)V

    .line 1133
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1134
    iget-object v2, p0, Lcom/squareup/payment/Transaction;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v3, Lcom/squareup/analytics/RegisterActionName;->CHECKOUT_ADD_GIFT_CARD:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 1139
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v2

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-virtual {v2, p1}, Lcom/squareup/payment/Order;->resolveTaxDiscrepancies(Ljava/util/Map;)V

    .line 1140
    invoke-direct {p0, v0}, Lcom/squareup/payment/Transaction;->pushItem(Lcom/squareup/checkout/CartItem;)V

    .line 1142
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_CHECKOUT_ADD_ITEM_TO_CART:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    const/4 p1, 0x1

    .line 1143
    invoke-direct {p0, p1, v1}, Lcom/squareup/payment/Transaction;->postAddedItem(ZLcom/squareup/checkout/CartItem;)V

    return-void
.end method

.method public applyPricingRuleBlocks(Ljava/util/Map;Ljava/util/Map;Ljava/util/List;ZZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;",
            ">;>;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;",
            ">;ZZ)V"
        }
    .end annotation

    .line 1220
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->beginTransactionIfCartEmpty()V

    .line 1222
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/payment/Order;->applyPricingRuleBlocks(Ljava/util/Map;Ljava/util/Map;Ljava/util/List;ZZ)Lcom/squareup/payment/ApplyAutomaticDiscountsResult;

    move-result-object p1

    .line 1226
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasEmptyKeypadItem()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 1227
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->popItem()Lcom/squareup/checkout/CartItem;

    move-result-object p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 1230
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    .line 1231
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p4

    invoke-virtual {p4}, Lcom/squareup/payment/Order;->coalesceNewestUnsavedItems()V

    .line 1232
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p4

    invoke-virtual {p4}, Lcom/squareup/payment/Order;->coalesceAdjacentSplitItems()V

    .line 1233
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p4

    invoke-virtual {p4}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object p4

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result p4

    if-eqz p2, :cond_1

    .line 1236
    invoke-direct {p0, p2}, Lcom/squareup/payment/Transaction;->pushItem(Lcom/squareup/checkout/CartItem;)V

    .line 1239
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->getSplit()I

    move-result p2

    const/4 p5, 0x0

    const/4 v0, 0x1

    if-lez p2, :cond_2

    .line 1240
    sget-object p2, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->PRICING_RULE:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-static {v0, p2}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemsChanged(ZLcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    goto :goto_1

    :cond_2
    if-eq p3, p4, :cond_3

    .line 1242
    sget-object p2, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->PRICING_RULE:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-static {p5, p2}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemsChanged(ZLcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    .line 1244
    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->getAdded()I

    move-result p2

    if-lez p2, :cond_4

    invoke-virtual {p1}, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->getRemoved()I

    move-result p2

    if-lez p2, :cond_4

    .line 1245
    sget-object p1, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->EDIT:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    sget-object p2, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->PRICING_RULE:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-static {v0, p1, p2}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged(ZLcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    goto :goto_2

    .line 1246
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->getAdded()I

    move-result p2

    if-lez p2, :cond_5

    .line 1247
    sget-object p1, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->ADD:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    sget-object p2, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->PRICING_RULE:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-static {v0, p1, p2}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged(ZLcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    goto :goto_2

    .line 1248
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->getRemoved()I

    move-result p1

    if-lez p1, :cond_6

    .line 1249
    sget-object p1, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->DELETE:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    sget-object p2, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->PRICING_RULE:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-static {p5, p1, p2}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged(ZLcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    :cond_6
    :goto_2
    return-void
.end method

.method public asAuthPayment()Lcom/squareup/payment/RequiresAuthorization;
    .locals 1

    .line 2131
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->paymentInFlight:Lcom/squareup/payment/Payment;

    instance-of v0, v0, Lcom/squareup/payment/RequiresAuthorization;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requireAuthPayment()Lcom/squareup/payment/RequiresAuthorization;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public asBillPayment()Lcom/squareup/payment/BillPayment;
    .locals 1

    .line 2144
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->paymentInFlight:Lcom/squareup/payment/Payment;

    instance-of v0, v0, Lcom/squareup/payment/BillPayment;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public asInvoicePayment()Lcom/squareup/payment/InvoicePayment;
    .locals 1

    .line 2175
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->paymentInFlight:Lcom/squareup/payment/Payment;

    instance-of v0, v0, Lcom/squareup/payment/InvoicePayment;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requireInvoicePayment()Lcom/squareup/payment/InvoicePayment;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public assertNoCard()V
    .locals 2

    .line 1448
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasCard()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1449
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected card found in cart."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public assertNoPayment()V
    .locals 2

    .line 1454
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 1455
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected payment found in cart."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public attributeChargeIfPossible()V
    .locals 1

    .line 2561
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2564
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/payment/Transaction;->setEmployee(Lcom/squareup/permissions/EmployeeInfo;)V

    return-void
.end method

.method public buildCartProtoForTicket()Lcom/squareup/protos/client/bills/Cart;
    .locals 3

    .line 2609
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasEmptyKeypadItem()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->popItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 2611
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    .line 2612
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getGrandTotal()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/payment/Order;->getCartProtoForTicket(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart;

    move-result-object v1

    if-eqz v0, :cond_1

    .line 2615
    invoke-direct {p0, v0}, Lcom/squareup/payment/Transaction;->pushItem(Lcom/squareup/checkout/CartItem;)V

    :cond_1
    return-object v1
.end method

.method public buildOrderForTicket(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/payment/Order;
    .locals 10

    .line 2583
    iget-object v1, p0, Lcom/squareup/payment/Transaction;->userId:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/payment/Transaction;->currency:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/payment/Transaction;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 2584
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getRoundingType()Lcom/squareup/calc/constants/RoundingType;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/payment/Transaction;->nextAvailableTaxes:Ljava/util/Map;

    iget-object v5, p0, Lcom/squareup/payment/Transaction;->nextAvailableTaxRules:Ljava/util/List;

    iget-object v0, p0, Lcom/squareup/payment/Transaction;->diningOptionCache:Lcom/squareup/ui/main/DiningOptionCache;

    .line 2585
    invoke-virtual {v0}, Lcom/squareup/ui/main/DiningOptionCache;->getDefaultDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v6

    iget-object v7, p0, Lcom/squareup/payment/Transaction;->res:Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/payment/Transaction;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 2586
    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidEnabled()Z

    move-result v8

    iget-object v0, p0, Lcom/squareup/payment/Transaction;->availableDiscountsStore:Lcom/squareup/payment/AvailableDiscountsStore;

    invoke-virtual {v0}, Lcom/squareup/payment/AvailableDiscountsStore;->isDiscountApplicationIdEnabled()Z

    move-result v9

    move-object v0, p1

    .line 2583
    invoke-static/range {v0 .. v9}, Lcom/squareup/payment/OrderProtoConversions;->forTicketFromCartProto(Lcom/squareup/tickets/OpenTicket;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/calc/constants/RoundingType;Ljava/util/Map;Ljava/util/List;Lcom/squareup/checkout/DiningOption;Lcom/squareup/util/Res;ZZ)Lcom/squareup/payment/Order;

    move-result-object p1

    return-object p1
.end method

.method public canStillNameCart()Z
    .locals 1

    .line 1547
    iget-boolean v0, p0, Lcom/squareup/payment/Transaction;->canStillNameCart:Z

    return v0
.end method

.method public captureLocalPaymentAndCreateReceipt()V
    .locals 2

    .line 1744
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    .line 1745
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->isLocalPayment()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 1750
    :try_start_0
    invoke-virtual {v0, v1}, Lcom/squareup/payment/Payment;->capture(Z)Z
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1755
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->endCurrentTenderAndCreateReceipt()V

    return-void

    :catch_0
    move-exception v0

    .line 1752
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1746
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Only local payments can be captured in sellerflow."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public captureLocalPaymentAndResetInSellerFlow()V
    .locals 3

    .line 1759
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    .line 1760
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->captureLocalPaymentAndCreateReceipt()V

    .line 1763
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1764
    new-instance v1, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 1765
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getUniqueClientId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v1

    .line 1766
    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    .line 1767
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->isStoreAndForward()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->OFFLINE_CHARGE:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->CHARGE:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    .line 1770
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/squareup/payment/Transaction;->closeCurrentTicketBeforeReset(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;)V

    .line 1772
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->reset()V

    return-void
.end method

.method public cardOption()Lcom/squareup/payment/CardOptionEnabled;
    .locals 1

    .line 2058
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2059
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->paymentIsBelowMinimum()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2060
    sget-object v0, Lcom/squareup/payment/CardOptionEnabled$ShouldBeDisabled$BelowMin;->INSTANCE:Lcom/squareup/payment/CardOptionEnabled$ShouldBeDisabled$BelowMin;

    return-object v0

    .line 2061
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->paymentIsAboveMaximum()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2062
    sget-object v0, Lcom/squareup/payment/CardOptionEnabled$ShouldBeDisabled$AboveMax;->INSTANCE:Lcom/squareup/payment/CardOptionEnabled$ShouldBeDisabled$AboveMax;

    return-object v0

    .line 2071
    :cond_1
    sget-object v0, Lcom/squareup/payment/CardOptionEnabled$ShouldBeEnabled;->INSTANCE:Lcom/squareup/payment/CardOptionEnabled$ShouldBeEnabled;

    return-object v0
.end method

.method public cardOptionShouldBeEnabled()Z
    .locals 2

    .line 2054
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->cardOption()Lcom/squareup/payment/CardOptionEnabled;

    move-result-object v0

    sget-object v1, Lcom/squareup/payment/CardOptionEnabled$ShouldBeEnabled;->INSTANCE:Lcom/squareup/payment/CardOptionEnabled$ShouldBeEnabled;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public cartChanges()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/payment/OrderEntryEvents$CartChanged;",
            ">;"
        }
    .end annotation

    .line 2707
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->cartChanges:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public catalogDiscountMayApplyAtCartScope(Ljava/lang/String;)Z
    .locals 1

    .line 1318
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->catalogDiscountMayApplyAtCartScope(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public checkDiscounts()V
    .locals 2

    .line 1845
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->eligibleForReset()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1847
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->nextAvailableDiscounts:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/payment/Transaction;->availableDiscountsStore:Lcom/squareup/payment/AvailableDiscountsStore;

    invoke-virtual {v1}, Lcom/squareup/payment/AvailableDiscountsStore;->getAvailableCogsDiscounts()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1848
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->reset()V

    :cond_0
    return-void
.end method

.method public checkFees()V
    .locals 2

    .line 1833
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->eligibleForReset()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1836
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->nextAvailableTaxes:Ljava/util/Map;

    iget-object v1, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getAvailableTaxes()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/Transaction;->nextAvailableTaxRules:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    .line 1837
    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getAvailableTaxRules()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1838
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->reset()V

    :cond_1
    return-void
.end method

.method public clearKeypadItem()V
    .locals 2

    .line 1147
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->popItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    .line 1148
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->buildKeypadItem()Lcom/squareup/checkout/CartItem;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/payment/Transaction;->pushItem(Lcom/squareup/checkout/CartItem;)V

    .line 1149
    invoke-direct {p0, v0}, Lcom/squareup/payment/Transaction;->postRemovedItem(Lcom/squareup/checkout/CartItem;)V

    return-void
.end method

.method public clearTip()V
    .locals 2

    .line 759
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 761
    invoke-virtual {v0, v1, v1}, Lcom/squareup/payment/Payment;->setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    :cond_0
    return-void
.end method

.method public closeCurrentTicketBeforeReset(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;)V
    .locals 3

    .line 2625
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2628
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->getOrderWithoutKeypadItem()Lcom/squareup/payment/Order;

    move-result-object v1

    .line 2629
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->isDiscountApplicationIdEnabled()Z

    move-result v2

    .line 2628
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tickets/OpenTicket;->updateAndSetWriteOnlyDeletes(Lcom/squareup/payment/Order;Z)V

    .line 2630
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v1, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-interface {v0, v1, p1, p2}, Lcom/squareup/tickets/Tickets;->closeTicketAndUnlock(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;)V

    .line 2631
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    invoke-virtual {p1}, Lcom/squareup/log/tickets/OpenTicketsLogger;->stopTicketInCartUi()V

    return-void

    .line 2626
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Can\'t close current open ticket as it is null!"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public commitKeypadItem()Z
    .locals 2

    .line 1060
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->doCommitKeypadItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1062
    invoke-static {}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->keypadCommitted()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    :cond_0
    return v0
.end method

.method public createPauseResumeHandler()Lcom/squareup/pauses/PausesAndResumes;
    .locals 1

    .line 1437
    new-instance v0, Lcom/squareup/payment/Transaction$1;

    invoke-direct {v0, p0}, Lcom/squareup/payment/Transaction$1;-><init>(Lcom/squareup/payment/Transaction;)V

    return-object v0
.end method

.method public deleteCurrentTicketAndReset()V
    .locals 3

    .line 2636
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2639
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->getOrderWithoutKeypadItem()Lcom/squareup/payment/Order;

    move-result-object v1

    .line 2640
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->isDiscountApplicationIdEnabled()Z

    move-result v2

    .line 2639
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tickets/OpenTicket;->updateAndSetWriteOnlyDeletes(Lcom/squareup/payment/Order;Z)V

    .line 2641
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v1, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-interface {v0, v1}, Lcom/squareup/tickets/Tickets;->deleteTicketAndUnlock(Lcom/squareup/tickets/OpenTicket;)V

    .line 2642
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    invoke-virtual {v0}, Lcom/squareup/log/tickets/OpenTicketsLogger;->stopTicketInCartUi()V

    .line 2643
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->reset()V

    return-void

    .line 2637
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Can\'t delete current open ticket as it is null!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public dropPayment(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 2

    .line 1727
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->asAuthPayment()Lcom/squareup/payment/RequiresAuthorization;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1728
    invoke-interface {v0}, Lcom/squareup/payment/RequiresAuthorization;->hasRequestedAuthorization()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1729
    invoke-interface {v0, p1}, Lcom/squareup/payment/RequiresAuthorization;->cancel(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 1732
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 1734
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->onPaymentDropped()V

    .line 1737
    :cond_1
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->clearPaymentInFlight()V

    .line 1738
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p1

    .line 1739
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getSensitiveValues()Lcom/squareup/payment/Order$SensitiveValues;

    move-result-object p1

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/squareup/payment/Order$SensitiveValues;->card:Lcom/squareup/Card;

    .line 1740
    invoke-static {}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->paymentChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    return-void
.end method

.method public dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 1

    .line 1708
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->isSingleTender()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1711
    :cond_0
    iget-object p2, p0, Lcom/squareup/payment/Transaction;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p2}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 1712
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p1}, Lcom/squareup/payment/TenderInEdit;->clearSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    goto :goto_1

    .line 1713
    :cond_1
    iget-object p2, p0, Lcom/squareup/payment/Transaction;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p2}, Lcom/squareup/payment/TenderInEdit;->isMagStripeTender()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 1714
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p1}, Lcom/squareup/payment/TenderInEdit;->clearMagStripeTender()Lcom/squareup/payment/tender/MagStripeTenderBuilder;

    goto :goto_1

    .line 1715
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/payment/BillPayment;->hasLastAddedTender()Z

    move-result p2

    if-eqz p2, :cond_4

    .line 1716
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/payment/BillPayment;->dropLastAddedTender(Z)Lcom/squareup/payment/tender/BaseTender$Builder;

    goto :goto_1

    .line 1709
    :cond_3
    :goto_0
    invoke-virtual {p0, p2}, Lcom/squareup/payment/Transaction;->dropPayment(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    :cond_4
    :goto_1
    return-void
.end method

.method public endCurrentTenderAndCreateReceipt()V
    .locals 1

    .line 2116
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->endCurrentTenderAndCreateReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Transaction;->receiptForLastPayment:Lcom/squareup/payment/PaymentReceipt;

    return-void
.end method

.method public ensurePaymentOrderTicketNameUpToDate()V
    .locals 2

    .line 1571
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getOrderTicketName()Ljava/lang/String;

    move-result-object v0

    .line 1572
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Payment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/payment/OrderSnapshot;->setOrderTicketName(Ljava/lang/String;)V

    return-void
.end method

.method public getAddedCouponsAndCartScopeDiscountEvents()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;"
        }
    .end annotation

    .line 1197
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscountEvents()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 1193
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getAdditionalTaxes()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 537
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAdditionalTaxes()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getAllComps()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 555
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAllComps()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getAllDiscounts()Lcom/squareup/protos/common/Money;
    .locals 6

    .line 546
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getNegativeAllDiscounts()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 547
    iget-object v1, p0, Lcom/squareup/payment/Transaction;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v1}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    return-object v0

    .line 549
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/protos/common/Money;->newBuilder()Lcom/squareup/protos/common/Money$Builder;

    move-result-object v1

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 550
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getAllComps()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/protos/common/Money$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object v0

    .line 551
    invoke-virtual {v0}, Lcom/squareup/protos/common/Money$Builder;->build()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getAllSurchargeAmountDue()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 571
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAllSurchargesAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getAllTaxes()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 583
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAllTaxes()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getAmountDue()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 456
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PAYMENT_FLOW_USE_PAYMENT_CONFIG:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/payment/Transaction;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    if-nez v0, :cond_0

    goto :goto_0

    .line 460
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->getAmountToCollect()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    .line 458
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getAmountForDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/protos/common/Money;
    .locals 1

    .line 542
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->getAmountForDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public getAmountToDisplayOnBuyerDisplayDisconnect(Z)Lcom/squareup/protos/common/Money;
    .locals 0

    if-eqz p1, :cond_0

    .line 600
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->lastCapturedAmount:Lcom/squareup/protos/common/Money;

    return-object p1

    .line 602
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getGrandTotal()Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public getAppliedDiscounts()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 1205
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAllAppliedDiscountsInDisplayOrder()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getAutoGratuity()Lcom/squareup/checkout/Surcharge$AutoGratuity;
    .locals 1

    .line 559
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAutoGratuity()Lcom/squareup/checkout/Surcharge$AutoGratuity;

    move-result-object v0

    return-object v0
.end method

.method public getAutoGratuityAmountDue()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 563
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAutoGratuityAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getAvailableTaxRules()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;"
        }
    .end annotation

    .line 1186
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAvailableTaxRules()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAvailableTaxes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 1182
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAvailableTaxesAsList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getBundler()Lmortar/bundler/Bundler;
    .locals 1

    .line 1423
    new-instance v0, Lcom/squareup/payment/Transaction$TransactionBundler;

    invoke-direct {v0, p0}, Lcom/squareup/payment/Transaction$TransactionBundler;-><init>(Lcom/squareup/payment/Transaction;)V

    return-object v0
.end method

.method public getCard()Lcom/squareup/Card;
    .locals 1

    .line 767
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getSensitiveValues()Lcom/squareup/payment/Order$SensitiveValues;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/payment/Order$SensitiveValues;->card:Lcom/squareup/Card;

    return-object v0
.end method

.method public getCartItemCount()I
    .locals 4

    .line 1166
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 1167
    iget-object v3, v2, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v3}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_1

    :cond_0
    iget-object v2, v2, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v2}, Ljava/math/BigDecimal;->intValue()I

    move-result v2

    :goto_1
    add-int/2addr v1, v2

    goto :goto_0

    .line 1169
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasEmptyKeypadItem()Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 v1, v1, -0x1

    :cond_2
    return v1
.end method

.method public getCartRowCount()I
    .locals 2

    .line 1349
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1350
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasEmptyKeypadItem()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, -0x1

    :cond_0
    return v0
.end method

.method public getCartTotalCount()I
    .locals 2

    .line 1178
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getCartItemCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getCurrentDiningOption()Lcom/squareup/checkout/DiningOption;
    .locals 1

    .line 2689
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentEmployeeToken()Ljava/lang/String;
    .locals 1

    .line 451
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentTicket()Lcom/squareup/tickets/OpenTicket;
    .locals 1

    .line 2676
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    return-object v0
.end method

.method public getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 2475
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    return-object v0
.end method

.method public getCustomerDisplayNameOrBlank()Ljava/lang/String;
    .locals 2

    .line 2548
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2549
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2552
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->display_name:Ljava/lang/String;

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getCustomerGiftCardInstrumentDetails()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation

    .line 2487
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getCustomerInstrumentDetails()Ljava/util/List;

    move-result-object v0

    .line 2488
    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->isNullOrEmpty(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 2491
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    .line 2492
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2494
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    .line 2495
    iget-object v3, v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-ne v3, v4, :cond_1

    .line 2496
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method public getCustomerId()Ljava/lang/String;
    .locals 1

    .line 2540
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getCustomerId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCustomerInstrumentDetails()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation

    .line 2483
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getCustomerInstrumentDetails()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCustomerNonGiftCardInstrumentDetails()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation

    .line 2504
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getCustomerInstrumentDetails()Ljava/util/List;

    move-result-object v0

    .line 2505
    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->isNullOrEmpty(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 2509
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2511
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    .line 2512
    iget-object v3, v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eq v3, v4, :cond_1

    .line 2513
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method public getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;
    .locals 1

    .line 2479
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultOrderName()Ljava/lang/String;
    .locals 2

    .line 1474
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/transaction/R$string;->kitchen_printing_order:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDiningOptionName()Ljava/lang/String;
    .locals 1

    .line 2685
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getDiningOptionName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .line 1464
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1465
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOpenTicketName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1467
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasOrderTicketName()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1468
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrderTicketName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDisplayNameOrDefault()Ljava/lang/String;
    .locals 2

    .line 1487
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 1488
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1489
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getDefaultOrderName()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getEcomCheckoutLinkItemId()Ljava/lang/String;
    .locals 2

    .line 825
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasItems()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 826
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->merchantCatalogObjectToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getEmptyKeypadItem()Lcom/squareup/checkout/CartItem;
    .locals 2

    .line 1393
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasEmptyKeypadItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1396
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->peekItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    return-object v0

    .line 1394
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cart has no empty keypad item"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getGiftCardTotal()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 2017
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getGiftCardTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getGrandTotal()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 588
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getIndexOfLastLockedItem()I
    .locals 2

    .line 2380
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 2382
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    iget-boolean v1, v1, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    if-eqz v1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public getInstrumentDetails(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;
    .locals 4

    .line 2521
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getCustomerInstrumentDetails()Ljava/util/List;

    move-result-object v0

    .line 2522
    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->isNullOrEmpty(Ljava/util/Collection;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return-object v2

    .line 2526
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    .line 2527
    iget-object v3, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->instrument_token:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    return-object v1

    :cond_2
    return-object v2
.end method

.method public getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 634
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getKeypadInclusiveTaxes()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 1153
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->peekItem()Lcom/squareup/checkout/CartItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order;->getAdjustedItemFor(Lcom/squareup/checkout/CartItem;)Lcom/squareup/calc/AdjustedItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/calc/AdjustedItem;->getTotalCollectedForInclusiveTaxes()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/payment/Transaction;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getKeypadNote()Ljava/lang/String;
    .locals 1

    .line 1036
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->peekItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getKeypadPrice()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 848
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->getKeypadPriceAmount()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/payment/Transaction;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getNotVoidedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 638
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getNotVoidedItems()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getNumSplitsTotal()J
    .locals 2

    .line 789
    iget-wide v0, p0, Lcom/squareup/payment/Transaction;->numSplitsTotal:J

    return-wide v0
.end method

.method public getOpenTicketName()Ljava/lang/String;
    .locals 1

    .line 1516
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getOpenTicketName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOpenTicketNote()Ljava/lang/String;
    .locals 1

    .line 1511
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getOpenTicketNote()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOrder()Lcom/squareup/payment/Order;
    .locals 4

    .line 2212
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    if-eqz v0, :cond_0

    return-object v0

    .line 2213
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/squareup/payment/Transaction;

    .line 2215
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "You did not register the %s bundler (see getBundler())"

    .line 2214
    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getOrderAdjustmentAmountsByIdForItemization(Lcom/squareup/checkout/CartItem;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/CartItem;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1406
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->getAdjustmentAmountsByIdForItem(Lcom/squareup/checkout/CartItem;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public getOrderItem(I)Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 1401
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/CartItem;

    return-object p1
.end method

.method public getOrderTicketName()Ljava/lang/String;
    .locals 1

    .line 1479
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getOrderTicketName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPayment()Lcom/squareup/payment/Payment;
    .locals 1

    .line 2022
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->paymentInFlight:Lcom/squareup/payment/Payment;

    return-object v0
.end method

.method public getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;
    .locals 1

    .line 1534
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v0

    return-object v0
.end method

.method public getReturnItemCount()I
    .locals 1

    .line 722
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getReturnItemCount()I

    move-result v0

    return v0
.end method

.method public getRoundedRemainingAmountDue()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 490
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    .line 493
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getTenderAmountDue()Lcom/squareup/protos/common/Money;
    .locals 6

    .line 469
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 470
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    .line 473
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 475
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/BillPayment;->getCapturedTenders()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v1, v1

    goto :goto_0

    :cond_1
    const-wide/16 v1, 0x0

    .line 476
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getNumSplitsTotal()J

    move-result-wide v3

    sub-long/2addr v3, v1

    const-wide/16 v1, 0x1

    cmp-long v5, v3, v1

    if-lez v5, :cond_2

    .line 478
    iget-object v1, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    div-long/2addr v1, v3

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :cond_2
    return-object v0

    .line 486
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getTenderIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 2731
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasTenderInFlight()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2732
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTicketId()Ljava/lang/String;
    .locals 1

    .line 2419
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getTicketId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTip()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 745
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 746
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getTipSettings()Lcom/squareup/settings/server/TipSettings;
    .locals 1

    .line 911
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 912
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v0

    return-object v0

    .line 914
    :cond_0
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->hasTipSettingsOverride()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 915
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->tipSettingsOverride:Lcom/squareup/settings/server/TipSettings;

    return-object v0

    .line 917
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v0

    return-object v0
.end method

.method public getTransactionMaximum()J
    .locals 2

    .line 2050
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getTransactionMaximum()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTransactionMinimum()J
    .locals 2

    .line 2035
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getTransactionMinimum()J

    move-result-wide v0

    return-wide v0
.end method

.method public getUnappliedDiscountsThatCanBeAppliedToOnlyOneItem()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 1326
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getUnappliedDiscountsThatCanBeAppliedToOnlyOneItem()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUniqueKey()Ljava/lang/String;
    .locals 1

    .line 1414
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getSensitiveValues()Lcom/squareup/payment/Order$SensitiveValues;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/payment/Order$SensitiveValues;->uniqueClientId:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->resetUniqueKey()V

    .line 1415
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getSensitiveValues()Lcom/squareup/payment/Order$SensitiveValues;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/payment/Order$SensitiveValues;->uniqueClientId:Ljava/lang/String;

    return-object v0
.end method

.method public getUnlockedItemCount()I
    .locals 5

    .line 2396
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2397
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasEmptyKeypadItem()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, -0x1

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    sub-int/2addr v0, v2

    :goto_0
    if-ltz v0, :cond_2

    .line 2400
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 2401
    iget-boolean v4, v3, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    if-nez v4, :cond_2

    .line 2402
    iget-object v4, v3, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v4}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    iget-object v3, v3, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    .line 2404
    invoke-virtual {v3}, Ljava/math/BigDecimal;->intValueExact()I

    move-result v3

    :goto_1
    add-int/2addr v1, v3

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    return v1
.end method

.method public getUnsavedVoidedItems()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 642
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 644
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/payment/Transaction;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v1}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 645
    iget-object v1, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-virtual {v1}, Lcom/squareup/tickets/OpenTicket;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-eqz v1, :cond_2

    .line 647
    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    if-eqz v1, :cond_2

    .line 649
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 650
    iget-object v2, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-virtual {v2}, Lcom/squareup/tickets/OpenTicket;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Itemization;

    .line 651
    iget-object v3, v3, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 654
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 655
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v3, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v4, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 656
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-object v0
.end method

.method public hasAtLeastOneModifier()Z
    .locals 1

    .line 1338
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasAtLeastOneModifier()Z

    move-result v0

    return v0
.end method

.method public hasAutoGratuity()Z
    .locals 1

    .line 567
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldShowAutoGratuity()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getAutoGratuity()Lcom/squareup/checkout/Surcharge$AutoGratuity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasBillPayment()Z
    .locals 1

    .line 2149
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasCard()Z
    .locals 1

    .line 771
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getCard()Lcom/squareup/Card;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasCompableItems()Z
    .locals 2

    .line 695
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getNotVoidedItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 696
    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public hasCustomer()Z
    .locals 1

    .line 2536
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasCustomer()Z

    move-result v0

    return v0
.end method

.method public hasDiningOption()Z
    .locals 1

    .line 2680
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasDiningOption()Z

    move-result v0

    return v0
.end method

.method public hasDiscountAppliedToAllItems(Ljava/lang/String;)Z
    .locals 1

    .line 1314
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->hasDiscountAppliedToAllItems(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public hasDiscounts()Z
    .locals 1

    .line 1330
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasDiscounts()Z

    move-result v0

    return v0
.end method

.method public hasEmptyKeypadItem()Z
    .locals 1

    .line 1080
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->peekItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasExactlyOneDiscount()Z
    .locals 1

    .line 1308
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1309
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasExactlyOneNonCompDiscount()Z

    move-result v0

    goto :goto_0

    .line 1310
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasExactlyOneDiscount()Z

    move-result v0

    :goto_0
    return v0
.end method

.method public hasGiftCardItem()Z
    .locals 1

    .line 2009
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasGiftCardItem()Z

    move-result v0

    return v0
.end method

.method public hasGiftCardItemWithServerId(Ljava/lang/String;)Z
    .locals 1

    .line 2013
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->hasGiftCardItemWithServerId(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public hasInclusiveTaxes()Z
    .locals 5

    .line 575
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getInclusiveTaxesAmount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasInterestingTaxState()Z
    .locals 6

    .line 1355
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getCartRowCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x1

    if-ge v2, v0, :cond_2

    .line 1357
    invoke-virtual {p0, v2}, Lcom/squareup/payment/Transaction;->getOrderItem(I)Lcom/squareup/checkout/CartItem;

    move-result-object v4

    .line 1358
    iget-object v5, v4, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    if-gtz v5, :cond_1

    invoke-virtual {v4}, Lcom/squareup/checkout/CartItem;->usingExactlyRuleBasedTaxes()Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return v3

    .line 1364
    :cond_2
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getSurcharges()Ljava/util/List;

    move-result-object v0

    .line 1365
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Surcharge;

    .line 1366
    invoke-virtual {v2}, Lcom/squareup/checkout/Surcharge;->appliedTaxes()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    return v3

    :cond_4
    return v1
.end method

.method public hasInvoicePayment()Z
    .locals 1

    .line 2167
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->asInvoicePayment()Lcom/squareup/payment/InvoicePayment;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasItems()Z
    .locals 4

    .line 808
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    .line 809
    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-gt v1, v3, :cond_0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v2, 0x1

    :cond_1
    return v2
.end method

.method public hasLockedAndNonLockedItems()Z
    .locals 7

    .line 2339
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasEmptyKeypadItem()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->popItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 2341
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_1
    if-ltz v1, :cond_3

    .line 2343
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/checkout/CartItem;

    iget-boolean v6, v6, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    if-eqz v6, :cond_1

    const/4 v4, 0x1

    goto :goto_2

    :cond_1
    const/4 v5, 0x1

    :goto_2
    if-eqz v4, :cond_2

    if-eqz v5, :cond_2

    goto :goto_3

    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_3
    :goto_3
    if-eqz v0, :cond_4

    .line 2353
    invoke-direct {p0, v0}, Lcom/squareup/payment/Transaction;->pushItem(Lcom/squareup/checkout/CartItem;)V

    :cond_4
    if-eqz v4, :cond_5

    if-eqz v5, :cond_5

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    return v2
.end method

.method public hasLostPayment()Z
    .locals 1

    .line 2096
    iget-boolean v0, p0, Lcom/squareup/payment/Transaction;->lostPayment:Z

    return v0
.end method

.method public hasNonCompDiscounts()Z
    .locals 1

    .line 1334
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasNonCompDiscounts()Z

    move-result v0

    return v0
.end method

.method public hasNonLockedItems()Z
    .locals 4

    .line 2363
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasEmptyKeypadItem()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->popItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 2365
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    :goto_1
    if-ltz v1, :cond_2

    .line 2367
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    iget-boolean v3, v3, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    if-nez v3, :cond_1

    or-int/lit8 v2, v2, 0x1

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    .line 2373
    invoke-direct {p0, v0}, Lcom/squareup/payment/Transaction;->pushItem(Lcom/squareup/checkout/CartItem;)V

    :cond_3
    return v2
.end method

.method public hasNonZeroTip()Z
    .locals 5

    .line 750
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 751
    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasOpenTicketName()Z
    .locals 1

    .line 1506
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOpenTicketName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasOpenTicketNote()Z
    .locals 1

    .line 1502
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOpenTicketNote()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasOrderTicketName()Z
    .locals 1

    .line 1494
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrderTicketName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasPayment()Z
    .locals 1

    .line 2153
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->paymentInFlight:Lcom/squareup/payment/Payment;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasReceiptForLastPayment()Z
    .locals 1

    .line 2120
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->receiptForLastPayment:Lcom/squareup/payment/PaymentReceipt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasReturn()Z
    .locals 1

    .line 715
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasReturn()Z

    move-result v0

    return v0
.end method

.method public hasSplitTenderBillPayment()Z
    .locals 2

    .line 775
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 777
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->isSingleTender()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 778
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasPartialAuthCardTenderInFlight()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasSplitTenderBillPaymentWithAtLeastOneTender()Z
    .locals 1

    .line 782
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/payment/Transaction;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 783
    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-nez v0, :cond_0

    .line 784
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasTenderInFlight()Z

    move-result v0

    if-nez v0, :cond_0

    .line 785
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasTendered()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasTicket()Z
    .locals 1

    .line 2415
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasTip()Z
    .locals 1

    .line 755
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasUnappliedDiscountsThatCanBeAppliedToOnlyOneItem()Z
    .locals 1

    .line 1322
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasUnappliedDiscountsThatCanBeAppliedToOnlyOneItem()Z

    move-result v0

    return v0
.end method

.method public hasUncapturedAuth()Z
    .locals 1

    .line 801
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->asAuthPayment()Lcom/squareup/payment/RequiresAuthorization;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 803
    invoke-interface {v0}, Lcom/squareup/payment/RequiresAuthorization;->hasRequestedAuthorization()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/Transaction;->paymentInFlight:Lcom/squareup/payment/Payment;

    .line 804
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->isCaptured()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public ingoreTransactionLimits()Z
    .locals 1

    .line 1559
    iget-boolean v0, p0, Lcom/squareup/payment/Transaction;->ignoreTransactionLimits:Z

    return v0
.end method

.method public isComped()Z
    .locals 3

    .line 726
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 730
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getNotVoidedItems()Ljava/util/List;

    move-result-object v0

    .line 731
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    return v1

    .line 735
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 736
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    :cond_3
    const/4 v0, 0x1

    return v0
.end method

.method public isDelayCapture()Z
    .locals 1

    .line 2723
    iget-boolean v0, p0, Lcom/squareup/payment/Transaction;->delayCapture:Z

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 836
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasItems()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasReturn()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasDiscounts()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isFullyComped()Z
    .locals 5

    .line 2043
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2044
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getAllComps()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    .line 2045
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOfflineTransactionLimitExceeded()Z
    .locals 4

    .line 497
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/StoreAndForwardSettings;->getSingleTransactionLimit()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 501
    :cond_0
    sget-object v2, Lcom/squareup/money/MoneyMath;->COMPARATOR:Ljava/util/Comparator;

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public isTakingPaymentFromInvoice()Z
    .locals 1

    .line 1011
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasInvoice()Z

    move-result v0

    return v0
.end method

.method public merchantAlwaysSkipSignature()Z
    .locals 1

    .line 890
    iget-boolean v0, p0, Lcom/squareup/payment/Transaction;->isReaderSdk:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/Transaction;->skipSignature:Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 892
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSignatureSettings()Lcom/squareup/settings/server/SignatureSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/SignatureSettings;->merchantAlwaysSkipSignature()Z

    move-result v0

    :goto_0
    return v0
.end method

.method public merchantIsAllowedToSkipSignaturesForSmallPayments()Z
    .locals 1

    .line 896
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSignatureSettings()Lcom/squareup/settings/server/SignatureSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/SignatureSettings;->merchantIsAllowedToSkipSignaturesForSmallPayments()Z

    move-result v0

    return v0
.end method

.method public merchantOptedInToSkipSignaturesForSmallPayments()Z
    .locals 1

    .line 900
    iget-boolean v0, p0, Lcom/squareup/payment/Transaction;->isReaderSdk:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/Transaction;->skipSignature:Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 902
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSignatureSettings()Lcom/squareup/settings/server/SignatureSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/SignatureSettings;->merchantOptedInToSkipSignaturesForSmallPayments()Z

    move-result v0

    :goto_0
    return v0
.end method

.method public onCustomerChanged()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 2544
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->orderCustomerChanged:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {v0}, Lrx/Observable;->switchOnNext(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method onDiningOptionUpdated(Lcom/squareup/ui/main/DiningOptionCache$Update;)V
    .locals 0

    .line 1691
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasDiningOption()Z

    move-result p1

    if-nez p1, :cond_0

    .line 1692
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->diningOptionCache:Lcom/squareup/ui/main/DiningOptionCache;

    invoke-virtual {p1}, Lcom/squareup/ui/main/DiningOptionCache;->getDefaultDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1694
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Transaction;->setDiningOption(Lcom/squareup/checkout/DiningOption;)V

    :cond_0
    return-void
.end method

.method onFeesUpdated(Lcom/squareup/settings/server/FeesUpdate;)V
    .locals 1

    .line 1678
    iget-object v0, p1, Lcom/squareup/settings/server/FeesUpdate;->taxes:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/checkout/Tax;->mapCatalogTaxes(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Transaction;->nextAvailableTaxes:Ljava/util/Map;

    .line 1679
    iget-object v0, p1, Lcom/squareup/settings/server/FeesUpdate;->taxRules:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/payment/OrderTaxRule;->listOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Transaction;->nextAvailableTaxRules:Ljava/util/List;

    .line 1680
    iget-object v0, p1, Lcom/squareup/settings/server/FeesUpdate;->discounts:Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/payment/Transaction;->nextAvailableDiscounts:Ljava/util/List;

    .line 1681
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    if-nez v0, :cond_0

    return-void

    .line 1682
    :cond_0
    iget-boolean p1, p1, Lcom/squareup/settings/server/FeesUpdate;->inForeground:Z

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->checkFees()V

    .line 1683
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->checkDiscounts()V

    :cond_3
    return-void
.end method

.method onTaxItemRelationshipsChanged(Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;)V
    .locals 10

    .line 1597
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    if-nez v0, :cond_0

    return-void

    .line 1600
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 1602
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    iget-object v1, p1, Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;->feeId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order;->getAvailableTaxes(Ljava/lang/String;)Lcom/squareup/checkout/Tax;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1605
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->reset()V

    return-void

    .line 1611
    :cond_2
    iget-boolean v1, v0, Lcom/squareup/checkout/Tax;->enabled:Z

    if-nez v1, :cond_3

    return-void

    .line 1613
    :cond_3
    iget-object v1, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_c

    .line 1615
    iget-object v4, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v4}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/CartItem;

    .line 1617
    invoke-virtual {v4}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v5

    if-eqz v5, :cond_4

    goto/16 :goto_3

    .line 1620
    :cond_4
    sget-object v5, Lcom/squareup/payment/Transaction$2;->$SwitchMap$com$squareup$payment$OrderEntryEvents$TaxItemChangeType:[I

    iget-object v6, p1, Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;->type:Lcom/squareup/payment/OrderEntryEvents$TaxItemChangeType;

    invoke-virtual {v6}, Lcom/squareup/payment/OrderEntryEvents$TaxItemChangeType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    const/4 v6, 0x1

    if-eq v5, v6, :cond_8

    const/4 v6, 0x2

    if-ne v5, v6, :cond_7

    .line 1625
    iget-object v5, p1, Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;->edits:Ljava/util/Map;

    iget-object v6, v4, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    if-nez v5, :cond_5

    .line 1627
    sget-object v5, Lcom/squareup/payment/Transaction$Change;->NO_OP:Lcom/squareup/payment/Transaction$Change;

    goto :goto_1

    .line 1629
    :cond_5
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_6

    sget-object v5, Lcom/squareup/payment/Transaction$Change;->ADD:Lcom/squareup/payment/Transaction$Change;

    goto :goto_1

    :cond_6
    sget-object v5, Lcom/squareup/payment/Transaction$Change;->DELETE:Lcom/squareup/payment/Transaction$Change;

    goto :goto_1

    .line 1633
    :cond_7
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected event type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;->type:Lcom/squareup/payment/OrderEntryEvents$TaxItemChangeType;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1622
    :cond_8
    sget-object v5, Lcom/squareup/payment/Transaction$Change;->ADD:Lcom/squareup/payment/Transaction$Change;

    .line 1636
    :goto_1
    sget-object v6, Lcom/squareup/payment/Transaction$Change;->NO_OP:Lcom/squareup/payment/Transaction$Change;

    if-ne v5, v6, :cond_9

    goto/16 :goto_3

    .line 1638
    :cond_9
    new-instance v6, Ljava/util/LinkedHashMap;

    iget-object v7, v4, Lcom/squareup/checkout/CartItem;->defaultTaxes:Ljava/util/Map;

    invoke-direct {v6, v7}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 1639
    new-instance v7, Ljava/util/LinkedHashMap;

    iget-object v8, v4, Lcom/squareup/checkout/CartItem;->ruleBasedTaxes:Ljava/util/Map;

    invoke-direct {v7, v8}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 1640
    new-instance v8, Ljava/util/LinkedHashMap;

    iget-object v9, v4, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-direct {v8, v9}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 1642
    sget-object v9, Lcom/squareup/payment/Transaction$Change;->ADD:Lcom/squareup/payment/Transaction$Change;

    if-ne v5, v9, :cond_a

    .line 1643
    iget-object v5, v4, Lcom/squareup/checkout/CartItem;->defaultTaxes:Ljava/util/Map;

    iget-object v9, v0, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v5, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 1644
    iget-object v5, v0, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v6, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1645
    invoke-direct {p0, v4, v0}, Lcom/squareup/payment/Transaction;->isTaxDisabledForItem(Lcom/squareup/checkout/CartItem;Lcom/squareup/checkout/Tax;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 1646
    iget-object v5, v0, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v7, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1648
    iget-object v5, v4, Lcom/squareup/checkout/CartItem;->userEditedTaxIds:Ljava/util/Set;

    iget-object v9, v0, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v5, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 1649
    iget-object v5, v0, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v8, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1654
    :cond_a
    iget-object v5, v4, Lcom/squareup/checkout/CartItem;->defaultTaxes:Ljava/util/Map;

    iget-object v9, v0, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v5, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 1655
    iget-object v5, v0, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v6, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1656
    iget-object v5, v0, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v7, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1657
    iget-object v5, v4, Lcom/squareup/checkout/CartItem;->userEditedTaxIds:Ljava/util/Set;

    iget-object v9, v0, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v5, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 1658
    iget-object v5, v0, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v8, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1663
    :cond_b
    :goto_2
    invoke-virtual {v4}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v4

    .line 1664
    invoke-virtual {v4, v6}, Lcom/squareup/checkout/CartItem$Builder;->defaultTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v4

    .line 1665
    invoke-virtual {v4, v7}, Lcom/squareup/checkout/CartItem$Builder;->ruleBasedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v4

    .line 1666
    invoke-virtual {v4, v8}, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v4

    .line 1667
    invoke-virtual {v4}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v4

    .line 1668
    invoke-virtual {p0, v3, v4, v2}, Lcom/squareup/payment/Transaction;->replaceItem(ILcom/squareup/checkout/CartItem;Z)V

    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 1670
    :cond_c
    invoke-virtual {p0, v2}, Lcom/squareup/payment/Transaction;->postItemsChanged(Z)V

    return-void
.end method

.method public orderCreated()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/payment/Order;",
            ">;"
        }
    .end annotation

    .line 2711
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->orderCreated:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public paymentIsAboveMaximum()Z
    .locals 5

    .line 2031
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getTenderAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getTransactionMaximum()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public paymentIsBelowMinimum()Z
    .locals 5

    .line 2026
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getTenderAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getTransactionMinimum()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public paymentIsWithinRange()Z
    .locals 1

    .line 2039
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->paymentIsBelowMinimum()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->paymentIsAboveMaximum()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public peekItemClientId()Ljava/lang/String;
    .locals 1

    .line 1085
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->peekItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    return-object v0
.end method

.method public performNoSalePaymentAndReset(Lcom/squareup/payment/tender/TenderFactory;)V
    .locals 1

    .line 2741
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->assertNoCard()V

    .line 2742
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requiresSynchronousAuthorization()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2746
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 2747
    invoke-virtual {p1}, Lcom/squareup/payment/tender/TenderFactory;->createNoSale()Lcom/squareup/payment/tender/NoSaleTender$Builder;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/BillPayment;->addLocalTender(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)V

    .line 2749
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->captureLocalPaymentAndResetInSellerFlow()V

    return-void

    .line 2743
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Payment cannot be processed in the background"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method postItemsChanged(Z)V
    .locals 0

    .line 2246
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemsChanged(Z)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    return-void
.end method

.method public postTaxChanged(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;)V
    .locals 0

    .line 2238
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->taxesChanged(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    return-void
.end method

.method public preserveCustomerOnNextReset()V
    .locals 1

    const/4 v0, 0x1

    .line 1555
    iput-boolean v0, p0, Lcom/squareup/payment/Transaction;->shouldPreserveCustomerOnNextReset:Z

    return-void
.end method

.method public reapplyTaxToItemsAndSurcharges(Ljava/lang/String;)V
    .locals 1

    .line 1388
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->reapplyTaxToItemsAndSurcharges(Ljava/lang/String;)V

    .line 1389
    sget-object p1, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->EDIT:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/Transaction;->postTaxChanged(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;)V

    return-void
.end method

.method public registerOnBus(Lcom/squareup/badbus/BadBus;)Lio/reactivex/disposables/Disposable;
    .locals 4

    .line 1588
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    const/4 v1, 0x3

    new-array v1, v1, [Lio/reactivex/disposables/Disposable;

    const-class v2, Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;

    .line 1589
    invoke-virtual {p1, v2}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/payment/-$$Lambda$7BsIoJ77S3Gv4AiS7tadsJK0LyI;

    invoke-direct {v3, p0}, Lcom/squareup/payment/-$$Lambda$7BsIoJ77S3Gv4AiS7tadsJK0LyI;-><init>(Lcom/squareup/payment/Transaction;)V

    .line 1590
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Lcom/squareup/settings/server/FeesUpdate;

    .line 1591
    invoke-virtual {p1, v2}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/payment/-$$Lambda$99taOo5jtkzGPo8GffGnCUZnDwg;

    invoke-direct {v3, p0}, Lcom/squareup/payment/-$$Lambda$99taOo5jtkzGPo8GffGnCUZnDwg;-><init>(Lcom/squareup/payment/Transaction;)V

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const-class v2, Lcom/squareup/ui/main/DiningOptionCache$Update;

    .line 1592
    invoke-virtual {p1, v2}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v2, Lcom/squareup/payment/-$$Lambda$1B-OTZBl1spXUSxM38l6I5Maqnk;

    invoke-direct {v2, p0}, Lcom/squareup/payment/-$$Lambda$1B-OTZBl1spXUSxM38l6I5Maqnk;-><init>(Lcom/squareup/payment/Transaction;)V

    invoke-virtual {p1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const/4 v2, 0x2

    aput-object p1, v1, v2

    invoke-direct {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>([Lio/reactivex/disposables/Disposable;)V

    return-object v0
.end method

.method public removeDiscountsFromAllItems(Ljava/lang/Iterable;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/payment/OrderEntryEvents$ChangeSource;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x1

    .line 1269
    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/payment/Transaction;->removeDiscountsFromAllItems(Ljava/lang/Iterable;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;Z)V

    return-void
.end method

.method public removeDiscountsFromAllItems(Ljava/lang/Iterable;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/payment/OrderEntryEvents$ChangeSource;",
            "Z)V"
        }
    .end annotation

    .line 1280
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz p3, :cond_0

    .line 1282
    invoke-direct {p0, v1}, Lcom/squareup/payment/Transaction;->returnCouponIfEligible(Ljava/lang/String;)V

    .line 1284
    :cond_0
    sget-object v2, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->PRICING_RULE:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    if-ne p2, v2, :cond_1

    .line 1286
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/squareup/payment/Order;->pricingEngineRemoveDiscountFromAllItems(Ljava/lang/String;)Z

    move-result v1

    goto :goto_1

    .line 1287
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/Transaction;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 1288
    invoke-interface {v3}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/payment/Transaction;->res:Lcom/squareup/util/Res;

    invoke-virtual {v3, v4}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProtoNonNull(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object v3

    .line 1287
    invoke-virtual {v2, v1, v3}, Lcom/squareup/payment/Order;->manuallyRemoveDiscountFromAllItems(Ljava/lang/String;Lcom/squareup/protos/client/Employee;)Z

    move-result v1

    :goto_1
    or-int/2addr v0, v1

    goto :goto_0

    :cond_2
    if-nez v0, :cond_3

    .line 1295
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/Order;->hasDiscounts()Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    .line 1299
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->shouldShowDiscountRow()Z

    move-result p1

    const/4 p3, 0x1

    if-nez p1, :cond_4

    .line 1301
    sget-object p1, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->DELETE:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    invoke-static {p3, p1, p2}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged(ZLcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    goto :goto_2

    .line 1303
    :cond_4
    sget-object p1, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->EDIT:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    invoke-static {p3, p1, p2}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged(ZLcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    :goto_2
    return-void
.end method

.method public removeItem(I)V
    .locals 3

    .line 667
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v0

    .line 669
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/Transaction;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v2}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProto(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2, v0}, Lcom/squareup/payment/Order;->removeItem(IZLcom/squareup/protos/client/Employee;)Lcom/squareup/checkout/CartItem;

    move-result-object v0

    .line 671
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne p1, v1, :cond_0

    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->buildKeypadItem()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->pushItem(Lcom/squareup/checkout/CartItem;)V

    .line 672
    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/payment/Transaction;->postRemovedItem(Lcom/squareup/checkout/CartItem;)V

    return-void
.end method

.method public removeItem(Ljava/lang/String;)V
    .locals 4

    .line 676
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v0

    .line 678
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/Transaction;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v2}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProto(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v0, v3}, Lcom/squareup/payment/Order;->removeItem(Ljava/lang/String;ZLcom/squareup/protos/client/Employee;Z)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    .line 679
    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postRemovedItem(Lcom/squareup/checkout/CartItem;)V

    return-void
.end method

.method public removeReturnCart()V
    .locals 2

    .line 707
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order;->setReturnCart(Lcom/squareup/checkout/ReturnCart;)V

    .line 708
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->postReturnChanged()V

    return-void
.end method

.method public removeTaxFromAllItemsAndSurcharges(Ljava/lang/String;)V
    .locals 1

    .line 1374
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->getAvailableTaxes(Ljava/lang/String;)Lcom/squareup/checkout/Tax;

    move-result-object p1

    .line 1375
    iget-object v0, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-static {v0, p1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/payment/Transaction;->removeTaxesFromAllItemsAndSurcharges(Ljava/util/Map;)V

    return-void
.end method

.method public removeTaxesFromAllItemsAndSurcharges(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Adjustment;",
            ">;)V"
        }
    .end annotation

    .line 1379
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->removeTaxesFromAllItemsAndSurcharges(Ljava/util/Map;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1381
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->shouldShowTaxRow()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1382
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasInterestingTaxState()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1383
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getAdditionalTaxesAmount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-lez p1, :cond_0

    sget-object p1, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->EDIT:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->DELETE:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    .line 1381
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Transaction;->postTaxChanged(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;)V

    :cond_1
    return-void
.end method

.method public removeUnlockedItems()V
    .locals 3

    .line 2295
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ORDER_ENTRY_CLEAR_SALE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 2299
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasEmptyKeypadItem()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->popItem()Lcom/squareup/checkout/CartItem;

    .line 2301
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    .line 2302
    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_1

    .line 2306
    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    iget-boolean v2, v2, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    if-nez v2, :cond_1

    .line 2307
    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order;->removeItem(I)Lcom/squareup/checkout/CartItem;

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 2315
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/payment/Order;->removeNonRecalledFromTicketDiscounts()Z

    .line 2319
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->buildKeypadItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/payment/Transaction;->pushItem(Lcom/squareup/checkout/CartItem;)V

    .line 2323
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2324
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->reset()V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    .line 2326
    invoke-virtual {p0, v0}, Lcom/squareup/payment/Transaction;->postItemsChanged(Z)V

    :goto_1
    return-void
.end method

.method public repeatItem(Ljava/lang/String;)Lcom/squareup/checkout/CartItem;
    .locals 2

    .line 683
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/Transaction;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/payment/Order;->repeatItem(Ljava/lang/String;Lcom/squareup/util/Res;)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    .line 684
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Transaction;->addOrderItem(Lcom/squareup/checkout/CartItem;)V

    return-object p1
.end method

.method public replaceItem(ILcom/squareup/checkout/CartItem;)V
    .locals 1

    const/4 v0, 0x1

    .line 610
    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/payment/Transaction;->replaceItem(ILcom/squareup/checkout/CartItem;Z)V

    return-void
.end method

.method public replaceItem(ILcom/squareup/checkout/CartItem;Z)V
    .locals 2

    .line 614
    iget-object v0, p2, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 616
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/squareup/payment/Order;->replaceItem(ILcom/squareup/checkout/CartItem;)V

    if-eqz p3, :cond_2

    .line 619
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 620
    invoke-virtual {v0}, Ljava/math/BigDecimal;->abs()Ljava/math/BigDecimal;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/checkout/CartItem$Builder;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 621
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    .line 623
    sget-object p3, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-static {v0, p3}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result p3

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 624
    invoke-direct {p0, p2, p1}, Lcom/squareup/payment/Transaction;->postAddedItem(ZLcom/squareup/checkout/CartItem;)V

    goto :goto_0

    .line 625
    :cond_0
    sget-object p3, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-static {v0, p3}, Lcom/squareup/util/BigDecimals;->lessThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 626
    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postRemovedItem(Lcom/squareup/checkout/CartItem;)V

    goto :goto_0

    .line 628
    :cond_1
    invoke-direct {p0, p2}, Lcom/squareup/payment/Transaction;->postEditedItem(Lcom/squareup/checkout/CartItem;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public requireAuthPayment()Lcom/squareup/payment/RequiresAuthorization;
    .locals 1

    .line 2136
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->paymentInFlight:Lcom/squareup/payment/Payment;

    check-cast v0, Lcom/squareup/payment/RequiresAuthorization;

    return-object v0
.end method

.method public requireBillPayment()Lcom/squareup/payment/BillPayment;
    .locals 1

    .line 2163
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/BillPayment;

    return-object v0
.end method

.method public requireInvoiceId()Ljava/lang/String;
    .locals 1

    .line 1007
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->requireInvoiceId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public requireInvoicePayment()Lcom/squareup/payment/InvoicePayment;
    .locals 1

    .line 2186
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/InvoicePayment;

    return-object v0
.end method

.method public requirePayment()Lcom/squareup/payment/Payment;
    .locals 2

    .line 2105
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->paymentInFlight:Lcom/squareup/payment/Payment;

    const-string v1, "paymentInFlight"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/Payment;

    return-object v0
.end method

.method public requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;
    .locals 2

    .line 2124
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasReceiptForLastPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2127
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->receiptForLastPayment:Lcom/squareup/payment/PaymentReceipt;

    return-object v0

    .line 2125
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No receipt in flight!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public requireSignedPayment()Lcom/squareup/payment/AcceptsSignature;
    .locals 1

    .line 2091
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/AcceptsSignature;

    return-object v0
.end method

.method public requireTippingPayment()Lcom/squareup/payment/AcceptsTips;
    .locals 1

    .line 2081
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/AcceptsTips;

    return-object v0
.end method

.method public requiresSynchronousAuthorization()Z
    .locals 1

    .line 2005
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->requiresSynchronousAuthorization()Z

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 4

    .line 1780
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->prepareForReset()V

    .line 1781
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->returnAllUnusedCoupons()V

    const/4 v0, 0x1

    .line 1782
    iput-boolean v0, p0, Lcom/squareup/payment/Transaction;->canStillNameCart:Z

    .line 1783
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->userId:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/payment/Transaction;->currency:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v2, p0, Lcom/squareup/payment/Transaction;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 1784
    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/PaymentSettings;->getRoundingType()Lcom/squareup/calc/constants/RoundingType;

    move-result-object v2

    .line 1783
    invoke-static {v0, v1, v2}, Lcom/squareup/payment/Order$Builder;->forPayment(Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/calc/constants/RoundingType;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/Transaction;->nextAvailableTaxes:Ljava/util/Map;

    .line 1785
    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->availableTaxes(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/Transaction;->nextAvailableTaxRules:Ljava/util/List;

    .line 1786
    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->availableTaxRules(Ljava/util/List;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/Transaction;->diningOptionCache:Lcom/squareup/ui/main/DiningOptionCache;

    .line 1787
    invoke-virtual {v1}, Lcom/squareup/ui/main/DiningOptionCache;->getDefaultDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->diningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 1788
    iget-object v1, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/squareup/payment/Transaction;->shouldPreserveCustomerOnNextReset:Z

    if-eqz v2, :cond_0

    .line 1789
    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->customerContact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    .line 1790
    invoke-virtual {v2}, Lcom/squareup/payment/Order;->getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/payment/Order$Builder;->customerSummary(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    .line 1791
    invoke-virtual {v2}, Lcom/squareup/payment/Order;->getCustomerInstrumentDetails()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/payment/Order$Builder;->customerInstruments(Ljava/util/List;)Lcom/squareup/payment/Order$Builder;

    .line 1793
    :cond_0
    iget-object v1, p0, Lcom/squareup/payment/Transaction;->availableDiscountsStore:Lcom/squareup/payment/AvailableDiscountsStore;

    .line 1794
    invoke-virtual {v1}, Lcom/squareup/payment/AvailableDiscountsStore;->isDiscountApplicationIdEnabled()Z

    move-result v1

    .line 1793
    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->discountApplicationIdEnabled(Z)Lcom/squareup/payment/Order$Builder;

    const/4 v1, 0x0

    .line 1795
    iput-boolean v1, p0, Lcom/squareup/payment/Transaction;->shouldPreserveCustomerOnNextReset:Z

    .line 1796
    iget-object v2, p0, Lcom/squareup/payment/Transaction;->availableDiscountsStore:Lcom/squareup/payment/AvailableDiscountsStore;

    iget-object v3, p0, Lcom/squareup/payment/Transaction;->nextAvailableDiscounts:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/squareup/payment/AvailableDiscountsStore;->update(Ljava/util/Collection;)V

    .line 1797
    invoke-virtual {v0}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    .line 1798
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->orderCreated:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v2, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    const/4 v0, 0x0

    .line 1799
    iput-object v0, p0, Lcom/squareup/payment/Transaction;->tipSettingsOverride:Lcom/squareup/settings/server/TipSettings;

    const-string v2, "NO_CLIENT_ID"

    .line 1800
    iput-object v2, p0, Lcom/squareup/payment/Transaction;->apiClientId:Ljava/lang/String;

    const-wide/16 v2, 0x0

    .line 1801
    iput-wide v2, p0, Lcom/squareup/payment/Transaction;->numSplitsTotal:J

    .line 1802
    iput-boolean v1, p0, Lcom/squareup/payment/Transaction;->startedFromChargeButton:Z

    .line 1803
    iput-boolean v1, p0, Lcom/squareup/payment/Transaction;->ignoreTransactionLimits:Z

    .line 1804
    iput-object v0, p0, Lcom/squareup/payment/Transaction;->networkRequestsModifier:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

    .line 1805
    iput-object v0, p0, Lcom/squareup/payment/Transaction;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    .line 1806
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->concludeReset()V

    return-void
.end method

.method public resetReceiptScreenStrategy()V
    .locals 1

    .line 878
    sget-object v0, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->READ_FROM_PAYMENT:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    iput-object v0, p0, Lcom/squareup/payment/Transaction;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    return-void
.end method

.method public returnAllCoupons()V
    .locals 2

    .line 1905
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    if-nez v0, :cond_0

    return-void

    .line 1909
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 1910
    invoke-direct {p0, v1}, Lcom/squareup/payment/Transaction;->returnCouponIfEligible(Lcom/squareup/checkout/Discount;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public saveGrandTotalToDisplayOnBuyerDisconnect()V
    .locals 1

    .line 606
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getGrandTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Transaction;->lastCapturedAmount:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public setCanRenameCart(Z)V
    .locals 0

    .line 1551
    iput-boolean p1, p0, Lcom/squareup/payment/Transaction;->canStillNameCart:Z

    return-void
.end method

.method public setCard(Lcom/squareup/Card;)V
    .locals 1

    .line 841
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getSensitiveValues()Lcom/squareup/payment/Order$SensitiveValues;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/payment/Order$SensitiveValues;->card:Lcom/squareup/Card;

    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 842
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getSensitiveValues()Lcom/squareup/payment/Order$SensitiveValues;

    move-result-object v0

    iput-object p1, v0, Lcom/squareup/payment/Order$SensitiveValues;->card:Lcom/squareup/Card;

    .line 843
    invoke-static {}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->cardChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postChange(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    :cond_0
    return-void
.end method

.method public setConfig(Lcom/squareup/payment/TransactionConfig;)V
    .locals 2

    .line 856
    invoke-virtual {p1}, Lcom/squareup/payment/TransactionConfig;->getClearTaxes()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 857
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Transaction;->nextAvailableTaxes:Ljava/util/Map;

    .line 859
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->reset()V

    .line 860
    invoke-virtual {p1}, Lcom/squareup/payment/TransactionConfig;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/payment/Transaction;->setTipSettings(Lcom/squareup/settings/server/TipSettings;)V

    .line 861
    invoke-virtual {p1}, Lcom/squareup/payment/TransactionConfig;->getApiClientId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Transaction;->apiClientId:Ljava/lang/String;

    .line 862
    invoke-virtual {p1}, Lcom/squareup/payment/TransactionConfig;->getReceiptScreenStrategy()Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Transaction;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    .line 863
    invoke-virtual {p1}, Lcom/squareup/payment/TransactionConfig;->getSkipSignature()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/Transaction;->skipSignature:Z

    .line 864
    invoke-virtual {p1}, Lcom/squareup/payment/TransactionConfig;->getDelayCapture()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/Transaction;->delayCapture:Z

    .line 865
    invoke-virtual {p1}, Lcom/squareup/payment/TransactionConfig;->getAmount()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/payment/Transaction;->setKeypadPrice(J)V

    .line 866
    invoke-virtual {p1}, Lcom/squareup/payment/TransactionConfig;->getNote()Ljava/lang/String;

    move-result-object p1

    .line 867
    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 868
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Transaction;->setKeypadNote(Ljava/lang/String;)V

    .line 870
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->commitKeypadItem()Z

    return-void
.end method

.method public setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;)V"
        }
    .end annotation

    .line 2471
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/payment/Order;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    return-void
.end method

.method public setDiningOption(Lcom/squareup/checkout/DiningOption;)V
    .locals 1

    .line 2693
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->setDiningOption(Lcom/squareup/checkout/DiningOption;)V

    .line 2694
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->postDiningOptionChanged()V

    return-void
.end method

.method public setEmployee(Lcom/squareup/permissions/EmployeeInfo;)V
    .locals 2

    .line 2569
    invoke-static {p1}, Lcom/squareup/permissions/EmployeeInfo;->isEmpty(Lcom/squareup/permissions/EmployeeInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2570
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    .line 2571
    iget-object v1, p1, Lcom/squareup/permissions/EmployeeInfo;->employeeToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order;->setEmployeeToken(Ljava/lang/String;)V

    .line 2572
    iget-object v1, p1, Lcom/squareup/permissions/EmployeeInfo;->firstName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order;->setEmployeeFirstName(Ljava/lang/String;)V

    .line 2573
    iget-object p1, p1, Lcom/squareup/permissions/EmployeeInfo;->lastName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->setEmployeeLastName(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setIgnoreTransactionLimits(Z)V
    .locals 0

    .line 1563
    iput-boolean p1, p0, Lcom/squareup/payment/Transaction;->ignoreTransactionLimits:Z

    return-void
.end method

.method public setKeypadNote(Ljava/lang/String;)V
    .locals 2

    .line 1041
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getKeypadNote()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/Strings;->hasTextDifference(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1043
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->beginTransactionIfCartEmpty()V

    .line 1045
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->popItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    .line 1046
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/squareup/checkout/CartItem$Builder;->notes(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    .line 1047
    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->pushItem(Lcom/squareup/checkout/CartItem;)V

    const/4 v1, 0x0

    .line 1048
    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/payment/Transaction;->postAddedOrEditedItem(Lcom/squareup/checkout/CartItem;Lcom/squareup/checkout/CartItem;Z)V

    :cond_0
    return-void
.end method

.method public setKeypadPrice(J)V
    .locals 3

    .line 1016
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->beginTransactionIfCartEmpty()V

    .line 1018
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->getKeypadPriceAmount()J

    move-result-wide v0

    cmp-long v2, v0, p1

    if-eqz v2, :cond_0

    .line 1019
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->popItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    .line 1020
    iget-object v1, p0, Lcom/squareup/payment/Transaction;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, p2, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 1021
    iget-object p2, p0, Lcom/squareup/payment/Transaction;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {p2}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object p2

    .line 1022
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    .line 1023
    invoke-virtual {v1, p1}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/payment/Transaction;->res:Lcom/squareup/util/Res;

    .line 1027
    invoke-virtual {p2, v1}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProto(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/checkout/util/ItemizationEvents;->creationEvent(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p2

    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/checkout/CartItem$Builder;->events(Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 1028
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    .line 1029
    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->pushItem(Lcom/squareup/checkout/CartItem;)V

    const/4 p2, 0x1

    .line 1030
    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/payment/Transaction;->postAddedOrEditedItem(Lcom/squareup/checkout/CartItem;Lcom/squareup/checkout/CartItem;Z)V

    :cond_0
    return-void
.end method

.method public setNetworkRequestModifier(Lcom/squareup/checkoutflow/services/NetworkRequestModifier;)V
    .locals 0

    .line 947
    iput-object p1, p0, Lcom/squareup/payment/Transaction;->networkRequestsModifier:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

    return-void
.end method

.method public setNumSplitsTotal(J)V
    .locals 0

    .line 793
    iput-wide p1, p0, Lcom/squareup/payment/Transaction;->numSplitsTotal:J

    return-void
.end method

.method public setOpenTicketNameAndNote(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1524
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->setOpenTicketName(Ljava/lang/String;)V

    .line 1525
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/payment/Order;->setOpenTicketNote(Ljava/lang/String;)V

    return-void
.end method

.method public setOrderTicketName(Ljava/lang/String;)V
    .locals 1

    .line 1498
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->setOrderTicketName(Ljava/lang/String;)V

    return-void
.end method

.method public setPaymentConfig(Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)V
    .locals 0

    .line 953
    iput-object p1, p0, Lcom/squareup/payment/Transaction;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    return-void
.end method

.method public setPredefinedTicket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V
    .locals 1

    .line 1543
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->setPredefinedTicket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    return-void
.end method

.method public setReturnCart(Lcom/squareup/checkout/ReturnCart;)V
    .locals 1

    .line 702
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->setReturnCart(Lcom/squareup/checkout/ReturnCart;)V

    .line 703
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->postReturnChanged()V

    return-void
.end method

.method public setStartedWithChargeButton(Z)V
    .locals 0

    .line 2715
    iput-boolean p1, p0, Lcom/squareup/payment/Transaction;->startedFromChargeButton:Z

    return-void
.end method

.method public setTicket(Lcom/squareup/tickets/OpenTicket;)V
    .locals 1

    .line 2424
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Transaction;->buildOrderForTicket(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/payment/Transaction;->setTicketFromSplit(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/payment/Order;)V

    return-void
.end method

.method public setTicketFromSplit(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/payment/Order;)V
    .locals 1

    .line 2448
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2452
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->prepareForReset()V

    .line 2454
    iput-object p1, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    .line 2455
    iput-object p2, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    .line 2456
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->orderCreated:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object p2, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 2459
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getOpenTicketName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/payment/Order;->setOrderTicketName(Ljava/lang/String;)V

    .line 2461
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->buildKeypadItem()Lcom/squareup/checkout/CartItem;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/payment/Order;->pushItem(Lcom/squareup/checkout/CartItem;)V

    const/4 p1, 0x1

    .line 2462
    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->postEverythingChanged(Z)V

    .line 2463
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->orderCustomerChanged:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object p2, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {p2}, Lcom/squareup/payment/Order;->onCustomerChanged()Lrx/Observable;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 2465
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    invoke-virtual {p1}, Lcom/squareup/log/tickets/OpenTicketsLogger;->stopTicketSelectedToCartUi()V

    .line 2466
    iget-object p1, p0, Lcom/squareup/payment/Transaction;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    invoke-virtual {p1}, Lcom/squareup/log/tickets/OpenTicketsLogger;->startTicketInCartUi()V

    return-void

    .line 2449
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "currentTicket already set, must remove before setting a new one!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setTicketWithoutLockingItems(Lcom/squareup/tickets/OpenTicket;Z)V
    .locals 1

    .line 2433
    invoke-direct {p0, p1}, Lcom/squareup/payment/Transaction;->buildOrderForTicketWithoutLockingItems(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/payment/Transaction;->setTicketFromSplit(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/payment/Order;)V

    if-eqz p2, :cond_0

    .line 2435
    iget-object p2, p0, Lcom/squareup/payment/Transaction;->tickets:Lcom/squareup/tickets/Tickets;

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lcom/squareup/tickets/Tickets;->addTicketAndLock(Ljava/util/Date;Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/tickets/OpenTicket;

    goto :goto_0

    .line 2437
    :cond_0
    iget-object p2, p0, Lcom/squareup/payment/Transaction;->tickets:Lcom/squareup/tickets/Tickets;

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lcom/squareup/tickets/Tickets;->addTicket(Ljava/util/Date;Lcom/squareup/protos/client/bills/Cart;)V

    :goto_0
    return-void
.end method

.method public setTipSettings(Lcom/squareup/settings/server/TipSettings;)V
    .locals 0

    .line 925
    iput-object p1, p0, Lcom/squareup/payment/Transaction;->tipSettingsOverride:Lcom/squareup/settings/server/TipSettings;

    return-void
.end method

.method public shouldShowCompRow()Z
    .locals 1

    .line 532
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasCompedItems()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public shouldShowDiscountRow()Z
    .locals 1

    .line 526
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasNonCompDiscounts()Z

    move-result v0

    goto :goto_0

    .line 528
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasDiscounts()Z

    move-result v0

    :goto_0
    return v0
.end method

.method public shouldShowIncludesTaxes()Z
    .locals 6

    .line 1854
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAllTaxesAmount()J

    move-result-wide v0

    .line 1855
    iget-object v2, p0, Lcom/squareup/payment/Transaction;->order:Lcom/squareup/payment/Order;

    if-eqz v2, :cond_0

    const-wide/16 v3, 0x0

    cmp-long v5, v0, v3

    if-lez v5, :cond_0

    .line 1857
    invoke-virtual {v2}, Lcom/squareup/payment/Order;->getInclusiveTaxesAmount()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public shouldShowPostTaxAutoGratuityRow()Z
    .locals 1

    .line 522
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasAutoGratuity()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getAutoGratuity()Lcom/squareup/checkout/Surcharge$AutoGratuity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/Surcharge$AutoGratuity;->isTaxable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public shouldShowPreTaxAutoGratuityRow()Z
    .locals 1

    .line 518
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasAutoGratuity()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getAutoGratuity()Lcom/squareup/checkout/Surcharge$AutoGratuity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/Surcharge$AutoGratuity;->isTaxable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public shouldShowTaxOff()Z
    .locals 1

    .line 514
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->allItemTaxesDisabled()Z

    move-result v0

    return v0
.end method

.method public shouldShowTaxRow()Z
    .locals 2

    .line 505
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldShowInclusiveTaxesInCart()Z

    move-result v0

    .line 506
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    if-eqz v0, :cond_0

    .line 508
    invoke-virtual {v1}, Lcom/squareup/payment/Order;->hasAvailableTaxes()Z

    move-result v0

    goto :goto_0

    .line 509
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/payment/Order;->hasAvailableNonInclusiveTaxes()Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_1

    .line 510
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->hasTaxableItems()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public shouldSkipReceipt()Z
    .locals 3

    .line 882
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    .line 883
    iget-object v1, p0, Lcom/squareup/payment/Transaction;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    sget-object v2, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->READ_FROM_PAYMENT:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    if-ne v1, v2, :cond_0

    .line 884
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->shouldSkipReceipt()Z

    move-result v0

    return v0

    .line 886
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    sget-object v1, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->SKIP_RECEIPT_SCREEN:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public startAppointmentsTransaction(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 942
    new-instance v0, Lcom/squareup/payment/-$$Lambda$Transaction$kycyRMFdBtmsUQxSmJ7UEvLBRMA;

    invoke-direct {v0, p1}, Lcom/squareup/payment/-$$Lambda$Transaction$kycyRMFdBtmsUQxSmJ7UEvLBRMA;-><init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;)V

    invoke-direct {p0, p2, p3, v0}, Lcom/squareup/payment/Transaction;->resetAndCreateOrderFromCart(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/rolodex/Contact;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public startInvoicePayment()Lcom/squareup/payment/InvoicePayment;
    .locals 2

    .line 1998
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->assertSameUser()V

    .line 1999
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->invoicePaymentFactory:Lcom/squareup/payment/InvoicePayment$Factory;

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/InvoicePayment$Factory;->create(Lcom/squareup/payment/Order;)Lcom/squareup/payment/InvoicePayment;

    move-result-object v0

    .line 2000
    iput-object v0, p0, Lcom/squareup/payment/Transaction;->paymentInFlight:Lcom/squareup/payment/Payment;

    return-object v0
.end method

.method public startInvoiceTransaction(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/client/rolodex/Contact;Z)V
    .locals 2

    .line 930
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;-><init>()V

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 931
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->invoice_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice;->version:Ljava/lang/String;

    .line 932
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->version(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;

    move-result-object v0

    .line 933
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    invoke-virtual {v0, p3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->is_final_payment(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;

    move-result-object p3

    .line 934
    invoke-virtual {p3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    move-result-object p3

    .line 936
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    new-instance v0, Lcom/squareup/payment/-$$Lambda$Transaction$YqQa7imXgb14szYs5mtQGR3MyL8;

    invoke-direct {v0, p3}, Lcom/squareup/payment/-$$Lambda$Transaction$YqQa7imXgb14szYs5mtQGR3MyL8;-><init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/payment/Transaction;->resetAndCreateOrderFromCart(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/rolodex/Contact;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;
    .locals 1

    const/4 v0, 0x1

    .line 1972
    invoke-direct {p0, v0}, Lcom/squareup/payment/Transaction;->startBillPayment(Z)Lcom/squareup/payment/BillPayment;

    move-result-object v0

    return-object v0
.end method

.method public startSplitTenderBillPayment()Lcom/squareup/payment/BillPayment;
    .locals 1

    const/4 v0, 0x0

    .line 1968
    invoke-direct {p0, v0}, Lcom/squareup/payment/Transaction;->startBillPayment(Z)Lcom/squareup/payment/BillPayment;

    move-result-object v0

    return-object v0
.end method

.method public startedWithChargeButton()Z
    .locals 1

    .line 2719
    iget-boolean v0, p0, Lcom/squareup/payment/Transaction;->startedFromChargeButton:Z

    return v0
.end method

.method public updateCurrentTicketBeforeReset()V
    .locals 5

    .line 2651
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2654
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->isPredefinedTicket()Z

    move-result v0

    .line 2655
    iget-object v1, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-virtual {v1}, Lcom/squareup/tickets/OpenTicket;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v1

    .line 2656
    iget-object v2, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->getOrderWithoutKeypadItem()Lcom/squareup/payment/Order;

    move-result-object v3

    .line 2657
    invoke-direct {p0}, Lcom/squareup/payment/Transaction;->isDiscountApplicationIdEnabled()Z

    move-result v4

    .line 2656
    invoke-virtual {v2, v3, v4}, Lcom/squareup/tickets/OpenTicket;->updateAndSetWriteOnlyDeletes(Lcom/squareup/payment/Order;Z)V

    .line 2658
    iget-object v2, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-virtual {v2}, Lcom/squareup/tickets/OpenTicket;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v2

    .line 2660
    iget-object v3, p0, Lcom/squareup/payment/Transaction;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v4, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-interface {v3, v4}, Lcom/squareup/tickets/Tickets;->updateTicketAndUnlock(Lcom/squareup/tickets/OpenTicket;)V

    .line 2661
    iget-object v3, p0, Lcom/squareup/payment/Transaction;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    invoke-virtual {v3}, Lcom/squareup/log/tickets/OpenTicketsLogger;->stopTicketInCartUi()V

    if-eqz v0, :cond_0

    .line 2662
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->isPredefinedTicket()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2663
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v3, Lcom/squareup/log/tickets/ConvertToCustomTicket;

    invoke-direct {v3, v1, v2}, Lcom/squareup/log/tickets/ConvertToCustomTicket;-><init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    invoke-interface {v0, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_0
    return-void

    .line 2652
    :cond_1
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Can\'t update current open ticket as it is null!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public voidItem(ILjava/lang/String;Lcom/squareup/protos/client/Employee;)V
    .locals 2

    .line 689
    iget-object v0, p0, Lcom/squareup/payment/Transaction;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidEnabled()Z

    move-result v0

    const-string v1, "Voiding is disabled."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 690
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0, p1, p3, p2}, Lcom/squareup/payment/Order;->voidItem(ILcom/squareup/protos/client/Employee;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 691
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Transaction;->postItemsChanged(Z)V

    return-void
.end method
