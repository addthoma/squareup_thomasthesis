.class public Lcom/squareup/payment/PaymentReceipt$Builder;
.super Ljava/lang/Object;
.source "PaymentReceipt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/PaymentReceipt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private defaultEmail:Lcom/squareup/payment/Obfuscated;

.field private defaultSms:Lcom/squareup/payment/Obfuscated;

.field private orderDisplayName:Ljava/lang/String;

.field private orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

.field private payment:Lcom/squareup/payment/Payment;

.field private ticketName:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/payment/PaymentReceipt$Builder;)Lcom/squareup/payment/Obfuscated;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/payment/PaymentReceipt$Builder;->defaultEmail:Lcom/squareup/payment/Obfuscated;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/payment/PaymentReceipt$Builder;)Lcom/squareup/payment/Obfuscated;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/payment/PaymentReceipt$Builder;->defaultSms:Lcom/squareup/payment/Obfuscated;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/payment/PaymentReceipt$Builder;)Lcom/squareup/payment/Payment;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/payment/PaymentReceipt$Builder;->payment:Lcom/squareup/payment/Payment;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/payment/PaymentReceipt$Builder;)Ljava/lang/CharSequence;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/payment/PaymentReceipt$Builder;->ticketName:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/payment/PaymentReceipt$Builder;)Lcom/squareup/payment/OrderSnapshot;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/payment/PaymentReceipt$Builder;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/payment/PaymentReceipt$Builder;)Ljava/lang/String;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/payment/PaymentReceipt$Builder;->orderDisplayName:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method buildNoTenderReceipt()Lcom/squareup/payment/PaymentReceipt$NoTenderReceipt;
    .locals 2

    .line 122
    new-instance v0, Lcom/squareup/payment/PaymentReceipt$NoTenderReceipt;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/PaymentReceipt$NoTenderReceipt;-><init>(Lcom/squareup/payment/PaymentReceipt$Builder;Lcom/squareup/payment/PaymentReceipt$1;)V

    return-object v0
.end method

.method buildTenderReceipt()Lcom/squareup/payment/PaymentReceipt$TenderReceipt;
    .locals 2

    .line 118
    new-instance v0, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/PaymentReceipt$TenderReceipt;-><init>(Lcom/squareup/payment/PaymentReceipt$Builder;Lcom/squareup/payment/PaymentReceipt$1;)V

    return-object v0
.end method

.method public buildVoidReceipt()Lcom/squareup/payment/PaymentReceipt;
    .locals 2

    .line 126
    new-instance v0, Lcom/squareup/payment/PaymentReceipt$VoidReceipt;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/PaymentReceipt$VoidReceipt;-><init>(Lcom/squareup/payment/PaymentReceipt$Builder;Lcom/squareup/payment/PaymentReceipt$1;)V

    return-object v0
.end method

.method public defaultEmail(Lcom/squareup/payment/Obfuscated;)Lcom/squareup/payment/PaymentReceipt$Builder;
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/payment/PaymentReceipt$Builder;->defaultEmail:Lcom/squareup/payment/Obfuscated;

    return-object p0
.end method

.method public defaultSms(Lcom/squareup/payment/Obfuscated;)Lcom/squareup/payment/PaymentReceipt$Builder;
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/squareup/payment/PaymentReceipt$Builder;->defaultSms:Lcom/squareup/payment/Obfuscated;

    return-object p0
.end method

.method public orderDisplayName(Ljava/lang/String;)Lcom/squareup/payment/PaymentReceipt$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/payment/PaymentReceipt$Builder;->orderDisplayName:Ljava/lang/String;

    return-object p0
.end method

.method public orderSnapshot(Lcom/squareup/payment/OrderSnapshot;)Lcom/squareup/payment/PaymentReceipt$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/payment/PaymentReceipt$Builder;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    return-object p0
.end method

.method public payment(Lcom/squareup/payment/Payment;)Lcom/squareup/payment/PaymentReceipt$Builder;
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/payment/PaymentReceipt$Builder;->payment:Lcom/squareup/payment/Payment;

    return-object p0
.end method

.method public ticketName(Ljava/lang/CharSequence;)Lcom/squareup/payment/PaymentReceipt$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/payment/PaymentReceipt$Builder;->ticketName:Ljava/lang/CharSequence;

    return-object p0
.end method
