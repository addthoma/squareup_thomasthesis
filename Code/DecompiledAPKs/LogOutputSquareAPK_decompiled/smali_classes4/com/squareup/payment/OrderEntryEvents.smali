.class public Lcom/squareup/payment/OrderEntryEvents;
.super Ljava/lang/Object;
.source "OrderEntryEvents.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/OrderEntryEvents$TicketRemoved;,
        Lcom/squareup/payment/OrderEntryEvents$TicketSaved;,
        Lcom/squareup/payment/OrderEntryEvents$CartChanged;,
        Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;,
        Lcom/squareup/payment/OrderEntryEvents$ItemStatus;,
        Lcom/squareup/payment/OrderEntryEvents$ChangeSource;,
        Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;,
        Lcom/squareup/payment/OrderEntryEvents$ItemType;,
        Lcom/squareup/payment/OrderEntryEvents$TaxItemChangeType;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
