.class public final Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;
.super Ljava/lang/Object;
.source "PendingPaymentsNotificationsSource.kt"

# interfaces
.implements Lcom/squareup/notificationcenterdata/NotificationsSource;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPendingPaymentsNotificationsSource.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PendingPaymentsNotificationsSource.kt\ncom/squareup/payment/pending/PendingPaymentsNotificationsSource\n*L\n1#1,114:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\r\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u000fH\u0002J\u000e\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u000eH\u0016J\u0010\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u000fH\u0002J\u0010\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u000fH\u0002J4\u0010\u001c\u001a&\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u00140\u0014 \u0010*\u0012\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u00140\u0014\u0018\u00010\u000e0\u000e2\u0006\u0010\u001d\u001a\u00020\u0016H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\r\u001a&\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f \u0010*\u0012\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f\u0018\u00010\u000e0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;",
        "Lcom/squareup/notificationcenterdata/NotificationsSource;",
        "res",
        "Lcom/squareup/util/Res;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "localNotificationAnalytics",
        "Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;",
        "notificationStateStore",
        "Lcom/squareup/notificationcenterdata/NotificationStateStore;",
        "pendingTransactionsStore",
        "Lcom/squareup/payment/pending/PendingTransactionsStore;",
        "(Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTime;Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;Lcom/squareup/notificationcenterdata/NotificationStateStore;Lcom/squareup/payment/pending/PendingTransactionsStore;)V",
        "pendingPaymentsCount",
        "Lio/reactivex/Observable;",
        "",
        "kotlin.jvm.PlatformType",
        "createPendingPaymentsNotification",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "state",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        "id",
        "",
        "count",
        "notifications",
        "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
        "pendingPaymentsNotificationContent",
        "pendingPaymentsNotificationTitle",
        "pendingPaymentsState",
        "notificationId",
        "payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final localNotificationAnalytics:Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

.field private final notificationStateStore:Lcom/squareup/notificationcenterdata/NotificationStateStore;

.field private final pendingPaymentsCount:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTime;Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;Lcom/squareup/notificationcenterdata/NotificationStateStore;Lcom/squareup/payment/pending/PendingTransactionsStore;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localNotificationAnalytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationStateStore"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pendingTransactionsStore"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p3, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->localNotificationAnalytics:Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

    iput-object p4, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->notificationStateStore:Lcom/squareup/notificationcenterdata/NotificationStateStore;

    .line 37
    invoke-interface {p5}, Lcom/squareup/payment/pending/PendingTransactionsStore;->allPendingTransactionsCount()Lio/reactivex/Observable;

    move-result-object p1

    const/4 p2, 0x0

    .line 38
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    .line 39
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->pendingPaymentsCount:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$createPendingPaymentsNotification(Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;ILcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/Notification;
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->createPendingPaymentsNotification(ILcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/Notification;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$id(Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;I)Ljava/lang/String;
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->id(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$pendingPaymentsState(Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->pendingPaymentsState(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final createPendingPaymentsNotification(ILcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/Notification;
    .locals 13

    .line 69
    new-instance v12, Lcom/squareup/notificationcenterdata/Notification;

    .line 70
    invoke-direct {p0, p1}, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->id(I)Ljava/lang/String;

    move-result-object v1

    .line 72
    invoke-direct {p0, p1}, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->pendingPaymentsNotificationTitle(I)Ljava/lang/String;

    move-result-object v3

    .line 73
    invoke-direct {p0, p1}, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->pendingPaymentsNotificationContent(I)Ljava/lang/String;

    move-result-object v4

    .line 75
    sget-object p1, Lcom/squareup/notificationcenterdata/Notification$Priority$Important;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$Priority$Important;

    move-object v6, p1

    check-cast v6, Lcom/squareup/notificationcenterdata/Notification$Priority;

    .line 76
    sget-object p1, Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;

    move-object v7, p1

    check-cast v7, Lcom/squareup/notificationcenterdata/Notification$DisplayType;

    .line 77
    sget-object v9, Lcom/squareup/notificationcenterdata/Notification$Source;->LOCAL:Lcom/squareup/notificationcenterdata/Notification$Source;

    .line 78
    new-instance p1, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;

    .line 79
    new-instance v0, Lcom/squareup/protos/client/ClientAction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$Builder;-><init>()V

    .line 80
    new-instance v2, Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet(Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;)Lcom/squareup/protos/client/ClientAction$Builder;

    move-result-object v0

    .line 81
    sget-object v2, Lcom/squareup/protos/client/ClientAction$FallbackBehavior;->NONE:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/ClientAction$Builder;->fallback_behavior(Lcom/squareup/protos/client/ClientAction$FallbackBehavior;)Lcom/squareup/protos/client/ClientAction$Builder;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$Builder;->build()Lcom/squareup/protos/client/ClientAction;

    move-result-object v0

    const-string v2, "ClientAction.Builder()\n \u2026\n                .build()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-direct {p1, v0}, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;-><init>(Lcom/squareup/protos/client/ClientAction;)V

    move-object v8, p1

    check-cast v8, Lcom/squareup/notificationcenterdata/Notification$Destination;

    .line 84
    iget-object p1, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {p1}, Lcom/squareup/time/CurrentTime;->instant()Lorg/threeten/bp/Instant;

    move-result-object v10

    .line 85
    sget-object p1, Lcom/squareup/communications/Message$Type$AlertToCompleteOrders;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertToCompleteOrders;

    move-object v11, p1

    check-cast v11, Lcom/squareup/communications/Message$Type;

    const-string v2, ""

    move-object v0, v12

    move-object v5, p2

    .line 69
    invoke-direct/range {v0 .. v11}, Lcom/squareup/notificationcenterdata/Notification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$State;Lcom/squareup/notificationcenterdata/Notification$Priority;Lcom/squareup/notificationcenterdata/Notification$DisplayType;Lcom/squareup/notificationcenterdata/Notification$Destination;Lcom/squareup/notificationcenterdata/Notification$Source;Lorg/threeten/bp/Instant;Lcom/squareup/communications/Message$Type;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->localNotificationAnalytics:Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

    invoke-interface {p1, v12}, Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;->logLocalNotificationCreated(Lcom/squareup/notificationcenterdata/Notification;)V

    return-object v12
.end method

.method private final id(I)Ljava/lang/String;
    .locals 2

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "pending-payments-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final pendingPaymentsNotificationContent(I)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    if-le p1, v0, :cond_1

    .line 98
    iget-object p1, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/payment/R$string;->pending_payments_notification_content_plural:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 99
    :cond_1
    iget-object p1, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/payment/R$string;->pending_payments_notification_content_singular:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1

    .line 96
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Failed requirement."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final pendingPaymentsNotificationTitle(I)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    if-le p1, v0, :cond_1

    .line 109
    iget-object p1, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/payment/R$string;->pending_payments_notification_title_plural:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 110
    :cond_1
    iget-object p1, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/payment/R$string;->pending_payments_notification_title_singular:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1

    .line 107
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Failed requirement."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final pendingPaymentsState(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ">;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->notificationStateStore:Lcom/squareup/notificationcenterdata/NotificationStateStore;

    .line 44
    sget-object v1, Lcom/squareup/notificationcenterdata/Notification$Source;->LOCAL:Lcom/squareup/notificationcenterdata/Notification$Source;

    invoke-interface {v0, p1, v1}, Lcom/squareup/notificationcenterdata/NotificationStateStore;->getNotificationState(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;)Lio/reactivex/Observable;

    move-result-object p1

    .line 45
    sget-object v0, Lcom/squareup/notificationcenterdata/Notification$State;->UNREAD:Lcom/squareup/notificationcenterdata/Notification$State;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public notifications()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
            ">;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;->pendingPaymentsCount:Lio/reactivex/Observable;

    .line 49
    new-instance v1, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource$notifications$1;

    invoke-direct {v1, p0}, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource$notifications$1;-><init>(Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "pendingPaymentsCount\n   \u2026              }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
