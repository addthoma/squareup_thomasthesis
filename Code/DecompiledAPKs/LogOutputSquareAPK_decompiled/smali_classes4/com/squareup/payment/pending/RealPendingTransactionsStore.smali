.class public final Lcom/squareup/payment/pending/RealPendingTransactionsStore;
.super Ljava/lang/Object;
.source "RealPendingTransactionsStore.kt"

# interfaces
.implements Lcom/squareup/payment/pending/PendingTransactionsStore;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;,
        Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealPendingTransactionsStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealPendingTransactionsStore.kt\ncom/squareup/payment/pending/RealPendingTransactionsStore\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,497:1\n92#2,4:498\n57#2,4:502\n119#2,4:506\n119#2,4:510\n119#2,4:514\n57#2,4:518\n*E\n*S KotlinDebug\n*F\n+ 1 RealPendingTransactionsStore.kt\ncom/squareup/payment/pending/RealPendingTransactionsStore\n*L\n69#1,4:498\n81#1,4:502\n98#1,4:506\n133#1,4:510\n151#1,4:514\n167#1,4:518\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008\u0007\u0018\u00002\u00020\u0001:\u0002>?BS\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J\u0014\u0010)\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001c0\u00170\u0016H\u0007J\u0014\u0010*\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u00170\u0016H\u0007J\u0014\u0010\u0015\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u00170\u0016H\u0016J\u000e\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0016H\u0016J\u0014\u0010\u001b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001c0\u00170\u0016H\u0016J\u001c\u0010+\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020-0#0,2\u0006\u0010.\u001a\u00020/H\u0016J\u001c\u00100\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180#0,2\u0006\u0010.\u001a\u00020/H\u0007J*\u00101\u001a\u0008\u0012\u0004\u0012\u00020$0#2\u000c\u00102\u001a\u0008\u0012\u0004\u0012\u00020$0#2\u000c\u00103\u001a\u0008\u0012\u0004\u0012\u00020$0#H\u0007J\u000e\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020 0\u0016H\u0016J\u000e\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0016H\u0016J\u0008\u00104\u001a\u000205H\u0016J\u0010\u00106\u001a\u0002072\u0006\u00108\u001a\u000209H\u0016J\u0008\u0010:\u001a\u000207H\u0016J\u0014\u0010;\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001c0\u00170\u0016H\u0016J\u0014\u0010<\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u00170\u0016H\u0016J\u000e\u0010=\u001a\u0008\u0012\u0004\u0012\u00020 0\u0016H\u0016J\u000e\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0016H\u0016J\u000e\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0016H\u0016J\u000e\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0016H\u0016R\u001a\u0010\u0015\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u00170\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001c0\u00170\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020 0\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\"\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020$0#0\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010%\u001a\u0008\u0012\u0004\u0012\u00020 0\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006@"
    }
    d2 = {
        "Lcom/squareup/payment/pending/RealPendingTransactionsStore;",
        "Lcom/squareup/payment/pending/PendingTransactionsStore;",
        "mainThread",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "res",
        "Lcom/squareup/util/Res;",
        "forwardedPaymentManager",
        "Lcom/squareup/payment/offline/ForwardedPaymentManager;",
        "storedPaymentsMonitor",
        "Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;",
        "pendingCapturesMonitor",
        "Lcom/squareup/queue/sqlite/PendingCapturesMonitor;",
        "localPaymentsMonitor",
        "Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "countSmoother",
        "Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;",
        "(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;Lcom/squareup/queue/sqlite/PendingCapturesMonitor;Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;)V",
        "allPendingTransactionsAsBillHistory",
        "Lio/reactivex/Observable;",
        "",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "allPendingTransactionsCount",
        "",
        "allTransactionSummaries",
        "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
        "disposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "hasPendingTransactions",
        "",
        "nonForwardedPendingTransactionsCount",
        "oldestStoredTransactionOrPendingCaptureTransaction",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/queue/CaptureTask;",
        "processingOfflinePayments",
        "ripenedAllPendingTransactionsCount",
        "smoothedAllPendingTransactionsCount",
        "smoothedRipenedAllPendingTransactionsCount",
        "allForwardedTransactionSummaries",
        "allForwardedTransactionsAsBillHistory",
        "fetchTransaction",
        "Lio/reactivex/Single;",
        "Lcom/squareup/transactionhistory/pending/PendingTransaction;",
        "transactionId",
        "",
        "forwardedTransactionAsBillHistory",
        "getOldestCaptureTask",
        "lhs",
        "rhs",
        "oldestStoredTransactionOrPendingCaptureTransactionTimestamp",
        "",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onProcessedForwardedTransactionSummaries",
        "onProcessedForwardedTransactionsAsBillHistory",
        "processingOfflineTransactions",
        "CountSmoother",
        "ProcessingOfflineTransactionsMonitor",
        "payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final allPendingTransactionsAsBillHistory:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation
.end field

.field private final allPendingTransactionsCount:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final allTransactionSummaries:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;>;"
        }
    .end annotation
.end field

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final forwardedPaymentManager:Lcom/squareup/payment/offline/ForwardedPaymentManager;

.field private final hasPendingTransactions:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final localPaymentsMonitor:Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final nonForwardedPendingTransactionsCount:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final oldestStoredTransactionOrPendingCaptureTransaction:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/queue/CaptureTask;",
            ">;>;"
        }
    .end annotation
.end field

.field private final pendingCapturesMonitor:Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

.field private final processingOfflinePayments:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final ripenedAllPendingTransactionsCount:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final smoothedAllPendingTransactionsCount:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final smoothedRipenedAllPendingTransactionsCount:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final storedPaymentsMonitor:Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;


# direct methods
.method public constructor <init>(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;Lcom/squareup/queue/sqlite/PendingCapturesMonitor;Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;)V
    .locals 5
    .param p1    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "mainThread"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "forwardedPaymentManager"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storedPaymentsMonitor"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pendingCapturesMonitor"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localPaymentsMonitor"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectivityMonitor"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countSmoother"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iput-object p2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p3, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->forwardedPaymentManager:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    iput-object p5, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->storedPaymentsMonitor:Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;

    iput-object p6, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->pendingCapturesMonitor:Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

    iput-object p7, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->localPaymentsMonitor:Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;

    .line 66
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 69
    sget-object p1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 70
    iget-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->storedPaymentsMonitor:Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;

    invoke-interface {p1}, Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;->distinctStoredPaymentsCount()Lrx/Observable;

    move-result-object p1

    const-string p2, "storedPaymentsMonitor.di\u2026inctStoredPaymentsCount()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    .line 71
    iget-object p3, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->pendingCapturesMonitor:Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

    invoke-interface {p3}, Lcom/squareup/queue/sqlite/PendingCapturesMonitor;->pendingCapturesCount()Lrx/Observable;

    move-result-object p3

    const-string p4, "pendingCapturesMonitor.pendingCapturesCount()"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p3}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p3

    .line 72
    iget-object p4, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->localPaymentsMonitor:Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;

    invoke-interface {p4}, Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;->localPaymentsCount()Lrx/Observable;

    move-result-object p4

    const-string p5, "localPaymentsMonitor.localPaymentsCount()"

    invoke-static {p4, p5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p4}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p4

    .line 499
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    check-cast p4, Lio/reactivex/ObservableSource;

    .line 500
    new-instance p5, Lcom/squareup/payment/pending/RealPendingTransactionsStore$$special$$inlined$combineLatest$1;

    invoke-direct {p5}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$$special$$inlined$combineLatest$1;-><init>()V

    check-cast p5, Lio/reactivex/functions/Function3;

    .line 498
    invoke-static {p1, p3, p4, p5}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p3, "Observable.combineLatest\u2026unction(t1, t2, t3) }\n  )"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    iget-object p3, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    const/4 p3, 0x1

    .line 77
    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    .line 78
    new-instance p4, Lcom/squareup/payment/pending/RealPendingTransactionsStore$nonForwardedPendingTransactionsCount$2;

    invoke-direct {p4, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$nonForwardedPendingTransactionsCount$2;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)V

    check-cast p4, Lio/reactivex/functions/Consumer;

    const/4 p5, 0x0

    invoke-virtual {p1, p5, p4}, Lio/reactivex/observables/ConnectableObservable;->autoConnect(ILio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p4, "Observables.combineLates\u2026) { disposables.add(it) }"

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->nonForwardedPendingTransactionsCount:Lio/reactivex/Observable;

    .line 81
    sget-object p1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 82
    iget-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->forwardedPaymentManager:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-virtual {p1}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->forwardedPaymentsCount()Lio/reactivex/Observable;

    move-result-object p1

    const-string p6, "forwardedPaymentManager.forwardedPaymentsCount()"

    invoke-static {p1, p6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-virtual {p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->nonForwardedPendingTransactionsCount()Lio/reactivex/Observable;

    move-result-object p7

    .line 503
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p7, Lio/reactivex/ObservableSource;

    .line 504
    new-instance v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$$special$$inlined$combineLatest$2;

    invoke-direct {v0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$$special$$inlined$combineLatest$2;-><init>()V

    check-cast v0, Lio/reactivex/functions/BiFunction;

    .line 502
    invoke-static {p1, p7, v0}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p7, "Observable.combineLatest\u2026ineFunction(t1, t2) }\n  )"

    invoke-static {p1, p7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 88
    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    .line 89
    new-instance v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$allPendingTransactionsCount$2;

    invoke-direct {v0, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$allPendingTransactionsCount$2;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, p5, v0}, Lio/reactivex/observables/ConnectableObservable;->autoConnect(ILio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->allPendingTransactionsCount:Lio/reactivex/Observable;

    .line 92
    invoke-virtual {p9}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;->getAllPendingTransactionsCount()Lio/reactivex/Observable;

    move-result-object p1

    .line 93
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 94
    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    .line 95
    new-instance v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$smoothedAllPendingTransactionsCount$1;

    invoke-direct {v0, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$smoothedAllPendingTransactionsCount$1;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, p5, v0}, Lio/reactivex/observables/ConnectableObservable;->autoConnect(ILio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "countSmoother.allPending\u2026) { disposables.add(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->smoothedAllPendingTransactionsCount:Lio/reactivex/Observable;

    .line 98
    sget-object p1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 99
    iget-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->forwardedPaymentManager:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-virtual {p1}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->forwardedPaymentsCount()Lio/reactivex/Observable;

    move-result-object p1

    invoke-static {p1, p6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    iget-object p6, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->storedPaymentsMonitor:Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;

    invoke-interface {p6}, Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;->distinctStoredPaymentsCount()Lrx/Observable;

    move-result-object p6

    invoke-static {p6, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p6}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p2

    .line 101
    iget-object p6, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->pendingCapturesMonitor:Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

    invoke-interface {p6}, Lcom/squareup/queue/sqlite/PendingCapturesMonitor;->ripenedPendingCapturesCount()Lrx/Observable;

    move-result-object p6

    const-string v0, "pendingCapturesMonitor.r\u2026nedPendingCapturesCount()"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p6}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p6

    .line 102
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->localPaymentsMonitor:Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;

    invoke-interface {v0}, Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;->ripenedLocalPaymentsCount()Lrx/Observable;

    move-result-object v0

    const-string v1, "localPaymentsMonitor.ripenedLocalPaymentsCount()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 507
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p6, Lio/reactivex/ObservableSource;

    check-cast v0, Lio/reactivex/ObservableSource;

    .line 508
    new-instance v1, Lcom/squareup/payment/pending/RealPendingTransactionsStore$$special$$inlined$combineLatest$3;

    invoke-direct {v1}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$$special$$inlined$combineLatest$3;-><init>()V

    check-cast v1, Lio/reactivex/functions/Function4;

    .line 506
    invoke-static {p1, p2, p6, v0, v1}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026ion(t1, t2, t3, t4) }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    iget-object p6, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p6}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 109
    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    .line 110
    new-instance p6, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ripenedAllPendingTransactionsCount$2;

    invoke-direct {p6, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ripenedAllPendingTransactionsCount$2;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)V

    check-cast p6, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, p5, p6}, Lio/reactivex/observables/ConnectableObservable;->autoConnect(ILio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->ripenedAllPendingTransactionsCount:Lio/reactivex/Observable;

    .line 113
    invoke-virtual {p9}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;->getRipenedAllPendingTransactionsCount()Lio/reactivex/Observable;

    move-result-object p1

    .line 114
    iget-object p6, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p6}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 115
    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    .line 116
    new-instance p6, Lcom/squareup/payment/pending/RealPendingTransactionsStore$smoothedRipenedAllPendingTransactionsCount$1;

    invoke-direct {p6, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$smoothedRipenedAllPendingTransactionsCount$1;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)V

    check-cast p6, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, p5, p6}, Lio/reactivex/observables/ConnectableObservable;->autoConnect(ILio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p6, "countSmoother.ripenedAll\u2026) { disposables.add(it) }"

    invoke-static {p1, p6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->smoothedRipenedAllPendingTransactionsCount:Lio/reactivex/Observable;

    .line 133
    sget-object p1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 134
    invoke-virtual {p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->allForwardedTransactionsAsBillHistory()Lio/reactivex/Observable;

    move-result-object p1

    .line 135
    iget-object p6, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->storedPaymentsMonitor:Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;

    iget-object p9, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->res:Lcom/squareup/util/Res;

    invoke-interface {p6, p9}, Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;->allDistinctStoredPaymentsAsBillHistory(Lcom/squareup/util/Res;)Lrx/Observable;

    move-result-object p6

    const-string p9, "storedPaymentsMonitor.al\u2026aymentsAsBillHistory(res)"

    invoke-static {p6, p9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p6}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p6

    .line 136
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->pendingCapturesMonitor:Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

    iget-object v1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, v1}, Lcom/squareup/queue/sqlite/PendingCapturesMonitor;->allPendingCapturesAsBillHistory(Lcom/squareup/util/Res;)Lrx/Observable;

    move-result-object v0

    const-string v1, "pendingCapturesMonitor.a\u2026apturesAsBillHistory(res)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 137
    iget-object v2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->localPaymentsMonitor:Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;

    iget-object v3, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->res:Lcom/squareup/util/Res;

    invoke-interface {v2, v3}, Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;->allLocalPaymentsAsBillHistory(Lcom/squareup/util/Res;)Lrx/Observable;

    move-result-object v2

    const-string v3, "localPaymentsMonitor.all\u2026aymentsAsBillHistory(res)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v2

    .line 511
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p6, Lio/reactivex/ObservableSource;

    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v2, Lio/reactivex/ObservableSource;

    .line 512
    new-instance v4, Lcom/squareup/payment/pending/RealPendingTransactionsStore$$special$$inlined$combineLatest$4;

    invoke-direct {v4}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$$special$$inlined$combineLatest$4;-><init>()V

    check-cast v4, Lio/reactivex/functions/Function4;

    .line 510
    invoke-static {p1, p6, v0, v2, v4}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    iget-object p6, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p6}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 144
    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    .line 145
    invoke-virtual {p1}, Lio/reactivex/observables/ConnectableObservable;->refCount()Lio/reactivex/Observable;

    move-result-object p1

    const-string p6, "Observables.combineLates\u2026ay(1)\n        .refCount()"

    invoke-static {p1, p6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->allTransactionSummaries:Lio/reactivex/Observable;

    .line 151
    sget-object p1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 152
    invoke-virtual {p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->allForwardedTransactionsAsBillHistory()Lio/reactivex/Observable;

    move-result-object p1

    .line 153
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->storedPaymentsMonitor:Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;

    iget-object v2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, v2}, Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;->allDistinctStoredPaymentsAsBillHistory(Lcom/squareup/util/Res;)Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p9

    .line 154
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->pendingCapturesMonitor:Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

    iget-object v2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, v2}, Lcom/squareup/queue/sqlite/PendingCapturesMonitor;->allPendingCapturesAsBillHistory(Lcom/squareup/util/Res;)Lrx/Observable;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 155
    iget-object v1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->localPaymentsMonitor:Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;

    iget-object v2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, v2}, Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;->allLocalPaymentsAsBillHistory(Lcom/squareup/util/Res;)Lrx/Observable;

    move-result-object v1

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v1

    .line 515
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p9, Lio/reactivex/ObservableSource;

    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 516
    new-instance v2, Lcom/squareup/payment/pending/RealPendingTransactionsStore$$special$$inlined$combineLatest$5;

    invoke-direct {v2}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$$special$$inlined$combineLatest$5;-><init>()V

    check-cast v2, Lio/reactivex/functions/Function4;

    .line 514
    invoke-static {p1, p9, v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    iget-object p2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 162
    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    .line 163
    invoke-virtual {p1}, Lio/reactivex/observables/ConnectableObservable;->refCount()Lio/reactivex/Observable;

    move-result-object p1

    invoke-static {p1, p6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->allPendingTransactionsAsBillHistory:Lio/reactivex/Observable;

    .line 167
    sget-object p1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 168
    iget-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->storedPaymentsMonitor:Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;

    invoke-interface {p1}, Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;->oldestStoredPayment()Lrx/Observable;

    move-result-object p1

    const-string p2, "storedPaymentsMonitor.oldestStoredPayment()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    .line 169
    sget-object p2, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$1;->INSTANCE:Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "storedPaymentsMonitor.ol\u2026ureTask() }\n            }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    iget-object p2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->pendingCapturesMonitor:Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

    invoke-interface {p2}, Lcom/squareup/queue/sqlite/PendingCapturesMonitor;->oldestPendingCapture()Lrx/Observable;

    move-result-object p2

    const-string p6, "pendingCapturesMonitor.oldestPendingCapture()"

    invoke-static {p2, p6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p2

    .line 173
    sget-object p6, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$2;->INSTANCE:Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$2;

    check-cast p6, Lio/reactivex/functions/Function;

    invoke-virtual {p2, p6}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p2

    const-string p6, "pendingCapturesMonitor.o\u2026ptureTask }\n            }"

    invoke-static {p2, p6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 519
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    .line 520
    new-instance p6, Lcom/squareup/payment/pending/RealPendingTransactionsStore$$special$$inlined$combineLatest$6;

    invoke-direct {p6, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$$special$$inlined$combineLatest$6;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)V

    check-cast p6, Lio/reactivex/functions/BiFunction;

    .line 518
    invoke-static {p1, p2, p6}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-static {p1, p7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    iget-object p2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 179
    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    .line 180
    new-instance p2, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$4;

    invoke-direct {p2, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$4;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)V

    check-cast p2, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, p5, p2}, Lio/reactivex/observables/ConnectableObservable;->autoConnect(ILio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->oldestStoredTransactionOrPendingCaptureTransaction:Lio/reactivex/Observable;

    .line 183
    iget-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->smoothedAllPendingTransactionsCount:Lio/reactivex/Observable;

    .line 184
    sget-object p2, Lcom/squareup/payment/pending/RealPendingTransactionsStore$hasPendingTransactions$1;->INSTANCE:Lcom/squareup/payment/pending/RealPendingTransactionsStore$hasPendingTransactions$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 185
    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    .line 186
    new-instance p2, Lcom/squareup/payment/pending/RealPendingTransactionsStore$hasPendingTransactions$2;

    invoke-direct {p2, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$hasPendingTransactions$2;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)V

    check-cast p2, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, p5, p2}, Lio/reactivex/observables/ConnectableObservable;->autoConnect(ILio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "smoothedAllPendingTransa\u2026) { disposables.add(it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->hasPendingTransactions:Lio/reactivex/Observable;

    .line 189
    new-instance p1, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;

    invoke-direct {p1, p0, p8}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;Lcom/squareup/connectivity/ConnectivityMonitor;)V

    .line 190
    invoke-virtual {p1}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;->processingOfflineTransactions()Lio/reactivex/Observable;

    move-result-object p1

    .line 191
    iget-object p2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 192
    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    .line 193
    new-instance p2, Lcom/squareup/payment/pending/RealPendingTransactionsStore$processingOfflinePayments$1;

    invoke-direct {p2, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$processingOfflinePayments$1;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)V

    check-cast p2, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, p5, p2}, Lio/reactivex/observables/ConnectableObservable;->autoConnect(ILio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "ProcessingOfflineTransac\u2026) { disposables.add(it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->processingOfflinePayments:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getDisposables$p(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)Lio/reactivex/disposables/CompositeDisposable;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)Lcom/squareup/util/Res;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->res:Lcom/squareup/util/Res;

    return-object p0
.end method


# virtual methods
.method public final allForwardedTransactionSummaries()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;>;"
        }
    .end annotation

    .line 255
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->forwardedPaymentManager:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-virtual {v0}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->allForwardedPayments()Lio/reactivex/Observable;

    move-result-object v0

    .line 256
    new-instance v1, Lcom/squareup/payment/pending/RealPendingTransactionsStore$allForwardedTransactionSummaries$1;

    invoke-direct {v1, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$allForwardedTransactionSummaries$1;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 257
    iget-object v1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x1

    .line 258
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object v0

    .line 259
    invoke-virtual {v0}, Lio/reactivex/observables/ConnectableObservable;->refCount()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "forwardedPaymentManager.\u2026ay(1)\n        .refCount()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final allForwardedTransactionsAsBillHistory()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation

    .line 279
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->forwardedPaymentManager:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-virtual {v0}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->allForwardedPayments()Lio/reactivex/Observable;

    move-result-object v0

    .line 280
    new-instance v1, Lcom/squareup/payment/pending/RealPendingTransactionsStore$allForwardedTransactionsAsBillHistory$1;

    invoke-direct {v1, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$allForwardedTransactionsAsBillHistory$1;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 283
    iget-object v1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x1

    .line 284
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object v0

    .line 285
    invoke-virtual {v0}, Lio/reactivex/observables/ConnectableObservable;->refCount()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "forwardedPaymentManager.\u2026ay(1)\n        .refCount()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public allPendingTransactionsAsBillHistory()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation

    .line 236
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->allPendingTransactionsAsBillHistory:Lio/reactivex/Observable;

    return-object v0
.end method

.method public allPendingTransactionsCount()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 218
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->allPendingTransactionsCount:Lio/reactivex/Observable;

    return-object v0
.end method

.method public allTransactionSummaries()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;>;"
        }
    .end annotation

    .line 233
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->allTransactionSummaries:Lio/reactivex/Observable;

    return-object v0
.end method

.method public fetchTransaction(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/transactionhistory/pending/PendingTransaction;",
            ">;>;"
        }
    .end annotation

    const-string v0, "transactionId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 325
    invoke-virtual {p0, p1}, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->forwardedTransactionAsBillHistory(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    check-cast v0, Lio/reactivex/SingleSource;

    .line 326
    iget-object v1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->storedPaymentsMonitor:Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;

    iget-object v2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, v2, p1}, Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;->fetchTransaction(Lcom/squareup/util/Res;Ljava/lang/String;)Lrx/Single;

    move-result-object v1

    const-string v2, "storedPaymentsMonitor.fe\u2026ction(res, transactionId)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object v1

    check-cast v1, Lio/reactivex/SingleSource;

    .line 327
    iget-object v2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->localPaymentsMonitor:Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;

    iget-object v3, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->res:Lcom/squareup/util/Res;

    invoke-interface {v2, v3, p1}, Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;->fetchTransaction(Lcom/squareup/util/Res;Ljava/lang/String;)Lrx/Single;

    move-result-object v2

    const-string v3, "localPaymentsMonitor.fet\u2026ction(res, transactionId)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object v2

    check-cast v2, Lio/reactivex/SingleSource;

    .line 328
    iget-object v3, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->pendingCapturesMonitor:Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

    iget-object v4, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->res:Lcom/squareup/util/Res;

    invoke-interface {v3, v4, p1}, Lcom/squareup/queue/sqlite/PendingCapturesMonitor;->fetchTransaction(Lcom/squareup/util/Res;Ljava/lang/String;)Lrx/Single;

    move-result-object p1

    const-string v3, "pendingCapturesMonitor.f\u2026ction(res, transactionId)"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p1

    check-cast p1, Lio/reactivex/SingleSource;

    .line 321
    invoke-static {v0, v1, v2, p1}, Lio/reactivex/Single;->concat(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;)Lio/reactivex/Flowable;

    move-result-object p1

    .line 330
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Flowable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Flowable;

    move-result-object p1

    .line 331
    sget-object v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$fetchTransaction$1;->INSTANCE:Lkotlin/reflect/KProperty1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/payment/pending/RealPendingTransactionsStoreKt$sam$io_reactivex_functions_Predicate$0;

    invoke-direct {v1, v0}, Lcom/squareup/payment/pending/RealPendingTransactionsStoreKt$sam$io_reactivex_functions_Predicate$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/functions/Predicate;

    invoke-virtual {p1, v0}, Lio/reactivex/Flowable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Flowable;

    move-result-object p1

    .line 332
    sget-object v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$fetchTransaction$2;->INSTANCE:Lcom/squareup/payment/pending/RealPendingTransactionsStore$fetchTransaction$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    if-eqz v0, :cond_1

    new-instance v1, Lcom/squareup/payment/pending/RealPendingTransactionsStoreKt$sam$io_reactivex_functions_Function$0;

    invoke-direct {v1, v0}, Lcom/squareup/payment/pending/RealPendingTransactionsStoreKt$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v0, v1

    :cond_1
    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Flowable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Flowable;

    move-result-object p1

    .line 333
    sget-object v0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v0}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Flowable;->first(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.concat(\n        /\u2026 .first(Optional.empty())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final forwardedTransactionAsBillHistory(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation

    const-string v0, "transactionId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 294
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->forwardedPaymentManager:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->forwardedPaymentWithId(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 295
    new-instance v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$forwardedTransactionAsBillHistory$1;

    invoke-direct {v0, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$forwardedTransactionAsBillHistory$1;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "forwardedPaymentManager.\u2026edPayment.asBill(res) } }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final getOldestCaptureTask(Lcom/squareup/util/Optional;Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "+",
            "Lcom/squareup/queue/CaptureTask;",
            ">;",
            "Lcom/squareup/util/Optional<",
            "+",
            "Lcom/squareup/queue/CaptureTask;",
            ">;)",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/queue/CaptureTask;",
            ">;"
        }
    .end annotation

    const-string v0, "lhs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rhs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 339
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 340
    sget-object p1, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {p1}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1

    .line 342
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    return-object p2

    .line 345
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-nez v0, :cond_2

    return-object p1

    .line 348
    :cond_2
    invoke-static {}, Lcom/squareup/util/ComparisonChain;->start()Lcom/squareup/util/ComparisonChain;

    move-result-object v0

    .line 349
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/queue/CaptureTask;

    invoke-interface {v1}, Lcom/squareup/queue/CaptureTask;->getTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    check-cast v1, Ljava/lang/Comparable;

    invoke-virtual {p2}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/queue/CaptureTask;

    invoke-interface {v2}, Lcom/squareup/queue/CaptureTask;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    check-cast v2, Ljava/lang/Comparable;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/ComparisonChain;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/util/ComparisonChain;

    move-result-object v0

    .line 351
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/queue/CaptureTask;

    invoke-interface {v1}, Lcom/squareup/queue/CaptureTask;->getAuthorizationId()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/Comparable;

    invoke-virtual {p2}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/queue/CaptureTask;

    invoke-interface {v2}, Lcom/squareup/queue/CaptureTask;->getAuthorizationId()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/Comparable;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/ComparisonChain;->compareWithNull(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/util/ComparisonChain;

    move-result-object v0

    .line 352
    invoke-virtual {v0}, Lcom/squareup/util/ComparisonChain;->result()I

    move-result v0

    if-gtz v0, :cond_3

    goto :goto_0

    :cond_3
    move-object p1, p2

    :goto_0
    return-object p1
.end method

.method public hasPendingTransactions()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 214
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->hasPendingTransactions:Lio/reactivex/Observable;

    return-object v0
.end method

.method public nonForwardedPendingTransactionsCount()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 230
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->nonForwardedPendingTransactionsCount:Lio/reactivex/Observable;

    return-object v0
.end method

.method public oldestStoredTransactionOrPendingCaptureTransactionTimestamp()J
    .locals 2

    .line 312
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 314
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->oldestStoredTransactionOrPendingCaptureTransaction:Lio/reactivex/Observable;

    .line 315
    sget-object v1, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransactionTimestamp$optionalTransaction$1;->INSTANCE:Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransactionTimestamp$optionalTransaction$1;

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 313
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2BlockingSupport;->getValueOrProvideDefault(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Optional;

    .line 316
    invoke-virtual {v0}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/CaptureTask;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/squareup/queue/CaptureTask;->getTime()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    const/16 v1, 0x8

    new-array v1, v1, [Lio/reactivex/disposables/Disposable;

    .line 199
    iget-object v2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->nonForwardedPendingTransactionsCount:Lio/reactivex/Observable;

    invoke-virtual {v2}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 200
    iget-object v2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->allPendingTransactionsCount:Lio/reactivex/Observable;

    invoke-virtual {v2}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 201
    iget-object v2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->smoothedAllPendingTransactionsCount:Lio/reactivex/Observable;

    invoke-virtual {v2}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    .line 202
    iget-object v2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->ripenedAllPendingTransactionsCount:Lio/reactivex/Observable;

    invoke-virtual {v2}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    .line 203
    iget-object v2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->smoothedRipenedAllPendingTransactionsCount:Lio/reactivex/Observable;

    invoke-virtual {v2}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const/4 v3, 0x4

    aput-object v2, v1, v3

    .line 204
    iget-object v2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->hasPendingTransactions:Lio/reactivex/Observable;

    invoke-virtual {v2}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const/4 v3, 0x5

    aput-object v2, v1, v3

    .line 205
    iget-object v2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->oldestStoredTransactionOrPendingCaptureTransaction:Lio/reactivex/Observable;

    invoke-virtual {v2}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const/4 v3, 0x6

    aput-object v2, v1, v3

    .line 206
    iget-object v2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->processingOfflinePayments:Lio/reactivex/Observable;

    invoke-virtual {v2}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const/4 v3, 0x7

    aput-object v2, v1, v3

    .line 198
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->addAll([Lio/reactivex/disposables/Disposable;)Z

    .line 208
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    check-cast v0, Lio/reactivex/disposables/Disposable;

    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onProcessedForwardedTransactionSummaries()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;>;"
        }
    .end annotation

    .line 299
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->forwardedPaymentManager:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    .line 300
    invoke-virtual {v0}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->onProcessedForwardedPayments()Lio/reactivex/Observable;

    move-result-object v0

    .line 301
    new-instance v1, Lcom/squareup/payment/pending/RealPendingTransactionsStore$onProcessedForwardedTransactionSummaries$1;

    invoke-direct {v1, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$onProcessedForwardedTransactionSummaries$1;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 302
    iget-object v1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "forwardedPaymentManager\n\u2026.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onProcessedForwardedTransactionsAsBillHistory()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation

    .line 305
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->forwardedPaymentManager:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-virtual {v0}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->onProcessedForwardedPayments()Lio/reactivex/Observable;

    move-result-object v0

    .line 306
    new-instance v1, Lcom/squareup/payment/pending/RealPendingTransactionsStore$onProcessedForwardedTransactionsAsBillHistory$1;

    invoke-direct {v1, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$onProcessedForwardedTransactionsAsBillHistory$1;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 309
    iget-object v1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "forwardedPaymentManager.\u2026.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public processingOfflineTransactions()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 216
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->processingOfflinePayments:Lio/reactivex/Observable;

    return-object v0
.end method

.method public ripenedAllPendingTransactionsCount()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 224
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->ripenedAllPendingTransactionsCount:Lio/reactivex/Observable;

    return-object v0
.end method

.method public smoothedAllPendingTransactionsCount()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 221
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->smoothedAllPendingTransactionsCount:Lio/reactivex/Observable;

    return-object v0
.end method

.method public smoothedRipenedAllPendingTransactionsCount()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 227
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->smoothedRipenedAllPendingTransactionsCount:Lio/reactivex/Observable;

    return-object v0
.end method
