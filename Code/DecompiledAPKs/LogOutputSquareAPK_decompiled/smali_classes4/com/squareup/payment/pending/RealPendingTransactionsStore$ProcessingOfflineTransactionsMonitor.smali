.class public final Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;
.super Ljava/lang/Object;
.source "RealPendingTransactionsStore.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/pending/RealPendingTransactionsStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ProcessingOfflineTransactionsMonitor"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealPendingTransactionsStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealPendingTransactionsStore.kt\ncom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,497:1\n57#2,4:498\n*E\n*S KotlinDebug\n*F\n+ 1 RealPendingTransactionsStore.kt\ncom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor\n*L\n464#1,4:498\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\u0008\u0087\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0015R&\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008\u0007\u0010\u0008\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR$\u0010\r\u001a\u00020\u000e8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008\u000f\u0010\u0008\u001a\u0004\u0008\u0010\u0010\u0011\"\u0004\u0008\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;",
        "",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "(Lcom/squareup/payment/pending/RealPendingTransactionsStore;Lcom/squareup/connectivity/ConnectivityMonitor;)V",
        "priorInternetState",
        "Lcom/squareup/connectivity/InternetState;",
        "priorInternetState$annotations",
        "()V",
        "getPriorInternetState",
        "()Lcom/squareup/connectivity/InternetState;",
        "setPriorInternetState",
        "(Lcom/squareup/connectivity/InternetState;)V",
        "processingOfflineTransactions",
        "",
        "processingOfflineTransactions$annotations",
        "getProcessingOfflineTransactions",
        "()Z",
        "setProcessingOfflineTransactions",
        "(Z)V",
        "processingOfflineTransactionsObservable",
        "Lio/reactivex/Observable;",
        "payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private priorInternetState:Lcom/squareup/connectivity/InternetState;

.field private processingOfflineTransactions:Z

.field private final processingOfflineTransactionsObservable:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/payment/pending/RealPendingTransactionsStore;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;Lcom/squareup/connectivity/ConnectivityMonitor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ")V"
        }
    .end annotation

    const-string v0, "connectivityMonitor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 458
    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 464
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 465
    invoke-interface {p2}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object p2

    const-string v0, "connectivityMonitor.internetState()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 466
    invoke-virtual {p1}, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->smoothedAllPendingTransactionsCount()Lio/reactivex/Observable;

    move-result-object p1

    .line 499
    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p1, Lio/reactivex/ObservableSource;

    .line 500
    new-instance v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor$$special$$inlined$combineLatest$1;

    invoke-direct {v0, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor$$special$$inlined$combineLatest$1;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;)V

    check-cast v0, Lio/reactivex/functions/BiFunction;

    .line 498
    invoke-static {p2, p1, v0}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026ineFunction(t1, t2) }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 489
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observables.combineLates\u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;->processingOfflineTransactionsObservable:Lio/reactivex/Observable;

    return-void
.end method

.method public static synthetic priorInternetState$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic processingOfflineTransactions$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final getPriorInternetState()Lcom/squareup/connectivity/InternetState;
    .locals 1

    .line 461
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;->priorInternetState:Lcom/squareup/connectivity/InternetState;

    return-object v0
.end method

.method public final getProcessingOfflineTransactions()Z
    .locals 1

    .line 460
    iget-boolean v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;->processingOfflineTransactions:Z

    return v0
.end method

.method public final processingOfflineTransactions()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 492
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;->processingOfflineTransactionsObservable:Lio/reactivex/Observable;

    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "processingOfflineTransac\u2026le.distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final setPriorInternetState(Lcom/squareup/connectivity/InternetState;)V
    .locals 0

    .line 461
    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;->priorInternetState:Lcom/squareup/connectivity/InternetState;

    return-void
.end method

.method public final setProcessingOfflineTransactions(Z)V
    .locals 0

    .line 460
    iput-boolean p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;->processingOfflineTransactions:Z

    return-void
.end method
