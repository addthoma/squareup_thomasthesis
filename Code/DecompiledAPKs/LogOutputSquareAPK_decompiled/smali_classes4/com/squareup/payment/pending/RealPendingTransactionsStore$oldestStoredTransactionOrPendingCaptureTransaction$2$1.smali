.class final Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$2$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealPendingTransactionsStore.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$2;->apply(Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/queue/retrofit/RetrofitTask<",
        "Ljava/lang/Object;",
        ">;",
        "Lcom/squareup/queue/CaptureTask;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012&\u0010\u0002\u001a\"\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u000b\u0012\u0002\u0008\u0003\u0018\u00010\u0003\u00a8\u0006\u00010\u0003\u00a8\u0006\u0001H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/queue/CaptureTask;",
        "it",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        "",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$2$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$2$1;

    invoke-direct {v0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$2$1;-><init>()V

    sput-object v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$2$1;->INSTANCE:Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$2$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/queue/retrofit/RetrofitTask;)Lcom/squareup/queue/CaptureTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/retrofit/RetrofitTask<",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/squareup/queue/CaptureTask;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 174
    check-cast p1, Lcom/squareup/queue/CaptureTask;

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.queue.CaptureTask"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 54
    check-cast p1, Lcom/squareup/queue/retrofit/RetrofitTask;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$2$1;->invoke(Lcom/squareup/queue/retrofit/RetrofitTask;)Lcom/squareup/queue/CaptureTask;

    move-result-object p1

    return-object p1
.end method
