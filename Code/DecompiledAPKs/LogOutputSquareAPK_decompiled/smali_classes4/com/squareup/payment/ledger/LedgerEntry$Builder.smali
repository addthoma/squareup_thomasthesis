.class Lcom/squareup/payment/ledger/LedgerEntry$Builder;
.super Ljava/lang/Object;
.source "LedgerEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/ledger/LedgerEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Builder"
.end annotation


# instance fields
.field cardBrand:Ljava/lang/String;

.field cardPanSuffix:Ljava/lang/String;

.field clientUniqueKey:Ljava/lang/String;

.field details:Lcom/squareup/wire/Message;

.field failureReason:Ljava/lang/String;

.field merchantId:Ljava/lang/String;

.field message:Ljava/lang/String;

.field otherTenderName:Ljava/lang/String;

.field paymentAmount:Ljava/lang/Long;

.field serverUniqueKey:Ljava/lang/String;

.field timestamp:Ljava/lang/String;

.field type:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;


# direct methods
.method constructor <init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V
    .locals 0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->type:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 65
    iput-object p2, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->merchantId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method build()Lcom/squareup/payment/ledger/LedgerEntry;
    .locals 14

    .line 134
    new-instance v13, Lcom/squareup/payment/ledger/LedgerEntry;

    iget-object v1, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->type:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->merchantId:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->clientUniqueKey:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->serverUniqueKey:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->timestamp:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->failureReason:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->paymentAmount:Ljava/lang/Long;

    iget-object v8, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->cardBrand:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->cardPanSuffix:Ljava/lang/String;

    iget-object v10, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->otherTenderName:Ljava/lang/String;

    iget-object v11, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->details:Lcom/squareup/wire/Message;

    iget-object v12, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->message:Ljava/lang/String;

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/squareup/payment/ledger/LedgerEntry;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/wire/Message;Ljava/lang/String;)V

    return-object v13
.end method

.method public setCardBrand(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->cardBrand:Ljava/lang/String;

    return-object p0
.end method

.method public setCardPanSuffix(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->cardPanSuffix:Ljava/lang/String;

    return-object p0
.end method

.method setClientUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->clientUniqueKey:Ljava/lang/String;

    return-object p0
.end method

.method setDetails(Lcom/squareup/wire/Message;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->details:Lcom/squareup/wire/Message;

    return-object p0
.end method

.method setFailureReason(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;
    .locals 0

    .line 94
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->failureReason:Ljava/lang/String;

    return-object p0
.end method

.method setMessage(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->message:Ljava/lang/String;

    return-object p0
.end method

.method public setOtherTenderName(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->otherTenderName:Ljava/lang/String;

    return-object p0
.end method

.method public setPaymentAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 99
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    :goto_0
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->paymentAmount:Ljava/lang/Long;

    return-object p0
.end method

.method public setPaymentAmount(Ljava/lang/Long;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->paymentAmount:Ljava/lang/Long;

    return-object p0
.end method

.method setServerUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->serverUniqueKey:Ljava/lang/String;

    return-object p0
.end method

.method setTimestamp(J)Lcom/squareup/payment/ledger/LedgerEntry$Builder;
    .locals 1

    .line 89
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-static {v0}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->timestamp:Ljava/lang/String;

    return-object p0
.end method

.method setTimestamp(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->timestamp:Ljava/lang/String;

    return-object p0
.end method

.method setTimestamp(Ljava/util/Date;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;
    .locals 0

    .line 84
    invoke-static {p1}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->timestamp:Ljava/lang/String;

    return-object p0
.end method
