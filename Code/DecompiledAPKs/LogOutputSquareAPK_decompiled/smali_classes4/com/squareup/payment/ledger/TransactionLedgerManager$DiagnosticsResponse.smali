.class public final Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;
.super Ljava/lang/Object;
.source "TransactionLedgerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/ledger/TransactionLedgerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DiagnosticsResponse"
.end annotation


# instance fields
.field public final responseString:Ljava/lang/String;

.field public final success:Z


# direct methods
.method public constructor <init>(ZLjava/lang/String;)V
    .locals 0

    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    iput-boolean p1, p0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;->success:Z

    .line 166
    iput-object p2, p0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;->responseString:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 171
    :cond_0
    instance-of v1, p1, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 172
    :cond_1
    check-cast p1, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;

    .line 173
    iget-boolean v1, p0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;->success:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v3, p1, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;->success:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;->responseString:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;->responseString:Ljava/lang/String;

    .line 174
    invoke-static {v1, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 178
    iget-boolean v1, p0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;->success:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;->responseString:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 182
    iget-boolean v1, p0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;->success:Z

    .line 183
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;->responseString:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "DiagnosticsResponse {success=%s, responseString=%s}"

    .line 182
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
