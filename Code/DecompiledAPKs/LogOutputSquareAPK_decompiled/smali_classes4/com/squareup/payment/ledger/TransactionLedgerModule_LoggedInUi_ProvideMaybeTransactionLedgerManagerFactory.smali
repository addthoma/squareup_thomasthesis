.class public final Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideMaybeTransactionLedgerManagerFactory;
.super Ljava/lang/Object;
.source "TransactionLedgerModule_LoggedInUi_ProvideMaybeTransactionLedgerManagerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final ledgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideMaybeTransactionLedgerManagerFactory;->ledgerManagerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideMaybeTransactionLedgerManagerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;)",
            "Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideMaybeTransactionLedgerManagerFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideMaybeTransactionLedgerManagerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideMaybeTransactionLedgerManagerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideMaybeTransactionLedgerManager(Lcom/squareup/payment/ledger/TransactionLedgerManager;)Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/payment/ledger/TransactionLedgerModule$LoggedInUi;->provideMaybeTransactionLedgerManager(Lcom/squareup/payment/ledger/TransactionLedgerManager;)Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideMaybeTransactionLedgerManagerFactory;->ledgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-static {v0}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideMaybeTransactionLedgerManagerFactory;->provideMaybeTransactionLedgerManager(Lcom/squareup/payment/ledger/TransactionLedgerManager;)Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideMaybeTransactionLedgerManagerFactory;->get()Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;

    move-result-object v0

    return-object v0
.end method
