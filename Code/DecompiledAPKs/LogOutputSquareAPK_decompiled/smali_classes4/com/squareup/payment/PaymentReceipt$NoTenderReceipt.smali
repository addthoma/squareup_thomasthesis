.class public Lcom/squareup/payment/PaymentReceipt$NoTenderReceipt;
.super Lcom/squareup/payment/PaymentReceipt;
.source "PaymentReceipt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/PaymentReceipt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NoTenderReceipt"
.end annotation


# direct methods
.method private constructor <init>(Lcom/squareup/payment/PaymentReceipt$Builder;)V
    .locals 1

    const/4 v0, 0x0

    .line 318
    invoke-direct {p0, p1, v0}, Lcom/squareup/payment/PaymentReceipt;-><init>(Lcom/squareup/payment/PaymentReceipt$Builder;Lcom/squareup/payment/PaymentReceipt$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/PaymentReceipt$Builder;Lcom/squareup/payment/PaymentReceipt$1;)V
    .locals 0

    .line 316
    invoke-direct {p0, p1}, Lcom/squareup/payment/PaymentReceipt$NoTenderReceipt;-><init>(Lcom/squareup/payment/PaymentReceipt$Builder;)V

    return-void
.end method


# virtual methods
.method public canAutoPrintReceipt()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public decline()V
    .locals 2

    .line 322
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "decline() should not be called in NoTenderReceipt"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public email(Ljava/lang/String;)V
    .locals 1

    .line 335
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "email() should not be called in NoTenderReceipt"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public emailDefault()V
    .locals 2

    .line 339
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "emailDefault() should be called in NoTenderReceipt"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public sms(Ljava/lang/String;)V
    .locals 1

    .line 326
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "sms() should not be called in NoTenderReceipt"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public smsDefault()V
    .locals 2

    .line 330
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "smsDefault() should not be called in NoTenderReceipt"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
