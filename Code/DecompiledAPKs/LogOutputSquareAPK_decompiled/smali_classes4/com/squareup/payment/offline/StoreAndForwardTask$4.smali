.class Lcom/squareup/payment/offline/StoreAndForwardTask$4;
.super Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;
.source "StoreAndForwardTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/offline/StoreAndForwardTask;->sendNextBatchToServer(Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/server/SquareCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;


# direct methods
.method constructor <init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/SquareCallback;)V
    .locals 0

    .line 235
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$4;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    invoke-direct {p0, p1, p2}, Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method


# virtual methods
.method protected doRun(Lcom/squareup/server/SquareCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 237
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    return-void
.end method
