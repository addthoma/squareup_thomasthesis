.class Lcom/squareup/payment/offline/StoreAndForwardTask$6;
.super Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;
.source "StoreAndForwardTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/offline/StoreAndForwardTask;->cleanupAndEnqueue(ZLcom/squareup/server/SquareCallback;Lcom/squareup/queue/StoredPaymentsQueue;Ljava/util/Set;ZLcom/squareup/payment/offline/StoredPayment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

.field final synthetic val$doRescheduleTask:Z

.field final synthetic val$excludeLastPayment:Z

.field final synthetic val$lastPaymentInBatch:Lcom/squareup/payment/offline/StoredPayment;

.field final synthetic val$paymentsToCleanup:Ljava/util/Set;

.field final synthetic val$storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;


# direct methods
.method constructor <init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/SquareCallback;Lcom/squareup/queue/StoredPaymentsQueue;Ljava/util/Set;ZZLcom/squareup/payment/offline/StoredPayment;)V
    .locals 0

    .line 347
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iput-object p3, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->val$storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;

    iput-object p4, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->val$paymentsToCleanup:Ljava/util/Set;

    iput-boolean p5, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->val$doRescheduleTask:Z

    iput-boolean p6, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->val$excludeLastPayment:Z

    iput-object p7, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->val$lastPaymentInBatch:Lcom/squareup/payment/offline/StoredPayment;

    invoke-direct {p0, p1, p2}, Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method


# virtual methods
.method protected doRun(Lcom/squareup/server/SquareCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 350
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->val$storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;

    iget-object v2, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->val$paymentsToCleanup:Ljava/util/Set;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/offline/StoreAndForwardTask;->cleanup(Lcom/squareup/queue/StoredPaymentsQueue;Ljava/util/Set;)V

    .line 351
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->val$storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;

    invoke-virtual {v0}, Lcom/squareup/queue/StoredPaymentsQueue;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 363
    :goto_0
    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v1, v1, Lcom/squareup/payment/offline/StoreAndForwardTask;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v2, Lcom/squareup/payment/offline/StoreAndForwardTask$6$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask$6$1;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask$6;Lcom/squareup/server/SquareCallback;Z)V

    invoke-interface {v1, v2}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
