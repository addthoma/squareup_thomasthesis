.class synthetic Lcom/squareup/payment/offline/StoreAndForwardTask$7;
.super Ljava/lang/Object;
.source "StoreAndForwardTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/offline/StoreAndForwardTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$protos$client$store_and_forward$bills$QueueBillResult$Status:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 304
    invoke-static {}, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;->values()[Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/payment/offline/StoreAndForwardTask$7;->$SwitchMap$com$squareup$protos$client$store_and_forward$bills$QueueBillResult$Status:[I

    :try_start_0
    sget-object v0, Lcom/squareup/payment/offline/StoreAndForwardTask$7;->$SwitchMap$com$squareup$protos$client$store_and_forward$bills$QueueBillResult$Status:[I

    sget-object v1, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;->RETRYABLE:Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;

    invoke-virtual {v1}, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/payment/offline/StoreAndForwardTask$7;->$SwitchMap$com$squareup$protos$client$store_and_forward$bills$QueueBillResult$Status:[I

    sget-object v1, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;->FAILED:Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;

    invoke-virtual {v1}, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method
