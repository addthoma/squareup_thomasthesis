.class public Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;
.super Ljava/lang/Object;
.source "ParsedStoreAndForwardKey.java"


# static fields
.field public static final ADAPTER:Lcom/squareup/encryption/CryptoKeyAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/encryption/CryptoKeyAdapter<",
            "Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final bletchleyKeyId:Ljava/lang/String;

.field public final clock:Lcom/squareup/util/Clock;

.field public final expirationTimeMillis:J

.field public final rawCertificate:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey$1;

    invoke-direct {v0}, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey$1;-><init>()V

    sput-object v0, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;->ADAPTER:Lcom/squareup/encryption/CryptoKeyAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Clock;Ljava/lang/String;[BJ)V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;->clock:Lcom/squareup/util/Clock;

    .line 38
    iput-object p2, p0, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;->bletchleyKeyId:Ljava/lang/String;

    .line 39
    iput-wide p4, p0, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;->expirationTimeMillis:J

    .line 40
    iput-object p3, p0, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;->rawCertificate:[B

    return-void
.end method


# virtual methods
.method public isExpired()Z
    .locals 5

    .line 44
    iget-wide v0, p0, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;->expirationTimeMillis:J

    iget-object v2, p0, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v2}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
