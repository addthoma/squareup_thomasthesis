.class public final Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;
.super Ljava/lang/Object;
.source "ForwardedPaymentManager_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/offline/ForwardedPaymentManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final forwardedPaymentsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/ForwardedPaymentsProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardBillServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/StoreAndForwardBillService;",
            ">;"
        }
    .end annotation
.end field

.field private final userIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/StoreAndForwardBillService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/ForwardedPaymentsProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;->badBusProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;->storeAndForwardBillServiceProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;->forwardedPaymentsProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;->userIdProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/StoreAndForwardBillService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/ForwardedPaymentsProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;"
        }
    .end annotation

    .line 55
    new-instance v7, Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lio/reactivex/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/badbus/BadBus;Lcom/squareup/server/bills/StoreAndForwardBillService;Lcom/squareup/payment/offline/ForwardedPaymentsProvider;Ljava/lang/String;)Lcom/squareup/payment/offline/ForwardedPaymentManager;
    .locals 8

    .line 61
    new-instance v7, Lcom/squareup/payment/offline/ForwardedPaymentManager;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/offline/ForwardedPaymentManager;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/badbus/BadBus;Lcom/squareup/server/bills/StoreAndForwardBillService;Lcom/squareup/payment/offline/ForwardedPaymentsProvider;Ljava/lang/String;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/payment/offline/ForwardedPaymentManager;
    .locals 7

    .line 47
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;->storeAndForwardBillServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/server/bills/StoreAndForwardBillService;

    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;->forwardedPaymentsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/payment/offline/ForwardedPaymentsProvider;

    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;->userIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    invoke-static/range {v1 .. v6}, Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;->newInstance(Lio/reactivex/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/badbus/BadBus;Lcom/squareup/server/bills/StoreAndForwardBillService;Lcom/squareup/payment/offline/ForwardedPaymentsProvider;Ljava/lang/String;)Lcom/squareup/payment/offline/ForwardedPaymentManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/payment/offline/ForwardedPaymentManager_Factory;->get()Lcom/squareup/payment/offline/ForwardedPaymentManager;

    move-result-object v0

    return-object v0
.end method
