.class public Lcom/squareup/payment/offline/StoreAndForwardPaymentService;
.super Ljava/lang/Object;
.source "StoreAndForwardPaymentService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/offline/StoreAndForwardPaymentService$ByteStringAdapter;,
        Lcom/squareup/payment/offline/StoreAndForwardPaymentService$EnqueueStoredPayment;
    }
.end annotation


# instance fields
.field private final billInFlightAwaitingReceiptInfo:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/payment/offline/BillInFlight;",
            ">;"
        }
    .end annotation
.end field

.field private final bus:Lcom/squareup/badbus/BadEventSink;

.field private final merchantKeyManager:Lcom/squareup/payment/offline/MerchantKeyManager;

.field private final queueBertPublicKeyManager:Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

.field private final sessionToken:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final squareHeaders:Lcom/squareup/http/SquareHeaders;

.field private final transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;


# direct methods
.method public constructor <init>(Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/AccountStatusSettings;Landroid/content/SharedPreferences;Lcom/squareup/payment/offline/QueueBertPublicKeyManager;Lcom/squareup/payment/offline/MerchantKeyManager;Ljavax/inject/Provider;Lcom/google/gson/Gson;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/http/SquareHeaders;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Landroid/content/SharedPreferences;",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;",
            "Lcom/squareup/payment/offline/MerchantKeyManager;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/gson/Gson;",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            "Lcom/squareup/http/SquareHeaders;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 55
    const-class v0, Lcom/squareup/payment/offline/BillInFlight;

    const-string v1, "store-and-forward-bill-in-flight.json"

    move-object v2, p3

    move-object/from16 v3, p7

    .line 57
    invoke-static {p3, v1, v3, v0}, Lcom/squareup/settings/GsonLocalSetting;->forClass(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/Class;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object v10

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p9

    move-object/from16 v9, p8

    .line 55
    invoke-direct/range {v2 .. v10}, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;-><init>(Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/offline/QueueBertPublicKeyManager;Lcom/squareup/payment/offline/MerchantKeyManager;Ljavax/inject/Provider;Lcom/squareup/http/SquareHeaders;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/settings/LocalSetting;)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/offline/QueueBertPublicKeyManager;Lcom/squareup/payment/offline/MerchantKeyManager;Ljavax/inject/Provider;Lcom/squareup/http/SquareHeaders;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;",
            "Lcom/squareup/payment/offline/MerchantKeyManager;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/http/SquareHeaders;",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/payment/offline/BillInFlight;",
            ">;)V"
        }
    .end annotation

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->bus:Lcom/squareup/badbus/BadEventSink;

    .line 68
    iput-object p2, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 69
    iput-object p3, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->queueBertPublicKeyManager:Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

    .line 70
    iput-object p4, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->merchantKeyManager:Lcom/squareup/payment/offline/MerchantKeyManager;

    .line 71
    iput-object p5, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->sessionToken:Ljavax/inject/Provider;

    .line 72
    iput-object p6, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->squareHeaders:Lcom/squareup/http/SquareHeaders;

    .line 73
    iput-object p7, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    .line 74
    iput-object p8, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->billInFlightAwaitingReceiptInfo:Lcom/squareup/settings/LocalSetting;

    return-void
.end method


# virtual methods
.method public enqueueBillInFlightAwaitingReceiptInfo()V
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->billInFlightAwaitingReceiptInfo:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/offline/BillInFlight;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 85
    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->enqueuePayment(Lcom/squareup/payment/offline/BillInFlight;Z)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :catch_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->billInFlightAwaitingReceiptInfo:Lcom/squareup/settings/LocalSetting;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public enqueuePayment(Lcom/squareup/payment/offline/BillInFlight;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 97
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/payment/offline/BillInFlight;->logReady(Lcom/squareup/payment/ledger/TransactionLedgerManager;Z)V

    .line 100
    :try_start_0
    iget-object v2, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->queueBertPublicKeyManager:Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

    iget-object v3, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->merchantKeyManager:Lcom/squareup/payment/offline/MerchantKeyManager;

    iget-object v4, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v5, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->sessionToken:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->squareHeaders:Lcom/squareup/http/SquareHeaders;

    move-object v1, p1

    move v6, p2

    .line 101
    invoke-virtual/range {v1 .. v7}, Lcom/squareup/payment/offline/BillInFlight;->createStoredPayment(Lcom/squareup/payment/offline/QueueBertPublicKeyManager;Lcom/squareup/payment/offline/MerchantKeyManager;Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;ZLcom/squareup/http/SquareHeaders;)Lcom/squareup/payment/offline/StoredPayment;

    move-result-object v0

    if-eqz p2, :cond_0

    .line 106
    iget-object p2, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->billInFlightAwaitingReceiptInfo:Lcom/squareup/settings/LocalSetting;

    invoke-interface {p2}, Lcom/squareup/settings/LocalSetting;->remove()V

    goto :goto_0

    .line 109
    :cond_0
    iget-object p2, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->billInFlightAwaitingReceiptInfo:Lcom/squareup/settings/LocalSetting;

    invoke-interface {p2, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 112
    :goto_0
    iget-object p2, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->bus:Lcom/squareup/badbus/BadEventSink;

    new-instance v1, Lcom/squareup/payment/offline/StoreAndForwardPaymentService$EnqueueStoredPayment;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/squareup/payment/offline/StoreAndForwardPaymentService$EnqueueStoredPayment;-><init>(Lcom/squareup/payment/offline/StoredPayment;Lcom/squareup/payment/offline/StoreAndForwardPaymentService$1;)V

    invoke-interface {p2, v1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p2

    .line 115
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/payment/offline/BillInFlight;->logFailed(Lcom/squareup/payment/ledger/TransactionLedgerManager;Ljava/lang/String;)V

    .line 116
    throw p2
.end method
