.class Lcom/squareup/payment/offline/StoreAndForwardTask$2;
.super Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;
.source "StoreAndForwardTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/offline/StoreAndForwardTask;->sendQueueToServer(Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/server/SquareCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

.field final synthetic val$storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;


# direct methods
.method constructor <init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/SquareCallback;Lcom/squareup/queue/StoredPaymentsQueue;)V
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$2;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iput-object p3, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$2;->val$storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;

    invoke-direct {p0, p1, p2}, Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method


# virtual methods
.method protected doRun(Lcom/squareup/server/SquareCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 158
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$2;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$2;->val$storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;

    invoke-static {v0, v1}, Lcom/squareup/payment/offline/StoreAndForwardTask;->access$100(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/queue/StoredPaymentsQueue;)Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;

    move-result-object v0

    .line 161
    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$2;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v1, v1, Lcom/squareup/payment/offline/StoreAndForwardTask;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v2, Lcom/squareup/payment/offline/StoreAndForwardTask$2$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask$2$1;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask$2;Lcom/squareup/server/SquareCallback;Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;)V

    invoke-interface {v1, v2}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
