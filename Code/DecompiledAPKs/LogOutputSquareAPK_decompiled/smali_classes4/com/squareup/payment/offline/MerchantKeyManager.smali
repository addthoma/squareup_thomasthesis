.class Lcom/squareup/payment/offline/MerchantKeyManager;
.super Ljava/lang/Object;
.source "MerchantKeyManager.java"


# static fields
.field private static final MERCHANT_KEY_ADAPTER:Lcom/squareup/encryption/CryptoKeyAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/encryption/CryptoKeyAdapter<",
            "Lcom/squareup/server/account/protos/User$MerchantKey;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private authCodeGenerator:Lcom/squareup/encryption/CryptoPrimitive;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/encryption/CryptoPrimitive<",
            "Lcom/squareup/server/account/protos/User$MerchantKey;",
            ">;"
        }
    .end annotation
.end field

.field private merchantKey:Lcom/squareup/server/account/protos/User$MerchantKey;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19
    new-instance v0, Lcom/squareup/payment/offline/MerchantKeyManager$1;

    invoke-direct {v0}, Lcom/squareup/payment/offline/MerchantKeyManager$1;-><init>()V

    sput-object v0, Lcom/squareup/payment/offline/MerchantKeyManager;->MERCHANT_KEY_ADAPTER:Lcom/squareup/encryption/CryptoKeyAdapter;

    return-void
.end method

.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/payment/offline/MerchantKeyManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method private updateMerchantKey()V
    .locals 3

    .line 65
    iget-object v0, p0, Lcom/squareup/payment/offline/MerchantKeyManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getMerchantKey()Lcom/squareup/server/account/protos/User$MerchantKey;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 68
    :cond_0
    iget-object v1, v0, Lcom/squareup/server/account/protos/User$MerchantKey;->key:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 69
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Bad merchant key from server"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    return-void

    .line 74
    :cond_1
    iget-object v1, p0, Lcom/squareup/payment/offline/MerchantKeyManager;->merchantKey:Lcom/squareup/server/account/protos/User$MerchantKey;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/squareup/server/account/protos/User$MerchantKey;->master_key_id:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/payment/offline/MerchantKeyManager;->merchantKey:Lcom/squareup/server/account/protos/User$MerchantKey;

    iget-object v2, v2, Lcom/squareup/server/account/protos/User$MerchantKey;->master_key_id:Ljava/lang/Integer;

    if-ne v1, v2, :cond_2

    return-void

    .line 78
    :cond_2
    iput-object v0, p0, Lcom/squareup/payment/offline/MerchantKeyManager;->merchantKey:Lcom/squareup/server/account/protos/User$MerchantKey;

    const/4 v0, 0x0

    .line 79
    iput-object v0, p0, Lcom/squareup/payment/offline/MerchantKeyManager;->authCodeGenerator:Lcom/squareup/encryption/CryptoPrimitive;

    return-void
.end method


# virtual methods
.method public declared-synchronized getAuthCodeGenerator()Lcom/squareup/encryption/CryptoPrimitive;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/encryption/CryptoPrimitive<",
            "Lcom/squareup/server/account/protos/User$MerchantKey;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    monitor-enter p0

    .line 46
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/payment/offline/MerchantKeyManager;->updateMerchantKey()V

    .line 48
    iget-object v0, p0, Lcom/squareup/payment/offline/MerchantKeyManager;->authCodeGenerator:Lcom/squareup/encryption/CryptoPrimitive;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/offline/MerchantKeyManager;->authCodeGenerator:Lcom/squareup/encryption/CryptoPrimitive;

    invoke-interface {v0}, Lcom/squareup/encryption/CryptoPrimitive;->isExpired()Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/squareup/payment/offline/MerchantKeyManager;->authCodeGenerator:Lcom/squareup/encryption/CryptoPrimitive;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    .line 52
    :try_start_1
    iput-object v0, p0, Lcom/squareup/payment/offline/MerchantKeyManager;->authCodeGenerator:Lcom/squareup/encryption/CryptoPrimitive;

    .line 54
    iget-object v0, p0, Lcom/squareup/payment/offline/MerchantKeyManager;->merchantKey:Lcom/squareup/server/account/protos/User$MerchantKey;

    if-eqz v0, :cond_1

    .line 55
    new-instance v0, Lcom/squareup/encryption/HmacSha256Generator;

    iget-object v1, p0, Lcom/squareup/payment/offline/MerchantKeyManager;->merchantKey:Lcom/squareup/server/account/protos/User$MerchantKey;

    sget-object v2, Lcom/squareup/payment/offline/MerchantKeyManager;->MERCHANT_KEY_ADAPTER:Lcom/squareup/encryption/CryptoKeyAdapter;

    invoke-direct {v0, v1, v2}, Lcom/squareup/encryption/HmacSha256Generator;-><init>(Ljava/lang/Object;Lcom/squareup/encryption/CryptoKeyAdapter;)V

    iput-object v0, p0, Lcom/squareup/payment/offline/MerchantKeyManager;->authCodeGenerator:Lcom/squareup/encryption/CryptoPrimitive;

    goto :goto_0

    :cond_1
    const-string v0, "Invalid or expired merchant key"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 57
    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    :goto_0
    iget-object v0, p0, Lcom/squareup/payment/offline/MerchantKeyManager;->authCodeGenerator:Lcom/squareup/encryption/CryptoPrimitive;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
