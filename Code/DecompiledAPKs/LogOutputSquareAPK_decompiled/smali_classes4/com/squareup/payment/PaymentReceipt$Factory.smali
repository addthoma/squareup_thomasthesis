.class public interface abstract Lcom/squareup/payment/PaymentReceipt$Factory;
.super Ljava/lang/Object;
.source "PaymentReceipt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/PaymentReceipt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Factory"
.end annotation


# virtual methods
.method public abstract createTenderReceipt(Lcom/squareup/payment/Obfuscated;Lcom/squareup/payment/Obfuscated;)Lcom/squareup/payment/PaymentReceipt;
.end method

.method public abstract createVoidReceipt()Lcom/squareup/payment/PaymentReceipt;
.end method
