.class public final Lcom/squareup/reports/applet/sales/SalesReportSection;
.super Lcom/squareup/applet/AppletSection;
.source "SalesReportSection.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/SalesReportSection;",
        "Lcom/squareup/applet/AppletSection;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "salesReportDetailLevelHolder",
        "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;)V",
        "getInitialScreen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesReportDetailLevelHolder"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    new-instance v0, Lcom/squareup/reports/applet/sales/SalesReportAccess;

    invoke-direct {v0, p1}, Lcom/squareup/reports/applet/sales/SalesReportAccess;-><init>(Lcom/squareup/settings/server/Features;)V

    check-cast v0, Lcom/squareup/applet/SectionAccess;

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/SalesReportSection;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/SalesReportSection;->salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    return-void
.end method


# virtual methods
.method public getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 2

    .line 20
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/SalesReportSection;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SALES_REPORT_V2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    new-instance v0, Lcom/squareup/reports/applet/sales/v2/SalesReportBootstrapScreen;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/SalesReportSection;->salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    invoke-interface {v1}, Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;->getDetailLevel()Lcom/squareup/api/salesreport/DetailLevel;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/reports/applet/sales/v2/SalesReportBootstrapScreen;-><init>(Lcom/squareup/api/salesreport/DetailLevel;)V

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    goto :goto_0

    .line 23
    :cond_0
    sget-object v0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;

    const-string v1, "SalesReportScreen.INSTANCE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    :goto_0
    return-object v0
.end method
