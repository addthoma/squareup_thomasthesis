.class public final Lcom/squareup/reports/applet/drawer/CurrentDrawerView_MembersInjector;
.super Ljava/lang/Object;
.source "CurrentDrawerView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/reports/applet/drawer/CurrentDrawerView;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView_MembersInjector;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerView;",
            ">;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectFeatures(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;Ljava/lang/Object;)V
    .locals 0

    .line 47
    check-cast p1, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->presenter:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    return-void
.end method

.method public static injectPriceLocaleHelper(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;Lcom/squareup/money/PriceLocaleHelper;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView_MembersInjector;->injectPresenter(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;Ljava/lang/Object;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView_MembersInjector;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/money/PriceLocaleHelper;

    invoke-static {p1, v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView_MembersInjector;->injectPriceLocaleHelper(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;Lcom/squareup/money/PriceLocaleHelper;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView_MembersInjector;->injectFeatures(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;Lcom/squareup/settings/server/Features;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView_MembersInjector;->injectMembers(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)V

    return-void
.end method
