.class final Lcom/squareup/reports/applet/ReportsApplet$badge$1;
.super Ljava/lang/Object;
.source "ReportsApplet.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/ReportsApplet;->badge()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/applet/Applet$Badge;",
        "hasActive",
        "",
        "apply",
        "(Ljava/lang/Boolean;)Lcom/squareup/applet/Applet$Badge;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/reports/applet/ReportsApplet;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/ReportsApplet;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/reports/applet/ReportsApplet$badge$1;->this$0:Lcom/squareup/reports/applet/ReportsApplet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Boolean;)Lcom/squareup/applet/Applet$Badge;
    .locals 3

    const-string v0, "hasActive"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/squareup/applet/Applet$Badge$Visible;

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/squareup/reports/applet/ReportsApplet$badge$1;->this$0:Lcom/squareup/reports/applet/ReportsApplet;

    invoke-static {v1}, Lcom/squareup/reports/applet/ReportsApplet;->access$getResources$p(Lcom/squareup/reports/applet/ReportsApplet;)Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/reports/applet/R$string;->new_badge:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "resources.getText(R.string.new_badge)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/squareup/applet/Applet$Badge$Priority;->HIGH:Lcom/squareup/applet/Applet$Badge$Priority;

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/applet/Applet$Badge$Visible;-><init>(ILjava/lang/CharSequence;Lcom/squareup/applet/Applet$Badge$Priority;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/applet/Applet$Badge$Hidden;->INSTANCE:Lcom/squareup/applet/Applet$Badge$Hidden;

    :goto_0
    check-cast p1, Lcom/squareup/applet/Applet$Badge;

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 37
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/ReportsApplet$badge$1;->apply(Ljava/lang/Boolean;)Lcom/squareup/applet/Applet$Badge;

    move-result-object p1

    return-object p1
.end method
