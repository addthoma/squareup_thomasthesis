.class public Lcom/squareup/reports/applet/ReportsAppletSectionsList;
.super Lcom/squareup/applet/AppletSectionsList;
.source "ReportsAppletSectionsList.java"


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/ReportsAppletEntryPoint;Lcom/squareup/reports/applet/ReportsAppletSections;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0, p1}, Lcom/squareup/applet/AppletSectionsList;-><init>(Lcom/squareup/applet/AppletEntryPoint;)V

    .line 16
    invoke-interface {p2}, Lcom/squareup/reports/applet/ReportsAppletSections;->orderedEntries()Ljava/util/List;

    move-result-object p1

    .line 17
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/applet/AppletSectionsListEntry;

    .line 18
    iget-object v0, p2, Lcom/squareup/applet/AppletSectionsListEntry;->section:Lcom/squareup/applet/AppletSection;

    iget-object v0, v0, Lcom/squareup/applet/AppletSection;->accessControl:Lcom/squareup/applet/SectionAccess;

    invoke-virtual {v0}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsList;->visibleEntries:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method
