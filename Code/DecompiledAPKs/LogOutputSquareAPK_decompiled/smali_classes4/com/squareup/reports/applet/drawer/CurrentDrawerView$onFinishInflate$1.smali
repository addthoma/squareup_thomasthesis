.class final Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$1;
.super Ljava/lang/Object;
.source "CurrentDrawerView.kt"

# interfaces
.implements Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCurrentDrawerView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CurrentDrawerView.kt\ncom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$1\n*L\n1#1,162:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "onConfirm"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/reports/applet/drawer/CurrentDrawerView;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$1;->this$0:Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onConfirm()V
    .locals 3

    .line 70
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$1;->this$0:Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    invoke-static {v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->access$getStartingCash$p(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)Lcom/squareup/widgets/SelectableEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 71
    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_1

    .line 72
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$1;->this$0:Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    invoke-static {v1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 73
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$1;->this$0:Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    invoke-virtual {v1}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->getPresenter$reports_applet_release()Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$1;->this$0:Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    invoke-virtual {v2}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->getPriceLocaleHelper$reports_applet_release()Lcom/squareup/money/PriceLocaleHelper;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->startCashDrawerShift(Lcom/squareup/protos/common/Money;)V

    :cond_1
    return-void
.end method
