.class public interface abstract Lcom/squareup/reports/applet/ReportsAppletReleaseComponent$ReleaseTabletComponent;
.super Ljava/lang/Object;
.source "ReportsAppletReleaseComponent.kt"

# interfaces
.implements Lcom/squareup/reports/applet/ReportsAppletParentComponent$TabletComponent;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/reports/applet/ReportsAppletScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/reports/applet/ReportsAppletReleaseModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/ReportsAppletReleaseComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ReleaseTabletComponent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u0001\u00a8\u0006\u0002"
    }
    d2 = {
        "Lcom/squareup/reports/applet/ReportsAppletReleaseComponent$ReleaseTabletComponent;",
        "Lcom/squareup/reports/applet/ReportsAppletParentComponent$TabletComponent;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
