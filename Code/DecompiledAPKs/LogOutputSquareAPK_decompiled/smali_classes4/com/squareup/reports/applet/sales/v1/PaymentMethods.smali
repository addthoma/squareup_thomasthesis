.class Lcom/squareup/reports/applet/sales/v1/PaymentMethods;
.super Ljava/lang/Object;
.source "PaymentMethods.java"


# instance fields
.field final cardMoney:Lcom/squareup/protos/common/Money;

.field final cashMoney:Lcom/squareup/protos/common/Money;

.field final giftCardMoney:Lcom/squareup/protos/common/Money;

.field final otherMoney:Lcom/squareup/protos/common/Money;

.field final thirdPartyCardMoney:Lcom/squareup/protos/common/Money;


# direct methods
.method constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/PaymentMethods;->cardMoney:Lcom/squareup/protos/common/Money;

    .line 15
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/PaymentMethods;->cashMoney:Lcom/squareup/protos/common/Money;

    .line 16
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/PaymentMethods;->giftCardMoney:Lcom/squareup/protos/common/Money;

    .line 17
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/PaymentMethods;->otherMoney:Lcom/squareup/protos/common/Money;

    .line 18
    iput-object p5, p0, Lcom/squareup/reports/applet/sales/v1/PaymentMethods;->thirdPartyCardMoney:Lcom/squareup/protos/common/Money;

    return-void
.end method
