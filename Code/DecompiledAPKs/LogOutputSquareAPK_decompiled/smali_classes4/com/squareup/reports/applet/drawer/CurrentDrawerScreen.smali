.class public final Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen;
.super Lcom/squareup/reports/applet/InReportsAppletScope;
.source "CurrentDrawerScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Component;,
        Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 67
    new-instance v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen;

    invoke-direct {v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen;->INSTANCE:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen;

    .line 329
    sget-object v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen;->INSTANCE:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 69
    invoke-direct {p0}, Lcom/squareup/reports/applet/InReportsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 73
    const-class v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 332
    sget v0, Lcom/squareup/reports/applet/R$layout;->current_drawer_view:I

    return v0
.end method
