.class public Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;
.super Ljava/lang/Object;
.source "DiscountReportRow.java"


# instance fields
.field public final discountName:Ljava/lang/String;

.field public final formattedDiscountMoney:Ljava/lang/String;

.field public final formattedQuantity:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;->discountName:Ljava/lang/String;

    .line 11
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;->formattedQuantity:Ljava/lang/String;

    .line 12
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;->formattedDiscountMoney:Ljava/lang/String;

    return-void
.end method
