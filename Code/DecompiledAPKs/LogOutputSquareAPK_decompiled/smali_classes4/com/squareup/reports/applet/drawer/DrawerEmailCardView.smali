.class public Lcom/squareup/reports/applet/drawer/DrawerEmailCardView;
.super Lcom/squareup/reports/applet/BaseEmailCardView;
.source "DrawerEmailCardView.java"


# instance fields
.field presenter:Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/squareup/reports/applet/BaseEmailCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    const-class p2, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Component;->inject(Lcom/squareup/reports/applet/drawer/DrawerEmailCardView;)V

    return-void
.end method


# virtual methods
.method public getPresenter()Lcom/squareup/reports/applet/BaseEmailCardPresenter;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailCardView;->presenter:Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;

    return-object v0
.end method
