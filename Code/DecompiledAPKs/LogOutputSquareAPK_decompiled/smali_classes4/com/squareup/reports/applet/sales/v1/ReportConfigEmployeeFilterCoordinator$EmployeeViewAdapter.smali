.class final Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "ReportConfigEmployeeFilterCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "EmployeeViewAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReportConfigEmployeeFilterCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReportConfigEmployeeFilterCoordinator.kt\ncom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter\n*L\n1#1,177:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u000cH\u0016J\u0018\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u000cH\u0016J\u000e\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0016\u001a\u00020\u0017R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;",
        "runner",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;",
        "(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)V",
        "employees",
        "",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;",
        "getRunner",
        "()Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;",
        "getItemCount",
        "",
        "onBindViewHolder",
        "",
        "holder",
        "position",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "viewType",
        "setEmployees",
        "screenData",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$FilterByEmployee;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final employees:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;",
            ">;"
        }
    .end annotation
.end field

.field private final runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)V
    .locals 1

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    .line 96
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;->employees:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;->employees:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    return v0
.end method

.method public final getRunner()Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    return-object v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 92
    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;->onBindViewHolder(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;I)V
    .locals 1

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;->employees:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;

    invoke-virtual {p1, p2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;->bind(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 92
    invoke-virtual {p0, p1, p2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;
    .locals 2

    const-string p2, "parent"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    new-instance p2, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;

    .line 118
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    sget v1, Lcom/squareup/reports/applet/R$layout;->report_config_employees_row:I

    invoke-static {v1, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 117
    invoke-direct {p2, v0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;Landroid/view/View;)V

    return-object p2
.end method

.method public final setEmployees(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$FilterByEmployee;)V
    .locals 1

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;->employees:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 100
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;->employees:Ljava/util/List;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$FilterByEmployee;->getListedEmployees()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 101
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;->notifyDataSetChanged()V

    return-void
.end method
