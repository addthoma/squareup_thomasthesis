.class final Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "ReportConfigEmployeeFilterCoordinator.kt"

# interfaces
.implements Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "EmployeeViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u000e\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fR\u0014\u0010\u0008\u001a\u00020\tX\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        "Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;",
        "runner",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;",
        "itemView",
        "Landroid/view/View;",
        "(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;Landroid/view/View;)V",
        "edges",
        "",
        "getEdges",
        "()I",
        "bind",
        "",
        "employee",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final edges:I

.field private final runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;Landroid/view/View;)V
    .locals 1

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemView"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    const/16 p1, 0xa

    .line 127
    iput p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;->edges:I

    return-void
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;)Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    return-object p0
.end method


# virtual methods
.method public final bind(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;)V
    .locals 4

    const-string v0, "employee"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/reports/applet/R$id;->report_config_employee_row:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 132
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;->itemView:Landroid/view/View;

    const-string v2, "itemView"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/reports/applet/R$string;->report_config_employee_selection_name:I

    invoke-static {v1, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 133
    invoke-virtual {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;->getFirstName()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    const-string v3, "first_name"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 134
    invoke-virtual {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;->getLastName()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    const-string v3, "last_name"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 135
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 136
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 131
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setText(Ljava/lang/CharSequence;)V

    const-string v1, "employeeRow"

    .line 138
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;->isSelected()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 139
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder$bind$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder$bind$1;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public getEdges()I
    .locals 1

    .line 127
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;->edges:I

    return v0
.end method

.method public getPadding(Landroid/graphics/Rect;)V
    .locals 1

    const-string v0, "outRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges$DefaultImpls;->getPadding(Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;Landroid/graphics/Rect;)V

    return-void
.end method
