.class Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ReportConfigScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;",
        ">;"
    }
.end annotation


# static fields
.field private static final DISPLAYED_MONTH_KEY:Ljava/lang/String; = "DISPLAYED_MONTH"

.field private static final DISPLAYED_YEAR_KEY:Ljava/lang/String; = "DISPLAYED_YEAR"


# instance fields
.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private monthAndYearSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;Lcom/squareup/permissions/EmployeeManagement;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 49
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 44
    invoke-static {}, Lrx/subjects/BehaviorSubject;->create()Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->monthAndYearSubject:Lrx/subjects/BehaviorSubject;

    .line 50
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 51
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    .line 52
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    return-void
.end method

.method private apply()V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->apply()V

    return-void
.end method

.method static synthetic lambda$onLoad$0(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)Ljava/util/Calendar;
    .locals 1

    .line 65
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 66
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    return-object v0
.end method

.method public static synthetic lambda$q1b_P_0Ol6WvKhObXRprvL4RIeo(Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->apply()V

    return-void
.end method

.method private updateUI(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Ljava/util/Calendar;Z)V
    .locals 3

    .line 96
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;

    .line 97
    invoke-virtual {v0, p2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->showCalendarMonth(Ljava/util/Calendar;)V

    .line 98
    iget-object p2, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    iget-object v1, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    iget-boolean v2, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->fullDays:Z

    invoke-virtual {v0, p2, v1, v2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->updateTime(Ljava/util/Date;Ljava/util/Date;Z)V

    .line 99
    iget-boolean p2, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->itemDetails:Z

    iget-boolean v1, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->filterByThisDevice:Z

    invoke-virtual {v0, p2, v1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->updateConfigButtons(ZZ)V

    .line 100
    invoke-virtual {v0, p3}, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->setEmployeeFilterVisibility(Z)V

    .line 101
    iget-object p1, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->selectedEmployees:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->updateEmployeeFilterValueText(I)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$2$ReportConfigScreen$Presenter(ZLcom/squareup/reports/applet/sales/v1/ReportConfig;Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;)V
    .locals 0

    .line 86
    invoke-virtual {p3}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;->asCalendarDate()Ljava/util/Calendar;

    move-result-object p3

    .line 85
    invoke-direct {p0, p2, p3, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->updateUI(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Ljava/util/Calendar;Z)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$ReportConfigScreen$Presenter(Ljava/util/Calendar;)V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->monthAndYearSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;->fromCalendar(Ljava/util/Calendar;)Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onLoad$3$ReportConfigScreen$Presenter(Z)Lrx/Subscription;
    .locals 3

    .line 84
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEdit()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->monthAndYearSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigScreen$Presenter$83iM5wm7MNcI5jeeTdvUOPeMSH4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigScreen$Presenter$83iM5wm7MNcI5jeeTdvUOPeMSH4;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;Z)V

    .line 85
    invoke-static {v1}, Lcom/squareup/util/RxTuples;->expandPair(Lrx/functions/Action2;)Lrx/functions/Action1;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method onBackPressed()Z
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->cancel()V

    const/4 v0, 0x1

    return v0
.end method

.method onClickEmployeeFilter()V
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->clickEmployeeFilter()V

    return-void
.end method

.method onDaySelected(III)V
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->daySelected(III)V

    return-void
.end method

.method onDisplayedMonthChange(II)V
    .locals 2

    .line 118
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->monthAndYearSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;

    iget v0, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;->year:I

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->monthAndYearSubject:Lrx/subjects/BehaviorSubject;

    .line 119
    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;

    iget v0, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;->month:I

    if-ne p2, v0, :cond_0

    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->monthAndYearSubject:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;

    invoke-direct {v1, p2, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;-><init>(II)V

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 56
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const/4 v1, 0x1

    .line 57
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    .line 58
    new-instance v2, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$1lJHUQqqDw0TfCRrx7WDuEXDyX8;

    invoke-direct {v2, p0}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$1lJHUQqqDw0TfCRrx7WDuEXDyX8;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 59
    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/reports/applet/R$string;->report_config_apply:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 60
    new-instance v2, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigScreen$Presenter$q1b_P_0Ol6WvKhObXRprvL4RIeo;

    invoke-direct {v2, p0}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigScreen$Presenter$q1b_P_0Ol6WvKhObXRprvL4RIeo;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 61
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/reports/applet/R$string;->report_config_customize:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    if-nez p1, :cond_0

    .line 64
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEdit()Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->first()Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigScreen$Presenter$ZbOpURsRRaj8vNMlGi3ua97N48c;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigScreen$Presenter$ZbOpURsRRaj8vNMlGi3ua97N48c;

    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigScreen$Presenter$V-oPa9F4p6bOq-znxZI668P10j8;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigScreen$Presenter$V-oPa9F4p6bOq-znxZI668P10j8;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;)V

    .line 68
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    goto :goto_0

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->monthAndYearSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->hasValue()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "DISPLAYED_YEAR"

    .line 73
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v2, "DISPLAYED_MONTH"

    .line 74
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    .line 75
    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->monthAndYearSubject:Lrx/subjects/BehaviorSubject;

    new-instance v3, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;

    invoke-direct {v3, p1, v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;-><init>(II)V

    invoke-virtual {v2, v3}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 77
    :cond_1
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->monthAndYearSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;->fromCalendar(Ljava/util/Calendar;)Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 79
    :goto_0
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {p1}, Lcom/squareup/permissions/EmployeeManagement;->arePasscodesEnabled()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 80
    invoke-interface {p1}, Lcom/squareup/permissions/EmployeeManagement;->onFreeTier()Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    .line 83
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigScreen$Presenter$NJSXIXZL29b02J2jbYAfYMAc6PQ;

    invoke-direct {v0, p0, v1}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigScreen$Presenter$NJSXIXZL29b02J2jbYAfYMAc6PQ;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;Z)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->monthAndYearSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;

    iget v0, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;->year:I

    const-string v1, "DISPLAYED_YEAR"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 91
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->monthAndYearSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;

    iget v0, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;->month:I

    const-string v1, "DISPLAYED_MONTH"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method onSelectEndTime()V
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->selectEndTime()V

    return-void
.end method

.method onSelectStartTime()V
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->selectStartTime()V

    return-void
.end method

.method setAllDayChecked(Z)V
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->setAllDayChecked(Z)V

    return-void
.end method

.method setDeviceFilterChecked(Z)V
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->setDeviceFilterChecked(Z)V

    return-void
.end method

.method setItemDetailsChecked(Z)V
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->setItemDetailsChecked(Z)V

    return-void
.end method
