.class public final Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ReportConfigEmployeeFilterCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;,
        Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;,
        Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReportConfigEmployeeFilterCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReportConfigEmployeeFilterCoordinator.kt\ncom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator\n*L\n1#1,177:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0003\u001a\u001b\u001cB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;",
        "(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "allEmployeesButton",
        "Lcom/squareup/widgets/list/ToggleButtonRow;",
        "context",
        "Landroid/content/Context;",
        "employeeFilterView",
        "Landroid/view/ViewGroup;",
        "employeeList",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "employeeListHeader",
        "Lcom/squareup/marketfont/MarketTextView;",
        "filterByEmployeesButton",
        "progressBar",
        "viewAnimator",
        "Lcom/squareup/widgets/SquareViewAnimator;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "EmployeeViewAdapter",
        "EmployeeViewHolder",
        "ScreenData",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private allEmployeesButton:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private context:Landroid/content/Context;

.field private employeeFilterView:Landroid/view/ViewGroup;

.field private employeeList:Landroidx/recyclerview/widget/RecyclerView;

.field private employeeListHeader:Lcom/squareup/marketfont/MarketTextView;

.field private filterByEmployeesButton:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private progressBar:Landroid/view/ViewGroup;

.field private final runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

.field private viewAnimator:Lcom/squareup/widgets/SquareViewAnimator;


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    return-void
.end method

.method public static final synthetic access$getAllEmployeesButton$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 1

    .line 28
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->allEmployeesButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez p0, :cond_0

    const-string v0, "allEmployeesButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getEmployeeFilterView$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)Landroid/view/ViewGroup;
    .locals 1

    .line 28
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->employeeFilterView:Landroid/view/ViewGroup;

    if-nez p0, :cond_0

    const-string v0, "employeeFilterView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getEmployeeList$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 1

    .line 28
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->employeeList:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p0, :cond_0

    const-string v0, "employeeList"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getEmployeeListHeader$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)Lcom/squareup/marketfont/MarketTextView;
    .locals 1

    .line 28
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->employeeListHeader:Lcom/squareup/marketfont/MarketTextView;

    if-nez p0, :cond_0

    const-string v0, "employeeListHeader"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getFilterByEmployeesButton$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 1

    .line 28
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->filterByEmployeesButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez p0, :cond_0

    const-string v0, "filterByEmployeesButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getProgressBar$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)Landroid/view/ViewGroup;
    .locals 1

    .line 28
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->progressBar:Landroid/view/ViewGroup;

    if-nez p0, :cond_0

    const-string v0, "progressBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    return-object p0
.end method

.method public static final synthetic access$getViewAnimator$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)Lcom/squareup/widgets/SquareViewAnimator;
    .locals 1

    .line 28
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->viewAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez p0, :cond_0

    const-string/jumbo v0, "viewAnimator"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setAllEmployeesButton$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;Lcom/squareup/widgets/list/ToggleButtonRow;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->allEmployeesButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    return-void
.end method

.method public static final synthetic access$setEmployeeFilterView$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;Landroid/view/ViewGroup;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->employeeFilterView:Landroid/view/ViewGroup;

    return-void
.end method

.method public static final synthetic access$setEmployeeList$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->employeeList:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method public static final synthetic access$setEmployeeListHeader$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;Lcom/squareup/marketfont/MarketTextView;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->employeeListHeader:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method public static final synthetic access$setFilterByEmployeesButton$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;Lcom/squareup/widgets/list/ToggleButtonRow;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->filterByEmployeesButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    return-void
.end method

.method public static final synthetic access$setProgressBar$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;Landroid/view/ViewGroup;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->progressBar:Landroid/view/ViewGroup;

    return-void
.end method

.method public static final synthetic access$setViewAnimator$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;Lcom/squareup/widgets/SquareViewAnimator;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->viewAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 82
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "ActionBarView.findIn(view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 83
    sget v0, Lcom/squareup/reports/applet/R$id;->report_config_employee_filter_view_animator:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->viewAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 84
    sget v0, Lcom/squareup/reports/applet/R$id;->report_config_employee_filter_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->progressBar:Landroid/view/ViewGroup;

    .line 85
    sget v0, Lcom/squareup/reports/applet/R$id;->report_config_employee_filter_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->employeeFilterView:Landroid/view/ViewGroup;

    .line 86
    sget v0, Lcom/squareup/reports/applet/R$id;->all_employees:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->allEmployeesButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 87
    sget v0, Lcom/squareup/reports/applet/R$id;->filter_by_employee:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->filterByEmployeesButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 88
    sget v0, Lcom/squareup/reports/applet/R$id;->employee_list_header:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->employeeListHeader:Lcom/squareup/marketfont/MarketTextView;

    .line 89
    sget v0, Lcom/squareup/reports/applet/R$id;->employee_recycler:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->employeeList:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->bindViews(Landroid/view/View;)V

    .line 43
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "view.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->context:Landroid/content/Context;

    .line 45
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 47
    :cond_0
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/reports/applet/R$string;->report_config_employee_selection:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 46
    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    .line 49
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    .line 50
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$$inlined$with$lambda$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$$inlined$with$lambda$1;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;Landroid/view/View;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->allEmployeesButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez v0, :cond_1

    const-string v1, "allEmployeesButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$2;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->filterByEmployeesButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez v0, :cond_2

    const-string v1, "filterByEmployeesButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$3;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$3;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;

    .line 58
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    .line 57
    invoke-direct {v0, v1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)V

    .line 60
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->employeeList:Landroidx/recyclerview/widget/RecyclerView;

    const-string v2, "employeeList"

    if-nez v1, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    move-object v3, v0

    check-cast v3, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {v1, v3}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 61
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->employeeList:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v1, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    new-instance v2, Lcom/squareup/noho/NohoEdgeDecoration;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string/jumbo v3, "view.resources"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, p1}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(Landroid/content/res/Resources;)V

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 63
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->runner:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->screenData()Lrx/Observable;

    move-result-object p1

    .line 64
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$4;

    invoke-direct {v1, p0, v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$4;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method
