.class public Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "PaidInOutRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ViewHolder"
.end annotation


# instance fields
.field private final dateFormatter:Ljava/text/DateFormat;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final row:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;Landroid/view/View;Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;Lcom/squareup/money/PriceLocaleHelper;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 127
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->this$0:Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;

    .line 128
    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 129
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    .line 130
    iput-object p3, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    .line 131
    iput-object p4, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 132
    iput-object p5, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->dateFormatter:Ljava/text/DateFormat;

    .line 133
    iput-object p6, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;)Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;
    .locals 0

    .line 117
    iget-object p0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;Lcom/squareup/ui/ConfirmButton;Lcom/squareup/ui/ConfirmButton;Ljava/lang/String;)V
    .locals 0

    .line 117
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->toggleButtons(Lcom/squareup/ui/ConfirmButton;Lcom/squareup/ui/ConfirmButton;Ljava/lang/String;)V

    return-void
.end method

.method private resetFieldsAndUpdateList(Landroid/widget/EditText;Lcom/squareup/widgets/SelectableEditText;)V
    .locals 0

    .line 195
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Landroid/text/Editable;->clear()V

    .line 196
    invoke-virtual {p2}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Landroid/text/Editable;->clear()V

    .line 197
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->this$0:Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->eventAdded()V

    return-void
.end method

.method private toggleButtons(Lcom/squareup/ui/ConfirmButton;Lcom/squareup/ui/ConfirmButton;Ljava/lang/String;)V
    .locals 4

    .line 184
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {v0, p3}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p3

    if-eqz p3, :cond_1

    .line 185
    iget-object p3, p3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p3, v0, v2

    if-nez p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x1

    .line 189
    invoke-virtual {p1, p3}, Lcom/squareup/ui/ConfirmButton;->setEnabled(Z)V

    .line 190
    invoke-virtual {p2, p3}, Lcom/squareup/ui/ConfirmButton;->setEnabled(Z)V

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p3, 0x0

    .line 186
    invoke-virtual {p1, p3}, Lcom/squareup/ui/ConfirmButton;->setEnabled(Z)V

    .line 187
    invoke-virtual {p2, p3}, Lcom/squareup/ui/ConfirmButton;->setEnabled(Z)V

    :goto_1
    return-void
.end method


# virtual methods
.method public bindPaidInOutHistoryRow(Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;)V
    .locals 6

    .line 201
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v1, Lcom/squareup/reports/applet/R$id;->date_time:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 202
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v2, Lcom/squareup/noho/R$id;->description:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 203
    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v3, Lcom/squareup/reports/applet/R$id;->amount:I

    invoke-static {v2, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 204
    iget-object v3, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v4, Lcom/squareup/reports/applet/R$id;->employee:I

    invoke-static {v3, v4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 206
    iget-object v4, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->dateFormatter:Ljava/text/DateFormat;

    iget-object v5, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v5, v5, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    .line 207
    invoke-static {v5}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 206
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->description:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_IN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-ne v0, v1, :cond_0

    .line 211
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 212
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_OUT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-ne v0, v1, :cond_1

    .line 213
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_money:Lcom/squareup/protos/common/Money;

    invoke-static {v1}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    :cond_1
    :goto_0
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    if-eqz v0, :cond_2

    .line 216
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->this$0:Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    invoke-static {v0, p1}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->access$200(Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    return-void
.end method

.method public bindStaticTopRowContent()V
    .locals 6

    .line 137
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v1, Lcom/squareup/reports/applet/R$id;->action_amount:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    .line 138
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v2, Lcom/squareup/reports/applet/R$id;->action_description:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 140
    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v3, Lcom/squareup/reports/applet/R$id;->paid_in_button:I

    invoke-static {v2, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/ConfirmButton;

    .line 141
    iget-object v3, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v4, Lcom/squareup/reports/applet/R$id;->paid_out_button:I

    invoke-static {v3, v4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/ConfirmButton;

    .line 143
    iget-object v4, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    invoke-virtual {v4}, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->getAmount()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 144
    iget-object v4, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    invoke-virtual {v4}, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 146
    new-instance v4, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder$1;

    invoke-direct {v4, p0, v2, v3}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder$1;-><init>(Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;Lcom/squareup/ui/ConfirmButton;Lcom/squareup/ui/ConfirmButton;)V

    invoke-virtual {v0, v4}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 154
    new-instance v4, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder$2;

    invoke-direct {v4, p0}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder$2;-><init>(Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;)V

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 160
    iget-object v4, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {v4, v0}, Lcom/squareup/money/PriceLocaleHelper;->removeScrubbingTextWatcher(Lcom/squareup/text/HasSelectableText;)V

    .line 161
    iget-object v4, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    sget-object v5, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANK_ON_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-virtual {v4, v0, v5}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)Lcom/squareup/text/ScrubbingTextWatcher;

    .line 163
    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 164
    invoke-direct {p0, v2, v3, v4}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->toggleButtons(Lcom/squareup/ui/ConfirmButton;Lcom/squareup/ui/ConfirmButton;Ljava/lang/String;)V

    .line 166
    sget-object v4, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v2, v4}, Lcom/squareup/ui/ConfirmButton;->setButtonWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 167
    new-instance v4, Lcom/squareup/reports/applet/drawer/-$$Lambda$PaidInOutRecyclerAdapter$ViewHolder$L1E57TpVh8kklqsp5cgun4WH1DY;

    invoke-direct {v4, p0, v0, v1}, Lcom/squareup/reports/applet/drawer/-$$Lambda$PaidInOutRecyclerAdapter$ViewHolder$L1E57TpVh8kklqsp5cgun4WH1DY;-><init>(Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;Lcom/squareup/widgets/SelectableEditText;Landroid/widget/EditText;)V

    invoke-virtual {v2, v4}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    .line 173
    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v3, v2}, Lcom/squareup/ui/ConfirmButton;->setButtonWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 174
    new-instance v2, Lcom/squareup/reports/applet/drawer/-$$Lambda$PaidInOutRecyclerAdapter$ViewHolder$Nb4tI9S9jxnYvz1lKOsCfifwYD0;

    invoke-direct {v2, p0, v0, v1}, Lcom/squareup/reports/applet/drawer/-$$Lambda$PaidInOutRecyclerAdapter$ViewHolder$Nb4tI9S9jxnYvz1lKOsCfifwYD0;-><init>(Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;Lcom/squareup/widgets/SelectableEditText;Landroid/widget/EditText;)V

    invoke-virtual {v3, v2}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    return-void
.end method

.method public bindTotalPaidInOutRow()V
    .locals 3

    .line 221
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    .line 222
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    invoke-virtual {v2}, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->getPaidInOutTotal()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 223
    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setValueWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method

.method public synthetic lambda$bindStaticTopRowContent$0$PaidInOutRecyclerAdapter$ViewHolder(Lcom/squareup/widgets/SelectableEditText;Landroid/widget/EditText;)V
    .locals 3

    .line 168
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 169
    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 168
    invoke-virtual {v0, v1, v2}, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->addPaidInEvent(Lcom/squareup/protos/common/Money;Ljava/lang/String;)V

    .line 170
    invoke-direct {p0, p2, p1}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->resetFieldsAndUpdateList(Landroid/widget/EditText;Lcom/squareup/widgets/SelectableEditText;)V

    return-void
.end method

.method public synthetic lambda$bindStaticTopRowContent$1$PaidInOutRecyclerAdapter$ViewHolder(Lcom/squareup/widgets/SelectableEditText;Landroid/widget/EditText;)V
    .locals 3

    .line 175
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 176
    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 175
    invoke-virtual {v0, v1, v2}, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->addPaidOutEvent(Lcom/squareup/protos/common/Money;Ljava/lang/String;)V

    .line 177
    invoke-direct {p0, p2, p1}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->resetFieldsAndUpdateList(Landroid/widget/EditText;Lcom/squareup/widgets/SelectableEditText;)V

    return-void
.end method
