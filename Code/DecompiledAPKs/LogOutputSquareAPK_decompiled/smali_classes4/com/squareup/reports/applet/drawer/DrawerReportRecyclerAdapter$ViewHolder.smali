.class public Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "DrawerReportRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewHolder"
.end annotation


# instance fields
.field private final dateFormatter:Ljava/text/DateFormat;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final presenter:Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;

.field private final row:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 120
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 121
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    .line 122
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->presenter:Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;

    .line 123
    iput-object p3, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->dateFormatter:Ljava/text/DateFormat;

    .line 124
    iput-object p4, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method public bindCashDrawerDetailsRow()V
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    check-cast v0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    .line 129
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->presenter:Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;

    iget-object v1, v1, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndConfigurePresenter(Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;)V

    .line 130
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->presenter:Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->formatInDrawerHistory()V

    return-void
.end method

.method public bindPaidInOutHistoryRow(Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;)V
    .locals 5

    .line 134
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v1, Lcom/squareup/reports/applet/R$id;->date_time:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 135
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v2, Lcom/squareup/noho/R$id;->description:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 136
    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v3, Lcom/squareup/reports/applet/R$id;->amount:I

    invoke-static {v2, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 138
    iget-object v3, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->dateFormatter:Ljava/text/DateFormat;

    iget-object v4, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v4, v4, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {v4}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->description:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_IN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-ne v0, v1, :cond_0

    .line 141
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 142
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_OUT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-ne v0, v1, :cond_1

    .line 143
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_money:Lcom/squareup/protos/common/Money;

    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public bindTotalPaidInOutRow()V
    .locals 3

    .line 148
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    .line 149
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->presenter:Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;

    invoke-virtual {v2}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->getPaidInOutTotal()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 150
    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setValueWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method
