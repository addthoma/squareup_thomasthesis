.class public final enum Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;
.super Ljava/lang/Enum;
.source "LoadingReportFailedEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ErrorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

.field public static final enum CLIENT_ERROR:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

.field public static final enum NETWORK_ERROR:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

.field public static final enum SERVER_ERROR:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

.field public static final enum SESSION_EXPIRED:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 10
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    const/4 v1, 0x0

    const-string v2, "CLIENT_ERROR"

    invoke-direct {v0, v2, v1}, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->CLIENT_ERROR:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    new-instance v0, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    const/4 v2, 0x1

    const-string v3, "SESSION_EXPIRED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->SESSION_EXPIRED:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    new-instance v0, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    const/4 v3, 0x2

    const-string v4, "SERVER_ERROR"

    invoke-direct {v0, v4, v3}, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->SERVER_ERROR:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    new-instance v0, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    const/4 v4, 0x3

    const-string v5, "NETWORK_ERROR"

    invoke-direct {v0, v5, v4}, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->NETWORK_ERROR:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    .line 9
    sget-object v5, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->CLIENT_ERROR:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->SESSION_EXPIRED:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->SERVER_ERROR:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->NETWORK_ERROR:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->$VALUES:[Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;
    .locals 1

    .line 9
    const-class v0, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->$VALUES:[Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    invoke-virtual {v0}, [Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    return-object v0
.end method
