.class Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder$1;
.super Ljava/lang/Object;
.source "SalesReportRecyclerAdapter.java"

# interfaces
.implements Lcom/github/mikephil/charting/listener/OnChartValueSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;

.field final synthetic val$itemView:Landroid/view/View;

.field final synthetic val$this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V
    .locals 0

    .line 458
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder$1;->this$1:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder$1;->val$this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder$1;->val$itemView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNothingSelected()V
    .locals 2

    .line 478
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder$1;->this$1:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;

    invoke-static {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->access$500(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onValueSelected(Lcom/github/mikephil/charting/data/Entry;ILcom/github/mikephil/charting/utils/Highlight;)V
    .locals 6

    .line 460
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder$1;->val$itemView:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 461
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->getData()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;

    .line 462
    iget-object p3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder$1;->this$1:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;

    iget-object p3, p3, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {p3}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$300(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;

    move-result-object p3

    const/4 v0, 0x0

    invoke-virtual {p3, p1, v0, v0}, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->format(Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;ZZ)Ljava/lang/String;

    move-result-object p3

    .line 463
    iget-wide v1, p1, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->transactionCount:J

    const-wide/16 v3, 0x1

    cmp-long v5, v1, v3

    if-lez v5, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-eqz v0, :cond_1

    .line 464
    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report_chart_highlight_plural:I

    goto :goto_0

    :cond_1
    iget-wide v1, p1, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->transactionCount:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-lez v5, :cond_2

    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report_chart_highlight_single:I

    goto :goto_0

    :cond_2
    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report_chart_highlight_zero:I

    .line 467
    :goto_0
    invoke-static {p2, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder$1;->val$itemView:Landroid/view/View;

    .line 469
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v1, v2}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object v1

    invoke-static {p3, v1}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p3

    const-string v1, "time"

    .line 468
    invoke-virtual {p2, v1, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    iget-object p3, p1, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->grossSalesFormatted:Ljava/lang/String;

    const-string v1, "amount"

    .line 470
    invoke-virtual {p2, v1, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    if-eqz v0, :cond_3

    .line 472
    iget-object p3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder$1;->this$1:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;

    iget-object p3, p3, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {p3}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$400(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Ljava/text/NumberFormat;

    move-result-object p3

    iget-wide v0, p1, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->transactionCount:J

    invoke-virtual {p3, v0, v1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object p1

    const-string p3, "sales_count"

    invoke-virtual {p2, p3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    .line 474
    :cond_3
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder$1;->this$1:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;

    invoke-static {p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->access$500(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
