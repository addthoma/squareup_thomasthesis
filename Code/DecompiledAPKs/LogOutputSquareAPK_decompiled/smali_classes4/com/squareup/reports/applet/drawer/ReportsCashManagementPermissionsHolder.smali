.class public final Lcom/squareup/reports/applet/drawer/ReportsCashManagementPermissionsHolder;
.super Ljava/lang/Object;
.source "ReportsCashManagementPermissionsHolder.kt"

# interfaces
.implements Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\t\u001a\u00020\nH\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/reports/applet/drawer/ReportsCashManagementPermissionsHolder;",
        "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
        "()V",
        "hasViewAmountInCashDrawerPermission",
        "",
        "getHasViewAmountInCashDrawerPermission",
        "()Z",
        "setHasViewAmountInCashDrawerPermission",
        "(Z)V",
        "revokeHasViewAmountInCashDrawerPermission",
        "",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private hasViewAmountInCashDrawerPermission:Z


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getHasViewAmountInCashDrawerPermission()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/squareup/reports/applet/drawer/ReportsCashManagementPermissionsHolder;->hasViewAmountInCashDrawerPermission:Z

    return v0
.end method

.method public revokeHasViewAmountInCashDrawerPermission()V
    .locals 1

    const/4 v0, 0x0

    .line 16
    invoke-virtual {p0, v0}, Lcom/squareup/reports/applet/drawer/ReportsCashManagementPermissionsHolder;->setHasViewAmountInCashDrawerPermission(Z)V

    return-void
.end method

.method public setHasViewAmountInCashDrawerPermission(Z)V
    .locals 0

    .line 13
    iput-boolean p1, p0, Lcom/squareup/reports/applet/drawer/ReportsCashManagementPermissionsHolder;->hasViewAmountInCashDrawerPermission:Z

    return-void
.end method
