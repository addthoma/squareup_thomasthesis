.class public final Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail_MembersInjector;
.super Ljava/lang/Object;
.source "SalesReportEmail_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;",
        ">;"
    }
.end annotation


# instance fields
.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final reportEmailServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/reporting/ReportEmailService;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/reporting/ReportEmailService;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail_MembersInjector;->reportEmailServiceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/reporting/ReportEmailService;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;",
            ">;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectReportEmailService(Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;Lcom/squareup/server/reporting/ReportEmailService;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;->reportEmailService:Lcom/squareup/server/reporting/ReportEmailService;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectMainScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectRpcScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail_MembersInjector;->reportEmailServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/reporting/ReportEmailService;

    invoke-static {p1, v0}, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail_MembersInjector;->injectReportEmailService(Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;Lcom/squareup/server/reporting/ReportEmailService;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail_MembersInjector;->injectMembers(Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;)V

    return-void
.end method
