.class public final Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;
.super Lcom/squareup/reports/applet/InReportsAppletScope;
.source "ReportEmailScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Component;,
        Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final reportConfig:Lcom/squareup/reports/applet/sales/v1/ReportConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 147
    sget-object v0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportEmailScreen$1CmjQMyvrqG4OYfUfvSOONoWE4Y;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportEmailScreen$1CmjQMyvrqG4OYfUfvSOONoWE4Y;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)V
    .locals 0

    .line 42
    invoke-direct {p0}, Lcom/squareup/reports/applet/InReportsAppletScope;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;->reportConfig:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;)Lcom/squareup/reports/applet/sales/v1/ReportConfig;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;->reportConfig:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;
    .locals 1

    .line 148
    const-class v0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 149
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    .line 150
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 144
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;->reportConfig:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 154
    sget v0, Lcom/squareup/reports/applet/R$layout;->report_email_card_view:I

    return v0
.end method
