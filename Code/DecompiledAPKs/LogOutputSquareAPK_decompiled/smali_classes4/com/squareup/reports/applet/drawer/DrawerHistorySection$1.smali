.class Lcom/squareup/reports/applet/drawer/DrawerHistorySection$1;
.super Lcom/squareup/applet/SectionAccess;
.source "DrawerHistorySection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/drawer/DrawerHistorySection;-><init>(Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic val$device:Lcom/squareup/util/Device;

.field final synthetic val$features:Lcom/squareup/settings/server/Features;


# direct methods
.method constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 22
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistorySection$1;->val$device:Lcom/squareup/util/Device;

    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/DrawerHistorySection$1;->val$features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistorySection$1;->val$device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isTablet()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistorySection$1;->val$features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_CASH_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

    .line 25
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistorySection$1;->val$features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_REPORTS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    sget-object v0, Lcom/squareup/permissions/Permission;->VIEW_CASH_DRAWER_REPORTS:Lcom/squareup/permissions/Permission;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0

    .line 32
    :cond_0
    sget-object v0, Lcom/squareup/permissions/Permission;->SUMMARIES_AND_REPORTS:Lcom/squareup/permissions/Permission;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
