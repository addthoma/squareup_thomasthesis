.class public final Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;
.super Lcom/squareup/reports/applet/InReportsAppletScope;
.source "SalesReportScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Component;,
        Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 63
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;

    invoke-direct {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;

    .line 455
    sget-object v0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 65
    invoke-direct {p0}, Lcom/squareup/reports/applet/InReportsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 69
    const-class v0, Lcom/squareup/reports/applet/sales/SalesReportSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 458
    sget v0, Lcom/squareup/reports/applet/R$layout;->sales_report_view_v1:I

    return v0
.end method
