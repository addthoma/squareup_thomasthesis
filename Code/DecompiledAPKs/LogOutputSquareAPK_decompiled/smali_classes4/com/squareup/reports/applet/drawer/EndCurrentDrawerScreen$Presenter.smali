.class Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "EndCurrentDrawerScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;",
        ">;"
    }
.end annotation


# static fields
.field static final DRAWER_ENDED:Ljava/lang/String; = "current_cash_drawer_ended"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final autoCloseDrawerRunnable:Ljava/lang/Runnable;

.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

.field private final cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

.field final detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

.field private drawerEnded:Z

.field private final flow:Lflow/Flow;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final reportsAppletSectionsListPresenter:Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/analytics/Analytics;Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 61
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const/4 v0, 0x0

    .line 49
    iput-boolean v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->drawerEnded:Z

    .line 51
    new-instance v0, Lcom/squareup/reports/applet/drawer/-$$Lambda$EndCurrentDrawerScreen$Presenter$lUXnO3JC4JgFZklm5_EfuDqEYEo;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$EndCurrentDrawerScreen$Presenter$lUXnO3JC4JgFZklm5_EfuDqEYEo;-><init>(Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;)V

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->autoCloseDrawerRunnable:Ljava/lang/Runnable;

    .line 62
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->flow:Lflow/Flow;

    .line 63
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 64
    iput-object p3, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    .line 65
    iput-object p4, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->reportsAppletSectionsListPresenter:Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    .line 66
    iput-object p5, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 67
    iput-object p6, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 68
    iput-object p7, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    .line 69
    iput-object p8, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    return-void
.end method

.method private autoCloseDrawer()V
    .locals 2

    .line 124
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_END_DRAWER_CLOSE_AUTOMATICALLY:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 126
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->onBackPressed()Z

    :cond_0
    return-void
.end method

.method public static synthetic lambda$lUXnO3JC4JgFZklm5_EfuDqEYEo(Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->autoCloseDrawer()V

    return-void
.end method


# virtual methods
.method endDrawer()V
    .locals 4

    .line 109
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getActualInDrawer()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-nez v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    invoke-interface {v0, v1}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->endCurrentCashDrawerShift(Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V

    goto :goto_0

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    invoke-interface {v1, v0, v2}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->endAndCloseCurrentCashDrawerShift(Lcom/squareup/protos/common/Money;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V

    .line 117
    :goto_0
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->reportsAppletSectionsListPresenter:Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->refreshIfSidebar()V

    const/4 v0, 0x1

    .line 118
    iput-boolean v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->drawerEnded:Z

    .line 119
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->animateToEndDrawerMessage()V

    .line 120
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->autoCloseDrawerRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public synthetic lambda$onLoad$0$EndCurrentDrawerScreen$Presenter(Lcom/squareup/cashmanagement/CashDrawerShiftsResult;)V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    invoke-interface {p1}, Lcom/squareup/cashmanagement/CashDrawerShiftsResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    invoke-virtual {v0, p1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->setShift(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    .line 84
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->formatForEndCurrentDrawer()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$EndCurrentDrawerScreen$Presenter()V
    .locals 2

    .line 91
    iget-boolean v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->drawerEnded:Z

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_END_DRAWER_CLOSE_MANUALLY:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 94
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->onBackPressed()Z

    return-void
.end method

.method public onBackPressed()Z
    .locals 5

    .line 103
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;->revokeHasViewAmountInCashDrawerPermission()V

    .line 104
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return v1
.end method

.method protected onExitScope()V
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->autoCloseDrawerRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    if-eqz p1, :cond_0

    const-string v0, "current_cash_drawer_ended"

    .line 77
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->showEndDrawerMessage()V

    goto :goto_0

    .line 80
    :cond_0
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    .line 81
    invoke-interface {p1}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->getCurrentCashDrawerShift()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_cash_drawer_shift_id:Ljava/lang/String;

    new-instance v1, Lcom/squareup/reports/applet/drawer/-$$Lambda$EndCurrentDrawerScreen$Presenter$v1R4RHgC5hjwhh-MkpP44paBTZg;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$EndCurrentDrawerScreen$Presenter$v1R4RHgC5hjwhh-MkpP44paBTZg;-><init>(Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;)V

    .line 80
    invoke-interface {p1, v0, v1}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->getCashDrawerShift(Ljava/lang/String;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V

    .line 88
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    .line 89
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/reports/applet/R$string;->current_drawer_end_drawer:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 90
    new-instance v0, Lcom/squareup/reports/applet/drawer/-$$Lambda$EndCurrentDrawerScreen$Presenter$1wivgf1rXFaRK3B-FOWj8nKC60I;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$EndCurrentDrawerScreen$Presenter$1wivgf1rXFaRK3B-FOWj8nKC60I;-><init>(Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 99
    iget-boolean v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->drawerEnded:Z

    const-string v1, "current_cash_drawer_ended"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
