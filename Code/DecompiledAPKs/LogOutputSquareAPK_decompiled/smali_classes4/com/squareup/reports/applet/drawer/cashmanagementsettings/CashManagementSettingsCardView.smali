.class public Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;
.super Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;
.source "CashManagementSettingsCardView.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field presenter:Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const-class p2, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Component;->inject(Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;)V

    return-void
.end method


# virtual methods
.method protected enableAutoEmailToggled(Z)V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;->presenter:Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->enableAutoEmailToggled(Z)V

    return-void
.end method

.method protected enableCashManagementToggled(Z)V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;->presenter:Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->enableCashManagementToggled(Z)V

    return-void
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-object v0
.end method

.method protected getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;->presenter:Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    return-object v0
.end method

.method protected getMoneyFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;->presenter:Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method

.method protected getPriceLocaleHelper()Lcom/squareup/money/PriceLocaleHelper;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;->presenter:Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->getPriceLocaleHelper()Lcom/squareup/money/PriceLocaleHelper;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()Z
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;->presenter:Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->onUpPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;->presenter:Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 35
    invoke-super {p0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 28
    invoke-super {p0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->onFinishInflate()V

    .line 29
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 30
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;->presenter:Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
