.class public final Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;
.super Lcom/squareup/applet/AppletSection;
.source "LoyaltyReportSection.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;",
        "Lcom/squareup/applet/AppletSection;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/loyalty/LoyaltySettings;)V",
        "getInitialScreen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/loyalty/LoyaltySettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltySettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    new-instance v0, Lcom/squareup/reports/applet/loyalty/LoyaltyReportAccess;

    invoke-direct {v0, p1, p2}, Lcom/squareup/reports/applet/loyalty/LoyaltyReportAccess;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/loyalty/LoyaltySettings;)V

    check-cast v0, Lcom/squareup/applet/SectionAccess;

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/reports/applet/loyalty/LoyaltyReportBoostrapScreen;->INSTANCE:Lcom/squareup/reports/applet/loyalty/LoyaltyReportBoostrapScreen;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method
