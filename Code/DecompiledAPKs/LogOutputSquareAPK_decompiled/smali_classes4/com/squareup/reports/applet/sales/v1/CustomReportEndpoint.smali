.class public final Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;
.super Ljava/lang/Object;
.source "CustomReportEndpoint.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCustomReportEndpoint.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CustomReportEndpoint.kt\ncom/squareup/reports/applet/sales/v1/CustomReportEndpoint\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,680:1\n1360#2:681\n1429#2,3:682\n704#2:685\n777#2,2:686\n1360#2:688\n1429#2,3:689\n950#2:692\n*E\n*S KotlinDebug\n*F\n+ 1 CustomReportEndpoint.kt\ncom/squareup/reports/applet/sales/v1/CustomReportEndpoint\n*L\n516#1:681\n516#1,3:682\n609#1:685\n609#1,2:686\n613#1:688\n613#1,3:689\n624#1:692\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00fe\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \\2\u00020\u0001:\u0001\\BW\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J\u001e\u0010\u0016\u001a\u00020\u00172\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u00192\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J<\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020 2\u0006\u0010\"\u001a\u00020\u00052\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u00020%0$2\u000c\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\'0$H\u0002J\u0010\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+H\u0002J\"\u0010,\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002000/0.0-2\u0006\u00101\u001a\u000202H\u0002J\u0018\u00103\u001a\u0008\u0012\u0004\u0012\u0002040$2\u0008\u00105\u001a\u0004\u0018\u000106H\u0007J\u001c\u00107\u001a\u0008\u0012\u0004\u0012\u00020\u001a0$2\u000c\u00108\u001a\u0008\u0012\u0004\u0012\u00020\u001c0$H\u0002J\u001e\u00109\u001a\u0008\u0012\u0004\u0012\u00020:0$2\u0006\u0010;\u001a\u00020<2\u0006\u0010=\u001a\u000206H\u0002J\u0016\u0010>\u001a\u0008\u0012\u0004\u0012\u00020\'0$2\u0006\u0010?\u001a\u000202H\u0002J\u0016\u0010@\u001a\u0008\u0012\u0004\u0012\u00020:0$2\u0006\u0010;\u001a\u00020<H\u0002J\u0010\u0010A\u001a\u00020B2\u0006\u0010C\u001a\u00020DH\u0002J\u001a\u0010E\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020F0/0-2\u0006\u0010?\u001a\u000202J8\u0010G\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020F0/0-2\u0006\u0010?\u001a\u0002022\u000c\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\'0$2\u000c\u0010H\u001a\u0008\u0012\u0004\u0012\u0002060/H\u0002J\u0016\u0010I\u001a\u0008\u0012\u0004\u0012\u00020K0J2\u0006\u0010L\u001a\u00020\u001cH\u0002J\u0012\u0010M\u001a\u0004\u0018\u00010N2\u0006\u0010L\u001a\u00020\u001cH\u0007J\u0010\u0010O\u001a\u00020\u00052\u0006\u0010L\u001a\u00020\u001cH\u0007J0\u0010P\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002060/0-2\u0006\u00101\u001a\u00020\u001e2\u0012\u0010Q\u001a\u000e\u0012\u0004\u0012\u000206\u0012\u0004\u0012\u00020S0RH\u0002J\u0010\u0010T\u001a\u00020S2\u0006\u0010L\u001a\u00020\u001cH\u0002J\u0008\u0010U\u001a\u00020SH\u0002J$\u0010V\u001a\u0008\u0012\u0004\u0012\u0002HX0W\"\u0004\u0008\u0000\u0010Y\"\u0004\u0008\u0001\u0010X*\u0008\u0012\u0004\u0012\u0002HY0WH\u0002J$\u0010Z\u001a\u0008\u0012\u0004\u0012\u0002HX0[\"\u0004\u0008\u0000\u0010Y\"\u0004\u0008\u0001\u0010X*\u0008\u0012\u0004\u0012\u0002HY0[H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006]"
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;",
        "",
        "customReportService",
        "Lcom/squareup/customreport/data/service/CustomReportService;",
        "merchantToken",
        "",
        "deviceIdProvider",
        "Lcom/squareup/settings/DeviceIdProvider;",
        "salesSummaryRowGenerator",
        "Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;",
        "nameOrTranslationTypeFormatter",
        "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "paymentMethodProcessor",
        "Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/customreport/data/service/CustomReportService;Ljava/lang/String;Lcom/squareup/settings/DeviceIdProvider;Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/Features;Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;Lcom/squareup/util/Res;)V",
        "addItemCategory",
        "",
        "categoryRows",
        "",
        "Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;",
        "itemCategory",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
        "buildCustomReportRequest",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;",
        "startTime",
        "Ljava/util/Date;",
        "endTime",
        "timeZone",
        "groupByTypes",
        "",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
        "filters",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Filter;",
        "buildTimeRange",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;",
        "currentMonth",
        "Ljava/util/Calendar;",
        "createChartData",
        "Lrx/Single;",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;",
        "request",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
        "createDiscountRows",
        "Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;",
        "discounts",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        "createItemCategoryRows",
        "itemCategoryReports",
        "createPaymentsRows",
        "Lcom/squareup/reports/applet/sales/v1/SalesReportRow;",
        "aggregate",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;",
        "paymentMethodResponse",
        "createReportFilters",
        "reportConfig",
        "createSalesRows",
        "dateTimeFromMillis",
        "Lcom/squareup/protos/common/time/DateTime;",
        "currentTimeMillis",
        "",
        "downloadReport",
        "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;",
        "downloadSubReports",
        "salesSummarySuccessOrFailure",
        "extractMeasurementUnits",
        "",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
        "report",
        "extractQuantity",
        "Ljava/math/BigDecimal;",
        "formatMeasurementUnit",
        "getCustomReport",
        "isSuccessful",
        "Lkotlin/Function1;",
        "",
        "reportHasDefaultMeasurementUnit",
        "seeMeasurementUnit",
        "mapError",
        "Lcom/squareup/receiving/ReceivedResponse$Error;",
        "U",
        "T",
        "mapFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "Companion",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$Companion;

.field public static final DISCOUNTS_GROUP_BY_TYPES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAX_DAYS_FOR_DAILY_SALES:I = 0x5a

.field public static final PAYMENT_METHOD_GROUP_BY_TYPES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            ">;"
        }
    .end annotation
.end field

.field public static final SALES_SUMMARY_GROUP_BY_TYPES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            ">;"
        }
    .end annotation
.end field

.field public static final SALES_SUMMARY_GROUP_BY_TYPES_MEASUREMENT_UNIT:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final customReportService:Lcom/squareup/customreport/data/service/CustomReportService;

.field private final deviceIdProvider:Lcom/squareup/settings/DeviceIdProvider;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final merchantToken:Ljava/lang/String;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

.field private final paymentMethodProcessor:Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;

.field private final res:Lcom/squareup/util/Res;

.field private final salesSummaryRowGenerator:Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    new-instance v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->Companion:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$Companion;

    const/4 v0, 0x4

    new-array v1, v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 667
    sget-object v2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->NONE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM_CATEGORY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    sget-object v2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    sget-object v2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM_VARIATION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v6, 0x3

    aput-object v2, v1, v6

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->SALES_SUMMARY_GROUP_BY_TYPES:Ljava/util/List;

    new-array v0, v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 669
    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->NONE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM_CATEGORY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM_VARIATION_MEASUREMENT_UNIT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    aput-object v1, v0, v6

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->SALES_SUMMARY_GROUP_BY_TYPES_MEASUREMENT_UNIT:Ljava/util/List;

    .line 672
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DISCOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->DISCOUNTS_GROUP_BY_TYPES:Ljava/util/List;

    .line 675
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PAYMENT_METHOD_SEPARATED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->PAYMENT_METHOD_GROUP_BY_TYPES:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/customreport/data/service/CustomReportService;Ljava/lang/String;Lcom/squareup/settings/DeviceIdProvider;Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/Features;Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;Lcom/squareup/util/Res;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/user/UserToken;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/service/CustomReportService;",
            "Ljava/lang/String;",
            "Lcom/squareup/settings/DeviceIdProvider;",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;",
            "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "customReportService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "merchantToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceIdProvider"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesSummaryRowGenerator"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameOrTranslationTypeFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentMethodProcessor"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->customReportService:Lcom/squareup/customreport/data/service/CustomReportService;

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->merchantToken:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->deviceIdProvider:Lcom/squareup/settings/DeviceIdProvider;

    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->salesSummaryRowGenerator:Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;

    iput-object p5, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    iput-object p6, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p7, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->features:Lcom/squareup/settings/server/Features;

    iput-object p8, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->paymentMethodProcessor:Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;

    iput-object p9, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$buildTimeRange(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Ljava/util/Calendar;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;
    .locals 0

    .line 70
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->buildTimeRange(Ljava/util/Calendar;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$createItemCategoryRows(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 70
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->createItemCategoryRows(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$createPaymentsRows(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Ljava/util/List;
    .locals 0

    .line 70
    invoke-direct {p0, p1, p2}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->createPaymentsRows(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$createSalesRows(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)Ljava/util/List;
    .locals 0

    .line 70
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->createSalesRows(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$downloadSubReports(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/reports/applet/sales/v1/ReportConfig;Ljava/util/List;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lrx/Single;
    .locals 0

    .line 70
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->downloadSubReports(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Ljava/util/List;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lrx/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getMoneyFormatter$p(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 70
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method public static final synthetic access$mapFailure(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;
    .locals 0

    .line 70
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->mapFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    move-result-object p0

    return-object p0
.end method

.method private final addItemCategory(Ljava/util/List;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;",
            ">;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
            ")V"
        }
    .end annotation

    .line 473
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 474
    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 475
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 477
    check-cast p1, Ljava/util/Collection;

    new-instance v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;

    .line 478
    sget-object v2, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->CATEGORY:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    .line 479
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    .line 480
    iget-object v3, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const-string v5, "itemCategory.group_by_va\u2026.name_or_translation_type"

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 479
    invoke-virtual {v1, v3}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->formatName(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;

    move-result-object v3

    .line 483
    invoke-virtual {p0, p2}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->extractQuantity(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Ljava/math/BigDecimal;

    move-result-object v5

    .line 484
    invoke-virtual {p0, p2}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->formatMeasurementUnit(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Ljava/lang/String;

    move-result-object v6

    move-object v1, v0

    .line 477
    invoke-direct/range {v1 .. v6}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;-><init>(Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 487
    iget-object p2, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    .line 488
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 489
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 491
    iget-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    const-string v3, "item.group_by_value.item.name_or_translation_type"

    if-ne v1, v2, :cond_0

    .line 492
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    .line 493
    iget-object v2, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 494
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const-string v6, "item.sub_report[0].group\u2026.name_or_translation_type"

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 492
    invoke-virtual {v1, v2, v3}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->formatNameAndVariation(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;

    move-result-object v1

    .line 497
    new-instance v8, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;

    .line 498
    sget-object v3, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->ITEM:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    .line 501
    iget-object v2, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    const-string v6, "item.sub_report[0]"

    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    invoke-virtual {p0, v2}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->extractQuantity(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Ljava/math/BigDecimal;

    move-result-object v7

    .line 502
    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    invoke-virtual {p0, v0}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->formatMeasurementUnit(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v8

    move-object v4, v1

    move-object v6, v7

    move-object v7, v0

    .line 497
    invoke-direct/range {v2 .. v7}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;-><init>(Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;)V

    invoke-interface {p1, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 505
    :cond_0
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;

    .line 506
    sget-object v4, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->ITEM:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    .line 507
    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    .line 508
    iget-object v6, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v6, v6, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    iget-object v6, v6, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-static {v6, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 507
    invoke-virtual {v2, v6}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->formatName(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;

    move-result-object v6

    const-string v2, "item"

    .line 511
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->extractQuantity(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Ljava/math/BigDecimal;

    move-result-object v7

    .line 512
    invoke-virtual {p0, v0}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->formatMeasurementUnit(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Ljava/lang/String;

    move-result-object v8

    move-object v2, v1

    move-object v3, v4

    move-object v4, v6

    move-object v6, v7

    move-object v7, v8

    .line 505
    invoke-direct/range {v2 .. v7}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;-><init>(Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;)V

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 515
    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    const-string v1, "item.sub_report"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 681
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 682
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 683
    check-cast v2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    .line 517
    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 518
    iget-object v4, v2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v4, v4, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v4, v4, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v3, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 519
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 521
    new-instance v3, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;

    .line 522
    sget-object v5, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->ITEM_VARIATION:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    .line 523
    iget-object v4, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    .line 524
    iget-object v6, v2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v6, v6, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    iget-object v6, v6, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const-string v8, "it.group_by_value.item_v\u2026.name_or_translation_type"

    invoke-static {v6, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 523
    invoke-virtual {v4, v6}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->formatName(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;

    move-result-object v6

    const-string v4, "it"

    .line 527
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->extractQuantity(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Ljava/math/BigDecimal;

    move-result-object v8

    .line 528
    invoke-virtual {p0, v2}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->formatMeasurementUnit(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Ljava/lang/String;

    move-result-object v9

    move-object v4, v3

    .line 521
    invoke-direct/range {v4 .. v9}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;-><init>(Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;)V

    .line 529
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 684
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 515
    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto/16 :goto_0

    :cond_2
    return-void
.end method

.method private final buildCustomReportRequest(Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Filter;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;"
        }
    .end annotation

    .line 634
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;-><init>()V

    .line 635
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->merchantToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    move-result-object v0

    .line 636
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->dateTimeFromMillis(J)Lcom/squareup/protos/common/time/DateTime;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->begin_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    move-result-object p1

    .line 637
    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->dateTimeFromMillis(J)Lcom/squareup/protos/common/time/DateTime;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->end_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    move-result-object p1

    .line 638
    invoke-virtual {p1, p3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->tz_name(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    move-result-object p1

    .line 639
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->build()Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    move-result-object p1

    .line 640
    new-instance p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;-><init>()V

    .line 641
    invoke-virtual {p2, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->request_params(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;

    move-result-object p1

    .line 642
    invoke-virtual {p1, p4}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->group_by_type(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;

    move-result-object p1

    .line 643
    invoke-virtual {p1, p5}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->filter(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;

    move-result-object p1

    .line 644
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    move-result-object p1

    const-string p2, "CustomReportRequest.Buil\u2026filters)\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final buildTimeRange(Ljava/util/Calendar;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;
    .locals 4

    .line 434
    new-instance v0, Lcom/squareup/protos/common/time/DateTime$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/DateTime$Builder;-><init>()V

    .line 435
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/common/time/DateTime$Builder;->instant_usec(Ljava/lang/Long;)Lcom/squareup/protos/common/time/DateTime$Builder;

    move-result-object p1

    .line 436
    invoke-virtual {p1}, Lcom/squareup/protos/common/time/DateTime$Builder;->build()Lcom/squareup/protos/common/time/DateTime;

    move-result-object p1

    .line 437
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;-><init>()V

    .line 438
    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;->begin_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;

    move-result-object p1

    .line 439
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    move-result-object p1

    const-string v0, "GroupByValue.TimeRange.B\u2026ateTime)\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final createChartData(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)Lrx/Single;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;",
            ">;>;>;"
        }
    .end annotation

    .line 257
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SALES_SUMMARY_CHARTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 258
    sget-object p1, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {p1}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-static {p1}, Lrx/Single;->just(Ljava/lang/Object;)Lrx/Single;

    move-result-object p1

    const-string v0, "Single.just(Optional.empty())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 261
    :cond_0
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 262
    iget-object v1, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 263
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 264
    iget-object v2, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 267
    invoke-static {v0, v1}, Lcom/squareup/util/Times;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->HOUR_OF_DAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    goto :goto_0

    .line 268
    :cond_1
    iget-object v2, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    iget-object v3, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    invoke-static {v2, v3}, Lcom/squareup/util/Times;->countDaysBetween(Ljava/util/Date;Ljava/util/Date;)J

    move-result-wide v2

    const/16 v4, 0x5a

    int-to-long v4, v4

    cmp-long v6, v2, v4

    if-gez v6, :cond_2

    sget-object v2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    goto :goto_0

    .line 269
    :cond_2
    sget-object v2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MONTH:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 273
    :goto_0
    iget-object v4, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    .line 274
    iget-object v5, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    .line 275
    iget-object v6, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->timeZone:Ljava/lang/String;

    .line 276
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    .line 277
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v8

    move-object v3, p0

    .line 272
    invoke-direct/range {v3 .. v8}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->buildCustomReportRequest(Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    move-result-object p1

    .line 280
    sget-object v3, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$1;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p1, v3}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->getCustomReport(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    .line 281
    new-instance v3, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;

    invoke-direct {v3, p0, v2, v0, v1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;-><init>(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Ljava/util/Calendar;Ljava/util/Calendar;)V

    check-cast v3, Lrx/functions/Func1;

    invoke-virtual {p1, v3}, Lrx/Single;->map(Lrx/functions/Func1;)Lrx/Single;

    move-result-object p1

    const-string v0, "getCustomReport(graphReq\u2026ta)\n          )\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final createItemCategoryRows(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;",
            ">;"
        }
    .end annotation

    .line 453
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 454
    check-cast v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    .line 455
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    .line 456
    iget-object v3, v2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iget-object v3, v3, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->name:Ljava/lang/String;

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v3}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iget-object v3, v3, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->translation_type:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    sget-object v4, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_CATEGORY:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    if-ne v3, v4, :cond_0

    move-object v1, v2

    goto :goto_0

    .line 462
    :cond_0
    invoke-direct {p0, v0, v2}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->addItemCategory(Ljava/util/List;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)V

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    .line 465
    invoke-direct {p0, v0, v1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->addItemCategory(Ljava/util/List;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)V

    :cond_2
    return-object v0
.end method

.method private final createPaymentsRows(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportRow;",
            ">;"
        }
    .end annotation

    .line 446
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->salesSummaryRowGenerator:Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;

    .line 447
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->paymentMethodProcessor:Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;

    invoke-virtual {v1, p2}, Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;->buildPaymentMethods(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/reports/applet/sales/v1/PaymentMethods;

    move-result-object p2

    .line 446
    invoke-virtual {v0, p1, p2}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->buildPaymentsRows(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;Lcom/squareup/reports/applet/sales/v1/PaymentMethods;)Ljava/util/List;

    move-result-object p1

    const-string p2, "salesSummaryRowGenerator\u2026s(paymentMethodResponse))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final createReportFilters(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Filter;",
            ">;"
        }
    .end annotation

    .line 115
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->deviceIdProvider:Lcom/squareup/settings/DeviceIdProvider;

    invoke-interface {v0}, Lcom/squareup/settings/DeviceIdProvider;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deviceIdProvider.deviceId"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/List;

    .line 117
    iget-boolean v2, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->filterByThisDevice:Z

    if-eqz v2, :cond_0

    .line 119
    new-instance v2, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;-><init>()V

    .line 120
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    move-result-object v2

    .line 123
    new-instance v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;-><init>()V

    .line 124
    new-instance v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device$Builder;-><init>()V

    invoke-virtual {v4, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device$Builder;->device_token(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    move-result-object v0

    .line 123
    invoke-virtual {v3, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->device(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    move-result-object v0

    .line 122
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 121
    invoke-virtual {v2, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_value(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Filter;

    move-result-object v0

    const-string v2, "Filter.Builder()\n       \u2026)\n              ).build()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_0
    iget-object v0, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->selectedEmployees:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 132
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 133
    iget-object p1, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->selectedEmployees:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 135
    new-instance v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;-><init>()V

    .line 137
    new-instance v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee$Builder;-><init>()V

    .line 138
    invoke-virtual {v4, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee$Builder;

    move-result-object v2

    .line 139
    invoke-virtual {v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    move-result-object v2

    .line 136
    invoke-virtual {v3, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->employee(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    move-result-object v2

    .line 141
    invoke-virtual {v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    move-result-object v2

    .line 134
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 146
    :cond_1
    new-instance p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;-><init>()V

    .line 147
    sget-object v2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->EMPLOYEE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {p1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    move-result-object p1

    .line 148
    check-cast v0, Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_value(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    move-result-object p1

    .line 149
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Filter;

    move-result-object p1

    const-string v0, "Filter.Builder()\n       \u2026s)\n              .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    :cond_2
    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private final createSalesRows(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportRow;",
            ">;"
        }
    .end annotation

    .line 443
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->salesSummaryRowGenerator:Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;

    invoke-virtual {v0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->buildSalesRows(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)Ljava/util/List;

    move-result-object p1

    const-string v0, "salesSummaryRowGenerator.buildSalesRows(aggregate)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final dateTimeFromMillis(J)Lcom/squareup/protos/common/time/DateTime;
    .locals 2

    .line 648
    new-instance v0, Lcom/squareup/protos/common/time/DateTime$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/DateTime$Builder;-><init>()V

    .line 649
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, v0, Lcom/squareup/protos/common/time/DateTime$Builder;->instant_usec:Ljava/lang/Long;

    .line 650
    invoke-virtual {v0}, Lcom/squareup/protos/common/time/DateTime$Builder;->build()Lcom/squareup/protos/common/time/DateTime;

    move-result-object p1

    const-string p2, "DateTime.Builder().apply\u2026meMillis)\n      }.build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final downloadSubReports(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Ljava/util/List;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lrx/Single;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Filter;",
            ">;",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ">;)",
            "Lrx/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;",
            ">;>;"
        }
    .end annotation

    .line 172
    instance-of v0, p3, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    check-cast p3, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p3}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    .line 188
    iget-object v1, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    iget-object v2, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    iget-object v3, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->timeZone:Ljava/lang/String;

    .line 189
    sget-object v4, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->DISCOUNTS_GROUP_BY_TYPES:Ljava/util/List;

    move-object v0, p0

    move-object v5, p2

    .line 187
    invoke-direct/range {v0 .. v5}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->buildCustomReportRequest(Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    move-result-object v0

    .line 193
    iget-object v2, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    iget-object v3, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    iget-object v4, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->timeZone:Ljava/lang/String;

    .line 194
    sget-object v5, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->PAYMENT_METHOD_GROUP_BY_TYPES:Ljava/util/List;

    move-object v1, p0

    move-object v6, p2

    .line 192
    invoke-direct/range {v1 .. v6}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->buildCustomReportRequest(Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    move-result-object p2

    .line 201
    sget-object v1, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$1;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->getCustomReport(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object v0

    .line 202
    sget-object v1, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$2;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p2, v1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->getCustomReport(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p2

    .line 203
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->createChartData(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)Lrx/Single;

    move-result-object v1

    .line 204
    new-instance v2, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;

    invoke-direct {v2, p0, p3, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;-><init>(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/reports/applet/sales/v1/ReportConfig;)V

    check-cast v2, Lrx/functions/Func3;

    .line 197
    invoke-static {v0, p2, v1, v2}, Lrx/Single;->zip(Lrx/Single;Lrx/Single;Lrx/Single;Lrx/functions/Func3;)Lrx/Single;

    move-result-object p1

    const-string p2, "Single.zip<\n        Succ\u2026          )\n      )\n    }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 174
    :cond_0
    instance-of p2, p3, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p2, :cond_3

    check-cast p3, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p3}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p2

    .line 176
    instance-of v0, p2, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_1

    new-instance p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    .line 177
    invoke-static {p1}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->noTransactions(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    move-result-object p1

    .line 176
    invoke-direct {p2, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;-><init>(Ljava/lang/Object;)V

    invoke-static {p2}, Lrx/Single;->just(Ljava/lang/Object;)Lrx/Single;

    move-result-object p1

    const-string p2, "Single.just(HandleSucces\u2026\n            )\n        ))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 182
    :cond_1
    instance-of p1, p2, Lcom/squareup/receiving/ReceivedResponse$Error;

    if-eqz p1, :cond_2

    invoke-direct {p0, p3}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->mapFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    move-result-object p1

    invoke-static {p1}, Lrx/Single;->just(Ljava/lang/Object;)Lrx/Single;

    move-result-object p1

    const-string p2, "Single.just(salesSummary\u2026ssOrFailure.mapFailure())"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 183
    :cond_2
    new-instance p1, Lkotlin/NotImplementedError;

    const-string p2, "Shouldn\'t happen"

    invoke-direct {p1, p2}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 174
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final extractMeasurementUnits(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
            ">;"
        }
    .end annotation

    .line 569
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 571
    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    if-eqz v1, :cond_0

    .line 572
    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 575
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    const-string v2, "subreport"

    .line 576
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->extractMeasurementUnits(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Ljava/util/Set;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 579
    :cond_1
    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method private final getCustomReport(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;Lkotlin/jvm/functions/Function1;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lrx/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ">;>;"
        }
    .end annotation

    .line 160
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->customReportService:Lcom/squareup/customreport/data/service/CustomReportService;

    .line 161
    invoke-interface {v0, p1}, Lcom/squareup/customreport/data/service/CustomReportService;->getCustomReport(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 162
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 163
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$getCustomReport$1;

    invoke-direct {v0, p2}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$getCustomReport$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "customReportService\n    \u2026jectIfNot(isSuccessful) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/SingleSource;

    .line 164
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method private final mapError(Lcom/squareup/receiving/ReceivedResponse$Error;)Lcom/squareup/receiving/ReceivedResponse$Error;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/ReceivedResponse$Error<",
            "+TT;>;)",
            "Lcom/squareup/receiving/ReceivedResponse$Error<",
            "TU;>;"
        }
    .end annotation

    .line 654
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;->INSTANCE:Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error;

    goto :goto_0

    .line 655
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->getStatusCode()I

    move-result p1

    invoke-direct {v0, p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;-><init>(I)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error;

    goto :goto_0

    .line 656
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    new-instance p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    invoke-direct {p1, v1}, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error;

    goto :goto_0

    .line 657
    :cond_2
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getStatusCode()I

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;-><init>(Ljava/lang/Object;I)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error;

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final mapFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+TT;>;)",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "TU;>;"
        }
    .end annotation

    .line 661
    new-instance v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error;

    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->mapError(Lcom/squareup/receiving/ReceivedResponse$Error;)Lcom/squareup/receiving/ReceivedResponse$Error;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse;

    invoke-direct {v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;-><init>(Lcom/squareup/receiving/ReceivedResponse;)V

    return-object v0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.receiving.ReceivedResponse.Error<T>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final reportHasDefaultMeasurementUnit(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Z
    .locals 4

    .line 585
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 586
    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    if-nez v0, :cond_0

    return v1

    .line 591
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    if-nez v2, :cond_2

    const-string v2, "subreport"

    .line 593
    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->reportHasDefaultMeasurementUnit(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    return v2
.end method

.method private final seeMeasurementUnit()Z
    .locals 2

    .line 565
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REPORTS_SEE_MEASUREMENT_UNIT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final createDiscountRows(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_5

    .line 603
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_3

    .line 608
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    const-string v0, "discounts.custom_report"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 685
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 686
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    .line 611
    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iget-object v2, v2, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->translation_type:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    sget-object v3, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_DISCOUNT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    if-eq v2, v3, :cond_2

    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 687
    :cond_3
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 688
    new-instance p1, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 689
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 690
    check-cast v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    .line 614
    iget-object v2, v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    .line 615
    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    .line 616
    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const-string v4, "it.group_by_value.discou\u2026.name_or_translation_type"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->formatName(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;

    move-result-object v1

    .line 617
    iget-object v3, v2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_count:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 618
    iget-object v4, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v4, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 619
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 620
    new-instance v4, Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;

    invoke-direct {v4, v1, v3, v2}, Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    invoke-interface {p1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 691
    :cond_4
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 692
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createDiscountRows$$inlined$sortedBy$1;

    invoke-direct {v0}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createDiscountRows$$inlined$sortedBy$1;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 605
    :cond_5
    :goto_3
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final downloadReport(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)Lrx/Single;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;",
            ">;>;"
        }
    .end annotation

    const-string v0, "reportConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->createReportFilters(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)Ljava/util/List;

    move-result-object v0

    .line 89
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->seeMeasurementUnit()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    sget-object v1, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->SALES_SUMMARY_GROUP_BY_TYPES_MEASUREMENT_UNIT:Ljava/util/List;

    goto :goto_0

    .line 92
    :cond_0
    sget-object v1, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->SALES_SUMMARY_GROUP_BY_TYPES:Ljava/util/List;

    :goto_0
    move-object v5, v1

    .line 96
    iget-object v2, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    .line 97
    iget-object v3, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    .line 98
    iget-object v4, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->timeZone:Ljava/lang/String;

    move-object v1, p0

    move-object v6, v0

    .line 95
    invoke-direct/range {v1 .. v6}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->buildCustomReportRequest(Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    move-result-object v1

    .line 104
    sget-object v2, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$1;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v1, v2}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->getCustomReport(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object v1

    .line 109
    new-instance v2, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$2;

    invoke-direct {v2, p0, p1, v0}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$2;-><init>(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/reports/applet/sales/v1/ReportConfig;Ljava/util/List;)V

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lrx/Single;->flatMap(Lrx/functions/Func1;)Lrx/Single;

    move-result-object p1

    const-string v0, "this\n        .getCustomR\u2026ccessOrFailure)\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final extractQuantity(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Ljava/math/BigDecimal;
    .locals 3

    const-string v0, "report"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 536
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->seeMeasurementUnit()Z

    move-result v0

    const-string v1, "report.aggregate.sales.item_quantity"

    if-nez v0, :cond_0

    .line 537
    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_quantity:Ljava/lang/String;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 540
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->extractMeasurementUnits(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Ljava/util/Set;

    move-result-object v0

    .line 542
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 543
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->reportHasDefaultMeasurementUnit(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return-object p1

    .line 544
    :cond_2
    :goto_0
    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_quantity:Ljava/lang/String;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final formatMeasurementUnit(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Ljava/lang/String;
    .locals 4

    const-string v0, "report"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 551
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->seeMeasurementUnit()Z

    move-result v0

    const-string v1, ""

    if-nez v0, :cond_0

    return-object v1

    .line 555
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->extractMeasurementUnits(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Ljava/util/Set;

    move-result-object v0

    .line 557
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->reportHasDefaultMeasurementUnit(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 558
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->single(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    return-object v1
.end method
