.class public final enum Lcom/squareup/permissions/PasscodesSettings$Timeout;
.super Ljava/lang/Enum;
.source "PasscodesSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/PasscodesSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Timeout"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/permissions/PasscodesSettings$Timeout;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/permissions/PasscodesSettings$Timeout;

.field public static final enum FIVE_MINUTES:Lcom/squareup/permissions/PasscodesSettings$Timeout;

.field public static final enum NEVER:Lcom/squareup/permissions/PasscodesSettings$Timeout;

.field public static final enum ONE_MINUTE:Lcom/squareup/permissions/PasscodesSettings$Timeout;

.field public static final enum THIRTY_SECONDS:Lcom/squareup/permissions/PasscodesSettings$Timeout;


# instance fields
.field public eventStreamLoggingDetail:Ljava/lang/String;

.field public oldTimeoutSetting:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 700
    new-instance v0, Lcom/squareup/permissions/PasscodesSettings$Timeout;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->NEVER:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    const/4 v2, 0x0

    const-string v3, "NEVER"

    const-string v4, "None"

    invoke-direct {v0, v3, v2, v1, v4}, Lcom/squareup/permissions/PasscodesSettings$Timeout;-><init>(Ljava/lang/String;ILcom/squareup/settings/server/EmployeeManagementSettings$Timeout;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/PasscodesSettings$Timeout;->NEVER:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    .line 701
    new-instance v0, Lcom/squareup/permissions/PasscodesSettings$Timeout;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_30S:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    const/4 v3, 0x1

    const-string v4, "THIRTY_SECONDS"

    const-string v5, "Thirty Seconds"

    invoke-direct {v0, v4, v3, v1, v5}, Lcom/squareup/permissions/PasscodesSettings$Timeout;-><init>(Ljava/lang/String;ILcom/squareup/settings/server/EmployeeManagementSettings$Timeout;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/PasscodesSettings$Timeout;->THIRTY_SECONDS:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    .line 702
    new-instance v0, Lcom/squareup/permissions/PasscodesSettings$Timeout;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_1M:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    const/4 v4, 0x2

    const-string v5, "ONE_MINUTE"

    const-string v6, "One Minute"

    invoke-direct {v0, v5, v4, v1, v6}, Lcom/squareup/permissions/PasscodesSettings$Timeout;-><init>(Ljava/lang/String;ILcom/squareup/settings/server/EmployeeManagementSettings$Timeout;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/PasscodesSettings$Timeout;->ONE_MINUTE:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    .line 703
    new-instance v0, Lcom/squareup/permissions/PasscodesSettings$Timeout;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_5M:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    const/4 v5, 0x3

    const-string v6, "FIVE_MINUTES"

    const-string v7, "Five Minutes"

    invoke-direct {v0, v6, v5, v1, v7}, Lcom/squareup/permissions/PasscodesSettings$Timeout;-><init>(Ljava/lang/String;ILcom/squareup/settings/server/EmployeeManagementSettings$Timeout;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/PasscodesSettings$Timeout;->FIVE_MINUTES:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/permissions/PasscodesSettings$Timeout;

    .line 699
    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$Timeout;->NEVER:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$Timeout;->THIRTY_SECONDS:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$Timeout;->ONE_MINUTE:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$Timeout;->FIVE_MINUTES:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/permissions/PasscodesSettings$Timeout;->$VALUES:[Lcom/squareup/permissions/PasscodesSettings$Timeout;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/settings/server/EmployeeManagementSettings$Timeout;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 709
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 710
    iput-object p3, p0, Lcom/squareup/permissions/PasscodesSettings$Timeout;->oldTimeoutSetting:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    .line 711
    iput-object p4, p0, Lcom/squareup/permissions/PasscodesSettings$Timeout;->eventStreamLoggingDetail:Ljava/lang/String;

    return-void
.end method

.method public static fromOldTimeout(Lcom/squareup/settings/server/PasscodeTimeout;)Lcom/squareup/permissions/PasscodesSettings$Timeout;
    .locals 5

    .line 715
    invoke-static {}, Lcom/squareup/permissions/PasscodesSettings$Timeout;->values()[Lcom/squareup/permissions/PasscodesSettings$Timeout;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 716
    iget-object v4, v3, Lcom/squareup/permissions/PasscodesSettings$Timeout;->oldTimeoutSetting:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    if-ne v4, p0, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$Timeout;
    .locals 1

    .line 699
    const-class v0, Lcom/squareup/permissions/PasscodesSettings$Timeout;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/permissions/PasscodesSettings$Timeout;

    return-object p0
.end method

.method public static values()[Lcom/squareup/permissions/PasscodesSettings$Timeout;
    .locals 1

    .line 699
    sget-object v0, Lcom/squareup/permissions/PasscodesSettings$Timeout;->$VALUES:[Lcom/squareup/permissions/PasscodesSettings$Timeout;

    invoke-virtual {v0}, [Lcom/squareup/permissions/PasscodesSettings$Timeout;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/permissions/PasscodesSettings$Timeout;

    return-object v0
.end method
