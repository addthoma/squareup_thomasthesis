.class final enum Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;
.super Ljava/lang/Enum;
.source "PasscodeEmployeeManagement.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/PasscodeEmployeeManagement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ATTEMPT_SCREEN_LOCK_ACTION"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

.field public static final enum LOG_IN_GUEST_AND_SHOW_HOME_SCREEN_NO_HUD:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

.field public static final enum LOG_IN_GUEST_AND_SHOW_HOME_SCREEN_WITH_HUD:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

.field public static final enum SHOW_HOME_SCREEN:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

.field public static final enum SHOW_PASSCODE_SCREEN:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 114
    new-instance v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    const/4 v1, 0x0

    const-string v2, "SHOW_HOME_SCREEN"

    invoke-direct {v0, v2, v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->SHOW_HOME_SCREEN:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    .line 115
    new-instance v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    const/4 v2, 0x1

    const-string v3, "SHOW_PASSCODE_SCREEN"

    invoke-direct {v0, v3, v2}, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->SHOW_PASSCODE_SCREEN:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    .line 116
    new-instance v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    const/4 v3, 0x2

    const-string v4, "LOG_IN_GUEST_AND_SHOW_HOME_SCREEN_NO_HUD"

    invoke-direct {v0, v4, v3}, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->LOG_IN_GUEST_AND_SHOW_HOME_SCREEN_NO_HUD:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    .line 117
    new-instance v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    const/4 v4, 0x3

    const-string v5, "LOG_IN_GUEST_AND_SHOW_HOME_SCREEN_WITH_HUD"

    invoke-direct {v0, v5, v4}, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->LOG_IN_GUEST_AND_SHOW_HOME_SCREEN_WITH_HUD:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    .line 113
    sget-object v5, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->SHOW_HOME_SCREEN:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->SHOW_PASSCODE_SCREEN:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->LOG_IN_GUEST_AND_SHOW_HOME_SCREEN_NO_HUD:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->LOG_IN_GUEST_AND_SHOW_HOME_SCREEN_WITH_HUD:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->$VALUES:[Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 113
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;
    .locals 1

    .line 113
    const-class v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    return-object p0
.end method

.method public static values()[Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;
    .locals 1

    .line 113
    sget-object v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->$VALUES:[Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    invoke-virtual {v0}, [Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    return-object v0
.end method
