.class public interface abstract Lcom/squareup/permissions/EmployeesStore;
.super Ljava/lang/Object;
.source "EmployeesStore.java"


# virtual methods
.method public abstract allEmployees()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Employee;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract close()V
.end method

.method public abstract update(Lcom/squareup/permissions/Employee;)Lio/reactivex/Completable;
.end method

.method public abstract update(Ljava/util/Set;)Lio/reactivex/Completable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Employee;",
            ">;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation
.end method
