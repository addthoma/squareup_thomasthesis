.class public final Lcom/squareup/permissions/ui/RealLockScreenMonitor;
.super Ljava/lang/Object;
.source "RealLockScreenMonitor.kt"

# interfaces
.implements Lcom/squareup/permissions/ui/LockScreenMonitor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/permissions/ui/RealLockScreenMonitor$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealLockScreenMonitor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealLockScreenMonitor.kt\ncom/squareup/permissions/ui/RealLockScreenMonitor\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,221:1\n57#2,4:222\n*E\n*S KotlinDebug\n*F\n+ 1 RealLockScreenMonitor.kt\ncom/squareup/permissions/ui/RealLockScreenMonitor\n*L\n72#1,4:222\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\u0018\u0000 /2\u00020\u0001:\u0001/B]\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\u0002\u0010\u0017J\u0008\u0010\u001d\u001a\u00020\u001eH\u0002J\u0008\u0010\u001f\u001a\u00020 H\u0016J\u0008\u0010!\u001a\u00020\u001eH\u0016J\u0010\u0010\"\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020$H\u0002J\u0010\u0010%\u001a\u00020\u001e2\u0006\u0010&\u001a\u00020\'H\u0016J\u0008\u0010(\u001a\u00020\u001eH\u0016J\u0018\u0010)\u001a\u00020\u001e2\u0006\u0010*\u001a\u00020$2\u0006\u0010+\u001a\u00020 H\u0002J\u0008\u0010,\u001a\u00020\u001eH\u0002J\u0008\u0010-\u001a\u00020\u001eH\u0002J\u0008\u0010.\u001a\u00020$H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0018\u001a\u00020\u00048BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001b\u0010\u001c\u001a\u0004\u0008\u0019\u0010\u001aR\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/permissions/ui/RealLockScreenMonitor;",
        "Lcom/squareup/permissions/ui/LockScreenMonitor;",
        "lazyFlow",
        "Ldagger/Lazy;",
        "Lflow/Flow;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "passcodeEmployeeManagement",
        "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
        "permissionGatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "posContainer",
        "Lcom/squareup/ui/main/PosContainer;",
        "hudToaster",
        "Lcom/squareup/hudtoaster/HudToaster;",
        "timecardsStarter",
        "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
        "jailKeeper",
        "Lcom/squareup/jailkeeper/JailKeeper;",
        "(Ldagger/Lazy;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/timecards/api/TimecardsLauncher;Lcom/squareup/jailkeeper/JailKeeper;)V",
        "flow",
        "getFlow",
        "()Lflow/Flow;",
        "flow$delegate",
        "Ldagger/Lazy;",
        "bootstrap",
        "",
        "getEmployeeLockScreenKey",
        "Lcom/squareup/container/ContainerTreeKey;",
        "goToEmployeeLockScreen",
        "onEmployeeLogOut",
        "showLogOutHud",
        "",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onLockAttempt",
        "locking",
        "currentPath",
        "resetHistoryOrShowTimecardsPrompt",
        "resetHistoryOrShowTimecardsPromptLegacy",
        "showLockScreen",
        "Companion",
        "employees-screens_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/squareup/permissions/ui/RealLockScreenMonitor$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final LOGGED_OUT_TOAST_BUNDLE:Lcom/squareup/hudtoaster/HudToaster$ToastBundle;


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow$delegate:Ldagger/Lazy;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final posContainer:Lcom/squareup/ui/main/PosContainer;

.field private final timecardsStarter:Lcom/squareup/ui/timecards/api/TimecardsLauncher;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/permissions/ui/RealLockScreenMonitor;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "flow"

    const-string v4, "getFlow()Lflow/Flow;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/squareup/permissions/ui/RealLockScreenMonitor$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/permissions/ui/RealLockScreenMonitor$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->Companion:Lcom/squareup/permissions/ui/RealLockScreenMonitor$Companion;

    .line 208
    new-instance v0, Lcom/squareup/permissions/ui/RealLockScreenMonitor$Companion$LOGGED_OUT_TOAST_BUNDLE$1;

    invoke-direct {v0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor$Companion$LOGGED_OUT_TOAST_BUNDLE$1;-><init>()V

    check-cast v0, Lcom/squareup/hudtoaster/HudToaster$ToastBundle;

    sput-object v0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->LOGGED_OUT_TOAST_BUNDLE:Lcom/squareup/hudtoaster/HudToaster$ToastBundle;

    return-void
.end method

.method public constructor <init>(Ldagger/Lazy;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/timecards/api/TimecardsLauncher;Lcom/squareup/jailkeeper/JailKeeper;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/hudtoaster/HudToaster;",
            "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "lazyFlow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "passcodeEmployeeManagement"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionGatekeeper"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "posContainer"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hudToaster"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "timecardsStarter"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jailKeeper"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p3, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->features:Lcom/squareup/settings/server/Features;

    iput-object p4, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iput-object p5, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    iput-object p6, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p7, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->posContainer:Lcom/squareup/ui/main/PosContainer;

    iput-object p8, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    iput-object p9, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->timecardsStarter:Lcom/squareup/ui/timecards/api/TimecardsLauncher;

    iput-object p10, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    .line 51
    iput-object p1, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->flow$delegate:Ldagger/Lazy;

    return-void
.end method

.method public static final synthetic access$bootstrap(Lcom/squareup/permissions/ui/RealLockScreenMonitor;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->bootstrap()V

    return-void
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/permissions/ui/RealLockScreenMonitor;)Lflow/Flow;
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->getFlow()Lflow/Flow;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getLOGGED_OUT_TOAST_BUNDLE$cp()Lcom/squareup/hudtoaster/HudToaster$ToastBundle;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->LOGGED_OUT_TOAST_BUNDLE:Lcom/squareup/hudtoaster/HudToaster$ToastBundle;

    return-object v0
.end method

.method public static final synthetic access$onEmployeeLogOut(Lcom/squareup/permissions/ui/RealLockScreenMonitor;Z)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->onEmployeeLogOut(Z)V

    return-void
.end method

.method public static final synthetic access$onLockAttempt(Lcom/squareup/permissions/ui/RealLockScreenMonitor;ZLcom/squareup/container/ContainerTreeKey;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->onLockAttempt(ZLcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method private final bootstrap()V
    .locals 1

    .line 111
    invoke-virtual {p0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->showLockScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->goToEmployeeLockScreen()V

    :cond_0
    return-void
.end method

.method private final getFlow()Lflow/Flow;
    .locals 3

    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->flow$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    return-object v0
.end method

.method private final onEmployeeLogOut(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 200
    iget-object p1, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->LOGGED_OUT_TOAST_BUNDLE:Lcom/squareup/hudtoaster/HudToaster$ToastBundle;

    invoke-interface {p1, v0}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    .line 202
    :cond_0
    iget-object p1, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->posContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {p1}, Lcom/squareup/ui/main/PosContainer;->resetHistory()V

    return-void
.end method

.method private final onLockAttempt(ZLcom/squareup/container/ContainerTreeKey;)V
    .locals 1

    if-eqz p1, :cond_2

    .line 124
    iget-object p1, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result p1

    if-nez p1, :cond_0

    .line 125
    invoke-virtual {p0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->goToEmployeeLockScreen()V

    .line 126
    iget-object p1, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_EMPLOYEE_MANAGEMENT_SWITCH_EMPLOYEES:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 130
    :cond_0
    const-class p1, Lcom/squareup/ui/permissions/PermissionDeniedScreen;

    .line 129
    invoke-virtual {p2, p1}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 135
    iget-object p1, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/permissions/PermissionGatekeeper;->dismiss(Z)V

    .line 137
    :cond_1
    iget-object p1, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->clearCurrentEmployee()V

    .line 138
    invoke-virtual {p0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->goToEmployeeLockScreen()V

    .line 139
    iget-object p1, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object p2, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_EMPLOYEE_MANAGEMENT_SWITCH_EMPLOYEES:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    goto :goto_0

    .line 141
    :cond_2
    iget-object p1, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->features:Lcom/squareup/settings/server/Features;

    sget-object p2, Lcom/squareup/settings/server/Features$Feature;->USE_BREAK_TRACKING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, p2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 142
    invoke-direct {p0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->resetHistoryOrShowTimecardsPrompt()V

    goto :goto_0

    .line 144
    :cond_3
    invoke-direct {p0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->resetHistoryOrShowTimecardsPromptLegacy()V

    :goto_0
    return-void
.end method

.method private final resetHistoryOrShowTimecardsPrompt()V
    .locals 2

    .line 162
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->canCurrentEmployeeUseTimecards()Z

    move-result v0

    if-nez v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isRepeatedLogin()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-direct {p0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->getFlow()Lflow/Flow;

    move-result-object v0

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    goto :goto_0

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->posContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->resetHistory()V

    goto :goto_0

    .line 170
    :cond_1
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    invoke-interface {v0}, Lcom/squareup/jailkeeper/JailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v0

    sget-object v1, Lcom/squareup/jailkeeper/JailKeeper$State;->READY:Lcom/squareup/jailkeeper/JailKeeper$State;

    if-eq v0, v1, :cond_2

    .line 174
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->posContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->resetHistory()V

    goto :goto_0

    .line 176
    :cond_2
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->timecardsStarter:Lcom/squareup/ui/timecards/api/TimecardsLauncher;

    invoke-interface {v0}, Lcom/squareup/ui/timecards/api/TimecardsLauncher;->showTimecardsLoadingScreenFromHome()V

    :goto_0
    return-void
.end method

.method private final resetHistoryOrShowTimecardsPromptLegacy()V
    .locals 1

    .line 182
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isRepeatedLogin()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    invoke-direct {p0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->getFlow()Lflow/Flow;

    move-result-object v0

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    goto :goto_0

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isCurrentEmployeeNotClockedIn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->timecardsStarter:Lcom/squareup/ui/timecards/api/TimecardsLauncher;

    invoke-interface {v0}, Lcom/squareup/ui/timecards/api/TimecardsLauncher;->showClockInOrContinueScreenFromHome()V

    goto :goto_0

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->posContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->resetHistory()V

    :goto_0
    return-void
.end method


# virtual methods
.method public getEmployeeLockScreenKey()Lcom/squareup/container/ContainerTreeKey;
    .locals 2

    .line 96
    sget-object v0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;->INSTANCE:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;

    const-string v1, "EnterPasscodeToUnlockScreen.INSTANCE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method

.method public goToEmployeeLockScreen()V
    .locals 2

    .line 90
    invoke-static {}, Lcom/squareup/widgets/glass/GlassConfirmController;->instance()Lcom/squareup/widgets/glass/GlassConfirmController;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/squareup/widgets/glass/GlassConfirmController;->maybeCancelAndRemoveGlass()Z

    .line 92
    invoke-direct {p0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->getFlow()Lflow/Flow;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->getEmployeeLockScreenKey()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 55
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onLockAttempt()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "passcodeEmployeeManageme\u2026\n        .onLockAttempt()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v1, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->posContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-static {v1}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v1

    check-cast v1, Lio/reactivex/ObservableSource;

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    .line 57
    new-instance v1, Lcom/squareup/permissions/ui/RealLockScreenMonitor$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor$onEnterScope$1;-><init>(Lcom/squareup/permissions/ui/RealLockScreenMonitor;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "passcodeEmployeeManageme\u2026kAttempt(locking, path) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 61
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onEmployeeLogOut()Lio/reactivex/Observable;

    move-result-object v0

    .line 62
    new-instance v1, Lcom/squareup/permissions/ui/RealLockScreenMonitor$onEnterScope$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/permissions/ui/RealLockScreenMonitor;

    invoke-direct {v1, v2}, Lcom/squareup/permissions/ui/RealLockScreenMonitor$onEnterScope$2;-><init>(Lcom/squareup/permissions/ui/RealLockScreenMonitor;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/permissions/ui/RealLockScreenMonitor$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/permissions/ui/RealLockScreenMonitor$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "passcodeEmployeeManageme\u2026cribe(::onEmployeeLogOut)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->posContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->hasViewNow()Lio/reactivex/Observable;

    move-result-object v0

    .line 66
    sget-object v1, Lcom/squareup/permissions/ui/RealLockScreenMonitor$onEnterScope$3;->INSTANCE:Lcom/squareup/permissions/ui/RealLockScreenMonitor$onEnterScope$3;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    .line 69
    new-instance v1, Lcom/squareup/permissions/ui/RealLockScreenMonitor$onEnterScope$4;

    invoke-direct {v1, p0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor$onEnterScope$4;-><init>(Lcom/squareup/permissions/ui/RealLockScreenMonitor;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "posContainer.hasViewNow(\u2026subscribe { bootstrap() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 72
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 73
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    invoke-virtual {v0}, Lcom/squareup/permissions/PermissionGatekeeper;->shouldBeShowingPermissionDeniedScreen()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "permissionGatekeeper.sho\u2026gPermissionDeniedScreen()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    iget-object v1, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->posContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v1}, Lcom/squareup/ui/main/PosContainer;->hasViewNow()Lio/reactivex/Observable;

    move-result-object v1

    .line 223
    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 224
    new-instance v2, Lcom/squareup/permissions/ui/RealLockScreenMonitor$onEnterScope$$inlined$combineLatest$1;

    invoke-direct {v2}, Lcom/squareup/permissions/ui/RealLockScreenMonitor$onEnterScope$$inlined$combineLatest$1;-><init>()V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    .line 222
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ineFunction(t1, t2) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    sget-object v1, Lcom/squareup/permissions/ui/RealLockScreenMonitor$onEnterScope$6;->INSTANCE:Lcom/squareup/permissions/ui/RealLockScreenMonitor$onEnterScope$6;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 77
    new-instance v1, Lcom/squareup/permissions/ui/RealLockScreenMonitor$onEnterScope$7;

    invoke-direct {v1, p0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor$onEnterScope$7;-><init>(Lcom/squareup/permissions/ui/RealLockScreenMonitor;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "combineLatest(\n        p\u2026creen.INSTANCE)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public showLockScreen()Z
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->shouldAutoLoginAsGuest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/MerchantRegisterSettings;->useTeamPermissions()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
