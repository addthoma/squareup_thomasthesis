.class public final Lcom/squareup/permissions/RealEmployeeManagement_Factory;
.super Ljava/lang/Object;
.source "RealEmployeeManagement_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/permissions/RealEmployeeManagement;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementModeDeciderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/permissions/RealEmployeeManagement_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/permissions/RealEmployeeManagement_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/permissions/RealEmployeeManagement_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/permissions/RealEmployeeManagement_Factory;->employeeManagementSettingsProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/permissions/RealEmployeeManagement_Factory;->resProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/permissions/RealEmployeeManagement_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/permissions/RealEmployeeManagement_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/permissions/RealEmployeeManagement_Factory;"
        }
    .end annotation

    .line 57
    new-instance v7, Lcom/squareup/permissions/RealEmployeeManagement_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/permissions/RealEmployeeManagement_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)Lcom/squareup/permissions/RealEmployeeManagement;
    .locals 8

    .line 64
    new-instance v7, Lcom/squareup/permissions/RealEmployeeManagement;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/permissions/RealEmployeeManagement;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/permissions/RealEmployeeManagement;
    .locals 7

    .line 48
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/permissions/EmployeeManagementModeDecider;

    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement_Factory;->employeeManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/EmployeeManagementSettings;

    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/analytics/Analytics;

    invoke-static/range {v1 .. v6}, Lcom/squareup/permissions/RealEmployeeManagement_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)Lcom/squareup/permissions/RealEmployeeManagement;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/permissions/RealEmployeeManagement_Factory;->get()Lcom/squareup/permissions/RealEmployeeManagement;

    move-result-object v0

    return-object v0
.end method
