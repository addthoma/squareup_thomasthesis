.class public final enum Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;
.super Ljava/lang/Enum;
.source "EmployeeManagementModeDecider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/EmployeeManagementModeDecider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

.field public static final enum EMPLOYEE_LOGIN:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

.field public static final enum OWNER:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

.field public static final enum PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 40
    new-instance v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    const/4 v1, 0x0

    const-string v2, "PASSCODE_EMPLOYEE_MANAGEMENT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    .line 52
    new-instance v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    const/4 v2, 0x1

    const-string v3, "EMPLOYEE_LOGIN"

    invoke-direct {v0, v3, v2}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->EMPLOYEE_LOGIN:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    .line 61
    new-instance v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    const/4 v3, 0x2

    const-string v4, "OWNER"

    invoke-direct {v0, v4, v3}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->OWNER:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    .line 26
    sget-object v4, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->EMPLOYEE_LOGIN:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->OWNER:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->$VALUES:[Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;
    .locals 1

    .line 26
    const-class v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;
    .locals 1

    .line 26
    sget-object v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->$VALUES:[Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    invoke-virtual {v0}, [Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    return-object v0
.end method
