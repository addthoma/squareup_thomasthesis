.class abstract Lcom/squareup/persistent/PersistentFile;
.super Ljava/lang/Object;
.source "PersistentFile.java"

# interfaces
.implements Lcom/squareup/persistent/Persistent;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/persistent/Persistent<",
        "TT;>;"
    }
.end annotation


# instance fields
.field protected final file:Ljava/io/File;

.field private final fileThreadExecutor:Ljava/util/concurrent/Executor;

.field private final mainThreadExecutor:Ljava/util/concurrent/Executor;

.field protected value:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ljava/io/File;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/persistent/PersistentFile;->file:Ljava/io/File;

    .line 43
    iput-object p2, p0, Lcom/squareup/persistent/PersistentFile;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    .line 44
    iput-object p3, p0, Lcom/squareup/persistent/PersistentFile;->mainThreadExecutor:Ljava/util/concurrent/Executor;

    .line 45
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 46
    invoke-static {p1}, Lcom/squareup/util/Files;->mkdirsSafe(Ljava/io/File;)V

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/squareup/persistent/PersistentFile;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Lcom/squareup/persistent/PersistentFile;->loadFromFile()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/persistent/PersistentFile;Lcom/squareup/persistent/AsyncCallback;ZLjava/lang/Object;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/persistent/PersistentFile;->callBackWithSuccess(Lcom/squareup/persistent/AsyncCallback;ZLjava/lang/Object;)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/persistent/PersistentFile;Lcom/squareup/persistent/AsyncCallback;ZLjava/lang/Throwable;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/persistent/PersistentFile;->callBackWithError(Lcom/squareup/persistent/AsyncCallback;ZLjava/lang/Throwable;)V

    return-void
.end method

.method private callBackWithError(Lcom/squareup/persistent/AsyncCallback;ZLjava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/persistent/AsyncCallback<",
            "*>;Z",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    if-eqz p2, :cond_1

    .line 187
    iget-object p2, p0, Lcom/squareup/persistent/PersistentFile;->mainThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/squareup/persistent/PersistentFile$2;

    invoke-direct {v0, p0, p1, p3}, Lcom/squareup/persistent/PersistentFile$2;-><init>(Lcom/squareup/persistent/PersistentFile;Lcom/squareup/persistent/AsyncCallback;Ljava/lang/Throwable;)V

    invoke-interface {p2, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 194
    :cond_1
    invoke-interface {p1, p3}, Lcom/squareup/persistent/AsyncCallback;->onError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method private callBackWithSuccess(Lcom/squareup/persistent/AsyncCallback;ZLjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/persistent/AsyncCallback<",
            "TT;>;ZTT;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    if-eqz p2, :cond_1

    .line 174
    iget-object p2, p0, Lcom/squareup/persistent/PersistentFile;->mainThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/squareup/persistent/-$$Lambda$PersistentFile$8Lv_x7D-Fqcs8Va_foUR9Xhq_7I;

    invoke-direct {v0, p1, p3}, Lcom/squareup/persistent/-$$Lambda$PersistentFile$8Lv_x7D-Fqcs8Va_foUR9Xhq_7I;-><init>(Lcom/squareup/persistent/AsyncCallback;Ljava/lang/Object;)V

    invoke-interface {p2, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 177
    :cond_1
    invoke-interface {p1, p3}, Lcom/squareup/persistent/AsyncCallback;->onSuccess(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private static isOnMainThread()Z
    .locals 2

    .line 28
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic lambda$callBackWithSuccess$1(Lcom/squareup/persistent/AsyncCallback;Ljava/lang/Object;)V
    .locals 0

    .line 174
    invoke-interface {p0, p1}, Lcom/squareup/persistent/AsyncCallback;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method

.method private loadFromFile()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 102
    iget-object v0, p0, Lcom/squareup/persistent/PersistentFile;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 103
    :cond_0
    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    iget-object v2, p0, Lcom/squareup/persistent/PersistentFile;->file:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/16 v2, 0x80

    invoke-direct {v0, v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 105
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/squareup/persistent/PersistentFile;->read(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 108
    throw v1
.end method

.method private runOnFileThread(Ljava/lang/Runnable;)V
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/persistent/PersistentFile;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public get(Lcom/squareup/persistent/AsyncCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/persistent/AsyncCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 140
    iget-object v0, p0, Lcom/squareup/persistent/PersistentFile;->value:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 142
    invoke-direct {p0, p1, v1, v0}, Lcom/squareup/persistent/PersistentFile;->callBackWithSuccess(Lcom/squareup/persistent/AsyncCallback;ZLjava/lang/Object;)V

    goto :goto_0

    .line 144
    :cond_0
    invoke-static {}, Lcom/squareup/persistent/PersistentFile;->isOnMainThread()Z

    move-result v0

    .line 145
    new-instance v1, Lcom/squareup/persistent/PersistentFile$1;

    invoke-direct {v1, p0, p1, v0}, Lcom/squareup/persistent/PersistentFile$1;-><init>(Lcom/squareup/persistent/PersistentFile;Lcom/squareup/persistent/AsyncCallback;Z)V

    .line 164
    invoke-direct {p0, v1}, Lcom/squareup/persistent/PersistentFile;->runOnFileThread(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method public final getSynchronous()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 91
    iget-object v0, p0, Lcom/squareup/persistent/PersistentFile;->value:Ljava/lang/Object;

    if-eqz v0, :cond_0

    return-object v0

    .line 94
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/persistent/PersistentFile;->loadFromFile()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/persistent/PersistentFile;->value:Ljava/lang/Object;

    .line 95
    iget-object v0, p0, Lcom/squareup/persistent/PersistentFile;->value:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 97
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Problem reading PersistentFile"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public synthetic lambda$set$0$PersistentFile(Ljava/lang/Object;Lcom/squareup/persistent/AsyncCallback;Z)V
    .locals 2

    .line 119
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/squareup/persistent/PersistentFile;->setSynchronous(Ljava/lang/Object;)V

    .line 120
    invoke-direct {p0, p2, p3, p1}, Lcom/squareup/persistent/PersistentFile;->callBackWithSuccess(Lcom/squareup/persistent/AsyncCallback;ZLjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p2

    const/4 p3, 0x0

    .line 122
    iput-object p3, p0, Lcom/squareup/persistent/PersistentFile;->value:Ljava/lang/Object;

    .line 124
    iget-object p3, p0, Lcom/squareup/persistent/PersistentFile;->file:Ljava/io/File;

    invoke-virtual {p3}, Ljava/io/File;->exists()Z

    move-result p3

    if-eqz p3, :cond_0

    iget-object p3, p0, Lcom/squareup/persistent/PersistentFile;->file:Ljava/io/File;

    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    move-result p3

    if-nez p3, :cond_0

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    const/4 v0, 0x0

    .line 125
    iget-object v1, p0, Lcom/squareup/persistent/PersistentFile;->file:Ljava/io/File;

    aput-object v1, p3, v0

    const-string v0, "Failed to delete %s."

    invoke-static {v0, p3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 127
    :cond_0
    new-instance p3, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to write "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " to "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/persistent/PersistentFile;->file:Ljava/io/File;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p3
.end method

.method protected abstract read(Ljava/io/InputStream;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public set(Ljava/lang/Object;Lcom/squareup/persistent/AsyncCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/squareup/persistent/AsyncCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 114
    iput-object p1, p0, Lcom/squareup/persistent/PersistentFile;->value:Ljava/lang/Object;

    .line 116
    invoke-static {}, Lcom/squareup/persistent/PersistentFile;->isOnMainThread()Z

    move-result v0

    .line 117
    new-instance v1, Lcom/squareup/persistent/-$$Lambda$PersistentFile$tGoceAnzuZKKTUoYX4bLDrJjT2Q;

    invoke-direct {v1, p0, p1, p2, v0}, Lcom/squareup/persistent/-$$Lambda$PersistentFile$tGoceAnzuZKKTUoYX4bLDrJjT2Q;-><init>(Lcom/squareup/persistent/PersistentFile;Ljava/lang/Object;Lcom/squareup/persistent/AsyncCallback;Z)V

    .line 130
    invoke-direct {p0, v1}, Lcom/squareup/persistent/PersistentFile;->runOnFileThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setSynchronous(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const/4 v0, 0x1

    .line 62
    invoke-virtual {p0, p1, v0}, Lcom/squareup/persistent/PersistentFile;->setSynchronous(Ljava/lang/Object;Z)V

    return-void
.end method

.method public setSynchronous(Ljava/lang/Object;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    .line 67
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/squareup/persistent/PersistentFile;->file:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :try_start_1
    invoke-virtual {p0, p1, v0}, Lcom/squareup/persistent/PersistentFile;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V

    if-eqz p2, :cond_0

    .line 73
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object p2

    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Ljava/nio/channels/FileChannel;->force(Z)V

    .line 75
    :cond_0
    iput-object p1, p0, Lcom/squareup/persistent/PersistentFile;->value:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    return-void

    :catchall_0
    move-exception p2

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 78
    throw p2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception p2

    .line 80
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Problem writing value to PersistentFile: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method protected abstract write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
