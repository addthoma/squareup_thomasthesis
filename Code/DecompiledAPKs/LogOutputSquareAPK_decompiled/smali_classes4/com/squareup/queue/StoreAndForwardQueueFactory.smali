.class public Lcom/squareup/queue/StoreAndForwardQueueFactory;
.super Ljava/lang/Object;
.source "StoreAndForwardQueueFactory.java"

# interfaces
.implements Lcom/squareup/queue/retrofit/QueueFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/retrofit/QueueFactory<",
        "Lcom/squareup/queue/StoredPaymentsQueue;",
        ">;"
    }
.end annotation


# static fields
.field private static final MAX_LENGTH:I = 0x4000

.field private static final QUEUE_FILE_HEADER_LENGTH:I = 0x10


# instance fields
.field protected final converter:Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;

.field protected final fileThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/log/OhSnapLogger;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;)V
    .locals 0
    .param p2    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 42
    iput-object p2, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory;->fileThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    .line 43
    iput-object p3, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory;->converter:Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;

    return-void
.end method

.method private deleteQueueFileAndLogContent(Ljava/io/File;)V
    .locals 5

    const/16 v0, 0x4000

    .line 67
    invoke-direct {p0, p1, v0}, Lcom/squareup/queue/StoreAndForwardQueueFactory;->readQueueFileData(Ljava/io/File;I)[B

    move-result-object v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->CORRUPT_QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Queue file contents: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v4, 0x2

    .line 70
    invoke-static {v0, v4}, Lcom/squareup/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 69
    invoke-interface {v1, v2, v0}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 72
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    .line 73
    iget-object v1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->CORRUPT_QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v0, :cond_1

    const-string v0, "Deleted"

    goto :goto_0

    :cond_1
    const-string v0, "Unable to delete"

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " corrupt queue file "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 73
    invoke-interface {v1, v2, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 75
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Deleted corrupt store and forward queue."

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    return-void
.end method

.method private readQueueFileData(Ljava/io/File;I)[B
    .locals 10

    const-string v0, ", length="

    const-string v1, "Unable to open queue, file="

    .line 79
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 80
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v3

    const/4 v5, 0x0

    .line 84
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/squareup/queue/StoreAndForwardQueueFactory;->readFileData(Ljava/io/File;I)[B

    move-result-object v5

    .line 86
    iget-object p1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object p2, Lcom/squareup/log/OhSnapLogger$EventType;->CORRUPT_QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v7, ", header="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v7, 0x0

    const/16 v8, 0x10

    const/4 v9, 0x2

    .line 88
    invoke-static {v5, v7, v8, v9}, Lcom/squareup/util/Base64;->encodeToString([BIII)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 86
    invoke-interface {p1, p2, v6}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_0
    iget-object p1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object p2, Lcom/squareup/log/OhSnapLogger$EventType;->CORRUPT_QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Queue file does not exist, file="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, p2, v6}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 93
    :catch_0
    iget-object p1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object p2, Lcom/squareup/log/OhSnapLogger$EventType;->CORRUPT_QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, ", file unreadable"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, p2, v0}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    :goto_0
    return-object v5
.end method


# virtual methods
.method public open(Ljava/io/File;)Lcom/squareup/queue/StoredPaymentsQueue;
    .locals 5

    .line 47
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    .line 49
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/squareup/queue/StoreAndForwardQueueFactory;->openQueueThrowing(Ljava/io/File;)Lcom/squareup/queue/StoredPaymentsQueue;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception v1

    if-eqz v0, :cond_0

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/queue/StoreAndForwardQueueFactory;->deleteQueueFileAndLogContent(Ljava/io/File;)V

    .line 56
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/squareup/queue/StoreAndForwardQueueFactory;->openQueueThrowing(Ljava/io/File;)Lcom/squareup/queue/StoredPaymentsQueue;

    move-result-object v0

    .line 57
    invoke-static {v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    return-object v0

    .line 60
    :catch_1
    iget-object v0, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->CORRUPT_QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed after deleted queue file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v2, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 61
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw p1

    .line 52
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not create queue file "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public bridge synthetic open(Ljava/io/File;)Lcom/squareup/tape/ObjectQueue;
    .locals 0

    .line 26
    invoke-virtual {p0, p1}, Lcom/squareup/queue/StoreAndForwardQueueFactory;->open(Ljava/io/File;)Lcom/squareup/queue/StoredPaymentsQueue;

    move-result-object p1

    return-object p1
.end method

.method protected openQueueThrowing(Ljava/io/File;)Lcom/squareup/queue/StoredPaymentsQueue;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 100
    new-instance v0, Lcom/squareup/queue/ReadableFileObjectQueue;

    iget-object v1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory;->converter:Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;

    invoke-direct {v0, p1, v1}, Lcom/squareup/queue/ReadableFileObjectQueue;-><init>(Ljava/io/File;Lcom/squareup/tape/FileObjectQueue$Converter;)V

    .line 101
    new-instance p1, Lcom/squareup/queue/StoredPaymentsQueue;

    iget-object v1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory;->fileThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-direct {p1, v0, v1}, Lcom/squareup/queue/StoredPaymentsQueue;-><init>(Lcom/squareup/queue/ReadableFileObjectQueue;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object p1
.end method

.method protected readFileData(Ljava/io/File;I)[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    int-to-long v0, p2

    .line 105
    invoke-static {p1, v0, v1}, Lcom/squareup/util/Files;->readPartially(Ljava/io/File;J)[B

    move-result-object p1

    return-object p1
.end method
