.class public final Lcom/squareup/queue/crm/ReturnCouponTask_MembersInjector;
.super Ljava/lang/Object;
.source "ReturnCouponTask_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/crm/ReturnCouponTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final loyaltyServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/queue/crm/ReturnCouponTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/queue/crm/ReturnCouponTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/queue/crm/ReturnCouponTask_MembersInjector;->loyaltyServiceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/crm/ReturnCouponTask;",
            ">;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/queue/crm/ReturnCouponTask_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/queue/crm/ReturnCouponTask_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectLoyaltyService(Lcom/squareup/queue/crm/ReturnCouponTask;Lcom/squareup/loyalty/LoyaltyServiceHelper;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/queue/crm/ReturnCouponTask;->loyaltyService:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/crm/ReturnCouponTask;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/queue/crm/ReturnCouponTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectMainScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/queue/crm/ReturnCouponTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectRpcScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/queue/crm/ReturnCouponTask_MembersInjector;->loyaltyServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/loyalty/LoyaltyServiceHelper;

    invoke-static {p1, v0}, Lcom/squareup/queue/crm/ReturnCouponTask_MembersInjector;->injectLoyaltyService(Lcom/squareup/queue/crm/ReturnCouponTask;Lcom/squareup/loyalty/LoyaltyServiceHelper;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/queue/crm/ReturnCouponTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/crm/ReturnCouponTask_MembersInjector;->injectMembers(Lcom/squareup/queue/crm/ReturnCouponTask;)V

    return-void
.end method
