.class public Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;
.super Ljava/lang/Object;
.source "SendBuyerLoyaltyStatusTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private employeeToken:Ljava/lang/String;

.field private phoneToken:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;)Ljava/lang/String;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;->phoneToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;)Ljava/lang/String;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;->employeeToken:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;
    .locals 2

    .line 49
    new-instance v0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;-><init>(Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$1;)V

    return-object v0
.end method

.method public employeeToken(Ljava/lang/String;)Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;->employeeToken:Ljava/lang/String;

    return-object p0
.end method

.method public phoneToken(Ljava/lang/String;)Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;->phoneToken:Ljava/lang/String;

    return-object p0
.end method
