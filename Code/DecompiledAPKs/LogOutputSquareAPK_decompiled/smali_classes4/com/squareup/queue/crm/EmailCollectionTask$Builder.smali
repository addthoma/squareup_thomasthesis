.class public Lcom/squareup/queue/crm/EmailCollectionTask$Builder;
.super Ljava/lang/Object;
.source "EmailCollectionTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/crm/EmailCollectionTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private buyerEmailAddress:Ljava/lang/String;

.field private customerContact:Lcom/squareup/protos/client/rolodex/Contact;

.field private paymentToken:Ljava/lang/String;

.field private requestToken:Ljava/util/UUID;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/queue/crm/EmailCollectionTask$Builder;)Ljava/util/UUID;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->requestToken:Ljava/util/UUID;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/queue/crm/EmailCollectionTask$Builder;)Ljava/lang/String;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->paymentToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/queue/crm/EmailCollectionTask$Builder;)Ljava/lang/String;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->buyerEmailAddress:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/queue/crm/EmailCollectionTask$Builder;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/queue/crm/EmailCollectionTask;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->requestToken:Ljava/util/UUID;

    if-nez v0, :cond_0

    .line 68
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->requestToken:Ljava/util/UUID;

    .line 70
    new-instance v0, Lcom/squareup/queue/crm/EmailCollectionTask;

    invoke-direct {v0, p0}, Lcom/squareup/queue/crm/EmailCollectionTask;-><init>(Lcom/squareup/queue/crm/EmailCollectionTask$Builder;)V

    return-object v0
.end method

.method public contact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/queue/crm/EmailCollectionTask$Builder;
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object p0
.end method

.method public emailAddress(Ljava/lang/String;)Lcom/squareup/queue/crm/EmailCollectionTask$Builder;
    .locals 0

    .line 52
    iput-object p1, p0, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->buyerEmailAddress:Ljava/lang/String;

    return-object p0
.end method

.method public paymentToken(Ljava/lang/String;)Lcom/squareup/queue/crm/EmailCollectionTask$Builder;
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->paymentToken:Ljava/lang/String;

    return-object p0
.end method

.method requestToken(Ljava/util/UUID;)Lcom/squareup/queue/crm/EmailCollectionTask$Builder;
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->requestToken:Ljava/util/UUID;

    return-object p0
.end method
