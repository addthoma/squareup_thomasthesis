.class Lcom/squareup/queue/Cash$1;
.super Lcom/squareup/server/SquareCallback;
.source "Cash.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/queue/Cash;->execute(Lcom/squareup/server/SquareCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/server/SquareCallback<",
        "Lcom/squareup/server/payment/CreatePaymentResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/queue/Cash;

.field final synthetic val$callback:Lcom/squareup/server/SquareCallback;


# direct methods
.method constructor <init>(Lcom/squareup/queue/Cash;Lcom/squareup/server/SquareCallback;)V
    .locals 0

    .line 224
    iput-object p1, p0, Lcom/squareup/queue/Cash$1;->this$0:Lcom/squareup/queue/Cash;

    iput-object p2, p0, Lcom/squareup/queue/Cash$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-direct {p0}, Lcom/squareup/server/SquareCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/server/payment/CreatePaymentResponse;)V
    .locals 6

    .line 227
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/server/payment/CreatePaymentResponse;->getPaymentId()Ljava/lang/String;

    move-result-object v0

    .line 229
    iget-object v1, p0, Lcom/squareup/queue/Cash$1;->this$0:Lcom/squareup/queue/Cash;

    iget-object v1, v1, Lcom/squareup/queue/Cash;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    iget-object v2, p0, Lcom/squareup/queue/Cash$1;->this$0:Lcom/squareup/queue/Cash;

    invoke-static {v2}, Lcom/squareup/queue/Cash;->access$000(Lcom/squareup/queue/Cash;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/squareup/server/payment/CreatePaymentResponse;->receipt_number:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Lcom/squareup/print/LocalTenderCache;->setLast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    iget-object v1, p0, Lcom/squareup/queue/Cash$1;->this$0:Lcom/squareup/queue/Cash;

    iget-object v1, v1, Lcom/squareup/queue/Cash;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    iget-object v0, p0, Lcom/squareup/queue/Cash$1;->this$0:Lcom/squareup/queue/Cash;

    invoke-static {v0}, Lcom/squareup/queue/Cash;->access$100(Lcom/squareup/queue/Cash;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/server/payment/CreatePaymentResponse;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/squareup/queue/Cash$1;->this$0:Lcom/squareup/queue/Cash;

    iget-object v0, v0, Lcom/squareup/queue/Cash;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v1, p0, Lcom/squareup/queue/Cash$1;->this$0:Lcom/squareup/queue/Cash;

    invoke-static {v1}, Lcom/squareup/queue/Cash;->access$100(Lcom/squareup/queue/Cash;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {p1}, Lcom/squareup/server/payment/CreatePaymentResponse;->getPaymentId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/queue/Cash$1;->this$0:Lcom/squareup/queue/Cash;

    invoke-static {v4}, Lcom/squareup/queue/Cash;->access$000(Lcom/squareup/queue/Cash;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/tickets/Tickets;->updateTerminalId(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)V

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/Cash$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, p1}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 236
    iget-object v1, p0, Lcom/squareup/queue/Cash$1;->this$0:Lcom/squareup/queue/Cash;

    invoke-static {v1}, Lcom/squareup/queue/Cash;->access$100(Lcom/squareup/queue/Cash;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/server/payment/CreatePaymentResponse;->isSuccessful()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 237
    iget-object v1, p0, Lcom/squareup/queue/Cash$1;->this$0:Lcom/squareup/queue/Cash;

    iget-object v1, v1, Lcom/squareup/queue/Cash;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v2, p0, Lcom/squareup/queue/Cash$1;->this$0:Lcom/squareup/queue/Cash;

    invoke-static {v2}, Lcom/squareup/queue/Cash;->access$100(Lcom/squareup/queue/Cash;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {p1}, Lcom/squareup/server/payment/CreatePaymentResponse;->getPaymentId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/queue/Cash$1;->this$0:Lcom/squareup/queue/Cash;

    invoke-static {v5}, Lcom/squareup/queue/Cash;->access$000(Lcom/squareup/queue/Cash;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2, v3}, Lcom/squareup/tickets/Tickets;->updateTerminalId(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)V

    .line 241
    :cond_1
    iget-object v1, p0, Lcom/squareup/queue/Cash$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v1, p1}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    .line 242
    throw v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 224
    check-cast p1, Lcom/squareup/server/payment/CreatePaymentResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/Cash$1;->call(Lcom/squareup/server/payment/CreatePaymentResponse;)V

    return-void
.end method

.method public clientError(Lcom/squareup/server/payment/CreatePaymentResponse;I)V
    .locals 1

    .line 250
    iget-object v0, p0, Lcom/squareup/queue/Cash$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/server/SquareCallback;->clientError(Ljava/lang/Object;I)V

    return-void
.end method

.method public bridge synthetic clientError(Ljava/lang/Object;I)V
    .locals 0

    .line 224
    check-cast p1, Lcom/squareup/server/payment/CreatePaymentResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/queue/Cash$1;->clientError(Lcom/squareup/server/payment/CreatePaymentResponse;I)V

    return-void
.end method

.method public networkError()V
    .locals 1

    .line 258
    iget-object v0, p0, Lcom/squareup/queue/Cash$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0}, Lcom/squareup/server/SquareCallback;->networkError()V

    return-void
.end method

.method public serverError(I)V
    .locals 1

    .line 254
    iget-object v0, p0, Lcom/squareup/queue/Cash$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, p1}, Lcom/squareup/server/SquareCallback;->serverError(I)V

    return-void
.end method

.method public sessionExpired()V
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/squareup/queue/Cash$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0}, Lcom/squareup/server/SquareCallback;->sessionExpired()V

    return-void
.end method

.method public unexpectedError(Ljava/lang/Throwable;)V
    .locals 1

    .line 262
    iget-object v0, p0, Lcom/squareup/queue/Cash$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, p1}, Lcom/squareup/server/SquareCallback;->unexpectedError(Ljava/lang/Throwable;)V

    return-void
.end method
