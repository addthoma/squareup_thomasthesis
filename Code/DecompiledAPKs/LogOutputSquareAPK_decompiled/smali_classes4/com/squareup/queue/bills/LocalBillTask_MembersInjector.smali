.class public final Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;
.super Ljava/lang/Object;
.source "LocalBillTask_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/bills/LocalBillTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final addTendersRequestServerIdsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;>;"
        }
    .end annotation
.end field

.field private final billCreationServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;"
        }
    .end annotation
.end field

.field private final lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final localTenderCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/LocalTenderCache;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderProtoFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderProtoFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/LocalTenderCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderProtoFactory;",
            ">;)V"
        }
    .end annotation

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p2, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p3, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->ticketsProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p4, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p5, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->addTendersRequestServerIdsProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p6, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->billCreationServiceProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p7, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p8, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->localTenderCacheProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p9, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->tenderProtoFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/LocalTenderCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderProtoFactory;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/bills/LocalBillTask;",
            ">;"
        }
    .end annotation

    .line 72
    new-instance v10, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static injectAddTendersRequestServerIds(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/bills/LocalBillTask;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;)V"
        }
    .end annotation

    .line 97
    iput-object p1, p0, Lcom/squareup/queue/bills/LocalBillTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method public static injectBillCreationService(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/server/bills/BillCreationService;)V
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/queue/bills/LocalBillTask;->billCreationService:Lcom/squareup/server/bills/BillCreationService;

    return-void
.end method

.method public static injectLastLocalPaymentServerId(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation runtime Lcom/squareup/settings/LastLocalPaymentServerId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/bills/LocalBillTask;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 91
    iput-object p1, p0, Lcom/squareup/queue/bills/LocalBillTask;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method public static injectLocalTenderCache(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/print/LocalTenderCache;)V
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/queue/bills/LocalBillTask;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    return-void
.end method

.method public static injectTenderProtoFactory(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/payment/tender/TenderProtoFactory;)V
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/queue/bills/LocalBillTask;->tenderProtoFactory:Lcom/squareup/payment/tender/TenderProtoFactory;

    return-void
.end method

.method public static injectTransactionLedgerManager(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/queue/bills/LocalBillTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/bills/LocalBillTask;)V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectMainScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectRpcScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/Tickets;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/BillTask_MembersInjector;->injectTickets(Lcom/squareup/queue/bills/BillTask;Lcom/squareup/tickets/Tickets;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->injectLastLocalPaymentServerId(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/settings/LocalSetting;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->addTendersRequestServerIdsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->injectAddTendersRequestServerIds(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/settings/LocalSetting;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->billCreationServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/bills/BillCreationService;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->injectBillCreationService(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/server/bills/BillCreationService;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->injectTransactionLedgerManager(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->localTenderCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/LocalTenderCache;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->injectLocalTenderCache(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/print/LocalTenderCache;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->tenderProtoFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/TenderProtoFactory;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->injectTenderProtoFactory(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/payment/tender/TenderProtoFactory;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/queue/bills/LocalBillTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->injectMembers(Lcom/squareup/queue/bills/LocalBillTask;)V

    return-void
.end method
