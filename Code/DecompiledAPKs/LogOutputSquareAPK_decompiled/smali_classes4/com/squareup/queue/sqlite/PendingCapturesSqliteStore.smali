.class public final Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;
.super Ljava/lang/Object;
.source "PendingCapturesSqliteStore.kt"

# interfaces
.implements Lcom/squareup/queue/sqlite/SqliteQueueStore;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$DatabaseCallback;,
        Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/sqlite/SqliteQueueStore<",
        "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPendingCapturesSqliteStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PendingCapturesSqliteStore.kt\ncom/squareup/queue/sqlite/PendingCapturesSqliteStore\n*L\n1#1,245:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u0000 32\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u000234B3\u0008\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0014\u0010\u000e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020$0\u000fH\u0016J\u000c\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u000fJ\u0008\u0010&\u001a\u00020\'H\u0016J\u000e\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000fH\u0016J\u000e\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u00110)H\u0016J\u000e\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\u00110)H\u0016J\u0010\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020\u0002H\u0002J\u001c\u0010.\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020\u001f0)2\u0006\u0010/\u001a\u00020,H\u0016J\u0014\u0010\u001e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020\u001f0\u000fH\u0016J\u0016\u0010 \u001a\u0008\u0012\u0004\u0012\u0002000)2\u0006\u0010-\u001a\u00020\u0002H\u0016J\u0012\u0010\"\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020\u001f0\u000fJ\u0008\u00101\u001a\u000202H\u0002J\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000fR\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0018\u001a\u00020\u00198\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u001a\u0010\u001b\u001a\u0004\u0008\u001c\u0010\u001dR\u001a\u0010\u001e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020\u001f0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\"\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020\u001f0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;",
        "Lcom/squareup/queue/sqlite/SqliteQueueStore;",
        "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
        "context",
        "Landroid/app/Application;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "userDir",
        "Ljava/io/File;",
        "sqlBrite",
        "Lcom/squareup/sqlbrite3/SqlBrite;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Landroid/app/Application;Lcom/squareup/util/Clock;Ljava/io/File;Lcom/squareup/sqlbrite3/SqlBrite;Lio/reactivex/Scheduler;)V",
        "allEntries",
        "Lio/reactivex/Observable;",
        "count",
        "",
        "db",
        "Lcom/squareup/sqlbrite3/BriteDatabase;",
        "deleteAll",
        "Lcom/squareup/queue/sqlite/PendingCapturesModel$DeleteAllEntries;",
        "deleteFirst",
        "Lcom/squareup/queue/sqlite/PendingCapturesModel$DeleteFirstEntry;",
        "disposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "disposables$annotations",
        "()V",
        "getDisposables",
        "()Lio/reactivex/disposables/CompositeDisposable;",
        "firstEntry",
        "Lcom/squareup/util/Optional;",
        "insert",
        "Lcom/squareup/queue/sqlite/PendingCapturesModel$InsertEntry;",
        "oldestEntry",
        "ripenedCount",
        "",
        "allEntriesAsStream",
        "close",
        "Lio/reactivex/Completable;",
        "deleteAllEntries",
        "Lio/reactivex/Single;",
        "deleteFirstEntry",
        "failedInsertMessage",
        "",
        "entry",
        "fetchEntry",
        "entryId",
        "",
        "preload",
        "",
        "Companion",
        "DatabaseCallback",
        "queue_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$Companion;


# instance fields
.field private final allEntries:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final count:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final db:Lcom/squareup/sqlbrite3/BriteDatabase;

.field private final deleteAll:Lcom/squareup/queue/sqlite/PendingCapturesModel$DeleteAllEntries;

.field private final deleteFirst:Lcom/squareup/queue/sqlite/PendingCapturesModel$DeleteFirstEntry;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final firstEntry:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
            ">;>;"
        }
    .end annotation
.end field

.field private final insert:Lcom/squareup/queue/sqlite/PendingCapturesModel$InsertEntry;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final oldestEntry:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
            ">;>;"
        }
    .end annotation
.end field

.field private final ripenedCount:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->Companion:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/squareup/util/Clock;Ljava/io/File;Lcom/squareup/sqlbrite3/SqlBrite;Lio/reactivex/Scheduler;)V
    .locals 7
    .param p3    # Ljava/io/File;
        .annotation runtime Lcom/squareup/user/UserDirectory;
        .end annotation
    .end param
    .param p5    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/LegacyMainScheduler;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userDir"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sqlBrite"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p5, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->mainScheduler:Lio/reactivex/Scheduler;

    .line 62
    new-instance p5, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p5}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p5, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 65
    check-cast p1, Landroid/content/Context;

    invoke-static {p1}, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;->builder(Landroid/content/Context;)Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;

    move-result-object p1

    .line 66
    new-instance p5, Ljava/io/File;

    const-string v0, "pending-captures.db"

    invoke-direct {p5, p3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;->name(Ljava/lang/String;)Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;

    move-result-object p1

    .line 67
    new-instance p3, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$DatabaseCallback;

    invoke-direct {p3}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$DatabaseCallback;-><init>()V

    check-cast p3, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;

    invoke-virtual {p1, p3}, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;->callback(Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;)Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;

    move-result-object p1

    .line 68
    invoke-virtual {p1}, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;->build()Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;

    move-result-object p1

    .line 70
    new-instance p3, Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelperFactory;

    invoke-direct {p3}, Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelperFactory;-><init>()V

    invoke-virtual {p3, p1}, Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelperFactory;->create(Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;)Landroidx/sqlite/db/SupportSQLiteOpenHelper;

    move-result-object p1

    .line 71
    iget-object p3, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p4, p1, p3}, Lcom/squareup/sqlbrite3/SqlBrite;->wrapDatabaseHelper(Landroidx/sqlite/db/SupportSQLiteOpenHelper;Lio/reactivex/Scheduler;)Lcom/squareup/sqlbrite3/BriteDatabase;

    move-result-object p1

    const-string p3, "sqlBrite.wrapDatabaseHelper(helper, mainScheduler)"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 73
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 75
    sget-object p1, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->Companion:Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;->getFACTORY()Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;->count()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    move-result-object v1

    const-string p1, "FACTORY.count()"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 76
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 77
    iget-object v3, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->mainScheduler:Lio/reactivex/Scheduler;

    .line 78
    iget-object v4, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 80
    sget-object p1, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$1;->INSTANCE:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$1;

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const-string v5, "Unable to fetch count"

    .line 74
    invoke-static/range {v0 .. v6}, Lcom/squareup/queue/sqlite/QueueStoresKt;->queryResults(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Ljava/lang/Object;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p1

    .line 81
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    const-string p3, "db\n        .queryResults\u2026  .distinctUntilChanged()"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->count:Lio/reactivex/Observable;

    .line 87
    iget-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->count:Lio/reactivex/Observable;

    .line 89
    iget-object p3, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->mainScheduler:Lio/reactivex/Scheduler;

    .line 90
    iget-object p4, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 92
    new-instance p5, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$2;

    invoke-direct {p5, p0, p2}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$2;-><init>(Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;Lcom/squareup/util/Clock;)V

    check-cast p5, Lkotlin/jvm/functions/Function1;

    const-string p2, "Unable to fetch ripened count"

    .line 88
    invoke-static {p1, p3, p4, p2, p5}, Lcom/squareup/queue/sqlite/QueueStoresKt;->ripenedCount(Lio/reactivex/Observable;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->ripenedCount:Lio/reactivex/Observable;

    .line 103
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 105
    sget-object p1, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->Companion:Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;->getFACTORY()Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;->firstEntry()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    move-result-object v1

    const-string p1, "FACTORY.firstEntry()"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v2, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->mainScheduler:Lio/reactivex/Scheduler;

    .line 107
    iget-object v3, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 109
    sget-object p1, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$3;->INSTANCE:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$3;

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const-string v4, "Unable to fetch first entry"

    .line 104
    invoke-static/range {v0 .. v5}, Lcom/squareup/queue/sqlite/QueueStoresKt;->queryOptionalResults(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->firstEntry:Lio/reactivex/Observable;

    .line 111
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 113
    sget-object p1, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->Companion:Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;->getFACTORY()Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;->oldestEntry()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    move-result-object v1

    const-string p1, "FACTORY.oldestEntry()"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iget-object v2, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->mainScheduler:Lio/reactivex/Scheduler;

    .line 115
    iget-object v3, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 117
    sget-object p1, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$4;->INSTANCE:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$4;

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const-string v4, "Unable to fetch oldest entry"

    .line 112
    invoke-static/range {v0 .. v5}, Lcom/squareup/queue/sqlite/QueueStoresKt;->queryOptionalResults(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->oldestEntry:Lio/reactivex/Observable;

    .line 119
    iget-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 120
    sget-object p2, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->Companion:Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;

    invoke-virtual {p2}, Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;->getFACTORY()Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;->allEntries()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    move-result-object p2

    const-string p3, "FACTORY.allEntries()"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    sget-object p3, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->Companion:Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;

    invoke-virtual {p3}, Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;->getALL_ENTRIES_MAPPER()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object p3

    .line 122
    iget-object p4, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->mainScheduler:Lio/reactivex/Scheduler;

    const-string p5, "Unable to fetch all entries"

    .line 119
    invoke-static {p1, p2, p3, p4, p5}, Lcom/squareup/queue/sqlite/QueueStoresKt;->allEntriesAsStream(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Lcom/squareup/sqldelight/prerelease/RowMapper;Lio/reactivex/Scheduler;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->allEntries:Lio/reactivex/Observable;

    .line 126
    new-instance p1, Lcom/squareup/queue/sqlite/PendingCapturesModel$InsertEntry;

    iget-object p2, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-virtual {p2}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/queue/sqlite/PendingCapturesModel$InsertEntry;-><init>(Landroidx/sqlite/db/SupportSQLiteDatabase;)V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->insert:Lcom/squareup/queue/sqlite/PendingCapturesModel$InsertEntry;

    .line 127
    new-instance p1, Lcom/squareup/queue/sqlite/PendingCapturesModel$DeleteFirstEntry;

    iget-object p2, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-virtual {p2}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/queue/sqlite/PendingCapturesModel$DeleteFirstEntry;-><init>(Landroidx/sqlite/db/SupportSQLiteDatabase;)V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->deleteFirst:Lcom/squareup/queue/sqlite/PendingCapturesModel$DeleteFirstEntry;

    .line 128
    new-instance p1, Lcom/squareup/queue/sqlite/PendingCapturesModel$DeleteAllEntries;

    iget-object p2, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-virtual {p2}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/queue/sqlite/PendingCapturesModel$DeleteAllEntries;-><init>(Landroidx/sqlite/db/SupportSQLiteDatabase;)V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->deleteAll:Lcom/squareup/queue/sqlite/PendingCapturesModel$DeleteAllEntries;

    return-void
.end method

.method public static final synthetic access$failedInsertMessage(Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;Lcom/squareup/queue/sqlite/PendingCapturesEntry;)Ljava/lang/String;
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->failedInsertMessage(Lcom/squareup/queue/sqlite/PendingCapturesEntry;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getDb$p(Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;)Lcom/squareup/sqlbrite3/BriteDatabase;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    return-object p0
.end method

.method public static final synthetic access$getInsert$p(Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;)Lcom/squareup/queue/sqlite/PendingCapturesModel$InsertEntry;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->insert:Lcom/squareup/queue/sqlite/PendingCapturesModel$InsertEntry;

    return-object p0
.end method

.method public static final synthetic access$preload(Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;)V
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->preload()V

    return-void
.end method

.method public static final create(Landroid/app/Application;Lcom/squareup/util/Clock;Ljava/io/File;Lio/reactivex/Scheduler;)Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;
    .locals 1
    .param p2    # Ljava/io/File;
        .annotation runtime Lcom/squareup/user/UserDirectory;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/LegacyMainScheduler;
        .end annotation
    .end param
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->Companion:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$Companion;->create(Landroid/app/Application;Lcom/squareup/util/Clock;Ljava/io/File;Lio/reactivex/Scheduler;)Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic disposables$annotations()V
    .locals 0

    return-void
.end method

.method private final failedInsertMessage(Lcom/squareup/queue/sqlite/PendingCapturesEntry;)Ljava/lang/String;
    .locals 4

    .line 215
    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->entry_id()Ljava/lang/String;

    move-result-object v0

    .line 216
    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "none"

    .line 217
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to insert entry: id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", timestamp "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->timestamp_ms()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final preload()V
    .locals 2

    .line 207
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->count:Lio/reactivex/Observable;

    invoke-virtual {v1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 208
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->ripenedCount:Lio/reactivex/Observable;

    invoke-virtual {v1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 209
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->firstEntry:Lio/reactivex/Observable;

    invoke-virtual {v1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 210
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->oldestEntry:Lio/reactivex/Observable;

    invoke-virtual {v1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method


# virtual methods
.method public allEntries()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
            ">;>;"
        }
    .end annotation

    .line 172
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->count:Lio/reactivex/Observable;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->allEntries:Lio/reactivex/Observable;

    invoke-static {v0, v1}, Lcom/squareup/queue/sqlite/QueueStoresKt;->allEntriesAsList(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final allEntriesAsStream()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
            ">;"
        }
    .end annotation

    .line 169
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->allEntries:Lio/reactivex/Observable;

    return-object v0
.end method

.method public close()Lio/reactivex/Completable;
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 197
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 198
    invoke-static {v0}, Lcom/squareup/queue/sqlite/QueueStoresKt;->closeReactive(Lcom/squareup/sqlbrite3/BriteDatabase;)Lio/reactivex/Completable;

    move-result-object v0

    .line 199
    iget-object v1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Completable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v0

    const-string v1, "db\n        .closeReactiv\u2026ubscribeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public count()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 131
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->count:Lio/reactivex/Observable;

    return-object v0
.end method

.method public deleteAllEntries()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 191
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->deleteAll:Lcom/squareup/queue/sqlite/PendingCapturesModel$DeleteAllEntries;

    check-cast v1, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;

    invoke-static {v0, v1}, Lcom/squareup/queue/sqlite/QueueStoresKt;->delete(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;)Lio/reactivex/Single;

    move-result-object v0

    .line 192
    iget-object v1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "db.delete(deleteAll)\n   \u2026ubscribeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public deleteFirstEntry()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 187
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->deleteFirst:Lcom/squareup/queue/sqlite/PendingCapturesModel$DeleteFirstEntry;

    check-cast v1, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;

    invoke-static {v0, v1}, Lcom/squareup/queue/sqlite/QueueStoresKt;->delete(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;)Lio/reactivex/Single;

    move-result-object v0

    .line 188
    iget-object v1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "db.delete(deleteFirst)\n \u2026ubscribeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public fetchEntry(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
            ">;>;"
        }
    .end annotation

    const-string v0, "entryId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    iget-object v1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 143
    sget-object v0, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->Companion:Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;->getFACTORY()Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;->getEntry(Ljava/lang/String;)Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    move-result-object v2

    const-string v0, "FACTORY.getEntry(entryId)"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    iget-object v3, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->mainScheduler:Lio/reactivex/Scheduler;

    .line 145
    iget-object v4, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to fetch entry with entryId: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 147
    sget-object p1, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$fetchEntry$fetchEntryResult$1;->INSTANCE:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$fetchEntry$fetchEntryResult$1;

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 142
    invoke-static/range {v1 .. v6}, Lcom/squareup/queue/sqlite/QueueStoresKt;->queryOptionalResults(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p1

    const-wide/16 v0, 0x1

    .line 149
    invoke-virtual {p1, v0, v1}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object p1

    check-cast p1, Lio/reactivex/ObservableSource;

    invoke-static {p1}, Lio/reactivex/Single;->fromObservable(Lio/reactivex/ObservableSource;)Lio/reactivex/Single;

    move-result-object p1

    .line 150
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.fromObservable(fe\u2026ubscribeOn(mainScheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public firstEntry()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
            ">;>;"
        }
    .end annotation

    .line 139
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->firstEntry:Lio/reactivex/Observable;

    return-object v0
.end method

.method public final getDisposables()Lio/reactivex/disposables/CompositeDisposable;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    return-object v0
.end method

.method public insert(Lcom/squareup/queue/sqlite/PendingCapturesEntry;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "entry"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    new-instance v0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$insert$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$insert$1;-><init>(Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;Lcom/squareup/queue/sqlite/PendingCapturesEntry;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object p1

    .line 184
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single\n        .fromCall\u2026ubscribeOn(mainScheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic insert(Ljava/lang/Object;)Lio/reactivex/Single;
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/queue/sqlite/PendingCapturesEntry;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->insert(Lcom/squareup/queue/sqlite/PendingCapturesEntry;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public final oldestEntry()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
            ">;>;"
        }
    .end annotation

    .line 157
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->oldestEntry:Lio/reactivex/Observable;

    return-object v0
.end method

.method public final ripenedCount()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 137
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->ripenedCount:Lio/reactivex/Observable;

    return-object v0
.end method
