.class public final Lcom/squareup/queue/sqlite/LocalPaymentModel$InsertEntry;
.super Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;
.source "LocalPaymentModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/LocalPaymentModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InsertEntry"
.end annotation


# direct methods
.method public constructor <init>(Landroidx/sqlite/db/SupportSQLiteDatabase;)V
    .locals 1

    const-string v0, "INSERT INTO local_payments_tasks (entry_id, timestamp_ms, data)\nVALUES (?, ?, ?)"

    .line 202
    invoke-interface {p1, v0}, Landroidx/sqlite/db/SupportSQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroidx/sqlite/db/SupportSQLiteStatement;

    move-result-object p1

    const-string v0, "local_payments_tasks"

    invoke-direct {p0, v0, p1}, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;-><init>(Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteStatement;)V

    return-void
.end method


# virtual methods
.method public bind(Ljava/lang/String;J[B)V
    .locals 1

    const/4 v0, 0x1

    .line 208
    invoke-virtual {p0, v0, p1}, Lcom/squareup/queue/sqlite/LocalPaymentModel$InsertEntry;->bindString(ILjava/lang/String;)V

    const/4 p1, 0x2

    .line 209
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/queue/sqlite/LocalPaymentModel$InsertEntry;->bindLong(IJ)V

    const/4 p1, 0x3

    .line 210
    invoke-virtual {p0, p1, p4}, Lcom/squareup/queue/sqlite/LocalPaymentModel$InsertEntry;->bindBlob(I[B)V

    return-void
.end method
