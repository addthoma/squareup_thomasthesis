.class public interface abstract Lcom/squareup/queue/sqlite/LocalPaymentModel$Creator;
.super Ljava/lang/Object;
.source "LocalPaymentModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/LocalPaymentModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Creator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/queue/sqlite/LocalPaymentModel;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract create(Ljava/lang/Long;Ljava/lang/String;J[B)Lcom/squareup/queue/sqlite/LocalPaymentModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "J[B)TT;"
        }
    .end annotation
.end method
