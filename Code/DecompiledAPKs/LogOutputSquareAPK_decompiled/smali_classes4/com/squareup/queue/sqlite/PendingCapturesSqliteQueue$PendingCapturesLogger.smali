.class public Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;
.super Ljava/lang/Object;
.source "PendingCapturesSqliteQueue.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PendingCapturesLogger"
.end annotation


# static fields
.field private static final CLOGGED_COUNT:I = 0x3


# instance fields
.field private final lastQueueServiceStart:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingCapturesMonitor:Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

.field private final queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;


# direct methods
.method constructor <init>(Lcom/squareup/queue/sqlite/PendingCapturesMonitor;Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/PendingCapturesMonitor;",
            "Lcom/squareup/queue/QueueServiceStarter;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221
    iput-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;->pendingCapturesMonitor:Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

    .line 222
    iput-object p2, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    .line 223
    iput-object p3, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;->lastQueueServiceStart:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method static synthetic lambda$onEnterScope$0(Ljava/lang/Integer;)Ljava/lang/Boolean;
    .locals 1

    .line 229
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    const/4 v0, 0x3

    if-lt p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic lambda$qmKLlj7JbYKlZkWhC7_2I3H6vvQ(Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;->logClogged(I)V

    return-void
.end method

.method private logClogged(I)V
    .locals 5

    .line 237
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    invoke-interface {v0}, Lcom/squareup/queue/QueueServiceStarter;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;->lastQueueServiceStart:Lcom/squareup/settings/LocalSetting;

    const-wide/16 v1, -0x1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-eqz v0, :cond_1

    .line 244
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-static {v0}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, "NEVER"

    .line 246
    :goto_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Clogged capture queue, size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", last queue start="

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 227
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;->pendingCapturesMonitor:Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

    .line 228
    invoke-interface {v0}, Lcom/squareup/queue/sqlite/PendingCapturesMonitor;->pendingCapturesCount()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/queue/sqlite/-$$Lambda$PendingCapturesSqliteQueue$PendingCapturesLogger$OALcwbRk0E54irroGLOjjFna-gY;->INSTANCE:Lcom/squareup/queue/sqlite/-$$Lambda$PendingCapturesSqliteQueue$PendingCapturesLogger$OALcwbRk0E54irroGLOjjFna-gY;

    .line 229
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/queue/sqlite/-$$Lambda$PendingCapturesSqliteQueue$PendingCapturesLogger$qmKLlj7JbYKlZkWhC7_2I3H6vvQ;

    invoke-direct {v1, p0}, Lcom/squareup/queue/sqlite/-$$Lambda$PendingCapturesSqliteQueue$PendingCapturesLogger$qmKLlj7JbYKlZkWhC7_2I3H6vvQ;-><init>(Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;)V

    .line 230
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 227
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
