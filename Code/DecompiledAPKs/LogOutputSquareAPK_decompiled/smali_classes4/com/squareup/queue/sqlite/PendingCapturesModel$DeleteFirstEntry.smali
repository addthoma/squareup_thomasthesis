.class public final Lcom/squareup/queue/sqlite/PendingCapturesModel$DeleteFirstEntry;
.super Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;
.source "PendingCapturesModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/PendingCapturesModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeleteFirstEntry"
.end annotation


# direct methods
.method public constructor <init>(Landroidx/sqlite/db/SupportSQLiteDatabase;)V
    .locals 1

    const-string v0, "DELETE FROM pending_captures\nWHERE _id = ( SELECT MIN(_id) FROM pending_captures )"

    .line 230
    invoke-interface {p1, v0}, Landroidx/sqlite/db/SupportSQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroidx/sqlite/db/SupportSQLiteStatement;

    move-result-object p1

    const-string v0, "pending_captures"

    invoke-direct {p0, v0, p1}, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;-><init>(Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteStatement;)V

    return-void
.end method
