.class final Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;
.super Ljava/lang/Object;
.source "SqliteQueues.java"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/SqliteQueues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ToBillHistory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S::",
        "Lcom/squareup/queue/sqlite/QueueStoreEntry;",
        "Q:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TS;",
        "Lcom/squareup/billhistory/model/BillHistory;",
        ">;"
    }
.end annotation


# instance fields
.field private final converter:Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter<",
            "TS;TQ;>;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter<",
            "TS;TQ;>;)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;->res:Lcom/squareup/util/Res;

    .line 50
    iput-object p2, p0, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;->converter:Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/queue/sqlite/QueueStoreEntry;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)",
            "Lcom/squareup/billhistory/model/BillHistory;"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;->converter:Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;

    invoke-interface {v0, p1}, Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;->toQueueEntry(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/queue/PendingPayment;

    .line 55
    iget-object v0, p0, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;->res:Lcom/squareup/util/Res;

    invoke-interface {p1, v0}, Lcom/squareup/queue/PendingPayment;->asBill(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/queue/sqlite/QueueStoreEntry;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;->call(Lcom/squareup/queue/sqlite/QueueStoreEntry;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    return-object p1
.end method
