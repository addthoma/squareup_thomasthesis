.class public Lcom/squareup/queue/RetrofitTaskInjector;
.super Ljava/lang/Object;
.source "RetrofitTaskInjector.java"

# interfaces
.implements Lcom/squareup/tape/TaskInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/tape/TaskInjector<",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final component:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/squareup/queue/RetrofitTaskInjector;->component:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/retrofit/RetrofitTask;)V
    .locals 4

    .line 17
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/RetrofitTaskInjector;->component:Ljava/lang/Object;

    invoke-interface {p1, v0}, Lcom/squareup/queue/retrofit/RetrofitTask;->inject(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 19
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Task "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " expects the concrete component "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/queue/RetrofitTaskInjector;->component:Ljava/lang/Object;

    .line 22
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " to implement an interface. Open the "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " class, go to the inject() method, look at the type of the parameter passed in, and update the Dagger component interface to extend that type. "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public bridge synthetic injectMembers(Lcom/squareup/tape/Task;)V
    .locals 0

    .line 6
    check-cast p1, Lcom/squareup/queue/retrofit/RetrofitTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/RetrofitTaskInjector;->injectMembers(Lcom/squareup/queue/retrofit/RetrofitTask;)V

    return-void
.end method
