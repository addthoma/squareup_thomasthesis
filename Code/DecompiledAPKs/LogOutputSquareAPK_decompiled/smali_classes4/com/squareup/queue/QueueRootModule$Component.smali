.class public interface abstract Lcom/squareup/queue/QueueRootModule$Component;
.super Ljava/lang/Object;
.source "QueueRootModule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/QueueRootModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/payment/offline/StoreAndForwardTask;)V
.end method

.method public abstract inject(Lcom/squareup/queue/QueueService$BootReceiver;)V
.end method

.method public abstract inject(Lcom/squareup/queue/QueueService;)V
.end method
