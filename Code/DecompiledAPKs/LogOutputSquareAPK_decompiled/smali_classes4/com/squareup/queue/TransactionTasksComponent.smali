.class public interface abstract Lcom/squareup/queue/TransactionTasksComponent;
.super Ljava/lang/Object;
.source "TransactionTasksComponent.java"


# virtual methods
.method public abstract inject(Lcom/squareup/queue/Cancel;)V
.end method

.method public abstract inject(Lcom/squareup/queue/Capture;)V
.end method

.method public abstract inject(Lcom/squareup/queue/Cash;)V
.end method

.method public abstract inject(Lcom/squareup/queue/DeclineReceipt;)V
.end method

.method public abstract inject(Lcom/squareup/queue/EmailReceipt;)V
.end method

.method public abstract inject(Lcom/squareup/queue/EmailReceiptById;)V
.end method

.method public abstract inject(Lcom/squareup/queue/EnqueueDeclineReceipt;)V
.end method

.method public abstract inject(Lcom/squareup/queue/EnqueueEmailReceipt;)V
.end method

.method public abstract inject(Lcom/squareup/queue/EnqueueEmailReceiptById;)V
.end method

.method public abstract inject(Lcom/squareup/queue/EnqueueSkipReceipt;)V
.end method

.method public abstract inject(Lcom/squareup/queue/EnqueueSmsReceipt;)V
.end method

.method public abstract inject(Lcom/squareup/queue/EnqueueSmsReceiptById;)V
.end method

.method public abstract inject(Lcom/squareup/queue/Itemize;)V
.end method

.method public abstract inject(Lcom/squareup/queue/OtherTenderTask;)V
.end method

.method public abstract inject(Lcom/squareup/queue/Sign;)V
.end method

.method public abstract inject(Lcom/squareup/queue/SkipReceipt;)V
.end method

.method public abstract inject(Lcom/squareup/queue/SmsReceipt;)V
.end method

.method public abstract inject(Lcom/squareup/queue/SmsReceiptById;)V
.end method

.method public abstract inject(Lcom/squareup/queue/UploadFailure$ForCapture;)V
.end method

.method public abstract inject(Lcom/squareup/queue/UploadFailure$ForSettle;)V
.end method

.method public abstract inject(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;)V
.end method

.method public abstract inject(Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;)V
.end method

.method public abstract inject(Lcom/squareup/queue/bills/CancelBill;)V
.end method

.method public abstract inject(Lcom/squareup/queue/bills/CaptureTendersTask;)V
.end method

.method public abstract inject(Lcom/squareup/queue/bills/CashBillTask;)V
.end method

.method public abstract inject(Lcom/squareup/queue/bills/CompleteBill;)V
.end method

.method public abstract inject(Lcom/squareup/queue/bills/NoSaleBillTask;)V
.end method

.method public abstract inject(Lcom/squareup/queue/bills/OtherTenderBillTask;)V
.end method

.method public abstract inject(Lcom/squareup/queue/bills/RemoveTendersTask;)V
.end method

.method public abstract inject(Lcom/squareup/queue/crm/AttachContactPostPaymentTask;)V
.end method

.method public abstract inject(Lcom/squareup/queue/crm/AttachContactTask;)V
.end method

.method public abstract inject(Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;)V
.end method

.method public abstract queueServiceLoggedInDependencies()Lcom/squareup/queue/QueueService$LoggedInDependencies;
.end method
