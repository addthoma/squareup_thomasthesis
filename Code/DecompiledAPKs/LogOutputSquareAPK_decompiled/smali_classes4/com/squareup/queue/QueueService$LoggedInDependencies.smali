.class public Lcom/squareup/queue/QueueService$LoggedInDependencies;
.super Ljava/lang/Object;
.source "QueueService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/QueueService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LoggedInDependencies"
.end annotation


# instance fields
.field final captureQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field final localPaymentsQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field final loggedInTaskWatcher:Lcom/squareup/queue/TaskWatcher;

.field final taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;


# direct methods
.method constructor <init>(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/TaskWatcher;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    iput-object p1, p0, Lcom/squareup/queue/QueueService$LoggedInDependencies;->captureQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 203
    iput-object p2, p0, Lcom/squareup/queue/QueueService$LoggedInDependencies;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 204
    iput-object p3, p0, Lcom/squareup/queue/QueueService$LoggedInDependencies;->localPaymentsQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 205
    iput-object p4, p0, Lcom/squareup/queue/QueueService$LoggedInDependencies;->loggedInTaskWatcher:Lcom/squareup/queue/TaskWatcher;

    return-void
.end method
