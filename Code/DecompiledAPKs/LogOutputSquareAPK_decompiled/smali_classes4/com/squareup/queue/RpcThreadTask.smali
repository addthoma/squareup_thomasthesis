.class public abstract Lcom/squareup/queue/RpcThreadTask;
.super Ljava/lang/Object;
.source "RpcThreadTask.java"

# interfaces
.implements Lcom/squareup/queue/retrofit/RetrofitTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/retrofit/RetrofitTask<",
        "TU;>;"
    }
.end annotation


# instance fields
.field transient mainScheduler:Lrx/Scheduler;
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient rpcScheduler:Lrx/Scheduler;
    .annotation runtime Lcom/squareup/thread/Rpc;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract callOnRpcThread()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public final execute(Lcom/squareup/server/SquareCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/queue/-$$Lambda$ideOsI1pVJuUHgSi-19K6aIdGd0;

    invoke-direct {v0, p0}, Lcom/squareup/queue/-$$Lambda$ideOsI1pVJuUHgSi-19K6aIdGd0;-><init>(Lcom/squareup/queue/RpcThreadTask;)V

    invoke-static {v0}, Lrx/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/RpcThreadTask;->rpcScheduler:Lrx/Scheduler;

    .line 29
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/RpcThreadTask;->mainScheduler:Lrx/Scheduler;

    .line 30
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/queue/RpcThreadTask$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/queue/RpcThreadTask$1;-><init>(Lcom/squareup/queue/RpcThreadTask;Lcom/squareup/server/SquareCallback;)V

    .line 31
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/Observer;)Lrx/Subscription;

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/RpcThreadTask;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method protected abstract handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/squareup/server/SimpleResponse;"
        }
    .end annotation
.end method
