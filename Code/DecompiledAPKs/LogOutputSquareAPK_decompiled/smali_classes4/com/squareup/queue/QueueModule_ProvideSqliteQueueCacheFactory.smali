.class public final Lcom/squareup/queue/QueueModule_ProvideSqliteQueueCacheFactory;
.super Ljava/lang/Object;
.source "QueueModule_ProvideSqliteQueueCacheFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/retrofit/QueueCache<",
        "Lcom/squareup/queue/retrofit/RetrofitQueue;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/queue/QueueModule;

.field private final sqliteQueueFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/queue/QueueModule;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/QueueModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/queue/QueueModule_ProvideSqliteQueueCacheFactory;->module:Lcom/squareup/queue/QueueModule;

    .line 27
    iput-object p2, p0, Lcom/squareup/queue/QueueModule_ProvideSqliteQueueCacheFactory;->sqliteQueueFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/queue/QueueModule;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueModule_ProvideSqliteQueueCacheFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/QueueModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;",
            ">;)",
            "Lcom/squareup/queue/QueueModule_ProvideSqliteQueueCacheFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/queue/QueueModule_ProvideSqliteQueueCacheFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/QueueModule_ProvideSqliteQueueCacheFactory;-><init>(Lcom/squareup/queue/QueueModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideSqliteQueueCache(Lcom/squareup/queue/QueueModule;Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;)Lcom/squareup/queue/retrofit/QueueCache;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/QueueModule;",
            "Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;",
            ")",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation

    .line 42
    invoke-virtual {p0, p1}, Lcom/squareup/queue/QueueModule;->provideSqliteQueueCache(Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;)Lcom/squareup/queue/retrofit/QueueCache;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/retrofit/QueueCache;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/retrofit/QueueCache;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideSqliteQueueCacheFactory;->module:Lcom/squareup/queue/QueueModule;

    iget-object v1, p0, Lcom/squareup/queue/QueueModule_ProvideSqliteQueueCacheFactory;->sqliteQueueFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;

    invoke-static {v0, v1}, Lcom/squareup/queue/QueueModule_ProvideSqliteQueueCacheFactory;->provideSqliteQueueCache(Lcom/squareup/queue/QueueModule;Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;)Lcom/squareup/queue/retrofit/QueueCache;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/queue/QueueModule_ProvideSqliteQueueCacheFactory;->get()Lcom/squareup/queue/retrofit/QueueCache;

    move-result-object v0

    return-object v0
.end method
