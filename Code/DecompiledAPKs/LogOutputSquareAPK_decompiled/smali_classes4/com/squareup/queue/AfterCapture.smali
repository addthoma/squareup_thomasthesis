.class public Lcom/squareup/queue/AfterCapture;
.super Ljava/lang/Object;
.source "AfterCapture.java"


# instance fields
.field private final lastCapturePaymentId:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;


# direct methods
.method constructor <init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/queue/retrofit/RetrofitQueue;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/queue/AfterCapture;->lastCapturePaymentId:Lcom/squareup/settings/LocalSetting;

    .line 16
    iput-object p2, p0, Lcom/squareup/queue/AfterCapture;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-void
.end method


# virtual methods
.method public enqueueForCapturedPayment(Ljava/lang/String;Lcom/squareup/queue/FutureReceipt;Ljava/lang/String;)V
    .locals 1

    if-eqz p3, :cond_0

    .line 26
    iget-object v0, p0, Lcom/squareup/queue/AfterCapture;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-static {p1, p3}, Lcom/squareup/queue/Sign;->payment(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/queue/Sign;

    move-result-object p3

    invoke-interface {v0, p3}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    :cond_0
    if-nez p2, :cond_1

    .line 33
    iget-object p2, p0, Lcom/squareup/queue/AfterCapture;->lastCapturePaymentId:Lcom/squareup/settings/LocalSetting;

    invoke-interface {p2, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 37
    :cond_1
    iget-object p3, p0, Lcom/squareup/queue/AfterCapture;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-virtual {p2, p3, p1}, Lcom/squareup/queue/FutureReceipt;->enqueue(Lcom/squareup/queue/retrofit/RetrofitQueue;Ljava/lang/String;)V

    :goto_0
    return-void
.end method
