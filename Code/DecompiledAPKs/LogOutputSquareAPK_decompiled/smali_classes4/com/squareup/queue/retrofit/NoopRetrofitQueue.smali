.class public final Lcom/squareup/queue/retrofit/NoopRetrofitQueue;
.super Ljava/lang/Object;
.source "NoopRetrofitQueue.kt"

# interfaces
.implements Lcom/squareup/queue/retrofit/RetrofitQueue;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0001\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0006H\u0016J\u0014\u0010\u0007\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00060\u0008H\u0016J\u0008\u0010\n\u001a\u00020\u0004H\u0016J\u0010\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\n\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J\u0008\u0010\u000f\u001a\u00020\u0004H\u0016J\u001e\u0010\u0010\u001a\u00020\u00042\u0014\u0010\u0011\u001a\u0010\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u0006\u0018\u00010\u0012H\u0016J\u0008\u0010\u0013\u001a\u00020\u0014H\u0016\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/queue/retrofit/NoopRetrofitQueue;",
        "Lcom/squareup/queue/retrofit/RetrofitQueue;",
        "()V",
        "add",
        "",
        "entry",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        "asList",
        "",
        "",
        "close",
        "delayClose",
        "",
        "peek",
        "",
        "remove",
        "setListener",
        "listener",
        "Lcom/squareup/tape/ObjectQueue$Listener;",
        "size",
        "",
        "queue_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/queue/retrofit/NoopRetrofitQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5
    new-instance v0, Lcom/squareup/queue/retrofit/NoopRetrofitQueue;

    invoke-direct {v0}, Lcom/squareup/queue/retrofit/NoopRetrofitQueue;-><init>()V

    sput-object v0, Lcom/squareup/queue/retrofit/NoopRetrofitQueue;->INSTANCE:Lcom/squareup/queue/retrofit/NoopRetrofitQueue;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Lcom/squareup/queue/retrofit/RetrofitTask;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/retrofit/RetrofitTask<",
            "*>;)V"
        }
    .end annotation

    return-void
.end method

.method public bridge synthetic add(Ljava/lang/Object;)V
    .locals 0

    .line 5
    check-cast p1, Lcom/squareup/queue/retrofit/RetrofitTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/retrofit/NoopRetrofitQueue;->add(Lcom/squareup/queue/retrofit/RetrofitTask;)V

    return-void
.end method

.method public asList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/queue/retrofit/RetrofitTask<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .line 16
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 0

    return-void
.end method

.method public delayClose(Z)V
    .locals 0

    return-void
.end method

.method public bridge synthetic peek()Ljava/lang/Object;
    .locals 1

    .line 5
    invoke-virtual {p0}, Lcom/squareup/queue/retrofit/NoopRetrofitQueue;->peek()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public peek()Ljava/lang/Void;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public remove()V
    .locals 0

    return-void
.end method

.method public setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue$Listener<",
            "Lcom/squareup/queue/retrofit/RetrofitTask<",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    return-void
.end method

.method public size()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
