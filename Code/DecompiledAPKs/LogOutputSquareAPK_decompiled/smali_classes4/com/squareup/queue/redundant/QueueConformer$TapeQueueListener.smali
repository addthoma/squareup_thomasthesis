.class public Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;
.super Ljava/lang/Object;
.source "QueueConformer.java"

# interfaces
.implements Lcom/squareup/tape/ObjectQueue$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/redundant/QueueConformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TapeQueueListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/tape/ObjectQueue$Listener<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private volatile enteringScope:Z

.field private initialQueueSize:I

.field private initialized:Z

.field private initializedEntryCount:I

.field private final queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;


# direct methods
.method public constructor <init>(Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 0

    .line 304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 305
    iput-object p1, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    .line 306
    iput-object p2, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method


# virtual methods
.method public onAdd(Lcom/squareup/tape/ObjectQueue;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue<",
            "TT;>;TT;)V"
        }
    .end annotation

    .line 343
    iget-object p1, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 344
    iget-boolean p1, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->initialized:Z

    if-nez p1, :cond_1

    .line 345
    iget p1, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->initializedEntryCount:I

    const/4 p2, 0x1

    add-int/2addr p1, p2

    iput p1, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->initializedEntryCount:I

    iget v0, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->initialQueueSize:I

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    iput-boolean p2, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->initialized:Z

    .line 349
    :cond_1
    iget-boolean p1, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->initialized:Z

    if-eqz p1, :cond_3

    .line 350
    iget-boolean p1, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->enteringScope:Z

    if-eqz p1, :cond_2

    .line 351
    iget-object p1, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    const-string p2, "Task added to queue, entering scope"

    invoke-interface {p1, p2}, Lcom/squareup/queue/QueueServiceStarter;->startWaitingForForeground(Ljava/lang/String;)V

    goto :goto_1

    .line 353
    :cond_2
    iget-object p1, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    const-string p2, "Task added to queue"

    invoke-interface {p1, p2}, Lcom/squareup/queue/QueueServiceStarter;->start(Ljava/lang/String;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public onRemove(Lcom/squareup/tape/ObjectQueue;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue<",
            "TT;>;)V"
        }
    .end annotation

    .line 364
    iget-object p1, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    return-void
.end method

.method setEnteringScope(Z)V
    .locals 0

    .line 310
    iput-boolean p1, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->enteringScope:Z

    return-void
.end method

.method startListeningTo(Lcom/squareup/tape/ObjectQueue;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue<",
            "TT;>;)V"
        }
    .end annotation

    .line 322
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 323
    invoke-interface {p1}, Lcom/squareup/tape/ObjectQueue;->size()I

    move-result v0

    iput v0, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->initialQueueSize:I

    const/4 v0, 0x0

    .line 324
    iput v0, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->initializedEntryCount:I

    .line 325
    iget v1, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->initialQueueSize:I

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->initialized:Z

    .line 326
    invoke-interface {p1, p0}, Lcom/squareup/tape/ObjectQueue;->setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V

    return-void
.end method

.method stopListeningTo(Lcom/squareup/tape/ObjectQueue;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue<",
            "TT;>;)V"
        }
    .end annotation

    .line 337
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    const/4 v0, 0x0

    .line 338
    invoke-interface {p1, v0}, Lcom/squareup/tape/ObjectQueue;->setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V

    return-void
.end method
