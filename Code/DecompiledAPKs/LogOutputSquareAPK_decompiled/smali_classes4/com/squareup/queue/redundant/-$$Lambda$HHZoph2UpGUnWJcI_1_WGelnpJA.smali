.class public final synthetic Lcom/squareup/queue/redundant/-$$Lambda$HHZoph2UpGUnWJcI_1_WGelnpJA;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/queue/redundant/-$$Lambda$HHZoph2UpGUnWJcI_1_WGelnpJA;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/queue/redundant/-$$Lambda$HHZoph2UpGUnWJcI_1_WGelnpJA;

    invoke-direct {v0}, Lcom/squareup/queue/redundant/-$$Lambda$HHZoph2UpGUnWJcI_1_WGelnpJA;-><init>()V

    sput-object v0, Lcom/squareup/queue/redundant/-$$Lambda$HHZoph2UpGUnWJcI_1_WGelnpJA;->INSTANCE:Lcom/squareup/queue/redundant/-$$Lambda$HHZoph2UpGUnWJcI_1_WGelnpJA;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1}, Lcom/squareup/queue/redundant/RedundantQueues;->verifyRemoval(I)V

    return-void
.end method
