.class public Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;
.super Ljava/lang/Object;
.source "MissedLoyaltyOpportunityTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private missedLoyaltyOpportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

.field private paymentId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;)Ljava/lang/String;
    .locals 0

    .line 71
    iget-object p0, p0, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;->paymentId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;
    .locals 0

    .line 71
    iget-object p0, p0, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;->missedLoyaltyOpportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;
    .locals 2

    .line 86
    new-instance v0, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;-><init>(Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$1;)V

    return-object v0
.end method

.method public missedLoyaltyOpportunity(Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;)Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;->missedLoyaltyOpportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    return-object p0
.end method

.method public paymentId(Ljava/lang/String;)Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;
    .locals 0

    .line 76
    iput-object p1, p0, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;->paymentId:Ljava/lang/String;

    return-object p0
.end method
