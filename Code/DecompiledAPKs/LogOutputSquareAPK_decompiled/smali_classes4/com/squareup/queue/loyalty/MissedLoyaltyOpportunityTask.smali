.class public Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;
.super Lcom/squareup/queue/RpcThreadTask;
.source "MissedLoyaltyOpportunityTask.java"

# interfaces
.implements Lcom/squareup/queue/LoggedInTransactionTask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/RpcThreadTask<",
        "Lcom/squareup/server/SimpleResponse;",
        "Lcom/squareup/queue/TransactionTasksComponent;",
        ">;",
        "Lcom/squareup/queue/LoggedInTransactionTask;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field transient loyaltyService:Lcom/squareup/server/loyalty/LoyaltyService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final missedLoyaltyOpportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

.field private final paymentId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;)V
    .locals 2

    .line 96
    invoke-direct {p0}, Lcom/squareup/queue/RpcThreadTask;-><init>()V

    .line 97
    invoke-static {p1}, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;->access$100(Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "paymentId"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;->paymentId:Ljava/lang/String;

    .line 99
    invoke-static {p1}, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;->access$200(Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    move-result-object p1

    const-string v0, "missedLoyaltyOpportunity"

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    iput-object p1, p0, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;->missedLoyaltyOpportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$1;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;-><init>(Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;)V

    return-void
.end method


# virtual methods
.method protected callOnRpcThread()Lcom/squareup/server/SimpleResponse;
    .locals 3

    .line 43
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;->loyaltyService:Lcom/squareup/server/loyalty/LoyaltyService;

    new-instance v1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;->paymentId:Ljava/lang/String;

    .line 45
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->server_payment_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;->missedLoyaltyOpportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    .line 46
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->missed_loyalty_opportunity(Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;

    move-result-object v1

    .line 47
    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;

    move-result-object v1

    .line 43
    invoke-interface {v0, v1}, Lcom/squareup/server/loyalty/LoyaltyService;->recordMissedLoyaltyOpportunity(Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    .line 51
    new-instance v1, Lcom/squareup/server/SimpleResponse;

    instance-of v0, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-direct {v1, v0}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 53
    :catch_0
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    return-object v0
.end method

.method protected bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;->callOnRpcThread()Lcom/squareup/server/SimpleResponse;

    move-result-object v0

    return-object v0
.end method

.method public getMissedLoyaltyOpportunity()Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;->missedLoyaltyOpportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    return-object v0
.end method

.method public getPaymentId()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;->paymentId:Ljava/lang/String;

    return-object v0
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/server/SimpleResponse;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    return-object p1
.end method

.method protected bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/server/SimpleResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;->handleResponseOnMainThread(Lcom/squareup/server/SimpleResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 35
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MissedLoyaltyOpportunityTask{paymentId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;->paymentId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', missedLoyaltyOpportunity=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;->missedLoyaltyOpportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\'}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
