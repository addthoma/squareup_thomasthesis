.class final Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$1;
.super Ljava/lang/Object;
.source "RealSafetyNetRecaptchaVerifier.kt"

# interfaces
.implements Lcom/google/android/gms/tasks/OnSuccessListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;->subscribe(Lio/reactivex/SingleEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TResult:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/tasks/OnSuccessListener<",
        "Lcom/google/android/gms/safetynet/SafetyNetApi$RecaptchaTokenResponse;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSafetyNetRecaptchaVerifier.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSafetyNetRecaptchaVerifier.kt\ncom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$1\n*L\n1#1,66:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "response",
        "Lcom/google/android/gms/safetynet/SafetyNetApi$RecaptchaTokenResponse;",
        "kotlin.jvm.PlatformType",
        "onSuccess"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $emitter:Lio/reactivex/SingleEmitter;

.field final synthetic this$0:Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;


# direct methods
.method constructor <init>(Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;Lio/reactivex/SingleEmitter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$1;->this$0:Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;

    iput-object p2, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$1;->$emitter:Lio/reactivex/SingleEmitter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSuccess(Lcom/google/android/gms/safetynet/SafetyNetApi$RecaptchaTokenResponse;)V
    .locals 3

    const-string v0, "response"

    .line 46
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/safetynet/SafetyNetApi$RecaptchaTokenResponse;->getTokenResult()Ljava/lang/String;

    move-result-object v0

    const-string v1, "response.tokenResult"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "NO_TOKEN:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$1;->this$0:Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;

    iget-object v0, v0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;->this$0:Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;

    invoke-static {v0}, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;->access$getInstallationId$p(Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 47
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/safetynet/SafetyNetApi$RecaptchaTokenResponse;->getTokenResult()Ljava/lang/String;

    move-result-object p1

    .line 49
    :goto_1
    iget-object v0, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$1;->$emitter:Lio/reactivex/SingleEmitter;

    new-instance v1, Lcom/squareup/safetynetrecaptcha/CaptchaResult$Success;

    const-string v2, "token"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p1}, Lcom/squareup/safetynetrecaptcha/CaptchaResult$Success;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Lcom/google/android/gms/safetynet/SafetyNetApi$RecaptchaTokenResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$1;->onSuccess(Lcom/google/android/gms/safetynet/SafetyNetApi$RecaptchaTokenResponse;)V

    return-void
.end method
