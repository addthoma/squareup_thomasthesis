.class public final Lcom/squareup/quickamounts/RealQuickAmountsSettings;
.super Ljava/lang/Object;
.source "RealQuickAmountsSettings.kt"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/quickamounts/QuickAmountsSettings;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealQuickAmountsSettings.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealQuickAmountsSettings.kt\ncom/squareup/quickamounts/RealQuickAmountsSettings\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,178:1\n704#2:179\n777#2,2:180\n950#2:182\n1360#2:183\n1429#2,3:184\n704#2:187\n777#2,2:188\n950#2:190\n1360#2:191\n1429#2,3:192\n*E\n*S KotlinDebug\n*F\n+ 1 RealQuickAmountsSettings.kt\ncom/squareup/quickamounts/RealQuickAmountsSettings\n*L\n105#1:179\n105#1,2:180\n106#1:182\n108#1:183\n108#1,3:184\n111#1:187\n111#1,2:188\n112#1:190\n114#1:191\n114#1,3:192\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002:\u0001,B!\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011H\u0016J\u0010\u0010\u0013\u001a\n \u000f*\u0004\u0018\u00010\u00140\u0014H\u0002J(\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010\u0018\u001a\u00020\u00192\u0010\u0008\u0002\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u001b\u0018\u00010\u001aH\u0002J\u0018\u0010\u001c\u001a\u00020\u00122\u0006\u0010\u001d\u001a\u00020\u00142\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0010\u0010 \u001a\u00020\u00122\u0006\u0010\u001d\u001a\u00020\u0014H\u0002J\u0010\u0010!\u001a\u00020\u00172\u0006\u0010\"\u001a\u00020#H\u0016J\u0008\u0010$\u001a\u00020\u0017H\u0016J\u0010\u0010%\u001a\u00020\u00122\u0006\u0010\u001d\u001a\u00020\u0014H\u0002J\u0016\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u00162\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0010\u0010\'\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0010\u0010(\u001a\u00020\u00172\u0006\u0010)\u001a\u00020\u0012H\u0016J\u000c\u0010*\u001a\u00020\u0019*\u00020+H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000c\u001a\u0010\u0012\u000c\u0012\n \u000f*\u0004\u0018\u00010\u000e0\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/quickamounts/RealQuickAmountsSettings;",
        "Lmortar/Scoped;",
        "Lcom/squareup/quickamounts/QuickAmountsSettings;",
        "badBus",
        "Lcom/squareup/badbus/BadBus;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "userToken",
        "",
        "(Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Ljava/lang/String;)V",
        "disposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "sourceToUse",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;",
        "kotlin.jvm.PlatformType",
        "amounts",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "createEmptyCatalogObject",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;",
        "doUpdate",
        "Lio/reactivex/Single;",
        "",
        "status",
        "Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "",
        "Lcom/squareup/protos/connect/v2/common/Money;",
        "fromCache",
        "catalogQuickAmounts",
        "cache",
        "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;",
        "fromCogs",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "quickAmountsForAnyOption",
        "quickAmountsForCurrentUnit",
        "setFeatureStatus",
        "updateSetAmounts",
        "setAmounts",
        "toStatus",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;",
        "DataSource",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final sourceToUse:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;",
            ">;"
        }
    .end annotation
.end field

.field private final userToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Ljava/lang/String;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/user/UserToken;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "badBus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cogs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->badBus:Lcom/squareup/badbus/BadBus;

    iput-object p2, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->cogs:Lcom/squareup/cogs/Cogs;

    iput-object p3, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->userToken:Ljava/lang/String;

    .line 41
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 42
    sget-object p1, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$CatalogLocal;->INSTANCE:Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$CatalogLocal;

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefa\u2026DataSource>(CatalogLocal)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->sourceToUse:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$createEmptyCatalogObject(Lcom/squareup/quickamounts/RealQuickAmountsSettings;)Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->createEmptyCatalogObject()Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$fromCache(Lcom/squareup/quickamounts/RealQuickAmountsSettings;Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;)Lcom/squareup/quickamounts/QuickAmounts;
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->fromCache(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$fromCogs(Lcom/squareup/quickamounts/RealQuickAmountsSettings;Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;)Lcom/squareup/quickamounts/QuickAmounts;
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->fromCogs(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getCogs$p(Lcom/squareup/quickamounts/RealQuickAmountsSettings;)Lcom/squareup/cogs/Cogs;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->cogs:Lcom/squareup/cogs/Cogs;

    return-object p0
.end method

.method public static final synthetic access$getSourceToUse$p(Lcom/squareup/quickamounts/RealQuickAmountsSettings;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->sourceToUse:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getUserToken$p(Lcom/squareup/quickamounts/RealQuickAmountsSettings;)Ljava/lang/String;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->userToken:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$quickAmountsForCurrentUnit(Lcom/squareup/quickamounts/RealQuickAmountsSettings;Lcom/squareup/cogs/Cogs;)Lio/reactivex/Single;
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->quickAmountsForCurrentUnit(Lcom/squareup/cogs/Cogs;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final createEmptyCatalogObject()Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;
    .locals 2

    .line 97
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;-><init>()V

    .line 98
    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->DISABLED:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->setCatalogQuickAmountsSettingsOption(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;)Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;

    move-result-object v0

    .line 99
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->setAmounts(Ljava/util/List;)Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;

    move-result-object v0

    .line 100
    iget-object v1, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->userToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->enableOnlyAtLocation(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    move-result-object v0

    return-object v0
.end method

.method private final doUpdate(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-direct {p0, v0}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->quickAmountsForCurrentUnit(Lcom/squareup/cogs/Cogs;)Lio/reactivex/Single;

    move-result-object v0

    .line 68
    new-instance v1, Lcom/squareup/quickamounts/RealQuickAmountsSettings$doUpdate$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$doUpdate$1;-><init>(Lcom/squareup/quickamounts/RealQuickAmountsSettings;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "quickAmountsForCurrentUn\u2026  .map { Unit }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method static synthetic doUpdate$default(Lcom/squareup/quickamounts/RealQuickAmountsSettings;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;ILjava/lang/Object;)Lio/reactivex/Single;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 65
    check-cast p2, Ljava/util/List;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->doUpdate(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final fromCache(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;)Lcom/squareup/quickamounts/QuickAmounts;
    .locals 6

    .line 141
    invoke-direct {p0, p1}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->quickAmountsForAnyOption(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->getStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/squareup/quickamounts/QuickAmounts;->copy$default(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/Map;ZILjava/lang/Object;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p1

    .line 142
    invoke-virtual {p2}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->getSetAmounts()Ljava/util/List;

    move-result-object p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/quickamounts/QuickAmounts;->getSetAmounts()Ljava/util/List;

    move-result-object p2

    .line 143
    :goto_0
    sget-object v0, Lcom/squareup/quickamounts/QuickAmountsStatus;->SET_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/quickamounts/QuickAmounts;->replaceAmounts(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p1

    return-object p1
.end method

.method private final fromCogs(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;)Lcom/squareup/quickamounts/QuickAmounts;
    .locals 0

    .line 137
    invoke-direct {p0, p1}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->quickAmountsForAnyOption(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p1

    return-object p1
.end method

.method private final quickAmountsForAnyOption(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;)Lcom/squareup/quickamounts/QuickAmounts;
    .locals 10

    .line 104
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;->getQuickAmounts()Ljava/util/List;

    move-result-object v0

    const-string v1, "catalogQuickAmounts.quickAmounts"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 179
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 180
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;

    .line 105
    iget-object v6, v6, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    sget-object v7, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->QUICK_AMOUNT_TYPE_MANUAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    if-ne v6, v7, :cond_1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_0

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 181
    :cond_2
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 182
    new-instance v0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForAnyOption$$inlined$sortedBy$1;

    invoke-direct {v0}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForAnyOption$$inlined$sortedBy$1;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    invoke-static {v2, v0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    const/4 v2, 0x3

    .line 107
    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 183
    new-instance v3, Ljava/util/ArrayList;

    const/16 v6, 0xa

    invoke-static {v0, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v3, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 184
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 185
    check-cast v7, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;

    .line 108
    iget-object v7, v7, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;->amount:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-interface {v3, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 186
    :cond_3
    check-cast v3, Ljava/util/List;

    .line 110
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;->getQuickAmounts()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 187
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 188
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    move-object v8, v7

    check-cast v8, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;

    .line 111
    iget-object v8, v8, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    sget-object v9, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->QUICK_AMOUNT_TYPE_AUTO:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    if-ne v8, v9, :cond_5

    const/4 v8, 0x1

    goto :goto_4

    :cond_5
    const/4 v8, 0x0

    :goto_4
    if-eqz v8, :cond_4

    invoke-interface {v1, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 189
    :cond_6
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 190
    new-instance v0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForAnyOption$$inlined$sortedBy$2;

    invoke-direct {v0}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForAnyOption$$inlined$sortedBy$2;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    invoke-static {v1, v0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 113
    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 191
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 192
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 193
    check-cast v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;

    .line 114
    iget-object v2, v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;->amount:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 194
    :cond_7
    check-cast v1, Ljava/util/List;

    .line 116
    new-instance v0, Lcom/squareup/quickamounts/QuickAmounts;

    .line 117
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;->getQuickAmountsOption()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    move-result-object v2

    const-string v6, "catalogQuickAmounts.quickAmountsOption"

    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->toStatus(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;)Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object v2

    const/4 v6, 0x2

    new-array v6, v6, [Lkotlin/Pair;

    .line 119
    sget-object v7, Lcom/squareup/quickamounts/QuickAmountsStatus;->SET_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-static {v7, v3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    aput-object v3, v6, v5

    .line 120
    sget-object v3, Lcom/squareup/quickamounts/QuickAmountsStatus;->AUTO_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-static {v3, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v6, v4

    .line 118
    invoke-static {v6}, Lkotlin/collections/MapsKt;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v1

    .line 122
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;->isEligibleForAutoAmounts()Z

    move-result p1

    .line 116
    invoke-direct {v0, v2, v1, p1}, Lcom/squareup/quickamounts/QuickAmounts;-><init>(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/Map;Z)V

    return-object v0
.end method

.method private final quickAmountsForCurrentUnit(Lcom/squareup/cogs/Cogs;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;",
            ">;"
        }
    .end annotation

    .line 80
    new-instance v0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForCurrentUnit$1;

    invoke-direct {v0, p0}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForCurrentUnit$1;-><init>(Lcom/squareup/quickamounts/RealQuickAmountsSettings;)V

    check-cast v0, Lcom/squareup/shared/catalog/CatalogTask;

    invoke-interface {p1, v0}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p1

    const-string v0, "cogs\n        .asSingle {\u2026on(userToken) }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p1

    .line 85
    new-instance v0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForCurrentUnit$2;

    invoke-direct {v0, p0}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForCurrentUnit$2;-><init>(Lcom/squareup/quickamounts/RealQuickAmountsSettings;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "cogs\n        .asSingle {\u2026ct() else it[0]\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final toStatus(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;)Lcom/squareup/quickamounts/QuickAmountsStatus;
    .locals 1

    .line 90
    sget-object v0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 94
    sget-object p1, Lcom/squareup/quickamounts/QuickAmountsStatus;->AUTO_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 93
    :cond_1
    sget-object p1, Lcom/squareup/quickamounts/QuickAmountsStatus;->SET_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    goto :goto_0

    .line 92
    :cond_2
    sget-object p1, Lcom/squareup/quickamounts/QuickAmountsStatus;->DISABLED:Lcom/squareup/quickamounts/QuickAmountsStatus;

    :goto_0
    return-object p1
.end method


# virtual methods
.method public amounts()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            ">;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->sourceToUse:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1;

    invoke-direct {v1, p0}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1;-><init>(Lcom/squareup/quickamounts/RealQuickAmountsSettings;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "sourceToUse.switchMapSin\u2026)\n        }\n      }\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    .line 128
    sget-object v1, Lcom/squareup/quickamounts/RealQuickAmountsSettings$onEnterScope$1;->INSTANCE:Lcom/squareup/quickamounts/RealQuickAmountsSettings$onEnterScope$1;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "badBus.events(CatalogCon\u2026QUICK_AMOUNTS_SETTINGS) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    new-instance v1, Lcom/squareup/quickamounts/RealQuickAmountsSettings$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$onEnterScope$2;-><init>(Lcom/squareup/quickamounts/RealQuickAmountsSettings;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public setFeatureStatus(Lcom/squareup/quickamounts/QuickAmountsStatus;)V
    .locals 3

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {p0, p1, v1, v2, v1}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->doUpdate$default(Lcom/squareup/quickamounts/RealQuickAmountsSettings;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;ILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public updateSetAmounts(Lcom/squareup/quickamounts/QuickAmounts;)V
    .locals 2

    const-string v0, "setAmounts"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {p1}, Lcom/squareup/quickamounts/QuickAmounts;->getStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/quickamounts/QuickAmounts;->getSetAmounts()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, v1, p1}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->doUpdate(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lio/reactivex/Single;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method
