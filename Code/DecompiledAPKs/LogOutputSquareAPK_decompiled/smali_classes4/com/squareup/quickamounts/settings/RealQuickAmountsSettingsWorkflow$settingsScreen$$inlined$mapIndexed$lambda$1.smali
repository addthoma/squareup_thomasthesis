.class final Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$$inlined$mapIndexed$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealQuickAmountsSettingsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->settingsScreen(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/workflow/RenderContext;Z)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;",
        "newText",
        "",
        "invoke",
        "com/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$amountEditTexts$1$editableText$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context$inlined:Lcom/squareup/workflow/RenderContext;

.field final synthetic $index:I

.field final synthetic $quickAmounts$inlined:Lcom/squareup/quickamounts/QuickAmounts;

.field final synthetic this$0:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;


# direct methods
.method constructor <init>(ILcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/quickamounts/QuickAmounts;)V
    .locals 0

    iput p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$$inlined$mapIndexed$lambda$1;->$index:I

    iput-object p2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$$inlined$mapIndexed$lambda$1;->this$0:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;

    iput-object p3, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$$inlined$mapIndexed$lambda$1;->$context$inlined:Lcom/squareup/workflow/RenderContext;

    iput-object p4, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$$inlined$mapIndexed$lambda$1;->$quickAmounts$inlined:Lcom/squareup/quickamounts/QuickAmounts;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/String;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;
    .locals 4

    const-string v0, "newText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    new-instance v0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$$inlined$mapIndexed$lambda$1;->this$0:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;

    invoke-static {v1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->access$getCurrency$p(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;)Lcom/squareup/protos/connect/v2/common/Currency;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$$inlined$mapIndexed$lambda$1;->$quickAmounts$inlined:Lcom/squareup/quickamounts/QuickAmounts;

    iget v3, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$$inlined$mapIndexed$lambda$1;->$index:I

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;-><init>(Lcom/squareup/protos/connect/v2/common/Currency;Lcom/squareup/quickamounts/QuickAmounts;ILjava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$$inlined$mapIndexed$lambda$1;->invoke(Ljava/lang/String;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;

    move-result-object p1

    return-object p1
.end method
