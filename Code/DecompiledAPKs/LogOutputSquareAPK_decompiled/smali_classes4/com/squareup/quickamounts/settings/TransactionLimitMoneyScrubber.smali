.class public final Lcom/squareup/quickamounts/settings/TransactionLimitMoneyScrubber;
.super Lcom/squareup/money/MoneyScrubber;
.source "TransactionLimitMoneyScrubber.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/quickamounts/settings/TransactionLimitMoneyScrubber;",
        "Lcom/squareup/money/MoneyScrubber;",
        "accountStatus",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountStatus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object p1

    const-string v0, "accountStatus.paymentSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/settings/server/PaymentSettings;->getTransactionMaximum()J

    move-result-wide v4

    sget-object v6, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANK_ON_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    .line 15
    invoke-direct/range {v1 .. v6}, Lcom/squareup/money/MoneyScrubber;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;JLcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V

    return-void
.end method
