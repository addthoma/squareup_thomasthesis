.class public final Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;
.super Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;
.source "RealQuickAmountsTutorialCreator.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator$Seed;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u0001\u0017B\u001d\u0008\u0001\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u000eH\u0016J\u0008\u0010\u0014\u001a\u00020\u0012H\u0016J\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0016H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0008\u001a\u0010\u0012\u000c\u0012\n \u000b*\u0004\u0018\u00010\n0\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u000c\u001a\u00020\r*\u00020\u000e8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;",
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
        "tutorialFactoryProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Ljavax/inject/Provider;Lcom/squareup/analytics/Analytics;)V",
        "seeds",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/tutorialv2/TutorialSeed;",
        "kotlin.jvm.PlatformType",
        "priority",
        "Lcom/squareup/tutorialv2/TutorialSeed$Priority;",
        "Lcom/squareup/quickamounts/ui/StartMode;",
        "getPriority",
        "(Lcom/squareup/quickamounts/ui/StartMode;)Lcom/squareup/tutorialv2/TutorialSeed$Priority;",
        "activate",
        "",
        "startMode",
        "deactivate",
        "triggeredTutorial",
        "Lio/reactivex/Observable;",
        "Seed",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "tutorialFactoryProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;-><init>()V

    iput-object p1, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;->tutorialFactoryProvider:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;->analytics:Lcom/squareup/analytics/Analytics;

    .line 26
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<TutorialSeed>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getPriority$p(Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;Lcom/squareup/quickamounts/ui/StartMode;)Lcom/squareup/tutorialv2/TutorialSeed$Priority;
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;->getPriority(Lcom/squareup/quickamounts/ui/StartMode;)Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getTutorialFactoryProvider$p(Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;)Ljavax/inject/Provider;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;->tutorialFactoryProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method private final getPriority(Lcom/squareup/quickamounts/ui/StartMode;)Lcom/squareup/tutorialv2/TutorialSeed$Priority;
    .locals 1

    .line 47
    sget-object v0, Lcom/squareup/quickamounts/ui/StartMode$ManualEligibilityStart;->INSTANCE:Lcom/squareup/quickamounts/ui/StartMode$ManualEligibilityStart;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->MERCHANT_STARTED:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    goto :goto_0

    .line 48
    :cond_0
    sget-object v0, Lcom/squareup/quickamounts/ui/StartMode$AutomaticEligibilityStart;->INSTANCE:Lcom/squareup/quickamounts/ui/StartMode$AutomaticEligibilityStart;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->AUTO_STARTED:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    goto :goto_0

    .line 49
    :cond_1
    sget-object v0, Lcom/squareup/quickamounts/ui/StartMode$AutomaticUpdatedAmountsStart;->INSTANCE:Lcom/squareup/quickamounts/ui/StartMode$AutomaticUpdatedAmountsStart;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->AUTO_STARTED:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public activate(Lcom/squareup/quickamounts/ui/StartMode;)V
    .locals 2

    const-string v0, "startMode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    sget-object v0, Lcom/squareup/quickamounts/ui/StartMode$ManualEligibilityStart;->INSTANCE:Lcom/squareup/quickamounts/ui/StartMode$ManualEligibilityStart;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialAnalyticsKt;->onClickedTutorial(Lcom/squareup/analytics/Analytics;)V

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator$Seed;

    invoke-direct {v1, p0, p1}, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator$Seed;-><init>(Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;Lcom/squareup/quickamounts/ui/StartMode;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public deactivate()V
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/tutorialv2/TutorialSeed;->NONE:Lcom/squareup/tutorialv2/TutorialSeed;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public triggeredTutorial()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
