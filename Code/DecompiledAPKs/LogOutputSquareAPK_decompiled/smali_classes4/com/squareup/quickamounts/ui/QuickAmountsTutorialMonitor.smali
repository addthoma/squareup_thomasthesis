.class public final Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;
.super Ljava/lang/Object;
.source "QuickAmountsTutorialMonitor.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$TutorialVisibilities;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nQuickAmountsTutorialMonitor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 QuickAmountsTutorialMonitor.kt\ncom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor\n*L\n1#1,111:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001:\u0001\u0016B-\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0002\u0010\u000bJ\u0018\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\nH\u0002J\u0018\u0010\u0010\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\nH\u0002J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u0012H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;",
        "Lmortar/Scoped;",
        "quickAmountsSettings",
        "Lcom/squareup/quickamounts/QuickAmountsSettings;",
        "quickAmountsTutorialCreator",
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "preference",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;Lcom/squareup/settings/server/Features;Lcom/f2prateek/rx/preferences2/Preference;)V",
        "checkForAutoAmountsEligibility",
        "",
        "oldAmounts",
        "newAmounts",
        "checkForAutoAmountsUpdated",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "TutorialVisibilities",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final preference:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            ">;"
        }
    .end annotation
.end field

.field private final quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

.field private final quickAmountsTutorialCreator:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;


# direct methods
.method public constructor <init>(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;Lcom/squareup/settings/server/Features;Lcom/f2prateek/rx/preferences2/Preference;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "quickAmountsSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "quickAmountsTutorialCreator"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "preference"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    iput-object p2, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->quickAmountsTutorialCreator:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;

    iput-object p3, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->features:Lcom/squareup/settings/server/Features;

    iput-object p4, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->preference:Lcom/f2prateek/rx/preferences2/Preference;

    return-void
.end method

.method public static final synthetic access$checkForAutoAmountsEligibility(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/quickamounts/QuickAmounts;)Z
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->checkForAutoAmountsEligibility(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/quickamounts/QuickAmounts;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$checkForAutoAmountsUpdated(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/quickamounts/QuickAmounts;)Z
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->checkForAutoAmountsUpdated(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/quickamounts/QuickAmounts;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$getPreference$p(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->preference:Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method

.method public static final synthetic access$getQuickAmountsTutorialCreator$p(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;)Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->quickAmountsTutorialCreator:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;

    return-object p0
.end method

.method private final checkForAutoAmountsEligibility(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/quickamounts/QuickAmounts;)Z
    .locals 2

    .line 86
    invoke-virtual {p1}, Lcom/squareup/quickamounts/QuickAmounts;->isEligibleForAutoAmounts()Z

    move-result p1

    const/4 v0, 0x1

    if-nez p1, :cond_0

    invoke-virtual {p2}, Lcom/squareup/quickamounts/QuickAmounts;->isEligibleForAutoAmounts()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lcom/squareup/quickamounts/QuickAmounts;->getStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object p1

    sget-object v1, Lcom/squareup/quickamounts/QuickAmountsStatus;->AUTO_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    if-ne p1, v1, :cond_0

    invoke-virtual {p2}, Lcom/squareup/quickamounts/QuickAmounts;->getAmounts()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v0

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final checkForAutoAmountsUpdated(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/quickamounts/QuickAmounts;)Z
    .locals 3

    .line 101
    invoke-virtual {p1}, Lcom/squareup/quickamounts/QuickAmounts;->getStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object v0

    sget-object v1, Lcom/squareup/quickamounts/QuickAmountsStatus;->AUTO_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p2}, Lcom/squareup/quickamounts/QuickAmounts;->getStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object v0

    sget-object v1, Lcom/squareup/quickamounts/QuickAmountsStatus;->AUTO_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    if-ne v0, v1, :cond_0

    invoke-virtual {p2}, Lcom/squareup/quickamounts/QuickAmounts;->getAmounts()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/quickamounts/QuickAmounts;->getAmounts()Ljava/util/List;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lcom/squareup/quickamounts/QuickAmounts;->getAmounts()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    invoke-interface {v0}, Lcom/squareup/quickamounts/QuickAmountsSettings;->amounts()Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 36
    new-instance v1, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$tutorialVisibilities$1;

    invoke-direct {v1, p0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$tutorialVisibilities$1;-><init>(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "quickAmountsSettings.amo\u2026            }\n          }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    sget-object v1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object v2, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->QUICK_AMOUNTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "features.featureEnabled(QUICK_AMOUNTS)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 56
    new-instance v1, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$1;-><init>(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
