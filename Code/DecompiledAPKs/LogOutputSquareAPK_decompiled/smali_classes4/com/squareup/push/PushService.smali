.class public interface abstract Lcom/squareup/push/PushService;
.super Ljava/lang/Object;
.source "PushService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0006\u001a\u00020\u0007H&J\u000e\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH&J\u000e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\tH&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/push/PushService;",
        "",
        "pushGateway",
        "Lcom/squareup/push/PushGateway;",
        "getPushGateway",
        "()Lcom/squareup/push/PushGateway;",
        "disable",
        "Lio/reactivex/Completable;",
        "enable",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/push/PushServiceRegistration;",
        "observeMessages",
        "Lcom/squareup/push/PushServiceMessage;",
        "public"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract disable()Lio/reactivex/Completable;
.end method

.method public abstract enable()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/push/PushServiceRegistration;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPushGateway()Lcom/squareup/push/PushGateway;
.end method

.method public abstract observeMessages()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/push/PushServiceMessage;",
            ">;"
        }
    .end annotation
.end method
