.class public final Lcom/squareup/register/widgets/LegacyNohoDatePickerView_MembersInjector;
.super Ljava/lang/Object;
.source "LegacyNohoDatePickerView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerView;",
        ">;"
    }
.end annotation


# instance fields
.field private final currentTimeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView_MembersInjector;->runnerProvider:Ljavax/inject/Provider;

    .line 22
    iput-object p2, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView_MembersInjector;->currentTimeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/register/widgets/LegacyNohoDatePickerView;",
            ">;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectCurrentTime(Lcom/squareup/register/widgets/LegacyNohoDatePickerView;Lcom/squareup/time/CurrentTime;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->currentTime:Lcom/squareup/time/CurrentTime;

    return-void
.end method

.method public static injectRunner(Lcom/squareup/register/widgets/LegacyNohoDatePickerView;Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->runner:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/register/widgets/LegacyNohoDatePickerView;)V
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView_MembersInjector;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;

    invoke-static {p1, v0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView_MembersInjector;->injectRunner(Lcom/squareup/register/widgets/LegacyNohoDatePickerView;Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView_MembersInjector;->currentTimeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/time/CurrentTime;

    invoke-static {p1, v0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView_MembersInjector;->injectCurrentTime(Lcom/squareup/register/widgets/LegacyNohoDatePickerView;Lcom/squareup/time/CurrentTime;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView_MembersInjector;->injectMembers(Lcom/squareup/register/widgets/LegacyNohoDatePickerView;)V

    return-void
.end method
