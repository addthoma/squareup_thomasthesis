.class public Lcom/squareup/register/widgets/TabletKeypadAmountContainer;
.super Landroid/widget/LinearLayout;
.source "TabletKeypadAmountContainer.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4

    .line 19
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x8

    .line 20
    invoke-virtual {p0}, Lcom/squareup/register/widgets/TabletKeypadAmountContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/widgets/pos/R$dimen;->keypad_backspace_half_width:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/register/widgets/TabletKeypadAmountContainer;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/register/widgets/TabletKeypadAmountContainer;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/register/widgets/TabletKeypadAmountContainer;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/squareup/register/widgets/TabletKeypadAmountContainer;->setPadding(IIII)V

    .line 22
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    return-void
.end method
