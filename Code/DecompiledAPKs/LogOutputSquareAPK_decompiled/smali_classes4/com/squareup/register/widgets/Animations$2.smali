.class final Lcom/squareup/register/widgets/Animations$2;
.super Landroid/view/animation/Animation;
.source "Animations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/Animations;->expandVertically(Landroid/view/View;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$height:I

.field final synthetic val$layoutParams:Landroid/view/ViewGroup$LayoutParams;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/ViewGroup$LayoutParams;ILandroid/view/View;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lcom/squareup/register/widgets/Animations$2;->val$layoutParams:Landroid/view/ViewGroup$LayoutParams;

    iput p2, p0, Lcom/squareup/register/widgets/Animations$2;->val$height:I

    iput-object p3, p0, Lcom/squareup/register/widgets/Animations$2;->val$view:Landroid/view/View;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 1

    .line 65
    iget-object p2, p0, Lcom/squareup/register/widgets/Animations$2;->val$layoutParams:Landroid/view/ViewGroup$LayoutParams;

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    iget v0, p0, Lcom/squareup/register/widgets/Animations$2;->val$height:I

    int-to-float v0, v0

    mul-float p1, p1, v0

    float-to-int p1, p1

    goto :goto_0

    :cond_0
    const/4 p1, -0x2

    :goto_0
    iput p1, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 66
    iget-object p1, p0, Lcom/squareup/register/widgets/Animations$2;->val$view:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public willChangeBounds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
