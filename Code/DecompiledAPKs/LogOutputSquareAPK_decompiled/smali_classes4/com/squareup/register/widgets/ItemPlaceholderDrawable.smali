.class public Lcom/squareup/register/widgets/ItemPlaceholderDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "ItemPlaceholderDrawable.java"


# static fields
.field public static final NO_INTRISIC_SIZE:I = -0x1


# instance fields
.field private final backgroundPaint:Landroid/graphics/Paint;

.field private dirty:Z

.field private final disabledTextPaint:Landroid/text/TextPaint;

.field private enabled:Z

.field private height:I

.field private maxHeight:F

.field private final text:Ljava/lang/String;

.field private final textPaint:Landroid/text/TextPaint;

.field private final textSpacingTemplate:Ljava/lang/String;

.field private textX:F

.field private textY:F

.field private width:I


# direct methods
.method private constructor <init>(Ljava/lang/String;ILandroid/content/Context;)V
    .locals 1

    .line 107
    invoke-static {p1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->spacingTemplate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;-><init>(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)V
    .locals 3

    .line 153
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    const/4 v0, -0x1

    .line 43
    iput v0, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->width:I

    .line 44
    iput v0, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->height:I

    .line 154
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 155
    sget v2, Lcom/squareup/marin/R$dimen;->marin_text_header_title:I

    .line 156
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->maxHeight:F

    const-string v2, "text"

    .line 158
    invoke-static {p1, v2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->text:Ljava/lang/String;

    const-string p1, "textSpacingTemplate"

    .line 159
    invoke-static {p2, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textSpacingTemplate:Ljava/lang/String;

    .line 161
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->backgroundPaint:Landroid/graphics/Paint;

    .line 162
    iget-object p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->backgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 164
    new-instance p1, Landroid/text/TextPaint;

    invoke-direct {p1}, Landroid/text/TextPaint;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textPaint:Landroid/text/TextPaint;

    .line 165
    iget-object p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 166
    iget-object p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textPaint:Landroid/text/TextPaint;

    sget-object p2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, p2}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 167
    iget-object p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textPaint:Landroid/text/TextPaint;

    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 p3, 0x0

    invoke-static {p4, p2, p3, p3}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;ZZ)Landroid/graphics/Typeface;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 168
    iget-object p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textPaint:Landroid/text/TextPaint;

    sget p2, Lcom/squareup/marin/R$color;->marin_text_shadow:I

    .line 169
    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 168
    invoke-virtual {p1, v0, v2, v0, p2}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 170
    iget-object p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textPaint:Landroid/text/TextPaint;

    invoke-static {p1}, Lcom/squareup/marketfont/MarketUtils;->configureOptimalPaintFlags(Landroid/graphics/Paint;)V

    .line 172
    sget p1, Lcom/squareup/marin/R$color;->marin_white_translucent:I

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    .line 173
    new-instance p2, Landroid/text/TextPaint;

    invoke-direct {p2}, Landroid/text/TextPaint;-><init>()V

    iput-object p2, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->disabledTextPaint:Landroid/text/TextPaint;

    .line 174
    iget-object p2, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->disabledTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p2, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 175
    iget-object p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->disabledTextPaint:Landroid/text/TextPaint;

    sget-object p2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, p2}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 176
    iget-object p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->disabledTextPaint:Landroid/text/TextPaint;

    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p4, p2, p3, p3}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;ZZ)Landroid/graphics/Typeface;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 177
    iget-object p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->disabledTextPaint:Landroid/text/TextPaint;

    invoke-static {p1}, Lcom/squareup/marketfont/MarketUtils;->configureOptimalPaintFlags(Landroid/graphics/Paint;)V

    const/4 p1, 0x1

    .line 179
    iput-boolean p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->enabled:Z

    return-void
.end method

.method public static defaultItemColor(Landroid/content/res/Resources;)I
    .locals 1

    .line 118
    sget v0, Lcom/squareup/widgets/pos/R$color;->edit_item_gray:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    return p0
.end method

.method public static forAllItems(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 4

    .line 122
    sget v0, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    .line 123
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STACK_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 124
    invoke-static {p0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->defaultItemColor(Landroid/content/res/Resources;)I

    move-result v2

    const/4 v3, 0x1

    .line 122
    invoke-static {p0, v0, v1, v2, v3}, Lcom/squareup/glyph/GlyphTypeface;->buildGlyphWithBackground(Landroid/content/res/Resources;ILcom/squareup/glyph/GlyphTypeface$Glyph;IZ)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method public static forAllServices(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 4

    .line 128
    sget v0, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    .line 129
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STACK_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 130
    invoke-static {p0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->defaultItemColor(Landroid/content/res/Resources;)I

    move-result v2

    const/4 v3, 0x1

    .line 128
    invoke-static {p0, v0, v1, v2, v3}, Lcom/squareup/glyph/GlyphTypeface;->buildGlyphWithBackground(Landroid/content/res/Resources;ILcom/squareup/glyph/GlyphTypeface$Glyph;IZ)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method public static forCustomAmount(Lcom/squareup/protos/common/CurrencyCode;Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .line 147
    new-instance v0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    .line 148
    invoke-static {p0}, Lcom/squareup/currency_db/Currencies;->getCurrencySymbol(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->defaultItemColor(Landroid/content/res/Resources;)I

    move-result v1

    invoke-direct {v0, p0, v1, p1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;-><init>(Ljava/lang/String;ILandroid/content/Context;)V

    return-object v0
.end method

.method public static forCustomItem(Ljava/lang/String;Landroid/content/Context;I)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->defaultItemColor(Landroid/content/res/Resources;)I

    move-result v1

    invoke-direct {v0, p0, v1, p1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;-><init>(Ljava/lang/String;ILandroid/content/Context;)V

    .line 66
    invoke-virtual {v0, p2, p2}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->setSize(II)V

    return-object v0
.end method

.method public static forDiscount(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 4

    .line 134
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    .line 135
    sget v0, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    .line 136
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v2, Lcom/squareup/widgets/pos/R$color;->edit_item_gray:I

    .line 137
    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const/4 v3, 0x1

    .line 135
    invoke-static {p0, v0, v1, v2, v3}, Lcom/squareup/glyph/GlyphTypeface;->buildGlyphWithBackground(Landroid/content/res/Resources;ILcom/squareup/glyph/GlyphTypeface$Glyph;IZ)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method public static forGiftCard(Ljava/lang/String;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 3

    .line 111
    invoke-static {p1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->defaultItemColor(Landroid/content/res/Resources;)I

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/util/Colors;->parseHex(Ljava/lang/String;I)I

    move-result p0

    .line 112
    sget v0, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    .line 113
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->GIFT_CARD_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v2, 0x1

    .line 112
    invoke-static {p1, v0, v1, p0, v2}, Lcom/squareup/glyph/GlyphTypeface;->buildGlyphWithBackground(Landroid/content/res/Resources;ILcom/squareup/glyph/GlyphTypeface$Glyph;IZ)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method public static forItem(Ljava/lang/String;Landroid/content/Context;)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;
    .locals 2

    .line 71
    new-instance v0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->defaultItemColor(Landroid/content/res/Resources;)I

    move-result v1

    invoke-direct {v0, p0, v1, p1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;-><init>(Ljava/lang/String;ILandroid/content/Context;)V

    return-object v0
.end method

.method public static forItem(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;
    .locals 0

    .line 95
    invoke-static {p0, p1, p3}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forItem(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    move-result-object p0

    .line 96
    invoke-virtual {p0, p2, p2}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->setSize(II)V

    return-object p0
.end method

.method public static forItem(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;
    .locals 2

    .line 102
    new-instance v0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    .line 103
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->defaultItemColor(Landroid/content/res/Resources;)I

    move-result v1

    invoke-static {p1, v1}, Lcom/squareup/util/Colors;->parseHex(Ljava/lang/String;I)I

    move-result p1

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;-><init>(Ljava/lang/String;ILandroid/content/Context;)V

    return-object v0
.end method

.method public static forItemization(Lcom/squareup/protos/client/bills/Itemization;ILandroid/content/Context;)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    iget-object v0, v0, Lcom/squareup/api/items/Item;->abbreviation:Ljava/lang/String;

    .line 80
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    iget-object v0, v0, Lcom/squareup/api/items/Item;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 84
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    iget-object v1, v1, Lcom/squareup/api/items/Item;->color:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 85
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    iget-object p0, p0, Lcom/squareup/api/items/Item;->color:Ljava/lang/String;

    invoke-static {v0, p0, p1, p2}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forItem(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    move-result-object p0

    return-object p0

    :cond_1
    const-string v0, ""

    .line 89
    :cond_2
    new-instance p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    .line 90
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->defaultItemColor(Landroid/content/res/Resources;)I

    move-result p1

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;-><init>(Ljava/lang/String;ILandroid/content/Context;)V

    return-object p0
.end method

.method public static forReward(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 4

    .line 141
    sget v0, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    .line 142
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REWARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v2, Lcom/squareup/widgets/pos/R$color;->edit_item_gray:I

    .line 143
    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const/4 v3, 0x1

    .line 141
    invoke-static {p0, v0, v1, v2, v3}, Lcom/squareup/glyph/GlyphTypeface;->buildGlyphWithBackground(Landroid/content/res/Resources;ILcom/squareup/glyph/GlyphTypeface$Glyph;IZ)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method private static spacingTemplate(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "l"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 4

    .line 192
    invoke-virtual {p0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 193
    iget-boolean v1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->dirty:Z

    if-eqz v1, :cond_1

    .line 194
    iget-object v1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textPaint:Landroid/text/TextPaint;

    iget-object v2, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textSpacingTemplate:Ljava/lang/String;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v1, v2, v0, v3}, Lcom/squareup/text/Fonts;->autoFitText(Landroid/text/TextPaint;Ljava/lang/String;Landroid/graphics/Rect;F)V

    .line 195
    iget-object v1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->getTextSize()F

    move-result v1

    iget v2, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->maxHeight:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 196
    iget-object v1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 197
    iget-object v1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->disabledTextPaint:Landroid/text/TextPaint;

    iget v2, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->maxHeight:F

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 199
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textX:F

    .line 200
    iget-object v1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textPaint:Landroid/text/TextPaint;

    iget v2, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    invoke-static {v1, v2, v0}, Lcom/squareup/text/Fonts;->getYForCenteredText(Landroid/text/TextPaint;FF)F

    move-result v0

    iput v0, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textY:F

    const/4 v0, 0x0

    .line 201
    iput-boolean v0, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->dirty:Z

    .line 203
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->backgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 204
    iget-boolean v0, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->enabled:Z

    if-eqz v0, :cond_2

    .line 205
    iget-object v0, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->text:Ljava/lang/String;

    iget v1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textX:F

    iget v2, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textY:F

    iget-object v3, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 207
    :cond_2
    iget-object v0, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->text:Ljava/lang/String;

    iget v1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textX:F

    iget v2, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textY:F

    iget-object v3, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->disabledTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :goto_0
    return-void
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .line 249
    iget v0, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->height:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .line 253
    iget v0, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->width:I

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->text:Ljava/lang/String;

    return-object v0
.end method

.method public isStateful()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 0

    .line 244
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    const/4 p1, 0x1

    .line 245
    iput-boolean p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->dirty:Z

    return-void
.end method

.method protected onStateChange([I)Z
    .locals 6

    .line 213
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x1

    if-ge v2, v0, :cond_1

    aget v4, p1, v2

    const v5, 0x101009e

    if-ne v4, v5, :cond_0

    const/4 p1, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 219
    :goto_1
    iget-boolean v0, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->enabled:Z

    if-eq p1, v0, :cond_2

    .line 220
    iput-boolean p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->enabled:Z

    .line 221
    invoke-virtual {p0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->invalidateSelf()V

    return v3

    :cond_2
    return v1
.end method

.method public setAlpha(I)V
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->backgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 233
    iget-object v0, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setAlpha(I)V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    return-void
.end method

.method public setMaxHeight(F)V
    .locals 0

    .line 267
    iput p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->maxHeight:F

    .line 268
    invoke-virtual {p0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->invalidateSelf()V

    return-void
.end method

.method public setSize(II)V
    .locals 0

    .line 261
    iput p1, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->width:I

    .line 262
    iput p2, p0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->height:I

    .line 263
    invoke-virtual {p0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->invalidateSelf()V

    return-void
.end method
