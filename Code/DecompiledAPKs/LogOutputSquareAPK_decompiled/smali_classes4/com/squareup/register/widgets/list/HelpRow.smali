.class public Lcom/squareup/register/widgets/list/HelpRow;
.super Landroid/widget/FrameLayout;
.source "HelpRow.java"


# instance fields
.field private final messageView:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 17
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 19
    sget v0, Lcom/squareup/widgets/pos/R$layout;->help_row:I

    invoke-static {p1, v0, p0}, Lcom/squareup/register/widgets/list/HelpRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 20
    sget p1, Lcom/squareup/widgets/pos/R$id;->message:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/register/widgets/list/HelpRow;->messageView:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/list/HelpRow;-><init>(Landroid/content/Context;)V

    .line 30
    iget-object p1, p0, Lcom/squareup/register/widgets/list/HelpRow;->messageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/CharSequence;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/list/HelpRow;-><init>(Landroid/content/Context;)V

    .line 25
    invoke-virtual {p0, p2}, Lcom/squareup/register/widgets/list/HelpRow;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public getText()Ljava/lang/CharSequence;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/register/widgets/list/HelpRow;->messageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0}, Lcom/squareup/widgets/MessageView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/register/widgets/list/HelpRow;->messageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
