.class public Lcom/squareup/register/widgets/StringDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "StringDrawable.java"


# instance fields
.field private dirty:Z

.field private final shadowFudge:F

.field private final string:Ljava/lang/String;

.field private final textBounds:Landroid/graphics/Rect;

.field private final textPaint:Landroid/text/TextPaint;

.field private textX:F

.field private textY:F


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Float;Landroid/graphics/Typeface;IFF)V
    .locals 1

    .line 28
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 21
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/squareup/register/widgets/StringDrawable;->textBounds:Landroid/graphics/Rect;

    const/4 v0, 0x1

    .line 25
    iput-boolean v0, p0, Lcom/squareup/register/widgets/StringDrawable;->dirty:Z

    .line 29
    iput-object p1, p0, Lcom/squareup/register/widgets/StringDrawable;->string:Ljava/lang/String;

    .line 30
    iput p6, p0, Lcom/squareup/register/widgets/StringDrawable;->shadowFudge:F

    .line 32
    new-instance p6, Landroid/text/TextPaint;

    const/16 v0, 0x81

    invoke-direct {p6, v0}, Landroid/text/TextPaint;-><init>(I)V

    iput-object p6, p0, Lcom/squareup/register/widgets/StringDrawable;->textPaint:Landroid/text/TextPaint;

    .line 33
    iget-object p6, p0, Lcom/squareup/register/widgets/StringDrawable;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    invoke-virtual {p6, p2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 34
    iget-object p2, p0, Lcom/squareup/register/widgets/StringDrawable;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p2, p3}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 35
    iget-object p2, p0, Lcom/squareup/register/widgets/StringDrawable;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p2, p4}, Landroid/text/TextPaint;->setColor(I)V

    .line 36
    iget-object p2, p0, Lcom/squareup/register/widgets/StringDrawable;->textPaint:Landroid/text/TextPaint;

    const/4 p3, 0x0

    const p4, -0x777778

    invoke-virtual {p2, p5, p3, p5, p4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 37
    iget-object p2, p0, Lcom/squareup/register/widgets/StringDrawable;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p3

    iget-object p4, p0, Lcom/squareup/register/widgets/StringDrawable;->textBounds:Landroid/graphics/Rect;

    const/4 p5, 0x0

    invoke-virtual {p2, p1, p5, p3, p4}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    return-void
.end method

.method private layoutNow()V
    .locals 4

    .line 72
    invoke-virtual {p0}, Lcom/squareup/register/widgets/StringDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 74
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    .line 75
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/register/widgets/StringDrawable;->textBounds:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/squareup/register/widgets/StringDrawable;->shadowFudge:F

    sub-float/2addr v1, v2

    .line 77
    iget v0, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Lcom/squareup/register/widgets/StringDrawable;->getIntrinsicWidth()I

    move-result v2

    sub-int/2addr v0, v2

    int-to-float v0, v0

    iput v0, p0, Lcom/squareup/register/widgets/StringDrawable;->textX:F

    .line 78
    iput v1, p0, Lcom/squareup/register/widgets/StringDrawable;->textY:F

    const/4 v0, 0x0

    .line 80
    iput-boolean v0, p0, Lcom/squareup/register/widgets/StringDrawable;->dirty:Z

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 4

    .line 41
    iget-boolean v0, p0, Lcom/squareup/register/widgets/StringDrawable;->dirty:Z

    if-eqz v0, :cond_0

    .line 42
    invoke-direct {p0}, Lcom/squareup/register/widgets/StringDrawable;->layoutNow()V

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/widgets/StringDrawable;->string:Ljava/lang/String;

    iget v1, p0, Lcom/squareup/register/widgets/StringDrawable;->textX:F

    iget v2, p0, Lcom/squareup/register/widgets/StringDrawable;->textY:F

    iget-object v3, p0, Lcom/squareup/register/widgets/StringDrawable;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method public getIntrinsicHeight()I
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/register/widgets/StringDrawable;->textBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/squareup/register/widgets/StringDrawable;->shadowFudge:F

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/register/widgets/StringDrawable;->textBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 0

    .line 67
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    const/4 p1, 0x1

    .line 68
    iput-boolean p1, p0, Lcom/squareup/register/widgets/StringDrawable;->dirty:Z

    return-void
.end method

.method public setAlpha(I)V
    .locals 0

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    return-void
.end method
