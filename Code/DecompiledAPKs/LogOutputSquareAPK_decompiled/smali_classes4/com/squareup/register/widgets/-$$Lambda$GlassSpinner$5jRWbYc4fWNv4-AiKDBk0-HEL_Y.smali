.class public final synthetic Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$5jRWbYc4fWNv4-AiKDBk0-HEL_Y;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/Observable$Transformer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/register/widgets/GlassSpinner;

.field private final synthetic f$1:I

.field private final synthetic f$2:J

.field private final synthetic f$3:Ljava/util/concurrent/TimeUnit;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/register/widgets/GlassSpinner;IJLjava/util/concurrent/TimeUnit;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$5jRWbYc4fWNv4-AiKDBk0-HEL_Y;->f$0:Lcom/squareup/register/widgets/GlassSpinner;

    iput p2, p0, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$5jRWbYc4fWNv4-AiKDBk0-HEL_Y;->f$1:I

    iput-wide p3, p0, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$5jRWbYc4fWNv4-AiKDBk0-HEL_Y;->f$2:J

    iput-object p5, p0, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$5jRWbYc4fWNv4-AiKDBk0-HEL_Y;->f$3:Ljava/util/concurrent/TimeUnit;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$5jRWbYc4fWNv4-AiKDBk0-HEL_Y;->f$0:Lcom/squareup/register/widgets/GlassSpinner;

    iget v1, p0, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$5jRWbYc4fWNv4-AiKDBk0-HEL_Y;->f$1:I

    iget-wide v2, p0, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$5jRWbYc4fWNv4-AiKDBk0-HEL_Y;->f$2:J

    iget-object v4, p0, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$5jRWbYc4fWNv4-AiKDBk0-HEL_Y;->f$3:Ljava/util/concurrent/TimeUnit;

    move-object v5, p1

    check-cast v5, Lrx/Observable;

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/register/widgets/GlassSpinner;->lambda$spinnerTransform$3$GlassSpinner(IJLjava/util/concurrent/TimeUnit;Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
