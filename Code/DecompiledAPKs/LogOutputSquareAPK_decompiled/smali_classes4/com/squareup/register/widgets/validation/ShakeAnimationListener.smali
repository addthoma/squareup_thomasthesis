.class public Lcom/squareup/register/widgets/validation/ShakeAnimationListener;
.super Lcom/squareup/ui/EmptyAnimationListener;
.source "ShakeAnimationListener.java"


# instance fields
.field private final originalHintColorStateList:Landroid/content/res/ColorStateList;

.field private final originalTextColorStateList:Landroid/content/res/ColorStateList;

.field private final restoreOriginalTextColorOnEnd:Z

.field private final view:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/widget/TextView;)V
    .locals 1

    const/4 v0, 0x1

    .line 18
    invoke-direct {p0, p1, v0}, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;-><init>(Landroid/widget/TextView;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/widget/TextView;Z)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/squareup/ui/EmptyAnimationListener;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;->view:Landroid/widget/TextView;

    .line 23
    iput-boolean p2, p0, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;->restoreOriginalTextColorOnEnd:Z

    .line 24
    invoke-virtual {p1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;->originalTextColorStateList:Landroid/content/res/ColorStateList;

    .line 25
    invoke-virtual {p1}, Landroid/widget/TextView;->getHintTextColors()Landroid/content/res/ColorStateList;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;->originalHintColorStateList:Landroid/content/res/ColorStateList;

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0

    .line 35
    iget-boolean p1, p0, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;->restoreOriginalTextColorOnEnd:Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;->restoreOriginalTextColor()V

    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    .line 29
    iget-object p1, p0, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;->view:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Views;->vibrate(Landroid/content/Context;)V

    .line 30
    iget-object p1, p0, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;->view:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/widgets/pos/R$color;->error_text_selector:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 31
    iget-object p1, p0, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;->view:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/widgets/pos/R$color;->error_text_selector:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public restoreOriginalTextColor()V
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;->view:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;->originalTextColorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;->view:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;->originalHintColorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method
