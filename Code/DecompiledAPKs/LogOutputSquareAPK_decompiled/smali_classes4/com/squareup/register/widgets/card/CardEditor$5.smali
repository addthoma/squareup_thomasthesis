.class Lcom/squareup/register/widgets/card/CardEditor$5;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "CardEditor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/card/CardEditor;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/card/CardEditor;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/card/CardEditor;)V
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor$5;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x4

    if-ne p2, p1, :cond_0

    .line 274
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor$5;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/CardEditor;->getCard()Lcom/squareup/Card;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 276
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor$5;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {p2}, Lcom/squareup/register/widgets/card/CardEditor;->access$300(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/register/widgets/card/OnCardListener;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/squareup/register/widgets/card/OnCardListener;->onChargeCard(Lcom/squareup/Card;)V

    :cond_0
    const/4 p1, 0x1

    return p1
.end method
