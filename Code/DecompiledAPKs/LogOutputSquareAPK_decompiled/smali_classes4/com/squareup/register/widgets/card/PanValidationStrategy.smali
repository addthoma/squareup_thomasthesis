.class public interface abstract Lcom/squareup/register/widgets/card/PanValidationStrategy;
.super Ljava/lang/Object;
.source "PanValidationStrategy.java"


# static fields
.field public static final GIFT_CARD_EXPIRATION_MONTH:Ljava/lang/String; = "12"

.field public static final GIFT_CARD_EXPIRATION_YEAR:Ljava/lang/String; = "99"


# virtual methods
.method public abstract cvvValid(Ljava/lang/String;Lcom/squareup/Card$Brand;)Z
.end method

.method public abstract determineCardNumberState(Ljava/lang/String;Lcom/squareup/Card$Brand;Z)Lcom/squareup/register/widgets/card/CardNumberState;
.end method

.method public abstract expirationValid(Ljava/lang/String;)Z
.end method

.method public abstract getBrand(Ljava/lang/String;)Lcom/squareup/Card$Brand;
.end method

.method public abstract getDefaultGlyph(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
.end method

.method public abstract getGlyph(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
.end method

.method public abstract getInputType()I
.end method

.method public abstract getRestartImeAfterScrubChange()Z
.end method

.method public abstract isMaximumLength(Lcom/squareup/Card$Brand;I)Z
.end method

.method public abstract panCharacterValid(C)Z
.end method

.method public abstract postalValid(ZLjava/lang/String;)Z
.end method

.method public abstract validateAndBuildCard(Ljava/lang/String;Lcom/squareup/giftcard/GiftCards;)Lcom/squareup/Card;
.end method

.method public abstract validateAndContinueBuildingCard(Lcom/squareup/Card;Lcom/squareup/giftcard/GiftCards;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/Card;
.end method

.method public abstract validateOnlyPan()Z
.end method
