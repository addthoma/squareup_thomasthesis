.class public Lcom/squareup/register/widgets/card/PostalEditText;
.super Lcom/squareup/register/widgets/card/CardEditorEditText;
.source "PostalEditText.java"


# static fields
.field private static final ALLOWED_US_CHARS:Ljava/lang/String; = "0123456789-"


# instance fields
.field private toggleKeyboardGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/squareup/register/widgets/card/CardEditorEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x1

    .line 36
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/card/PostalEditText;->setSingleLine(Z)V

    .line 37
    sget-object p2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, p2}, Lcom/squareup/register/widgets/card/PostalEditText;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    const p2, 0x10000004

    .line 38
    invoke-virtual {p0, p2}, Lcom/squareup/register/widgets/card/PostalEditText;->setImeOptions(I)V

    const/4 p2, 0x2

    new-array p2, p2, [Landroid/text/InputFilter;

    .line 40
    new-instance v0, Landroid/text/InputFilter$AllCaps;

    invoke-direct {v0}, Landroid/text/InputFilter$AllCaps;-><init>()V

    const/4 v1, 0x0

    aput-object v0, p2, v1

    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v0, p2, p1

    invoke-virtual {p0, p2}, Lcom/squareup/register/widgets/card/PostalEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 46
    sget-object p1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/card/PostalEditText;->showDefaultKeyboardForCountry(Lcom/squareup/CountryCode;)V

    return-void
.end method

.method private setPostalAlphaKeyboard()V
    .locals 1

    .line 89
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->KEYBOARD_NUMBERS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput-object v0, p0, Lcom/squareup/register/widgets/card/PostalEditText;->toggleKeyboardGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const v0, 0x81070

    .line 90
    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/card/PostalEditText;->setInputType(I)V

    return-void
.end method

.method private setPostalNumberKeyboard()V
    .locals 1

    .line 82
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->KEYBOARD_ALPHA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput-object v0, p0, Lcom/squareup/register/widgets/card/PostalEditText;->toggleKeyboardGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const v0, 0x80002

    .line 83
    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/card/PostalEditText;->setInputType(I)V

    const-string v0, "0123456789-"

    .line 84
    invoke-static {v0}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/card/PostalEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 85
    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/PostalEditText;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/card/PostalEditText;->setSelection(I)V

    return-void
.end method

.method private showKeyboard(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 66
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/PostalEditText;->setPostalAlphaKeyboard()V

    goto :goto_0

    .line 68
    :cond_0
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/PostalEditText;->setPostalNumberKeyboard()V

    :goto_0
    return-void
.end method


# virtual methods
.method public getToggleKeyboardGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PostalEditText;->toggleKeyboardGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public showDefaultKeyboardForCountry(Lcom/squareup/CountryCode;)V
    .locals 0

    .line 56
    invoke-static {p1}, Lcom/squareup/address/CountryResources;->prefersAlphanumericKeyboardForPostal(Lcom/squareup/CountryCode;)Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/card/PostalEditText;->showKeyboard(Z)V

    return-void
.end method

.method public showHintForCountry(Lcom/squareup/CountryCode;)V
    .locals 0

    .line 51
    invoke-static {p1}, Lcom/squareup/address/CountryResources;->postalCardHint(Lcom/squareup/CountryCode;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/card/PostalEditText;->setHint(I)V

    return-void
.end method

.method public toggleKeyboard()V
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PostalEditText;->toggleKeyboardGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->KEYBOARD_ALPHA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/squareup/register/widgets/card/PostalEditText;->showKeyboard(Z)V

    return-void
.end method
