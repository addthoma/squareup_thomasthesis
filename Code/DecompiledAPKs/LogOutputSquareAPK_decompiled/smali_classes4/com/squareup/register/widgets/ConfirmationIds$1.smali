.class final Lcom/squareup/register/widgets/ConfirmationIds$1;
.super Ljava/lang/Object;
.source "ConfirmationIds.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/ConfirmationIds;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/register/widgets/ConfirmationIds;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/register/widgets/ConfirmationIds;
    .locals 4

    .line 61
    new-instance v0, Lcom/squareup/register/widgets/ConfirmationIds;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/register/widgets/ConfirmationIds;-><init>(IIII)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 59
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/ConfirmationIds$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/register/widgets/ConfirmationIds;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/register/widgets/ConfirmationIds;
    .locals 0

    .line 66
    new-array p1, p1, [Lcom/squareup/register/widgets/ConfirmationIds;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 59
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/ConfirmationIds$1;->newArray(I)[Lcom/squareup/register/widgets/ConfirmationIds;

    move-result-object p1

    return-object p1
.end method
