.class public final Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen_DialogBuilder_MembersInjector;
.super Ljava/lang/Object;
.source "NohoDurationPickerDialogScreen_DialogBuilder_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;",
        ">;"
    }
.end annotation


# instance fields
.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen_DialogBuilder_MembersInjector;->runnerProvider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen_DialogBuilder_MembersInjector;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen_DialogBuilder_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen_DialogBuilder_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectRes(Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;Lcom/squareup/util/Res;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static injectRunner(Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;Lcom/squareup/register/widgets/NohoDurationPickerRunner;)V
    .locals 0

    .line 37
    iput-object p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->runner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen_DialogBuilder_MembersInjector;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    invoke-static {p1, v0}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen_DialogBuilder_MembersInjector;->injectRunner(Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;Lcom/squareup/register/widgets/NohoDurationPickerRunner;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen_DialogBuilder_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen_DialogBuilder_MembersInjector;->injectRes(Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen_DialogBuilder_MembersInjector;->injectMembers(Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;)V

    return-void
.end method
