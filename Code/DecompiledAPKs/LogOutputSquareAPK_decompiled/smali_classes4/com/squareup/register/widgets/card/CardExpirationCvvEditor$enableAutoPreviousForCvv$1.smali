.class final Lcom/squareup/register/widgets/card/CardExpirationCvvEditor$enableAutoPreviousForCvv$1;
.super Ljava/lang/Object;
.source "CardExpirationCvvEditor.kt"

# interfaces
.implements Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->enableAutoPreviousForCvv()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onDeleteKey"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor$enableAutoPreviousForCvv$1;->this$0:Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDeleteKey(Landroid/view/View;)V
    .locals 2

    .line 91
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor$enableAutoPreviousForCvv$1;->this$0:Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->access$getCvvEditor$p(Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;)Lcom/squareup/register/widgets/card/CvvEditor;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor$enableAutoPreviousForCvv$1;->this$0:Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;

    invoke-static {v0}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->access$getExpirationEditor$p(Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;)Lcom/squareup/register/widgets/card/ExpirationEditor;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/squareup/text/AutoAdvanceTextWatcher;->focusNextView(Landroid/view/View;Landroid/view/View;Z)V

    return-void
.end method
