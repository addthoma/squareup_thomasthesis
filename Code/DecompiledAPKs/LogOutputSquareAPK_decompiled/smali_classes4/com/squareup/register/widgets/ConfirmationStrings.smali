.class public Lcom/squareup/register/widgets/ConfirmationStrings;
.super Ljava/lang/Object;
.source "ConfirmationStrings.java"

# interfaces
.implements Lcom/squareup/register/widgets/Confirmation;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/register/widgets/ConfirmationStrings;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final body:Ljava/lang/String;

.field private final cancel:Ljava/lang/String;

.field private final confirm:Ljava/lang/String;

.field private final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 68
    new-instance v0, Lcom/squareup/register/widgets/ConfirmationStrings$1;

    invoke-direct {v0}, Lcom/squareup/register/widgets/ConfirmationStrings$1;-><init>()V

    sput-object v0, Lcom/squareup/register/widgets/ConfirmationStrings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->title:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->body:Ljava/lang/String;

    .line 20
    iput-object p3, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->confirm:Ljava/lang/String;

    .line 21
    iput-object p4, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->cancel:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_6

    .line 35
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_0

    .line 37
    :cond_1
    check-cast p1, Lcom/squareup/register/widgets/ConfirmationStrings;

    .line 39
    iget-object v2, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/register/widgets/ConfirmationStrings;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    .line 40
    :cond_2
    iget-object v2, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->body:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/register/widgets/ConfirmationStrings;->body:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    return v1

    .line 41
    :cond_3
    iget-object v2, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->confirm:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/register/widgets/ConfirmationStrings;->confirm:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    return v1

    .line 42
    :cond_4
    iget-object v2, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->cancel:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/register/widgets/ConfirmationStrings;->cancel:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    return v1

    :cond_5
    return v0

    :cond_6
    :goto_0
    return v1
.end method

.method public getStrings(Landroid/content/res/Resources;)Lcom/squareup/register/widgets/Confirmation$Strings;
    .locals 4

    .line 25
    new-instance p1, Lcom/squareup/register/widgets/Confirmation$Strings;

    iget-object v0, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->title:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->body:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->confirm:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->cancel:Ljava/lang/String;

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/squareup/register/widgets/Confirmation$Strings;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 49
    iget-object v1, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->title:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->body:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->confirm:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->cancel:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ConfirmationStrings{title=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", body=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->body:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", confirm=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->confirm:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", cancel=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->cancel:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 53
    iget-object p2, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 54
    iget-object p2, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->body:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 55
    iget-object p2, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->confirm:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 56
    iget-object p2, p0, Lcom/squareup/register/widgets/ConfirmationStrings;->cancel:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
