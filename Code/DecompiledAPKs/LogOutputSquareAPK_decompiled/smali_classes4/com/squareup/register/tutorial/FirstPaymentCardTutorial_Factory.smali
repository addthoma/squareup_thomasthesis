.class public final Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;
.super Ljava/lang/Object;
.source "FirstPaymentCardTutorial_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountFreezeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final bankAccountSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final bankLinkingStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderOracleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutWorkflowRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final feeTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final firstPaymentTooltipStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/FirstPaymentTooltipStatus;",
            ">;>;"
        }
    .end annotation
.end field

.field private final homeIdentifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;",
            ">;"
        }
    .end annotation
.end field

.field private final loggingHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSelectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryPagesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingTransactionsStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final textRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/FirstPaymentTooltipStatus;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 114
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->accountFreezeProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 115
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->applicationProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 116
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->presenterProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 117
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 118
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 119
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 120
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 121
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 122
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->pendingTransactionsStoreProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 123
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->busProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 124
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->orderEntryPagesProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 125
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 126
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->openTicketsSelectorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 127
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->homeIdentifierProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 128
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 129
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->firstPaymentTooltipStatusProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 130
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 131
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 132
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->checkoutWorkflowRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 133
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 134
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->textRendererProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 135
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->bankAccountSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 136
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->loggingHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 137
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 138
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->bankLinkingStarterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 139
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->feeTutorialProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/FirstPaymentTooltipStatus;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;)",
            "Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    .line 171
    new-instance v27, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;

    move-object/from16 v0, v27

    invoke-direct/range {v0 .. v26}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v27
.end method

.method public static newInstance(Lcom/squareup/accountfreeze/AccountFreeze;Landroid/app/Application;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/LocalSetting;Lio/reactivex/Scheduler;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/feetutorial/FeeTutorial;)Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            "Landroid/app/Application;",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
            "Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/FirstPaymentTooltipStatus;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            "Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ")",
            "Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    .line 187
    new-instance v27, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;

    move-object/from16 v0, v27

    invoke-direct/range {v0 .. v26}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;-><init>(Lcom/squareup/accountfreeze/AccountFreeze;Landroid/app/Application;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/LocalSetting;Lio/reactivex/Scheduler;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/feetutorial/FeeTutorial;)V

    return-object v27
.end method


# virtual methods
.method public get()Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;
    .locals 28

    move-object/from16 v0, p0

    .line 144
    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->accountFreezeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/accountfreeze/AccountFreeze;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Landroid/app/Application;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->pendingTransactionsStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/payment/pending/PendingTransactionsStore;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->orderEntryPagesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/orderentry/pages/OrderEntryPages;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->openTicketsSelectorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->homeIdentifierProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->firstPaymentTooltipStatusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/settings/LocalSetting;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lio/reactivex/Scheduler;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->checkoutWorkflowRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/ui/main/CheckoutWorkflowRunner;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->textRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->bankAccountSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/banklinking/BankAccountSettings;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->loggingHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->bankLinkingStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/banklinking/BankLinkingStarter;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->feeTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/feetutorial/FeeTutorial;

    invoke-static/range {v2 .. v27}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->newInstance(Lcom/squareup/accountfreeze/AccountFreeze;Landroid/app/Application;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/LocalSetting;Lio/reactivex/Scheduler;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/feetutorial/FeeTutorial;)Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial_Factory;->get()Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;

    move-result-object v0

    return-object v0
.end method
