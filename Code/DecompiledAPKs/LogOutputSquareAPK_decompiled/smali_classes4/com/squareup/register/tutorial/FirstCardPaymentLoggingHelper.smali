.class public Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;
.super Ljava/lang/Object;
.source "FirstCardPaymentLoggingHelper.java"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private lastLoggedPanel:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

.field private lastPaymentAmount:J

.field private lastTenderType:Ljava/lang/String;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/Transaction;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 19
    iput-wide v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->lastPaymentAmount:J

    .line 20
    sget-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_1_start:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    iput-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->lastLoggedPanel:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    .line 23
    iput-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->analytics:Lcom/squareup/analytics/Analytics;

    .line 24
    iput-object p2, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->transaction:Lcom/squareup/payment/Transaction;

    return-void
.end method

.method private getTenderTypeForLogging()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->dashedCardLoggingName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->lastTenderType:Ljava/lang/String;

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->lastTenderType:Ljava/lang/String;

    return-object v0
.end method

.method private paymentAmount()J
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    .line 56
    iput-wide v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->lastPaymentAmount:J

    .line 59
    :cond_1
    iget-wide v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->lastPaymentAmount:J

    return-wide v0
.end method


# virtual methods
.method logPanel(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;)V
    .locals 5

    .line 28
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->lastLoggedPanel:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {v0, p1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->shouldSkip(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 32
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->lastLoggedPanel:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {v1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->ordinal()I

    move-result v1

    if-ge v0, v1, :cond_1

    const/4 v0, 0x0

    .line 33
    iput-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->lastTenderType:Ljava/lang/String;

    const-wide/16 v0, 0x0

    .line 34
    iput-wide v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->lastPaymentAmount:J

    .line 36
    :cond_1
    iput-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->lastLoggedPanel:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    .line 37
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent;

    .line 38
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->getTenderTypeForLogging()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->paymentAmount()J

    move-result-wide v3

    invoke-direct {v1, p1, v2, v3, v4}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent;-><init>(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/String;J)V

    .line 37
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
