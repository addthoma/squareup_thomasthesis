.class Lcom/squareup/register/tutorial/TutorialDialog$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "TutorialDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/tutorial/TutorialDialog;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/tutorial/TutorialDialog;

.field final synthetic val$presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

.field final synthetic val$prompt:Lcom/squareup/register/tutorial/TutorialDialog$Prompt;


# direct methods
.method constructor <init>(Lcom/squareup/register/tutorial/TutorialDialog;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/register/tutorial/TutorialDialog$Prompt;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/squareup/register/tutorial/TutorialDialog$2;->this$0:Lcom/squareup/register/tutorial/TutorialDialog;

    iput-object p2, p0, Lcom/squareup/register/tutorial/TutorialDialog$2;->val$presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    iput-object p3, p0, Lcom/squareup/register/tutorial/TutorialDialog$2;->val$prompt:Lcom/squareup/register/tutorial/TutorialDialog$Prompt;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 66
    iget-object p1, p0, Lcom/squareup/register/tutorial/TutorialDialog$2;->val$presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialDialog$2;->val$prompt:Lcom/squareup/register/tutorial/TutorialDialog$Prompt;

    sget-object v1, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;->SECONDARY:Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/register/tutorial/TutorialPresenter;->onDialogButtonTap(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;)V

    .line 67
    iget-object p1, p0, Lcom/squareup/register/tutorial/TutorialDialog$2;->this$0:Lcom/squareup/register/tutorial/TutorialDialog;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/TutorialDialog;->dismiss()V

    return-void
.end method
