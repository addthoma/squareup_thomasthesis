.class public final Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;
.super Lcom/squareup/tutorialv2/TutorialCreator;
.source "PlaceholderTutorial.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/PlaceholderTutorial;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Creator"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u000cH\u0016R\u001c\u0010\u0003\u001a\u0010\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;",
        "Lcom/squareup/tutorialv2/TutorialCreator;",
        "()V",
        "seeds",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/tutorialv2/TutorialSeed;",
        "kotlin.jvm.PlatformType",
        "ready",
        "",
        "autoStarted",
        "",
        "triggeredTutorial",
        "Lio/reactivex/Observable;",
        "pos-tutorials_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 47
    invoke-direct {p0}, Lcom/squareup/tutorialv2/TutorialCreator;-><init>()V

    .line 48
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    const-string v1, "BehaviorRelay.create<TutorialSeed>()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method


# virtual methods
.method public final ready(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 53
    sget-object p1, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->AUTO_STARTED:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->MERCHANT_STARTED:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    .line 54
    :goto_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator$ready$1;

    invoke-direct {v1, p1, p1}, Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator$ready$1;-><init>(Lcom/squareup/tutorialv2/TutorialSeed$Priority;Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public triggeredTutorial()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;"
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
