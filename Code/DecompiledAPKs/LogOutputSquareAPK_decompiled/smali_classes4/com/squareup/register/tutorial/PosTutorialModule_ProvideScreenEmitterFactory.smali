.class public final Lcom/squareup/register/tutorial/PosTutorialModule_ProvideScreenEmitterFactory;
.super Ljava/lang/Object;
.source "PosTutorialModule_ProvideScreenEmitterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/tutorial/TutorialScreenEmitter;",
        ">;"
    }
.end annotation


# instance fields
.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/register/tutorial/PosTutorialModule_ProvideScreenEmitterFactory;->mainContainerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/register/tutorial/PosTutorialModule_ProvideScreenEmitterFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)",
            "Lcom/squareup/register/tutorial/PosTutorialModule_ProvideScreenEmitterFactory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/register/tutorial/PosTutorialModule_ProvideScreenEmitterFactory;

    invoke-direct {v0, p0}, Lcom/squareup/register/tutorial/PosTutorialModule_ProvideScreenEmitterFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideScreenEmitter(Ldagger/Lazy;)Lcom/squareup/register/tutorial/TutorialScreenEmitter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)",
            "Lcom/squareup/register/tutorial/TutorialScreenEmitter;"
        }
    .end annotation

    .line 38
    invoke-static {p0}, Lcom/squareup/register/tutorial/PosTutorialModule;->provideScreenEmitter(Ldagger/Lazy;)Lcom/squareup/register/tutorial/TutorialScreenEmitter;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/register/tutorial/TutorialScreenEmitter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/register/tutorial/TutorialScreenEmitter;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialModule_ProvideScreenEmitterFactory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/register/tutorial/PosTutorialModule_ProvideScreenEmitterFactory;->provideScreenEmitter(Ldagger/Lazy;)Lcom/squareup/register/tutorial/TutorialScreenEmitter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/PosTutorialModule_ProvideScreenEmitterFactory;->get()Lcom/squareup/register/tutorial/TutorialScreenEmitter;

    move-result-object v0

    return-object v0
.end method
