.class public final Lcom/squareup/register/tutorial/link/FirstPaymentTutorialDeepLinksHandler;
.super Ljava/lang/Object;
.source "FirstPaymentTutorialDeepLinksHandler.kt"

# interfaces
.implements Lcom/squareup/deeplinks/DeepLinkHandler;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\u0006H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/register/tutorial/link/FirstPaymentTutorialDeepLinksHandler;",
        "Lcom/squareup/deeplinks/DeepLinkHandler;",
        "tutorialApi",
        "Lcom/squareup/register/tutorial/TutorialApi;",
        "(Lcom/squareup/register/tutorial/TutorialApi;)V",
        "handleExternal",
        "Lcom/squareup/deeplinks/DeepLinkResult;",
        "uri",
        "Landroid/net/Uri;",
        "handleFirstPaymentTutorialDeepLink",
        "pos-tutorials_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final tutorialApi:Lcom/squareup/register/tutorial/TutorialApi;


# direct methods
.method public constructor <init>(Lcom/squareup/register/tutorial/TutorialApi;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "tutorialApi"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/tutorial/link/FirstPaymentTutorialDeepLinksHandler;->tutorialApi:Lcom/squareup/register/tutorial/TutorialApi;

    return-void
.end method

.method private final handleFirstPaymentTutorialDeepLink()Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/squareup/register/tutorial/link/FirstPaymentTutorialDeepLinksHandler;->tutorialApi:Lcom/squareup/register/tutorial/TutorialApi;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialApi;->maybeStartFirstPaymentTutorialForDeepLink()Lcom/squareup/ui/main/HistoryFactory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 42
    new-instance v1, Lcom/squareup/deeplinks/DeepLinkResult;

    invoke-direct {v1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    goto :goto_0

    .line 44
    :cond_0
    new-instance v1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {v1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    :goto_0
    return-object v1
.end method


# virtual methods
.method public handleExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 3

    const-string v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x550eda8c

    if-eq v1, v2, :cond_3

    const v2, -0x4b89d351

    if-eq v1, v2, :cond_2

    const v2, 0x6fa0894b

    if-eq v1, v2, :cond_1

    goto :goto_1

    :cond_1
    const-string v1, "/payment-tutorial-test"

    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_0

    :cond_2
    const-string v1, "/payment-tutorial-nd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_0

    :cond_3
    const-string v1, "/payment-tutorial"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 20
    :goto_0
    invoke-direct {p0}, Lcom/squareup/register/tutorial/link/FirstPaymentTutorialDeepLinksHandler;->handleFirstPaymentTutorialDeepLink()Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    :cond_4
    :goto_1
    const-string v0, "root"

    const-string v1, "help"

    .line 24
    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->contains(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 25
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/startTutorial"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "name"

    .line 26
    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "paymentFlow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 27
    invoke-direct {p0}, Lcom/squareup/register/tutorial/link/FirstPaymentTutorialDeepLinksHandler;->handleFirstPaymentTutorialDeepLink()Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 32
    :cond_5
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1
.end method
