.class public final Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;
.super Ljava/lang/Object;
.source "LoyaltyTourScreenRunner.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001BO\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000e\u0008\u0001\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u000e\u0008\u0001\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u000e\u0008\u0001\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0013\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0016\u00a2\u0006\u0002\u0010\u0018J\u0012\u0010\u0019\u001a\u00020\u00162\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016J\u0008\u0010\u001c\u001a\u00020\u0016H\u0016J\u0012\u0010\u0012\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00140\u00130\u001dJ\u000e\u0010\u001e\u001a\u00020\u00162\u0006\u0010\u001f\u001a\u00020\u0010J\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u001dR\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000e\u001a\u0010\u0012\u000c\u0012\n \u0011*\u0004\u0018\u00010\u00100\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010\u0012\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0014 \u0011*\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u00130\u00130\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;",
        "Lmortar/Scoped;",
        "flow",
        "Lflow/Flow;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "enrollLoyaltyTutorialViewed",
        "Lcom/squareup/settings/LocalSetting;",
        "",
        "adjustPointsTutorialViewed",
        "redeemRewardsTutorialViewed",
        "badge",
        "Lcom/squareup/ui/help/HelpBadge;",
        "(Lflow/Flow;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/help/HelpBadge;)V",
        "currentTourType",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;",
        "kotlin.jvm.PlatformType",
        "pages",
        "",
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;",
        "closeLoyaltyTour",
        "",
        "unit",
        "(Lkotlin/Unit;)V",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "Lio/reactivex/Observable;",
        "postTutorialCore",
        "tourType",
        "pos-tutorials_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final adjustPointsTutorialViewed:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final badge:Lcom/squareup/ui/help/HelpBadge;

.field private final currentTourType:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;",
            ">;"
        }
    .end annotation
.end field

.field private final enrollLoyaltyTutorialViewed:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private final pages:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;",
            ">;>;"
        }
    .end annotation
.end field

.field private final redeemRewardsTutorialViewed:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/help/HelpBadge;)V
    .locals 1
    .param p3    # Lcom/squareup/settings/LocalSetting;
        .annotation runtime Lcom/squareup/settings/LoyaltyEnrollTutorialViewed;
        .end annotation
    .end param
    .param p4    # Lcom/squareup/settings/LocalSetting;
        .annotation runtime Lcom/squareup/settings/LoyaltyAdjustPointsTutorialViewed;
        .end annotation
    .end param
    .param p5    # Lcom/squareup/settings/LocalSetting;
        .annotation runtime Lcom/squareup/settings/LoyaltyRedeemRewardsTutorialViewed;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/ui/help/HelpBadge;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tutorialCore"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "enrollLoyaltyTutorialViewed"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "adjustPointsTutorialViewed"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "redeemRewardsTutorialViewed"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badge"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object p3, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->enrollLoyaltyTutorialViewed:Lcom/squareup/settings/LocalSetting;

    iput-object p4, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->adjustPointsTutorialViewed:Lcom/squareup/settings/LocalSetting;

    iput-object p5, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->redeemRewardsTutorialViewed:Lcom/squareup/settings/LocalSetting;

    iput-object p6, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->badge:Lcom/squareup/ui/help/HelpBadge;

    .line 47
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<Lis\u2026tyTourPage.ScreenData>>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->pages:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 48
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<LoyaltyTourType>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->currentTourType:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method


# virtual methods
.method public final closeLoyaltyTour(Lkotlin/Unit;)V
    .locals 1

    const-string v0, "unit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->currentTourType:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    goto :goto_0

    .line 79
    :cond_1
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Leaving LoyaltyTourScreen - Adjust Points"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    goto :goto_0

    .line 78
    :cond_2
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Leaving LoyaltyTourScreen - Redemption"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :cond_3
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Leaving LoyaltyTourScreen - Enroll"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 81
    :goto_0
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 51
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    if-eqz p1, :cond_3

    check-cast p1, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;

    .line 52
    invoke-virtual {p1}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;->getTourType()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;

    move-result-object p1

    sget-object v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 64
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->currentTourType:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;->ADJUST_POINTS:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;

    invoke-virtual {p1, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 65
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->adjustPointsTutorialViewed:Lcom/squareup/settings/LocalSetting;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 66
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->pages:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;->INSTANCE:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;->getAdjustPointsPages()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 59
    :cond_1
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->currentTourType:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;->REDEMPTION:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;

    invoke-virtual {p1, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 60
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->redeemRewardsTutorialViewed:Lcom/squareup/settings/LocalSetting;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 61
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->pages:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;->INSTANCE:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;->getRedeemRewardsPages()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 54
    :cond_2
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->currentTourType:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;->ENROLL:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;

    invoke-virtual {p1, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 55
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->pages:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;->INSTANCE:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;

    invoke-virtual {v1}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;->getEnrollmentPages()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 56
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->enrollLoyaltyTutorialViewed:Lcom/squareup/settings/LocalSetting;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 69
    :goto_0
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->badge:Lcom/squareup/ui/help/HelpBadge;

    invoke-interface {p1}, Lcom/squareup/ui/help/HelpBadge;->refresh()V

    return-void

    .line 51
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.register.tutorial.loyalty.LoyaltyTourScreen"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public final pages()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;",
            ">;>;"
        }
    .end annotation

    .line 97
    iget-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->pages:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "pages.distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final postTutorialCore(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;)V
    .locals 1

    const-string v0, "tourType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    sget-object v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 92
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Shown LoyaltyTourScreen - Adjust Points"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    goto :goto_0

    .line 91
    :cond_1
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Shown LoyaltyTourScreen - Redemption"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_2
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Shown LoyaltyTourScreen - Enroll"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public final tourType()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;",
            ">;"
        }
    .end annotation

    .line 85
    iget-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->currentTourType:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "currentTourType.distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
