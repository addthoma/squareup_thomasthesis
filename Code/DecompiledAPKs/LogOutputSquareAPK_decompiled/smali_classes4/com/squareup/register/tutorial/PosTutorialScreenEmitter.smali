.class public Lcom/squareup/register/tutorial/PosTutorialScreenEmitter;
.super Ljava/lang/Object;
.source "PosTutorialScreenEmitter.java"

# interfaces
.implements Lcom/squareup/register/tutorial/TutorialScreenEmitter;


# instance fields
.field private final mainContainerProvider:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldagger/Lazy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)V"
        }
    .end annotation

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/register/tutorial/PosTutorialScreenEmitter;->mainContainerProvider:Ldagger/Lazy;

    return-void
.end method


# virtual methods
.method public traversalCompleting()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialScreenEmitter;->mainContainerProvider:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->topOfTraversalCompleting()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
