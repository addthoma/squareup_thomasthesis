.class public interface abstract Lcom/squareup/register/tutorial/TutorialDialog$Prompt;
.super Ljava/lang/Object;
.source "TutorialDialog.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/TutorialDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Prompt"
.end annotation


# virtual methods
.method public abstract getContent(Landroid/content/Context;)Ljava/lang/CharSequence;
.end method

.method public abstract getLayoutResId()I
.end method

.method public abstract getPrimaryButton()I
.end method

.method public abstract getSecondaryButton()I
.end method

.method public abstract getTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
.end method
