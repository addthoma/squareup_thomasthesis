.class public interface abstract Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;
.super Ljava/lang/Object;
.source "DisplayRewardByCodeScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u000cJ\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0006\u001a\u00020\u0003H&J\u0008\u0010\u0007\u001a\u00020\u0003H&J\u0008\u0010\u0008\u001a\u00020\u0003H&J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH&\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;",
        "",
        "applyCouponFromCode",
        "",
        "coupon",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "closeDisplayRewardByCodeScreen",
        "closeDisplayRewardByCodeScreenOnRedeemReward",
        "closeDisplayRewardByCodeScreenOnSearchAgain",
        "onLookupRewardResult",
        "Lrx/Observable;",
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;",
        "LookupRewardResult",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract applyCouponFromCode(Lcom/squareup/protos/client/coupons/Coupon;)V
.end method

.method public abstract closeDisplayRewardByCodeScreen()V
.end method

.method public abstract closeDisplayRewardByCodeScreenOnRedeemReward()V
.end method

.method public abstract closeDisplayRewardByCodeScreenOnSearchAgain()V
.end method

.method public abstract onLookupRewardResult()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;",
            ">;"
        }
    .end annotation
.end method
