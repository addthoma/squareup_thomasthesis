.class public final Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;
.super Lcom/squareup/redeemrewards/RedeemRewardState;
.source "RedeemRewardState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/RedeemRewardState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AddingEligibleItemFromContact"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\u0019\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;",
        "Lcom/squareup/redeemrewards/RedeemRewardState;",
        "nextState",
        "Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;",
        "couponToAdd",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "props",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;",
        "(Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)V",
        "getCouponToAdd",
        "()Lcom/squareup/protos/client/coupons/Coupon;",
        "getNextState",
        "()Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;",
        "getProps",
        "()Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final couponToAdd:Lcom/squareup/protos/client/coupons/Coupon;

.field private final nextState:Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

.field private final props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact$Creator;

    invoke-direct {v0}, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact$Creator;-><init>()V

    sput-object v0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)V
    .locals 2

    const-string v0, "nextState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "couponToAdd"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "props"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;->getSearchTerm()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/redeemrewards/RedeemRewardState;-><init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->nextState:Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->couponToAdd:Lcom/squareup/protos/client/coupons/Coupon;

    iput-object p3, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;ILjava/lang/Object;)Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->nextState:Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->couponToAdd:Lcom/squareup/protos/client/coupons/Coupon;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->copy(Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->nextState:Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/client/coupons/Coupon;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->couponToAdd:Lcom/squareup/protos/client/coupons/Coupon;

    return-object v0
.end method

.method public final component3()Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    return-object v0
.end method

.method public final copy(Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;
    .locals 1

    const-string v0, "nextState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "couponToAdd"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "props"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;-><init>(Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->nextState:Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    iget-object v1, p1, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->nextState:Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->couponToAdd:Lcom/squareup/protos/client/coupons/Coupon;

    iget-object v1, p1, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->couponToAdd:Lcom/squareup/protos/client/coupons/Coupon;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    iget-object p1, p1, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCouponToAdd()Lcom/squareup/protos/client/coupons/Coupon;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->couponToAdd:Lcom/squareup/protos/client/coupons/Coupon;

    return-object v0
.end method

.method public final getNextState()Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->nextState:Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    return-object v0
.end method

.method public final getProps()Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->nextState:Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->couponToAdd:Lcom/squareup/protos/client/coupons/Coupon;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AddingEligibleItemFromContact(nextState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->nextState:Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", couponToAdd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->couponToAdd:Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", props="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->nextState:Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->couponToAdd:Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
