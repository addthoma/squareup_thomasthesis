.class public interface abstract Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;
.super Ljava/lang/Object;
.source "ChooseItemInCategoryScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0008\u0010\u0008\u001a\u00020\u0003H&J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH&\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;",
        "",
        "addSelectedRewardItemToCart",
        "",
        "itemThumbnail",
        "Landroid/widget/ImageView;",
        "selectedItem",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "cancelItemSelection",
        "getChooseItemScreenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract addSelectedRewardItemToCart(Landroid/widget/ImageView;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V
.end method

.method public abstract cancelItemSelection()V
.end method

.method public abstract getChooseItemScreenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;",
            ">;"
        }
    .end annotation
.end method
