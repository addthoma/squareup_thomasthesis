.class public final Lcom/squareup/redeemrewards/ViewRewardsWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "ViewRewardsWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/redeemrewards/ViewRewardsProps;",
        "Lcom/squareup/redeemrewards/ViewRewardsState;",
        "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViewRewardsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ViewRewardsWorkflow.kt\ncom/squareup/redeemrewards/ViewRewardsWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 4 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n+ 5 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 6 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 7 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 8 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,295:1\n122#1:308\n122#1:314\n122#1:320\n32#2,12:296\n149#3,5:309\n149#3,5:315\n149#3,5:321\n149#3,5:326\n67#4:331\n92#4,3:332\n1360#5:335\n1429#5,3:336\n85#6:339\n85#6:342\n240#7:340\n240#7:343\n276#8:341\n276#8:344\n*E\n*S KotlinDebug\n*F\n+ 1 ViewRewardsWorkflow.kt\ncom/squareup/redeemrewards/ViewRewardsWorkflow\n*L\n81#1:308\n106#1:314\n119#1:320\n64#1,12:296\n81#1,5:309\n106#1,5:315\n119#1,5:321\n122#1,5:326\n187#1:331\n187#1,3:332\n221#1:335\n221#1,3:336\n258#1:339\n279#1:342\n258#1:340\n279#1:343\n258#1:341\n279#1:344\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00c6\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002@\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0006`\t0\u0001B\'\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J-\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00142\u0006\u0010\u0016\u001a\u00020\u00172\n\u0008\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0002\u00a2\u0006\u0002\u0010\u001aJ\u001a\u0010\u001b\u001a\u00020\u00032\u0006\u0010\u001c\u001a\u00020\u00022\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016JP\u0010\u001f\u001a$\u0012\u0004\u0012\u00020 \u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020 `\t2\u0006\u0010\u001c\u001a\u00020\u00022\u001c\u0010!\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0014j\u0002`#0\"H\u0002J4\u0010$\u001a\u0008\u0012\u0004\u0012\u00020&0%2\u0006\u0010\'\u001a\u00020(2\u001c\u0010!\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0014j\u0002`#0\"H\u0002JR\u0010)\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0006`\t2\u0006\u0010\u001c\u001a\u00020\u00022\u0006\u0010*\u001a\u00020\u00032\u0012\u0010+\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040,H\u0016J\u0010\u0010-\u001a\u00020\u001e2\u0006\u0010*\u001a\u00020\u0003H\u0016J@\u0010.\u001a$\u0012\u0004\u0012\u00020 \u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020 `\t\"\n\u0008\u0000\u0010/\u0018\u0001*\u000200*\u0002H/H\u0082\u0008\u00a2\u0006\u0002\u00101J\"\u00102\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0014j\u0002`#03*\u000204H\u0002J\"\u00105\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0014j\u0002`#03*\u000206H\u0002J\u000e\u00107\u001a\u000208*\u0004\u0018\u000109H\u0002J\u001e\u0010:\u001a\u0008\u0012\u0004\u0012\u00020<0;*\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020(0\u0005H\u0002J\u000c\u0010=\u001a\u00020>*\u00020?H\u0002J*\u0010@\u001a\u00020A*\u00020\u00022\u001c\u0010!\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0014j\u0002`#0\"H\u0002R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006B"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/ViewRewardsWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/redeemrewards/ViewRewardsProps;",
        "Lcom/squareup/redeemrewards/ViewRewardsState;",
        "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "loyalty",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
        "couponDiscountFormatter",
        "Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;",
        "pointsTermsFormatter",
        "Lcom/squareup/loyalty/PointsTermsFormatter;",
        "(Lcom/squareup/util/Res;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/loyalty/PointsTermsFormatter;)V",
        "dismissErrorAction",
        "Lcom/squareup/workflow/WorkflowAction;",
        "applyCouponAction",
        "coupon",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "updatedCurrentPoints",
        "",
        "(Lcom/squareup/protos/client/coupons/Coupon;Ljava/lang/Integer;)Lcom/squareup/workflow/WorkflowAction;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "loadingScreen",
        "Lcom/squareup/container/PosLayering;",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/redeemrewards/Action0;",
        "onBackAction",
        "Lkotlin/Function0;",
        "",
        "isContactAddedToSale",
        "",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "asCard",
        "ScreenT",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "(Lcom/squareup/workflow/legacy/V2Screen;)Ljava/util/Map;",
        "redeemLoyaltyTier",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/redeemrewards/ViewRewardsState$RedeemingLoyaltyTier;",
        "returnLoyaltyTier",
        "Lcom/squareup/redeemrewards/ViewRewardsState$ReturningLoyaltyTier;",
        "toLoyaltyRewardSection",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;",
        "Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;",
        "toRewardRows",
        "",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
        "toRow",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
        "Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;",
        "toViewRewardsScreen",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final couponDiscountFormatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

.field private final dismissErrorAction:Lcom/squareup/workflow/WorkflowAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/ViewRewardsState;",
            "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
            ">;"
        }
    .end annotation
.end field

.field private final loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

.field private final pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/loyalty/PointsTermsFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyalty"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "couponDiscountFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pointsTermsFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iput-object p3, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->couponDiscountFormatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    iput-object p4, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    .line 290
    sget-object p1, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$dismissErrorAction$1;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsWorkflow$dismissErrorAction$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    const/4 p2, 0x0

    const/4 p3, 0x1

    invoke-static {p0, p2, p1, p3, p2}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->dismissErrorAction:Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method

.method public static final synthetic access$applyCouponAction(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/protos/client/coupons/Coupon;Ljava/lang/Integer;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 49
    invoke-direct {p0, p1, p2}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->applyCouponAction(Lcom/squareup/protos/client/coupons/Coupon;Ljava/lang/Integer;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getDismissErrorAction$p(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->dismissErrorAction:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method

.method public static final synthetic access$getPointsTermsFormatter$p(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;)Lcom/squareup/loyalty/PointsTermsFormatter;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    return-object p0
.end method

.method private final applyCouponAction(Lcom/squareup/protos/client/coupons/Coupon;Ljava/lang/Integer;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/ViewRewardsState;",
            "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
            ">;"
        }
    .end annotation

    .line 285
    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$applyCouponAction$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$applyCouponAction$1;-><init>(Lcom/squareup/protos/client/coupons/Coupon;Ljava/lang/Integer;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-static {p0, p1, v0, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method static synthetic applyCouponAction$default(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/protos/client/coupons/Coupon;Ljava/lang/Integer;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 284
    check-cast p2, Ljava/lang/Integer;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->applyCouponAction(Lcom/squareup/protos/client/coupons/Coupon;Ljava/lang/Integer;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final synthetic asCard(Lcom/squareup/workflow/legacy/V2Screen;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ScreenT::",
            "Lcom/squareup/workflow/legacy/V2Screen;",
            ">(TScreenT;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 327
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    const/4 v1, 0x4

    const-string v2, "ScreenT"

    .line 328
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Lcom/squareup/workflow/legacy/V2Screen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 329
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 327
    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 122
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {v0, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final loadingScreen(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/workflow/Sink;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/ViewRewardsProps;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/ViewRewardsState;",
            "+",
            "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
            ">;>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 114
    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsScreen;

    .line 115
    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->getTitle()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v1, Lcom/squareup/util/ViewString;

    .line 116
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->isContactAddedToSale()Z

    move-result v2

    invoke-direct {p0, v2, p2}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->onBackAction(ZLcom/squareup/workflow/Sink;)Lkotlin/jvm/functions/Function0;

    move-result-object p2

    .line 117
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->isContactAddedToSale()Z

    move-result p1

    .line 118
    sget-object v2, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Loading;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Loading;

    check-cast v2, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;

    .line 114
    invoke-direct {v0, v1, p2, p1, v2}, Lcom/squareup/redeemrewards/ViewRewardsScreen;-><init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;ZLcom/squareup/redeemrewards/ViewRewardsScreen$Type;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 322
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 323
    const-class p2, Lcom/squareup/redeemrewards/ViewRewardsScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 324
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 322
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 320
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final onBackAction(ZLcom/squareup/workflow/Sink;)Lkotlin/jvm/functions/Function0;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/ViewRewardsState;",
            "+",
            "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
            ">;>;)",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 181
    new-instance p1, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$onBackAction$1;

    invoke-direct {p1, p0, p2}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$onBackAction$1;-><init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/workflow/Sink;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    goto :goto_0

    .line 183
    :cond_0
    new-instance p1, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$onBackAction$2;

    invoke-direct {p1, p0, p2}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$onBackAction$2;-><init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/workflow/Sink;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    :goto_0
    return-object p1
.end method

.method private final redeemLoyaltyTier(Lcom/squareup/redeemrewards/ViewRewardsState$RedeemingLoyaltyTier;)Lcom/squareup/workflow/Worker;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/ViewRewardsState$RedeemingLoyaltyTier;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/ViewRewardsState;",
            "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
            ">;>;"
        }
    .end annotation

    .line 237
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    .line 238
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsState$RedeemingLoyaltyTier;->getCouponDefinitionToken()Ljava/lang/String;

    move-result-object v1

    .line 239
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsState$RedeemingLoyaltyTier;->getPhoneToken()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    .line 237
    invoke-static/range {v0 .. v5}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->redeemPoints$default(Lcom/squareup/loyalty/LoyaltyServiceHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    .line 241
    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1;

    invoke-direct {v0, p0}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1;-><init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "loyalty.redeemPoints(\n  \u2026  }\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 339
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 340
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 341
    const-class v0, Lcom/squareup/workflow/WorkflowAction;

    sget-object v1, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v2, Lcom/squareup/redeemrewards/ViewRewardsState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v3, Lcom/squareup/redeemrewards/ViewRewardsOutput;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method

.method private final returnLoyaltyTier(Lcom/squareup/redeemrewards/ViewRewardsState$ReturningLoyaltyTier;)Lcom/squareup/workflow/Worker;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/ViewRewardsState$ReturningLoyaltyTier;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/ViewRewardsState;",
            "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
            ">;>;"
        }
    .end annotation

    .line 262
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsState$ReturningLoyaltyTier;->getCouponToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->returnReward(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 263
    new-instance v1, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$returnLoyaltyTier$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$returnLoyaltyTier$1;-><init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/redeemrewards/ViewRewardsState$ReturningLoyaltyTier;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "loyalty.returnReward(cou\u2026  }\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 342
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$returnLoyaltyTier$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$returnLoyaltyTier$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 343
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 344
    const-class v0, Lcom/squareup/workflow/WorkflowAction;

    sget-object v1, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v2, Lcom/squareup/redeemrewards/ViewRewardsState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v3, Lcom/squareup/redeemrewards/ViewRewardsOutput;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method

.method private final toLoyaltyRewardSection(Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;)Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;
    .locals 4

    if-nez p1, :cond_0

    .line 198
    sget-object p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection$Hidden;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection$Hidden;

    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;

    goto :goto_2

    .line 200
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->getRewardTierStates()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    .line 202
    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    .line 203
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->getCurrentPoints()I

    move-result v2

    .line 204
    sget v3, Lcom/squareup/redeemrewards/impl/R$string;->loyalty_points_available:I

    .line 202
    invoke-virtual {v1, v2, v3}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(II)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_1

    .line 207
    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/redeemrewards/impl/R$string;->loyalty_rewards_no_available:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 209
    :cond_1
    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/redeemrewards/impl/R$string;->loyalty_rewards_available:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    if-eqz v0, :cond_2

    .line 213
    new-instance p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection$ShownNoTiers;

    invoke-direct {p1, v1, v2}, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection$ShownNoTiers;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;

    goto :goto_2

    .line 221
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->getRewardTierStates()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 335
    new-instance v0, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {p1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 336
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 337
    check-cast v3, Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;

    .line 221
    invoke-direct {p0, v3}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->toRow(Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;)Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 338
    :cond_3
    check-cast v0, Ljava/util/List;

    .line 218
    new-instance p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection$Shown;

    invoke-direct {p1, v1, v2, v0}, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection$Shown;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;

    :goto_2
    return-object p1
.end method

.method private final toRewardRows(Ljava/util/Map;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
            ">;"
        }
    .end annotation

    .line 331
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 332
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 333
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 188
    new-instance v3, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;

    .line 189
    iget-object v4, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->couponDiscountFormatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    invoke-virtual {v4, v2}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->formatName(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 190
    iget-object v5, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->couponDiscountFormatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    invoke-virtual {v5, v2}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->formatCouponExpirationDate(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    goto :goto_1

    :cond_0
    const-string v5, ""

    .line 188
    :goto_1
    invoke-direct {v3, v4, v5, v1, v2}, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;-><init>(Ljava/lang/String;Ljava/lang/String;ZLcom/squareup/protos/client/coupons/Coupon;)V

    .line 193
    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 334
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final toRow(Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;)Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;
    .locals 8

    .line 227
    new-instance v7, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;

    .line 228
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;->getRewardTier()Lcom/squareup/server/account/protos/RewardTier;

    move-result-object v0

    iget-object v1, v0, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v0, "rewardTier.coupon_definition_token!!"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;->getRewardTier()Lcom/squareup/server/account/protos/RewardTier;

    move-result-object v0

    iget-object v2, v0, Lcom/squareup/server/account/protos/RewardTier;->name:Ljava/lang/String;

    if-nez v2, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    const-string v0, "rewardTier.name!!"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;->getRewardTier()Lcom/squareup/server/account/protos/RewardTier;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    const-string v4, "rewardTier.points"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(J)Ljava/lang/String;

    move-result-object v3

    const-string v0, "pointsTermsFormatter.points(rewardTier.points)"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 231
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;->getRewardTier()Lcom/squareup/server/account/protos/RewardTier;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v4, v4

    .line 232
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;->getAppliedCouponTokens()Ljava/util/List;

    move-result-object v5

    .line 233
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;->getEnabled()Z

    move-result v6

    move-object v0, v7

    .line 227
    invoke-direct/range {v0 .. v6}, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;Z)V

    return-object v7
.end method

.method private final toViewRewardsScreen(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/workflow/Sink;)Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/ViewRewardsProps;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/ViewRewardsState;",
            "+",
            "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
            ">;>;)",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;"
        }
    .end annotation

    .line 129
    new-instance v9, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;

    .line 130
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->isContactAddedToSale()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/redeemrewards/impl/R$string;->redeem_rewards_remove_customer:I

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    goto :goto_0

    .line 133
    :cond_0
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/redeemrewards/impl/R$string;->redeem_rewards_add_customer:I

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    .line 130
    :goto_0
    move-object v1, v0

    check-cast v1, Lcom/squareup/util/ViewString;

    .line 135
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->isContactAddedToSale()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$1;-><init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/workflow/Sink;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    goto :goto_1

    .line 138
    :cond_1
    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$2;

    invoke-direct {v0, p0, p2}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$2;-><init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/workflow/Sink;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    :goto_1
    move-object v5, v0

    .line 141
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->getCoupons()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->toRewardRows(Ljava/util/Map;)Ljava/util/List;

    move-result-object v2

    .line 142
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->getCoupons()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    .line 143
    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3;

    invoke-direct {v0, p0, p2}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3;-><init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/workflow/Sink;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 151
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->getLoyaltySectionInfo()Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->toLoyaltyRewardSection(Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;)Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;

    move-result-object v4

    .line 152
    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$4;-><init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/workflow/Sink;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 163
    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$5;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$5;-><init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/workflow/Sink;)V

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function1;

    move-object v0, v9

    .line 129
    invoke-direct/range {v0 .. v8}, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;-><init>(Lcom/squareup/util/ViewString;Ljava/util/List;ZLcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-object v9
.end method


# virtual methods
.method public initialState(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/redeemrewards/ViewRewardsState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 296
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 301
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 302
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 303
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 304
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 305
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 306
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 307
    :cond_3
    check-cast v1, Lcom/squareup/redeemrewards/ViewRewardsState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 64
    :cond_4
    sget-object p1, Lcom/squareup/redeemrewards/ViewRewardsState$ShowRewards;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsState$ShowRewards;

    move-object v1, p1

    check-cast v1, Lcom/squareup/redeemrewards/ViewRewardsState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->initialState(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/redeemrewards/ViewRewardsState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsProps;

    check-cast p2, Lcom/squareup/redeemrewards/ViewRewardsState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->render(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/redeemrewards/ViewRewardsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/redeemrewards/ViewRewardsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/ViewRewardsProps;",
            "Lcom/squareup/redeemrewards/ViewRewardsState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/redeemrewards/ViewRewardsState;",
            "-",
            "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 75
    sget-object v1, Lcom/squareup/redeemrewards/ViewRewardsState$ShowRewards;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsState$ShowRewards;

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const-string v2, ""

    if-eqz v1, :cond_0

    .line 76
    new-instance p2, Lcom/squareup/redeemrewards/ViewRewardsScreen;

    .line 77
    new-instance p3, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->getTitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {p3, v1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast p3, Lcom/squareup/util/ViewString;

    .line 78
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->isContactAddedToSale()Z

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->onBackAction(ZLcom/squareup/workflow/Sink;)Lkotlin/jvm/functions/Function0;

    move-result-object v1

    .line 79
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->isContactAddedToSale()Z

    move-result v3

    .line 80
    invoke-direct {p0, p1, v0}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->toViewRewardsScreen(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/workflow/Sink;)Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;

    move-result-object p1

    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;

    .line 76
    invoke-direct {p2, p3, v1, v3, p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen;-><init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;ZLcom/squareup/redeemrewards/ViewRewardsScreen$Type;)V

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 310
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 311
    const-class p3, Lcom/squareup/redeemrewards/ViewRewardsScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 312
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 310
    invoke-direct {p1, p3, p2, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 308
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 84
    :cond_0
    instance-of v1, p2, Lcom/squareup/redeemrewards/ViewRewardsState$RedeemingLoyaltyTier;

    if-eqz v1, :cond_1

    .line 85
    move-object v1, p2

    check-cast v1, Lcom/squareup/redeemrewards/ViewRewardsState$RedeemingLoyaltyTier;

    invoke-direct {p0, v1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->redeemLoyaltyTier(Lcom/squareup/redeemrewards/ViewRewardsState$RedeemingLoyaltyTier;)Lcom/squareup/workflow/Worker;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/redeemrewards/ViewRewardsState;->toString()Ljava/lang/String;

    move-result-object p2

    sget-object v2, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$render$1;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsWorkflow$render$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v1, p2, v2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 89
    invoke-direct {p0, p1, v0}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->loadingScreen(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/workflow/Sink;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 92
    :cond_1
    instance-of v1, p2, Lcom/squareup/redeemrewards/ViewRewardsState$ReturningLoyaltyTier;

    if-eqz v1, :cond_2

    .line 93
    move-object v1, p2

    check-cast v1, Lcom/squareup/redeemrewards/ViewRewardsState$ReturningLoyaltyTier;

    invoke-direct {p0, v1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->returnLoyaltyTier(Lcom/squareup/redeemrewards/ViewRewardsState$ReturningLoyaltyTier;)Lcom/squareup/workflow/Worker;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/redeemrewards/ViewRewardsState;->toString()Ljava/lang/String;

    move-result-object p2

    sget-object v2, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$render$2;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsWorkflow$render$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v1, p2, v2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 97
    invoke-direct {p0, p1, v0}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->loadingScreen(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/workflow/Sink;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 100
    :cond_2
    instance-of p3, p2, Lcom/squareup/redeemrewards/ViewRewardsState$ErrorRedeemingLoyaltyTier;

    if-eqz p3, :cond_3

    .line 101
    new-instance p3, Lcom/squareup/redeemrewards/RedeemRewardsErrorScreen;

    .line 102
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->getTitle()Ljava/lang/String;

    move-result-object p1

    .line 103
    check-cast p2, Lcom/squareup/redeemrewards/ViewRewardsState$ErrorRedeemingLoyaltyTier;

    invoke-virtual {p2}, Lcom/squareup/redeemrewards/ViewRewardsState$ErrorRedeemingLoyaltyTier;->getErrorMessage()Ljava/lang/String;

    move-result-object p2

    const/4 v1, 0x0

    .line 105
    new-instance v3, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$render$3;

    invoke-direct {v3, p0, v0}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$render$3;-><init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/workflow/Sink;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 101
    invoke-direct {p3, p1, p2, v1, v3}, Lcom/squareup/redeemrewards/RedeemRewardsErrorScreen;-><init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function0;)V

    check-cast p3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 316
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 317
    const-class p2, Lcom/squareup/redeemrewards/RedeemRewardsErrorScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    invoke-static {p2, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 318
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 316
    invoke-direct {p1, p2, p3, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 314
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/redeemrewards/ViewRewardsState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsState;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->snapshotState(Lcom/squareup/redeemrewards/ViewRewardsState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
