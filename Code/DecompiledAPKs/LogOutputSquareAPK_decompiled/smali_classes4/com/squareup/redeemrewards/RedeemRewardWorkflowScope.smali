.class public final Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "RedeemRewardWorkflowScope.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope$ParentComponent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRedeemRewardWorkflowScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RedeemRewardWorkflowScope.kt\ncom/squareup/redeemrewards/RedeemRewardWorkflowScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,28:1\n35#2:29\n*E\n*S KotlinDebug\n*F\n+ 1 RedeemRewardWorkflowScope.kt\ncom/squareup/redeemrewards/RedeemRewardWorkflowScope\n*L\n14#1:29\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0001\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016R\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;",
        "ParentComponent",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 9
    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope;

    invoke-direct {v0}, Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope;-><init>()V

    sput-object v0, Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope;

    .line 26
    sget-object v0, Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "forSingleton(RedeemRewardWorkflowScope)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    const-class v0, Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 14
    check-cast v0, Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope$ParentComponent;

    .line 15
    invoke-interface {v0}, Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope$ParentComponent;->redeemRewardWorkflowRunner()Lcom/squareup/redeemrewards/RedeemRewardWorkflowRunner;

    move-result-object v0

    .line 17
    invoke-super {p0, p1}, Lcom/squareup/ui/main/RegisterTreeKey;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object p1

    const-string v1, "it"

    .line 18
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/redeemrewards/RedeemRewardWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    const-string v0, "super.buildScope(parentS\u2026er.registerServices(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public getParentKey()Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope;->getParentKey()Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;

    move-result-object v0

    return-object v0
.end method
