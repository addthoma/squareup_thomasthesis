.class final Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionBarAction$1;
.super Ljava/lang/Object;
.source "RedeemRewardsScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;",
        "c",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionBarAction$1;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;
    .locals 2

    .line 220
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance p1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction$ChooseCustomerToAdd;

    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionBarAction$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionBarAction$1$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionBarAction$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, v0}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction$ChooseCustomerToAdd;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;

    goto :goto_0

    .line 221
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionBarAction$1;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-static {v1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->access$getHoldsCustomer$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/payment/crm/HoldsCustomer;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/payment/crm/HoldsCustomer;->getCustomerId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction$RemoveCustomer;

    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionBarAction$1$2;

    invoke-direct {v0, p0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionBarAction$1$2;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionBarAction$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, v0}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction$RemoveCustomer;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;

    goto :goto_0

    .line 222
    :cond_1
    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction$AddThisCustomer;

    new-instance v1, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionBarAction$1$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionBarAction$1$3;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionBarAction$1;Lcom/squareup/protos/client/rolodex/Contact;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction$AddThisCustomer;-><init>(Lkotlin/jvm/functions/Function0;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionBarAction$1;->call(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;

    move-result-object p1

    return-object p1
.end method
