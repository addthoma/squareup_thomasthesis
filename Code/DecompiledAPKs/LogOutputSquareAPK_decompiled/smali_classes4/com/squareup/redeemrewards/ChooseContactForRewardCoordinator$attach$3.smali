.class final Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;
.super Lkotlin/jvm/internal/Lambda;
.source "ChooseContactForRewardCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;",
        "+",
        "Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012F\u0010\u0002\u001aB\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    iput-object p2, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;->invoke(Lkotlin/Pair;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;",
            "Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    if-eqz p1, :cond_1

    .line 112
    sget-object v0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_2

    .line 127
    :pswitch_0
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    invoke-static {v0}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->access$getProgress$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Landroid/widget/ProgressBar;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 128
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    invoke-static {v0}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->access$getContactList$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 129
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    invoke-static {v0}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->access$getMessage$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Lcom/squareup/widgets/MessageView;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;->$view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 130
    sget-object v2, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_FAILED_TO_LOAD:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    if-ne p1, v2, :cond_0

    .line 131
    sget p1, Lcom/squareup/crm/R$string;->crm_failed_to_load_customers:I

    goto :goto_0

    .line 133
    :cond_0
    sget p1, Lcom/squareup/crm/R$string;->crm_contact_search_empty:I

    .line 129
    :goto_0
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    invoke-static {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->access$getMessage$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Lcom/squareup/widgets/MessageView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 136
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    invoke-static {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->access$getSearchBox$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Lcom/squareup/ui/XableEditText;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_1

    .line 120
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    invoke-static {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->access$getProgress$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Landroid/widget/ProgressBar;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 121
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    invoke-static {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->access$getContactList$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 122
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    invoke-static {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->access$getMessage$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Lcom/squareup/widgets/MessageView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 123
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    invoke-static {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->access$getSearchBox$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Lcom/squareup/ui/XableEditText;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_1

    .line 114
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    invoke-static {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->access$getProgress$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Landroid/widget/ProgressBar;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 115
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    invoke-static {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->access$getContactList$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 116
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    invoke-static {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->access$getSearchBox$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Lcom/squareup/ui/XableEditText;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    :goto_1
    return-void

    .line 139
    :cond_1
    :goto_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected visual state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
