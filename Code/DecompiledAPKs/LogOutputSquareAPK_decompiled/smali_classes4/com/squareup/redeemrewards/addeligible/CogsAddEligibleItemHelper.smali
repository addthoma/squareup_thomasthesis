.class public final Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;
.super Ljava/lang/Object;
.source "CogsAddEligibleItemHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J*\u0010\u000c\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000f0\u000ej\u0008\u0012\u0004\u0012\u00020\u000f`\u00100\r2\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000bJ*\u0010\u0013\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000f0\u000ej\u0008\u0012\u0004\u0012\u00020\u000f`\u00100\r2\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u0007\u001a&\u0012\u000c\u0012\n \n*\u0004\u0018\u00010\t0\t \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u00010\t0\t\u0018\u00010\u000b0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;",
        "",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "accountSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/AccountStatusSettings;)V",
        "supportedCatalogTypes",
        "",
        "Lcom/squareup/api/items/Item$Type;",
        "kotlin.jvm.PlatformType",
        "",
        "getAllItemsInCategory",
        "Lio/reactivex/Single;",
        "Ljava/util/HashSet;",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "Lkotlin/collections/HashSet;",
        "categoryIds",
        "",
        "getAllItemsInVariation",
        "variationIds",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final supportedCatalogTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cogs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;->cogs:Lcom/squareup/cogs/Cogs;

    const/4 p1, 0x1

    new-array p1, p1, [Lcom/squareup/api/items/Item$Type;

    .line 18
    sget-object v0, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    const/4 v1, 0x0

    aput-object v0, p1, v1

    invoke-virtual {p2, p1}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;->supportedCatalogTypes:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$getSupportedCatalogTypes$p(Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;)Ljava/util/List;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;->supportedCatalogTypes:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public final getAllItemsInCategory(Ljava/util/List;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/Single<",
            "Ljava/util/HashSet<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;>;"
        }
    .end annotation

    const-string v0, "categoryIds"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper$getAllItemsInCategory$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper$getAllItemsInCategory$1;-><init>(Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;Ljava/util/List;)V

    check-cast v1, Lcom/squareup/shared/catalog/CatalogTask;

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p1

    const-string v0, "cogs.asSingle { cogsLoca\u2026      matchingItems\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public final getAllItemsInVariation(Ljava/util/List;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/Single<",
            "Ljava/util/HashSet<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;>;"
        }
    .end annotation

    const-string/jumbo v0, "variationIds"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper$getAllItemsInVariation$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper$getAllItemsInVariation$1;-><init>(Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;Ljava/util/List;)V

    check-cast v1, Lcom/squareup/shared/catalog/CatalogTask;

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p1

    const-string v0, "cogs.asSingle { cogsLoca\u2026ingle matchingItems\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
