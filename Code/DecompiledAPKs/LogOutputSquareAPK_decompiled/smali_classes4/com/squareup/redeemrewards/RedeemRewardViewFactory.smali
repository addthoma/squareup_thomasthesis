.class public final Lcom/squareup/redeemrewards/RedeemRewardViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RedeemRewardViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "chooseContactForRewardFactory",
        "Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$Factory;",
        "viewRewardsCoordinatorFactory",
        "Lcom/squareup/redeemrewards/ViewRewardsCoordinator$Factory;",
        "(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$Factory;Lcom/squareup/redeemrewards/ViewRewardsCoordinator$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$Factory;Lcom/squareup/redeemrewards/ViewRewardsCoordinator$Factory;)V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "chooseContactForRewardFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewRewardsCoordinatorFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 24
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 25
    sget-object v2, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;->Companion:Lcom/squareup/redeemrewards/ChooseContactForRewardScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen$Companion;->getKEY$impl_release()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 26
    sget v3, Lcom/squareup/redeemrewards/impl/R$layout;->redeem_rewards_choose_contact:I

    .line 27
    new-instance v4, Lcom/squareup/redeemrewards/RedeemRewardViewFactory$1;

    invoke-direct {v4, p1}, Lcom/squareup/redeemrewards/RedeemRewardViewFactory$1;-><init>(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$Factory;)V

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 24
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 30
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 31
    sget-object p1, Lcom/squareup/redeemrewards/ViewRewardsScreen;->Companion:Lcom/squareup/redeemrewards/ViewRewardsScreen$Companion;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$Companion;->getKEY$impl_release()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 32
    sget v4, Lcom/squareup/redeemrewards/impl/R$layout;->view_rewards_screen:I

    .line 33
    new-instance p1, Lcom/squareup/redeemrewards/RedeemRewardViewFactory$2;

    invoke-direct {p1, p2}, Lcom/squareup/redeemrewards/RedeemRewardViewFactory$2;-><init>(Lcom/squareup/redeemrewards/ViewRewardsCoordinator$Factory;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    .line 30
    invoke-static/range {v2 .. v9}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v0, p2

    .line 36
    new-instance p1, Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner$Binding;

    invoke-direct {p1}, Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner$Binding;-><init>()V

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p2, 0x2

    aput-object p1, v0, p2

    .line 23
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
