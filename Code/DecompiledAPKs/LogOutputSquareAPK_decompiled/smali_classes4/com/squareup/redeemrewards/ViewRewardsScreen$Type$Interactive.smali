.class public final Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;
.super Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;
.source "ViewRewardsScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Interactive"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u001b\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001Bu\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\r0\u000f\u0012\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0\u000f\u0012\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0\u000f\u00a2\u0006\u0002\u0010\u0013J\t\u0010\"\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\t\u0010$\u001a\u00020\u0008H\u00c6\u0003J\t\u0010%\u001a\u00020\nH\u00c6\u0003J\u000f\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cH\u00c6\u0003J\u0015\u0010\'\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\r0\u000fH\u00c6\u0003J\u0015\u0010(\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0\u000fH\u00c6\u0003J\u0015\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0\u000fH\u00c6\u0003J\u0089\u0001\u0010*\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u000e\u0008\u0002\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0014\u0008\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\r0\u000f2\u0014\u0008\u0002\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0\u000f2\u0014\u0008\u0002\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0\u000fH\u00c6\u0001J\u0013\u0010+\u001a\u00020\u00082\u0008\u0010,\u001a\u0004\u0018\u00010-H\u00d6\u0003J\t\u0010.\u001a\u00020/H\u00d6\u0001J\t\u00100\u001a\u000201H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0017\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u001d\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\r0\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u001d\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001dR\u001d\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u001dR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010!\u00a8\u00062"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;",
        "actionBarActionText",
        "Lcom/squareup/util/ViewString;",
        "coupons",
        "",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
        "showCouponEmptyLabel",
        "",
        "loyaltyRewardSection",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;",
        "onActionBarAction",
        "Lkotlin/Function0;",
        "",
        "onCouponRowClicked",
        "Lkotlin/Function1;",
        "onRewardRowRedeemed",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
        "onRewardRowRemoved",
        "(Lcom/squareup/util/ViewString;Ljava/util/List;ZLcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V",
        "getActionBarActionText",
        "()Lcom/squareup/util/ViewString;",
        "getCoupons",
        "()Ljava/util/List;",
        "getLoyaltyRewardSection",
        "()Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;",
        "getOnActionBarAction",
        "()Lkotlin/jvm/functions/Function0;",
        "getOnCouponRowClicked",
        "()Lkotlin/jvm/functions/Function1;",
        "getOnRewardRowRedeemed",
        "getOnRewardRowRemoved",
        "getShowCouponEmptyLabel",
        "()Z",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBarActionText:Lcom/squareup/util/ViewString;

.field private final coupons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyRewardSection:Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;

.field private final onActionBarAction:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onCouponRowClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onRewardRowRedeemed:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onRewardRowRemoved:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final showCouponEmptyLabel:Z


# direct methods
.method public constructor <init>(Lcom/squareup/util/ViewString;Ljava/util/List;ZLcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ViewString;",
            "Ljava/util/List<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
            ">;Z",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "actionBarActionText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "coupons"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyRewardSection"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onActionBarAction"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCouponRowClicked"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onRewardRowRedeemed"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onRewardRowRemoved"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, v0}, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->actionBarActionText:Lcom/squareup/util/ViewString;

    iput-object p2, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->coupons:Ljava/util/List;

    iput-boolean p3, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->showCouponEmptyLabel:Z

    iput-object p4, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->loyaltyRewardSection:Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;

    iput-object p5, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onActionBarAction:Lkotlin/jvm/functions/Function0;

    iput-object p6, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onCouponRowClicked:Lkotlin/jvm/functions/Function1;

    iput-object p7, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onRewardRowRedeemed:Lkotlin/jvm/functions/Function1;

    iput-object p8, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onRewardRowRemoved:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;Lcom/squareup/util/ViewString;Ljava/util/List;ZLcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->actionBarActionText:Lcom/squareup/util/ViewString;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->coupons:Ljava/util/List;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->showCouponEmptyLabel:Z

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->loyaltyRewardSection:Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onActionBarAction:Lkotlin/jvm/functions/Function0;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onCouponRowClicked:Lkotlin/jvm/functions/Function1;

    goto :goto_5

    :cond_5
    move-object v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onRewardRowRedeemed:Lkotlin/jvm/functions/Function1;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onRewardRowRemoved:Lkotlin/jvm/functions/Function1;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move-object p1, v2

    move-object p2, v3

    move p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->copy(Lcom/squareup/util/ViewString;Ljava/util/List;ZLcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/util/ViewString;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->actionBarActionText:Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->coupons:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->showCouponEmptyLabel:Z

    return v0
.end method

.method public final component4()Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->loyaltyRewardSection:Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;

    return-object v0
.end method

.method public final component5()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onActionBarAction:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component6()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onCouponRowClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component7()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onRewardRowRedeemed:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component8()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onRewardRowRemoved:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Lcom/squareup/util/ViewString;Ljava/util/List;ZLcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ViewString;",
            "Ljava/util/List<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
            ">;Z",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;"
        }
    .end annotation

    const-string v0, "actionBarActionText"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "coupons"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyRewardSection"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onActionBarAction"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCouponRowClicked"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onRewardRowRedeemed"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onRewardRowRemoved"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;

    move-object v1, v0

    move v4, p3

    invoke-direct/range {v1 .. v9}, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;-><init>(Lcom/squareup/util/ViewString;Ljava/util/List;ZLcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->actionBarActionText:Lcom/squareup/util/ViewString;

    iget-object v1, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->actionBarActionText:Lcom/squareup/util/ViewString;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->coupons:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->coupons:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->showCouponEmptyLabel:Z

    iget-boolean v1, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->showCouponEmptyLabel:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->loyaltyRewardSection:Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;

    iget-object v1, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->loyaltyRewardSection:Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onActionBarAction:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onActionBarAction:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onCouponRowClicked:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onCouponRowClicked:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onRewardRowRedeemed:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onRewardRowRedeemed:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onRewardRowRemoved:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onRewardRowRemoved:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getActionBarActionText()Lcom/squareup/util/ViewString;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->actionBarActionText:Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public final getCoupons()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
            ">;"
        }
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->coupons:Ljava/util/List;

    return-object v0
.end method

.method public final getLoyaltyRewardSection()Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->loyaltyRewardSection:Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;

    return-object v0
.end method

.method public final getOnActionBarAction()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onActionBarAction:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnCouponRowClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onCouponRowClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnRewardRowRedeemed()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onRewardRowRedeemed:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnRewardRowRemoved()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onRewardRowRemoved:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getShowCouponEmptyLabel()Z
    .locals 1

    .line 21
    iget-boolean v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->showCouponEmptyLabel:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->actionBarActionText:Lcom/squareup/util/ViewString;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->coupons:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->showCouponEmptyLabel:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->loyaltyRewardSection:Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onActionBarAction:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onCouponRowClicked:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onRewardRowRedeemed:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onRewardRowRemoved:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Interactive(actionBarActionText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->actionBarActionText:Lcom/squareup/util/ViewString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", coupons="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->coupons:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showCouponEmptyLabel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->showCouponEmptyLabel:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", loyaltyRewardSection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->loyaltyRewardSection:Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onActionBarAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onActionBarAction:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onCouponRowClicked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onCouponRowClicked:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onRewardRowRedeemed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onRewardRowRedeemed:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onRewardRowRemoved="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->onRewardRowRemoved:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
