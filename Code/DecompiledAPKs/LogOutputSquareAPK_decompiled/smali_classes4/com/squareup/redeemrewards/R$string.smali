.class public final Lcom/squareup/redeemrewards/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final applied_reward_to_cart:I = 0x7f1200d4

.field public static final apply_reward_to_cart:I = 0x7f1200db

.field public static final coupon_redeem_reward_subtitle:I = 0x7f1205a9

.field public static final coupon_redeem_rewards:I = 0x7f1205aa

.field public static final coupon_search_by_code:I = 0x7f1205ab

.field public static final coupon_search_by_code_hint:I = 0x7f1205ac

.field public static final coupon_search_by_code_hint_alphanumeric:I = 0x7f1205ad

.field public static final coupon_search_failed:I = 0x7f1205ae

.field public static final coupon_search_reward_conflicting_msg:I = 0x7f1205b1

.field public static final coupon_search_reward_conflicting_title:I = 0x7f1205b2

.field public static final coupon_search_reward_not_found:I = 0x7f1205b4

.field public static final coupon_search_reward_not_found_helper:I = 0x7f1205b5

.field public static final coupon_search_try_again:I = 0x7f1205b8

.field public static final points_empty_state_title:I = 0x7f12144e

.field public static final points_empty_state_unenrolled:I = 0x7f12144f

.field public static final points_empty_state_unsubscribed:I = 0x7f121450

.field public static final points_redemption_error_generic:I = 0x7f121454

.field public static final points_total_amount:I = 0x7f121455

.field public static final redeem_rewards_add_customer:I = 0x7f1215fc

.field public static final redeem_rewards_cart_points_title:I = 0x7f1215fd

.field public static final redeem_rewards_cart_points_value:I = 0x7f1215fe

.field public static final redeem_rewards_customer_points_title:I = 0x7f121601

.field public static final redeem_rewards_item_in_category_title:I = 0x7f121604

.field public static final redeem_rewards_item_popup_body:I = 0x7f121606

.field public static final redeem_rewards_item_popup_body_category:I = 0x7f121607

.field public static final redeem_rewards_item_popup_negative_button:I = 0x7f12160a

.field public static final redeem_rewards_item_popup_positive_button:I = 0x7f12160b

.field public static final redeem_rewards_item_popup_title:I = 0x7f12160c

.field public static final redeem_rewards_points_message_add:I = 0x7f12160e

.field public static final redeem_rewards_points_message_enroll:I = 0x7f12160f

.field public static final redeem_rewards_remove_customer:I = 0x7f121611

.field public static final redeem_rewards_title_coupons:I = 0x7f121612

.field public static final redeem_rewards_title_rewards:I = 0x7f121613

.field public static final redeem_rewards_use_code:I = 0x7f121615


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
