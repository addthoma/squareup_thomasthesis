.class public final Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "NoRewardCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNoRewardCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NoRewardCoordinator.kt\ncom/squareup/redeemrewards/bycode/NoRewardCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,60:1\n1103#2,7:61\n*E\n*S KotlinDebug\n*F\n+ 1 NoRewardCoordinator.kt\ncom/squareup/redeemrewards/bycode/NoRewardCoordinator\n*L\n57#1,7:61\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B)\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0014\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0005H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/redeemrewards/bycode/NoRewardScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "emptyView",
        "Lcom/squareup/ui/EmptyView;",
        "searchAgain",
        "Lcom/squareup/noho/NohoButton;",
        "view",
        "Landroid/view/View;",
        "attach",
        "",
        "bindViews",
        "onScreen",
        "screen",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private emptyView:Lcom/squareup/ui/EmptyView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private searchAgain:Lcom/squareup/noho/NohoButton;

.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;Lcom/squareup/redeemrewards/bycode/NoRewardScreen;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;->onScreen(Lcom/squareup/redeemrewards/bycode/NoRewardScreen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 38
    iput-object p1, p0, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;->view:Landroid/view/View;

    .line 39
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 40
    sget v0, Lcom/squareup/redeemrewards/bycode/impl/R$id;->empty_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/EmptyView;

    iput-object v0, p0, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    .line 41
    sget v0, Lcom/squareup/redeemrewards/bycode/impl/R$id;->crm_search_reward_again:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;->searchAgain:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method private final onScreen(Lcom/squareup/redeemrewards/bycode/NoRewardScreen;)V
    .locals 4

    .line 50
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 47
    :cond_0
    iget-object v2, p0, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 48
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator$onScreen$1;

    invoke-direct {v3, p1}, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator$onScreen$1;-><init>(Lcom/squareup/redeemrewards/bycode/NoRewardScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 49
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/redeemrewards/bycode/impl/R$string;->enter_reward_code_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 50
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;->view:Landroid/view/View;

    if-nez v0, :cond_2

    const-string/jumbo v1, "view"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v1, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator$onScreen$2;

    invoke-direct {v1, p1}, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator$onScreen$2;-><init>(Lcom/squareup/redeemrewards/bycode/NoRewardScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    const-string v1, "emptyView"

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/EmptyView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/bycode/NoRewardScreen;->getWarningTitle()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/EmptyView;->setTitle(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/bycode/NoRewardScreen;->getWarningSubtitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/EmptyView;->setMessage(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;->searchAgain:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_6

    const-string v1, "searchAgain"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast v0, Landroid/view/View;

    .line 61
    new-instance v1, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator$onScreen$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator$onScreen$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/redeemrewards/bycode/NoRewardScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;->bindViews(Landroid/view/View;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;->screens:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator$attach$1;->INSTANCE:Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator$attach$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens.map { it.unwrapV2Screen }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator$attach$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator$attach$2;-><init>(Lcom/squareup/redeemrewards/bycode/NoRewardCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
