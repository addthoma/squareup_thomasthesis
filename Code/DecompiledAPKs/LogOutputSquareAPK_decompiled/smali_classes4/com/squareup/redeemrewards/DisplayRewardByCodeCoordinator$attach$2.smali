.class final Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;
.super Lkotlin/jvm/internal/Lambda;
.source "DisplayRewardByCodeCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "result",
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $animate:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic $r:Landroid/content/res/Resources;

.field final synthetic $shortAnimTime:I

.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;Landroid/view/View;Landroid/content/res/Resources;Ljava/util/concurrent/atomic/AtomicBoolean;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->this$0:Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;

    iput-object p2, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$view:Landroid/view/View;

    iput-object p3, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$r:Landroid/content/res/Resources;

    iput-object p4, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$animate:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput p5, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$shortAnimTime:I

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->invoke(Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;)V
    .locals 5

    .line 48
    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$view:Landroid/view/View;

    sget v1, Lcom/squareup/redeemrewards/R$id;->crm_progress_bar:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    .line 49
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 51
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->getResponse$redeem_rewards_release()Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    .line 52
    sget-object v0, Lcom/squareup/redeemrewards/CouponSearch;->INSTANCE:Lcom/squareup/redeemrewards/CouponSearch;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->getResponse$redeem_rewards_release()Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;->coupon:Ljava/util/List;

    const-string v3, "result.response.coupon"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->this$0:Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;

    invoke-static {v3}, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;->access$getTransaction$p(Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;)Lcom/squareup/payment/Transaction;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/squareup/redeemrewards/CouponSearch;->getUniqueCoupons(Ljava/util/List;Lcom/squareup/payment/Transaction;)Ljava/util/List;

    move-result-object v0

    .line 54
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 55
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->getResponse$redeem_rewards_release()Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;->coupon:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/coupons/Coupon;

    .line 57
    iget-object v2, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$view:Landroid/view/View;

    sget v3, Lcom/squareup/redeemrewards/R$id;->coupon_ribbon:I

    invoke-static {v2, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/CouponRibbonView;

    .line 58
    iget-object v3, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->this$0:Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;

    invoke-static {v3}, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;->access$getFormatter$p(Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;)Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    invoke-virtual {v3, v4}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->formatShortestDescription(Lcom/squareup/api/items/Discount;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/ui/CouponRibbonView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    iget-object v2, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$view:Landroid/view/View;

    sget v3, Lcom/squareup/redeemrewards/R$id;->crm_redeem_reward_title:I

    invoke-static {v2, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/marketfont/MarketTextView;

    .line 61
    iget-object v3, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->this$0:Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;

    invoke-static {v3}, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;->access$getFormatter$p(Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;)Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->formatName(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v2, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$view:Landroid/view/View;

    sget v3, Lcom/squareup/redeemrewards/R$id;->crm_redeem_reward_subtitle:I

    invoke-static {v2, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 64
    iget-object v3, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$r:Landroid/content/res/Resources;

    sget v4, Lcom/squareup/redeemrewards/R$string;->coupon_redeem_reward_subtitle:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v3}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 65
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->getRewardCode$redeem_rewards_release()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v4, "code"

    invoke-virtual {v3, v4, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 66
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object p1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$view:Landroid/view/View;

    sget v2, Lcom/squareup/redeemrewards/R$id;->crm_redeem_reward_button:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    .line 70
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object p1

    .line 71
    iget-object v2, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$view:Landroid/view/View;

    new-instance v3, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2$1;

    invoke-direct {v3, p0, v0}, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2$1;-><init>(Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;Lcom/squareup/protos/client/coupons/Coupon;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v2, v3}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    .line 76
    iget-object p1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$view:Landroid/view/View;

    .line 77
    sget v0, Lcom/squareup/redeemrewards/R$id;->crm_redeem_reward_layout:I

    .line 76
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    .line 78
    check-cast p1, Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$animate:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$shortAnimTime:I

    :cond_0
    invoke-static {p1, v1}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    return-void

    .line 80
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->getResponse$redeem_rewards_release()Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;->coupon:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, v2, :cond_3

    .line 84
    iget-object p1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$view:Landroid/view/View;

    sget v0, Lcom/squareup/redeemrewards/R$id;->empty_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/EmptyView;

    .line 85
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/EmptyView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 86
    sget v0, Lcom/squareup/redeemrewards/R$string;->coupon_search_reward_conflicting_title:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/EmptyView;->setTitle(I)V

    .line 87
    sget v0, Lcom/squareup/redeemrewards/R$string;->coupon_search_reward_conflicting_msg:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/EmptyView;->setMessage(I)V

    .line 88
    iget-object p1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$view:Landroid/view/View;

    sget v0, Lcom/squareup/redeemrewards/R$id;->crm_reward_not_found_layout:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    .line 89
    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$animate:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$shortAnimTime:I

    :cond_2
    invoke-static {p1, v1}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    return-void

    .line 94
    :cond_3
    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$view:Landroid/view/View;

    sget v2, Lcom/squareup/redeemrewards/R$id;->empty_view:I

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/EmptyView;

    .line 95
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->getError$redeem_rewards_release()Ljava/lang/Throwable;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 96
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_EXCLAMATION:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/EmptyView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 97
    sget p1, Lcom/squareup/redeemrewards/R$string;->coupon_search_failed:I

    invoke-virtual {v0, p1}, Lcom/squareup/ui/EmptyView;->setTitle(I)V

    goto :goto_0

    .line 99
    :cond_4
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/EmptyView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 100
    sget p1, Lcom/squareup/redeemrewards/R$string;->coupon_search_reward_not_found:I

    invoke-virtual {v0, p1}, Lcom/squareup/ui/EmptyView;->setTitle(I)V

    .line 102
    :goto_0
    sget p1, Lcom/squareup/redeemrewards/R$string;->coupon_search_reward_not_found_helper:I

    invoke-virtual {v0, p1}, Lcom/squareup/ui/EmptyView;->setMessage(I)V

    .line 104
    iget-object p1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$view:Landroid/view/View;

    sget v0, Lcom/squareup/redeemrewards/R$id;->crm_reward_not_found_layout:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    .line 105
    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$animate:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_5

    iget v1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->$shortAnimTime:I

    :cond_5
    invoke-static {p1, v1}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    return-void
.end method
