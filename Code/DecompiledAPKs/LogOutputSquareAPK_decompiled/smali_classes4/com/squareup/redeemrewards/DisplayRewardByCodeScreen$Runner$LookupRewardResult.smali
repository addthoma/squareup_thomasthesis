.class public final Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;
.super Ljava/lang/Object;
.source "DisplayRewardByCodeScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LookupRewardResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \u001c2\u00020\u0001:\u0001\u001cB#\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0003H\u00c0\u0003\u00a2\u0006\u0002\u0008\u0010J\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0005H\u00c0\u0003\u00a2\u0006\u0002\u0008\u0012J\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0007H\u00c0\u0003\u00a2\u0006\u0002\u0008\u0014J-\u0010\u0015\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u0003H\u00d6\u0001R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;",
        "",
        "rewardCode",
        "",
        "response",
        "Lcom/squareup/protos/client/coupons/LookupCouponsResponse;",
        "error",
        "",
        "(Ljava/lang/String;Lcom/squareup/protos/client/coupons/LookupCouponsResponse;Ljava/lang/Throwable;)V",
        "getError$redeem_rewards_release",
        "()Ljava/lang/Throwable;",
        "getResponse$redeem_rewards_release",
        "()Lcom/squareup/protos/client/coupons/LookupCouponsResponse;",
        "getRewardCode$redeem_rewards_release",
        "()Ljava/lang/String;",
        "component1",
        "component1$redeem_rewards_release",
        "component2",
        "component2$redeem_rewards_release",
        "component3",
        "component3$redeem_rewards_release",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "Companion",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult$Companion;

.field public static final EMPTY:Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;


# instance fields
.field private final error:Ljava/lang/Throwable;

.field private final response:Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

.field private final rewardCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->Companion:Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult$Companion;

    .line 49
    new-instance v0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;

    move-object v2, v1

    check-cast v2, Ljava/lang/Throwable;

    invoke-direct {v0, v1, v1, v2}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/coupons/LookupCouponsResponse;Ljava/lang/Throwable;)V

    sput-object v0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->EMPTY:Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/coupons/LookupCouponsResponse;Ljava/lang/Throwable;)V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->rewardCode:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->response:Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    iput-object p3, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->error:Ljava/lang/Throwable;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;Ljava/lang/String;Lcom/squareup/protos/client/coupons/LookupCouponsResponse;Ljava/lang/Throwable;ILjava/lang/Object;)Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->rewardCode:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->response:Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->error:Ljava/lang/Throwable;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->copy(Ljava/lang/String;Lcom/squareup/protos/client/coupons/LookupCouponsResponse;Ljava/lang/Throwable;)Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1$redeem_rewards_release()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->rewardCode:Ljava/lang/String;

    return-object v0
.end method

.method public final component2$redeem_rewards_release()Lcom/squareup/protos/client/coupons/LookupCouponsResponse;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->response:Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    return-object v0
.end method

.method public final component3$redeem_rewards_release()Ljava/lang/Throwable;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->error:Ljava/lang/Throwable;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/protos/client/coupons/LookupCouponsResponse;Ljava/lang/Throwable;)Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;
    .locals 1

    new-instance v0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/coupons/LookupCouponsResponse;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;

    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->rewardCode:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->rewardCode:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->response:Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    iget-object v1, p1, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->response:Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->error:Ljava/lang/Throwable;

    iget-object p1, p1, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->error:Ljava/lang/Throwable;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getError$redeem_rewards_release()Ljava/lang/Throwable;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->error:Ljava/lang/Throwable;

    return-object v0
.end method

.method public final getResponse$redeem_rewards_release()Lcom/squareup/protos/client/coupons/LookupCouponsResponse;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->response:Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    return-object v0
.end method

.method public final getRewardCode$redeem_rewards_release()Ljava/lang/String;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->rewardCode:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->rewardCode:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->response:Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->error:Ljava/lang/Throwable;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LookupRewardResult(rewardCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->rewardCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->response:Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->error:Ljava/lang/Throwable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
