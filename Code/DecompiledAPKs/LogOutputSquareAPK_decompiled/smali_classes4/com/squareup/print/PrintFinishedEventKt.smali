.class public final Lcom/squareup/print/PrintFinishedEventKt;
.super Ljava/lang/Object;
.source "PrintFinishedEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u001a\u000e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u000e\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u000e\u0010\u0005\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u000e\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u00a8\u0006\u0007"
    }
    d2 = {
        "forPrintFailed",
        "Lcom/squareup/print/PrintFinishedEvent;",
        "job",
        "Lcom/squareup/print/PrintJob;",
        "targetDoesNotExistError",
        "targetHardwarePrinterUnavailableError",
        "targetHasNoHardwarePrinterError",
        "hardware_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final forPrintFailed(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrintFinishedEvent;
    .locals 3

    const-string v0, "job"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lcom/squareup/print/PrintFinishedEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ERROR:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PRINT_JOB_FAILED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/print/PrintFinishedEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrintJob;)V

    return-object v0
.end method

.method public static final targetDoesNotExistError(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrintFinishedEvent;
    .locals 3

    const-string v0, "job"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    new-instance v0, Lcom/squareup/print/PrintFinishedEvent;

    .line 43
    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ERROR:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PRINT_SPOOLER_TARGET_DOES_NOT_EXIST_ERROR:Lcom/squareup/analytics/RegisterActionName;

    .line 42
    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/print/PrintFinishedEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrintJob;)V

    return-object v0
.end method

.method public static final targetHardwarePrinterUnavailableError(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrintFinishedEvent;
    .locals 3

    const-string v0, "job"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    new-instance v0, Lcom/squareup/print/PrintFinishedEvent;

    .line 57
    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ERROR:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PRINT_SPOOLER_TARGET_HARDWARE_PRINTER_UNAVAILABLE_ERROR:Lcom/squareup/analytics/RegisterActionName;

    .line 56
    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/print/PrintFinishedEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrintJob;)V

    return-object v0
.end method

.method public static final targetHasNoHardwarePrinterError(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrintFinishedEvent;
    .locals 3

    const-string v0, "job"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v0, Lcom/squareup/print/PrintFinishedEvent;

    .line 50
    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ERROR:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PRINT_SPOOLER_NO_HARDWARE_PRINTER_ERROR:Lcom/squareup/analytics/RegisterActionName;

    .line 49
    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/print/PrintFinishedEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrintJob;)V

    return-object v0
.end method
