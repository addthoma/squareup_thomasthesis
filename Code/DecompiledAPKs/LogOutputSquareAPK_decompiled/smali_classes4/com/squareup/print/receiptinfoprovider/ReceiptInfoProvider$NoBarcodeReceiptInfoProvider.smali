.class public final Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider$NoBarcodeReceiptInfoProvider;
.super Ljava/lang/Object;
.source "ReceiptInfoProvider.kt"

# interfaces
.implements Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoBarcodeReceiptInfoProvider"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0001\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider$NoBarcodeReceiptInfoProvider;",
        "Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;",
        "()V",
        "formatBarcodeForReceiptNumber",
        "",
        "receiptNumber",
        "",
        "shouldPrintBarcodes",
        "",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic formatBarcodeForReceiptNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 27
    invoke-virtual {p0, p1}, Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider$NoBarcodeReceiptInfoProvider;->formatBarcodeForReceiptNumber(Ljava/lang/String;)Ljava/lang/Void;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public formatBarcodeForReceiptNumber(Ljava/lang/String;)Ljava/lang/Void;
    .locals 1

    const-string v0, "receiptNumber"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    new-instance p1, Ljava/lang/IllegalAccessException;

    const-string v0, "Barcode printing is not supported, this method should not be called."

    invoke-direct {p1, v0}, Ljava/lang/IllegalAccessException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public shouldPrintBarcodes()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
