.class public final Lcom/squareup/print/payload/ReceiptPayload$Builder;
.super Ljava/lang/Object;
.source "ReceiptPayload.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/payload/ReceiptPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009a\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010i\u001a\u00020jR\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008R\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u001c\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014R\u001c\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\"\u0004\u0008\u0019\u0010\u001aR\u001c\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001d\u0010\u001e\"\u0004\u0008\u001f\u0010 R\u001c\u0010!\u001a\u0004\u0018\u00010\"X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008#\u0010$\"\u0004\u0008%\u0010&R\u001c\u0010\'\u001a\u0004\u0018\u00010(X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008)\u0010*\"\u0004\u0008+\u0010,R\u001c\u0010-\u001a\u0004\u0018\u00010.X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008/\u00100\"\u0004\u00081\u00102R\u001c\u00103\u001a\u0004\u0018\u000104X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00085\u00106\"\u0004\u00087\u00108R\u001c\u00109\u001a\u0004\u0018\u00010:X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008;\u0010<\"\u0004\u0008=\u0010>R\u001c\u0010?\u001a\u0004\u0018\u00010@X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008A\u0010B\"\u0004\u0008C\u0010DR\u001c\u0010E\u001a\u0004\u0018\u00010FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008G\u0010H\"\u0004\u0008I\u0010JR\u001c\u0010K\u001a\u0004\u0018\u00010LX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008M\u0010N\"\u0004\u0008O\u0010PR\u001c\u0010Q\u001a\u0004\u0018\u00010RX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008S\u0010T\"\u0004\u0008U\u0010VR\u001c\u0010W\u001a\u0004\u0018\u00010XX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008Y\u0010Z\"\u0004\u0008[\u0010\\R\u001c\u0010]\u001a\u0004\u0018\u00010^X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008_\u0010`\"\u0004\u0008a\u0010bR\u001c\u0010c\u001a\u0004\u0018\u00010dX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008e\u0010f\"\u0004\u0008g\u0010h\u00a8\u0006k"
    }
    d2 = {
        "Lcom/squareup/print/payload/ReceiptPayload$Builder;",
        "",
        "()V",
        "barcodeSection",
        "Lcom/squareup/print/sections/BarcodeSection;",
        "getBarcodeSection",
        "()Lcom/squareup/print/sections/BarcodeSection;",
        "setBarcodeSection",
        "(Lcom/squareup/print/sections/BarcodeSection;)V",
        "codes",
        "Lcom/squareup/print/sections/CodesSection;",
        "getCodes",
        "()Lcom/squareup/print/sections/CodesSection;",
        "setCodes",
        "(Lcom/squareup/print/sections/CodesSection;)V",
        "emv",
        "Lcom/squareup/print/sections/EmvSection;",
        "getEmv",
        "()Lcom/squareup/print/sections/EmvSection;",
        "setEmv",
        "(Lcom/squareup/print/sections/EmvSection;)V",
        "footer",
        "Lcom/squareup/print/sections/FooterSection;",
        "getFooter",
        "()Lcom/squareup/print/sections/FooterSection;",
        "setFooter",
        "(Lcom/squareup/print/sections/FooterSection;)V",
        "header",
        "Lcom/squareup/print/sections/HeaderSection;",
        "getHeader",
        "()Lcom/squareup/print/sections/HeaderSection;",
        "setHeader",
        "(Lcom/squareup/print/sections/HeaderSection;)V",
        "itemsAndDiscounts",
        "Lcom/squareup/print/sections/ItemsAndDiscountsSection;",
        "getItemsAndDiscounts",
        "()Lcom/squareup/print/sections/ItemsAndDiscountsSection;",
        "setItemsAndDiscounts",
        "(Lcom/squareup/print/sections/ItemsAndDiscountsSection;)V",
        "multipleTaxBreakdown",
        "Lcom/squareup/print/sections/MultipleTaxBreakdownSection;",
        "getMultipleTaxBreakdown",
        "()Lcom/squareup/print/sections/MultipleTaxBreakdownSection;",
        "setMultipleTaxBreakdown",
        "(Lcom/squareup/print/sections/MultipleTaxBreakdownSection;)V",
        "receiptType",
        "Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;",
        "getReceiptType",
        "()Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;",
        "setReceiptType",
        "(Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;)V",
        "refunds",
        "Lcom/squareup/print/sections/RefundsSection;",
        "getRefunds",
        "()Lcom/squareup/print/sections/RefundsSection;",
        "setRefunds",
        "(Lcom/squareup/print/sections/RefundsSection;)V",
        "renderType",
        "Lcom/squareup/print/payload/ReceiptPayload$RenderType;",
        "getRenderType",
        "()Lcom/squareup/print/payload/ReceiptPayload$RenderType;",
        "setRenderType",
        "(Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V",
        "returnSubtotals",
        "Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;",
        "getReturnSubtotals",
        "()Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;",
        "setReturnSubtotals",
        "(Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;)V",
        "returns",
        "Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;",
        "getReturns",
        "()Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;",
        "setReturns",
        "(Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;)V",
        "signature",
        "Lcom/squareup/print/papersig/SignatureSection;",
        "getSignature",
        "()Lcom/squareup/print/papersig/SignatureSection;",
        "setSignature",
        "(Lcom/squareup/print/papersig/SignatureSection;)V",
        "subtotalsAndAdjustments",
        "Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;",
        "getSubtotalsAndAdjustments",
        "()Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;",
        "setSubtotalsAndAdjustments",
        "(Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;)V",
        "tender",
        "Lcom/squareup/print/sections/TenderSection;",
        "getTender",
        "()Lcom/squareup/print/sections/TenderSection;",
        "setTender",
        "(Lcom/squareup/print/sections/TenderSection;)V",
        "tip",
        "Lcom/squareup/print/papersig/TipSections;",
        "getTip",
        "()Lcom/squareup/print/papersig/TipSections;",
        "setTip",
        "(Lcom/squareup/print/papersig/TipSections;)V",
        "total",
        "Lcom/squareup/print/sections/TotalSection;",
        "getTotal",
        "()Lcom/squareup/print/sections/TotalSection;",
        "setTotal",
        "(Lcom/squareup/print/sections/TotalSection;)V",
        "build",
        "Lcom/squareup/print/payload/ReceiptPayload;",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private barcodeSection:Lcom/squareup/print/sections/BarcodeSection;

.field private codes:Lcom/squareup/print/sections/CodesSection;

.field private emv:Lcom/squareup/print/sections/EmvSection;

.field private footer:Lcom/squareup/print/sections/FooterSection;

.field private header:Lcom/squareup/print/sections/HeaderSection;

.field private itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

.field private multipleTaxBreakdown:Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

.field private receiptType:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

.field private refunds:Lcom/squareup/print/sections/RefundsSection;

.field private renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

.field private returnSubtotals:Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

.field private returns:Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

.field private signature:Lcom/squareup/print/papersig/SignatureSection;

.field private subtotalsAndAdjustments:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

.field private tender:Lcom/squareup/print/sections/TenderSection;

.field private tip:Lcom/squareup/print/papersig/TipSections;

.field private total:Lcom/squareup/print/sections/TotalSection;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final build()Lcom/squareup/print/payload/ReceiptPayload;
    .locals 21

    move-object/from16 v0, p0

    .line 105
    new-instance v19, Lcom/squareup/print/payload/ReceiptPayload;

    .line 106
    iget-object v2, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->header:Lcom/squareup/print/sections/HeaderSection;

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 107
    :cond_0
    iget-object v3, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->codes:Lcom/squareup/print/sections/CodesSection;

    if-nez v3, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 108
    :cond_1
    iget-object v4, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->emv:Lcom/squareup/print/sections/EmvSection;

    if-nez v4, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 109
    :cond_2
    iget-object v5, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    .line 110
    iget-object v6, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->subtotalsAndAdjustments:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    .line 111
    iget-object v7, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->total:Lcom/squareup/print/sections/TotalSection;

    if-nez v7, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 112
    :cond_3
    iget-object v8, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->tender:Lcom/squareup/print/sections/TenderSection;

    if-nez v8, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 113
    :cond_4
    iget-object v9, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->refunds:Lcom/squareup/print/sections/RefundsSection;

    .line 114
    iget-object v10, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->returns:Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    .line 115
    iget-object v11, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->returnSubtotals:Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    .line 116
    iget-object v12, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->tip:Lcom/squareup/print/papersig/TipSections;

    .line 117
    iget-object v13, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->signature:Lcom/squareup/print/papersig/SignatureSection;

    .line 118
    iget-object v14, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->multipleTaxBreakdown:Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    .line 119
    iget-object v15, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->footer:Lcom/squareup/print/sections/FooterSection;

    if-nez v15, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 120
    :cond_5
    iget-object v1, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->barcodeSection:Lcom/squareup/print/sections/BarcodeSection;

    move-object/from16 v16, v15

    .line 121
    iget-object v15, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    if-nez v15, :cond_6

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_6
    move-object/from16 v17, v1

    .line 122
    iget-object v1, v0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->receiptType:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    move-object/from16 v18, v1

    move-object/from16 v1, v19

    move-object/from16 v20, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v20

    .line 105
    invoke-direct/range {v1 .. v18}, Lcom/squareup/print/payload/ReceiptPayload;-><init>(Lcom/squareup/print/sections/HeaderSection;Lcom/squareup/print/sections/CodesSection;Lcom/squareup/print/sections/EmvSection;Lcom/squareup/print/sections/ItemsAndDiscountsSection;Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;Lcom/squareup/print/sections/TotalSection;Lcom/squareup/print/sections/TenderSection;Lcom/squareup/print/sections/RefundsSection;Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;Lcom/squareup/print/papersig/TipSections;Lcom/squareup/print/papersig/SignatureSection;Lcom/squareup/print/sections/MultipleTaxBreakdownSection;Lcom/squareup/print/sections/FooterSection;Lcom/squareup/print/sections/BarcodeSection;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;)V

    return-object v19
.end method

.method public final getBarcodeSection()Lcom/squareup/print/sections/BarcodeSection;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->barcodeSection:Lcom/squareup/print/sections/BarcodeSection;

    return-object v0
.end method

.method public final getCodes()Lcom/squareup/print/sections/CodesSection;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->codes:Lcom/squareup/print/sections/CodesSection;

    return-object v0
.end method

.method public final getEmv()Lcom/squareup/print/sections/EmvSection;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->emv:Lcom/squareup/print/sections/EmvSection;

    return-object v0
.end method

.method public final getFooter()Lcom/squareup/print/sections/FooterSection;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->footer:Lcom/squareup/print/sections/FooterSection;

    return-object v0
.end method

.method public final getHeader()Lcom/squareup/print/sections/HeaderSection;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->header:Lcom/squareup/print/sections/HeaderSection;

    return-object v0
.end method

.method public final getItemsAndDiscounts()Lcom/squareup/print/sections/ItemsAndDiscountsSection;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    return-object v0
.end method

.method public final getMultipleTaxBreakdown()Lcom/squareup/print/sections/MultipleTaxBreakdownSection;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->multipleTaxBreakdown:Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    return-object v0
.end method

.method public final getReceiptType()Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->receiptType:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    return-object v0
.end method

.method public final getRefunds()Lcom/squareup/print/sections/RefundsSection;
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->refunds:Lcom/squareup/print/sections/RefundsSection;

    return-object v0
.end method

.method public final getRenderType()Lcom/squareup/print/payload/ReceiptPayload$RenderType;
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    return-object v0
.end method

.method public final getReturnSubtotals()Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->returnSubtotals:Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    return-object v0
.end method

.method public final getReturns()Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->returns:Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    return-object v0
.end method

.method public final getSignature()Lcom/squareup/print/papersig/SignatureSection;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->signature:Lcom/squareup/print/papersig/SignatureSection;

    return-object v0
.end method

.method public final getSubtotalsAndAdjustments()Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->subtotalsAndAdjustments:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    return-object v0
.end method

.method public final getTender()Lcom/squareup/print/sections/TenderSection;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->tender:Lcom/squareup/print/sections/TenderSection;

    return-object v0
.end method

.method public final getTip()Lcom/squareup/print/papersig/TipSections;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->tip:Lcom/squareup/print/papersig/TipSections;

    return-object v0
.end method

.method public final getTotal()Lcom/squareup/print/sections/TotalSection;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->total:Lcom/squareup/print/sections/TotalSection;

    return-object v0
.end method

.method public final setBarcodeSection(Lcom/squareup/print/sections/BarcodeSection;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->barcodeSection:Lcom/squareup/print/sections/BarcodeSection;

    return-void
.end method

.method public final setCodes(Lcom/squareup/print/sections/CodesSection;)V
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->codes:Lcom/squareup/print/sections/CodesSection;

    return-void
.end method

.method public final setEmv(Lcom/squareup/print/sections/EmvSection;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->emv:Lcom/squareup/print/sections/EmvSection;

    return-void
.end method

.method public final setFooter(Lcom/squareup/print/sections/FooterSection;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->footer:Lcom/squareup/print/sections/FooterSection;

    return-void
.end method

.method public final setHeader(Lcom/squareup/print/sections/HeaderSection;)V
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->header:Lcom/squareup/print/sections/HeaderSection;

    return-void
.end method

.method public final setItemsAndDiscounts(Lcom/squareup/print/sections/ItemsAndDiscountsSection;)V
    .locals 0

    .line 89
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    return-void
.end method

.method public final setMultipleTaxBreakdown(Lcom/squareup/print/sections/MultipleTaxBreakdownSection;)V
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->multipleTaxBreakdown:Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    return-void
.end method

.method public final setReceiptType(Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;)V
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->receiptType:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    return-void
.end method

.method public final setRefunds(Lcom/squareup/print/sections/RefundsSection;)V
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->refunds:Lcom/squareup/print/sections/RefundsSection;

    return-void
.end method

.method public final setRenderType(Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    return-void
.end method

.method public final setReturnSubtotals(Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;)V
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->returnSubtotals:Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    return-void
.end method

.method public final setReturns(Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;)V
    .locals 0

    .line 94
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->returns:Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    return-void
.end method

.method public final setSignature(Lcom/squareup/print/papersig/SignatureSection;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->signature:Lcom/squareup/print/papersig/SignatureSection;

    return-void
.end method

.method public final setSubtotalsAndAdjustments(Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;)V
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->subtotalsAndAdjustments:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    return-void
.end method

.method public final setTender(Lcom/squareup/print/sections/TenderSection;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->tender:Lcom/squareup/print/sections/TenderSection;

    return-void
.end method

.method public final setTip(Lcom/squareup/print/papersig/TipSections;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->tip:Lcom/squareup/print/papersig/TipSections;

    return-void
.end method

.method public final setTotal(Lcom/squareup/print/sections/TotalSection;)V
    .locals 0

    .line 91
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$Builder;->total:Lcom/squareup/print/sections/TotalSection;

    return-void
.end method
