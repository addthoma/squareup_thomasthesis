.class public Lcom/squareup/print/PrintJob$PrintAttempt;
.super Ljava/lang/Object;
.source "PrintJob.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PrintJob;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PrintAttempt"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PrintJob$PrintAttempt$Result;
    }
.end annotation


# static fields
.field private static final NOT_YET_ATTEMPTED:Lcom/squareup/print/PrintJob$PrintAttempt;


# instance fields
.field public final errorMessage:Ljava/lang/String;

.field public final hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

.field public final portAcquisitionAccumulatedDurationMs:J

.field public final portAcquisitionAttempts:I

.field public final printTimingData:Lcom/squareup/print/PrintTimingData;

.field public final result:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

.field public final vendorSpecificResult:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 21
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt;

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->NOT_YET_ATTEMPTED:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    new-instance v2, Lcom/squareup/print/PrintTimingData;

    invoke-direct {v2}, Lcom/squareup/print/PrintTimingData;-><init>()V

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/squareup/print/PrintJob$PrintAttempt;-><init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Lcom/squareup/print/PrintTimingData;)V

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt;->NOT_YET_ATTEMPTED:Lcom/squareup/print/PrintJob$PrintAttempt;

    return-void
.end method

.method constructor <init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Lcom/squareup/print/HardwarePrinter$HardwareInfo;IJLjava/lang/String;Ljava/lang/String;Lcom/squareup/print/PrintTimingData;)V
    .locals 0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput-object p1, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->result:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 100
    iput-object p2, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    .line 101
    iput p3, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->portAcquisitionAttempts:I

    .line 102
    iput-wide p4, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->portAcquisitionAccumulatedDurationMs:J

    .line 103
    iput-object p6, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->errorMessage:Ljava/lang/String;

    .line 104
    iput-object p7, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->vendorSpecificResult:Ljava/lang/String;

    .line 105
    iput-object p8, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->printTimingData:Lcom/squareup/print/PrintTimingData;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Lcom/squareup/print/PrintTimingData;)V
    .locals 2

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iput-object p1, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->result:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/4 p1, -0x1

    .line 122
    iput p1, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->portAcquisitionAttempts:I

    const-wide/16 v0, -0x1

    .line 123
    iput-wide v0, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->portAcquisitionAccumulatedDurationMs:J

    const/4 p1, 0x0

    .line 124
    iput-object p1, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->errorMessage:Ljava/lang/String;

    .line 125
    iput-object p2, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    .line 126
    iput-object p1, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->vendorSpecificResult:Ljava/lang/String;

    .line 127
    iput-object p3, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->printTimingData:Lcom/squareup/print/PrintTimingData;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Lcom/squareup/print/PrintTimingData;)V
    .locals 2

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p1, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->result:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/4 p1, -0x1

    .line 111
    iput p1, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->portAcquisitionAttempts:I

    const-wide/16 v0, -0x1

    .line 112
    iput-wide v0, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->portAcquisitionAccumulatedDurationMs:J

    const/4 p1, 0x0

    .line 113
    iput-object p1, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->errorMessage:Ljava/lang/String;

    .line 114
    iput-object p2, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    .line 115
    iput-object p3, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->vendorSpecificResult:Ljava/lang/String;

    .line 116
    iput-object p4, p0, Lcom/squareup/print/PrintJob$PrintAttempt;->printTimingData:Lcom/squareup/print/PrintTimingData;

    return-void
.end method

.method static synthetic access$000()Lcom/squareup/print/PrintJob$PrintAttempt;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/print/PrintJob$PrintAttempt;->NOT_YET_ATTEMPTED:Lcom/squareup/print/PrintJob$PrintAttempt;

    return-object v0
.end method
