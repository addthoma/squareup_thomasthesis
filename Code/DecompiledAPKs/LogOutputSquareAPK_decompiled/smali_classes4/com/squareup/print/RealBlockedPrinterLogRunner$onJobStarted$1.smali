.class final Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobStarted$1;
.super Ljava/lang/Object;
.source "RealBlockedPrinterLogRunner.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/print/RealBlockedPrinterLogRunner;->onJobStarted(Lcom/squareup/print/PrintJob;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $printJob:Lcom/squareup/print/PrintJob;

.field final synthetic this$0:Lcom/squareup/print/RealBlockedPrinterLogRunner;


# direct methods
.method constructor <init>(Lcom/squareup/print/RealBlockedPrinterLogRunner;Lcom/squareup/print/PrintJob;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobStarted$1;->this$0:Lcom/squareup/print/RealBlockedPrinterLogRunner;

    iput-object p2, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobStarted$1;->$printJob:Lcom/squareup/print/PrintJob;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .line 18
    iget-object v0, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobStarted$1;->this$0:Lcom/squareup/print/RealBlockedPrinterLogRunner;

    iget-object v1, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobStarted$1;->$printJob:Lcom/squareup/print/PrintJob;

    invoke-virtual {v0, v1}, Lcom/squareup/print/RealBlockedPrinterLogRunner;->getUniqueLogId$hardware_release(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/RealBlockedPrinterLogRunner$UniqueLogId;

    move-result-object v0

    .line 19
    iget-object v1, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobStarted$1;->this$0:Lcom/squareup/print/RealBlockedPrinterLogRunner;

    invoke-static {v1}, Lcom/squareup/print/RealBlockedPrinterLogRunner;->access$getJobToRunnableMap$p(Lcom/squareup/print/RealBlockedPrinterLogRunner;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "Starting log runner for an existing unique id. Weird!!"

    .line 22
    invoke-static {v3, v2}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 23
    iget-object v2, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobStarted$1;->this$0:Lcom/squareup/print/RealBlockedPrinterLogRunner;

    invoke-static {v2}, Lcom/squareup/print/RealBlockedPrinterLogRunner;->access$getMainThread$p(Lcom/squareup/print/RealBlockedPrinterLogRunner;)Lcom/squareup/thread/executor/MainThread;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 24
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 27
    :cond_0
    new-instance v1, Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutRunner;

    iget-object v2, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobStarted$1;->this$0:Lcom/squareup/print/RealBlockedPrinterLogRunner;

    iget-object v3, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobStarted$1;->$printJob:Lcom/squareup/print/PrintJob;

    invoke-direct {v1, v2, v3}, Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutRunner;-><init>(Lcom/squareup/print/RealBlockedPrinterLogRunner;Lcom/squareup/print/PrintJob;)V

    check-cast v1, Ljava/lang/Runnable;

    .line 28
    iget-object v2, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobStarted$1;->this$0:Lcom/squareup/print/RealBlockedPrinterLogRunner;

    invoke-static {v2}, Lcom/squareup/print/RealBlockedPrinterLogRunner;->access$getJobToRunnableMap$p(Lcom/squareup/print/RealBlockedPrinterLogRunner;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    iget-object v0, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobStarted$1;->this$0:Lcom/squareup/print/RealBlockedPrinterLogRunner;

    invoke-static {v0}, Lcom/squareup/print/RealBlockedPrinterLogRunner;->access$getMainThread$p(Lcom/squareup/print/RealBlockedPrinterLogRunner;)Lcom/squareup/thread/executor/MainThread;

    move-result-object v0

    invoke-static {}, Lcom/squareup/print/RealBlockedPrinterLogRunner;->access$getDELAY_MILLIS$cp()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
