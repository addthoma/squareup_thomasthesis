.class public Lcom/squareup/print/util/StarBitmap;
.super Ljava/lang/Object;
.source "StarBitmap.java"


# static fields
.field private static final BLACK_BIT:I = 0x1

.field private static final THRESHOLD:I = 0xc8

.field private static final WHITE_BITS:I = 0x0

.field private static final WHITE_BYTE:B = -0x1t


# instance fields
.field height:I

.field imageData:[B

.field linebytes:I

.field pixels:[B

.field width:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;I)V
    .locals 6

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    const/4 v1, 0x0

    if-le v0, p2, :cond_0

    .line 36
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    mul-int v0, v0, p2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/2addr v0, v2

    .line 37
    invoke-static {p1, p2, v0, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 40
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p2

    iput p2, p0, Lcom/squareup/print/util/StarBitmap;->width:I

    .line 41
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p2

    iput p2, p0, Lcom/squareup/print/util/StarBitmap;->height:I

    .line 43
    iget p2, p0, Lcom/squareup/print/util/StarBitmap;->width:I

    add-int/lit8 p2, p2, 0x7

    and-int/lit8 p2, p2, -0x8

    iput p2, p0, Lcom/squareup/print/util/StarBitmap;->linebytes:I

    .line 44
    iget p2, p0, Lcom/squareup/print/util/StarBitmap;->height:I

    iget v0, p0, Lcom/squareup/print/util/StarBitmap;->linebytes:I

    mul-int p2, p2, v0

    new-array p2, p2, [B

    iput-object p2, p0, Lcom/squareup/print/util/StarBitmap;->pixels:[B

    const/4 p2, 0x0

    .line 47
    :goto_0
    iget v0, p0, Lcom/squareup/print/util/StarBitmap;->height:I

    if-ge p2, v0, :cond_3

    .line 48
    iget v0, p0, Lcom/squareup/print/util/StarBitmap;->linebytes:I

    mul-int v0, v0, p2

    move v2, v0

    const/4 v0, 0x0

    .line 49
    :goto_1
    iget v3, p0, Lcom/squareup/print/util/StarBitmap;->width:I

    if-ge v0, v3, :cond_1

    .line 50
    iget-object v3, p0, Lcom/squareup/print/util/StarBitmap;->pixels:[B

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v5

    invoke-direct {p0, v5}, Lcom/squareup/print/util/StarBitmap;->grayscale(I)B

    move-result v5

    aput-byte v5, v3, v2

    add-int/lit8 v0, v0, 0x1

    move v2, v4

    goto :goto_1

    .line 53
    :cond_1
    :goto_2
    iget v0, p0, Lcom/squareup/print/util/StarBitmap;->linebytes:I

    if-ge v3, v0, :cond_2

    .line 54
    iget-object v0, p0, Lcom/squareup/print/util/StarBitmap;->pixels:[B

    add-int/lit8 v4, v2, 0x1

    const/4 v5, -0x1

    aput-byte v5, v0, v2

    add-int/lit8 v3, v3, 0x1

    move v2, v4

    goto :goto_2

    :cond_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private emitBlankLines([BIII)I
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p3, v1, :cond_0

    const/4 v1, 0x3

    if-ge p4, v1, :cond_0

    add-int/lit8 p3, p2, 0x1

    const/16 v1, 0x62

    .line 145
    aput-byte v1, p1, p2

    add-int/lit8 p2, p3, 0x1

    int-to-byte v1, p4

    .line 146
    aput-byte v1, p1, p3

    add-int/lit8 p3, p2, 0x1

    .line 147
    aput-byte v0, p1, p2

    const/4 p2, 0x0

    :goto_0
    if-ge p2, p4, :cond_1

    add-int/lit8 v1, p3, 0x1

    .line 149
    aput-byte v0, p1, p3

    add-int/lit8 p2, p2, 0x1

    move p3, v1

    goto :goto_0

    :cond_0
    add-int/lit8 p4, p2, 0x1

    const/16 v1, 0x1b

    .line 153
    aput-byte v1, p1, p2

    add-int/lit8 p2, p4, 0x1

    const/16 v1, 0x2a

    .line 154
    aput-byte v1, p1, p4

    add-int/lit8 p4, p2, 0x1

    const/16 v1, 0x72

    .line 155
    aput-byte v1, p1, p2

    add-int/lit8 p2, p4, 0x1

    const/16 v1, 0x59

    .line 156
    aput-byte v1, p1, p4

    .line 159
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p3

    sget-object p4, Lcom/squareup/util/Strings;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p3, p4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p3

    .line 160
    array-length p4, p3

    invoke-static {p3, v0, p1, p2, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 161
    array-length p3, p3

    add-int/2addr p2, p3

    add-int/lit8 p3, p2, 0x1

    .line 162
    aput-byte v0, p1, p2

    :cond_1
    return p3
.end method

.method private grayscale(I)B
    .locals 2

    shr-int/lit8 v0, p1, 0x10

    and-int/lit16 v0, v0, 0xff

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    and-int/lit16 p1, p1, 0xff

    mul-int/lit8 v0, v0, 0x4d

    mul-int/lit16 v1, v1, 0x97

    add-int/2addr v0, v1

    mul-int/lit8 p1, p1, 0x1c

    add-int/2addr v0, p1

    shr-int/lit8 p1, v0, 0x8

    int-to-byte p1, p1

    return p1
.end method


# virtual methods
.method public getImageRasterDataForPrinting()[B
    .locals 17

    move-object/from16 v0, p0

    .line 68
    iget-object v1, v0, Lcom/squareup/print/util/StarBitmap;->imageData:[B

    if-eqz v1, :cond_0

    return-object v1

    .line 72
    :cond_0
    iget v1, v0, Lcom/squareup/print/util/StarBitmap;->linebytes:I

    const/16 v2, 0x8

    div-int/2addr v1, v2

    add-int/lit8 v3, v1, 0x3

    .line 74
    new-array v4, v3, [B

    .line 75
    iget v5, v0, Lcom/squareup/print/util/StarBitmap;->height:I

    mul-int v5, v5, v3

    new-array v5, v5, [B

    iput-object v5, v0, Lcom/squareup/print/util/StarBitmap;->imageData:[B

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 81
    :goto_0
    iget v10, v0, Lcom/squareup/print/util/StarBitmap;->height:I

    if-ge v6, v10, :cond_6

    const/16 v10, 0x62

    .line 83
    aput-byte v10, v4, v5

    const/4 v10, 0x2

    .line 84
    rem-int/lit16 v11, v1, 0x100

    int-to-byte v11, v11

    const/4 v12, 0x1

    aput-byte v11, v4, v12

    .line 85
    div-int/lit16 v11, v1, 0x100

    int-to-byte v11, v11

    aput-byte v11, v4, v10

    const/4 v10, 0x3

    move v10, v9

    const/4 v9, 0x0

    const/4 v11, 0x3

    :goto_1
    if-ge v9, v1, :cond_3

    move v14, v10

    move v13, v12

    const/4 v10, 0x0

    const/4 v12, 0x0

    :goto_2
    if-ge v10, v2, :cond_2

    shl-int/lit8 v12, v12, 0x1

    int-to-byte v12, v12

    .line 93
    iget-object v15, v0, Lcom/squareup/print/util/StarBitmap;->pixels:[B

    add-int/lit8 v16, v14, 0x1

    aget-byte v14, v15, v14

    and-int/lit16 v14, v14, 0xff

    const/16 v15, 0xc8

    if-ge v14, v15, :cond_1

    or-int/lit8 v12, v12, 0x1

    int-to-byte v12, v12

    const/4 v13, 0x0

    :cond_1
    add-int/lit8 v10, v10, 0x1

    move/from16 v14, v16

    goto :goto_2

    :cond_2
    add-int/lit8 v10, v11, 0x1

    .line 98
    aput-byte v12, v4, v11

    add-int/lit8 v9, v9, 0x1

    move v11, v10

    move v12, v13

    move v10, v14

    goto :goto_1

    :cond_3
    if-eqz v12, :cond_4

    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    :cond_4
    if-lez v7, :cond_5

    .line 107
    iget-object v9, v0, Lcom/squareup/print/util/StarBitmap;->imageData:[B

    invoke-direct {v0, v9, v8, v7, v1}, Lcom/squareup/print/util/StarBitmap;->emitBlankLines([BIII)I

    move-result v8

    .line 111
    :cond_5
    iget-object v7, v0, Lcom/squareup/print/util/StarBitmap;->imageData:[B

    invoke-static {v4, v5, v7, v8, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v8, v3

    const/4 v7, 0x0

    :goto_3
    add-int/lit8 v6, v6, 0x1

    move v9, v10

    goto :goto_0

    :cond_6
    if-lez v7, :cond_7

    .line 119
    iget-object v2, v0, Lcom/squareup/print/util/StarBitmap;->imageData:[B

    invoke-direct {v0, v2, v8, v7, v1}, Lcom/squareup/print/util/StarBitmap;->emitBlankLines([BIII)I

    move-result v8

    .line 123
    :cond_7
    iget-object v1, v0, Lcom/squareup/print/util/StarBitmap;->imageData:[B

    array-length v2, v1

    if-ge v8, v2, :cond_8

    .line 124
    new-array v2, v8, [B

    .line 125
    invoke-static {v1, v5, v2, v5, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 126
    iput-object v2, v0, Lcom/squareup/print/util/StarBitmap;->imageData:[B

    .line 128
    :cond_8
    iget-object v1, v0, Lcom/squareup/print/util/StarBitmap;->imageData:[B

    return-object v1
.end method
