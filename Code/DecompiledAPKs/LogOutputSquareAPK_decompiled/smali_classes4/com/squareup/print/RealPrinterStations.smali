.class public final Lcom/squareup/print/RealPrinterStations;
.super Ljava/lang/Object;
.source "RealPrinterStations.kt"

# interfaces
.implements Lcom/squareup/print/PrinterStations;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealPrinterStations.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealPrinterStations.kt\ncom/squareup/print/RealPrinterStations\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,166:1\n1550#2,3:167\n1550#2,3:170\n1550#2,3:173\n1550#2,3:176\n1550#2,3:179\n704#2:182\n777#2,2:183\n747#2:185\n769#2,2:186\n704#2:188\n777#2,2:189\n704#2:191\n777#2,2:192\n747#2:194\n769#2,2:195\n704#2:197\n777#2,2:198\n1577#2,4:200\n704#2:204\n777#2,2:205\n1360#2:207\n1429#2,3:208\n1176#2,2:211\n1190#2,4:213\n*E\n*S KotlinDebug\n*F\n+ 1 RealPrinterStations.kt\ncom/squareup/print/RealPrinterStations\n*L\n38#1,3:167\n44#1,3:170\n50#1,3:173\n58#1,3:176\n64#1,3:179\n111#1:182\n111#1,2:183\n120#1:185\n120#1,2:186\n125#1:188\n125#1,2:189\n130#1:191\n130#1,2:192\n135#1:194\n135#1,2:195\n140#1:197\n140#1,2:198\n144#1,4:200\n149#1:204\n149#1,2:205\n152#1:207\n152#1,3:208\n34#1,2:211\n34#1,4:213\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010%\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\u0011\n\u0002\u0008\u000b\u0008\u0007\u0018\u00002\u00020\u0001B=\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0014\u0008\u0001\u0010\u0004\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00060\u0005\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u000e\u0008\u0001\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0005\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0010H\u0016J\u0014\u0010\r\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00100\u000f0\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\u0007H\u0016J\u000e\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fH\u0016J\u000e\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fH\u0016J\u000e\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fH\u0016J\u0010\u0010\u001d\u001a\u00020\u00072\u0006\u0010\u001e\u001a\u00020\u0007H\u0016J\u0016\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010 \u001a\u00020!H\u0016J\u0016\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010 \u001a\u00020!H\u0016J\u0016\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010 \u001a\u00020!H\u0016J\u0016\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010 \u001a\u00020!H\u0016J\u0010\u0010%\u001a\u00020&2\u0006\u0010 \u001a\u00020!H\u0016J\u0012\u0010\'\u001a\u0004\u0018\u00010\u00102\u0006\u0010(\u001a\u00020\u0007H\u0016J!\u0010)\u001a\u00020\u000b2\u0012\u0010*\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020!0+\"\u00020!H\u0016\u00a2\u0006\u0002\u0010,J\u0008\u0010-\u001a\u00020\u000bH\u0016J!\u0010.\u001a\u00020\u000b2\u0012\u0010*\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020!0+\"\u00020!H\u0016\u00a2\u0006\u0002\u0010,J\u0008\u0010/\u001a\u00020\u000bH\u0016J\u0008\u00100\u001a\u00020\u000bH\u0016J\u0010\u00101\u001a\u00020\u000b2\u0006\u0010\u001e\u001a\u00020\u0007H\u0016J!\u00102\u001a\u00020\u000b2\u0012\u0010*\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020!0+\"\u00020!H\u0016\u00a2\u0006\u0002\u0010,J\u0010\u00103\u001a\u00020\u000b2\u0006\u00104\u001a\u00020\u0010H\u0002J\u0008\u00105\u001a\u00020\u000bH\u0016R(\u0010\r\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0010 \u0011*\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f0\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00100\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0004\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00066"
    }
    d2 = {
        "Lcom/squareup/print/RealPrinterStations;",
        "Lcom/squareup/print/PrinterStations;",
        "printerStationFactory",
        "Lcom/squareup/print/PrinterStationFactory;",
        "stationUuidsSetting",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "ticketAutoNumberingEnabled",
        "",
        "(Lcom/squareup/print/PrinterStationFactory;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/util/Res;Lcom/f2prateek/rx/preferences2/Preference;)V",
        "allStations",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "Lcom/squareup/print/PrinterStation;",
        "kotlin.jvm.PlatformType",
        "printerStationMap",
        "",
        "addNewPrinterStationByUuid",
        "",
        "printerStation",
        "Lio/reactivex/Observable;",
        "deletePrinterStation",
        "stationUuid",
        "getAllEnabledStations",
        "getAllStations",
        "getDisabledStations",
        "getDisplayableStationsStringForPrinter",
        "hardwarePrinterId",
        "getEnabledExternalStationsFor",
        "role",
        "Lcom/squareup/print/PrinterStation$Role;",
        "getEnabledInternalStationsFor",
        "getEnabledInternalStationsWithoutPaperCutterFor",
        "getEnabledStationsFor",
        "getNumberOfEnabledStationsFor",
        "",
        "getPrinterStationById",
        "uuid",
        "hasEnabledExternalStationsForAnyRoleIn",
        "roles",
        "",
        "([Lcom/squareup/print/PrinterStation$Role;)Z",
        "hasEnabledInternalStation",
        "hasEnabledInternalStationsForAnyRoleIn",
        "hasEnabledKitchenPrintingStations",
        "hasEnabledStations",
        "hasEnabledStationsFor",
        "hasEnabledStationsForAnyRoleIn",
        "isStationInternal",
        "station",
        "isTicketAutoNumberingEnabled",
        "hardware_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final allStations:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;>;"
        }
    .end annotation
.end field

.field private final printerStationMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final stationUuidsSetting:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final ticketAutoNumberingEnabled:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/print/PrinterStationFactory;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/util/Res;Lcom/f2prateek/rx/preferences2/Preference;)V
    .locals 2
    .param p2    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/print/PrinterStationUuids;
        .end annotation
    .end param
    .param p4    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/tickets/TicketAutoNumberingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrinterStationFactory;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/squareup/util/Res;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "printerStationFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stationUuidsSetting"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ticketAutoNumberingEnabled"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/print/RealPrinterStations;->stationUuidsSetting:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p3, p0, Lcom/squareup/print/RealPrinterStations;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/print/RealPrinterStations;->ticketAutoNumberingEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    .line 32
    iget-object p2, p0, Lcom/squareup/print/RealPrinterStations;->stationUuidsSetting:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {p2}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Set;

    if-eqz p2, :cond_0

    goto :goto_0

    .line 33
    :cond_0
    invoke-static {}, Lkotlin/collections/SetsKt;->emptySet()Ljava/util/Set;

    move-result-object p2

    :goto_0
    check-cast p2, Ljava/lang/Iterable;

    .line 211
    new-instance p3, Ljava/util/LinkedHashMap;

    const/16 p4, 0xa

    invoke-static {p2, p4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p4

    invoke-static {p4}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result p4

    const/16 v0, 0x10

    invoke-static {p4, v0}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result p4

    invoke-direct {p3, p4}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 213
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    .line 214
    move-object v0, p3

    check-cast v0, Ljava/util/Map;

    move-object v1, p4

    check-cast v1, Ljava/lang/String;

    .line 34
    invoke-interface {p1, v1}, Lcom/squareup/print/PrinterStationFactory;->generate(Ljava/lang/String;)Lcom/squareup/print/PrinterStation;

    move-result-object v1

    invoke-interface {v0, p4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 216
    :cond_1
    check-cast p3, Ljava/util/Map;

    .line 35
    invoke-static {p3}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    .line 36
    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStations;->getAllStations()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(getAllStations())"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/print/RealPrinterStations;->allStations:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method private final isStationInternal(Lcom/squareup/print/PrinterStation;)Z
    .locals 0

    .line 164
    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object p1

    iget-boolean p1, p1, Lcom/squareup/print/PrinterStationConfiguration;->internal:Z

    return p1
.end method


# virtual methods
.method public addNewPrinterStationByUuid(Lcom/squareup/print/PrinterStation;)V
    .locals 4

    const-string v0, "printerStation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->getId()Ljava/lang/String;

    move-result-object v0

    .line 83
    new-instance v1, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/squareup/print/RealPrinterStations;->stationUuidsSetting:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v2}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v2, Ljava/util/Collection;

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 84
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 85
    move-object v2, v1

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v2, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    const-string v3, "stationUuid"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    iget-object p1, p0, Lcom/squareup/print/RealPrinterStations;->stationUuidsSetting:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {p1, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 90
    :cond_1
    iget-object p1, p0, Lcom/squareup/print/RealPrinterStations;->allStations:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStations;->getAllStations()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public allStations()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;>;"
        }
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->allStations:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public deletePrinterStation(Ljava/lang/String;)V
    .locals 5

    const-string v0, "stationUuid"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 98
    :goto_0
    new-instance v3, Ljava/util/HashSet;

    iget-object v4, p0, Lcom/squareup/print/RealPrinterStations;->stationUuidsSetting:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v4}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v4, Ljava/util/Collection;

    invoke-direct {v3, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 99
    invoke-virtual {v3, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v0, :cond_2

    if-eqz v4, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_3

    .line 105
    iget-object p1, p0, Lcom/squareup/print/RealPrinterStations;->stationUuidsSetting:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {p1, v3}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 106
    iget-object p1, p0, Lcom/squareup/print/RealPrinterStations;->allStations:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStations;->getAllStations()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void

    .line 103
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Attempting to remove a station that doesn\'t exist with uuid "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 102
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public getAllEnabledStations()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation

    .line 110
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 182
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 183
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/print/PrinterStation;

    .line 111
    invoke-virtual {v3}, Lcom/squareup/print/PrinterStation;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 184
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public getAllStations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDisabledStations()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation

    .line 119
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 185
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 186
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/print/PrinterStation;

    .line 120
    invoke-virtual {v3}, Lcom/squareup/print/PrinterStation;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 187
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public getDisplayableStationsStringForPrinter(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const-string v0, "hardwarePrinterId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 204
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 205
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/print/PrinterStation;

    .line 150
    invoke-virtual {v3}, Lcom/squareup/print/PrinterStation;->hasHardwarePrinterSelected()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/squareup/print/PrinterStation;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 206
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 207
    new-instance p1, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-static {v1, v0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 208
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 209
    check-cast v1, Lcom/squareup/print/PrinterStation;

    .line 152
    invoke-virtual {v1}, Lcom/squareup/print/PrinterStation;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 210
    :cond_3
    check-cast p1, Ljava/util/List;

    .line 156
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/utilities/R$string;->list_pattern_long_nonfinal_separator:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 155
    invoke-static {v0}, Lcom/squareup/util/ListPhrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;

    move-result-object v0

    .line 158
    check-cast p1, Ljava/lang/Iterable;

    invoke-virtual {v0, p1}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 159
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getEnabledExternalStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrinterStation$Role;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation

    const-string v0, "role"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 197
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 198
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/print/PrinterStation;

    .line 140
    invoke-direct {p0, v3}, Lcom/squareup/print/RealPrinterStations;->isStationInternal(Lcom/squareup/print/PrinterStation;)Z

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-nez v4, :cond_1

    new-array v4, v5, [Lcom/squareup/print/PrinterStation$Role;

    aput-object p1, v4, v6

    invoke-virtual {v3, v4}, Lcom/squareup/print/PrinterStation;->hasAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 199
    :cond_2
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public getEnabledInternalStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrinterStation$Role;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation

    const-string v0, "role"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 191
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 192
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/print/PrinterStation;

    .line 130
    invoke-direct {p0, v3}, Lcom/squareup/print/RealPrinterStations;->isStationInternal(Lcom/squareup/print/PrinterStation;)Z

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v4, :cond_1

    new-array v4, v5, [Lcom/squareup/print/PrinterStation$Role;

    aput-object p1, v4, v6

    invoke-virtual {v3, v4}, Lcom/squareup/print/PrinterStation;->hasAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 193
    :cond_2
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public getEnabledInternalStationsWithoutPaperCutterFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrinterStation$Role;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation

    const-string v0, "role"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-virtual {p0, p1}, Lcom/squareup/print/RealPrinterStations;->getEnabledInternalStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 194
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 195
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/print/PrinterStation;

    .line 135
    invoke-virtual {v2}, Lcom/squareup/print/PrinterStation;->hasAutomaticPaperCutter()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 196
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrinterStation$Role;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation

    const-string v0, "role"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 188
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 189
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/print/PrinterStation;

    .line 125
    invoke-virtual {v3}, Lcom/squareup/print/PrinterStation;->isEnabled()Z

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v4, :cond_1

    new-array v4, v5, [Lcom/squareup/print/PrinterStation$Role;

    aput-object p1, v4, v6

    invoke-virtual {v3, v4}, Lcom/squareup/print/PrinterStation;->hasAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 190
    :cond_2
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public getNumberOfEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)I
    .locals 5

    const-string v0, "role"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 200
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    .line 202
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/print/PrinterStation;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/squareup/print/PrinterStation$Role;

    aput-object p1, v4, v2

    .line 144
    invoke-virtual {v3, v4}, Lcom/squareup/print/PrinterStation;->hasAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v3

    if-eqz v3, :cond_1

    add-int/lit8 v1, v1, 0x1

    if-gez v1, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_0

    :cond_2
    :goto_1
    return v1
.end method

.method public getPrinterStationById(Ljava/lang/String;)Lcom/squareup/print/PrinterStation;
    .locals 3

    const-string v0, "uuid"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/PrinterStation;

    if-nez v0, :cond_0

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "Attempted to get a printer station with uuid %s that does not exist!"

    .line 74
    invoke-static {p1, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-object v0

    .line 70
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "uuid cannot be blank"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public varargs hasEnabledExternalStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z
    .locals 5

    const-string v0, "roles"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 179
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 180
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrinterStation;

    .line 65
    invoke-virtual {v1}, Lcom/squareup/print/PrinterStation;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/print/RealPrinterStations;->isStationInternal(Lcom/squareup/print/PrinterStation;)Z

    move-result v4

    if-nez v4, :cond_2

    array-length v4, p1

    invoke-static {p1, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/squareup/print/PrinterStation$Role;

    invoke-virtual {v1, v4}, Lcom/squareup/print/PrinterStation;->hasAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const/4 v3, 0x1

    :cond_3
    :goto_1
    return v3
.end method

.method public hasEnabledInternalStation()Z
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/print/PrinterStation$Role;

    .line 54
    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/squareup/print/RealPrinterStations;->hasEnabledInternalStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    return v0
.end method

.method public varargs hasEnabledInternalStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z
    .locals 5

    const-string v0, "roles"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 176
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 177
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrinterStation;

    .line 59
    invoke-virtual {v1}, Lcom/squareup/print/PrinterStation;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/print/RealPrinterStations;->isStationInternal(Lcom/squareup/print/PrinterStation;)Z

    move-result v4

    if-eqz v4, :cond_2

    array-length v4, p1

    invoke-static {p1, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/squareup/print/PrinterStation$Role;

    invoke-virtual {v1, v4}, Lcom/squareup/print/PrinterStation;->hasAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const/4 v3, 0x1

    :cond_3
    :goto_1
    return v3
.end method

.method public hasEnabledKitchenPrintingStations()Z
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/print/PrinterStation$Role;

    .line 41
    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/squareup/print/RealPrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    return v0
.end method

.method public hasEnabledStations()Z
    .locals 3

    .line 38
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 167
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 168
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrinterStation;

    .line 38
    invoke-virtual {v1}, Lcom/squareup/print/PrinterStation;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :cond_2
    :goto_0
    return v2
.end method

.method public hasEnabledStationsFor(Ljava/lang/String;)Z
    .locals 5

    const-string v0, "hardwarePrinterId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 170
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 171
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrinterStation;

    .line 45
    invoke-virtual {v1}, Lcom/squareup/print/PrinterStation;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Lcom/squareup/print/PrinterStation;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const/4 v3, 0x1

    :cond_3
    :goto_1
    return v3
.end method

.method public varargs hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z
    .locals 5

    const-string v0, "roles"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->printerStationMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 173
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 174
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrinterStation;

    .line 50
    invoke-virtual {v1}, Lcom/squareup/print/PrinterStation;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    array-length v4, p1

    invoke-static {p1, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/squareup/print/PrinterStation$Role;

    invoke-virtual {v1, v4}, Lcom/squareup/print/PrinterStation;->hasAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const/4 v3, 0x1

    :cond_3
    :goto_1
    return v3
.end method

.method public isTicketAutoNumberingEnabled()Z
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations;->ticketAutoNumberingEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
