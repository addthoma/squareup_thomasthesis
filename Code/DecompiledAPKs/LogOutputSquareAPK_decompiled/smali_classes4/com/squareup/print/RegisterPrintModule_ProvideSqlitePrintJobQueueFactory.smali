.class public final Lcom/squareup/print/RegisterPrintModule_ProvideSqlitePrintJobQueueFactory;
.super Ljava/lang/Object;
.source "RegisterPrintModule_ProvideSqlitePrintJobQueueFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/PrintJobQueue;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final userDirProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/print/RegisterPrintModule_ProvideSqlitePrintJobQueueFactory;->contextProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/print/RegisterPrintModule_ProvideSqlitePrintJobQueueFactory;->userDirProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/print/RegisterPrintModule_ProvideSqlitePrintJobQueueFactory;->gsonProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/RegisterPrintModule_ProvideSqlitePrintJobQueueFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)",
            "Lcom/squareup/print/RegisterPrintModule_ProvideSqlitePrintJobQueueFactory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/print/RegisterPrintModule_ProvideSqlitePrintJobQueueFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/print/RegisterPrintModule_ProvideSqlitePrintJobQueueFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideSqlitePrintJobQueue(Landroid/app/Application;Ljava/io/File;Lcom/google/gson/Gson;)Lcom/squareup/print/PrintJobQueue;
    .locals 0

    .line 47
    invoke-static {p0, p1, p2}, Lcom/squareup/print/RegisterPrintModule;->provideSqlitePrintJobQueue(Landroid/app/Application;Ljava/io/File;Lcom/google/gson/Gson;)Lcom/squareup/print/PrintJobQueue;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/PrintJobQueue;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/print/PrintJobQueue;
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintModule_ProvideSqlitePrintJobQueueFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/print/RegisterPrintModule_ProvideSqlitePrintJobQueueFactory;->userDirProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    iget-object v2, p0, Lcom/squareup/print/RegisterPrintModule_ProvideSqlitePrintJobQueueFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/gson/Gson;

    invoke-static {v0, v1, v2}, Lcom/squareup/print/RegisterPrintModule_ProvideSqlitePrintJobQueueFactory;->provideSqlitePrintJobQueue(Landroid/app/Application;Ljava/io/File;Lcom/google/gson/Gson;)Lcom/squareup/print/PrintJobQueue;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/print/RegisterPrintModule_ProvideSqlitePrintJobQueueFactory;->get()Lcom/squareup/print/PrintJobQueue;

    move-result-object v0

    return-object v0
.end method
