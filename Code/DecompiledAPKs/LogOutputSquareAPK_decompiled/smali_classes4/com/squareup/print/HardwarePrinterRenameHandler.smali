.class public Lcom/squareup/print/HardwarePrinterRenameHandler;
.super Ljava/lang/Object;
.source "HardwarePrinterRenameHandler.java"

# interfaces
.implements Lcom/squareup/print/HardwarePrinterTracker$Listener;


# instance fields
.field private final printerStations:Lcom/squareup/print/PrinterStations;


# direct methods
.method constructor <init>(Lcom/squareup/print/PrinterStations;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/print/HardwarePrinterRenameHandler;->printerStations:Lcom/squareup/print/PrinterStations;

    return-void
.end method


# virtual methods
.method public printerConnected(Lcom/squareup/print/HardwarePrinter;)V
    .locals 8

    .line 21
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->hasUniqueAttribute()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/print/HardwarePrinterRenameHandler;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 22
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsFor(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 23
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getNonUniqueId()Ljava/lang/String;

    move-result-object v0

    .line 25
    iget-object v1, p0, Lcom/squareup/print/HardwarePrinterRenameHandler;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {v1}, Lcom/squareup/print/PrinterStations;->getAllStations()Ljava/util/List;

    move-result-object v1

    .line 26
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/PrinterStation;

    .line 27
    invoke-virtual {v2}, Lcom/squareup/print/PrinterStation;->hasHardwarePrinterSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 30
    invoke-virtual {v2}, Lcom/squareup/print/PrinterStation;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-eqz v3, :cond_1

    new-array v3, v4, [Ljava/lang/Object;

    .line 32
    invoke-virtual {v2}, Lcom/squareup/print/PrinterStation;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getDisplayableModelName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const-string v4, "Changing printer station %s to use printer %s because a unique ID was added"

    .line 31
    invoke-static {v4, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 34
    :cond_1
    invoke-virtual {v2}, Lcom/squareup/print/PrinterStation;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object v3

    .line 35
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v7

    iget-object v7, v7, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->uniqueAttribute:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 36
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionTypeName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 37
    invoke-virtual {v2}, Lcom/squareup/print/PrinterStation;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object v3

    .line 38
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionTypeName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-array v3, v4, [Ljava/lang/Object;

    .line 46
    invoke-virtual {v2}, Lcom/squareup/print/PrinterStation;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getDisplayableModelName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const-string v4, "Changing printer station %s to use printer %s; model changed during SDK update"

    .line 44
    invoke-static {v4, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_0

    .line 51
    invoke-virtual {v2}, Lcom/squareup/print/PrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/print/PrinterStationConfiguration;->buildUpon()Lcom/squareup/print/PrinterStationConfiguration$Builder;

    move-result-object v3

    .line 52
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->setSelectedHardwarePrinterId(Ljava/lang/String;)Lcom/squareup/print/PrinterStationConfiguration$Builder;

    move-result-object v3

    .line 53
    invoke-virtual {v3}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->build()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v3

    .line 54
    invoke-virtual {v2, v3}, Lcom/squareup/print/PrinterStation;->commit(Lcom/squareup/print/PrinterStationConfiguration;)V

    .line 55
    iget-object v3, p0, Lcom/squareup/print/HardwarePrinterRenameHandler;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {v3, v2}, Lcom/squareup/print/PrinterStations;->addNewPrinterStationByUuid(Lcom/squareup/print/PrinterStation;)V

    goto/16 :goto_0

    :cond_3
    return-void
.end method

.method public printerDisconnected(Lcom/squareup/print/HardwarePrinter;)V
    .locals 0

    return-void
.end method
