.class final Lcom/squareup/print/RealPrintoutDispatcher$throwForPrinterNotFound$availablePrinterIds$1;
.super Lkotlin/jvm/internal/Lambda;
.source "PrintoutDispatcher.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/print/RealPrintoutDispatcher;->throwForPrinterNotFound(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/print/HardwarePrinter;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "printer",
        "Lcom/squareup/print/HardwarePrinter;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/print/RealPrintoutDispatcher$throwForPrinterNotFound$availablePrinterIds$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/print/RealPrintoutDispatcher$throwForPrinterNotFound$availablePrinterIds$1;

    invoke-direct {v0}, Lcom/squareup/print/RealPrintoutDispatcher$throwForPrinterNotFound$availablePrinterIds$1;-><init>()V

    sput-object v0, Lcom/squareup/print/RealPrintoutDispatcher$throwForPrinterNotFound$availablePrinterIds$1;->INSTANCE:Lcom/squareup/print/RealPrintoutDispatcher$throwForPrinterNotFound$availablePrinterIds$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 66
    check-cast p1, Lcom/squareup/print/HardwarePrinter;

    invoke-virtual {p0, p1}, Lcom/squareup/print/RealPrintoutDispatcher$throwForPrinterNotFound$availablePrinterIds$1;->invoke(Lcom/squareup/print/HardwarePrinter;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/print/HardwarePrinter;)Ljava/lang/String;
    .locals 2

    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "printer"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 p1, 0x29

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
