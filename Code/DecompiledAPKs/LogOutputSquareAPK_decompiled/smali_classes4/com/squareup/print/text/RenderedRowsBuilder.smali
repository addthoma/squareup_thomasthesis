.class public final Lcom/squareup/print/text/RenderedRowsBuilder;
.super Ljava/lang/Object;
.source "RenderedRowsBuilder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/text/RenderedRowsBuilder$DifferentStylingOnSameLineException;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRenderedRowsBuilder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RenderedRowsBuilder.kt\ncom/squareup/print/text/RenderedRowsBuilder\n*L\n1#1,58:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001:\u0001\u0014B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0006\u001a\u00020\u0007J\u000e\u0010\u0008\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\nJ\u001c\u0010\u000b\u001a\u00020\u00072\u0006\u0010\u000c\u001a\u00020\n2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eJ\u0008\u0010\u0010\u001a\u00020\u0011H\u0002J\u0006\u0010\u0012\u001a\u00020\u0013R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/print/text/RenderedRowsBuilder;",
        "",
        "()V",
        "rows",
        "",
        "Lcom/squareup/print/text/Row;",
        "addNewLine",
        "",
        "addRawChars",
        "chars",
        "",
        "addStyledText",
        "text",
        "styles",
        "",
        "Lcom/squareup/print/text/Style;",
        "enforceDifferentStylesSeparatedByNewLine",
        "",
        "getRenderedRows",
        "Lcom/squareup/print/text/RenderedRows;",
        "DifferentStylingOnSameLineException",
        "hardware_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final rows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/text/Row;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/print/text/RenderedRowsBuilder;->rows:Ljava/util/List;

    return-void
.end method

.method private final enforceDifferentStylesSeparatedByNewLine()V
    .locals 5

    const/4 v0, 0x0

    .line 40
    check-cast v0, Lcom/squareup/print/text/Row$StyledRow;

    .line 41
    iget-object v1, p0, Lcom/squareup/print/text/RenderedRowsBuilder;->rows:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    move-object v2, v0

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/print/text/Row;

    .line 43
    instance-of v4, v3, Lcom/squareup/print/text/Row$StyledRow;

    if-eqz v4, :cond_2

    if-eqz v2, :cond_1

    .line 44
    invoke-virtual {v2}, Lcom/squareup/print/text/Row$StyledRow;->getStyles()Ljava/util/Set;

    move-result-object v2

    move-object v4, v3

    check-cast v4, Lcom/squareup/print/text/Row$StyledRow;

    invoke-virtual {v4}, Lcom/squareup/print/text/Row$StyledRow;->getStyles()Ljava/util/Set;

    move-result-object v4

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_0

    goto :goto_2

    .line 45
    :cond_0
    new-instance v0, Lcom/squareup/print/text/RenderedRowsBuilder$DifferentStylingOnSameLineException;

    iget-object v1, p0, Lcom/squareup/print/text/RenderedRowsBuilder;->rows:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/print/text/RenderedRowsBuilder$DifferentStylingOnSameLineException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 47
    :cond_1
    :goto_2
    check-cast v3, Lcom/squareup/print/text/Row$StyledRow;

    move-object v2, v3

    goto :goto_1

    .line 49
    :cond_2
    sget-object v4, Lcom/squareup/print/text/Row$NewLine;->INSTANCE:Lcom/squareup/print/text/Row$NewLine;

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_0

    .line 50
    :cond_3
    instance-of v3, v3, Lcom/squareup/print/text/Row$RawChars;

    goto :goto_1

    :cond_4
    return-void
.end method


# virtual methods
.method public final addNewLine()Z
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/print/text/RenderedRowsBuilder;->rows:Ljava/util/List;

    sget-object v1, Lcom/squareup/print/text/Row$NewLine;->INSTANCE:Lcom/squareup/print/text/Row$NewLine;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final addRawChars(Ljava/lang/String;)Z
    .locals 2

    const-string v0, "chars"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lcom/squareup/print/text/RenderedRowsBuilder;->rows:Ljava/util/List;

    new-instance v1, Lcom/squareup/print/text/Row$RawChars;

    invoke-direct {v1, p1}, Lcom/squareup/print/text/Row$RawChars;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final addStyledText(Ljava/lang/String;Ljava/util/Set;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "+",
            "Lcom/squareup/print/text/Style;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "styles"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iget-object v0, p0, Lcom/squareup/print/text/RenderedRowsBuilder;->rows:Ljava/util/List;

    new-instance v1, Lcom/squareup/print/text/Row$StyledRow;

    invoke-direct {v1, p1, p2}, Lcom/squareup/print/text/Row$StyledRow;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final getRenderedRows()Lcom/squareup/print/text/RenderedRows;
    .locals 2

    .line 31
    invoke-direct {p0}, Lcom/squareup/print/text/RenderedRowsBuilder;->enforceDifferentStylesSeparatedByNewLine()V

    .line 32
    new-instance v0, Lcom/squareup/print/text/RenderedRows;

    iget-object v1, p0, Lcom/squareup/print/text/RenderedRowsBuilder;->rows:Ljava/util/List;

    invoke-direct {v0, v1}, Lcom/squareup/print/text/RenderedRows;-><init>(Ljava/util/List;)V

    return-object v0
.end method
