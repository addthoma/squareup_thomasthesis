.class public Lcom/squareup/print/HardwarePrintersAnalyticsLogger;
.super Ljava/lang/Object;
.source "HardwarePrintersAnalyticsLogger.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final hardwarePrinterListener:Lcom/squareup/print/HardwarePrinterTracker$Listener;

.field private final hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

.field private final printerStations:Lcom/squareup/print/PrinterStations;


# direct methods
.method constructor <init>(Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/print/PrinterStations;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    .line 27
    iput-object p4, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 29
    new-instance p1, Lcom/squareup/print/HardwarePrintersAnalyticsLogger$1;

    invoke-direct {p1, p0, p2, p3}, Lcom/squareup/print/HardwarePrintersAnalyticsLogger$1;-><init>(Lcom/squareup/print/HardwarePrintersAnalyticsLogger;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V

    iput-object p1, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger;->hardwarePrinterListener:Lcom/squareup/print/HardwarePrinterTracker$Listener;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/print/HardwarePrintersAnalyticsLogger;Lcom/squareup/print/HardwarePrinter;)Ljava/util/List;
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/print/HardwarePrintersAnalyticsLogger;->associatedPrinterStations(Lcom/squareup/print/HardwarePrinter;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private associatedPrinterStations(Lcom/squareup/print/HardwarePrinter;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/HardwarePrinter;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/print/Station;",
            ">;"
        }
    .end annotation

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 62
    iget-object v1, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {v1}, Lcom/squareup/print/PrinterStations;->getAllEnabledStations()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/PrinterStation;

    .line 63
    invoke-virtual {v2}, Lcom/squareup/print/PrinterStation;->hasHardwarePrinterSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 64
    invoke-virtual {v2}, Lcom/squareup/print/PrinterStation;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 65
    new-instance v3, Lcom/squareup/print/Station;

    invoke-direct {v3, v2}, Lcom/squareup/print/Station;-><init>(Lcom/squareup/print/PrinterStation;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 53
    iget-object p1, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    iget-object v0, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger;->hardwarePrinterListener:Lcom/squareup/print/HardwarePrinterTracker$Listener;

    invoke-virtual {p1, v0}, Lcom/squareup/print/HardwarePrinterTracker;->addListener(Lcom/squareup/print/HardwarePrinterTracker$Listener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    iget-object v1, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger;->hardwarePrinterListener:Lcom/squareup/print/HardwarePrinterTracker$Listener;

    invoke-virtual {v0, v1}, Lcom/squareup/print/HardwarePrinterTracker;->removeListener(Lcom/squareup/print/HardwarePrinterTracker$Listener;)V

    return-void
.end method
