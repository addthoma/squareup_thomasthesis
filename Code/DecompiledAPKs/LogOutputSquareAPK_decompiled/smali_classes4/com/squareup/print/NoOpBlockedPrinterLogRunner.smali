.class public final Lcom/squareup/print/NoOpBlockedPrinterLogRunner;
.super Ljava/lang/Object;
.source "BlockedPrinterLogRunner.kt"

# interfaces
.implements Lcom/squareup/print/BlockedPrinterLogRunner;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/print/NoOpBlockedPrinterLogRunner;",
        "Lcom/squareup/print/BlockedPrinterLogRunner;",
        "()V",
        "onJobFinished",
        "",
        "printJob",
        "Lcom/squareup/print/PrintJob;",
        "onJobStarted",
        "hardware_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onJobFinished(Lcom/squareup/print/PrintJob;)V
    .locals 1

    const-string v0, "printJob"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onJobStarted(Lcom/squareup/print/PrintJob;)V
    .locals 1

    const-string v0, "printJob"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
