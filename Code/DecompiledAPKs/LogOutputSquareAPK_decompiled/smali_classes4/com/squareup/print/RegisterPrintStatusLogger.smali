.class public Lcom/squareup/print/RegisterPrintStatusLogger;
.super Ljava/lang/Object;
.source "RegisterPrintStatusLogger.java"

# interfaces
.implements Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/print/RegisterPrintStatusLogger;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private logSuccessfulAttempt(Lcom/squareup/print/PrintJob;)V
    .locals 3

    .line 44
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getPayload()Lcom/squareup/print/PrintablePayload;

    move-result-object v0

    .line 46
    instance-of v1, v0, Lcom/squareup/print/payload/ReceiptPayload;

    if-eqz v1, :cond_3

    .line 47
    check-cast v0, Lcom/squareup/print/payload/ReceiptPayload;

    .line 48
    invoke-virtual {v0}, Lcom/squareup/print/payload/ReceiptPayload;->getReceiptType()Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    move-result-object v0

    .line 49
    sget-object v1, Lcom/squareup/print/RegisterPrintStatusLogger$1;->$SwitchMap$com$squareup$print$payload$ReceiptPayload$ReceiptType:[I

    invoke-virtual {v0}, Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 57
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintStatusLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/print/PrinterStationEvent;->forAuthSlipCopyPrinted(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrinterEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 60
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected receiptType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintStatusLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/print/PrinterStationEvent;->forAuthSlipPrinted(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrinterEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 51
    :cond_2
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintStatusLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/print/PrinterStationEvent;->forReceiptPrinted(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrinterEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 62
    :cond_3
    instance-of v1, v0, Lcom/squareup/print/payload/TicketPayload;

    if-eqz v1, :cond_5

    .line 63
    check-cast v0, Lcom/squareup/print/payload/TicketPayload;

    .line 64
    invoke-virtual {v0}, Lcom/squareup/print/payload/TicketPayload;->isVoidTicket()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 65
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintStatusLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/print/PrinterStationEvent;->forVoidTicketPrinted(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrinterEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 67
    :cond_4
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintStatusLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/print/PrinterStationEvent;->forTicketPrinted(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrinterEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 69
    :cond_5
    instance-of v0, v0, Lcom/squareup/print/payload/StubPayload;

    if-eqz v0, :cond_6

    .line 70
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintStatusLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/print/PrinterStationEvent;->forStubPrinted(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrinterEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_6
    :goto_0
    return-void
.end method


# virtual methods
.method public onPrintJobAttempted(Lcom/squareup/print/PrintJob;)V
    .locals 2

    .line 36
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/print/PrintJob$PrintAttempt;->result:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->SUCCESS:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    if-ne v0, v1, :cond_0

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/print/RegisterPrintStatusLogger;->logSuccessfulAttempt(Lcom/squareup/print/PrintJob;)V

    goto :goto_0

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintStatusLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/print/PrintFinishedEventKt;->forPrintFailed(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrintFinishedEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :goto_0
    return-void
.end method

.method public onPrintJobEnqueued(Lcom/squareup/print/PrintJob;)V
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintStatusLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/print/PrintStatusEventKt;->forPrintEnqueued(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrintStatusEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
