.class public final Lcom/squareup/print/RealTicketAutoIdentifiers_Factory;
.super Ljava/lang/Object;
.source "RealTicketAutoIdentifiers_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/RealTicketAutoIdentifiers;",
        ">;"
    }
.end annotation


# instance fields
.field private final identifierServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/TicketIdentifierService;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketAutoNumberingEnabledProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/TicketIdentifierService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/print/RealTicketAutoIdentifiers_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/print/RealTicketAutoIdentifiers_Factory;->identifierServiceProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/print/RealTicketAutoIdentifiers_Factory;->ticketAutoNumberingEnabledProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/print/RealTicketAutoIdentifiers_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/RealTicketAutoIdentifiers_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/TicketIdentifierService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;)",
            "Lcom/squareup/print/RealTicketAutoIdentifiers_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/print/RealTicketAutoIdentifiers_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/print/RealTicketAutoIdentifiers_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/server/TicketIdentifierService;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/print/PrinterStations;)Lcom/squareup/print/RealTicketAutoIdentifiers;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/server/TicketIdentifierService;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/print/PrinterStations;",
            ")",
            "Lcom/squareup/print/RealTicketAutoIdentifiers;"
        }
    .end annotation

    .line 52
    new-instance v0, Lcom/squareup/print/RealTicketAutoIdentifiers;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/print/RealTicketAutoIdentifiers;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/server/TicketIdentifierService;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/print/PrinterStations;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/print/RealTicketAutoIdentifiers;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/print/RealTicketAutoIdentifiers_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/Transaction;

    iget-object v1, p0, Lcom/squareup/print/RealTicketAutoIdentifiers_Factory;->identifierServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/TicketIdentifierService;

    iget-object v2, p0, Lcom/squareup/print/RealTicketAutoIdentifiers_Factory;->ticketAutoNumberingEnabledProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v3, p0, Lcom/squareup/print/RealTicketAutoIdentifiers_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/print/PrinterStations;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/print/RealTicketAutoIdentifiers_Factory;->newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/server/TicketIdentifierService;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/print/PrinterStations;)Lcom/squareup/print/RealTicketAutoIdentifiers;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/print/RealTicketAutoIdentifiers_Factory;->get()Lcom/squareup/print/RealTicketAutoIdentifiers;

    move-result-object v0

    return-object v0
.end method
