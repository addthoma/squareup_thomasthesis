.class synthetic Lcom/squareup/print/sections/CodesSection$1;
.super Ljava/lang/Object;
.source "CodesSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/sections/CodesSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$checkout$SubtotalType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 56
    invoke-static {}, Lcom/squareup/checkout/SubtotalType;->values()[Lcom/squareup/checkout/SubtotalType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/print/sections/CodesSection$1;->$SwitchMap$com$squareup$checkout$SubtotalType:[I

    :try_start_0
    sget-object v0, Lcom/squareup/print/sections/CodesSection$1;->$SwitchMap$com$squareup$checkout$SubtotalType:[I

    sget-object v1, Lcom/squareup/checkout/SubtotalType;->TAX:Lcom/squareup/checkout/SubtotalType;

    invoke-virtual {v1}, Lcom/squareup/checkout/SubtotalType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/print/sections/CodesSection$1;->$SwitchMap$com$squareup$checkout$SubtotalType:[I

    sget-object v1, Lcom/squareup/checkout/SubtotalType;->INCLUSIVE_TAX:Lcom/squareup/checkout/SubtotalType;

    invoke-virtual {v1}, Lcom/squareup/checkout/SubtotalType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method
