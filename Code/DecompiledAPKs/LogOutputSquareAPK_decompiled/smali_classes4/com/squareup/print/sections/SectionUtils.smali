.class public final Lcom/squareup/print/sections/SectionUtils;
.super Ljava/lang/Object;
.source "SectionUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static calculateTotalNegativeDiscount(Ljava/util/List;ZZ)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;ZZ)J"
        }
    .end annotation

    .line 108
    invoke-static {p0, p1, p2}, Lcom/squareup/print/sections/SectionUtils;->findAdjustments(Ljava/util/List;ZZ)Ljava/util/List;

    move-result-object p0

    .line 110
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const-wide/16 p1, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/OrderAdjustment;

    .line 111
    iget-object v0, v0, Lcom/squareup/payment/OrderAdjustment;->appliedMoney:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long/2addr p1, v0

    goto :goto_0

    .line 115
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Math;->abs(J)J

    move-result-wide p0

    neg-long p0, p0

    return-wide p0
.end method

.method public static calculateTotalPositiveDiscount(Ljava/util/List;ZZ)J
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;ZZ)J"
        }
    .end annotation

    .line 120
    invoke-static {p0, p1, p2}, Lcom/squareup/print/sections/SectionUtils;->calculateTotalNegativeDiscount(Ljava/util/List;ZZ)J

    move-result-wide p0

    neg-long p0, p0

    return-wide p0
.end method

.method public static canShowTaxBreakDownTable(Lcom/squareup/settings/server/AccountStatusSettings;)Z
    .locals 3

    .line 38
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCodeOrNull()Lcom/squareup/CountryCode;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 40
    sget-object v1, Lcom/squareup/print/sections/SectionUtils$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {v0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    goto :goto_0

    :cond_0
    return v1

    .line 47
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldShowFeeBreakdownTableOnReceipts()Z

    move-result p0

    return p0
.end method

.method public static createInclusiveTaxes(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/util/TaxBreakdown;Z)Ljava/lang/String;
    .locals 3

    .line 126
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getOrderAdjustments()Ljava/util/List;

    move-result-object v0

    .line 127
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    .line 128
    invoke-static {v0}, Lcom/squareup/print/sections/SectionUtils;->findInclusiveTaxes(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 130
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz p3, :cond_0

    iget-object p3, p2, Lcom/squareup/util/TaxBreakdown;->taxBreakdownType:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    sget-object v2, Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;->SIMPLE:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    if-eq p3, v2, :cond_0

    goto :goto_0

    .line 135
    :cond_0
    invoke-static {v0}, Lcom/squareup/print/sections/SectionUtils;->findAdditiveTaxes(Ljava/util/List;)Ljava/util/List;

    move-result-object p3

    .line 136
    invoke-static {v1, p1}, Lcom/squareup/print/sections/SectionUtils;->sumAppliedMoney(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 137
    iget-object p2, p2, Lcom/squareup/util/TaxBreakdown;->taxBreakdownType:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    invoke-virtual {p0, p2, p3, v1, p1}, Lcom/squareup/print/ReceiptFormatter;->inclusiveTaxTitleAndAmount(Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    :goto_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static findAdditiveTaxes(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;"
        }
    .end annotation

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 65
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/OrderAdjustment;

    .line 66
    iget-object v2, v1, Lcom/squareup/payment/OrderAdjustment;->subtotalType:Lcom/squareup/checkout/SubtotalType;

    sget-object v3, Lcom/squareup/checkout/SubtotalType;->TAX:Lcom/squareup/checkout/SubtotalType;

    if-ne v2, v3, :cond_0

    .line 67
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static findAdjustments(Ljava/util/List;ZZ)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;ZZ)",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;"
        }
    .end annotation

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 90
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/OrderAdjustment;

    .line 91
    iget-object v2, v1, Lcom/squareup/payment/OrderAdjustment;->subtotalType:Lcom/squareup/checkout/SubtotalType;

    sget-object v3, Lcom/squareup/checkout/SubtotalType;->DISCOUNT:Lcom/squareup/checkout/SubtotalType;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    .line 92
    :goto_1
    iget-object v3, v1, Lcom/squareup/payment/OrderAdjustment;->discountApplicationMethod:Lcom/squareup/api/items/Discount$ApplicationMethod;

    sget-object v6, Lcom/squareup/api/items/Discount$ApplicationMethod;->COMP:Lcom/squareup/api/items/Discount$ApplicationMethod;

    if-ne v3, v6, :cond_1

    goto :goto_2

    :cond_1
    const/4 v4, 0x0

    :goto_2
    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    if-eqz v4, :cond_3

    if-nez p2, :cond_3

    goto :goto_0

    :cond_3
    if-nez v4, :cond_4

    if-nez p1, :cond_4

    goto :goto_0

    .line 97
    :cond_4
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    return-object v0
.end method

.method private static findInclusiveTaxes(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;"
        }
    .end annotation

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 77
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/OrderAdjustment;

    .line 78
    iget-object v2, v1, Lcom/squareup/payment/OrderAdjustment;->subtotalType:Lcom/squareup/checkout/SubtotalType;

    sget-object v3, Lcom/squareup/checkout/SubtotalType;->INCLUSIVE_TAX:Lcom/squareup/checkout/SubtotalType;

    if-ne v2, v3, :cond_0

    .line 79
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static getSeatsVariationsAndModifiersToPrint(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/print/PrintableOrderItem;ZZZLcom/squareup/util/Res;Z)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/ReceiptFormatter;",
            "Lcom/squareup/print/PrintableOrderItem;",
            "ZZZ",
            "Lcom/squareup/util/Res;",
            "Z)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 158
    invoke-interface {p1}, Lcom/squareup/print/PrintableOrderItem;->getDestination()Lcom/squareup/checkout/OrderDestination;

    move-result-object v1

    if-eqz p6, :cond_0

    if-eqz v1, :cond_0

    if-nez p2, :cond_0

    .line 160
    invoke-virtual {v1, p5}, Lcom/squareup/checkout/OrderDestination;->description(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p6

    invoke-interface {v0, p6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    :cond_0
    invoke-interface {p1}, Lcom/squareup/print/PrintableOrderItem;->getShouldShowVariationName()Z

    move-result p6

    if-eqz p6, :cond_2

    if-eqz p3, :cond_1

    .line 165
    invoke-interface {p1}, Lcom/squareup/print/PrintableOrderItem;->getSelectedVariationDisplayName()Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    .line 166
    :cond_1
    invoke-interface {p1}, Lcom/squareup/print/PrintableOrderItem;->getSelectedVariationDisplayName()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1}, Lcom/squareup/print/PrintableOrderItem;->getUnitPrice()Lcom/squareup/protos/common/Money;

    move-result-object p6

    .line 167
    invoke-interface {p1, p5}, Lcom/squareup/print/PrintableOrderItem;->unitAbbreviation(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    .line 166
    invoke-virtual {p0, p3, p6, v1}, Lcom/squareup/print/ReceiptFormatter;->labelAndUnitPrice(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 164
    :goto_0
    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    :cond_2
    invoke-interface {p1}, Lcom/squareup/print/PrintableOrderItem;->getUnitPrice()Lcom/squareup/protos/common/Money;

    move-result-object p3

    const/4 p6, 0x0

    if-nez p3, :cond_3

    move-object p3, p6

    goto :goto_1

    .line 172
    :cond_3
    iget-object p3, p3, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    :goto_1
    if-eqz p3, :cond_4

    const-wide/16 v1, 0x0

    .line 174
    invoke-static {v1, v2, p3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p6

    .line 177
    :cond_4
    invoke-interface {p1}, Lcom/squareup/print/PrintableOrderItem;->getSelectedModifiers()Ljava/util/List;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_2
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrintableOrderItemModifier;

    if-eqz p2, :cond_5

    .line 178
    invoke-interface {v1}, Lcom/squareup/print/PrintableOrderItemModifier;->getHideFromCustomer()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 189
    invoke-interface {v1}, Lcom/squareup/print/PrintableOrderItemModifier;->getBasePriceTimesModifierQuantity()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {p6, v1}, Lcom/squareup/print/sections/SectionUtils;->sumOf(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p6

    goto :goto_2

    .line 192
    :cond_5
    invoke-interface {v1}, Lcom/squareup/print/PrintableOrderItemModifier;->getBasePriceTimesModifierQuantity()Lcom/squareup/protos/common/Money;

    move-result-object v2

    if-nez p4, :cond_6

    .line 195
    invoke-interface {v1, p5}, Lcom/squareup/print/PrintableOrderItemModifier;->getDisplayName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    .line 196
    invoke-interface {p1, p5}, Lcom/squareup/print/PrintableOrderItem;->unitAbbreviation(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v3

    .line 195
    invoke-virtual {p0, v1, v2, v3}, Lcom/squareup/print/ReceiptFormatter;->labelAndUnitPrice(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 194
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 198
    :cond_6
    invoke-interface {v1, p5}, Lcom/squareup/print/PrintableOrderItemModifier;->getDisplayName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 204
    :cond_7
    invoke-static {p6}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result p2

    if-eqz p2, :cond_8

    .line 207
    invoke-interface {p1, p5}, Lcom/squareup/print/PrintableOrderItem;->unitAbbreviation(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    .line 206
    invoke-virtual {p0, p6, p1}, Lcom/squareup/print/ReceiptFormatter;->additionsLabelAndUnitPrice(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 205
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    :cond_8
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static hasBothTaxedAndUntaxedItems(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)Z"
        }
    .end annotation

    .line 54
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 55
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->isTaxed()Z

    move-result v5

    or-int/2addr v1, v5

    .line 56
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->isTaxed()Z

    move-result v3

    xor-int/2addr v3, v4

    or-int/2addr v2, v3

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static isAustralia(Lcom/squareup/settings/server/AccountStatusSettings;)Z
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/settings/server/UserSettings;->getCountryCodeOrNull()Lcom/squareup/CountryCode;

    move-result-object p0

    sget-object v0, Lcom/squareup/CountryCode;->AU:Lcom/squareup/CountryCode;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static sumAppliedMoney(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    const-wide/16 v0, 0x0

    .line 230
    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 232
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/OrderAdjustment;

    .line 233
    iget-object v0, v0, Lcom/squareup/payment/OrderAdjustment;->appliedMoney:Lcom/squareup/protos/common/Money;

    invoke-static {p1, v0}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    :cond_0
    return-object p1
.end method

.method static sumOf(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 0

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 220
    :try_start_0
    invoke-static {p0, p1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p0, 0x0

    :cond_0
    :goto_0
    return-object p0
.end method
