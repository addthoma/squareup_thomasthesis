.class public final Lcom/squareup/print/PrinterEvent$AttemptCountPrinterEvent;
.super Lcom/squareup/print/PrinterEvent;
.source "PrinterEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PrinterEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AttemptCountPrinterEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u001f\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/print/PrinterEvent$AttemptCountPrinterEvent;",
        "Lcom/squareup/print/PrinterEvent;",
        "name",
        "Lcom/squareup/eventstream/v1/EventStream$Name;",
        "value",
        "",
        "attemptCount",
        "",
        "(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;I)V",
        "getAttemptCount",
        "()I",
        "hardware_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final attemptCount:I


# direct methods
.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;I)V
    .locals 12

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xfc

    const/4 v11, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    .line 39
    invoke-direct/range {v1 .. v11}, Lcom/squareup/print/PrinterEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p3, p0, Lcom/squareup/print/PrinterEvent$AttemptCountPrinterEvent;->attemptCount:I

    return-void
.end method


# virtual methods
.method public final getAttemptCount()I
    .locals 1

    .line 38
    iget v0, p0, Lcom/squareup/print/PrinterEvent$AttemptCountPrinterEvent;->attemptCount:I

    return v0
.end method
