.class public interface abstract Lcom/squareup/print/PrintCallback;
.super Ljava/lang/Object;
.source "PrintCallback.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PrintCallback$PrintCallbackResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract call(Lcom/squareup/print/PrintCallback$PrintCallbackResult;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintCallback$PrintCallbackResult<",
            "TT;>;)V"
        }
    .end annotation
.end method
