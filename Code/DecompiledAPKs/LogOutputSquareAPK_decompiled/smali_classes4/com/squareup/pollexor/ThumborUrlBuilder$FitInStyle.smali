.class public final enum Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;
.super Ljava/lang/Enum;
.source "ThumborUrlBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/pollexor/ThumborUrlBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FitInStyle"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

.field public static final enum ADAPTIVE:Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

.field public static final enum FULL:Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

.field public static final enum NORMAL:Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;


# instance fields
.field final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 90
    new-instance v0, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    const/4 v1, 0x0

    const-string v2, "NORMAL"

    const-string v3, "fit-in"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;->NORMAL:Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    new-instance v0, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    const/4 v2, 0x1

    const-string v3, "FULL"

    const-string v4, "full-fit-in"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;->FULL:Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    new-instance v0, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    const/4 v3, 0x2

    const-string v4, "ADAPTIVE"

    const-string v5, "adaptive-fit-in"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;->ADAPTIVE:Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    .line 89
    sget-object v4, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;->NORMAL:Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;->FULL:Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;->ADAPTIVE:Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;->$VALUES:[Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 95
    iput-object p3, p0, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;
    .locals 1

    .line 89
    const-class v0, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    return-object p0
.end method

.method public static values()[Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;
    .locals 1

    .line 89
    sget-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;->$VALUES:[Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    invoke-virtual {v0}, [Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    return-object v0
.end method
