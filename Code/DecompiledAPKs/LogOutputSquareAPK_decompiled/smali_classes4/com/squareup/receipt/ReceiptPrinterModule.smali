.class public abstract Lcom/squareup/receipt/ReceiptPrinterModule;
.super Ljava/lang/Object;
.source "ReceiptPrinterModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideReceiptSender(Lcom/squareup/receipt/RealReceiptSender;)Lcom/squareup/receipt/ReceiptSender;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
