.class public final synthetic Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$2SE-3EQBCvGQ_PgdTT_RERR6onM;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic f$0:Lcom/squareup/receipt/ReceiptValidator$ReceiptNumberReadyCallback;

.field private final synthetic f$1:Lcom/squareup/payment/PaymentReceipt;

.field private final synthetic f$2:Lcom/squareup/payment/tender/BaseTender;

.field private final synthetic f$3:Lcom/squareup/print/payload/ReceiptPayload$RenderType;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/receipt/ReceiptValidator$ReceiptNumberReadyCallback;Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$2SE-3EQBCvGQ_PgdTT_RERR6onM;->f$0:Lcom/squareup/receipt/ReceiptValidator$ReceiptNumberReadyCallback;

    iput-object p2, p0, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$2SE-3EQBCvGQ_PgdTT_RERR6onM;->f$1:Lcom/squareup/payment/PaymentReceipt;

    iput-object p3, p0, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$2SE-3EQBCvGQ_PgdTT_RERR6onM;->f$2:Lcom/squareup/payment/tender/BaseTender;

    iput-object p4, p0, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$2SE-3EQBCvGQ_PgdTT_RERR6onM;->f$3:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$2SE-3EQBCvGQ_PgdTT_RERR6onM;->f$0:Lcom/squareup/receipt/ReceiptValidator$ReceiptNumberReadyCallback;

    iget-object v1, p0, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$2SE-3EQBCvGQ_PgdTT_RERR6onM;->f$1:Lcom/squareup/payment/PaymentReceipt;

    iget-object v2, p0, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$2SE-3EQBCvGQ_PgdTT_RERR6onM;->f$2:Lcom/squareup/payment/tender/BaseTender;

    iget-object v3, p0, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$2SE-3EQBCvGQ_PgdTT_RERR6onM;->f$3:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/receipt/ReceiptValidator;->lambda$waitForReceiptNumber$0(Lcom/squareup/receipt/ReceiptValidator$ReceiptNumberReadyCallback;Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V

    return-void
.end method
