.class synthetic Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;
.super Ljava/lang/Object;
.source "ReaderStatusAndMessageBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/messagebar/ReaderStatusAndMessageBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$messagebar$ReaderStatusAndMessageBar$BarAnimationType:[I

.field static final synthetic $SwitchMap$com$squareup$messagebar$ReaderStatusAndMessageBar$MessageAnimationType:[I

.field static final synthetic $SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Level:[I

.field static final synthetic $SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 668
    invoke-static {}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->values()[Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$messagebar$ReaderStatusAndMessageBar$MessageAnimationType:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$messagebar$ReaderStatusAndMessageBar$MessageAnimationType:[I

    sget-object v2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->IGNORE_MESSAGE:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    invoke-virtual {v2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$messagebar$ReaderStatusAndMessageBar$MessageAnimationType:[I

    sget-object v3, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->SHOW_IMMEDIATELY:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    invoke-virtual {v3}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$messagebar$ReaderStatusAndMessageBar$MessageAnimationType:[I

    sget-object v4, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->CYCLE_IN_FROM_TOP:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    invoke-virtual {v4}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    .line 647
    :catch_2
    invoke-static {}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->values()[Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$messagebar$ReaderStatusAndMessageBar$BarAnimationType:[I

    :try_start_3
    sget-object v3, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$messagebar$ReaderStatusAndMessageBar$BarAnimationType:[I

    sget-object v4, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->IGNORE_BAR:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    invoke-virtual {v4}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v3, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$messagebar$ReaderStatusAndMessageBar$BarAnimationType:[I

    sget-object v4, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->HIDE_IMMEDIATELY:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    invoke-virtual {v4}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->ordinal()I

    move-result v4

    aput v1, v3, v4
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v3, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$messagebar$ReaderStatusAndMessageBar$BarAnimationType:[I

    sget-object v4, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->CYCLE_IN_IF_GONE:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    invoke-virtual {v4}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    const/4 v3, 0x4

    :try_start_6
    sget-object v4, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$messagebar$ReaderStatusAndMessageBar$BarAnimationType:[I

    sget-object v5, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->CYCLE_OUT_TO_TOP:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    invoke-virtual {v5}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    .line 271
    :catch_6
    invoke-static {}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->values()[Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    move-result-object v4

    array-length v4, v4

    new-array v4, v4, [I

    sput-object v4, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Level:[I

    :try_start_7
    sget-object v4, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Level:[I

    sget-object v5, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_UPDATING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    invoke-virtual {v5}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->ordinal()I

    move-result v5

    aput v0, v4, v5
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v4, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Level:[I

    sget-object v5, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_PERMISSION_MISSING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    invoke-virtual {v5}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->ordinal()I

    move-result v5

    aput v1, v4, v5
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    :try_start_9
    sget-object v4, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Level:[I

    sget-object v5, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    invoke-virtual {v5}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->ordinal()I

    move-result v5

    aput v2, v4, v5
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v4, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Level:[I

    sget-object v5, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    invoke-virtual {v5}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    const/4 v4, 0x5

    :try_start_b
    sget-object v5, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Level:[I

    sget-object v6, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    invoke-virtual {v6}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->ordinal()I

    move-result v6

    aput v4, v5, v6
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :catch_b
    const/4 v5, 0x6

    :try_start_c
    sget-object v6, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Level:[I

    sget-object v7, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    invoke-virtual {v7}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->ordinal()I

    move-result v7

    aput v5, v6, v7
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_c

    .line 151
    :catch_c
    invoke-static {}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->values()[Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    move-result-object v6

    array-length v6, v6

    new-array v6, v6, [I

    sput-object v6, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    :try_start_d
    sget-object v6, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v7, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_CHECKING_SERVER_FOR_FWUP:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v7}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v7

    aput v0, v6, v7
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_d

    :catch_d
    :try_start_e
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v6, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_KEY_INJECTION_STARTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v6}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v6

    aput v1, v0, v6
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_e

    :catch_e
    :try_start_f
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_IS_FWUP_REBOOTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_f

    :catch_f
    :try_start_10
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_APPLYING_BLOCKING_AND_REBOOTING_FWUP_BUNDLE:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    aput v3, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_10

    :catch_10
    :try_start_11
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_APPLYING_BLOCKING_FWUP_BUNDLE:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    aput v4, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_11

    :catch_11
    :try_start_12
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_MIC_PERMISSION_MISSING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    aput v5, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_12

    :catch_12
    :try_start_13
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_KEY_INJECTION_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_13

    :catch_13
    :try_start_14
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_TAMPERED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :catch_14
    :try_start_15
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_FWUP_ERROR:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_15

    :catch_15
    :try_start_16
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_SERVER_DENIED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_16

    :catch_16
    :try_start_17
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_17

    :catch_17
    :try_start_18
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BATTERY_DEAD:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_18

    :catch_18
    :try_start_19
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_STILL_BLUETOOTH_PAIRING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_19

    :catch_19
    :try_start_1a
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_STILL_BLE_PAIRING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_1a

    :catch_1a
    :try_start_1b
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_RENEWING_SESSION:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_1b

    :catch_1b
    :try_start_1c
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_COMMS_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_1c

    :catch_1c
    :try_start_1d
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_ESTABLISHING_SESSION:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_1d

    :catch_1d
    :try_start_1e
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BATTERY_LOW:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_1e

    :catch_1e
    :try_start_1f
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_OFFLINE_SWIPE_ONLY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_1f

    :catch_1f
    :try_start_20
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_20

    :catch_20
    :try_start_21
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BLE_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_21

    :catch_21
    return-void
.end method
