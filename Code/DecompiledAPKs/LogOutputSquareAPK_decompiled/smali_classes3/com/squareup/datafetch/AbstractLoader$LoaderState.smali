.class public abstract Lcom/squareup/datafetch/AbstractLoader$LoaderState;
.super Ljava/lang/Object;
.source "AbstractLoader.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/datafetch/AbstractLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "LoaderState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;,
        Lcom/squareup/datafetch/AbstractLoader$LoaderState$Failure;,
        Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TInput:",
        "Ljava/lang/Object;",
        "TItem:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000*\u0004\u0008\u0002\u0010\u0001*\u0004\u0008\u0003\u0010\u00022\u00020\u0003:\u0003\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004R\u0012\u0010\u0005\u001a\u00028\u0002X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007\u0082\u0001\u0003\u000b\u000c\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/datafetch/AbstractLoader$LoaderState;",
        "TInput",
        "TItem",
        "",
        "()V",
        "input",
        "getInput",
        "()Ljava/lang/Object;",
        "Failure",
        "Progress",
        "Results",
        "Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;",
        "Lcom/squareup/datafetch/AbstractLoader$LoaderState$Failure;",
        "Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;",
        "data-fetch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 234
    invoke-direct {p0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getInput()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTInput;"
        }
    .end annotation
.end method
