.class public final Lcom/squareup/datafetch/AbstractLoader$PagingParams;
.super Ljava/lang/Object;
.source "AbstractLoader.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/datafetch/AbstractLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PagingParams"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u000f\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0019\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000bJ&\u0010\u0011\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u00c6\u0001\u00a2\u0006\u0002\u0010\u0012J\u0013\u0010\u0013\u001a\u00020\u00082\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\tR\u0015\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\n\n\u0002\u0010\u000c\u001a\u0004\u0008\n\u0010\u000bR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/datafetch/AbstractLoader$PagingParams;",
        "",
        "pagingKey",
        "",
        "pageSize",
        "",
        "(Ljava/lang/String;Ljava/lang/Integer;)V",
        "isFirstPage",
        "",
        "()Z",
        "getPageSize",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "getPagingKey",
        "()Ljava/lang/String;",
        "component1",
        "component2",
        "copy",
        "(Ljava/lang/String;Ljava/lang/Integer;)Lcom/squareup/datafetch/AbstractLoader$PagingParams;",
        "equals",
        "other",
        "hashCode",
        "toString",
        "data-fetch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final isFirstPage:Z

.field private final pageSize:Ljava/lang/Integer;

.field private final pagingKey:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0

    .line 314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pageSize:Ljava/lang/Integer;

    .line 318
    iget-object p1, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->isFirstPage:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/datafetch/AbstractLoader$PagingParams;Ljava/lang/String;Ljava/lang/Integer;ILjava/lang/Object;)Lcom/squareup/datafetch/AbstractLoader$PagingParams;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pageSize:Ljava/lang/Integer;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->copy(Ljava/lang/String;Ljava/lang/Integer;)Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pageSize:Ljava/lang/Integer;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/Integer;)Lcom/squareup/datafetch/AbstractLoader$PagingParams;
    .locals 1

    new-instance v0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    invoke-direct {v0, p1, p2}, Lcom/squareup/datafetch/AbstractLoader$PagingParams;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pageSize:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pageSize:Ljava/lang/Integer;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPageSize()Ljava/lang/Integer;
    .locals 1

    .line 316
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pageSize:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getPagingKey()Ljava/lang/String;
    .locals 1

    .line 315
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pageSize:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final isFirstPage()Z
    .locals 1

    .line 318
    iget-boolean v0, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->isFirstPage:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PagingParams(pagingKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", pageSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->pageSize:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
