.class public final Lcom/squareup/datafetch/AbstractLoader$Response$Success;
.super Lcom/squareup/datafetch/AbstractLoader$Response;
.source "AbstractLoader.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/datafetch/AbstractLoader$Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Success"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TInput:",
        "Ljava/lang/Object;",
        "TItem:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/datafetch/AbstractLoader$Response<",
        "TTInput;TTItem;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0011\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000*\u0004\u0008\u0004\u0010\u0001*\u0004\u0008\u0005\u0010\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u0003B/\u0012\u0006\u0010\u0004\u001a\u00028\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u000e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00028\u0005\u0018\u00010\u0008\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u000bJ\u000e\u0010\u0015\u001a\u00028\u0004H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000fJ\t\u0010\u0016\u001a\u00020\u0006H\u00c6\u0003J\u0011\u0010\u0017\u001a\n\u0012\u0004\u0012\u00028\u0005\u0018\u00010\u0008H\u00c6\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\nH\u00c6\u0003JL\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u00050\u00002\u0008\u0008\u0002\u0010\u0004\u001a\u00028\u00042\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0010\u0008\u0002\u0010\u0007\u001a\n\u0012\u0004\u0012\u00028\u0005\u0018\u00010\u00082\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\nH\u00c6\u0001\u00a2\u0006\u0002\u0010\u001aJ\u0013\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u00d6\u0003J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001J\t\u0010!\u001a\u00020\nH\u00d6\u0001R\u0019\u0010\u0007\u001a\n\u0012\u0004\u0012\u00028\u0005\u0018\u00010\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0016\u0010\u0004\u001a\u00028\u0004X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\u0008\u000e\u0010\u000fR\u0013\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/datafetch/AbstractLoader$Response$Success;",
        "TInput",
        "TItem",
        "Lcom/squareup/datafetch/AbstractLoader$Response;",
        "input",
        "pagingParams",
        "Lcom/squareup/datafetch/AbstractLoader$PagingParams;",
        "fetchedItems",
        "",
        "nextPagingKey",
        "",
        "(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Ljava/util/List;Ljava/lang/String;)V",
        "getFetchedItems",
        "()Ljava/util/List;",
        "getInput",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "getNextPagingKey",
        "()Ljava/lang/String;",
        "getPagingParams",
        "()Lcom/squareup/datafetch/AbstractLoader$PagingParams;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/datafetch/AbstractLoader$Response$Success;",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "data-fetch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final fetchedItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TTItem;>;"
        }
    .end annotation
.end field

.field private final input:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTInput;"
        }
    .end annotation
.end field

.field private final nextPagingKey:Ljava/lang/String;

.field private final pagingParams:Lcom/squareup/datafetch/AbstractLoader$PagingParams;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTInput;",
            "Lcom/squareup/datafetch/AbstractLoader$PagingParams;",
            "Ljava/util/List<",
            "+TTItem;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "pagingParams"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 302
    invoke-direct {p0, v0}, Lcom/squareup/datafetch/AbstractLoader$Response;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->input:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->pagingParams:Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    iput-object p3, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->fetchedItems:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->nextPagingKey:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/datafetch/AbstractLoader$Response$Success;Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Ljava/util/List;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/datafetch/AbstractLoader$Response$Success;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->getInput()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->getPagingParams()Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    move-result-object p2

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->fetchedItems:Ljava/util/List;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->nextPagingKey:Ljava/lang/String;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->copy(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/datafetch/AbstractLoader$Response$Success;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTInput;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->getInput()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/datafetch/AbstractLoader$PagingParams;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->getPagingParams()Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TTItem;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->fetchedItems:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->nextPagingKey:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/datafetch/AbstractLoader$Response$Success;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTInput;",
            "Lcom/squareup/datafetch/AbstractLoader$PagingParams;",
            "Ljava/util/List<",
            "+TTItem;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/datafetch/AbstractLoader$Response$Success<",
            "TTInput;TTItem;>;"
        }
    .end annotation

    const-string v0, "pagingParams"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/datafetch/AbstractLoader$Response$Success;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/datafetch/AbstractLoader$Response$Success;

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->getInput()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->getInput()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->getPagingParams()Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->getPagingParams()Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->fetchedItems:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->fetchedItems:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->nextPagingKey:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->nextPagingKey:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFetchedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TTItem;>;"
        }
    .end annotation

    .line 300
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->fetchedItems:Ljava/util/List;

    return-object v0
.end method

.method public getInput()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTInput;"
        }
    .end annotation

    .line 298
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->input:Ljava/lang/Object;

    return-object v0
.end method

.method public final getNextPagingKey()Ljava/lang/String;
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->nextPagingKey:Ljava/lang/String;

    return-object v0
.end method

.method public getPagingParams()Lcom/squareup/datafetch/AbstractLoader$PagingParams;
    .locals 1

    .line 299
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->pagingParams:Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->getInput()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->getPagingParams()Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->fetchedItems:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->nextPagingKey:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Success(input="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->getInput()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", pagingParams="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->getPagingParams()Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", fetchedItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->fetchedItems:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", nextPagingKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->nextPagingKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
