.class public final Lcom/squareup/filepicker/RealFilePicker;
.super Ljava/lang/Object;
.source "RealFilePicker.kt"

# interfaces
.implements Lcom/squareup/filepicker/FilePicker;
.implements Lcom/squareup/ui/ActivityDelegate;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/filepicker/RealFilePicker$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealFilePicker.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealFilePicker.kt\ncom/squareup/filepicker/RealFilePicker\n*L\n1#1,77:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000 \u00172\u00020\u00012\u00020\u0002:\u0001\u0017B\u0017\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010\n\u001a\u00020\u000bH\u0016J!\u0010\u000c\u001a\u00020\r2\u0012\u0010\u000e\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00100\u000f\"\u00020\u0010H\u0016\u00a2\u0006\u0002\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\r2\u0006\u0010\u0008\u001a\u00020\tH\u0016J\u0010\u0010\u0013\u001a\u00020\r2\u0006\u0010\u0008\u001a\u00020\tH\u0016J\u000e\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015H\u0016R\u0010\u0010\u0008\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/filepicker/RealFilePicker;",
        "Lcom/squareup/filepicker/FilePicker;",
        "Lcom/squareup/ui/ActivityDelegate;",
        "activityResultHandler",
        "Lcom/squareup/ui/ActivityResultHandler;",
        "intentAvailabilityManager",
        "Lcom/squareup/util/IntentAvailabilityManager;",
        "(Lcom/squareup/ui/ActivityResultHandler;Lcom/squareup/util/IntentAvailabilityManager;)V",
        "activity",
        "Landroid/app/Activity;",
        "isAvailable",
        "",
        "launch",
        "",
        "mimeTypes",
        "",
        "",
        "([Ljava/lang/String;)V",
        "onCreate",
        "onDestroy",
        "results",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/filepicker/FilePickerResult;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/filepicker/RealFilePicker$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private activity:Landroid/app/Activity;

.field private final activityResultHandler:Lcom/squareup/ui/ActivityResultHandler;

.field private final intentAvailabilityManager:Lcom/squareup/util/IntentAvailabilityManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/filepicker/RealFilePicker$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/filepicker/RealFilePicker$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/filepicker/RealFilePicker;->Companion:Lcom/squareup/filepicker/RealFilePicker$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/ActivityResultHandler;Lcom/squareup/util/IntentAvailabilityManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "activityResultHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intentAvailabilityManager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/filepicker/RealFilePicker;->activityResultHandler:Lcom/squareup/ui/ActivityResultHandler;

    iput-object p2, p0, Lcom/squareup/filepicker/RealFilePicker;->intentAvailabilityManager:Lcom/squareup/util/IntentAvailabilityManager;

    return-void
.end method


# virtual methods
.method public isAvailable()Z
    .locals 3

    .line 50
    iget-object v0, p0, Lcom/squareup/filepicker/RealFilePicker;->intentAvailabilityManager:Lcom/squareup/util/IntentAvailabilityManager;

    sget-object v1, Lcom/squareup/filepicker/RealFilePicker;->Companion:Lcom/squareup/filepicker/RealFilePicker$Companion;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/squareup/filepicker/RealFilePicker$Companion;->access$createIntent(Lcom/squareup/filepicker/RealFilePicker$Companion;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/util/IntentAvailabilityManager;->isAvailable(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public varargs launch([Ljava/lang/String;)V
    .locals 3

    const-string v0, "mimeTypes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/squareup/filepicker/RealFilePicker;->activity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 32
    sget-object v1, Lcom/squareup/filepicker/RealFilePicker;->Companion:Lcom/squareup/filepicker/RealFilePicker$Companion;

    array-length v2, p1

    invoke-static {p1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/squareup/filepicker/RealFilePicker$Companion;->access$createIntent(Lcom/squareup/filepicker/RealFilePicker$Companion;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    const/4 v1, 0x4

    .line 34
    invoke-virtual {v0, p1, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    .line 30
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Don\'t have a valid Activity; can\'t start File Picker."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public onCreate(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iput-object p1, p0, Lcom/squareup/filepicker/RealFilePicker;->activity:Landroid/app/Activity;

    return-void
.end method

.method public onDestroy(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/filepicker/RealFilePicker;->activity:Landroid/app/Activity;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 59
    check-cast p1, Landroid/app/Activity;

    iput-object p1, p0, Lcom/squareup/filepicker/RealFilePicker;->activity:Landroid/app/Activity;

    :cond_0
    return-void
.end method

.method public results()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/filepicker/FilePickerResult;",
            ">;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/filepicker/RealFilePicker;->activityResultHandler:Lcom/squareup/ui/ActivityResultHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/ActivityResultHandler;->results()Lio/reactivex/Observable;

    move-result-object v0

    .line 39
    sget-object v1, Lcom/squareup/filepicker/RealFilePicker$results$1;->INSTANCE:Lcom/squareup/filepicker/RealFilePicker$results$1;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 40
    sget-object v1, Lcom/squareup/filepicker/RealFilePicker$results$2;->INSTANCE:Lcom/squareup/filepicker/RealFilePicker$results$2;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "activityResultHandler.re\u2026!!)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
