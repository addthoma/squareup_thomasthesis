.class public final enum Lcom/squareup/onboarding/BannerButton$Kind;
.super Ljava/lang/Enum;
.source "BannerButton.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/BannerButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Kind"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/onboarding/BannerButton$Kind;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000f\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B!\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0006R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0004\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0008R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u0003X\u0080\u0004\u00a2\u0006\n\n\u0002\u0010\u000c\u001a\u0004\u0008\n\u0010\u000bj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/onboarding/BannerButton$Kind;",
        "",
        "callToAction",
        "",
        "subtitle",
        "url",
        "(Ljava/lang/String;IIILjava/lang/Integer;)V",
        "getCallToAction$onboarding_release",
        "()I",
        "getSubtitle$onboarding_release",
        "getUrl$onboarding_release",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "NONE",
        "ACTIVATE_ACCOUNT",
        "FINALIZE_ACCOUNT_SETUP",
        "LINK_BANK_ACCOUNT_IN_APP",
        "LINK_BANK_ACCOUNT_ON_WEB",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/onboarding/BannerButton$Kind;

.field public static final enum ACTIVATE_ACCOUNT:Lcom/squareup/onboarding/BannerButton$Kind;

.field public static final enum FINALIZE_ACCOUNT_SETUP:Lcom/squareup/onboarding/BannerButton$Kind;

.field public static final enum LINK_BANK_ACCOUNT_IN_APP:Lcom/squareup/onboarding/BannerButton$Kind;

.field public static final enum LINK_BANK_ACCOUNT_ON_WEB:Lcom/squareup/onboarding/BannerButton$Kind;

.field public static final enum NONE:Lcom/squareup/onboarding/BannerButton$Kind;


# instance fields
.field private final callToAction:I

.field private final subtitle:I

.field private final url:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/onboarding/BannerButton$Kind;

    new-instance v7, Lcom/squareup/onboarding/BannerButton$Kind;

    const-string v2, "NONE"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, v7

    .line 35
    invoke-direct/range {v1 .. v6}, Lcom/squareup/onboarding/BannerButton$Kind;-><init>(Ljava/lang/String;IIILjava/lang/Integer;)V

    sput-object v7, Lcom/squareup/onboarding/BannerButton$Kind;->NONE:Lcom/squareup/onboarding/BannerButton$Kind;

    const/4 v1, 0x0

    aput-object v7, v0, v1

    new-instance v1, Lcom/squareup/onboarding/BannerButton$Kind;

    .line 37
    sget v11, Lcom/squareup/onboarding/common/R$string;->activation_account_title:I

    .line 38
    sget v12, Lcom/squareup/onboarding/common/R$string;->activation_call_to_action_no_free_reader:I

    const-string v9, "ACTIVATE_ACCOUNT"

    const/4 v10, 0x1

    const/4 v13, 0x0

    move-object v8, v1

    .line 36
    invoke-direct/range {v8 .. v13}, Lcom/squareup/onboarding/BannerButton$Kind;-><init>(Ljava/lang/String;IIILjava/lang/Integer;)V

    sput-object v1, Lcom/squareup/onboarding/BannerButton$Kind;->ACTIVATE_ACCOUNT:Lcom/squareup/onboarding/BannerButton$Kind;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onboarding/BannerButton$Kind;

    .line 42
    sget v6, Lcom/squareup/onboarding/common/R$string;->finalize_account_setup:I

    .line 43
    sget v7, Lcom/squareup/onboarding/common/R$string;->world_finish_account_subtitle:I

    const-string v4, "FINALIZE_ACCOUNT_SETUP"

    const/4 v5, 0x2

    const/4 v8, 0x0

    move-object v3, v1

    .line 41
    invoke-direct/range {v3 .. v8}, Lcom/squareup/onboarding/BannerButton$Kind;-><init>(Ljava/lang/String;IIILjava/lang/Integer;)V

    sput-object v1, Lcom/squareup/onboarding/BannerButton$Kind;->FINALIZE_ACCOUNT_SETUP:Lcom/squareup/onboarding/BannerButton$Kind;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onboarding/BannerButton$Kind;

    .line 47
    sget v6, Lcom/squareup/onboarding/common/R$string;->add_bank_account:I

    .line 48
    sget v7, Lcom/squareup/onboarding/common/R$string;->bank_call_to_action:I

    const-string v4, "LINK_BANK_ACCOUNT_IN_APP"

    const/4 v5, 0x3

    move-object v3, v1

    .line 46
    invoke-direct/range {v3 .. v8}, Lcom/squareup/onboarding/BannerButton$Kind;-><init>(Ljava/lang/String;IIILjava/lang/Integer;)V

    sput-object v1, Lcom/squareup/onboarding/BannerButton$Kind;->LINK_BANK_ACCOUNT_IN_APP:Lcom/squareup/onboarding/BannerButton$Kind;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onboarding/BannerButton$Kind;

    .line 52
    sget v6, Lcom/squareup/onboarding/common/R$string;->add_bank_account:I

    .line 53
    sget v7, Lcom/squareup/onboarding/common/R$string;->bank_call_to_action:I

    .line 54
    sget v2, Lcom/squareup/banklinking/R$string;->link_bank_account_url:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const-string v4, "LINK_BANK_ACCOUNT_ON_WEB"

    const/4 v5, 0x4

    move-object v3, v1

    .line 51
    invoke-direct/range {v3 .. v8}, Lcom/squareup/onboarding/BannerButton$Kind;-><init>(Ljava/lang/String;IIILjava/lang/Integer;)V

    sput-object v1, Lcom/squareup/onboarding/BannerButton$Kind;->LINK_BANK_ACCOUNT_ON_WEB:Lcom/squareup/onboarding/BannerButton$Kind;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/onboarding/BannerButton$Kind;->$VALUES:[Lcom/squareup/onboarding/BannerButton$Kind;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILjava/lang/Integer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/onboarding/BannerButton$Kind;->callToAction:I

    iput p4, p0, Lcom/squareup/onboarding/BannerButton$Kind;->subtitle:I

    iput-object p5, p0, Lcom/squareup/onboarding/BannerButton$Kind;->url:Ljava/lang/Integer;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/onboarding/BannerButton$Kind;
    .locals 1

    const-class v0, Lcom/squareup/onboarding/BannerButton$Kind;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/onboarding/BannerButton$Kind;

    return-object p0
.end method

.method public static values()[Lcom/squareup/onboarding/BannerButton$Kind;
    .locals 1

    sget-object v0, Lcom/squareup/onboarding/BannerButton$Kind;->$VALUES:[Lcom/squareup/onboarding/BannerButton$Kind;

    invoke-virtual {v0}, [Lcom/squareup/onboarding/BannerButton$Kind;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/onboarding/BannerButton$Kind;

    return-object v0
.end method


# virtual methods
.method public final getCallToAction$onboarding_release()I
    .locals 1

    .line 31
    iget v0, p0, Lcom/squareup/onboarding/BannerButton$Kind;->callToAction:I

    return v0
.end method

.method public final getSubtitle$onboarding_release()I
    .locals 1

    .line 32
    iget v0, p0, Lcom/squareup/onboarding/BannerButton$Kind;->subtitle:I

    return v0
.end method

.method public final getUrl$onboarding_release()Ljava/lang/Integer;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/onboarding/BannerButton$Kind;->url:Ljava/lang/Integer;

    return-object v0
.end method
