.class public final Lcom/squareup/onboarding/OnboardingWorkflowRunner$NoOp;
.super Ljava/lang/Object;
.source "OnboardingWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/onboarding/OnboardingWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/OnboardingWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoOp"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingWorkflowRunner.kt\ncom/squareup/onboarding/OnboardingWorkflowRunner$NoOp\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,36:1\n151#2,2:37\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingWorkflowRunner.kt\ncom/squareup/onboarding/OnboardingWorkflowRunner$NoOp\n*L\n30#1,2:37\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0011\u0010\u0003\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00050\u0004H\u0096\u0001J\u0015\u0010\u0006\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00070\u0004H\u0096\u0001J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0011\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000fH\u0096\u0001\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/onboarding/OnboardingWorkflowRunner$NoOp;",
        "Lcom/squareup/onboarding/OnboardingWorkflowRunner;",
        "()V",
        "onResult",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
        "onUpdateScreens",
        "",
        "Lcom/squareup/container/WorkflowTreeKey;",
        "registerServices",
        "",
        "scopeBuilder",
        "Lmortar/MortarScope$Builder;",
        "startWorkflow",
        "flow",
        "Lcom/squareup/onboarding/OnboardingFlow;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/onboarding/OnboardingWorkflowRunner;


# direct methods
.method public constructor <init>()V
    .locals 5

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 37
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 38
    const-class v2, Lcom/squareup/onboarding/OnboardingWorkflowRunner;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/onboarding/OnboardingWorkflowRunner;

    iput-object v0, p0, Lcom/squareup/onboarding/OnboardingWorkflowRunner$NoOp;->$$delegate_0:Lcom/squareup/onboarding/OnboardingWorkflowRunner;

    return-void
.end method


# virtual methods
.method public onResult()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingWorkflowRunner$NoOp;->$$delegate_0:Lcom/squareup/onboarding/OnboardingWorkflowRunner;

    invoke-interface {v0}, Lcom/squareup/onboarding/OnboardingWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onUpdateScreens()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/container/WorkflowTreeKey;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingWorkflowRunner$NoOp;->$$delegate_0:Lcom/squareup/onboarding/OnboardingWorkflowRunner;

    invoke-interface {v0}, Lcom/squareup/onboarding/OnboardingWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public registerServices(Lmortar/MortarScope$Builder;)V
    .locals 1

    const-string v0, "scopeBuilder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public startWorkflow(Lcom/squareup/onboarding/OnboardingFlow;)V
    .locals 1

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingWorkflowRunner$NoOp;->$$delegate_0:Lcom/squareup/onboarding/OnboardingWorkflowRunner;

    invoke-interface {v0, p1}, Lcom/squareup/onboarding/OnboardingWorkflowRunner;->startWorkflow(Lcom/squareup/onboarding/OnboardingFlow;)V

    return-void
.end method
