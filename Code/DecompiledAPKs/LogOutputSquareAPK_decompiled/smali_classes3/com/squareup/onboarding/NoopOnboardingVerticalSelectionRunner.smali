.class public final Lcom/squareup/onboarding/NoopOnboardingVerticalSelectionRunner;
.super Ljava/lang/Object;
.source "NoOnboardingVerticalSelectionModule.kt"

# interfaces
.implements Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0008\u0010\u0008\u001a\u00020\tH\u0016\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/onboarding/NoopOnboardingVerticalSelectionRunner;",
        "Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;",
        "()V",
        "onActivationCompleted",
        "Lio/reactivex/Single;",
        "Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner$VerticalSelectionResult;",
        "businessCategoryKey",
        "",
        "onOnboardingCompleted",
        "Lio/reactivex/Completable;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onboarding/NoopOnboardingVerticalSelectionRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19
    new-instance v0, Lcom/squareup/onboarding/NoopOnboardingVerticalSelectionRunner;

    invoke-direct {v0}, Lcom/squareup/onboarding/NoopOnboardingVerticalSelectionRunner;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/NoopOnboardingVerticalSelectionRunner;->INSTANCE:Lcom/squareup/onboarding/NoopOnboardingVerticalSelectionRunner;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivationCompleted(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner$VerticalSelectionResult;",
            ">;"
        }
    .end annotation

    const-string v0, "businessCategoryKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    new-instance p1, Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner$VerticalSelectionResult;

    const/4 v0, 0x0

    invoke-direct {p1, v0, v0}, Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner$VerticalSelectionResult;-><init>(ZZ)V

    .line 22
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(\n      Verti\u2026utOnComplete = false)\n  )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public onOnboardingCompleted()Lio/reactivex/Completable;
    .locals 2

    .line 26
    invoke-static {}, Lio/reactivex/Completable;->complete()Lio/reactivex/Completable;

    move-result-object v0

    const-string v1, "Completable.complete()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
