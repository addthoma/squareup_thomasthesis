.class public final Lcom/squareup/onboarding/NoOnboardingVerticalSelectionModule;
.super Ljava/lang/Object;
.source "NoOnboardingVerticalSelectionModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/onboarding/NoOnboardingVerticalSelectionModule;",
        "",
        "()V",
        "provideOnboardingVerticalSelectionRunner",
        "Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onboarding/NoOnboardingVerticalSelectionModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/squareup/onboarding/NoOnboardingVerticalSelectionModule;

    invoke-direct {v0}, Lcom/squareup/onboarding/NoOnboardingVerticalSelectionModule;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/NoOnboardingVerticalSelectionModule;->INSTANCE:Lcom/squareup/onboarding/NoOnboardingVerticalSelectionModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideOnboardingVerticalSelectionRunner()Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 13
    sget-object v0, Lcom/squareup/onboarding/NoopOnboardingVerticalSelectionRunner;->INSTANCE:Lcom/squareup/onboarding/NoopOnboardingVerticalSelectionRunner;

    check-cast v0, Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;

    return-object v0
.end method
