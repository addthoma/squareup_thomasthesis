.class public final Lcom/squareup/onboarding/flow/Starter;
.super Ljava/lang/Object;
.source "RealOnboardingWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowStarter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowStarter<",
        "Lcom/squareup/onboarding/flow/OnboardingEvent;",
        "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOnboardingWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOnboardingWorkflowRunner.kt\ncom/squareup/onboarding/flow/Starter\n*L\n1#1,69:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J`\u0010\t\u001aP\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\r\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000ej\u0002`\u000f0\u000cj\n\u0012\u0006\u0008\u0001\u0012\u00020\r`\u00100\u000b\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\nj\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003`\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/Starter;",
        "Lcom/squareup/container/PosWorkflowStarter;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent;",
        "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
        "onboardingReactor",
        "Lcom/squareup/onboarding/flow/OnboardingReactor;",
        "onboardingRenderer",
        "Lcom/squareup/onboarding/flow/OnboardingRenderer;",
        "(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/onboarding/flow/OnboardingRenderer;)V",
        "start",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/container/PosWorkflow;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final onboardingReactor:Lcom/squareup/onboarding/flow/OnboardingReactor;

.field private final onboardingRenderer:Lcom/squareup/onboarding/flow/OnboardingRenderer;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/onboarding/flow/OnboardingRenderer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "onboardingReactor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingRenderer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/Starter;->onboardingReactor:Lcom/squareup/onboarding/flow/OnboardingReactor;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/Starter;->onboardingRenderer:Lcom/squareup/onboarding/flow/OnboardingRenderer;

    return-void
.end method

.method public static final synthetic access$getOnboardingRenderer$p(Lcom/squareup/onboarding/flow/Starter;)Lcom/squareup/onboarding/flow/OnboardingRenderer;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/onboarding/flow/Starter;->onboardingRenderer:Lcom/squareup/onboarding/flow/OnboardingRenderer;

    return-object p0
.end method


# virtual methods
.method public start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/onboarding/flow/OnboardingEvent;",
            "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 57
    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;

    invoke-virtual {v0, p1}, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->readState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onboarding/flow/OnboardingState;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/onboarding/flow/OnboardingState$Waiting;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingState$Waiting;

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingState;

    .line 59
    :goto_0
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-direct {v0}, Lcom/squareup/workflow/legacy/WorkflowPool;-><init>()V

    .line 60
    iget-object v1, p0, Lcom/squareup/onboarding/flow/Starter;->onboardingReactor:Lcom/squareup/onboarding/flow/OnboardingReactor;

    invoke-virtual {v1, p1, v0}, Lcom/squareup/onboarding/flow/OnboardingReactor;->launch(Lcom/squareup/onboarding/flow/OnboardingState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    .line 62
    new-instance v1, Lcom/squareup/onboarding/flow/Starter$start$1;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v0, v2}, Lcom/squareup/onboarding/flow/Starter$start$1;-><init>(Lcom/squareup/onboarding/flow/Starter;Lcom/squareup/workflow/legacy/Workflow;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {p1, v1}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->mapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method
