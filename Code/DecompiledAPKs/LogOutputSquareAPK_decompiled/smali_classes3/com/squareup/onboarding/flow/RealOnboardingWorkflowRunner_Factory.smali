.class public final Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "RealOnboardingWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final viewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/OnboardingViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final workflowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/Starter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/OnboardingViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/Starter;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner_Factory;->workflowStarterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/OnboardingViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/Starter;",
            ">;)",
            "Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/onboarding/flow/OnboardingViewFactory;Lcom/squareup/onboarding/flow/Starter;)Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner;-><init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/onboarding/flow/OnboardingViewFactory;Lcom/squareup/onboarding/flow/Starter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/onboarding/flow/OnboardingViewFactory;

    iget-object v2, p0, Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner_Factory;->workflowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/onboarding/flow/Starter;

    invoke-static {v0, v1, v2}, Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner_Factory;->newInstance(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/onboarding/flow/OnboardingViewFactory;Lcom/squareup/onboarding/flow/Starter;)Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner_Factory;->get()Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
