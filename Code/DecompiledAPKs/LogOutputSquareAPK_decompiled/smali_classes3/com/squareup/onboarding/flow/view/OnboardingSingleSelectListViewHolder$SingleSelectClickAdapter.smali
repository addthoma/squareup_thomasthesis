.class final Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;
.super Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;
.source "OnboardingSingleSelectListViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SingleSelectClickAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter<",
        "Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$ClickRowHolder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0002H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;",
        "Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;",
        "Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$ClickRowHolder;",
        "component",
        "Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "(Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;Lcom/squareup/onboarding/flow/OnboardingInputHandler;)V",
        "clickAction",
        "",
        "getComponent",
        "()Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;",
        "getInputHandler",
        "()Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "onBindViewHolder",
        "",
        "holder",
        "position",
        "",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "onRowTapped",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clickAction:Ljava/lang/String;

.field private final component:Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;

.field private final inputHandler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;Lcom/squareup/onboarding/flow/OnboardingInputHandler;)V
    .locals 1

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inputHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;-><init>(Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;)V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;->component:Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;->inputHandler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    .line 129
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;->component:Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;->clickAction()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;->clickAction:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getComponent()Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;->component:Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;

    return-object v0
.end method

.method public final getInputHandler()Lcom/squareup/onboarding/flow/OnboardingInputHandler;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;->inputHandler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    return-object v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 125
    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$ClickRowHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;->onBindViewHolder(Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$ClickRowHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$ClickRowHolder;I)V
    .locals 2

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$ClickRowHolder;->getTextView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;->getValues()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/Pair;

    invoke-virtual {v1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 139
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$ClickRowHolder;->getTextView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoRow;

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;->getValues()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lkotlin/Pair;

    invoke-virtual {p2}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 125
    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;)Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$ClickRowHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;)Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$ClickRowHolder;
    .locals 2

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    new-instance v0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$ClickRowHolder;

    sget v1, Lcom/squareup/onboarding/flow/R$layout;->onboarding_component_click_list_item:I

    invoke-static {v1, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$ClickRowHolder;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public bridge synthetic onRowTapped(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 0

    .line 125
    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$ClickRowHolder;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;->onRowTapped(Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$ClickRowHolder;)V

    return-void
.end method

.method public onRowTapped(Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$ClickRowHolder;)V
    .locals 3

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;->inputHandler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;->component:Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;->getKeys()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$ClickRowHolder;->getAdapterPosition()I

    move-result p1

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {v1, p1}, Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;->keyOutput(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/onboarding/flow/OnboardingInputHandler;->onOutput(Lcom/squareup/protos/client/onboard/Output;)V

    .line 144
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;->inputHandler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;->clickAction:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/onboarding/flow/OnboardingInputHandler;->onButtonClicked(Ljava/lang/String;)V

    return-void
.end method
