.class final Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;
.super Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$AbstractRowHolder;
.source "OnboardingSingleSelectListViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "RadioRowHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$AbstractRowHolder<",
        "Lcom/squareup/noho/NohoCheckableRow;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;",
        "Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$AbstractRowHolder;",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$AbstractRowHolder;-><init>(Landroid/view/View;)V

    return-void
.end method
