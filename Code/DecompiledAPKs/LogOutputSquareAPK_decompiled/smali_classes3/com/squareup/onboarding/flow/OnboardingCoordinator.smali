.class public final Lcom/squareup/onboarding/flow/OnboardingCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OnboardingCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/flow/OnboardingCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingCoordinator.kt\ncom/squareup/onboarding/flow/OnboardingCoordinator\n*L\n1#1,94:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u0015B+\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0005H\u0002J\u000c\u0010\u0012\u001a\u00020\u0013*\u00020\u0014H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
        "Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;",
        "Lcom/squareup/onboarding/flow/OnboardingPanelScreen;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "(Lio/reactivex/Observable;Lcom/squareup/register/widgets/GlassSpinner;)V",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "spinnerForScreenData",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "screenData",
        "isPolling",
        "",
        "Lcom/squareup/protos/client/onboard/Panel;",
        "Factory",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
            "Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
            "Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;",
            ">;>;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassSpinner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-void
.end method

.method public static final synthetic access$spinnerForScreenData(Lcom/squareup/onboarding/flow/OnboardingCoordinator;Lcom/squareup/onboarding/flow/OnboardingScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingCoordinator;->spinnerForScreenData(Lcom/squareup/onboarding/flow/OnboardingScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method

.method private final isPolling(Lcom/squareup/protos/client/onboard/Panel;)Z
    .locals 5

    .line 92
    iget-object v0, p1, Lcom/squareup/protos/client/onboard/Panel;->navigation:Lcom/squareup/protos/client/onboard/Navigation;

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Navigation;->timeout_duration:Ljava/lang/Integer;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    .line 90
    move-object v4, v0

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v4

    if-lez v4, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_1

    move-object v3, v0

    :cond_1
    if-eqz v3, :cond_2

    iget-object v0, p1, Lcom/squareup/protos/client/onboard/Panel;->navigation:Lcom/squareup/protos/client/onboard/Navigation;

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Navigation;->can_go_back:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 92
    iget-object p1, p1, Lcom/squareup/protos/client/onboard/Panel;->navigation:Lcom/squareup/protos/client/onboard/Navigation;

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/Navigation;->navigation_buttons:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method private final spinnerForScreenData(Lcom/squareup/onboarding/flow/OnboardingScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 4

    .line 61
    instance-of v0, p1, Lcom/squareup/onboarding/flow/OnboardingScreenData$Loading;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 64
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingScreenData;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingCoordinator;->isPolling(Lcom/squareup/protos/client/onboard/Panel;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 65
    sget-object p1, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    sget v0, Lcom/squareup/onboarding/flow/R$string;->onboarding_panel_processing:I

    invoke-virtual {p1, v1, v0}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner(ZI)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    goto :goto_0

    .line 67
    :cond_0
    sget-object p1, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    const/4 v0, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {p1, v1, v0, v2, v3}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    goto :goto_0

    .line 70
    :cond_1
    instance-of v0, p1, Lcom/squareup/onboarding/flow/OnboardingScreenData$Success;

    if-eqz v0, :cond_3

    .line 72
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingScreenData;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingCoordinator;->isPolling(Lcom/squareup/protos/client/onboard/Panel;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 73
    sget-object p1, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    sget v0, Lcom/squareup/onboarding/flow/R$string;->onboarding_panel_processing:I

    invoke-virtual {p1, v1, v0}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner(ZI)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    goto :goto_0

    .line 75
    :cond_2
    sget-object p1, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->hideSpinner()Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    goto :goto_0

    .line 78
    :cond_3
    instance-of p1, p1, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;

    if-eqz p1, :cond_4

    sget-object p1, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->hideSpinner()Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 38
    move-object v0, p1

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingPanelView;

    .line 40
    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v1

    const-string v2, "glassSpinner.showOrHideSpinner(view.context)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-static {v1, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 43
    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->submissions()Lio/reactivex/Observable;

    move-result-object v1

    .line 44
    iget-object v2, p0, Lcom/squareup/onboarding/flow/OnboardingCoordinator;->screens:Lio/reactivex/Observable;

    check-cast v2, Lio/reactivex/ObservableSource;

    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v1

    .line 45
    sget-object v2, Lcom/squareup/onboarding/flow/OnboardingCoordinator$attach$1;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingCoordinator$attach$1;

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "panelView.submissions()\n\u2026ut)\n          )\n        }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-static {v1, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 52
    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingCoordinator;->screens:Lio/reactivex/Observable;

    .line 53
    sget-object v2, Lcom/squareup/onboarding/flow/OnboardingCoordinator$attach$2;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingCoordinator$attach$2;

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 54
    iget-object v2, p0, Lcom/squareup/onboarding/flow/OnboardingCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    new-instance v3, Lcom/squareup/onboarding/flow/OnboardingCoordinator$attach$3;

    move-object v4, p0

    check-cast v4, Lcom/squareup/onboarding/flow/OnboardingCoordinator;

    invoke-direct {v3, v4}, Lcom/squareup/onboarding/flow/OnboardingCoordinator$attach$3;-><init>(Lcom/squareup/onboarding/flow/OnboardingCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    new-instance v4, Lcom/squareup/onboarding/flow/OnboardingCoordinator$sam$rx_functions_Func1$0;

    invoke-direct {v4, v3}, Lcom/squareup/onboarding/flow/OnboardingCoordinator$sam$rx_functions_Func1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v4, Lrx/functions/Func1;

    invoke-virtual {v2, v4}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform(Lrx/functions/Func1;)Lrx/Observable$Transformer;

    move-result-object v2

    const-string v3, "glassSpinner.spinnerTran\u2026m(::spinnerForScreenData)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v2, v3}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Transformer(Lrx/Observable$Transformer;Lio/reactivex/BackpressureStrategy;)Lio/reactivex/ObservableTransformer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 55
    new-instance v2, Lcom/squareup/onboarding/flow/OnboardingCoordinator$attach$4;

    invoke-direct {v2, v0}, Lcom/squareup/onboarding/flow/OnboardingCoordinator$attach$4;-><init>(Lcom/squareup/onboarding/flow/OnboardingPanelView;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingCoordinator$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v0, v2}, Lcom/squareup/onboarding/flow/OnboardingCoordinator$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens\n        .map { s\u2026be(panelView::updateView)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
