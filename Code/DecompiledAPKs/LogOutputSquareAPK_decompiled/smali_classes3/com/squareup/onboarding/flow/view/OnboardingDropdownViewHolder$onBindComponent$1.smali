.class public final Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder$onBindComponent$1;
.super Lcom/squareup/ui/EmptyOnItemSelectedListener;
.source "OnboardingDropdownViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000)\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\t\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J0\u0010\u0002\u001a\u00020\u00032\u000c\u0010\u0004\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00052\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016\u00a8\u0006\u000c"
    }
    d2 = {
        "com/squareup/onboarding/flow/view/OnboardingDropdownViewHolder$onBindComponent$1",
        "Lcom/squareup/ui/EmptyOnItemSelectedListener;",
        "onItemSelected",
        "",
        "parent",
        "Landroid/widget/AdapterView;",
        "view",
        "Landroid/view/View;",
        "position",
        "",
        "id",
        "",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $component:Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;

.field final synthetic $keys:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;",
            "Ljava/util/List;",
            ")V"
        }
    .end annotation

    .line 31
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder$onBindComponent$1;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder$onBindComponent$1;->$component:Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;

    iput-object p3, p0, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder$onBindComponent$1;->$keys:Ljava/util/List;

    invoke-direct {p0}, Lcom/squareup/ui/EmptyOnItemSelectedListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 38
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder$onBindComponent$1;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;->getInputHandler()Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder$onBindComponent$1;->$component:Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;

    iget-object p4, p0, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder$onBindComponent$1;->$keys:Ljava/util/List;

    invoke-interface {p4, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    invoke-virtual {p2, p3}, Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;->keyOutput(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/onboarding/flow/OnboardingInputHandler;->onOutput(Lcom/squareup/protos/client/onboard/Output;)V

    return-void
.end method
