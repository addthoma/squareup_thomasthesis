.class public final Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "OnboardingMultiSelectListViewHolder.kt"

# interfaces
.implements Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RowHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        "Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;",
        "itemView",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "edges",
        "",
        "getEdges",
        "()I",
        "textView",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "getTextView",
        "()Lcom/squareup/noho/NohoCheckableRow;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final edges:I

.field private final textView:Lcom/squareup/noho/NohoCheckableRow;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    const/16 v0, 0xa

    .line 124
    iput v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;->edges:I

    .line 126
    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;->textView:Lcom/squareup/noho/NohoCheckableRow;

    return-void
.end method


# virtual methods
.method public getEdges()I
    .locals 1

    .line 124
    iget v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;->edges:I

    return v0
.end method

.method public getPadding(Landroid/graphics/Rect;)V
    .locals 1

    const-string v0, "outRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges$DefaultImpls;->getPadding(Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;Landroid/graphics/Rect;)V

    return-void
.end method

.method public final getTextView()Lcom/squareup/noho/NohoCheckableRow;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;->textView:Lcom/squareup/noho/NohoCheckableRow;

    return-object v0
.end method
