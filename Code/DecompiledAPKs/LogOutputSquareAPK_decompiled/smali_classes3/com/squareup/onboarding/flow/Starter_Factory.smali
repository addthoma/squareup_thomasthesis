.class public final Lcom/squareup/onboarding/flow/Starter_Factory;
.super Ljava/lang/Object;
.source "Starter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onboarding/flow/Starter;",
        ">;"
    }
.end annotation


# instance fields
.field private final onboardingReactorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/OnboardingReactor;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/OnboardingRenderer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/OnboardingReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/OnboardingRenderer;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/onboarding/flow/Starter_Factory;->onboardingReactorProvider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/onboarding/flow/Starter_Factory;->onboardingRendererProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/onboarding/flow/Starter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/OnboardingReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/OnboardingRenderer;",
            ">;)",
            "Lcom/squareup/onboarding/flow/Starter_Factory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/onboarding/flow/Starter_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/flow/Starter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/onboarding/flow/OnboardingRenderer;)Lcom/squareup/onboarding/flow/Starter;
    .locals 1

    .line 38
    new-instance v0, Lcom/squareup/onboarding/flow/Starter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/flow/Starter;-><init>(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/onboarding/flow/OnboardingRenderer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/onboarding/flow/Starter;
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/onboarding/flow/Starter_Factory;->onboardingReactorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingReactor;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/Starter_Factory;->onboardingRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/onboarding/flow/OnboardingRenderer;

    invoke-static {v0, v1}, Lcom/squareup/onboarding/flow/Starter_Factory;->newInstance(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/onboarding/flow/OnboardingRenderer;)Lcom/squareup/onboarding/flow/Starter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/Starter_Factory;->get()Lcom/squareup/onboarding/flow/Starter;

    move-result-object v0

    return-object v0
.end method
