.class final Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder$onBindComponent$2;
.super Lkotlin/jvm/internal/Lambda;
.source "OnboardingDatePickerViewHolder.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lorg/threeten/bp/LocalDate;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lorg/threeten/bp/LocalDate;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $component:Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;

.field final synthetic this$0:Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder$onBindComponent$2;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder$onBindComponent$2;->$component:Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lorg/threeten/bp/LocalDate;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder$onBindComponent$2;->invoke(Lorg/threeten/bp/LocalDate;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lorg/threeten/bp/LocalDate;)V
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder$onBindComponent$2;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;->getInputHandler()Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder$onBindComponent$2;->$component:Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;

    invoke-virtual {v1, p1}, Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;->dateOutput(Lorg/threeten/bp/LocalDate;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/onboarding/flow/OnboardingInputHandler;->onOutput(Lcom/squareup/protos/client/onboard/Output;)V

    return-void
.end method
