.class public final enum Lcom/squareup/onboarding/flow/data/ImageSize;
.super Ljava/lang/Enum;
.source "OnboardingImageItem.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/onboarding/flow/data/ImageSize;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\n\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0019\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/data/ImageSize;",
        "",
        "key",
        "",
        "sizeResId",
        "",
        "(Ljava/lang/String;ILjava/lang/String;I)V",
        "getKey",
        "()Ljava/lang/String;",
        "getSizeResId",
        "()I",
        "SMALL",
        "MEDIUM",
        "LARGE",
        "ORIGINAL",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/onboarding/flow/data/ImageSize;

.field public static final enum LARGE:Lcom/squareup/onboarding/flow/data/ImageSize;

.field public static final enum MEDIUM:Lcom/squareup/onboarding/flow/data/ImageSize;

.field public static final enum ORIGINAL:Lcom/squareup/onboarding/flow/data/ImageSize;

.field public static final enum SMALL:Lcom/squareup/onboarding/flow/data/ImageSize;


# instance fields
.field private final key:Ljava/lang/String;

.field private final sizeResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/onboarding/flow/data/ImageSize;

    new-instance v1, Lcom/squareup/onboarding/flow/data/ImageSize;

    .line 25
    sget v2, Lcom/squareup/onboarding/flow/R$dimen;->onboarding_image_size_small:I

    const/4 v3, 0x0

    const-string v4, "SMALL"

    const-string v5, "small"

    invoke-direct {v1, v4, v3, v5, v2}, Lcom/squareup/onboarding/flow/data/ImageSize;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, Lcom/squareup/onboarding/flow/data/ImageSize;->SMALL:Lcom/squareup/onboarding/flow/data/ImageSize;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/onboarding/flow/data/ImageSize;

    .line 26
    sget v2, Lcom/squareup/onboarding/flow/R$dimen;->onboarding_image_size_medium:I

    const/4 v3, 0x1

    const-string v4, "MEDIUM"

    const-string v5, "medium"

    invoke-direct {v1, v4, v3, v5, v2}, Lcom/squareup/onboarding/flow/data/ImageSize;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, Lcom/squareup/onboarding/flow/data/ImageSize;->MEDIUM:Lcom/squareup/onboarding/flow/data/ImageSize;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/onboarding/flow/data/ImageSize;

    .line 27
    sget v2, Lcom/squareup/onboarding/flow/R$dimen;->onboarding_image_size_large:I

    const/4 v3, 0x2

    const-string v4, "LARGE"

    const-string v5, "large"

    invoke-direct {v1, v4, v3, v5, v2}, Lcom/squareup/onboarding/flow/data/ImageSize;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, Lcom/squareup/onboarding/flow/data/ImageSize;->LARGE:Lcom/squareup/onboarding/flow/data/ImageSize;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/onboarding/flow/data/ImageSize;

    const/4 v2, 0x3

    const-string v3, "ORIGINAL"

    const-string v4, "original"

    const/4 v5, -0x1

    .line 28
    invoke-direct {v1, v3, v2, v4, v5}, Lcom/squareup/onboarding/flow/data/ImageSize;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, Lcom/squareup/onboarding/flow/data/ImageSize;->ORIGINAL:Lcom/squareup/onboarding/flow/data/ImageSize;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/onboarding/flow/data/ImageSize;->$VALUES:[Lcom/squareup/onboarding/flow/data/ImageSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/onboarding/flow/data/ImageSize;->key:Ljava/lang/String;

    iput p4, p0, Lcom/squareup/onboarding/flow/data/ImageSize;->sizeResId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/onboarding/flow/data/ImageSize;
    .locals 1

    const-class v0, Lcom/squareup/onboarding/flow/data/ImageSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/onboarding/flow/data/ImageSize;

    return-object p0
.end method

.method public static values()[Lcom/squareup/onboarding/flow/data/ImageSize;
    .locals 1

    sget-object v0, Lcom/squareup/onboarding/flow/data/ImageSize;->$VALUES:[Lcom/squareup/onboarding/flow/data/ImageSize;

    invoke-virtual {v0}, [Lcom/squareup/onboarding/flow/data/ImageSize;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/onboarding/flow/data/ImageSize;

    return-object v0
.end method


# virtual methods
.method public final getKey()Ljava/lang/String;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/onboarding/flow/data/ImageSize;->key:Ljava/lang/String;

    return-object v0
.end method

.method public final getSizeResId()I
    .locals 1

    .line 23
    iget v0, p0, Lcom/squareup/onboarding/flow/data/ImageSize;->sizeResId:I

    return v0
.end method
