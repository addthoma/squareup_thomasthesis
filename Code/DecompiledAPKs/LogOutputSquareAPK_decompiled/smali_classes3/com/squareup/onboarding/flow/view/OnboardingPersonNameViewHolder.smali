.class public final Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;
.super Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;
.source "OnboardingPersonNameViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
        "Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingPersonNameViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingPersonNameViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder\n*L\n1#1,64:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0014J\u0008\u0010\u0013\u001a\u00020\u0011H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;",
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;",
        "Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V",
        "firstName",
        "Lcom/squareup/noho/NohoEditText;",
        "header",
        "Lcom/squareup/marketfont/MarketTextView;",
        "lastName",
        "valueWatcherFirst",
        "Landroid/text/TextWatcher;",
        "valueWatcherLast",
        "onBindComponent",
        "",
        "component",
        "onHighlightError",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final firstName:Lcom/squareup/noho/NohoEditText;

.field private final header:Lcom/squareup/marketfont/MarketTextView;

.field private final lastName:Lcom/squareup/noho/NohoEditText;

.field private valueWatcherFirst:Landroid/text/TextWatcher;

.field private valueWatcherLast:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V
    .locals 1

    const-string v0, "inputHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_component_person_name:I

    .line 20
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;I)V

    .line 24
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->itemView:Landroid/view/View;

    const-string p2, "itemView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_person_name_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    .line 25
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_first_name:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditText;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->firstName:Lcom/squareup/noho/NohoEditText;

    .line 26
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget p2, Lcom/squareup/onboarding/flow/R$id;->onboarding_last_name:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditText;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->lastName:Lcom/squareup/noho/NohoEditText;

    return-void
.end method


# virtual methods
.method public bridge synthetic onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;)V

    return-void
.end method

.method protected onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;)V
    .locals 2

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;->label()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->firstName:Lcom/squareup/noho/NohoEditText;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->valueWatcherFirst:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 35
    new-instance v0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder$onBindComponent$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder$onBindComponent$1;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;)V

    check-cast v0, Landroid/text/TextWatcher;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->valueWatcherFirst:Landroid/text/TextWatcher;

    .line 40
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->firstName:Lcom/squareup/noho/NohoEditText;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->valueWatcherFirst:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->lastName:Lcom/squareup/noho/NohoEditText;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->valueWatcherLast:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 43
    new-instance v0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder$onBindComponent$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder$onBindComponent$2;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;)V

    check-cast v0, Landroid/text/TextWatcher;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->valueWatcherLast:Landroid/text/TextWatcher;

    .line 48
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->lastName:Lcom/squareup/noho/NohoEditText;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->valueWatcherLast:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->firstName:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;->firstNameHint()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->lastName:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;->lastNameHint()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->firstName:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;->defaultFirstName()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->lastName:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;->defaultLastName()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onHighlightError()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/widget/TextView;

    .line 58
    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->firstName:Lcom/squareup/noho/NohoEditText;

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->lastName:Lcom/squareup/noho/NohoEditText;

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Views;->getEmptyView([Landroid/widget/TextView;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 59
    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    if-eqz v0, :cond_0

    .line 60
    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->setSelectionEnd(Landroid/widget/EditText;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.widget.EditText"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-void
.end method
