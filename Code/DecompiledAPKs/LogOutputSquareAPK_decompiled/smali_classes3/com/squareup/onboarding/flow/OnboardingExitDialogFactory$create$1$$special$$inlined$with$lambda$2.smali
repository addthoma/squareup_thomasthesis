.class final Lcom/squareup/onboarding/flow/OnboardingExitDialogFactory$create$1$$special$$inlined$with$lambda$2;
.super Ljava/lang/Object;
.source "OnboardingExitDialogFactory.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/OnboardingExitDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/content/DialogInterface;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "onClick",
        "com/squareup/onboarding/flow/OnboardingExitDialogFactory$create$1$1$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic this$0:Lcom/squareup/onboarding/flow/OnboardingExitDialogFactory$create$1;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/OnboardingExitDialogFactory$create$1;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingExitDialogFactory$create$1$$special$$inlined$with$lambda$2;->this$0:Lcom/squareup/onboarding/flow/OnboardingExitDialogFactory$create$1;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingExitDialogFactory$create$1$$special$$inlined$with$lambda$2;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 46
    iget-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingExitDialogFactory$create$1$$special$$inlined$with$lambda$2;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object p2, Lcom/squareup/onboarding/flow/OnboardingExitDialog$Event$ExitFlowCancelled;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingExitDialog$Event$ExitFlowCancelled;

    invoke-interface {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
