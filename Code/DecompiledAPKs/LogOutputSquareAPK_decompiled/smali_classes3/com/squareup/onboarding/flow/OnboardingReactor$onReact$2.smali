.class final Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$2;
.super Lkotlin/jvm/internal/Lambda;
.source "OnboardingReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/OnboardingReactor;->onReact(Lcom/squareup/onboarding/flow/OnboardingState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/onboard/StartSessionResponse;",
        "Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;",
        "it",
        "Lcom/squareup/protos/client/onboard/StartSessionResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$2;

    invoke-direct {v0}, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$2;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$2;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/protos/client/onboard/StartSessionResponse;)Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;
    .locals 4

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;

    iget-object v1, p1, Lcom/squareup/protos/client/onboard/StartSessionResponse;->session_token:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/protos/client/onboard/StartSessionResponse;->status:Lcom/squareup/protos/client/Status;

    const-string v3, "it.status"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/StartSessionResponse;->step:Lcom/squareup/protos/client/onboard/Step;

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/onboard/Step;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 71
    check-cast p1, Lcom/squareup/protos/client/onboard/StartSessionResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$2;->invoke(Lcom/squareup/protos/client/onboard/StartSessionResponse;)Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;

    move-result-object p1

    return-object p1
.end method
