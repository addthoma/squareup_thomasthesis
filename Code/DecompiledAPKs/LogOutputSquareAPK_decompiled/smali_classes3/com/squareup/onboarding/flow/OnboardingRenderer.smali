.class public final Lcom/squareup/onboarding/flow/OnboardingRenderer;
.super Ljava/lang/Object;
.source "OnboardingRenderer.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/Renderer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/Renderer<",
        "Lcom/squareup/onboarding/flow/OnboardingState;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent;",
        "Ljava/util/Map<",
        "Lcom/squareup/workflow/MainAndModal;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u000026\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00080\u0001:\u0001\u001bB\u000f\u0008\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJH\u0010\u000c\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\r\u001a\u00020\u00022\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J:\u0010\u0012\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\r\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J:\u0010\u0016\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u000c\u0010\u0017\u001a\u00020\u0018*\u00020\u0013H\u0002J\u000c\u0010\u0019\u001a\u00020\u001a*\u00020\u0002H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingRenderer;",
        "Lcom/squareup/workflow/legacy/Renderer;",
        "Lcom/squareup/onboarding/flow/OnboardingState;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent;",
        "",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "render",
        "state",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "showExitDialogScreen",
        "Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;",
        "input",
        "Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;",
        "showPanelScreen",
        "toExitDialogData",
        "Lcom/squareup/onboarding/flow/OnboardingExitDialog$ScreenData;",
        "toPanelData",
        "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
        "ScreenInput",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingRenderer;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private final showExitDialogScreen(Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;",
            "Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 64
    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 65
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingRenderer;->toExitDialogData(Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;)Lcom/squareup/onboarding/flow/OnboardingExitDialog$ScreenData;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;->getForDialog()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/onboarding/flow/OnboardingExitDialogScreenKt;->OnboardingExitDialogScreen(Lcom/squareup/onboarding/flow/OnboardingExitDialog$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v1

    .line 66
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->getContext()Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    move-result-object p1

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingState;

    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingRenderer;->toPanelData(Lcom/squareup/onboarding/flow/OnboardingState;)Lcom/squareup/onboarding/flow/OnboardingScreenData;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;->getForPanel()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/onboarding/flow/OnboardingPanelScreenKt;->OnboardingPanelScreen(Lcom/squareup/onboarding/flow/OnboardingScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 64
    invoke-virtual {v0, p1, v1}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final showPanelScreen(Lcom/squareup/onboarding/flow/OnboardingState;Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            "Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 57
    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingRenderer;->toPanelData(Lcom/squareup/onboarding/flow/OnboardingState;)Lcom/squareup/onboarding/flow/OnboardingScreenData;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;->getForPanel()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/onboarding/flow/OnboardingPanelScreenKt;->OnboardingPanelScreen(Lcom/squareup/onboarding/flow/OnboardingScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final toExitDialogData(Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;)Lcom/squareup/onboarding/flow/OnboardingExitDialog$ScreenData;
    .locals 1

    .line 87
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingExitDialog$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->getExitDialog()Lcom/squareup/protos/client/onboard/Dialog;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/onboarding/flow/OnboardingExitDialog$ScreenData;-><init>(Lcom/squareup/protos/client/onboard/Dialog;)V

    return-object v0
.end method

.method private final toPanelData(Lcom/squareup/onboarding/flow/OnboardingState;)Lcom/squareup/onboarding/flow/OnboardingScreenData;
    .locals 4

    .line 75
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState;->getHistory()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/PanelHistory;->getTop()Lcom/squareup/onboarding/flow/PanelScreen;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/PanelScreen;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object v0

    .line 77
    instance-of v1, p1, Lcom/squareup/onboarding/flow/OnboardingState$Waiting;

    const-string v2, " to panel data."

    const-string v3, "Cannot coerce "

    if-nez v1, :cond_5

    .line 78
    instance-of v1, p1, Lcom/squareup/onboarding/flow/OnboardingState$LoadingFirst;

    if-eqz v1, :cond_0

    goto :goto_0

    .line 79
    :cond_0
    instance-of v1, p1, Lcom/squareup/onboarding/flow/OnboardingState$LoadingNext;

    if-eqz v1, :cond_1

    :goto_0
    new-instance p1, Lcom/squareup/onboarding/flow/OnboardingScreenData$Loading;

    invoke-direct {p1, v0}, Lcom/squareup/onboarding/flow/OnboardingScreenData$Loading;-><init>(Lcom/squareup/protos/client/onboard/Panel;)V

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingScreenData;

    goto :goto_1

    .line 80
    :cond_1
    instance-of v1, p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    if-eqz v1, :cond_2

    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingScreenData$Success;

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;->getError()Lcom/squareup/onboarding/flow/PanelError;

    move-result-object p1

    invoke-direct {v1, v0, p1}, Lcom/squareup/onboarding/flow/OnboardingScreenData$Success;-><init>(Lcom/squareup/protos/client/onboard/Panel;Lcom/squareup/onboarding/flow/PanelError;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingScreenData;

    goto :goto_1

    .line 81
    :cond_2
    instance-of v1, p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;

    if-eqz v1, :cond_3

    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;->getErrorTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;->getErrorMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v0, v2, p1}, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;-><init>(Lcom/squareup/protos/client/onboard/Panel;Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingScreenData;

    :goto_1
    return-object p1

    .line 82
    :cond_3
    instance-of v0, p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 77
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/onboarding/flow/OnboardingRenderer;->render(Lcom/squareup/onboarding/flow/OnboardingState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/onboarding/flow/OnboardingState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/onboarding/flow/OnboardingEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    new-instance p3, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingRenderer;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-direct {p3, p2, v0}, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/analytics/Analytics;)V

    .line 44
    instance-of p2, p1, Lcom/squareup/onboarding/flow/OnboardingState$Waiting;

    if-nez p2, :cond_5

    .line 45
    instance-of p2, p1, Lcom/squareup/onboarding/flow/OnboardingState$LoadingFirst;

    if-eqz p2, :cond_0

    goto :goto_0

    .line 46
    :cond_0
    instance-of p2, p1, Lcom/squareup/onboarding/flow/OnboardingState$LoadingNext;

    if-eqz p2, :cond_1

    goto :goto_0

    .line 47
    :cond_1
    instance-of p2, p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    if-eqz p2, :cond_2

    goto :goto_0

    .line 48
    :cond_2
    instance-of p2, p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;

    if-eqz p2, :cond_3

    :goto_0
    invoke-direct {p0, p1, p3}, Lcom/squareup/onboarding/flow/OnboardingRenderer;->showPanelScreen(Lcom/squareup/onboarding/flow/OnboardingState;Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 49
    :cond_3
    instance-of p2, p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;

    if-eqz p2, :cond_4

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;

    invoke-direct {p0, p1, p3}, Lcom/squareup/onboarding/flow/OnboardingRenderer;->showExitDialogScreen(Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;)Ljava/util/Map;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 44
    :cond_5
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Cannot render screen for "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method
