.class final Lcom/squareup/onboarding/OnboardingNotificationsSource$notifications$1;
.super Ljava/lang/Object;
.source "OnboardingNotificationsSource.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/OnboardingNotificationsSource;->notifications()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;",
        "it",
        "",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsSource$notifications$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/onboarding/OnboardingNotificationsSource$notifications$1;

    invoke-direct {v0}, Lcom/squareup/onboarding/OnboardingNotificationsSource$notifications$1;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/OnboardingNotificationsSource$notifications$1;->INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsSource$notifications$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/util/List;)Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;)",
            "Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    new-instance v0, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;

    invoke-direct {v0, p1}, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 54
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/OnboardingNotificationsSource$notifications$1;->apply(Ljava/util/List;)Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;

    move-result-object p1

    return-object p1
.end method
