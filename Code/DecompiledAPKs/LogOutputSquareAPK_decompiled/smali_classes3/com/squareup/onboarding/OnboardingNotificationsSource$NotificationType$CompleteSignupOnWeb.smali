.class public final Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupOnWeb;
.super Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;
.source "OnboardingNotificationsSource.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CompleteSignupOnWeb"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupOnWeb;",
        "Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;",
        "()V",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupOnWeb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 159
    new-instance v0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupOnWeb;

    invoke-direct {v0}, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupOnWeb;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupOnWeb;->INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupOnWeb;

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .line 161
    sget v2, Lcom/squareup/onboarding/common/R$string;->complete_signup_notification_title:I

    .line 162
    sget v3, Lcom/squareup/onboarding/common/R$string;->complete_signup_notification_content:I

    .line 163
    sget-object v0, Lcom/squareup/notificationcenterdata/Notification$DisplayType$WarningBanner;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$DisplayType$WarningBanner;

    move-object v4, v0

    check-cast v4, Lcom/squareup/notificationcenterdata/Notification$DisplayType;

    .line 164
    sget-object v0, Lcom/squareup/communications/Message$Type$AlertSetupPos;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertSetupPos;

    move-object v5, v0

    check-cast v5, Lcom/squareup/communications/Message$Type;

    const-string v1, "onboarding-complete-signup-on-web"

    const/4 v6, 0x0

    move-object v0, p0

    .line 159
    invoke-direct/range {v0 .. v6}, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;-><init>(Ljava/lang/String;IILcom/squareup/notificationcenterdata/Notification$DisplayType;Lcom/squareup/communications/Message$Type;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
