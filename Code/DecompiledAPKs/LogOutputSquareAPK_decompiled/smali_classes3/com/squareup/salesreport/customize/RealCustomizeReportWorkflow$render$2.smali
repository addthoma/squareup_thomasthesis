.class final Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "CustomizeReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->render(Lcom/squareup/salesreport/customize/CustomizeReportInput;Lcom/squareup/salesreport/customize/CustomizeReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/customreport/data/EmployeeFilter;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "+",
        "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
        "employeeFilters",
        "",
        "Lcom/squareup/customreport/data/EmployeeFilter;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/salesreport/customize/CustomizeReportState;

.field final synthetic this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$2;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;

    iput-object p2, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$2;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/util/List;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/customreport/data/EmployeeFilter;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/salesreport/customize/CustomizeReportState;",
            "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
            ">;"
        }
    .end annotation

    const-string v0, "employeeFilters"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$2;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;

    new-instance v1, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$2$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$2$1;-><init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$2;Ljava/util/List;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, p1, v1, v2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$2;->invoke(Ljava/util/List;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
