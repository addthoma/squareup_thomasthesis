.class public final Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;
.super Ljava/lang/Object;
.source "EmployeeSelectionCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B!\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J*\u0010\t\u001a\u00020\n2\"\u0010\u000b\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rj\u0008\u0012\u0004\u0012\u00020\u000e`\u00100\u000cR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;",
        "",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Scheduler;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)V",
        "create",
        "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lio/reactivex/Scheduler;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)V
    .locals 1
    .param p1    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "mainScheduler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p2, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object p3, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    new-instance v0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;

    .line 71
    iget-object v1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    .line 72
    sget-object v2, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory$create$1;->INSTANCE:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory$create$1;

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v2, "screens.map { it.unwrapV2Screen }"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    iget-object v2, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 74
    iget-object v3, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;->res:Lcom/squareup/util/Res;

    .line 70
    invoke-direct {v0, v1, p1, v2, v3}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;-><init>(Lio/reactivex/Scheduler;Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)V

    return-object v0
.end method
