.class public final Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "CustomizeReportAllFieldsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCustomizeReportAllFieldsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CustomizeReportAllFieldsCoordinator.kt\ncom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,236:1\n1103#2,7:237\n1103#2,7:244\n1103#2,7:251\n*E\n*S KotlinDebug\n*F\n+ 1 CustomizeReportAllFieldsCoordinator.kt\ncom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator\n*L\n148#1,7:237\n149#1,7:244\n203#1,7:251\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u00012BA\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\"\u0010\u0006\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008j\u0008\u0012\u0004\u0012\u00020\t`\u000b0\u0007\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0010\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0016J\u0010\u0010$\u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0002J(\u0010%\u001a\u00020!2\u0006\u0010&\u001a\u00020\t2\u0006\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020(2\u0006\u0010*\u001a\u00020(H\u0002J\u0018\u0010+\u001a\u00020!2\u0006\u0010&\u001a\u00020\t2\u0006\u0010\"\u001a\u00020#H\u0002J\u0010\u0010,\u001a\u00020!2\u0006\u0010-\u001a\u00020.H\u0002J\u000c\u0010/\u001a\u000200*\u000201H\u0002R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R-\u0010\u0006\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008j\u0008\u0012\u0004\u0012\u00020\t`\u000b0\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u000e\u0010\u001f\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00063"
    }
    d2 = {
        "Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "localTimeFormatter",
        "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/salesreport/util/LocalTimeFormatter;Lio/reactivex/Scheduler;Lio/reactivex/Observable;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "allDayToggle",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "datePicker",
        "Lcom/squareup/register/widgets/date/DatePicker;",
        "deviceFilterContainer",
        "Landroid/view/ViewGroup;",
        "deviceFilters",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "employeeFilter",
        "Lcom/squareup/noho/NohoRow;",
        "employeeFilterContainer",
        "endTimeRow",
        "getScreens",
        "()Lio/reactivex/Observable;",
        "startTimeRow",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "handleDateRangeSelection",
        "screen",
        "startDate",
        "Lorg/threeten/bp/LocalDate;",
        "endDate",
        "selectedDate",
        "onScreen",
        "setVisibilityForTimeFields",
        "allDay",
        "",
        "format",
        "",
        "Lorg/threeten/bp/LocalTime;",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private allDayToggle:Lcom/squareup/noho/NohoCheckableRow;

.field private datePicker:Lcom/squareup/register/widgets/date/DatePicker;

.field private deviceFilterContainer:Landroid/view/ViewGroup;

.field private deviceFilters:Lcom/squareup/noho/NohoCheckableGroup;

.field private employeeFilter:Lcom/squareup/noho/NohoRow;

.field private employeeFilterContainer:Landroid/view/ViewGroup;

.field private endTimeRow:Lcom/squareup/noho/NohoRow;

.field private final localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private startTimeRow:Lcom/squareup/noho/NohoRow;


# direct methods
.method public constructor <init>(Lcom/squareup/salesreport/util/LocalTimeFormatter;Lio/reactivex/Scheduler;Lio/reactivex/Observable;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            "Lio/reactivex/Scheduler;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    const-string v0, "localTimeFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

    iput-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p3, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p4, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$handleDateRangeSelection(Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->handleDateRangeSelection(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V

    return-void
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;Landroid/view/View;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->onScreen(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;Landroid/view/View;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 3

    .line 90
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026s.R.id.stable_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 91
    sget v0, Lcom/squareup/salesreport/impl/R$id;->config_start_time_row:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.config_start_time_row)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->startTimeRow:Lcom/squareup/noho/NohoRow;

    .line 92
    sget v0, Lcom/squareup/salesreport/impl/R$id;->config_end_time_row:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.config_end_time_row)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->endTimeRow:Lcom/squareup/noho/NohoRow;

    .line 93
    sget v0, Lcom/squareup/salesreport/impl/R$id;->config_all_day:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.config_all_day)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->allDayToggle:Lcom/squareup/noho/NohoCheckableRow;

    .line 94
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->allDayToggle:Lcom/squareup/noho/NohoCheckableRow;

    const-string v1, "allDayToggle"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 95
    sget v0, Lcom/squareup/salesreport/impl/R$id;->config_device_filter_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v2, "view.findViewById(R.id.c\u2026_device_filter_container)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->deviceFilterContainer:Landroid/view/ViewGroup;

    .line 96
    sget v0, Lcom/squareup/salesreport/impl/R$id;->config_employee_filter_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v2, "view.findViewById(R.id.c\u2026mployee_filter_container)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->employeeFilterContainer:Landroid/view/ViewGroup;

    .line 97
    sget v0, Lcom/squareup/salesreport/impl/R$id;->config_device_filter:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v2, "view.findViewById(R.id.config_device_filter)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoCheckableGroup;

    iput-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->deviceFilters:Lcom/squareup/noho/NohoCheckableGroup;

    .line 98
    sget v0, Lcom/squareup/salesreport/impl/R$id;->config_employee_filter:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v2, "view.findViewById(R.id.config_employee_filter)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->employeeFilter:Lcom/squareup/noho/NohoRow;

    .line 99
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->allDayToggle:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableRow;->isChecked()Z

    move-result v0

    if-nez v0, :cond_4

    .line 100
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->startTimeRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_2

    const-string v1, "startTimeRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 101
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->endTimeRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_3

    const-string v1, "endTimeRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 103
    :cond_4
    sget v0, Lcom/squareup/salesreport/impl/R$id;->customize_report_date_picker:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.c\u2026omize_report_date_picker)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/register/widgets/date/DatePicker;

    iput-object p1, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->datePicker:Lcom/squareup/register/widgets/date/DatePicker;

    .line 106
    iget-object p1, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->datePicker:Lcom/squareup/register/widgets/date/DatePicker;

    if-nez p1, :cond_5

    const-string v0, "datePicker"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    new-instance v0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$bindViews$1;

    invoke-direct {v0}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$bindViews$1;-><init>()V

    check-cast v0, Lcom/squareup/register/widgets/date/DatePickerListener;

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/date/DatePicker;->setListener(Lcom/squareup/register/widgets/date/DatePickerListener;)V

    return-void
.end method

.method private final format(Lorg/threeten/bp/LocalTime;)Ljava/lang/String;
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

    invoke-virtual {v0, p1}, Lcom/squareup/salesreport/util/LocalTimeFormatter;->format(Lorg/threeten/bp/LocalTime;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final handleDateRangeSelection(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V
    .locals 1

    .line 215
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    move-object v0, p2

    check-cast v0, Lorg/threeten/bp/chrono/ChronoLocalDate;

    invoke-virtual {p4, v0}, Lorg/threeten/bp/LocalDate;->compareTo(Lorg/threeten/bp/chrono/ChronoLocalDate;)I

    move-result v0

    if-gez v0, :cond_0

    .line 218
    invoke-static {p4, p3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    goto :goto_0

    .line 220
    :cond_0
    invoke-static {p2, p4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    goto :goto_0

    .line 224
    :cond_1
    invoke-static {p4, p4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    .line 215
    :goto_0
    invoke-virtual {p2}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lorg/threeten/bp/LocalDate;

    invoke-virtual {p2}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lorg/threeten/bp/LocalDate;

    .line 226
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    new-instance p4, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDateRange;

    invoke-direct {p4, p3, p2}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDateRange;-><init>(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V

    invoke-interface {p1, p4}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final onScreen(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;Landroid/view/View;)V
    .locals 11

    .line 124
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->getState()Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    .line 126
    new-instance v1, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$1;

    invoke-direct {v1, p1}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$1;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 134
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p2, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 128
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 129
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/salesreport/impl/R$string;->customize_report_card_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 130
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$2;

    invoke-direct {v3, p1}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$2;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v4

    .line 131
    sget-object v5, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/salesreport/impl/R$string;->customize_report_action_button:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v6, v1

    check-cast v6, Lcom/squareup/resources/TextModel;

    const/4 v7, 0x0

    new-instance v1, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$3;

    invoke-direct {v1, p1}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$3;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;)V

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function0;

    const/4 v9, 0x4

    const/4 v10, 0x0

    invoke-static/range {v4 .. v10}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 134
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 136
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->allDayToggle:Lcom/squareup/noho/NohoCheckableRow;

    const-string v1, "allDayToggle"

    if-nez p2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig;->getAllDay()Z

    move-result v2

    invoke-virtual {p2, v2}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 137
    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig;->getAllDay()Z

    move-result p2

    const-string v2, "endTimeRow"

    const-string v3, "startTimeRow"

    if-nez p2, :cond_4

    .line 138
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->startTimeRow:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig;->getStartTime()Lorg/threeten/bp/LocalTime;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->format(Lorg/threeten/bp/LocalTime;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {p2, v4}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 139
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->endTimeRow:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig;->getEndTime()Lorg/threeten/bp/LocalTime;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->format(Lorg/threeten/bp/LocalTime;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {p2, v4}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 142
    :cond_4
    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig;->getAllDay()Z

    move-result p2

    invoke-direct {p0, p2}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->setVisibilityForTimeFields(Z)V

    .line 144
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->allDayToggle:Lcom/squareup/noho/NohoCheckableRow;

    if-nez p2, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    new-instance v1, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$4;

    invoke-direct {v1, p1}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$4;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 148
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->startTimeRow:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_6

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p2, Landroid/view/View;

    .line 237
    new-instance v1, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->endTimeRow:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_7

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast p2, Landroid/view/View;

    .line 244
    new-instance v1, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$$inlined$onClickDebounced$2;

    invoke-direct {v1, p1}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->datePicker:Lcom/squareup/register/widgets/date/DatePicker;

    const-string v1, "datePicker"

    if-nez p2, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/utilities/threeten/compat/LocalDatesKt;->toDate(Lorg/threeten/bp/LocalDate;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/utilities/threeten/compat/LocalDatesKt;->toDate(Lorg/threeten/bp/LocalDate;)Ljava/util/Date;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Lcom/squareup/register/widgets/date/DatePicker;->setSelectedRange(Ljava/util/Date;Ljava/util/Date;)V

    .line 153
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->getUiDisplayState()Lcom/squareup/salesreport/UiDisplayState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/salesreport/UiDisplayState;->getEarliestSelectableDate()Lorg/threeten/bp/LocalDate;

    move-result-object p2

    if-eqz p2, :cond_a

    .line 154
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    const-string v3, "calendar"

    .line 155
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/squareup/utilities/threeten/compat/LocalDatesKt;->toDate(Lorg/threeten/bp/LocalDate;)Ljava/util/Date;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 156
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->datePicker:Lcom/squareup/register/widgets/date/DatePicker;

    if-nez p2, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p2, v2}, Lcom/squareup/register/widgets/date/DatePicker;->setDateRangeStart(Ljava/util/Calendar;)V

    .line 159
    :cond_a
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->datePicker:Lcom/squareup/register/widgets/date/DatePicker;

    if-nez p2, :cond_b

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->getLatestSelectableDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/utilities/threeten/compat/LocalDatesKt;->toDate(Lorg/threeten/bp/LocalDate;)Ljava/util/Date;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/util/Dates;->toCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/squareup/register/widgets/date/DatePicker;->setDateRangeEnd(Ljava/util/Calendar;)V

    .line 161
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->datePicker:Lcom/squareup/register/widgets/date/DatePicker;

    if-nez p2, :cond_c

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 162
    :cond_c
    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v2

    .line 164
    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    invoke-virtual {v3}, Lorg/threeten/bp/LocalDate;->getMonthValue()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 165
    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v4

    invoke-virtual {v4}, Lorg/threeten/bp/LocalDate;->getDayOfMonth()I

    move-result v4

    .line 161
    invoke-virtual {p2, v2, v3, v4}, Lcom/squareup/register/widgets/date/DatePicker;->setDate(III)V

    .line 169
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->getState()Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->isDatePickerInitialized()Z

    move-result p2

    if-nez p2, :cond_d

    .line 170
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    sget-object v2, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$DatePickerInitialized;->INSTANCE:Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$DatePickerInitialized;

    invoke-interface {p2, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    :cond_d
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->datePicker:Lcom/squareup/register/widgets/date/DatePicker;

    if-nez p2, :cond_e

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    new-instance v1, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$8;

    invoke-direct {v1, p0, p1, v0}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$8;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;Lcom/squareup/customreport/data/ReportConfig;)V

    check-cast v1, Lcom/squareup/register/widgets/date/DatePickerListener;

    invoke-virtual {p2, v1}, Lcom/squareup/register/widgets/date/DatePicker;->setListener(Lcom/squareup/register/widgets/date/DatePickerListener;)V

    .line 192
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->deviceFilters:Lcom/squareup/noho/NohoCheckableGroup;

    const-string v1, "deviceFilters"

    if-nez p2, :cond_f

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 193
    :cond_f
    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig;->getThisDeviceOnly()Z

    move-result v2

    if-eqz v2, :cond_10

    sget v2, Lcom/squareup/salesreport/impl/R$id;->config_this_device:I

    goto :goto_0

    :cond_10
    sget v2, Lcom/squareup/salesreport/impl/R$id;->config_all_devices:I

    .line 192
    :goto_0
    invoke-virtual {p2, v2}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    .line 195
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->deviceFilters:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez p2, :cond_11

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    new-instance v1, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$9;

    invoke-direct {v1, v0, p1}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$9;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;)V

    check-cast v1, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoCheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V

    .line 202
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->employeeFilterContainer:Landroid/view/ViewGroup;

    if-nez p2, :cond_12

    const-string v1, "employeeFilterContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    check-cast p2, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->getState()Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->getShowEmployeeFilter()Z

    move-result v1

    invoke-static {p2, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 203
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->employeeFilter:Lcom/squareup/noho/NohoRow;

    const-string v1, "employeeFilter"

    if-nez p2, :cond_13

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_13
    check-cast p2, Landroid/view/View;

    .line 251
    new-instance v2, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$$inlined$onClickDebounced$3;

    invoke-direct {v2, p1}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$$inlined$onClickDebounced$3;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    iget-object p1, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->employeeFilter:Lcom/squareup/noho/NohoRow;

    if-nez p1, :cond_14

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_14
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {v0, p2}, Lcom/squareup/salesreport/util/ReportConfigsKt;->employeeSelectionDescription(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final setVisibilityForTimeFields(Z)V
    .locals 2

    .line 230
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->startTimeRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_0

    const-string v1, "startTimeRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    xor-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 231
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->endTimeRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_1

    const-string v1, "endTimeRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 83
    invoke-direct {p0, p1}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->bindViews(Landroid/view/View;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->screens:Lio/reactivex/Observable;

    .line 85
    iget-object v1, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens\n        .observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    new-instance v1, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$attach$1;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public final getScreens()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->screens:Lio/reactivex/Observable;

    return-object v0
.end method
