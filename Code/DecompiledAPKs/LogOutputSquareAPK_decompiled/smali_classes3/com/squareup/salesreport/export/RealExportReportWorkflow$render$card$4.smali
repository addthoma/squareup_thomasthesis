.class final Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;
.super Lkotlin/jvm/internal/Lambda;
.source "ExportReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/export/RealExportReportWorkflow;->render(Lcom/squareup/salesreport/export/ExportReportProps;Lcom/squareup/salesreport/export/ExportReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/salesreport/export/ExportReportProps;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic $state:Lcom/squareup/salesreport/export/ExportReportState;

.field final synthetic this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/export/RealExportReportWorkflow;Lcom/squareup/salesreport/export/ExportReportState;Lcom/squareup/salesreport/export/ExportReportProps;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    iput-object p2, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->$state:Lcom/squareup/salesreport/export/ExportReportState;

    iput-object p3, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->$props:Lcom/squareup/salesreport/export/ExportReportProps;

    iput-object p4, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 62
    invoke-virtual {p0}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    .line 112
    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->$state:Lcom/squareup/salesreport/export/ExportReportState;

    check-cast v0, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;

    invoke-virtual {v0}, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;->getEmail()Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$ValidEmail;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    .line 114
    iget-object v1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->$state:Lcom/squareup/salesreport/export/ExportReportState;

    check-cast v1, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;

    invoke-virtual {v1}, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;->getEmail()Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email;

    move-result-object v1

    check-cast v1, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$ValidEmail;

    .line 115
    iget-object v2, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    invoke-static {v2}, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->access$getIncludeItemsInReportLocalSetting$p(Lcom/squareup/salesreport/export/RealExportReportWorkflow;)Lcom/squareup/settings/LocalSetting;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v2

    const-string v3, "includeItemsInReportLocalSetting.get()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 116
    iget-object v3, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->$props:Lcom/squareup/salesreport/export/ExportReportProps;

    invoke-virtual {v3}, Lcom/squareup/salesreport/export/ExportReportProps;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v3

    .line 113
    invoke-static {v0, v1, v2, v3}, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->access$emailReport(Lcom/squareup/salesreport/export/RealExportReportWorkflow;Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$ValidEmail;ZLcom/squareup/customreport/data/ReportConfig;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    invoke-static {v0}, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->access$getSalesReportAnalytics$p(Lcom/squareup/salesreport/export/RealExportReportWorkflow;)Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->$props:Lcom/squareup/salesreport/export/ExportReportProps;

    invoke-virtual {v1}, Lcom/squareup/salesreport/export/ExportReportProps;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    .line 120
    iget-object v2, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    invoke-static {v2}, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->access$getAccountStatusSettings$p(Lcom/squareup/salesreport/export/RealExportReportWorkflow;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v2

    const-string v3, "accountStatusSettings.userSettings"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/settings/server/UserSettings;->getEmail()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->$state:Lcom/squareup/salesreport/export/ExportReportState;

    check-cast v3, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;

    invoke-virtual {v3}, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;->getEmail()Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email;->getEmail()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    .line 118
    invoke-interface {v0, v1, v2}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logReportEmailed(Lcom/squareup/customreport/data/ReportConfig;Z)V

    .line 122
    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SendEmailReport;

    iget-object v2, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->$props:Lcom/squareup/salesreport/export/ExportReportProps;

    invoke-virtual {v2}, Lcom/squareup/salesreport/export/ExportReportProps;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;->$state:Lcom/squareup/salesreport/export/ExportReportState;

    check-cast v3, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;

    invoke-virtual {v3}, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;->getEmail()Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email;

    move-result-object v3

    check-cast v3, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$ValidEmail;

    invoke-direct {v1, v2, v3}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SendEmailReport;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$ValidEmail;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
