.class public final Lcom/squareup/salesreport/chart/SalesDataPoint;
.super Lcom/squareup/chartography/DataPoint;
.source "SalesDataPoint.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/chartography/DataPoint<",
        "Lorg/threeten/bp/LocalDateTime;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0008\u0008\n\u0002\u0010\u0011\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u001d\u0012\u0006\u0010\u0004\u001a\u00020\u0002\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u0012\u001a\u00020\u0002H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\'\u0010\u0015\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00022\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\tR\u001c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u000cX\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u000f\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0002\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/salesreport/chart/SalesDataPoint;",
        "Lcom/squareup/chartography/DataPoint;",
        "Lorg/threeten/bp/LocalDateTime;",
        "",
        "time",
        "currentSale",
        "previousDaySale",
        "(Lorg/threeten/bp/LocalDateTime;JJ)V",
        "getCurrentSale",
        "()J",
        "getPreviousDaySale",
        "ranges",
        "",
        "getRanges",
        "()[Ljava/lang/Long;",
        "[Ljava/lang/Long;",
        "getTime",
        "()Lorg/threeten/bp/LocalDateTime;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentSale:J

.field private final previousDaySale:J

.field private final ranges:[Ljava/lang/Long;

.field private final time:Lorg/threeten/bp/LocalDateTime;


# direct methods
.method public constructor <init>(Lorg/threeten/bp/LocalDateTime;JJ)V
    .locals 1

    const-string/jumbo v0, "time"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0, p1}, Lcom/squareup/chartography/DataPoint;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->time:Lorg/threeten/bp/LocalDateTime;

    iput-wide p2, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->currentSale:J

    iput-wide p4, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->previousDaySale:J

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Long;

    .line 15
    iget-wide p2, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->currentSale:J

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    const/4 p3, 0x0

    aput-object p2, p1, p3

    iget-wide p2, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->previousDaySale:J

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    const/4 p3, 0x1

    aput-object p2, p1, p3

    iput-object p1, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->ranges:[Ljava/lang/Long;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/salesreport/chart/SalesDataPoint;Lorg/threeten/bp/LocalDateTime;JJILjava/lang/Object;)Lcom/squareup/salesreport/chart/SalesDataPoint;
    .locals 2

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->time:Lorg/threeten/bp/LocalDateTime;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-wide p2, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->currentSale:J

    :cond_1
    move-wide v0, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-wide p4, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->previousDaySale:J

    :cond_2
    move-wide p6, p4

    move-object p2, p0

    move-object p3, p1

    move-wide p4, v0

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/salesreport/chart/SalesDataPoint;->copy(Lorg/threeten/bp/LocalDateTime;JJ)Lcom/squareup/salesreport/chart/SalesDataPoint;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lorg/threeten/bp/LocalDateTime;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->time:Lorg/threeten/bp/LocalDateTime;

    return-object v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->currentSale:J

    return-wide v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->previousDaySale:J

    return-wide v0
.end method

.method public final copy(Lorg/threeten/bp/LocalDateTime;JJ)Lcom/squareup/salesreport/chart/SalesDataPoint;
    .locals 7

    const-string/jumbo v0, "time"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/salesreport/chart/SalesDataPoint;

    move-object v1, v0

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/salesreport/chart/SalesDataPoint;-><init>(Lorg/threeten/bp/LocalDateTime;JJ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/salesreport/chart/SalesDataPoint;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/salesreport/chart/SalesDataPoint;

    iget-object v0, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->time:Lorg/threeten/bp/LocalDateTime;

    iget-object v1, p1, Lcom/squareup/salesreport/chart/SalesDataPoint;->time:Lorg/threeten/bp/LocalDateTime;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->currentSale:J

    iget-wide v2, p1, Lcom/squareup/salesreport/chart/SalesDataPoint;->currentSale:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->previousDaySale:J

    iget-wide v2, p1, Lcom/squareup/salesreport/chart/SalesDataPoint;->previousDaySale:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCurrentSale()J
    .locals 2

    .line 12
    iget-wide v0, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->currentSale:J

    return-wide v0
.end method

.method public final getPreviousDaySale()J
    .locals 2

    .line 13
    iget-wide v0, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->previousDaySale:J

    return-wide v0
.end method

.method public getRanges()[Ljava/lang/Long;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->ranges:[Ljava/lang/Long;

    return-object v0
.end method

.method public bridge synthetic getRanges()[Ljava/lang/Number;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/salesreport/chart/SalesDataPoint;->getRanges()[Ljava/lang/Long;

    move-result-object v0

    check-cast v0, [Ljava/lang/Number;

    return-object v0
.end method

.method public final getTime()Lorg/threeten/bp/LocalDateTime;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->time:Lorg/threeten/bp/LocalDateTime;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->time:Lorg/threeten/bp/LocalDateTime;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->currentSale:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->previousDaySale:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SalesDataPoint(time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->time:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", currentSale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->currentSale:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", previousDaySale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/salesreport/chart/SalesDataPoint;->previousDaySale:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
