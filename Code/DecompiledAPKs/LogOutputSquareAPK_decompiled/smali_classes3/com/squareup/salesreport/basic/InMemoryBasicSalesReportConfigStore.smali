.class public final Lcom/squareup/salesreport/basic/InMemoryBasicSalesReportConfigStore;
.super Ljava/lang/Object;
.source "BasicSalesReportConfigStore.kt"

# interfaces
.implements Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\n\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0016J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0004H\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/salesreport/basic/InMemoryBasicSalesReportConfigStore;",
        "Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;",
        "()V",
        "basicSalesReportConfig",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "get",
        "set",
        "",
        "reportConfig",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private basicSalesReportConfig:Lcom/squareup/customreport/data/ReportConfig;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Lcom/squareup/customreport/data/ReportConfig;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/salesreport/basic/InMemoryBasicSalesReportConfigStore;->basicSalesReportConfig:Lcom/squareup/customreport/data/ReportConfig;

    return-object v0
.end method

.method public set(Lcom/squareup/customreport/data/ReportConfig;)V
    .locals 1

    const-string v0, "reportConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iput-object p1, p0, Lcom/squareup/salesreport/basic/InMemoryBasicSalesReportConfigStore;->basicSalesReportConfig:Lcom/squareup/customreport/data/ReportConfig;

    return-void
.end method
