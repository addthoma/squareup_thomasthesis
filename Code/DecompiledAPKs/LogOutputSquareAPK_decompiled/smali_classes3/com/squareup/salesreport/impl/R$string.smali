.class public final Lcom/squareup/salesreport/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final compare_report_label:I = 0x7f12046d

.field public static final customize_report_action_button:I = 0x7f1207ae

.field public static final customize_report_all_day:I = 0x7f1207af

.field public static final customize_report_all_devices:I = 0x7f1207b0

.field public static final customize_report_all_employees:I = 0x7f1207b1

.field public static final customize_report_card_title:I = 0x7f1207b2

.field public static final customize_report_device_filter:I = 0x7f1207b3

.field public static final customize_report_employee_filter_deselect_all:I = 0x7f1207b4

.field public static final customize_report_employee_filter_select_all:I = 0x7f1207b5

.field public static final customize_report_employee_filter_title:I = 0x7f1207b6

.field public static final customize_report_employee_name:I = 0x7f1207b7

.field public static final customize_report_end_time_label:I = 0x7f1207b8

.field public static final customize_report_filter_device_header_uppercase:I = 0x7f1207b9

.field public static final customize_report_filter_employee_header_uppercase:I = 0x7f1207ba

.field public static final customize_report_multiple_employee_filter_description:I = 0x7f1207bb

.field public static final customize_report_start_time_label:I = 0x7f1207bc

.field public static final email_report_connect_message:I = 0x7f12099a

.field public static final email_report_connect_message_title:I = 0x7f12099b

.field public static final email_report_edit_label:I = 0x7f12099c

.field public static final email_report_include_items:I = 0x7f12099d

.field public static final email_report_send_label:I = 0x7f12099e

.field public static final email_report_sent_message_title:I = 0x7f12099f

.field public static final email_report_title:I = 0x7f1209a0

.field public static final empty:I = 0x7f120a3a

.field public static final export_report_email_label:I = 0x7f120a9e

.field public static final export_report_label:I = 0x7f120a9f

.field public static final export_report_print_label:I = 0x7f120aa0

.field public static final popup_actions_view_dismiss:I = 0x7f121456

.field public static final print_report_header:I = 0x7f1214b1

.field public static final print_report_include_items:I = 0x7f1214b2

.field public static final print_report_print_label:I = 0x7f1214b3

.field public static final print_report_report_printed_title:I = 0x7f1214b4

.field public static final sales_report_action_bar_title:I = 0x7f1216b0

.field public static final sales_report_chart_scrub_compare_label_weekly_format:I = 0x7f1216b8

.field public static final sales_report_chart_scrub_label_date_format:I = 0x7f1216b9

.field public static final sales_report_chart_scrub_label_hour_format:I = 0x7f1216ba

.field public static final sales_report_chart_timespan_daily_uppercase:I = 0x7f1216bb

.field public static final sales_report_chart_timespan_monthly3_uppercase:I = 0x7f1216bc

.field public static final sales_report_chart_timespan_monthly_uppercase:I = 0x7f1216bd

.field public static final sales_report_chart_timespan_weekly_uppercase:I = 0x7f1216be

.field public static final sales_report_chart_timespan_yearly_uppercase:I = 0x7f1216bf

.field public static final sales_report_chart_title_custom_timespan_format:I = 0x7f1216c0

.field public static final sales_report_chart_title_preset_timespan_format:I = 0x7f1216c1

.field public static final sales_report_chart_type_gross_sales:I = 0x7f1216c2

.field public static final sales_report_chart_type_gross_sales_uppercase:I = 0x7f1216c3

.field public static final sales_report_chart_type_net_sales:I = 0x7f1216c4

.field public static final sales_report_chart_type_net_sales_uppercase:I = 0x7f1216c5

.field public static final sales_report_chart_type_sales_count:I = 0x7f1216c6

.field public static final sales_report_chart_type_sales_count_uppercase:I = 0x7f1216c7

.field public static final sales_report_compare_button_content_description:I = 0x7f1216c8

.field public static final sales_report_comparison_previous_day:I = 0x7f1216c9

.field public static final sales_report_comparison_previous_month:I = 0x7f1216ca

.field public static final sales_report_comparison_previous_three_months:I = 0x7f1216cb

.field public static final sales_report_comparison_previous_week:I = 0x7f1216cc

.field public static final sales_report_comparison_previous_year:I = 0x7f1216cd

.field public static final sales_report_comparison_same_day_previous_week:I = 0x7f1216ce

.field public static final sales_report_comparison_same_day_previous_year:I = 0x7f1216cf

.field public static final sales_report_comparison_same_month_previous_year:I = 0x7f1216d0

.field public static final sales_report_comparison_same_months_previous_year:I = 0x7f1216d1

.field public static final sales_report_comparison_same_week_previous_year:I = 0x7f1216d2

.field public static final sales_report_comparison_yesterday:I = 0x7f1216d3

.field public static final sales_report_count_tab:I = 0x7f1216d4

.field public static final sales_report_customize_button_content_description:I = 0x7f1216d6

.field public static final sales_report_dashboard_category_sales_url:I = 0x7f1216d7

.field public static final sales_report_dashboard_item_sales_url:I = 0x7f1216d8

.field public static final sales_report_date_format_date_only:I = 0x7f1216d9

.field public static final sales_report_date_format_date_range:I = 0x7f1216da

.field public static final sales_report_date_format_date_time_range:I = 0x7f1216db

.field public static final sales_report_date_format_day_of_week:I = 0x7f1216dc

.field public static final sales_report_date_format_day_of_week_letter:I = 0x7f1216dd

.field public static final sales_report_date_format_day_only:I = 0x7f1216de

.field public static final sales_report_date_format_month_day:I = 0x7f1216df

.field public static final sales_report_date_format_month_day_year:I = 0x7f1216e0

.field public static final sales_report_date_format_month_only:I = 0x7f1216e1

.field public static final sales_report_date_format_month_year:I = 0x7f1216e2

.field public static final sales_report_date_format_same_date_time_range:I = 0x7f1216e3

.field public static final sales_report_date_format_year_only:I = 0x7f1216e4

.field public static final sales_report_details_cash_rounding:I = 0x7f1216e5

.field public static final sales_report_details_discounts_and_comps:I = 0x7f1216e6

.field public static final sales_report_details_down_caret_description:I = 0x7f1216e7

.field public static final sales_report_details_gift_cards:I = 0x7f1216e8

.field public static final sales_report_details_gross_sales:I = 0x7f1216e9

.field public static final sales_report_details_label_uppercase:I = 0x7f1216ea

.field public static final sales_report_details_net_sales:I = 0x7f1216eb

.field public static final sales_report_details_refund:I = 0x7f1216ec

.field public static final sales_report_details_refunds_by_amount:I = 0x7f1216ed

.field public static final sales_report_details_right_caret_description:I = 0x7f1216ee

.field public static final sales_report_details_show_overview:I = 0x7f1216ef

.field public static final sales_report_details_tax:I = 0x7f1216f0

.field public static final sales_report_details_tips:I = 0x7f1216f1

.field public static final sales_report_details_total:I = 0x7f1216f2

.field public static final sales_report_employee_no_employees_found:I = 0x7f1216f7

.field public static final sales_report_employee_selection_search_hint:I = 0x7f1216f8

.field public static final sales_report_empty_button:I = 0x7f1216f9

.field public static final sales_report_empty_message:I = 0x7f1216fa

.field public static final sales_report_empty_title:I = 0x7f1216fb

.field public static final sales_report_fee_details_learn_more:I = 0x7f1216fc

.field public static final sales_report_fee_details_message:I = 0x7f1216fd

.field public static final sales_report_fee_details_title:I = 0x7f1216fe

.field public static final sales_report_header_category_uppercase:I = 0x7f121703

.field public static final sales_report_header_count:I = 0x7f121704

.field public static final sales_report_header_count_uppercase:I = 0x7f121705

.field public static final sales_report_header_gross:I = 0x7f121706

.field public static final sales_report_header_gross_uppercase:I = 0x7f121707

.field public static final sales_report_header_item_uppercase:I = 0x7f121708

.field public static final sales_report_more_options_button:I = 0x7f12170b

.field public static final sales_report_network_error_button:I = 0x7f12170e

.field public static final sales_report_network_error_message:I = 0x7f12170f

.field public static final sales_report_network_error_title:I = 0x7f121710

.field public static final sales_report_overflow_options_button_content_description:I = 0x7f121714

.field public static final sales_report_overview_average_sale_label:I = 0x7f121715

.field public static final sales_report_overview_discount_label:I = 0x7f121716

.field public static final sales_report_overview_gross_sales_label:I = 0x7f121717

.field public static final sales_report_overview_header_uppercase:I = 0x7f121718

.field public static final sales_report_overview_net_sales_label:I = 0x7f121719

.field public static final sales_report_overview_refunds_label:I = 0x7f12171a

.field public static final sales_report_overview_sales_label:I = 0x7f12171b

.field public static final sales_report_overview_show_details:I = 0x7f12171c

.field public static final sales_report_payment_method_card_title:I = 0x7f12171e

.field public static final sales_report_payment_method_cash_title:I = 0x7f12171f

.field public static final sales_report_payment_method_emoney_title:I = 0x7f121720

.field public static final sales_report_payment_method_external_title:I = 0x7f121721

.field public static final sales_report_payment_method_fees:I = 0x7f121722

.field public static final sales_report_payment_method_gift_card_title:I = 0x7f121723

.field public static final sales_report_payment_method_installment_title:I = 0x7f121724

.field public static final sales_report_payment_method_net_total:I = 0x7f121725

.field public static final sales_report_payment_method_other_title:I = 0x7f121726

.field public static final sales_report_payment_method_split_tender_title:I = 0x7f121727

.field public static final sales_report_payment_method_third_party_card_title:I = 0x7f121728

.field public static final sales_report_payment_method_total_collected:I = 0x7f121729

.field public static final sales_report_payment_method_zero_amount_title:I = 0x7f12172a

.field public static final sales_report_payment_methods_header_uppercase:I = 0x7f12172b

.field public static final sales_report_print_category_sales_uppercase:I = 0x7f12172c

.field public static final sales_report_print_discounts_applied_uppercase:I = 0x7f12172e

.field public static final sales_report_print_item_sales_uppercase:I = 0x7f12172f

.field public static final sales_report_print_payments_uppercase:I = 0x7f121731

.field public static final sales_report_print_report_date_printout_format:I = 0x7f121733

.field public static final sales_report_print_report_date_printout_pattern:I = 0x7f121734

.field public static final sales_report_print_sales_uppercase:I = 0x7f121735

.field public static final sales_report_print_title_uppercase:I = 0x7f121736

.field public static final sales_report_sales_chart_overflow_options_button_content_description:I = 0x7f12173b

.field public static final sales_report_summary_overflow_options_button_content_description:I = 0x7f12173f

.field public static final sales_report_time_selection_1_day:I = 0x7f121742

.field public static final sales_report_time_selection_1_month:I = 0x7f121743

.field public static final sales_report_time_selection_1_week:I = 0x7f121744

.field public static final sales_report_time_selection_1_year:I = 0x7f121745

.field public static final sales_report_time_selection_3_months:I = 0x7f121746

.field public static final sales_report_time_selection_custom_1_day:I = 0x7f121747

.field public static final sales_report_time_selection_custom_1_month:I = 0x7f121748

.field public static final sales_report_time_selection_custom_1_week:I = 0x7f121749

.field public static final sales_report_time_selection_custom_3_months:I = 0x7f12174a

.field public static final sales_report_time_selector_1day:I = 0x7f12174b

.field public static final sales_report_time_selector_1month:I = 0x7f12174c

.field public static final sales_report_time_selector_1week:I = 0x7f12174d

.field public static final sales_report_time_selector_1year:I = 0x7f12174e

.field public static final sales_report_time_selector_3month:I = 0x7f12174f

.field public static final sales_report_time_selector_one_day_content_description:I = 0x7f121750

.field public static final sales_report_time_selector_one_month_content_description:I = 0x7f121751

.field public static final sales_report_time_selector_one_week_content_description:I = 0x7f121752

.field public static final sales_report_time_selector_one_year_content_description:I = 0x7f121753

.field public static final sales_report_time_selector_three_months_content_description:I = 0x7f121754

.field public static final sales_report_top_bar_subtitle_format:I = 0x7f121756

.field public static final sales_report_top_bar_subtitle_format_without_comparison:I = 0x7f121757

.field public static final sales_report_top_bar_subtitle_format_without_employee:I = 0x7f121758

.field public static final sales_report_top_bar_subtitle_format_without_employee_and_comparison:I = 0x7f121759

.field public static final sales_report_top_bar_title_format:I = 0x7f12175a

.field public static final sales_report_top_categories_header_uppercase:I = 0x7f12175b

.field public static final sales_report_top_categories_overflow_options_button_content_description:I = 0x7f12175c

.field public static final sales_report_top_items_header_uppercase:I = 0x7f12175d

.field public static final sales_report_top_items_overflow_options_button_content_description:I = 0x7f12175e

.field public static final sales_report_unattributed_employee:I = 0x7f121761

.field public static final sales_report_view_collapse_category_details:I = 0x7f121762

.field public static final sales_report_view_collapse_item_details:I = 0x7f121763

.field public static final sales_report_view_count_10:I = 0x7f121764

.field public static final sales_report_view_count_5:I = 0x7f121765

.field public static final sales_report_view_count_all:I = 0x7f121766

.field public static final sales_report_view_expand_category_details:I = 0x7f121767

.field public static final sales_report_view_expand_item_details:I = 0x7f121768

.field public static final sales_report_view_in_dashboard:I = 0x7f121769


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
