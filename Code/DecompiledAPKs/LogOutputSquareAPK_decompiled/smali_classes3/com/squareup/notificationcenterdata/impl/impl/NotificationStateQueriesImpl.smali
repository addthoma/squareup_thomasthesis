.class final Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;
.super Lcom/squareup/sqldelight/TransacterImpl;
.source "DatabaseImpl.kt"

# interfaces
.implements Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;,
        Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectIdWithSourceAndState;,
        Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectIdWithState;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0003\u001a\u001b\u001cB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J \u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u001e\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00140\n2\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0016\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00140\n2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u001e\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00180\n2\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0008\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\n0\tX\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u001e\u0010\r\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\n0\tX\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000cR\u001e\u0010\u000f\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\n0\tX\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000c\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;",
        "Lcom/squareup/sqldelight/TransacterImpl;",
        "Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;",
        "database",
        "Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;",
        "driver",
        "Lcom/squareup/sqldelight/db/SqlDriver;",
        "(Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;Lcom/squareup/sqldelight/db/SqlDriver;)V",
        "selectIdWithSourceAndState",
        "",
        "Lcom/squareup/sqldelight/Query;",
        "getSelectIdWithSourceAndState$impl_release",
        "()Ljava/util/List;",
        "selectIdWithState",
        "getSelectIdWithState$impl_release",
        "selectState",
        "getSelectState$impl_release",
        "insertNotification",
        "",
        "id",
        "",
        "source",
        "Lcom/squareup/notificationcenterdata/Notification$Source;",
        "state",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        "removeNotification",
        "SelectIdWithSourceAndState",
        "SelectIdWithState",
        "SelectState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final database:Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;

.field private final driver:Lcom/squareup/sqldelight/db/SqlDriver;

.field private final selectIdWithSourceAndState:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/sqldelight/Query<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final selectIdWithState:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/sqldelight/Query<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final selectState:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/sqldelight/Query<",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;Lcom/squareup/sqldelight/db/SqlDriver;)V
    .locals 1

    const-string v0, "database"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "driver"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-direct {p0, p2}, Lcom/squareup/sqldelight/TransacterImpl;-><init>(Lcom/squareup/sqldelight/db/SqlDriver;)V

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->database:Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;

    iput-object p2, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->driver:Lcom/squareup/sqldelight/db/SqlDriver;

    .line 60
    invoke-static {}, Lcom/squareup/sqldelight/internal/FunctionsJvmKt;->copyOnWriteList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->selectState:Ljava/util/List;

    .line 62
    invoke-static {}, Lcom/squareup/sqldelight/internal/FunctionsJvmKt;->copyOnWriteList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->selectIdWithSourceAndState:Ljava/util/List;

    .line 64
    invoke-static {}, Lcom/squareup/sqldelight/internal/FunctionsJvmKt;->copyOnWriteList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->selectIdWithState:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$getDatabase$p(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;)Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->database:Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;

    return-object p0
.end method

.method public static final synthetic access$getDriver$p(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;)Lcom/squareup/sqldelight/db/SqlDriver;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->driver:Lcom/squareup/sqldelight/db/SqlDriver;

    return-object p0
.end method


# virtual methods
.method public final getSelectIdWithSourceAndState$impl_release()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/sqldelight/Query<",
            "*>;>;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->selectIdWithSourceAndState:Ljava/util/List;

    return-object v0
.end method

.method public final getSelectIdWithState$impl_release()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/sqldelight/Query<",
            "*>;>;"
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->selectIdWithState:Ljava/util/List;

    return-object v0
.end method

.method public final getSelectState$impl_release()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/sqldelight/Query<",
            "*>;>;"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->selectState:Ljava/util/List;

    return-object v0
.end method

.method public insertNotification(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)V
    .locals 4

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->driver:Lcom/squareup/sqldelight/db/SqlDriver;

    const v1, 0x4349ae97

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 89
    new-instance v3, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$insertNotification$1;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$insertNotification$1;-><init>(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const-string p1, "INSERT OR REPLACE INTO notification_states (id, source, state)\nVALUES (?1, ?2, ?3)"

    const/4 p2, 0x3

    .line 86
    invoke-interface {v0, v2, p1, p2, v3}, Lcom/squareup/sqldelight/db/SqlDriver;->execute(Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/functions/Function1;)V

    .line 94
    new-instance p1, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$insertNotification$2;

    invoke-direct {p1, p0}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$insertNotification$2;-><init>(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p0, v1, p1}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->notifyQueries(ILkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public removeNotification(Ljava/lang/String;)V
    .locals 5

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->driver:Lcom/squareup/sqldelight/db/SqlDriver;

    const v1, 0x1d628be2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 103
    new-instance v3, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$removeNotification$1;

    invoke-direct {v3, p1}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$removeNotification$1;-><init>(Ljava/lang/String;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const-string p1, "DELETE FROM notification_states\nWHERE id = ?1"

    const/4 v4, 0x1

    .line 100
    invoke-interface {v0, v2, p1, v4, v3}, Lcom/squareup/sqldelight/db/SqlDriver;->execute(Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/functions/Function1;)V

    .line 106
    new-instance p1, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$removeNotification$2;

    invoke-direct {p1, p0}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$removeNotification$2;-><init>(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p0, v1, p1}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->notifyQueries(ILkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public selectIdWithSourceAndState(Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/sqldelight/Query;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenterdata/Notification$Source;",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ")",
            "Lcom/squareup/sqldelight/Query<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    new-instance v0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectIdWithSourceAndState;

    sget-object v1, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectIdWithSourceAndState$1;->INSTANCE:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectIdWithSourceAndState$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectIdWithSourceAndState;-><init>(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/sqldelight/Query;

    return-object v0
.end method

.method public selectIdWithState(Lcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/sqldelight/Query;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ")",
            "Lcom/squareup/sqldelight/Query<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    new-instance v0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectIdWithState;

    sget-object v1, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectIdWithState$1;->INSTANCE:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectIdWithState$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectIdWithState;-><init>(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;Lcom/squareup/notificationcenterdata/Notification$State;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/sqldelight/Query;

    return-object v0
.end method

.method public selectState(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;)Lcom/squareup/sqldelight/Query;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/notificationcenterdata/Notification$Source;",
            ")",
            "Lcom/squareup/sqldelight/Query<",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ">;"
        }
    .end annotation

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    new-instance v0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;

    new-instance v1, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectState$1;

    invoke-direct {v1, p0}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectState$1;-><init>(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;-><init>(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/sqldelight/Query;

    return-object v0
.end method
