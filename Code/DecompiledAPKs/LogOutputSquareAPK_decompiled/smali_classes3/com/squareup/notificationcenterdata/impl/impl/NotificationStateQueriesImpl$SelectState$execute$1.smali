.class final Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState$execute$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DatabaseImpl.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;->execute()Lcom/squareup/sqldelight/db/SqlCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/sqldelight/db/SqlPreparedStatement;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\n\u0008\u0000\u0010\u0002 \u0001*\u00020\u0003*\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "",
        "Lcom/squareup/sqldelight/db/SqlPreparedStatement;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;


# direct methods
.method constructor <init>(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState$execute$1;->this$0:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 111
    check-cast p1, Lcom/squareup/sqldelight/db/SqlPreparedStatement;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState$execute$1;->invoke(Lcom/squareup/sqldelight/db/SqlPreparedStatement;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/sqldelight/db/SqlPreparedStatement;)V
    .locals 2

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState$execute$1;->this$0:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;

    iget-object v0, v0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;->id:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-interface {p1, v1, v0}, Lcom/squareup/sqldelight/db/SqlPreparedStatement;->bindString(ILjava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState$execute$1;->this$0:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;

    iget-object v0, v0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;->this$0:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;

    invoke-static {v0}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->access$getDatabase$p(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;)Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;->getNotification_statesAdapter$impl_release()Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;->getSourceAdapter()Lcom/squareup/sqldelight/ColumnAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState$execute$1;->this$0:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;

    iget-object v1, v1, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;->source:Lcom/squareup/notificationcenterdata/Notification$Source;

    invoke-interface {v0, v1}, Lcom/squareup/sqldelight/ColumnAdapter;->encode(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x2

    invoke-interface {p1, v1, v0}, Lcom/squareup/sqldelight/db/SqlPreparedStatement;->bindString(ILjava/lang/String;)V

    return-void
.end method
