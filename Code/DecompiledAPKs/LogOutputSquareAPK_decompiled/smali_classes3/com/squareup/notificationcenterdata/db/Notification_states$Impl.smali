.class public final Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;
.super Ljava/lang/Object;
.source "Notification_states.kt"

# interfaces
.implements Lcom/squareup/notificationcenterdata/db/Notification_states;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenterdata/db/Notification_states;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Impl"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\u0008\u0010\u0019\u001a\u00020\u0003H\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;",
        "Lcom/squareup/notificationcenterdata/db/Notification_states;",
        "id",
        "",
        "source",
        "Lcom/squareup/notificationcenterdata/Notification$Source;",
        "state",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        "(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)V",
        "getId",
        "()Ljava/lang/String;",
        "getSource",
        "()Lcom/squareup/notificationcenterdata/Notification$Source;",
        "getState",
        "()Lcom/squareup/notificationcenterdata/Notification$State;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final id:Ljava/lang/String;

.field private final source:Lcom/squareup/notificationcenterdata/Notification$Source;

.field private final state:Lcom/squareup/notificationcenterdata/Notification$State;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)V
    .locals 1

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->source:Lcom/squareup/notificationcenterdata/Notification$Source;

    iput-object p3, p0, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->state:Lcom/squareup/notificationcenterdata/Notification$State;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;ILjava/lang/Object;)Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getId()Ljava/lang/String;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getSource()Lcom/squareup/notificationcenterdata/Notification$Source;

    move-result-object p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getState()Lcom/squareup/notificationcenterdata/Notification$State;

    move-result-object p3

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->copy(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/notificationcenterdata/Notification$Source;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getSource()Lcom/squareup/notificationcenterdata/Notification$Source;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Lcom/squareup/notificationcenterdata/Notification$State;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getState()Lcom/squareup/notificationcenterdata/Notification$State;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;
    .locals 1

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;-><init>(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getSource()Lcom/squareup/notificationcenterdata/Notification$Source;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getSource()Lcom/squareup/notificationcenterdata/Notification$Source;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getState()Lcom/squareup/notificationcenterdata/Notification$State;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getState()Lcom/squareup/notificationcenterdata/Notification$State;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getSource()Lcom/squareup/notificationcenterdata/Notification$Source;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->source:Lcom/squareup/notificationcenterdata/Notification$Source;

    return-object v0
.end method

.method public getState()Lcom/squareup/notificationcenterdata/Notification$State;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->state:Lcom/squareup/notificationcenterdata/Notification$State;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getSource()Lcom/squareup/notificationcenterdata/Notification$Source;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getState()Lcom/squareup/notificationcenterdata/Notification$State;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\n    |Notification_states.Impl [\n    |  id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n    |  source: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getSource()Lcom/squareup/notificationcenterdata/Notification$Source;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n    |  state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;->getState()Lcom/squareup/notificationcenterdata/Notification$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n    |]\n    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 30
    invoke-static {v0, v1, v2, v1}, Lkotlin/text/StringsKt;->trimMargin$default(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
