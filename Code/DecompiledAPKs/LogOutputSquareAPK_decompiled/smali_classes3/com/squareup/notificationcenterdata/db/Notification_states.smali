.class public interface abstract Lcom/squareup/notificationcenterdata/db/Notification_states;
.super Ljava/lang/Object;
.source "Notification_states.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;,
        Lcom/squareup/notificationcenterdata/db/Notification_states$Impl;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008f\u0018\u00002\u00020\u0001:\u0002\u000e\u000fR\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u0012\u0010\n\u001a\u00020\u000bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/db/Notification_states;",
        "",
        "id",
        "",
        "getId",
        "()Ljava/lang/String;",
        "source",
        "Lcom/squareup/notificationcenterdata/Notification$Source;",
        "getSource",
        "()Lcom/squareup/notificationcenterdata/Notification$Source;",
        "state",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        "getState",
        "()Lcom/squareup/notificationcenterdata/Notification$State;",
        "Adapter",
        "Impl",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getSource()Lcom/squareup/notificationcenterdata/Notification$Source;
.end method

.method public abstract getState()Lcom/squareup/notificationcenterdata/Notification$State;
.end method
