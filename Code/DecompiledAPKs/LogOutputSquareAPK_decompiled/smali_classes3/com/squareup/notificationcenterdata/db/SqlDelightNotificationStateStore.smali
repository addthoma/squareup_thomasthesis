.class public final Lcom/squareup/notificationcenterdata/db/SqlDelightNotificationStateStore;
.super Ljava/lang/Object;
.source "SqlDelightNotificationStateStore.kt"

# interfaces
.implements Lcom/squareup/notificationcenterdata/NotificationStateStore;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0019\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J \u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J$\u0010\u000f\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\u00110\u00102\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u001c\u0010\u000f\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\u00110\u00102\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u001e\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u00102\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0010\u0010\u0013\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/db/SqlDelightNotificationStateStore;",
        "Lcom/squareup/notificationcenterdata/NotificationStateStore;",
        "database",
        "Lcom/squareup/notificationcenterdata/impl/Database;",
        "fileScheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/notificationcenterdata/impl/Database;Lio/reactivex/Scheduler;)V",
        "addNotification",
        "",
        "id",
        "",
        "source",
        "Lcom/squareup/notificationcenterdata/Notification$Source;",
        "state",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        "getNotificationIds",
        "Lio/reactivex/Observable;",
        "",
        "getNotificationState",
        "removeNotificationWithId",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final database:Lcom/squareup/notificationcenterdata/impl/Database;

.field private final fileScheduler:Lio/reactivex/Scheduler;


# direct methods
.method public constructor <init>(Lcom/squareup/notificationcenterdata/impl/Database;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "database"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/db/SqlDelightNotificationStateStore;->database:Lcom/squareup/notificationcenterdata/impl/Database;

    iput-object p2, p0, Lcom/squareup/notificationcenterdata/db/SqlDelightNotificationStateStore;->fileScheduler:Lio/reactivex/Scheduler;

    return-void
.end method


# virtual methods
.method public addNotification(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)V
    .locals 1

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/SqlDelightNotificationStateStore;->database:Lcom/squareup/notificationcenterdata/impl/Database;

    invoke-interface {v0}, Lcom/squareup/notificationcenterdata/impl/Database;->getNotificationStateQueries()Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;

    move-result-object v0

    .line 27
    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;->insertNotification(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)V

    return-void
.end method

.method public getNotificationIds(Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenterdata/Notification$Source;",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/SqlDelightNotificationStateStore;->database:Lcom/squareup/notificationcenterdata/impl/Database;

    invoke-interface {v0}, Lcom/squareup/notificationcenterdata/impl/Database;->getNotificationStateQueries()Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;

    move-result-object v0

    .line 40
    invoke-interface {v0, p1, p2}, Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;->selectIdWithSourceAndState(Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/sqldelight/Query;

    move-result-object p1

    .line 41
    iget-object p2, p0, Lcom/squareup/notificationcenterdata/db/SqlDelightNotificationStateStore;->fileScheduler:Lio/reactivex/Scheduler;

    invoke-static {p1, p2}, Lcom/squareup/sqldelight/runtime/rx/RxQuery;->toObservable(Lcom/squareup/sqldelight/Query;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 42
    invoke-static {p1}, Lcom/squareup/sqldelight/runtime/rx/RxQuery;->mapToList(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getNotificationIds(Lcom/squareup/notificationcenterdata/Notification$State;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/SqlDelightNotificationStateStore;->database:Lcom/squareup/notificationcenterdata/impl/Database;

    invoke-interface {v0}, Lcom/squareup/notificationcenterdata/impl/Database;->getNotificationStateQueries()Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;

    move-result-object v0

    .line 47
    invoke-interface {v0, p1}, Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;->selectIdWithState(Lcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/sqldelight/Query;

    move-result-object p1

    .line 48
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/SqlDelightNotificationStateStore;->fileScheduler:Lio/reactivex/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/sqldelight/runtime/rx/RxQuery;->toObservable(Lcom/squareup/sqldelight/Query;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 49
    invoke-static {p1}, Lcom/squareup/sqldelight/runtime/rx/RxQuery;->mapToList(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getNotificationState(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/notificationcenterdata/Notification$Source;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ">;"
        }
    .end annotation

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/SqlDelightNotificationStateStore;->database:Lcom/squareup/notificationcenterdata/impl/Database;

    invoke-interface {v0}, Lcom/squareup/notificationcenterdata/impl/Database;->getNotificationStateQueries()Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;

    move-result-object v0

    .line 57
    invoke-interface {v0, p1, p2}, Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;->selectState(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;)Lcom/squareup/sqldelight/Query;

    move-result-object p1

    .line 58
    iget-object p2, p0, Lcom/squareup/notificationcenterdata/db/SqlDelightNotificationStateStore;->fileScheduler:Lio/reactivex/Scheduler;

    invoke-static {p1, p2}, Lcom/squareup/sqldelight/runtime/rx/RxQuery;->toObservable(Lcom/squareup/sqldelight/Query;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 59
    invoke-static {p1}, Lcom/squareup/sqldelight/runtime/rx/RxQuery;->mapToOneNonNull(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public removeNotificationWithId(Ljava/lang/String;)V
    .locals 1

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/SqlDelightNotificationStateStore;->database:Lcom/squareup/notificationcenterdata/impl/Database;

    invoke-interface {v0}, Lcom/squareup/notificationcenterdata/impl/Database;->getNotificationStateQueries()Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;

    move-result-object v0

    .line 32
    invoke-interface {v0, p1}, Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;->removeNotification(Ljava/lang/String;)V

    return-void
.end method
