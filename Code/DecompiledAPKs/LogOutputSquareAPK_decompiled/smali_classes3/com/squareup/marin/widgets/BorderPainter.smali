.class public Lcom/squareup/marin/widgets/BorderPainter;
.super Ljava/lang/Object;
.source "BorderPainter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/marin/widgets/BorderPainter$Borders;
    }
.end annotation


# static fields
.field public static final ALL:I = 0xf

.field public static final BOTTOM:I = 0x8

.field public static final LEFT:I = 0x1

.field public static final NONE:I = 0x0

.field public static final RIGHT:I = 0x4

.field public static final TOP:I = 0x2


# instance fields
.field private final borderPaint:Landroid/graphics/Paint;

.field private borderWidth:I

.field private final canvasBounds:Landroid/graphics/Rect;

.field private drawInside:Z

.field private final hostView:Landroid/view/View;

.field private leftInset:I

.field private rightInset:I

.field private sidesToPaint:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .line 71
    sget v0, Lcom/squareup/marin/R$dimen;->marin_divider_width_1dp:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/marin/widgets/BorderPainter;-><init>(Landroid/view/View;I)V

    return-void
.end method

.method public constructor <init>(Landroid/view/View;I)V
    .locals 1

    .line 75
    sget v0, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/marin/widgets/BorderPainter;-><init>(Landroid/view/View;II)V

    return-void
.end method

.method public constructor <init>(Landroid/view/View;II)V
    .locals 2

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    const/4 v0, 0x0

    .line 64
    iput v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->sidesToPaint:I

    .line 79
    iput-object p1, p0, Lcom/squareup/marin/widgets/BorderPainter;->hostView:Landroid/view/View;

    .line 80
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 81
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderPaint:Landroid/graphics/Paint;

    .line 82
    iget-object v1, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    invoke-virtual {v1, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 83
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderWidth:I

    const/4 p1, 0x1

    .line 84
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/BorderPainter;->drawInside:Z

    .line 85
    iput v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->rightInset:I

    .line 86
    iput v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->leftInset:I

    return-void
.end method


# virtual methods
.method public addBorder(I)V
    .locals 1

    .line 137
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->sidesToPaint:I

    or-int/2addr p1, v0

    iput p1, p0, Lcom/squareup/marin/widgets/BorderPainter;->sidesToPaint:I

    return-void
.end method

.method public clearBorders()V
    .locals 1

    const/4 v0, 0x0

    .line 141
    iput v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->sidesToPaint:I

    return-void
.end method

.method public clearEmphasis()V
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    return-void
.end method

.method public drawBorderInside()V
    .locals 1

    const/4 v0, 0x1

    .line 106
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->drawInside:Z

    return-void
.end method

.method public drawBorderOutside()V
    .locals 1

    const/4 v0, 0x0

    .line 99
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->drawInside:Z

    return-void
.end method

.method public drawBorders(Landroid/graphics/Canvas;)V
    .locals 7

    .line 149
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->drawInside:Z

    if-eqz v0, :cond_5

    .line 154
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->hostView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    .line 159
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->sidesToPaint:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderWidth:I

    add-int/2addr v0, v1

    int-to-float v4, v0

    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v0

    iget-object v6, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderPaint:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 164
    :cond_1
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->sidesToPaint:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 165
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lcom/squareup/marin/widgets/BorderPainter;->leftInset:I

    add-int/2addr v0, v1

    int-to-float v2, v0

    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget v1, p0, Lcom/squareup/marin/widgets/BorderPainter;->rightInset:I

    sub-int/2addr v0, v1

    int-to-float v4, v0

    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderWidth:I

    add-int/2addr v0, v1

    int-to-float v5, v0

    iget-object v6, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderPaint:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 169
    :cond_2
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->sidesToPaint:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 170
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget v1, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderWidth:I

    sub-int/2addr v0, v1

    int-to-float v2, v0

    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v4, v0

    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v0

    iget-object v6, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderPaint:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 174
    :cond_3
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->sidesToPaint:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 175
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lcom/squareup/marin/widgets/BorderPainter;->leftInset:I

    add-int/2addr v0, v1

    int-to-float v2, v0

    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v1, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderWidth:I

    sub-int/2addr v0, v1

    int-to-float v3, v0

    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget v1, p0, Lcom/squareup/marin/widgets/BorderPainter;->rightInset:I

    sub-int/2addr v0, v1

    int-to-float v4, v0

    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v0

    iget-object v6, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderPaint:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_4
    return-void

    .line 150
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Can only drawBorders on the inside!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public drawChildBorders(Landroid/graphics/Canvas;Landroid/view/View;)V
    .locals 13

    .line 186
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->hostView:Landroid/view/View;

    if-eq p2, v0, :cond_a

    .line 190
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_9

    .line 193
    check-cast v0, Landroid/view/ViewGroup;

    .line 196
    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 200
    :cond_0
    invoke-static {p2, v0}, Lcom/squareup/util/Views;->getLeftRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;)I

    move-result v1

    .line 201
    invoke-static {p2, v0}, Lcom/squareup/util/Views;->getTopRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;)I

    move-result v2

    .line 202
    invoke-static {p2, v0}, Lcom/squareup/util/Views;->getRightRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;)I

    move-result v3

    .line 203
    invoke-static {p2, v0}, Lcom/squareup/util/Views;->getBottomRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;)I

    move-result p2

    .line 205
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->sidesToPaint:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 209
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->drawInside:Z

    if-eqz v0, :cond_1

    .line 211
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderWidth:I

    add-int/2addr v0, v1

    move v4, v0

    move v0, v1

    goto :goto_0

    .line 213
    :cond_1
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderWidth:I

    sub-int v0, v1, v0

    move v4, v1

    :goto_0
    int-to-float v6, v0

    int-to-float v7, v2

    int-to-float v8, v4

    int-to-float v9, p2

    .line 216
    iget-object v10, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderPaint:Landroid/graphics/Paint;

    move-object v5, p1

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 219
    :cond_2
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->sidesToPaint:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    .line 222
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->drawInside:Z

    if-eqz v0, :cond_3

    .line 224
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderWidth:I

    add-int/2addr v0, v2

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 226
    :cond_3
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderWidth:I

    sub-int v0, v2, v0

    move v4, v2

    .line 229
    :goto_1
    iget v5, p0, Lcom/squareup/marin/widgets/BorderPainter;->leftInset:I

    add-int/2addr v5, v1

    int-to-float v7, v5

    int-to-float v8, v0

    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->rightInset:I

    sub-int v0, v3, v0

    int-to-float v9, v0

    int-to-float v10, v4

    iget-object v11, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderPaint:Landroid/graphics/Paint;

    move-object v6, p1

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 233
    :cond_4
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->sidesToPaint:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    .line 236
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->drawInside:Z

    if-eqz v0, :cond_5

    .line 237
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderWidth:I

    sub-int v0, v3, v0

    move v4, v3

    goto :goto_2

    .line 241
    :cond_5
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderWidth:I

    add-int/2addr v0, v3

    move v4, v0

    move v0, v3

    :goto_2
    int-to-float v6, v0

    int-to-float v7, v2

    int-to-float v8, v4

    int-to-float v9, p2

    .line 243
    iget-object v10, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderPaint:Landroid/graphics/Paint;

    move-object v5, p1

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 246
    :cond_6
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->sidesToPaint:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_8

    .line 249
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->drawInside:Z

    if-eqz v0, :cond_7

    .line 250
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderWidth:I

    sub-int v0, p2, v0

    move v12, v0

    move v0, p2

    move p2, v12

    goto :goto_3

    .line 254
    :cond_7
    iget v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderWidth:I

    add-int/2addr v0, p2

    .line 256
    :goto_3
    iget v2, p0, Lcom/squareup/marin/widgets/BorderPainter;->leftInset:I

    add-int/2addr v1, v2

    int-to-float v5, v1

    int-to-float v6, p2

    iget p2, p0, Lcom/squareup/marin/widgets/BorderPainter;->rightInset:I

    sub-int/2addr v3, p2

    int-to-float v7, v3

    int-to-float v8, v0

    iget-object v9, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderPaint:Landroid/graphics/Paint;

    move-object v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_8
    return-void

    .line 191
    :cond_9
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "hostView must be a ViewGroup if it allegedly has children."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 187
    :cond_a
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "It\'s gonna look bad, call `drawBorders()` instead!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setBorderWidth(I)V
    .locals 0

    .line 125
    iput p1, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderWidth:I

    return-void
.end method

.method public setColor(I)V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public setHorizontalInsets(I)V
    .locals 0

    .line 113
    iput p1, p0, Lcom/squareup/marin/widgets/BorderPainter;->rightInset:I

    .line 114
    iput p1, p0, Lcom/squareup/marin/widgets/BorderPainter;->leftInset:I

    return-void
.end method

.method public setMultiply()V
    .locals 3

    .line 133
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderPainter;->borderPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    return-void
.end method

.method public setRightInset(I)V
    .locals 0

    .line 121
    iput p1, p0, Lcom/squareup/marin/widgets/BorderPainter;->rightInset:I

    return-void
.end method
