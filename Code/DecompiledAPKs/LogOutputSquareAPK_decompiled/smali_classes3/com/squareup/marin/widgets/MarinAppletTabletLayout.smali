.class public final Lcom/squareup/marin/widgets/MarinAppletTabletLayout;
.super Landroid/view/ViewGroup;
.source "MarinAppletTabletLayout.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/marin/widgets/MarinAppletTabletLayout$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMarinAppletTabletLayout.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MarinAppletTabletLayout.kt\ncom/squareup/marin/widgets/MarinAppletTabletLayout\n*L\n1#1,143:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0007\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\t\u0018\u0000  2\u00020\u0001:\u0001 B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0014J\u0008\u0010\u0015\u001a\u00020\u0012H\u0014J0\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\n2\u0006\u0010\u001a\u001a\u00020\n2\u0006\u0010\u001b\u001a\u00020\n2\u0006\u0010\u001c\u001a\u00020\nH\u0014J\u0018\u0010\u001d\u001a\u00020\u00122\u0006\u0010\u001e\u001a\u00020\n2\u0006\u0010\u001f\u001a\u00020\nH\u0014R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/marin/widgets/MarinAppletTabletLayout;",
        "Landroid/view/ViewGroup;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "detailView",
        "Landroid/view/View;",
        "dividerOffsetFromTop",
        "",
        "dividerPaint",
        "Landroid/graphics/Paint;",
        "dividerWidth",
        "masterColumnRatio",
        "",
        "masterView",
        "onDraw",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "onFinishInflate",
        "onLayout",
        "changed",
        "",
        "l",
        "t",
        "r",
        "b",
        "onMeasure",
        "widthMeasureSpec",
        "heightMeasureSpec",
        "Companion",
        "marin_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/marin/widgets/MarinAppletTabletLayout$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MASTER_COLUMN_RELATIVE_WIDTH:F = 0.3f


# instance fields
.field private detailView:Landroid/view/View;

.field private final dividerOffsetFromTop:I

.field private final dividerPaint:Landroid/graphics/Paint;

.field private final dividerWidth:I

.field private final masterColumnRatio:F

.field private masterView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/marin/widgets/MarinAppletTabletLayout$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->Companion:Lcom/squareup/marin/widgets/MarinAppletTabletLayout$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 42
    sget-object v1, Lcom/squareup/marin/R$styleable;->MarinAppletTabletLayout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 44
    sget p2, Lcom/squareup/marin/R$styleable;->MarinAppletTabletLayout_ratio:I

    const v1, 0x3e99999a    # 0.3f

    .line 43
    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result p2

    iput p2, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->masterColumnRatio:F

    .line 49
    sget p2, Lcom/squareup/marin/R$styleable;->MarinAppletTabletLayout_dividerWidth:I

    .line 50
    sget v1, Lcom/squareup/marin/R$dimen;->marin_divider_width_1dp:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 48
    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->dividerWidth:I

    .line 53
    sget p2, Lcom/squareup/marin/R$styleable;->MarinAppletTabletLayout_dividerOffsetFromTop:I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->dividerOffsetFromTop:I

    .line 55
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 58
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->dividerPaint:Landroid/graphics/Paint;

    .line 59
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->dividerPaint:Landroid/graphics/Paint;

    sget p2, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 62
    invoke-virtual {p0, v1}, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->setWillNotDraw(Z)V

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 128
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->masterView:Landroid/view/View;

    const-string v1, "masterView"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/util/Views;->isVisible(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 130
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->masterView:Landroid/view/View;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v3, v0

    .line 131
    iget v0, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->dividerOffsetFromTop:I

    int-to-float v4, v0

    .line 132
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->masterView:Landroid/view/View;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->dividerWidth:I

    add-int/2addr v0, v1

    int-to-float v5, v0

    .line 133
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->getHeight()I

    move-result v0

    int-to-float v6, v0

    .line 134
    iget-object v7, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->dividerPaint:Landroid/graphics/Paint;

    move-object v2, p1

    .line 129
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_3
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 66
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    const/4 v0, 0x0

    .line 67
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "getChildAt(0)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->masterView:Landroid/view/View;

    const/4 v0, 0x1

    .line 68
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "getChildAt(1)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->detailView:Landroid/view/View;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .line 111
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->getMeasuredWidth()I

    move-result p1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->getMeasuredHeight()I

    move-result p2

    .line 114
    iget-object p3, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->masterView:Landroid/view/View;

    const-string p4, "masterView"

    if-nez p3, :cond_0

    invoke-static {p4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {p3}, Lcom/squareup/util/Views;->isVisible(Landroid/view/View;)Z

    move-result p3

    const-string p5, "detailView"

    const/4 v0, 0x0

    if-nez p3, :cond_2

    .line 115
    iget-object p3, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->detailView:Landroid/view/View;

    if-nez p3, :cond_1

    invoke-static {p5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p3, v0, v0, p1, p2}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    .line 118
    :cond_2
    iget-object p3, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->masterView:Landroid/view/View;

    if-nez p3, :cond_3

    invoke-static {p4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p3}, Landroid/view/View;->getMeasuredWidth()I

    move-result p3

    .line 119
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->masterView:Landroid/view/View;

    if-nez v1, :cond_4

    invoke-static {p4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v1, v0, v0, p3, p2}, Landroid/view/View;->layout(IIII)V

    .line 122
    iget-object p4, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->detailView:Landroid/view/View;

    if-nez p4, :cond_5

    invoke-static {p5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    iget p5, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->dividerWidth:I

    add-int/2addr p3, p5

    invoke-virtual {p4, p3, v0, p1, p2}, Landroid/view/View;->layout(IIII)V

    :goto_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .line 75
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    .line 76
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    .line 78
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->masterView:Landroid/view/View;

    const-string v1, "masterView"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/util/Views;->isVisible(Landroid/view/View;)Z

    move-result v0

    const-string v2, "detailView"

    const/high16 v3, 0x40000000    # 2.0f

    if-nez v0, :cond_2

    .line 80
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->detailView:Landroid/view/View;

    if-nez v0, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 81
    :cond_1
    invoke-static {p1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 82
    invoke-static {p2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 80
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    goto :goto_0

    :cond_2
    int-to-float v0, p1

    .line 86
    iget v4, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->masterColumnRatio:F

    mul-float v4, v4, v0

    float-to-int v4, v4

    .line 87
    iget-object v5, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->masterView:Landroid/view/View;

    if-nez v5, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 88
    :cond_3
    iget v1, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->dividerWidth:I

    sub-int/2addr v4, v1

    invoke-static {v4, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 89
    invoke-static {p2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 87
    invoke-virtual {v5, v1, v4}, Landroid/view/View;->measure(II)V

    const/4 v1, 0x1

    int-to-float v1, v1

    .line 93
    iget v4, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->masterColumnRatio:F

    sub-float/2addr v1, v4

    mul-float v0, v0, v1

    float-to-int v0, v0

    .line 94
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->detailView:Landroid/view/View;

    if-nez v1, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 95
    :cond_4
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 96
    invoke-static {p2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 94
    invoke-virtual {v1, v0, v2}, Landroid/view/View;->measure(II)V

    .line 101
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/marin/widgets/MarinAppletTabletLayout;->setMeasuredDimension(II)V

    return-void
.end method
