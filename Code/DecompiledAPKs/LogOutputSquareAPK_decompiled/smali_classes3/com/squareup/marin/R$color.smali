.class public final Lcom/squareup/marin/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/marin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final marin_action_bar_background:I = 0x7f060135

.field public static final marin_action_bar_background_inverted:I = 0x7f060136

.field public static final marin_action_bar_divider_inverted:I = 0x7f060137

.field public static final marin_barely_dark_translucent_pressed:I = 0x7f060138

.field public static final marin_black:I = 0x7f060139

.field public static final marin_black_transparent_fifty:I = 0x7f06013a

.field public static final marin_black_transparent_forty:I = 0x7f06013b

.field public static final marin_black_transparent_seventyfive:I = 0x7f06013c

.field public static final marin_black_transparent_thirty:I = 0x7f06013d

.field public static final marin_blue:I = 0x7f06013e

.field public static final marin_blue_disabled:I = 0x7f06013f

.field public static final marin_blue_divider:I = 0x7f060140

.field public static final marin_blue_pressed:I = 0x7f060141

.field public static final marin_blue_selected:I = 0x7f060142

.field public static final marin_blue_selected_pressed:I = 0x7f060143

.field public static final marin_clear:I = 0x7f060144

.field public static final marin_dark_gray:I = 0x7f060145

.field public static final marin_dark_gray_pressed:I = 0x7f060146

.field public static final marin_dark_green:I = 0x7f060147

.field public static final marin_dark_translucent:I = 0x7f060148

.field public static final marin_dark_translucent_pressed:I = 0x7f060149

.field public static final marin_darker_blue:I = 0x7f06014a

.field public static final marin_darker_blue_pressed:I = 0x7f06014b

.field public static final marin_darker_gray:I = 0x7f06014c

.field public static final marin_darker_gray_pressed:I = 0x7f06014d

.field public static final marin_dim_translucent_pressed:I = 0x7f06014e

.field public static final marin_divider_gray:I = 0x7f06014f

.field public static final marin_drawer_background:I = 0x7f060150

.field public static final marin_drawer_background_pressed:I = 0x7f060151

.field public static final marin_drawer_dark_gray:I = 0x7f060152

.field public static final marin_fake_transparent_white_on_gray:I = 0x7f060153

.field public static final marin_focused_fill:I = 0x7f060154

.field public static final marin_green:I = 0x7f060155

.field public static final marin_green_border:I = 0x7f060156

.field public static final marin_green_pressed:I = 0x7f060157

.field public static final marin_light_gray:I = 0x7f060158

.field public static final marin_light_gray_pressed:I = 0x7f060159

.field public static final marin_light_green:I = 0x7f06015a

.field public static final marin_medium_gray:I = 0x7f06015b

.field public static final marin_medium_gray_pressed:I = 0x7f06015c

.field public static final marin_orange:I = 0x7f06015d

.field public static final marin_orange_disabled:I = 0x7f06015e

.field public static final marin_order_entry_tab_color:I = 0x7f06015f

.field public static final marin_red:I = 0x7f060160

.field public static final marin_red_pressed:I = 0x7f060161

.field public static final marin_screen_scrim:I = 0x7f060162

.field public static final marin_text_selector_blue:I = 0x7f060163

.field public static final marin_text_selector_blue_disabled_dark_gray:I = 0x7f060164

.field public static final marin_text_selector_blue_disabled_light_gray:I = 0x7f060165

.field public static final marin_text_selector_blue_disabled_light_gray_activated_white:I = 0x7f060166

.field public static final marin_text_selector_blue_disabled_medium_gray:I = 0x7f060167

.field public static final marin_text_selector_dark_gray:I = 0x7f060168

.field public static final marin_text_selector_dark_gray_checked_blue:I = 0x7f060169

.field public static final marin_text_selector_dark_gray_checked_white:I = 0x7f06016a

.field public static final marin_text_selector_dark_gray_disabled_light_gray_pressed_light_gray:I = 0x7f06016b

.field public static final marin_text_selector_dark_gray_disabled_medium_gray:I = 0x7f06016c

.field public static final marin_text_selector_dark_gray_selected_blue:I = 0x7f06016d

.field public static final marin_text_selector_dark_green_checked_light_green:I = 0x7f06016e

.field public static final marin_text_selector_drawer_dark_selected_white:I = 0x7f06016f

.field public static final marin_text_selector_edit_text:I = 0x7f060170

.field public static final marin_text_selector_light_gray_selected_white:I = 0x7f060171

.field public static final marin_text_selector_list:I = 0x7f060172

.field public static final marin_text_selector_medium_gray:I = 0x7f060173

.field public static final marin_text_selector_medium_gray_checked_white:I = 0x7f060174

.field public static final marin_text_selector_medium_gray_disabled_light_gray:I = 0x7f060175

.field public static final marin_text_selector_white:I = 0x7f060176

.field public static final marin_text_selector_white_disabled_dark_gray:I = 0x7f060177

.field public static final marin_text_selector_white_disabled_fake_transparent:I = 0x7f060178

.field public static final marin_text_selector_white_disabled_light_gray:I = 0x7f060179

.field public static final marin_text_selector_white_disabled_white_translucent:I = 0x7f06017a

.field public static final marin_text_shadow:I = 0x7f06017b

.field public static final marin_translucent_black:I = 0x7f06017c

.field public static final marin_translucent_dark:I = 0x7f06017d

.field public static final marin_transparent:I = 0x7f06017e

.field public static final marin_ultra_light_gray:I = 0x7f06017f

.field public static final marin_ultra_light_gray_disabled:I = 0x7f060180

.field public static final marin_ultra_light_gray_inline_button_disabled:I = 0x7f060181

.field public static final marin_ultra_light_gray_pressed:I = 0x7f060182

.field public static final marin_white:I = 0x7f060183

.field public static final marin_white_translucent:I = 0x7f060184

.field public static final marin_white_transparent_twenty:I = 0x7f060185

.field public static final marin_window_background:I = 0x7f060186

.field public static final marin_window_background_dark:I = 0x7f060187

.field public static final marin_window_background_inverted:I = 0x7f060188


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
