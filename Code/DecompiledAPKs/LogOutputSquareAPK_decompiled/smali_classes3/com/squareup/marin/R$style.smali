.class public final Lcom/squareup/marin/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/marin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final BottomSheetDialogStyle:I = 0x7f1300e2

.field public static final DateTimePicker:I = 0x7f1300f3

.field public static final MarinButtonBar:I = 0x7f130137

.field public static final OrientationVerticalOnPhonePortrait:I = 0x7f13014e

.field public static final TextAppearance_Marin:I = 0x7f13022f

.field public static final TextAppearance_Marin_Badge_Drawer:I = 0x7f130230

.field public static final TextAppearance_Marin_Badge_Hamburger:I = 0x7f130231

.field public static final TextAppearance_Marin_Badge_Hamburger_Wide:I = 0x7f130232

.field public static final TextAppearance_Marin_Blue:I = 0x7f130233

.field public static final TextAppearance_Marin_Charge:I = 0x7f130234

.field public static final TextAppearance_Marin_Charge_White:I = 0x7f130235

.field public static final TextAppearance_Marin_Display:I = 0x7f130236

.field public static final TextAppearance_Marin_Gray:I = 0x7f130237

.field public static final TextAppearance_Marin_Gray_Landing:I = 0x7f130238

.field public static final TextAppearance_Marin_HeaderTitle:I = 0x7f130239

.field public static final TextAppearance_Marin_HeaderTitle_Blue:I = 0x7f13023a

.field public static final TextAppearance_Marin_HeaderTitle_Medium:I = 0x7f13023c

.field public static final TextAppearance_Marin_HeaderTitle_MediumGray:I = 0x7f130241

.field public static final TextAppearance_Marin_HeaderTitle_Medium_Blue:I = 0x7f13023d

.field public static final TextAppearance_Marin_HeaderTitle_Medium_Centered:I = 0x7f13023e

.field public static final TextAppearance_Marin_HeaderTitle_Medium_Centered_Blue:I = 0x7f13023f

.field public static final TextAppearance_Marin_HeaderTitle_Medium_White:I = 0x7f130240

.field public static final TextAppearance_Marin_HeaderTitle_NullState:I = 0x7f130242

.field public static final TextAppearance_Marin_HeaderTitle_White:I = 0x7f130243

.field public static final TextAppearance_Marin_Headline:I = 0x7f130244

.field public static final TextAppearance_Marin_Headline_Medium:I = 0x7f130245

.field public static final TextAppearance_Marin_Headline_White:I = 0x7f130246

.field public static final TextAppearance_Marin_Headline_White_Medium:I = 0x7f130247

.field public static final TextAppearance_Marin_HelpText:I = 0x7f130248

.field public static final TextAppearance_Marin_HelpText_DarkGray:I = 0x7f130249

.field public static final TextAppearance_Marin_HelpText_Large:I = 0x7f13024a

.field public static final TextAppearance_Marin_HelpText_Large_Italic:I = 0x7f13024b

.field public static final TextAppearance_Marin_HelpText_Red:I = 0x7f13024c

.field public static final TextAppearance_Marin_HelpText_Sign:I = 0x7f13024d

.field public static final TextAppearance_Marin_HelpText_White:I = 0x7f13024e

.field public static final TextAppearance_Marin_Keypad:I = 0x7f130250

.field public static final TextAppearance_Marin_Keypad_Home:I = 0x7f130251

.field public static final TextAppearance_Marin_Light:I = 0x7f130252

.field public static final TextAppearance_Marin_Medium:I = 0x7f130253

.field public static final TextAppearance_Marin_Medium_Blue:I = 0x7f130254

.field public static final TextAppearance_Marin_Medium_Centered:I = 0x7f130255

.field public static final TextAppearance_Marin_Medium_Centered_Selectable:I = 0x7f130256

.field public static final TextAppearance_Marin_Medium_Green:I = 0x7f130257

.field public static final TextAppearance_Marin_Medium_White:I = 0x7f130258

.field public static final TextAppearance_Marin_Message:I = 0x7f130259

.field public static final TextAppearance_Marin_ModalTitle:I = 0x7f13025a

.field public static final TextAppearance_Marin_ModalTitle_SingleLineByOrientation:I = 0x7f13025b

.field public static final TextAppearance_Marin_Note:I = 0x7f13025c

.field public static final TextAppearance_Marin_Placeholder:I = 0x7f13025d

.field public static final TextAppearance_Marin_SectionHeader:I = 0x7f13025f

.field public static final TextAppearance_Marin_SectionHeader_Light:I = 0x7f130260

.field public static final TextAppearance_Marin_SectionHeader_Medium:I = 0x7f130261

.field public static final TextAppearance_Marin_SignTitle:I = 0x7f130262

.field public static final TextAppearance_Marin_SubText:I = 0x7f130263

.field public static final TextAppearance_Marin_SubText_Centered:I = 0x7f130264

.field public static final TextAppearance_Marin_SubText_Centered_LightGray:I = 0x7f130265

.field public static final TextAppearance_Marin_SubText_Grey:I = 0x7f130266

.field public static final TextAppearance_Marin_SubText_Grey_Landing:I = 0x7f130267

.field public static final TextAppearance_Marin_SubText_Grey_Medium:I = 0x7f130268

.field public static final TextAppearance_Marin_SubText_Grey_Medium_Centered:I = 0x7f130269

.field public static final TextAppearance_Marin_SubText_NullState:I = 0x7f13026c

.field public static final TextAppearance_Marin_SubText_White:I = 0x7f13026d

.field public static final TextAppearance_Marin_Title1:I = 0x7f13026e

.field public static final TextAppearance_Marin_Title2:I = 0x7f13026f

.field public static final TextAppearance_Marin_White:I = 0x7f130275

.field public static final TextAppearance_Marin_White_Landing:I = 0x7f130276

.field public static final TextAppearance_Marin_White_LandingWorld:I = 0x7f130277

.field public static final Theme_Marin:I = 0x7f1302f3

.field public static final Theme_MarinBase:I = 0x7f130308

.field public static final Theme_Marin_ActionBar:I = 0x7f1302f4

.field public static final Theme_Marin_ActionBar_Blue:I = 0x7f1302f5

.field public static final Theme_Marin_ActionBar_BlueSecondaryButton:I = 0x7f1302f6

.field public static final Theme_Marin_ActionBar_Card:I = 0x7f1302f7

.field public static final Theme_Marin_ActionBar_Card_BlueSecondaryButton:I = 0x7f1302f8

.field public static final Theme_Marin_ActionBar_Green:I = 0x7f1302fa

.field public static final Theme_Marin_ActionBar_NoBackground:I = 0x7f1302fb

.field public static final Theme_Marin_ActionBar_NoBackground_WhiteText_DarkTranslucentSelector:I = 0x7f1302fc

.field public static final Theme_Marin_ActionBar_NoBottomBorder:I = 0x7f1302fd

.field public static final Theme_Marin_ActionBar_UltraLightGray:I = 0x7f1302fe

.field public static final Theme_Marin_ActionBar_UltraLightGray_BlueSecondaryButton:I = 0x7f1302ff

.field public static final Theme_Marin_ActionBar_UltraLightGray_NoBottomBorder:I = 0x7f130300

.field public static final Theme_Marin_ActionBar_White:I = 0x7f130301

.field public static final Theme_Marin_Inverted:I = 0x7f130302

.field public static final Theme_Marin_NoAnimation:I = 0x7f130303

.field public static final Theme_Marin_NoBackground:I = 0x7f130304

.field public static final Widget_Marin_ActionBar:I = 0x7f1303f5

.field public static final Widget_Marin_ActionBar_Green:I = 0x7f1303f6

.field public static final Widget_Marin_ActionBar_Navigation:I = 0x7f1303f7

.field public static final Widget_Marin_ActionBar_Navigation_Edit:I = 0x7f1303f8

.field public static final Widget_Marin_ActionBar_NoBackground:I = 0x7f1303f9

.field public static final Widget_Marin_ActionBar_NoBackground_WhiteText:I = 0x7f1303fa

.field public static final Widget_Marin_ActionBar_NoBorder:I = 0x7f1303fb

.field public static final Widget_Marin_ActionBar_UltraLightGray:I = 0x7f1303fc

.field public static final Widget_Marin_ActionBar_UltraLightGray_NoBorder:I = 0x7f1303fd

.field public static final Widget_Marin_ActionBar_White:I = 0x7f1303fe

.field public static final Widget_Marin_Applet_Header:I = 0x7f1303ff

.field public static final Widget_Marin_Applet_Header_List:I = 0x7f130400

.field public static final Widget_Marin_Applet_Header_Sidebar:I = 0x7f130401

.field public static final Widget_Marin_AutoPaddingTextView:I = 0x7f130402

.field public static final Widget_Marin_Button:I = 0x7f130403

.field public static final Widget_Marin_Button_ActionBar:I = 0x7f130404

.field public static final Widget_Marin_Button_ActionBar_BlueBackground:I = 0x7f130405

.field public static final Widget_Marin_Button_ActionBar_BlueText:I = 0x7f130406

.field public static final Widget_Marin_Button_ActionBar_BottomBorder:I = 0x7f130407

.field public static final Widget_Marin_Button_ActionBar_BottomBorder_BlueText:I = 0x7f130408

.field public static final Widget_Marin_Button_ActionBar_BottomBorder_UpButton:I = 0x7f130409

.field public static final Widget_Marin_Button_ActionBar_BottomBorder_UpButton_BlueText:I = 0x7f13040a

.field public static final Widget_Marin_Button_ActionBar_BottomBorder_UpButton_WhiteText:I = 0x7f13040b

.field public static final Widget_Marin_Button_ActionBar_BottomBorder_WhiteText:I = 0x7f13040c

.field public static final Widget_Marin_Button_ActionBar_BottomBorder_WhiteText_GreenBackground:I = 0x7f13040d

.field public static final Widget_Marin_Button_ActionBar_BottomBorder_WhiteText_GreenBackground_UpButton:I = 0x7f13040e

.field public static final Widget_Marin_Button_ActionBar_Navigation:I = 0x7f13040f

.field public static final Widget_Marin_Button_ActionBar_Navigation_Burger:I = 0x7f130410

.field public static final Widget_Marin_Button_ActionBar_Navigation_Burger_Wide:I = 0x7f130411

.field public static final Widget_Marin_Button_ActionBar_Navigation_Edit:I = 0x7f130412

.field public static final Widget_Marin_Button_ActionBar_NoBorder:I = 0x7f130413

.field public static final Widget_Marin_Button_ActionBar_UltraLightGrayBackground:I = 0x7f130414

.field public static final Widget_Marin_Button_ActionBar_UltraLightGrayBackground_BlueText:I = 0x7f130415

.field public static final Widget_Marin_Button_ActionBar_UltraLightGrayBackground_NoBorder:I = 0x7f130416

.field public static final Widget_Marin_Button_ActionBar_UpButton:I = 0x7f130417

.field public static final Widget_Marin_Button_ActionBar_UpButton_BlueText:I = 0x7f130418

.field public static final Widget_Marin_Button_ActionBar_UpButton_UltraLightGrayBackground:I = 0x7f130419

.field public static final Widget_Marin_Button_ActionBar_UpButton_UltraLightGrayBackground_NoBorder:I = 0x7f13041a

.field public static final Widget_Marin_Button_ActionBar_UpButton_WhiteText:I = 0x7f13041b

.field public static final Widget_Marin_Button_ActionBar_WhiteText:I = 0x7f13041c

.field public static final Widget_Marin_Button_ActionBar_WhiteText_BlackTransparentFity:I = 0x7f13041d

.field public static final Widget_Marin_Button_BlackTransparentFifty:I = 0x7f13041e

.field public static final Widget_Marin_Button_BlueBackground:I = 0x7f13041f

.field public static final Widget_Marin_Button_BlueBackground_Inline:I = 0x7f130420

.field public static final Widget_Marin_Button_BlueText:I = 0x7f130421

.field public static final Widget_Marin_Button_DarkTranslucent:I = 0x7f130422

.field public static final Widget_Marin_Button_Dialog:I = 0x7f130423

.field public static final Widget_Marin_Button_EditText:I = 0x7f130425

.field public static final Widget_Marin_Button_InvertedGrayOnDisabled:I = 0x7f130426

.field public static final Widget_Marin_Button_Link:I = 0x7f130427

.field public static final Widget_Marin_Button_MediumGrayBackground:I = 0x7f130428

.field public static final Widget_Marin_Button_NoBackground:I = 0x7f130429

.field public static final Widget_Marin_Button_Primary:I = 0x7f13042a

.field public static final Widget_Marin_Button_Primary_Blue:I = 0x7f13042b

.field public static final Widget_Marin_Button_Primary_UltraLightGray:I = 0x7f13042c

.field public static final Widget_Marin_Button_RedBackground:I = 0x7f13042d

.field public static final Widget_Marin_Button_Tab:I = 0x7f13042e

.field public static final Widget_Marin_Button_Tab_ThreeColumn:I = 0x7f13042f

.field public static final Widget_Marin_Button_Tab_WithDrawableAtop:I = 0x7f130430

.field public static final Widget_Marin_Button_WeightRegular:I = 0x7f130433

.field public static final Widget_Marin_Button_White:I = 0x7f130434

.field public static final Widget_Marin_Button_White_BlueText:I = 0x7f130435

.field public static final Widget_Marin_Button_White_WeightRegular:I = 0x7f130436

.field public static final Widget_Marin_Charge:I = 0x7f130437

.field public static final Widget_Marin_Charge_Subtitle:I = 0x7f130438

.field public static final Widget_Marin_Charge_Title:I = 0x7f130439

.field public static final Widget_Marin_Charge_Title_NoTickets:I = 0x7f13043a

.field public static final Widget_Marin_CheckableGroup:I = 0x7f13043b

.field public static final Widget_Marin_CheckableGroup_Margin_GutterHalf:I = 0x7f13043c

.field public static final Widget_Marin_CheckableGroup_ThickBlankDivider:I = 0x7f13043d

.field public static final Widget_Marin_CheckableGroup_TwoColumnsThickDivider:I = 0x7f13043e

.field public static final Widget_Marin_CheckableTextView:I = 0x7f13043f

.field public static final Widget_Marin_DatePicker:I = 0x7f130441

.field public static final Widget_Marin_Divider:I = 0x7f130444

.field public static final Widget_Marin_Divider_NoMargins:I = 0x7f130445

.field public static final Widget_Marin_Drill:I = 0x7f130447

.field public static final Widget_Marin_DropDownMenu:I = 0x7f130448

.field public static final Widget_Marin_DropDownMenu_Container:I = 0x7f130449

.field public static final Widget_Marin_DropDownMenu_Container_Cart:I = 0x7f13044a

.field public static final Widget_Marin_DropDownMenu_Item:I = 0x7f13044b

.field public static final Widget_Marin_DropDownMenu_Item_Cart:I = 0x7f13044c

.field public static final Widget_Marin_DropDownMenu_Item_SplitTicket:I = 0x7f13044d

.field public static final Widget_Marin_EditText:I = 0x7f13044e

.field public static final Widget_Marin_EditText_TextBoxBorder:I = 0x7f13044f

.field public static final Widget_Marin_EditText_WhiteText:I = 0x7f130450

.field public static final Widget_Marin_Element:I = 0x7f130455

.field public static final Widget_Marin_Element_Clickable:I = 0x7f130456

.field public static final Widget_Marin_Element_HelpLine:I = 0x7f130457

.field public static final Widget_Marin_Element_HelpLine_Clickable:I = 0x7f130458

.field public static final Widget_Marin_Element_HelpLine_Clickable_Link:I = 0x7f130459

.field public static final Widget_Marin_Element_HelpText:I = 0x7f13045a

.field public static final Widget_Marin_Element_HelpText_BottomItem:I = 0x7f13045b

.field public static final Widget_Marin_Element_HelpText_Margin_GutterFull:I = 0x7f13045c

.field public static final Widget_Marin_Element_HelpText_Margin_GutterFull_BottomItem:I = 0x7f13045d

.field public static final Widget_Marin_Element_HelpText_Margin_GutterFull_NoBottomMargin:I = 0x7f13045e

.field public static final Widget_Marin_Element_HelpText_Margin_GutterFull_NoMargin:I = 0x7f13045f

.field public static final Widget_Marin_Element_HelpText_Margin_GutterHalf:I = 0x7f130460

.field public static final Widget_Marin_Element_HelpText_Margin_GutterHalf_BottomItem:I = 0x7f130461

.field public static final Widget_Marin_Element_HelpText_Margin_GutterHalf_NoBottomMargin:I = 0x7f130462

.field public static final Widget_Marin_Element_HelpText_MiniBottomMargin:I = 0x7f130463

.field public static final Widget_Marin_Element_HelpText_NoBottomMargin:I = 0x7f130464

.field public static final Widget_Marin_Element_HelpText_NoBottomMargin_MiniTopMargin:I = 0x7f130465

.field public static final Widget_Marin_Element_HelpText_NoMargin:I = 0x7f130466

.field public static final Widget_Marin_Element_HelpText_Padded_GutterHalf:I = 0x7f130467

.field public static final Widget_Marin_Element_HelpText_Padded_GutterHalf_BottomItem:I = 0x7f130468

.field public static final Widget_Marin_Element_Margin_GutterFull:I = 0x7f130469

.field public static final Widget_Marin_Element_Margin_GutterFull_BottomItem:I = 0x7f13046a

.field public static final Widget_Marin_Element_Margin_GutterFull_Clickable:I = 0x7f13046b

.field public static final Widget_Marin_Element_Margin_GutterHalf:I = 0x7f13046c

.field public static final Widget_Marin_Element_Margin_GutterHalf_BottomButton:I = 0x7f13046d

.field public static final Widget_Marin_Element_Margin_GutterHalf_Clickable:I = 0x7f13046e

.field public static final Widget_Marin_Element_Margin_GutterHalf_EditText:I = 0x7f13046f

.field public static final Widget_Marin_Element_Message:I = 0x7f130470

.field public static final Widget_Marin_Element_Message_GutterFull:I = 0x7f130471

.field public static final Widget_Marin_Element_Message_GutterFull_MultilinePadded:I = 0x7f130472

.field public static final Widget_Marin_Element_Padded_GutterFull:I = 0x7f130473

.field public static final Widget_Marin_Element_Padded_GutterFull_Clickable:I = 0x7f130474

.field public static final Widget_Marin_Element_Padded_GutterHalf:I = 0x7f130475

.field public static final Widget_Marin_Element_Padded_GutterHalf_Clickable:I = 0x7f130476

.field public static final Widget_Marin_Element_Padded_GutterHalf_Clickable_Lollipop:I = 0x7f130477

.field public static final Widget_Marin_Element_Padded_GutterHalf_Clickable_UltraLightGray:I = 0x7f130478

.field public static final Widget_Marin_Element_Padded_GutterHalf_Margin_GutterHalf:I = 0x7f130479

.field public static final Widget_Marin_Element_Padded_GutterHalf_Margin_GutterHalf_Clickable:I = 0x7f13047a

.field public static final Widget_Marin_Element_Padded_GutterHalf_Margin_GutterHalf_Clickable_Lollipop:I = 0x7f13047b

.field public static final Widget_Marin_Element_Padded_Medium_Framed_Selectable:I = 0x7f13047c

.field public static final Widget_Marin_Element_Padded_Multiline:I = 0x7f13047d

.field public static final Widget_Marin_Element_Padded_Multiline_Margin_GutterFull:I = 0x7f13047e

.field public static final Widget_Marin_Element_Padded_Multiline_Margin_GutterHalf:I = 0x7f13047f

.field public static final Widget_Marin_Element_WarningText:I = 0x7f130480

.field public static final Widget_Marin_GlyphTextView:I = 0x7f130481

.field public static final Widget_Marin_GlyphView:I = 0x7f130482

.field public static final Widget_Marin_GlyphView_Button:I = 0x7f130483

.field public static final Widget_Marin_GlyphView_Button_ActionBar:I = 0x7f130484

.field public static final Widget_Marin_GlyphView_Button_ActionBar_Green:I = 0x7f130485

.field public static final Widget_Marin_GlyphView_Button_ActionBar_UltraLightGrayBackground:I = 0x7f130486

.field public static final Widget_Marin_GlyphView_Button_Inline:I = 0x7f130487

.field public static final Widget_Marin_GlyphView_Button_Inline_BlueBackground:I = 0x7f130488

.field public static final Widget_Marin_GlyphView_Button_Inset_Blue:I = 0x7f130489

.field public static final Widget_Marin_GlyphView_Button_RedGlyph:I = 0x7f13048a

.field public static final Widget_Marin_GlyphView_DarkGrayText:I = 0x7f13048b

.field public static final Widget_Marin_GlyphView_VisibleByOrientation:I = 0x7f13048c

.field public static final Widget_Marin_ListItem:I = 0x7f13048e

.field public static final Widget_Marin_ListItem_Applet_List:I = 0x7f13048f

.field public static final Widget_Marin_ListItem_Applet_List_GutterFull:I = 0x7f130490

.field public static final Widget_Marin_ListItem_Applet_Sidebar:I = 0x7f130491

.field public static final Widget_Marin_ListItem_Applet_Sidebar_BlueText:I = 0x7f130492

.field public static final Widget_Marin_ListItem_Checked:I = 0x7f130495

.field public static final Widget_Marin_ListItem_Checked_GutterFull:I = 0x7f130496

.field public static final Widget_Marin_ListItem_Checked_Multiple:I = 0x7f130497

.field public static final Widget_Marin_ListItem_Checked_UltraLightGray:I = 0x7f130499

.field public static final Widget_Marin_ListItem_HelpText:I = 0x7f13049b

.field public static final Widget_Marin_ListItem_HelpText_BottomItem_Clickable:I = 0x7f13049c

.field public static final Widget_Marin_ListItem_HelpText_Clickable:I = 0x7f13049d

.field public static final Widget_Marin_ListItem_HelpText_Padded:I = 0x7f13049e

.field public static final Widget_Marin_ListItem_Margin_GutterHalf:I = 0x7f13049f

.field public static final Widget_Marin_ListItem_MediumWeight:I = 0x7f1304a0

.field public static final Widget_Marin_ListItem_NoLeftPadding:I = 0x7f1304a1

.field public static final Widget_Marin_ListItem_NoPadding:I = 0x7f1304a2

.field public static final Widget_Marin_ListItem_Padding_GutterFull:I = 0x7f1304a3

.field public static final Widget_Marin_ListItem_Thumbnail:I = 0x7f1304a5

.field public static final Widget_Marin_ListView:I = 0x7f1304a6

.field public static final Widget_Marin_ListViewBase:I = 0x7f1304b0

.field public static final Widget_Marin_ListView_GutterFull:I = 0x7f1304a7

.field public static final Widget_Marin_ListView_GutterFull_RightGutterHalf:I = 0x7f1304a8

.field public static final Widget_Marin_ListView_GutterHalf:I = 0x7f1304a9

.field public static final Widget_Marin_ListView_GutterHalf_NoTopPadding:I = 0x7f1304aa

.field public static final Widget_Marin_ListView_NoPadding:I = 0x7f1304ab

.field public static final Widget_Marin_ListView_NoPadding_NoTopPadding:I = 0x7f1304ac

.field public static final Widget_Marin_ListView_NoPadding_SelectorOnTop:I = 0x7f1304ad

.field public static final Widget_Marin_OrderTicketBar:I = 0x7f1304b1

.field public static final Widget_Marin_PreservedLayoutView:I = 0x7f1304b2

.field public static final Widget_Marin_ProgressBar:I = 0x7f1304b3

.field public static final Widget_Marin_ProgressBar_Dark:I = 0x7f1304b4

.field public static final Widget_Marin_ProgressBar_Large:I = 0x7f1304b6

.field public static final Widget_Marin_ProgressBar_Large_Dark:I = 0x7f1304b7

.field public static final Widget_Marin_ProgressBar_Large_Light:I = 0x7f1304b8

.field public static final Widget_Marin_ProgressBar_Small:I = 0x7f1304b9

.field public static final Widget_Marin_ProgressBar_Small_Dark:I = 0x7f1304ba

.field public static final Widget_Marin_ProgressBar_Small_Light:I = 0x7f1304bb

.field public static final Widget_Marin_ProgressBar_Tiny:I = 0x7f1304bc

.field public static final Widget_Marin_RadioButton:I = 0x7f1304bd

.field public static final Widget_Marin_ScrollView:I = 0x7f1304be

.field public static final Widget_Marin_ScrollView_GutterFull:I = 0x7f1304bf

.field public static final Widget_Marin_ScrollView_GutterHalf:I = 0x7f1304c0

.field public static final Widget_Marin_Spinner:I = 0x7f1304c2

.field public static final Widget_Marin_Spinner_DropDown:I = 0x7f1304c3

.field public static final Widget_Marin_Switch:I = 0x7f1304c4

.field public static final Widget_Marin_TextView:I = 0x7f1304c5

.field public static final Widget_Marin_TextView_ActionBar_Navigation:I = 0x7f1304c6

.field public static final Widget_Marin_TextView_DropDownItem:I = 0x7f1304ca

.field public static final Widget_Marin_TextView_SectionHeader:I = 0x7f1304cb

.field public static final Widget_Marin_TextView_SectionHeader_GutterFull:I = 0x7f1304cc

.field public static final Widget_Marin_TextView_SectionHeader_GutterFull_LargerTopPadding:I = 0x7f1304cd

.field public static final Widget_Marin_TextView_SectionHeader_GutterFull_NoTopPadding:I = 0x7f1304ce

.field public static final Widget_Marin_TextView_SectionHeader_GutterHalf:I = 0x7f1304cf

.field public static final Widget_Marin_TextView_SectionHeader_GutterHalf_Gradient:I = 0x7f1304d0

.field public static final Widget_Marin_TextView_SectionHeader_GutterHalf_LargerTopPadding:I = 0x7f1304d1

.field public static final Widget_Marin_TextView_SectionHeader_GutterHalf_NoTopPadding:I = 0x7f1304d2

.field public static final Widget_Marin_TextView_SectionHeader_LargerTopPadding:I = 0x7f1304d5

.field public static final Widget_Marin_TextView_SectionHeader_MediumGray:I = 0x7f1304d6

.field public static final Widget_Marin_TextView_SectionHeader_NoTopPadding:I = 0x7f1304d7

.field public static final Widget_Marin_TextView_Spinner:I = 0x7f1304d9

.field public static final Widget_Marin_TextView_Spinner_Margin_GutterHalf:I = 0x7f1304da

.field public static final Widget_Marin_TextView_TextBoxBorder:I = 0x7f1304db

.field public static final Widget_Marin_TextView_Tooltip:I = 0x7f1304dc

.field public static final Widget_Marin_TokenView:I = 0x7f1304df


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
