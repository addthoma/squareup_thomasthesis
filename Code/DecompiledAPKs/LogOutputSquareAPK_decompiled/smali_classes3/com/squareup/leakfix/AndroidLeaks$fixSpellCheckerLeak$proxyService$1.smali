.class final Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;
.super Ljava/lang/Object;
.source "AndroidLeaks.kt"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/leakfix/AndroidLeaks;->fixSpellCheckerLeak()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u00052\u000e\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u0007H\n\u00a2\u0006\u0004\u0008\u0008\u0010\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 0>",
        "method",
        "Ljava/lang/reflect/Method;",
        "args",
        "",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $listenerImplHandlerField:Ljava/lang/reflect/Field;

.field final synthetic $mSpellCheckerSessionListenerField:Ljava/lang/reflect/Field;

.field final synthetic $noOpListener:Ljava/lang/Object;

.field final synthetic $outerInstanceField:Ljava/lang/reflect/Field;

.field final synthetic $realService:Ljava/lang/Object;

.field final synthetic $spellCheckerListenerToSession:Ljava/util/Map;


# direct methods
.method constructor <init>(Ljava/lang/reflect/Field;Ljava/lang/reflect/Field;Ljava/util/Map;Ljava/lang/Object;Ljava/lang/reflect/Field;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;->$listenerImplHandlerField:Ljava/lang/reflect/Field;

    iput-object p2, p0, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;->$outerInstanceField:Ljava/lang/reflect/Field;

    iput-object p3, p0, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;->$spellCheckerListenerToSession:Ljava/util/Map;

    iput-object p4, p0, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;->$noOpListener:Ljava/lang/Object;

    iput-object p5, p0, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;->$mSpellCheckerSessionListenerField:Ljava/lang/reflect/Field;

    iput-object p6, p0, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;->$realService:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "method"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 389
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getSpellCheckerService"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez p3, :cond_0

    .line 393
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const/4 v0, 0x3

    aget-object v0, p3, v0

    .line 394
    iget-object v1, p0, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;->$listenerImplHandlerField:Ljava/lang/reflect/Field;

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 395
    :cond_1
    iget-object v2, p0, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;->$outerInstanceField:Ljava/lang/reflect/Field;

    invoke-virtual {v2, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 397
    :cond_2
    iget-object v2, p0, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;->$spellCheckerListenerToSession:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 398
    :cond_3
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "finishSpellCheckerService"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    if-nez p3, :cond_4

    .line 402
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    aget-object v0, p3, p1

    .line 404
    iget-object v1, p0, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;->$spellCheckerListenerToSession:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 409
    :cond_5
    iget-object v1, p0, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;->$mSpellCheckerSessionListenerField:Ljava/lang/reflect/Field;

    iget-object v2, p0, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;->$noOpListener:Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 412
    check-cast v0, Ljava/lang/Throwable;

    new-array v1, p1, [Ljava/lang/Object;

    const-string v2, "Unable to fix SpellChecker leak"

    invoke-static {v0, v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_6
    :goto_0
    if-eqz p3, :cond_7

    .line 417
    :try_start_1
    iget-object p1, p0, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;->$realService:Ljava/lang/Object;

    array-length v0, p3

    invoke-static {p3, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p2, p1, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_1

    .line 419
    :cond_7
    iget-object p3, p0, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;->$realService:Ljava/lang/Object;

    new-array p1, p1, [Ljava/lang/Object;

    invoke-virtual {p2, p3, p1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return-object p1

    :catch_1
    move-exception p1

    .line 422
    invoke-virtual {p1}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object p1

    const-string p2, "invocationException.targetException"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    throw p1
.end method
