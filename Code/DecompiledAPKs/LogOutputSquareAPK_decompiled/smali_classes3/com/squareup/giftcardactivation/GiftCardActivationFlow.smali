.class public interface abstract Lcom/squareup/giftcardactivation/GiftCardActivationFlow;
.super Ljava/lang/Object;
.source "GiftCardActivationFlow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u0000 \u00082\u00020\u0001:\u0001\u0008J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0018\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0003H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/giftcardactivation/GiftCardActivationFlow;",
        "",
        "activationScreen",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "checkBalanceFor",
        "giftCard",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "parentKey",
        "NOT_SUPPORTED",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final NOT_SUPPORTED:Lcom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED;->$$INSTANCE:Lcom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED;

    sput-object v0, Lcom/squareup/giftcardactivation/GiftCardActivationFlow;->NOT_SUPPORTED:Lcom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED;

    return-void
.end method


# virtual methods
.method public abstract activationScreen()Lcom/squareup/ui/main/RegisterTreeKey;
.end method

.method public abstract checkBalanceFor(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/ui/main/RegisterTreeKey;)Lcom/squareup/ui/main/RegisterTreeKey;
.end method
