.class final Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$takeSnapshot$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ChooseDateState.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;->takeSnapshot()Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;


# direct methods
.method constructor <init>(Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$takeSnapshot$1;->this$0:Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$takeSnapshot$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$takeSnapshot$1;->this$0:Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "this::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 39
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$takeSnapshot$1;->this$0:Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    .line 40
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;->getData()Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateStateKt;->access$writeChooseDateScreenData(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;)V

    goto :goto_0

    .line 41
    :cond_0
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingCustomDate;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingCustomDate;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingCustomDate;->getData()Lcom/squareup/invoices/workflow/edit/CustomDateInfo;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateStateKt;->access$writeCustomDateScreenData(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/CustomDateInfo;)V

    :cond_1
    :goto_0
    return-void
.end method
