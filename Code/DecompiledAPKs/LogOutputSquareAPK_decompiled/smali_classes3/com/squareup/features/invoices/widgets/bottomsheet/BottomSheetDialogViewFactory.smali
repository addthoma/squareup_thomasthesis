.class public interface abstract Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;
.super Ljava/lang/Object;
.source "BottomSheetDialogViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J,\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;",
        "",
        "create",
        "",
        "Landroid/view/View;",
        "elements",
        "Lcom/squareup/features/invoices/widgets/SectionElement;",
        "parent",
        "Landroid/view/ViewGroup;",
        "eventHandler",
        "Lcom/squareup/features/invoices/widgets/EventHandler;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract create(Ljava/util/List;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/EventHandler;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/features/invoices/widgets/EventHandler;",
            ")",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end method
