.class public final Lcom/squareup/features/invoices/widgets/V2RendererSelector_Factory;
.super Ljava/lang/Object;
.source "V2RendererSelector_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/features/invoices/widgets/V2RendererSelector;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final v1SectionRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final v2SectionRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/V2RendererSelector_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/V2RendererSelector_Factory;->v1SectionRendererProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/V2RendererSelector_Factory;->v2SectionRendererProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/features/invoices/widgets/V2RendererSelector_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;",
            ">;)",
            "Lcom/squareup/features/invoices/widgets/V2RendererSelector_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/features/invoices/widgets/V2RendererSelector_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/features/invoices/widgets/V2RendererSelector_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;)Lcom/squareup/features/invoices/widgets/V2RendererSelector;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/features/invoices/widgets/V2RendererSelector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/features/invoices/widgets/V2RendererSelector;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/features/invoices/widgets/V2RendererSelector;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/V2RendererSelector_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/V2RendererSelector_Factory;->v1SectionRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/V2RendererSelector_Factory;->v2SectionRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;

    invoke-static {v0, v1, v2}, Lcom/squareup/features/invoices/widgets/V2RendererSelector_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;)Lcom/squareup/features/invoices/widgets/V2RendererSelector;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/V2RendererSelector_Factory;->get()Lcom/squareup/features/invoices/widgets/V2RendererSelector;

    move-result-object v0

    return-object v0
.end method
