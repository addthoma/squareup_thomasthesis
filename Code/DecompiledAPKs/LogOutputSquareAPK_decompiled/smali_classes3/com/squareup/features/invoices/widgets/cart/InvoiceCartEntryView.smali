.class public final Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;
.super Lcom/squareup/ui/cart/CartEntryView;
.source "InvoiceCartEntryView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J0\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u00082\u0006\u0010\u000e\u001a\u00020\u00082\u0006\u0010\u000f\u001a\u00020\u00082\u0006\u0010\u0010\u001a\u00020\u0008H\u0014J\u0018\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u00082\u0006\u0010\u0013\u001a\u00020\u0008H\u0014R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;",
        "Lcom/squareup/ui/cart/CartEntryView;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "imageSize",
        "",
        "onLayout",
        "",
        "changed",
        "",
        "l",
        "t",
        "r",
        "b",
        "onMeasure",
        "widthMeasureSpec",
        "heightMeasureSpec",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final imageSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/cart/CartEntryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/features/invoices/widgets/R$dimen;->v2_row_image_size:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->imageSize:I

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 1

    .line 79
    invoke-super/range {p0 .. p5}, Lcom/squareup/ui/cart/CartEntryView;->onLayout(ZIIII)V

    .line 82
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getSubLabelField()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/TextView;->getVisibility()I

    move-result p1

    const/16 p2, 0x8

    if-eq p1, p2, :cond_0

    .line 85
    iget p1, p0, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->imageSize:I

    div-int/lit8 p1, p1, 0x2

    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getAmountField()Lcom/squareup/marin/widgets/MarinGlyphTextView;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getMeasuredHeight()I

    move-result p2

    div-int/lit8 p2, p2, 0x2

    sub-int/2addr p1, p2

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 90
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getAmountField()Lcom/squareup/marin/widgets/MarinGlyphTextView;

    move-result-object p2

    .line 91
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getMeasuredWidth()I

    move-result p3

    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getPaddingRight()I

    move-result p4

    sub-int/2addr p3, p4

    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getAmountField()Lcom/squareup/marin/widgets/MarinGlyphTextView;

    move-result-object p4

    invoke-virtual {p4}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getMeasuredWidth()I

    move-result p4

    sub-int/2addr p3, p4

    .line 92
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getNameAndQuantityField()Lcom/squareup/widgets/PreservedLabelView;

    move-result-object p4

    invoke-virtual {p4}, Lcom/squareup/widgets/PreservedLabelView;->getTop()I

    move-result p4

    add-int/2addr p4, p1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getMeasuredWidth()I

    move-result p5

    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getPaddingRight()I

    move-result v0

    sub-int/2addr p5, v0

    .line 94
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getNameAndQuantityField()Lcom/squareup/widgets/PreservedLabelView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/widgets/PreservedLabelView;->getBottom()I

    move-result v0

    add-int/2addr v0, p1

    .line 90
    invoke-virtual {p2, p3, p4, p5, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .line 39
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/cart/CartEntryView;->onMeasure(II)V

    .line 41
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    .line 42
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getPaddingLeft()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 46
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getAmountField()Lcom/squareup/marin/widgets/MarinGlyphTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getMarinGapMedium()I

    move-result v1

    sub-int/2addr v0, v1

    const/high16 v1, -0x80000000

    .line 45
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 49
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getSubLabelField()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getSubLabelField()Landroid/widget/TextView;

    move-result-object v1

    const/4 v3, 0x0

    .line 55
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getSubLabelField()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 53
    invoke-static {p2, v3, v4}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result p2

    .line 51
    invoke-virtual {v1, v0, p2}, Landroid/widget/TextView;->measure(II)V

    .line 60
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getNameAndQuantityField()Lcom/squareup/widgets/PreservedLabelView;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/widgets/PreservedLabelView;->getMeasuredHeight()I

    move-result p2

    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getSubLabelField()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr p2, v0

    .line 62
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->getSubLabelField()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_1

    goto :goto_0

    .line 66
    :cond_1
    iget p2, p0, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->imageSize:I

    .line 69
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/features/invoices/widgets/cart/InvoiceCartEntryView;->setMeasuredDimension(II)V

    return-void
.end method
