.class public final Lcom/squareup/features/invoices/widgets/packages/ExpandableView;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "ExpandableView.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nExpandableView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ExpandableView.kt\ncom/squareup/features/invoices/widgets/packages/ExpandableView\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,123:1\n1103#2,7:124\n*E\n*S KotlinDebug\n*F\n+ 1 ExpandableView.kt\ncom/squareup/features/invoices/widgets/packages/ExpandableView\n*L\n96#1,7:124\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010$\u001a\u00020%2\u0006\u0010\u001d\u001a\u00020\u001cH\u0002J\u0006\u0010&\u001a\u00020%R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\t\u001a\u00020\u00082\u0006\u0010\u0007\u001a\u00020\u0008@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR(\u0010\u000e\u001a\u0004\u0018\u00010\u00082\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u000b\"\u0004\u0008\u0010\u0010\rR\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0013\u001a\u00020\u00082\u0006\u0010\u0007\u001a\u00020\u0008@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0014\u0010\u000b\"\u0004\u0008\u0015\u0010\rR(\u0010\u0017\u001a\u0004\u0018\u00010\u00162\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0016@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\"\u0004\u0008\u001a\u0010\u001bR$\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u0007\u001a\u00020\u001c@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001d\u0010\u001e\"\u0004\u0008\u001f\u0010 R\u000e\u0010!\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/packages/ExpandableView;",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "bodyContainer",
        "Landroid/view/ViewGroup;",
        "value",
        "",
        "collapsedTitle",
        "getCollapsedTitle",
        "()Ljava/lang/CharSequence;",
        "setCollapsedTitle",
        "(Ljava/lang/CharSequence;)V",
        "description",
        "getDescription",
        "setDescription",
        "descriptionView",
        "Lcom/squareup/noho/NohoLabel;",
        "expandedTitle",
        "getExpandedTitle",
        "setExpandedTitle",
        "Landroid/view/View;",
        "expandedView",
        "getExpandedView",
        "()Landroid/view/View;",
        "setExpandedView",
        "(Landroid/view/View;)V",
        "",
        "isCollapsed",
        "()Z",
        "setCollapsed",
        "(Z)V",
        "titleView",
        "toggleButton",
        "Landroid/widget/ImageView;",
        "onCollapsedStateChanged",
        "",
        "toggle",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bodyContainer:Landroid/view/ViewGroup;

.field private collapsedTitle:Ljava/lang/CharSequence;

.field private description:Ljava/lang/CharSequence;

.field private final descriptionView:Lcom/squareup/noho/NohoLabel;

.field private expandedTitle:Ljava/lang/CharSequence;

.field private expandedView:Landroid/view/View;

.field private isCollapsed:Z

.field private final titleView:Lcom/squareup/noho/NohoLabel;

.field private final toggleButton:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0, p1}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;)V

    const-string v0, ""

    .line 49
    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->collapsedTitle:Ljava/lang/CharSequence;

    .line 54
    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->expandedTitle:Ljava/lang/CharSequence;

    .line 59
    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->description:Ljava/lang/CharSequence;

    const/4 v0, 0x1

    .line 76
    iput-boolean v0, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->isCollapsed:Z

    .line 83
    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$layout;->expandable_view:I

    move-object v2, p0

    check-cast v2, Landroid/view/ViewGroup;

    invoke-static {p1, v1, v2}, Landroidx/constraintlayout/widget/ConstraintLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$drawable;->expandable_view_background:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 87
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/squareup/noho/R$dimen;->noho_gap_20:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 88
    new-instance v1, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    check-cast v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, v1}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 89
    invoke-virtual {p0, p1, p1, p1, p1}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->setPadding(IIII)V

    .line 91
    sget p1, Lcom/squareup/features/invoices/widgets/impl/R$id;->title:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->titleView:Lcom/squareup/noho/NohoLabel;

    .line 92
    sget p1, Lcom/squareup/features/invoices/widgets/impl/R$id;->description:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->descriptionView:Lcom/squareup/noho/NohoLabel;

    .line 93
    sget p1, Lcom/squareup/features/invoices/widgets/impl/R$id;->toggle:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->toggleButton:Landroid/widget/ImageView;

    .line 94
    sget p1, Lcom/squareup/features/invoices/widgets/impl/R$id;->content_cont:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->bodyContainer:Landroid/view/ViewGroup;

    .line 96
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->toggleButton:Landroid/widget/ImageView;

    check-cast p1, Landroid/view/View;

    .line 124
    new-instance v1, Lcom/squareup/features/invoices/widgets/packages/ExpandableView$$special$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView$$special$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/features/invoices/widgets/packages/ExpandableView;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    new-instance p1, Landroid/animation/LayoutTransition;

    invoke-direct {p1}, Landroid/animation/LayoutTransition;-><init>()V

    const/4 v1, 0x4

    .line 99
    invoke-virtual {p1, v1}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 98
    invoke-virtual {p0, p1}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 102
    invoke-direct {p0, v0}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->onCollapsedStateChanged(Z)V

    return-void
.end method

.method private final onCollapsedStateChanged(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 111
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->titleView:Lcom/squareup/noho/NohoLabel;

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->collapsedTitle:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->descriptionView:Lcom/squareup/noho/NohoLabel;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 113
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->bodyContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 114
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->toggleButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$drawable;->expandable_view_expand:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 116
    :cond_0
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->titleView:Lcom/squareup/noho/NohoLabel;

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->expandedTitle:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->descriptionView:Lcom/squareup/noho/NohoLabel;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 118
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->bodyContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 119
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->toggleButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$drawable;->expandable_view_collapse:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public final getCollapsedTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->collapsedTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getDescription()Ljava/lang/CharSequence;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->description:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getExpandedTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->expandedTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getExpandedView()Landroid/view/View;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->expandedView:Landroid/view/View;

    return-object v0
.end method

.method public final isCollapsed()Z
    .locals 1

    .line 76
    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->isCollapsed:Z

    return v0
.end method

.method public final setCollapsed(Z)V
    .locals 0

    .line 78
    iput-boolean p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->isCollapsed:Z

    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->onCollapsedStateChanged(Z)V

    return-void
.end method

.method public final setCollapsedTitle(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->collapsedTitle:Ljava/lang/CharSequence;

    .line 52
    iget-boolean p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->isCollapsed:Z

    invoke-direct {p0, p1}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->onCollapsedStateChanged(Z)V

    return-void
.end method

.method public final setDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .line 61
    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->description:Ljava/lang/CharSequence;

    .line 62
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->descriptionView:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iget-boolean p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->isCollapsed:Z

    invoke-direct {p0, p1}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->onCollapsedStateChanged(Z)V

    return-void
.end method

.method public final setExpandedTitle(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->expandedTitle:Ljava/lang/CharSequence;

    .line 57
    iget-boolean p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->isCollapsed:Z

    invoke-direct {p0, p1}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->onCollapsedStateChanged(Z)V

    return-void
.end method

.method public final setExpandedView(Landroid/view/View;)V
    .locals 3

    .line 67
    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->expandedView:Landroid/view/View;

    .line 68
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->bodyContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 69
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->bodyContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->removeViews(II)V

    :cond_0
    if-eqz p1, :cond_1

    .line 73
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->bodyContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method public final toggle()V
    .locals 1

    .line 106
    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->isCollapsed:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->setCollapsed(Z)V

    return-void
.end method
