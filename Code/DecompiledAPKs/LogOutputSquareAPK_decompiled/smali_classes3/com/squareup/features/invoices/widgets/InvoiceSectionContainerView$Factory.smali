.class public interface abstract Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView$Factory;
.super Ljava/lang/Object;
.source "InvoiceSectionContainerView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0008f\u0018\u00002\u00020\u0001J(\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH&\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView$Factory;",
        "",
        "create",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView;",
        "invoiceSectionData",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
        "context",
        "Landroid/content/Context;",
        "eventHandler",
        "Lcom/squareup/features/invoices/widgets/EventHandler;",
        "useV2Widgets",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract create(Lcom/squareup/features/invoices/widgets/InvoiceSectionData;Landroid/content/Context;Lcom/squareup/features/invoices/widgets/EventHandler;Z)Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView;
.end method
