.class public final Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;
.super Ljava/lang/Object;
.source "InvoiceV1SectionRenderer.kt"

# interfaces
.implements Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceV1SectionRenderer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceV1SectionRenderer.kt\ncom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n+ 4 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,521:1\n1103#2,7:522\n1103#2,7:531\n1103#2,7:538\n1103#2,7:545\n1103#2,7:554\n1103#2,7:567\n17#3,2:529\n1651#4,2:552\n1653#4:561\n1642#4,2:562\n1651#4,3:564\n1642#4,2:574\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceV1SectionRenderer.kt\ncom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer\n*L\n85#1,7:522\n125#1,7:531\n193#1,7:538\n220#1,7:545\n348#1,7:554\n462#1,7:567\n125#1,2:529\n348#1,2:552\n348#1:561\n392#1,2:562\n407#1,3:564\n493#1,2:574\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00ec\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\tJ0\u0010\n\u001a\u00020\u000b*\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u0012H\u0002J\u0016\u0010\u0014\u001a\u00020\u000b*\u00020\u000c2\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0002J\u001c\u0010\u0017\u001a\u00020\u000b*\u00020\u000c2\u000e\u0010\u0018\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00160\u0019H\u0002J8\u0010\u001a\u001a\u00020\u000b*\u00020\u000c2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u000f\u001a\u00020\u00102\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u0012H\u0002J\u0014\u0010\u001f\u001a\u00020 *\u00020 2\u0006\u0010!\u001a\u00020\"H\u0003J(\u0010#\u001a\u00020$*\u00020%2\u0006\u0010&\u001a\u00020\'2\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u0012H\u0016J(\u0010#\u001a\u00020$*\u00020(2\u0006\u0010&\u001a\u00020\'2\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u0012H\u0016J(\u0010#\u001a\u00020$*\u00020)2\u0006\u0010&\u001a\u00020\'2\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u0012H\u0016J(\u0010#\u001a\u00020$*\u00020*2\u0006\u0010&\u001a\u00020\'2\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u0012H\u0016J\"\u0010#\u001a\u00020+*\u00020,2\u0006\u0010-\u001a\u00020.2\u000c\u0010/\u001a\u0008\u0012\u0004\u0012\u00020\u000b00H\u0016J(\u0010#\u001a\u00020$*\u0002012\u0006\u0010&\u001a\u00020\'2\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u0012H\u0016J(\u0010#\u001a\u00020$*\u0002022\u0006\u0010&\u001a\u00020\'2\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u0012H\u0016J\u0014\u0010#\u001a\u00020$*\u0002032\u0006\u0010-\u001a\u00020.H\u0016J\u0014\u0010#\u001a\u00020$*\u0002042\u0006\u0010&\u001a\u00020\'H\u0016J\u0014\u0010#\u001a\u00020$*\u0002052\u0006\u0010&\u001a\u00020\'H\u0016J\u0014\u0010#\u001a\u00020$*\u0002062\u0006\u0010&\u001a\u00020\'H\u0016J\u0014\u0010#\u001a\u00020$*\u0002072\u0006\u0010&\u001a\u00020\'H\u0016J(\u0010#\u001a\u00020$*\u0002082\u0006\u0010&\u001a\u00020\'2\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u0012H\u0016J(\u0010#\u001a\u00020$*\u0002092\u0006\u0010&\u001a\u00020\'2\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u0012H\u0016J\u0014\u0010#\u001a\u00020 *\u00020:2\u0006\u0010&\u001a\u00020\'H\u0016J(\u0010#\u001a\u00020;*\u00020<2\u0006\u0010-\u001a\u00020.2\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u0012H\u0016J(\u0010#\u001a\u00020$*\u00020=2\u0006\u0010-\u001a\u00020.2\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u0012H\u0016J\u0014\u0010#\u001a\u00020$*\u00020>2\u0006\u0010&\u001a\u00020\'H\u0016J\u001c\u0010#\u001a\u00020?*\u00020@2\u0006\u0010&\u001a\u00020\'2\u0006\u0010A\u001a\u00020BH\u0016J(\u0010#\u001a\u00020C*\u00020D2\u0006\u0010-\u001a\u00020.2\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u0012H\u0016J\u0014\u0010E\u001a\u00020;*\u00020=2\u0006\u0010-\u001a\u00020.H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006F"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;",
        "cartEntryViewsFactory",
        "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
        "invoiceTimelineViewFactory",
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/ui/cart/CartEntryViewsFactory;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;Lcom/squareup/text/Formatter;)V",
        "addAttachmentRow",
        "",
        "Landroid/widget/LinearLayout;",
        "attachment",
        "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
        "event",
        "",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
        "addLineItemRow",
        "cartEntryView",
        "Lcom/squareup/ui/cart/CartEntryView;",
        "addLineItemRows",
        "cartEntryViews",
        "",
        "addPaymentRequestRow",
        "paymentRequestData",
        "Lcom/squareup/invoices/PaymentRequestData;",
        "index",
        "",
        "configureForInvoicePreview",
        "Landroid/webkit/WebView;",
        "invoiceUrl",
        "",
        "render",
        "Landroid/view/View;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;",
        "context",
        "Landroid/content/Context;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;",
        "Lcom/squareup/noho/NohoRow;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;",
        "parent",
        "Landroid/view/ViewGroup;",
        "onEditClicked",
        "Lkotlin/Function0;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$FloatingHeaderRowData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$InfoBoxData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$InvoiceDetailHeaderData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$MessageData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$PreviewData;",
        "Lcom/squareup/ui/account/view/SmartLineRow;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$RowData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$SubheaderData;",
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;",
        "eventHandler",
        "Lcom/squareup/features/invoices/widgets/EventHandler;",
        "Lcom/squareup/widgets/list/ToggleButtonRow;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;",
        "renderReadOnly",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cartEntryViewsFactory:Lcom/squareup/ui/cart/CartEntryViewsFactory;

.field private final invoiceTimelineViewFactory:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/cart/CartEntryViewsFactory;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cartEntryViewsFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceTimelineViewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->cartEntryViewsFactory:Lcom/squareup/ui/cart/CartEntryViewsFactory;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->invoiceTimelineViewFactory:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;

    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method private final addAttachmentRow(Landroid/widget/LinearLayout;Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/LinearLayout;",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            "Ljava/lang/Object;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 458
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 459
    iget-object v0, p2, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->size_bytes:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/squareup/util/Files;->readableFileSizeFromBytes(J)Ljava/lang/String;

    move-result-object v4

    .line 461
    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1c

    const/4 v9, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v9}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/noho/NohoRow$Icon;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v1, p1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v1}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->renderReadOnly(Lcom/squareup/features/invoices/widgets/SectionElement$RowData;Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v0

    .line 463
    instance-of v1, p3, Lcom/squareup/features/invoices/widgets/NoOp;

    if-nez v1, :cond_0

    sget-object v2, Lcom/squareup/marin/widgets/ChevronVisibility;->IF_ENABLED:Lcom/squareup/marin/widgets/ChevronVisibility;

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    :goto_0
    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    if-nez v1, :cond_1

    .line 465
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    .line 567
    new-instance v2, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$addAttachmentRow$$inlined$apply$lambda$1;

    invoke-direct {v2, p3, p4, p2}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$addAttachmentRow$$inlined$apply$lambda$1;-><init>(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 462
    :cond_1
    check-cast v0, Landroid/view/View;

    .line 460
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private final addLineItemRow(Landroid/widget/LinearLayout;Lcom/squareup/ui/cart/CartEntryView;)V
    .locals 0

    if-eqz p2, :cond_0

    .line 489
    check-cast p2, Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private final addLineItemRows(Landroid/widget/LinearLayout;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/LinearLayout;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/ui/cart/CartEntryView;",
            ">;)V"
        }
    .end annotation

    .line 493
    check-cast p2, Ljava/lang/Iterable;

    .line 574
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartEntryView;

    .line 493
    invoke-direct {p0, p1, v0}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->addLineItemRow(Landroid/widget/LinearLayout;Lcom/squareup/ui/cart/CartEntryView;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private final addPaymentRequestRow(Landroid/widget/LinearLayout;Lcom/squareup/invoices/PaymentRequestData;ILjava/lang/Object;Lkotlin/jvm/functions/Function1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/LinearLayout;",
            "Lcom/squareup/invoices/PaymentRequestData;",
            "I",
            "Ljava/lang/Object;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 480
    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;

    .line 481
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/squareup/invoices/PaymentRequestData;->getFormattedAmount()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/invoices/PaymentRequestData;->getRequestType()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 482
    invoke-virtual {p2}, Lcom/squareup/invoices/PaymentRequestData;->getFormattedDueDate()Ljava/lang/CharSequence;

    move-result-object p2

    .line 480
    invoke-direct {v0, v1, p2, p4}, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 484
    move-object p2, p1

    check-cast p2, Landroid/view/ViewGroup;

    new-instance v1, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$addPaymentRequestRow$1;

    invoke-direct {v1, p5, p4, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$addPaymentRequestRow$1;-><init>(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;I)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p0, v0, p2, v1}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoRow;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    .line 479
    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private final configureForInvoicePreview(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebView;
    .locals 2

    .line 502
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const-string v1, "settings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 507
    new-instance v0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$configureForInvoicePreview$$inlined$apply$lambda$1;

    invoke-direct {v0, p2}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$configureForInvoicePreview$$inlined$apply$lambda$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Landroid/webkit/WebViewClient;

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 518
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    return-object p1
.end method

.method private final renderReadOnly(Lcom/squareup/features/invoices/widgets/SectionElement$RowData;Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 1

    .line 106
    invoke-static {p2}, Lcom/squareup/ui/account/view/SmartLineRow;->inflateForListView(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p2

    .line 108
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;->getTitle()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 109
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;->getValue()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 111
    sget p1, Lcom/squareup/marin/R$drawable;->marin_selector_ultra_light_gray_when_pressed:I

    .line 110
    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setBackgroundResource(I)V

    const/4 p1, 0x1

    .line 113
    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 114
    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleEnabled(Z)V

    .line 115
    sget p1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    const-string p1, "SmartLineRow.inflateForL\u2026arin_dark_gray)\n        }"

    .line 107
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p2
.end method


# virtual methods
.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 386
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 p2, 0x1

    .line 387
    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 388
    new-instance p2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {p2, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 392
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;->getAttachmentList()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 562
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    .line 392
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;->getEvent()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->addAttachmentRow(Landroid/widget/LinearLayout;Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 394
    :cond_0
    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    new-instance v0, Lcom/squareup/marketfont/MarketButton;

    invoke-direct {v0, p2}, Lcom/squareup/marketfont/MarketButton;-><init>(Landroid/content/Context;)V

    .line 219
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;->getTitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 220
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    .line 545
    new-instance v2, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$onClickDebounced$1;

    invoke-direct {v2, p1, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p3, -0x1

    const/4 v2, -0x2

    invoke-direct {p1, p3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 227
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v2, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p3, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    .line 229
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v2, Lcom/squareup/marin/R$dimen;->marin_gap_small:I

    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 231
    iput p2, p1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 232
    iput p2, p1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 233
    iput p3, p1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 234
    iput p3, p1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 235
    check-cast p1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 339
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 p2, 0x1

    .line 340
    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 341
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 345
    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->cartEntryViewsFactory:Lcom/squareup/ui/cart/CartEntryViewsFactory;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v2

    move-object v3, v0

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->buildCartEntries(Lcom/squareup/protos/client/bills/Cart;Landroid/view/ViewGroup;)Lcom/squareup/ui/cart/CartEntryViews;

    move-result-object v1

    .line 347
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->getLineItemClickedEvent()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/squareup/features/invoices/widgets/NoOp;->INSTANCE:Lcom/squareup/features/invoices/widgets/NoOp;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/2addr p2, v2

    if-eqz p2, :cond_1

    .line 348
    invoke-virtual {v1}, Lcom/squareup/ui/cart/CartEntryViews;->getOrderItemViews()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    const/4 v2, 0x0

    .line 553
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    if-gez v2, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v3, Lcom/squareup/ui/cart/CartEntryView;

    .line 349
    check-cast v3, Landroid/view/View;

    .line 554
    new-instance v5, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$forEachIndexed$lambda$1;

    invoke-direct {v5, v2, p1, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$forEachIndexed$lambda$1;-><init>(ILcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;Lkotlin/jvm/functions/Function1;)V

    check-cast v5, Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move v2, v4

    goto :goto_0

    .line 353
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/ui/cart/CartEntryViews;->getOrderItemViews()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->addLineItemRows(Landroid/widget/LinearLayout;Ljava/util/List;)V

    .line 354
    invoke-virtual {v1}, Lcom/squareup/ui/cart/CartEntryViews;->getSubTotalView()Lcom/squareup/ui/cart/CartEntryView;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->addLineItemRow(Landroid/widget/LinearLayout;Lcom/squareup/ui/cart/CartEntryView;)V

    .line 355
    invoke-virtual {v1}, Lcom/squareup/ui/cart/CartEntryViews;->getDiscountViews()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->addLineItemRows(Landroid/widget/LinearLayout;Ljava/util/List;)V

    .line 356
    invoke-virtual {v1}, Lcom/squareup/ui/cart/CartEntryViews;->getTaxViews()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->addLineItemRows(Landroid/widget/LinearLayout;Ljava/util/List;)V

    .line 357
    invoke-virtual {v1}, Lcom/squareup/ui/cart/CartEntryViews;->getTipView()Lcom/squareup/ui/cart/CartEntryView;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->addLineItemRow(Landroid/widget/LinearLayout;Lcom/squareup/ui/cart/CartEntryView;)V

    .line 358
    invoke-virtual {v1}, Lcom/squareup/ui/cart/CartEntryViews;->getTotalView()Lcom/squareup/ui/cart/CartEntryView;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->addLineItemRow(Landroid/widget/LinearLayout;Lcom/squareup/ui/cart/CartEntryView;)V

    .line 360
    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 245
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$layout;->invoice_confirm_button:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    .line 246
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;->getInitialText()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setInitialText(Ljava/lang/CharSequence;)V

    .line 247
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;->getConfirmText()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setConfirmText(Ljava/lang/CharSequence;)V

    .line 248
    new-instance v1, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$7;

    invoke-direct {v1, p1, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$7;-><init>(Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    .line 253
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p3, -0x1

    const/4 v1, -0x2

    invoke-direct {p1, p3, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 255
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    .line 257
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gap_small:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 259
    iput p2, p1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 260
    iput p2, p1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 261
    iput p3, p1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 262
    iput p3, p1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 263
    check-cast p1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ConfirmButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 265
    check-cast v0, Landroid/view/View;

    return-object v0

    .line 245
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.ui.ConfirmButton"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "onEvent"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 422
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unsupported in V1."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 297
    new-instance v0, Lcom/squareup/widgets/SelectableEditText;

    invoke-direct {v0, p2}, Lcom/squareup/widgets/SelectableEditText;-><init>(Landroid/content/Context;)V

    .line 299
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 302
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 304
    iput p2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 305
    iput p2, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 309
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;->getMessage()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 310
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;->getHintText()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/widgets/SelectableEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 311
    check-cast v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 314
    new-instance p2, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$9;

    invoke-direct {p2, p1, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$9;-><init>(Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;Lkotlin/jvm/functions/Function1;)V

    check-cast p2, Landroid/text/TextWatcher;

    invoke-virtual {v0, p2}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 320
    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$FloatingHeaderRowData;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    new-instance v0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string v1, "parent.context"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {v0, p2, v1, v2, v1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 99
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$FloatingHeaderRowData;->getTitle()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->setHeaderText(Ljava/lang/CharSequence;)V

    .line 100
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$FloatingHeaderRowData;->getValue()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->setBodyText(Ljava/lang/CharSequence;)V

    .line 98
    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;Landroid/content/Context;)Landroid/view/View;
    .locals 2

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 286
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    .line 287
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$layout;->invoice_help_text:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_0

    check-cast p2, Lcom/squareup/widgets/MessageView;

    .line 288
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;->getMessage()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/MessageView;->setTextAndVisibility(Ljava/lang/CharSequence;)V

    .line 290
    check-cast p2, Landroid/view/View;

    return-object p2

    .line 287
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.widgets.MessageView"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$InfoBoxData;Landroid/content/Context;)Landroid/view/View;
    .locals 1

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 448
    new-instance v0, Lcom/squareup/features/invoices/widgets/InfoBoxView;

    invoke-direct {v0, p2}, Lcom/squareup/features/invoices/widgets/InfoBoxView;-><init>(Landroid/content/Context;)V

    .line 449
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$InfoBoxData;->getMessage()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/features/invoices/widgets/InfoBoxView;->setMessage(Ljava/lang/CharSequence;)V

    .line 448
    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$InvoiceDetailHeaderData;Landroid/content/Context;)Landroid/view/View;
    .locals 2

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 324
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceDetailHeader;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, Lcom/squareup/invoices/ui/InvoiceDetailHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 326
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    const/4 v1, 0x0

    .line 327
    invoke-virtual {v0, p2, v1, p2, v1}, Lcom/squareup/invoices/ui/InvoiceDetailHeader;->setPadding(IIII)V

    .line 328
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$InvoiceDetailHeaderData;->getPrimaryValue()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/invoices/ui/InvoiceDetailHeader;->setPrimaryValue(Ljava/lang/CharSequence;)V

    .line 329
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$InvoiceDetailHeaderData;->getPrimaryText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailHeader;->setPrimaryHeaderText(Ljava/lang/CharSequence;)V

    .line 330
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceDetailHeader;->setDividerVisible()V

    .line 332
    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$MessageData;Landroid/content/Context;)Landroid/view/View;
    .locals 2

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 276
    new-instance v0, Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, Lcom/squareup/widgets/MessageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 278
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 279
    invoke-virtual {v0, p2, p2, p2, p2}, Lcom/squareup/widgets/MessageView;->setPadding(IIII)V

    .line 280
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$MessageData;->getMessage()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setTextAndVisibility(Ljava/lang/CharSequence;)V

    .line 282
    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 428
    new-instance v0, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;

    invoke-direct {v0, p2}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;-><init>(Landroid/content/Context;)V

    .line 429
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->setCollapsedTitle(Ljava/lang/CharSequence;)V

    .line 430
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;->getTitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->setExpandedTitle(Ljava/lang/CharSequence;)V

    .line 431
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;->getDescription()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->setDescription(Ljava/lang/CharSequence;)V

    .line 432
    new-instance v1, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;-><init>(Lcom/squareup/protos/client/bills/Cart;Ljava/lang/Object;Ljava/lang/Object;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, v1, p2, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->setExpandedView(Landroid/view/View;)V

    .line 435
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p3, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 437
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget p3, Lcom/squareup/marin/R$dimen;->marin_gap_medium:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 438
    new-instance p3, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {p3, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 440
    iput p1, p3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 441
    iput p1, p3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 442
    iput p2, p3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 439
    check-cast p3, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, p3}, Lcom/squareup/features/invoices/widgets/packages/ExpandableView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 428
    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 401
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 p2, 0x1

    .line 402
    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 403
    new-instance p2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {p2, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 407
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;->getPaymentRequestData()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 565
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v1, 0x0

    const/4 v4, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v7, v4, 0x1

    if-gez v4, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    move-object v3, v1

    check-cast v3, Lcom/squareup/invoices/PaymentRequestData;

    .line 411
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;->getEvent()Ljava/lang/Object;

    move-result-object v5

    move-object v1, p0

    move-object v2, v0

    move-object v6, p3

    .line 408
    invoke-direct/range {v1 .. v6}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->addPaymentRequestRow(Landroid/widget/LinearLayout;Lcom/squareup/invoices/PaymentRequestData;ILjava/lang/Object;Lkotlin/jvm/functions/Function1;)V

    move v4, v7

    goto :goto_0

    .line 416
    :cond_1
    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public bridge synthetic render(Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 0

    .line 75
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$RowData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$RowData;",
            "Landroid/view/ViewGroup;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-direct {p0, p1, p2}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->renderReadOnly(Lcom/squareup/features/invoices/widgets/SectionElement$RowData;Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p2

    .line 86
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;->getEvent()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/features/invoices/widgets/NoOp;

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->IF_ENABLED:Lcom/squareup/marin/widgets/ChevronVisibility;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    :goto_0
    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 87
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;->getEvent()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/features/invoices/widgets/NoOp;

    if-nez v0, :cond_1

    .line 88
    move-object v0, p2

    check-cast v0, Landroid/view/View;

    .line 522
    new-instance v1, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$apply$lambda$1;

    invoke-direct {v1, p1, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$apply$lambda$1;-><init>(Lcom/squareup/features/invoices/widgets/SectionElement$RowData;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    :cond_1
    check-cast p2, Landroid/view/View;

    return-object p2
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$SubheaderData;Landroid/content/Context;)Landroid/view/View;
    .locals 2

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    .line 270
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$layout;->invoice_section_container_header:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_0

    check-cast p2, Lcom/squareup/marketfont/MarketTextView;

    .line 271
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$SubheaderData;->getTitle()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    check-cast p2, Landroid/view/View;

    return-object p2

    .line 270
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.marketfont.MarketTextView"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic render(Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;Landroid/content/Context;Lcom/squareup/features/invoices/widgets/EventHandler;)Landroid/view/View;
    .locals 0

    .line 75
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;Landroid/content/Context;Lcom/squareup/features/invoices/widgets/EventHandler;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public bridge synthetic render(Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 0

    .line 75
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Lcom/squareup/widgets/list/ToggleButtonRow;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement;Lcom/squareup/features/invoices/widgets/EventHandler;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer$DefaultImpls;->render(Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;Lcom/squareup/features/invoices/widgets/SectionElement;Lcom/squareup/features/invoices/widgets/EventHandler;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$PreviewData;Landroid/content/Context;)Landroid/webkit/WebView;
    .locals 2

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 377
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    .line 378
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$layout;->invoice_preview_webview:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_0

    check-cast p2, Landroid/webkit/WebView;

    .line 379
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$PreviewData;->getInvoiceUrl()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->configureForInvoicePreview(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebView;

    move-result-object p1

    return-object p1

    .line 378
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.webkit.WebView"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;Landroid/content/Context;Lcom/squareup/features/invoices/widgets/EventHandler;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;
    .locals 1

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 367
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->invoiceTimelineViewFactory:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;

    invoke-virtual {v0, p2, p1, p3}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;->create(Landroid/content/Context;Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;Lcom/squareup/features/invoices/widgets/EventHandler;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;

    move-result-object p1

    .line 370
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget p3, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    const/4 p3, 0x0

    .line 371
    invoke-virtual {p1, p3, p2, p2, p3}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->setPadding(IIII)V

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoRow;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;",
            "Landroid/view/ViewGroup;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/noho/NohoRow;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEditClicked"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    new-instance v0, Lcom/squareup/noho/NohoRow;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v1, "parent.context"

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 127
    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 130
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 132
    iput v1, v3, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 133
    iput v1, v3, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 135
    check-cast v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 137
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;->getTitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 138
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;->getSubtitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 140
    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getDescription()Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v1, :cond_1

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_2

    .line 142
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    sget v5, Lcom/squareup/noho/R$id;->description:I

    invoke-static {v1, v5}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v5, 0x5

    .line 143
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 144
    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 148
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;->getEvent()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/features/invoices/widgets/NoOp;

    if-nez v1, :cond_3

    .line 149
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v5, Lcom/squareup/common/strings/R$string;->edit:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v5, "parent.resources.getStri\u2026on.strings.R.string.edit)"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    check-cast v1, Ljava/lang/CharSequence;

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/text/style/CharacterStyle;

    .line 151
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    .line 153
    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v7

    sget v8, Lcom/squareup/noho/R$color;->noho_text_button_link_enabled:I

    .line 152
    invoke-static {v7, v8}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v7

    .line 151
    invoke-direct {v6, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    check-cast v6, Landroid/text/style/CharacterStyle;

    aput-object v6, v5, v4

    .line 156
    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 530
    new-instance v7, Lcom/squareup/fonts/FontSpan;

    invoke-static {v2, v4}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result v2

    invoke-direct {v7, v6, v2}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast v7, Landroid/text/style/CharacterStyle;

    aput-object v7, v5, v3

    .line 150
    invoke-static {v1, v5}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;[Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 159
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    .line 531
    new-instance v2, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$apply$lambda$2;

    invoke-direct {v2, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$apply$lambda$2;-><init>(Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    return-object v0
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;",
            "Landroid/view/ViewGroup;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/ui/account/view/SmartLineRow;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    invoke-static {p2}, Lcom/squareup/ui/account/view/SmartLineRow;->inflateForListView(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p2

    .line 194
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;->getTitle()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 195
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;->getRepeatEveryString()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 197
    sget v0, Lcom/squareup/marin/R$drawable;->marin_selector_ultra_light_gray_when_pressed:I

    .line 196
    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setBackgroundResource(I)V

    const/4 v0, 0x1

    .line 199
    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 200
    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleEnabled(Z)V

    .line 201
    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p2, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 203
    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 204
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;->getEndOnString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p2, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueSubtitleText(Ljava/lang/CharSequence;)V

    .line 205
    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueSubtitleVisible(Z)V

    .line 207
    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->IF_ENABLED:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 208
    move-object v0, p2

    check-cast v0, Landroid/view/View;

    .line 538
    new-instance v1, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$apply$lambda$4;

    invoke-direct {v1, p1, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$apply$lambda$4;-><init>(Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string p1, "SmartLineRow.inflateForL\u2026t))\n          }\n        }"

    .line 193
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p2
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;",
            "Landroid/view/ViewGroup;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/widgets/list/ToggleButtonRow;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->getLabel()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    .line 173
    sget v2, Lcom/squareup/marin/R$dimen;->marin_gutter_half_lollipop:I

    .line 174
    sget v3, Lcom/squareup/marin/R$drawable;->marin_selector_ultra_light_gray_when_pressed:I

    .line 171
    invoke-static {p2, v0, v1, v2, v3}, Lcom/squareup/widgets/list/ToggleButtonRow;->switchRow(Landroid/content/Context;Ljava/lang/CharSequence;III)Lcom/squareup/widgets/list/ToggleButtonRow;

    move-result-object p2

    .line 177
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->getChecked()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 178
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->getEvent()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/squareup/features/invoices/widgets/NoOp;->INSTANCE:Lcom/squareup/features/invoices/widgets/NoOp;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setEnabled(Z)V

    .line 179
    new-instance v0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$apply$lambda$3;

    invoke-direct {v0, p1, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$apply$lambda$3;-><init>(Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const-string p1, "ToggleButtonRow.switchRo\u2026d))\n          }\n        }"

    .line 176
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p2
.end method
