.class public final Lcom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactory;
.super Ljava/lang/Object;
.source "RealBottomSheetDialogViewFactory.kt"

# interfaces
.implements Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBottomSheetDialogViewFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBottomSheetDialogViewFactory.kt\ncom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,77:1\n1360#2:78\n1429#2,3:79\n*E\n*S KotlinDebug\n*F\n+ 1 RealBottomSheetDialogViewFactory.kt\ncom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactory\n*L\n29#1:78\n29#1,3:79\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J,\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00042\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactory;",
        "Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;",
        "()V",
        "create",
        "",
        "Landroid/view/View;",
        "elements",
        "Lcom/squareup/features/invoices/widgets/SectionElement;",
        "parent",
        "Landroid/view/ViewGroup;",
        "eventHandler",
        "Lcom/squareup/features/invoices/widgets/EventHandler;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Ljava/util/List;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/EventHandler;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/features/invoices/widgets/EventHandler;",
            ")",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    const-string v0, "elements"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    check-cast p1, Ljava/lang/Iterable;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 79
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 80
    check-cast v1, Lcom/squareup/features/invoices/widgets/SectionElement;

    .line 31
    instance-of v2, v1, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-static {v1, p2, p3}, Lcom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactoryKt;->access$render(Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/EventHandler;)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 32
    :cond_0
    instance-of v2, v1, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    if-eqz v2, :cond_1

    check-cast v1, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    invoke-static {v1, p2, p3}, Lcom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactoryKt;->access$render(Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/EventHandler;)Landroid/view/View;

    move-result-object v1

    .line 34
    :goto_1
    invoke-static {v1}, Lcom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactoryKt;->access$withLayoutParams(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 33
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Can\'t render "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 p2, 0x2e

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 81
    :cond_2
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
