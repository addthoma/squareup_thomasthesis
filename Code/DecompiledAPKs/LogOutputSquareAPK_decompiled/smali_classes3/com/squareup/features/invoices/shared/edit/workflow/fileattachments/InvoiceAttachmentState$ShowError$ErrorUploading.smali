.class public final Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;
.super Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError;
.source "InvoiceAttachmentState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ErrorUploading"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0014\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\tH\u00c6\u0003J\t\u0010\u001d\u001a\u00020\tH\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u000cH\u00c6\u0003JE\u0010\u001f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\t2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cH\u00c6\u0001J\t\u0010 \u001a\u00020!H\u00d6\u0001J\u0013\u0010\"\u001a\u00020#2\u0008\u0010$\u001a\u0004\u0018\u00010%H\u00d6\u0003J\t\u0010&\u001a\u00020!H\u00d6\u0001J\t\u0010\'\u001a\u00020\tH\u00d6\u0001J\u0019\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020!H\u00d6\u0001R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0014\u0010\n\u001a\u00020\tX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0014\u0010\u0008\u001a\u00020\tX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0011R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0014\u0010\u000b\u001a\u00020\u000cX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError;",
        "uploadValidationInfo",
        "Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;",
        "attachmentStatus",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;",
        "fileMetadata",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;",
        "errorTitle",
        "",
        "errorBody",
        "invoiceTokenType",
        "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
        "(Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)V",
        "getAttachmentStatus",
        "()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;",
        "getErrorBody",
        "()Ljava/lang/String;",
        "getErrorTitle",
        "getFileMetadata",
        "()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;",
        "getInvoiceTokenType",
        "()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
        "getUploadValidationInfo",
        "()Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final attachmentStatus:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

.field private final errorBody:Ljava/lang/String;

.field private final errorTitle:Ljava/lang/String;

.field private final fileMetadata:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

.field private final invoiceTokenType:Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

.field private final uploadValidationInfo:Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading$Creator;

    invoke-direct {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading$Creator;-><init>()V

    sput-object v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)V
    .locals 1

    const-string/jumbo v0, "uploadValidationInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attachmentStatus"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileMetadata"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorTitle"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorBody"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceTokenType"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 100
    invoke-direct {p0, v0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->uploadValidationInfo:Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    iput-object p2, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->attachmentStatus:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    iput-object p3, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->fileMetadata:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    iput-object p4, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->errorTitle:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->errorBody:Ljava/lang/String;

    iput-object p6, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->invoiceTokenType:Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;ILjava/lang/Object;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->uploadValidationInfo:Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getAttachmentStatus()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    move-result-object p2

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object p3

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getErrorTitle()Ljava/lang/String;

    move-result-object p4

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getErrorBody()Ljava/lang/String;

    move-result-object p5

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object p6

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->copy(Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->uploadValidationInfo:Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    return-object v0
.end method

.method public final component2()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getAttachmentStatus()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object v0

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getErrorTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getErrorBody()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component6()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;
    .locals 8

    const-string/jumbo v0, "uploadValidationInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attachmentStatus"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileMetadata"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorTitle"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorBody"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceTokenType"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;-><init>(Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;

    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->uploadValidationInfo:Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    iget-object v1, p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->uploadValidationInfo:Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getAttachmentStatus()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getAttachmentStatus()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getErrorTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getErrorTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getErrorBody()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getErrorBody()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getAttachmentStatus()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->attachmentStatus:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    return-object v0
.end method

.method public getErrorBody()Ljava/lang/String;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->errorBody:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorTitle()Ljava/lang/String;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->errorTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->fileMetadata:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    return-object v0
.end method

.method public getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->invoiceTokenType:Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    return-object v0
.end method

.method public final getUploadValidationInfo()Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->uploadValidationInfo:Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->uploadValidationInfo:Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getAttachmentStatus()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getErrorTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getErrorBody()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ErrorUploading(uploadValidationInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->uploadValidationInfo:Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", attachmentStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getAttachmentStatus()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", fileMetadata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", errorTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getErrorTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", errorBody="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getErrorBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", invoiceTokenType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->uploadValidationInfo:Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->attachmentStatus:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->fileMetadata:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->errorTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->errorBody:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;->invoiceTokenType:Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
