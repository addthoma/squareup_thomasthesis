.class public final Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1$$special$$inlined$combineLatest$1;
.super Ljava/lang/Object;
.source "RxKotlin.kt"

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;->apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/BiFunction<",
        "TT1;TT2;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxKotlin.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxKotlin.kt\ncom/squareup/util/rx2/Observables$combineLatest$1\n+ 2 RealConnectedScalesRepository.kt\ncom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,1655:1\n72#2:1656\n73#2:1662\n74#2:1664\n2454#3,5:1657\n2460#3:1663\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0006\u0010\u0000\u001a\u0002H\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0001*\u00020\u00032\u0006\u0010\u0005\u001a\u0002H\u00022\u0006\u0010\u0006\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "R",
        "T1",
        "",
        "T2",
        "t1",
        "t2",
        "apply",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
        "com/squareup/util/rx2/Observables$combineLatest$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;


# direct methods
.method public constructor <init>(Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1$$special$$inlined$combineLatest$1;->this$0:Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT1;TT2;)TR;"
        }
    .end annotation

    const-string v0, "t1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "t2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    check-cast p2, Ljava/util/List;

    check-cast p1, Ljava/util/List;

    .line 1656
    check-cast p1, Ljava/lang/Iterable;

    .line 1657
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1658
    check-cast p2, Ljava/lang/Iterable;

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/16 v2, 0xa

    .line 1659
    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p1

    invoke-static {p2, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p2

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2, p1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1660
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1661
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/scales/UnitOfMeasurement;

    check-cast p1, Lcom/squareup/scales/ScaleTracker$HardwareScale;

    .line 1662
    iget-object v3, p0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1$$special$$inlined$combineLatest$1;->this$0:Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;

    iget-object v3, v3, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;->this$0:Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;

    invoke-static {v3, p1, v2}, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;->access$toConnectedScale(Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/UnitOfMeasurement;)Lcom/squareup/connectedscalesdata/ConnectedScale;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1663
    :cond_0
    check-cast p2, Ljava/util/List;

    return-object p2
.end method
