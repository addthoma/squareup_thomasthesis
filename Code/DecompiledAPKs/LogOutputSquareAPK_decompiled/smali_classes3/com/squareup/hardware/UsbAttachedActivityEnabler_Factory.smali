.class public final Lcom/squareup/hardware/UsbAttachedActivityEnabler_Factory;
.super Ljava/lang/Object;
.source "UsbAttachedActivityEnabler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/hardware/UsbAttachedActivityEnabler;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ActivityListener;",
            ">;"
        }
    .end annotation
.end field

.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final packageManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/pm/PackageManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/pm/PackageManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ActivityListener;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/hardware/UsbAttachedActivityEnabler_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/hardware/UsbAttachedActivityEnabler_Factory;->packageManagerProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/hardware/UsbAttachedActivityEnabler_Factory;->activityListenerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/hardware/UsbAttachedActivityEnabler_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/pm/PackageManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ActivityListener;",
            ">;)",
            "Lcom/squareup/hardware/UsbAttachedActivityEnabler_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/hardware/UsbAttachedActivityEnabler_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/hardware/UsbAttachedActivityEnabler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/app/Application;Landroid/content/pm/PackageManager;Lcom/squareup/ActivityListener;)Lcom/squareup/hardware/UsbAttachedActivityEnabler;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/hardware/UsbAttachedActivityEnabler;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/hardware/UsbAttachedActivityEnabler;-><init>(Landroid/app/Application;Landroid/content/pm/PackageManager;Lcom/squareup/ActivityListener;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/hardware/UsbAttachedActivityEnabler;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/hardware/UsbAttachedActivityEnabler_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/hardware/UsbAttachedActivityEnabler_Factory;->packageManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageManager;

    iget-object v2, p0, Lcom/squareup/hardware/UsbAttachedActivityEnabler_Factory;->activityListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ActivityListener;

    invoke-static {v0, v1, v2}, Lcom/squareup/hardware/UsbAttachedActivityEnabler_Factory;->newInstance(Landroid/app/Application;Landroid/content/pm/PackageManager;Lcom/squareup/ActivityListener;)Lcom/squareup/hardware/UsbAttachedActivityEnabler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/hardware/UsbAttachedActivityEnabler_Factory;->get()Lcom/squareup/hardware/UsbAttachedActivityEnabler;

    move-result-object v0

    return-object v0
.end method
