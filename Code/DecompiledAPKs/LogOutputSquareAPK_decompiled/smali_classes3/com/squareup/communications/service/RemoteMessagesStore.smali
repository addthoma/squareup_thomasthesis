.class public interface abstract Lcom/squareup/communications/service/RemoteMessagesStore;
.super Ljava/lang/Object;
.source "RemoteMessagesStore.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008f\u0018\u00002\u00020\u0001J\"\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u00050\u00040\u00032\u0006\u0010\u0007\u001a\u00020\u0008H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/communications/service/RemoteMessagesStore;",
        "",
        "messages",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "",
        "Lcom/squareup/communications/Message;",
        "placementId",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract messages(Ljava/lang/String;)Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Ljava/util/List<",
            "Lcom/squareup/communications/Message;",
            ">;>;>;"
        }
    .end annotation
.end method
