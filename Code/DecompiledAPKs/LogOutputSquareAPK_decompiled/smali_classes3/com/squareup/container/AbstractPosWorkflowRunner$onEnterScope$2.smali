.class final Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$2;
.super Ljava/lang/Object;
.source "AbstractPosWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/AbstractPosWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lcom/squareup/container/WorkflowTreeKey;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0004\u0008\u0002\u0010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "P",
        "",
        "O",
        "R",
        "it",
        "Lcom/squareup/container/WorkflowTreeKey;",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/container/AbstractPosWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final test(Lcom/squareup/container/WorkflowTreeKey;)Z
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 407
    iget-object p1, p1, Lcom/squareup/container/WorkflowTreeKey;->runnerServiceName:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->access$getServiceName$p(Lcom/squareup/container/AbstractPosWorkflowRunner;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 140
    check-cast p1, Lcom/squareup/container/WorkflowTreeKey;

    invoke-virtual {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$2;->test(Lcom/squareup/container/WorkflowTreeKey;)Z

    move-result p1

    return p1
.end method
