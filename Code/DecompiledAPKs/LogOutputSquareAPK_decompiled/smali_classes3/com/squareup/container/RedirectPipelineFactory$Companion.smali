.class public final Lcom/squareup/container/RedirectPipelineFactory$Companion;
.super Ljava/lang/Object;
.source "RedirectPipelineFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/RedirectPipelineFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRedirectPipelineFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RedirectPipelineFactory.kt\ncom/squareup/container/RedirectPipelineFactory$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,320:1\n1550#2,3:321\n111#3:324\n111#3:325\n111#3:326\n111#3:327\n111#3:328\n*E\n*S KotlinDebug\n*F\n+ 1 RedirectPipelineFactory.kt\ncom/squareup/container/RedirectPipelineFactory$Companion\n*L\n145#1,3:321\n224#1:324\n225#1:325\n226#1:326\n227#1:327\n228#1:328\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0014\u0010\u0007\u001a\u00020\u0008*\u00020\t2\u0006\u0010\n\u001a\u00020\u0008H\u0002J\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u0004*\u00020\t2\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0016\u0010\u000c\u001a\u0004\u0018\u00010\u0004*\u00020\t2\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0016\u0010\r\u001a\u0004\u0018\u00010\u0004*\u00020\t2\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u0004*\u00020\t2\u0006\u0010\u0005\u001a\u00020\u0006H\u0003J\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u0010*\u00020\t2\u0006\u0010\u0011\u001a\u00020\u0010J\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u0010*\u00020\t2\u0006\u0010\u0005\u001a\u00020\u0006H\u0002\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/container/RedirectPipelineFactory$Companion;",
        "",
        "()V",
        "redirectToNewSection",
        "Lcom/squareup/container/RedirectStep$Result;",
        "traversal",
        "Lflow/Traversal;",
        "initialDetailScreenForPath",
        "Lflow/path/Path;",
        "Lcom/squareup/container/RedirectPipelineFactory;",
        "masterScreen",
        "redirectBackwardPastMaster",
        "redirectFromHidesMasterToMaster",
        "redirectFromMasterToDetail",
        "redirectToEnforceLayering",
        "tweakHistoryFromMasterToDetail",
        "Lflow/History;",
        "given",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 58
    invoke-direct {p0}, Lcom/squareup/container/RedirectPipelineFactory$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$redirectBackwardPastMaster(Lcom/squareup/container/RedirectPipelineFactory$Companion;Lcom/squareup/container/RedirectPipelineFactory;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 0

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/squareup/container/RedirectPipelineFactory$Companion;->redirectBackwardPastMaster(Lcom/squareup/container/RedirectPipelineFactory;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$redirectFromHidesMasterToMaster(Lcom/squareup/container/RedirectPipelineFactory$Companion;Lcom/squareup/container/RedirectPipelineFactory;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 0

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/squareup/container/RedirectPipelineFactory$Companion;->redirectFromHidesMasterToMaster(Lcom/squareup/container/RedirectPipelineFactory;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$redirectFromMasterToDetail(Lcom/squareup/container/RedirectPipelineFactory$Companion;Lcom/squareup/container/RedirectPipelineFactory;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 0

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/squareup/container/RedirectPipelineFactory$Companion;->redirectFromMasterToDetail(Lcom/squareup/container/RedirectPipelineFactory;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$redirectToEnforceLayering(Lcom/squareup/container/RedirectPipelineFactory$Companion;Lcom/squareup/container/RedirectPipelineFactory;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 0

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/squareup/container/RedirectPipelineFactory$Companion;->redirectToEnforceLayering(Lcom/squareup/container/RedirectPipelineFactory;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$redirectToNewSection(Lcom/squareup/container/RedirectPipelineFactory$Companion;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 0

    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/container/RedirectPipelineFactory$Companion;->redirectToNewSection(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object p0

    return-object p0
.end method

.method private final initialDetailScreenForPath(Lcom/squareup/container/RedirectPipelineFactory;Lflow/path/Path;)Lflow/path/Path;
    .locals 0

    .line 205
    invoke-virtual {p1}, Lcom/squareup/container/RedirectPipelineFactory;->getInitialDetailScreenForAnnotation()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-static {p2}, Lcom/squareup/container/Masters;->getMasterAnnotation(Lflow/path/Path;)Lcom/squareup/container/layer/Master;

    move-result-object p2

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/path/Path;

    return-object p1
.end method

.method private final redirectBackwardPastMaster(Lcom/squareup/container/RedirectPipelineFactory;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 2

    .line 159
    iget-object v0, p2, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 168
    invoke-virtual {p1}, Lcom/squareup/container/RedirectPipelineFactory;->getHasMasterLayout()Z

    move-result p1

    const-string v1, "goingTo"

    if-nez p1, :cond_0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lflow/path/Path;

    invoke-static {p1}, Lcom/squareup/container/Masters;->isMasterRequiringDetail(Lflow/path/Path;)Z

    move-result p1

    if-eqz p1, :cond_2

    :cond_0
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lflow/path/Path;

    invoke-static {p1}, Lcom/squareup/container/Masters;->isMasterScreen(Lflow/path/Path;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 166
    iget-object p1, p2, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {p1}, Lflow/History;->size()I

    move-result p1

    const/4 v1, 0x1

    if-le p1, v1, :cond_2

    .line 167
    iget-object p1, p2, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {p1, v1}, Lflow/History;->peek(I)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p2, Lflow/Traversal;->direction:Lflow/Direction;

    sget-object v0, Lflow/Direction;->BACKWARD:Lflow/Direction;

    if-ne p1, v0, :cond_2

    .line 170
    iget-object p1, p2, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    .line 171
    invoke-virtual {p1}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    const-string p2, "redirect"

    .line 173
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lflow/History$Builder;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 176
    sget-object p2, Lcom/squareup/container/Halt;->INSTANCE:Lcom/squareup/container/Halt;

    invoke-virtual {p1, p2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 181
    :cond_1
    new-instance p2, Lcom/squareup/container/RedirectStep$Result;

    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p1, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    const-string v0, "backwardPastMaster"

    invoke-direct {p2, v0, p1}, Lcom/squareup/container/RedirectStep$Result;-><init>(Ljava/lang/String;Lcom/squareup/container/Command;)V

    return-object p2

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method private final redirectFromHidesMasterToMaster(Lcom/squareup/container/RedirectPipelineFactory;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 6

    .line 140
    invoke-virtual {p1}, Lcom/squareup/container/RedirectPipelineFactory;->getHasMasterLayout()Z

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 143
    :cond_0
    iget-object p1, p2, Lflow/Traversal;->origin:Lflow/History;

    const-string/jumbo v1, "traversal.origin"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/container/layer/HidesMasterKt;->hidesMaster(Lflow/History;)Z

    move-result p1

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-object p1, p2, Lflow/Traversal;->destination:Lflow/History;

    const-string/jumbo v3, "traversal.destination"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/container/layer/HidesMasterKt;->hidesMaster(Lflow/History;)Z

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 144
    :goto_0
    iget-object v3, p2, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v3}, Lflow/History;->framesFromBottom()Ljava/lang/Iterable;

    move-result-object v3

    const-string/jumbo v4, "traversal.destination.framesFromBottom<Path>()"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 321
    instance-of v4, v3, Ljava/util/Collection;

    if-eqz v4, :cond_2

    move-object v4, v3

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_1

    .line 322
    :cond_2
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lflow/path/Path;

    const-string v5, "it"

    .line 145
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4}, Lcom/squareup/container/Masters;->isMasterScreen(Lflow/path/Path;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v2, 0x1

    :cond_4
    :goto_1
    if-eqz p1, :cond_6

    if-nez v2, :cond_5

    goto :goto_2

    .line 150
    :cond_5
    new-instance p1, Lcom/squareup/container/RedirectStep$Result;

    iget-object p2, p2, Lflow/Traversal;->destination:Lflow/History;

    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p2, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p2

    const-string v0, "redirectFromHidesMasterToMaster"

    invoke-direct {p1, v0, p2}, Lcom/squareup/container/RedirectStep$Result;-><init>(Ljava/lang/String;Lcom/squareup/container/Command;)V

    return-object p1

    :cond_6
    :goto_2
    return-object v0
.end method

.method private final redirectFromMasterToDetail(Lcom/squareup/container/RedirectPipelineFactory;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 2

    .line 193
    invoke-virtual {p1}, Lcom/squareup/container/RedirectPipelineFactory;->getHasMasterLayout()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v1, "traversal.destination.top<Path>()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lflow/path/Path;

    invoke-static {v0}, Lcom/squareup/container/Masters;->isMasterRequiringDetail(Lflow/path/Path;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 194
    :cond_0
    move-object v0, p0

    check-cast v0, Lcom/squareup/container/RedirectPipelineFactory$Companion;

    invoke-direct {v0, p1, p2}, Lcom/squareup/container/RedirectPipelineFactory$Companion;->tweakHistoryFromMasterToDetail(Lcom/squareup/container/RedirectPipelineFactory;Lflow/Traversal;)Lflow/History;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 197
    new-instance v0, Lcom/squareup/container/RedirectStep$Result;

    iget-object p2, p2, Lflow/Traversal;->direction:Lflow/Direction;

    invoke-static {p1, p2}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    const-string p2, "fromMasterToDetail"

    invoke-direct {v0, p2, p1}, Lcom/squareup/container/RedirectStep$Result;-><init>(Ljava/lang/String;Lcom/squareup/container/Command;)V

    return-object v0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private final redirectToEnforceLayering(Lcom/squareup/container/RedirectPipelineFactory;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 7

    .line 64
    invoke-virtual {p1}, Lcom/squareup/container/RedirectPipelineFactory;->getLayerIndex()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    iget-object v1, p2, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v1

    const-string/jumbo v2, "traversal.destination.top()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 66
    invoke-static {}, Lflow/History;->emptyBuilder()Lflow/History$Builder;

    move-result-object v1

    .line 69
    iget-object v2, p2, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v2}, Lflow/History;->framesFromBottom()Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/container/ContainerTreeKey;

    .line 70
    invoke-virtual {p1}, Lcom/squareup/container/RedirectPipelineFactory;->getLayerIndex()Lkotlin/jvm/functions/Function1;

    move-result-object v5

    const-string v6, "key"

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v5, v4}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->intValue()I

    move-result v5

    if-gt v5, v0, :cond_0

    .line 71
    invoke-virtual {v1, v4}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    goto :goto_0

    :cond_0
    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    if-eqz v3, :cond_2

    .line 78
    new-instance p1, Lcom/squareup/container/RedirectStep$Result;

    invoke-virtual {v1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object v0

    iget-object p2, p2, Lflow/Traversal;->direction:Lflow/Direction;

    invoke-static {v0, p2}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p2

    const-string v0, "enforceLayering"

    invoke-direct {p1, v0, p2}, Lcom/squareup/container/RedirectStep$Result;-><init>(Ljava/lang/String;Lcom/squareup/container/Command;)V

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    return-object p1
.end method

.method private final redirectToNewSection(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 6

    .line 91
    iget-object v0, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    const-string v1, "incomingPath"

    .line 93
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/container/Masters;->getSection(Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_b

    .line 98
    move-object v2, v1

    check-cast v2, Ljava/lang/Class;

    .line 99
    iget-object v3, p1, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {v3}, Lflow/History;->size()I

    move-result v3

    const/4 v4, 0x0

    move-object v5, v2

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    if-nez v5, :cond_1

    .line 102
    iget-object v5, p1, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {v5, v2}, Lflow/History;->peek(I)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-static {v5}, Lcom/squareup/container/RedirectPipelineFactoryKt;->access$getSection$p(Ljava/lang/Object;)Ljava/lang/Class;

    move-result-object v5

    goto :goto_1

    :cond_0
    move-object v5, v1

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    if-eqz v5, :cond_b

    .line 105
    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_9

    .line 111
    :cond_2
    iget-object p1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    .line 114
    new-instance v2, Ljava/util/ArrayDeque;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/ArrayDeque;-><init>(I)V

    .line 115
    :goto_2
    invoke-virtual {p1}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-static {v5}, Lcom/squareup/container/RedirectPipelineFactoryKt;->access$getSection$p(Ljava/lang/Object;)Ljava/lang/Class;

    move-result-object v5

    goto :goto_3

    :cond_3
    move-object v5, v1

    :goto_3
    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p1}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto :goto_2

    .line 118
    :cond_4
    :goto_4
    invoke-virtual {p1}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {v0}, Lcom/squareup/container/RedirectPipelineFactoryKt;->access$getSection$p(Ljava/lang/Object;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_5

    :cond_5
    move-object v0, v1

    :goto_5
    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    goto :goto_4

    :cond_6
    :goto_6
    const-string v0, "builder"

    .line 122
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lflow/History$Builder;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 123
    invoke-virtual {p1}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "screen"

    .line 124
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/container/RedirectPipelineFactoryKt;->access$getSection$p(Ljava/lang/Object;)Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_7

    const/4 v1, 0x1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    if-eqz v1, :cond_8

    .line 125
    invoke-virtual {v2, v0}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto :goto_6

    .line 124
    :cond_8
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected @Section-annotated screen "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " found"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 129
    :cond_9
    :goto_8
    invoke-virtual {v2}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 130
    invoke-virtual {v2}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    goto :goto_8

    .line 134
    :cond_a
    new-instance v0, Lcom/squareup/container/RedirectStep$Result;

    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p1, v1}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    const-string/jumbo v1, "to new master/detail section"

    invoke-direct {v0, v1, p1}, Lcom/squareup/container/RedirectStep$Result;-><init>(Ljava/lang/String;Lcom/squareup/container/Command;)V

    return-object v0

    :cond_b
    :goto_9
    return-object v1
.end method

.method private final tweakHistoryFromMasterToDetail(Lcom/squareup/container/RedirectPipelineFactory;Lflow/Traversal;)Lflow/History;
    .locals 5

    const/4 v0, 0x0

    .line 221
    move-object v1, v0

    check-cast v1, Lflow/path/Path;

    .line 222
    iget-object v2, p2, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v2}, Lflow/History;->framesFromTop()Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/container/ContainerTreeKey;

    const-string v4, "path"

    .line 223
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/squareup/container/layer/HidesMasterKt;->hidesMaster(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 324
    const-class v4, Lcom/squareup/container/layer/CardScreen;

    invoke-static {v3, v4}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 325
    const-class v4, Lcom/squareup/container/layer/FullSheet;

    invoke-static {v3, v4}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 326
    const-class v4, Lcom/squareup/container/layer/CardOverSheetScreen;

    invoke-static {v3, v4}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 327
    const-class v4, Lcom/squareup/container/layer/DialogCardScreen;

    invoke-static {v3, v4}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 328
    const-class v4, Lcom/squareup/container/layer/DialogScreen;

    invoke-static {v3, v4}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_0

    .line 232
    :cond_1
    move-object v1, v3

    check-cast v1, Lflow/path/Path;

    :cond_2
    if-eqz v1, :cond_5

    .line 236
    invoke-static {v1}, Lcom/squareup/container/Masters;->isMasterScreen(Lflow/path/Path;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    .line 240
    invoke-static {}, Lflow/History;->emptyBuilder()Lflow/History$Builder;

    move-result-object v0

    .line 241
    iget-object p2, p2, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {p2}, Lflow/History;->framesFromBottom()Ljava/lang/Iterable;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_3
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflow/path/Path;

    .line 242
    invoke-virtual {v0, v2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 243
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 244
    move-object v2, p0

    check-cast v2, Lcom/squareup/container/RedirectPipelineFactory$Companion;

    invoke-direct {v2, p1, v1}, Lcom/squareup/container/RedirectPipelineFactory$Companion;->initialDetailScreenForPath(Lcom/squareup/container/RedirectPipelineFactory;Lflow/path/Path;)Lflow/path/Path;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    goto :goto_1

    .line 248
    :cond_4
    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    return-object p1

    :cond_5
    return-object v0
.end method


# virtual methods
.method public final tweakHistoryFromMasterToDetail(Lcom/squareup/container/RedirectPipelineFactory;Lflow/History;)Lflow/History;
    .locals 2

    const-string v0, "$this$tweakHistoryFromMasterToDetail"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "given"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    move-object v0, p0

    check-cast v0, Lcom/squareup/container/RedirectPipelineFactory$Companion;

    new-instance v1, Lflow/Traversal;

    invoke-direct {v1, p2}, Lflow/Traversal;-><init>(Lflow/History;)V

    invoke-direct {v0, p1, v1}, Lcom/squareup/container/RedirectPipelineFactory$Companion;->tweakHistoryFromMasterToDetail(Lcom/squareup/container/RedirectPipelineFactory;Lflow/Traversal;)Lflow/History;

    move-result-object p1

    return-object p1
.end method
