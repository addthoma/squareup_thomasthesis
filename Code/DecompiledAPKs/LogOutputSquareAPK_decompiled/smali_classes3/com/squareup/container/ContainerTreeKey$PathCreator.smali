.class public abstract Lcom/squareup/container/ContainerTreeKey$PathCreator;
.super Ljava/lang/Object;
.source "ContainerTreeKey.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/ContainerTreeKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PathCreator"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/ContainerTreeKey$PathCreator$ParceledPathCreator;,
        Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/container/ContainerTreeKey;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">(TT;)",
            "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
            "TT;>;"
        }
    .end annotation

    .line 357
    new-instance v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$1;

    invoke-direct {v0, p0}, Lcom/squareup/container/ContainerTreeKey$PathCreator$1;-><init>(Lcom/squareup/container/ContainerTreeKey;)V

    return-object v0
.end method

.method public static fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">(",
            "Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc<",
            "TT;>;)",
            "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
            "TT;>;"
        }
    .end annotation

    .line 382
    new-instance v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParceledPathCreator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParceledPathCreator;-><init>(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;Lcom/squareup/container/ContainerTreeKey$1;)V

    return-object v0
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Lcom/squareup/container/ContainerTreeKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")TT;"
        }
    .end annotation

    .line 337
    invoke-virtual {p0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 338
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readSparseArray(Ljava/lang/ClassLoader;)Landroid/util/SparseArray;

    move-result-object v0

    .line 339
    invoke-virtual {p0, p1}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->doCreateFromParcel(Landroid/os/Parcel;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    .line 340
    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->setViewState(Landroid/util/SparseArray;)V

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 334
    invoke-virtual {p0, p1}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1
.end method

.method protected abstract doCreateFromParcel(Landroid/os/Parcel;)Lcom/squareup/container/ContainerTreeKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")TT;"
        }
    .end annotation
.end method

.method protected getClassLoader()Ljava/lang/ClassLoader;
    .locals 1

    .line 347
    const-class v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    return-object v0
.end method
