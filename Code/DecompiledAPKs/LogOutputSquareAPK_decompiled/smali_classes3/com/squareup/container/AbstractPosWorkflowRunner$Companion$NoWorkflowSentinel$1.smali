.class public final Lcom/squareup/container/AbstractPosWorkflowRunner$Companion$NoWorkflowSentinel$1;
.super Ljava/lang/Object;
.source "AbstractPosWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/rx2/RxWorkflowHost;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/AbstractPosWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAbstractPosWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AbstractPosWorkflowRunner.kt\ncom/squareup/container/AbstractPosWorkflowRunner$Companion$NoWorkflowSentinel$1\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,525:1\n151#2,2:526\n*E\n*S KotlinDebug\n*F\n+ 1 AbstractPosWorkflowRunner.kt\ncom/squareup/container/AbstractPosWorkflowRunner$Companion$NoWorkflowSentinel$1\n*L\n522#1,2:526\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000)\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u0001J\t\u0010\u000c\u001a\u00020\rH\u0096\u0001R\u001a\u0010\u0003\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00020\u0004X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R \u0010\u0007\u001a\u0010\u0012\u000c\u0008\u0001\u0012\u0008\u0012\u0004\u0012\u00020\u00020\t0\u0008X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u000e"
    }
    d2 = {
        "com/squareup/container/AbstractPosWorkflowRunner$Companion$NoWorkflowSentinel$1",
        "Lcom/squareup/workflow/rx2/RxWorkflowHost;",
        "",
        "outputs",
        "Lio/reactivex/Flowable;",
        "getOutputs",
        "()Lio/reactivex/Flowable;",
        "renderingsAndSnapshots",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/RenderingAndSnapshot;",
        "getRenderingsAndSnapshots",
        "()Lio/reactivex/Observable;",
        "cancel",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/workflow/rx2/RxWorkflowHost;


# direct methods
.method constructor <init>()V
    .locals 5

    .line 522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 526
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 527
    const-class v2, Lcom/squareup/workflow/rx2/RxWorkflowHost;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/rx2/RxWorkflowHost;

    iput-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$Companion$NoWorkflowSentinel$1;->$$delegate_0:Lcom/squareup/workflow/rx2/RxWorkflowHost;

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$Companion$NoWorkflowSentinel$1;->$$delegate_0:Lcom/squareup/workflow/rx2/RxWorkflowHost;

    invoke-interface {v0}, Lcom/squareup/workflow/rx2/RxWorkflowHost;->cancel()V

    return-void
.end method

.method public getOutputs()Lio/reactivex/Flowable;
    .locals 1

    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$Companion$NoWorkflowSentinel$1;->$$delegate_0:Lcom/squareup/workflow/rx2/RxWorkflowHost;

    invoke-interface {v0}, Lcom/squareup/workflow/rx2/RxWorkflowHost;->getOutputs()Lio/reactivex/Flowable;

    move-result-object v0

    return-object v0
.end method

.method public getRenderingsAndSnapshots()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/workflow/RenderingAndSnapshot;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$Companion$NoWorkflowSentinel$1;->$$delegate_0:Lcom/squareup/workflow/rx2/RxWorkflowHost;

    invoke-interface {v0}, Lcom/squareup/workflow/rx2/RxWorkflowHost;->getRenderingsAndSnapshots()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
