.class public Lcom/squareup/container/spot/Exchangers;
.super Ljava/lang/Object;
.source "Exchangers.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static fade(Z)Lcom/squareup/container/spot/ExchangeSet$Exchanger;
    .locals 1

    .line 116
    new-instance v0, Lcom/squareup/container/spot/-$$Lambda$Exchangers$HgSuKAwg5QMztgj8zy3DsHfVSHk;

    invoke-direct {v0, p0}, Lcom/squareup/container/spot/-$$Lambda$Exchangers$HgSuKAwg5QMztgj8zy3DsHfVSHk;-><init>(Z)V

    return-object v0
.end method

.method public static fadeIn()Lcom/squareup/container/spot/ExchangeSet$Exchanger;
    .locals 1

    const/4 v0, 0x1

    .line 46
    invoke-static {v0}, Lcom/squareup/container/spot/Exchangers;->fade(Z)Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v0

    return-object v0
.end method

.method public static fadeOut()Lcom/squareup/container/spot/ExchangeSet$Exchanger;
    .locals 1

    const/4 v0, 0x0

    .line 50
    invoke-static {v0}, Lcom/squareup/container/spot/Exchangers;->fade(Z)Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v0

    return-object v0
.end method

.method private static getTopRelativeToHost(Landroid/view/ViewGroup;Landroid/view/View;)I
    .locals 4

    .line 163
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 165
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    const/4 v1, 0x0

    .line 167
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p1

    add-int/2addr v1, p1

    .line 169
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    .line 170
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_0

    return v1

    :cond_0
    move-object v3, v0

    move-object v0, p1

    move-object p1, v3

    goto :goto_0
.end method

.method public static hide()Lcom/squareup/container/spot/ExchangeSet$Exchanger;
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/container/spot/-$$Lambda$Exchangers$R4hHcBWve6SfNCoURwmO6kZJB34;->INSTANCE:Lcom/squareup/container/spot/-$$Lambda$Exchangers$R4hHcBWve6SfNCoURwmO6kZJB34;

    return-object v0
.end method

.method static synthetic lambda$fade$3(ZLandroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Landroid/animation/AnimatorSet$Builder;I)V
    .locals 4

    .line 117
    sget-object p1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 p3, 0x2

    new-array p3, p3, [F

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    if-eqz p0, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/high16 v2, 0x3f800000    # 1.0f

    :goto_0
    const/4 v3, 0x0

    aput v2, p3, v3

    const/4 v2, 0x1

    if-eqz p0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :cond_1
    aput v0, p3, v2

    invoke-static {p2, p1, p3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p1

    if-eqz p0, :cond_2

    .line 119
    new-instance p0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {p1, p0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    goto :goto_1

    .line 121
    :cond_2
    new-instance p0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {p0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {p1, p0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    :goto_1
    int-to-long p2, p5

    .line 123
    invoke-virtual {p1, p2, p3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 124
    invoke-virtual {p4, p1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    return-void
.end method

.method static synthetic lambda$hide$0(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Landroid/animation/AnimatorSet$Builder;I)V
    .locals 1

    .line 55
    sget-object p0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 p2, 0x1

    new-array p2, p2, [F

    const/4 p4, 0x0

    const/4 v0, 0x0

    aput v0, p2, p4

    invoke-static {p1, p0, p2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p0

    const-wide/16 p1, 0x0

    .line 56
    invoke-virtual {p0, p1, p2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 57
    invoke-virtual {p3, p0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    return-void
.end method

.method static synthetic lambda$translateX$1(ZZLandroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Landroid/animation/AnimatorSet$Builder;I)V
    .locals 1

    .line 63
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getWidth()I

    move-result p2

    const/4 p4, 0x0

    if-eqz p0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    neg-int p2, p2

    goto :goto_1

    :cond_1
    if-eqz p1, :cond_2

    neg-int p2, p2

    :goto_0
    const/4 p0, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    move p0, p2

    const/4 p2, 0x0

    .line 83
    :goto_2
    sget-object p1, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    const/4 v0, 0x2

    new-array v0, v0, [F

    int-to-float p2, p2

    aput p2, v0, p4

    const/4 p2, 0x1

    int-to-float p0, p0

    aput p0, v0, p2

    invoke-static {p3, p1, v0}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p0

    int-to-long p1, p6

    .line 84
    invoke-virtual {p0, p1, p2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 85
    invoke-virtual {p5, p0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    return-void
.end method

.method static synthetic lambda$translateY$2(ZLandroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Landroid/animation/AnimatorSet$Builder;I)V
    .locals 4

    .line 92
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result p1

    const/4 p3, 0x0

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    move p0, p1

    const/4 p1, 0x0

    .line 103
    :goto_0
    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v2, v1, [F

    int-to-float p1, p1

    aput p1, v2, p3

    const/4 p1, 0x1

    int-to-float p0, p0

    aput p0, v2, p1

    .line 104
    invoke-static {p2, v0, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p0

    int-to-long v2, p5

    .line 105
    invoke-virtual {p0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 106
    invoke-virtual {p4, p0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 108
    sget-object p0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array p1, v1, [F

    fill-array-data p1, :array_0

    invoke-static {p2, p0, p1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p0

    .line 109
    invoke-virtual {p0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 110
    new-instance p1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {p0, p1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 111
    invoke-virtual {p4, p0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic lambda$tweenYAndFade$4(ZLandroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Landroid/animation/AnimatorSet$Builder;I)V
    .locals 7

    .line 130
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v1, 0x3

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    if-eqz p0, :cond_0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    const/high16 v4, 0x3f800000    # 1.0f

    :goto_0
    const/4 v5, 0x0

    aput v4, v1, v5

    if-eqz p0, :cond_1

    const/high16 v4, 0x3e800000    # 0.25f

    goto :goto_1

    :cond_1
    const/high16 v4, 0x3f400000    # 0.75f

    :goto_1
    const/4 v6, 0x1

    aput v4, v1, v6

    if-eqz p0, :cond_2

    const/high16 v2, 0x3f800000    # 1.0f

    :cond_2
    const/4 v3, 0x2

    aput v2, v1, v3

    invoke-static {p2, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 134
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    int-to-long v1, p5

    .line 135
    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 136
    invoke-virtual {p4, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    if-nez p3, :cond_3

    return-void

    .line 143
    :cond_3
    invoke-static {p1, p3}, Lcom/squareup/container/spot/Exchangers;->getTopRelativeToHost(Landroid/view/ViewGroup;Landroid/view/View;)I

    move-result p3

    .line 144
    invoke-static {p1, p2}, Lcom/squareup/container/spot/Exchangers;->getTopRelativeToHost(Landroid/view/ViewGroup;Landroid/view/View;)I

    move-result p1

    sub-int p1, p3, p1

    if-eqz p0, :cond_4

    move p3, p1

    goto :goto_2

    :cond_4
    const/4 p3, 0x0

    :goto_2
    if-eqz p0, :cond_5

    const/4 p1, 0x0

    .line 149
    :cond_5
    sget-object p0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array p5, v3, [F

    int-to-float p3, p3

    aput p3, p5, v5

    int-to-float p1, p1

    aput p1, p5, v6

    invoke-static {p2, p0, p5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p0

    .line 151
    new-instance p1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {p0, p1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 152
    invoke-virtual {p0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 153
    invoke-virtual {p4, p0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    return-void
.end method

.method public static slideInFromLeft()Lcom/squareup/container/spot/ExchangeSet$Exchanger;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 22
    invoke-static {v0, v1}, Lcom/squareup/container/spot/Exchangers;->translateX(ZZ)Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v0

    return-object v0
.end method

.method public static slideInFromRight()Lcom/squareup/container/spot/ExchangeSet$Exchanger;
    .locals 1

    const/4 v0, 0x1

    .line 26
    invoke-static {v0, v0}, Lcom/squareup/container/spot/Exchangers;->translateX(ZZ)Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v0

    return-object v0
.end method

.method public static slideOutToLeft()Lcom/squareup/container/spot/ExchangeSet$Exchanger;
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 30
    invoke-static {v0, v1}, Lcom/squareup/container/spot/Exchangers;->translateX(ZZ)Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v0

    return-object v0
.end method

.method public static slideOutToRight()Lcom/squareup/container/spot/ExchangeSet$Exchanger;
    .locals 1

    const/4 v0, 0x0

    .line 34
    invoke-static {v0, v0}, Lcom/squareup/container/spot/Exchangers;->translateX(ZZ)Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v0

    return-object v0
.end method

.method private static translateX(ZZ)Lcom/squareup/container/spot/ExchangeSet$Exchanger;
    .locals 1

    .line 62
    new-instance v0, Lcom/squareup/container/spot/-$$Lambda$Exchangers$zeFiKFNrL2CbM8Eo51sr3XXmh0I;

    invoke-direct {v0, p0, p1}, Lcom/squareup/container/spot/-$$Lambda$Exchangers$zeFiKFNrL2CbM8Eo51sr3XXmh0I;-><init>(ZZ)V

    return-object v0
.end method

.method private static translateY(Z)Lcom/squareup/container/spot/ExchangeSet$Exchanger;
    .locals 1

    .line 91
    new-instance v0, Lcom/squareup/container/spot/-$$Lambda$Exchangers$9GKD30HTsgX4E1099ZHR8n4G8YI;

    invoke-direct {v0, p0}, Lcom/squareup/container/spot/-$$Lambda$Exchangers$9GKD30HTsgX4E1099ZHR8n4G8YI;-><init>(Z)V

    return-object v0
.end method

.method private static tweenYAndFade(Z)Lcom/squareup/container/spot/ExchangeSet$Exchanger;
    .locals 1

    .line 129
    new-instance v0, Lcom/squareup/container/spot/-$$Lambda$Exchangers$aZ_RvxGv3pUVtsEqbPl0f_vzAHU;

    invoke-direct {v0, p0}, Lcom/squareup/container/spot/-$$Lambda$Exchangers$aZ_RvxGv3pUVtsEqbPl0f_vzAHU;-><init>(Z)V

    return-object v0
.end method

.method public static tweenYAndFadeIn()Lcom/squareup/container/spot/ExchangeSet$Exchanger;
    .locals 1

    const/4 v0, 0x1

    .line 38
    invoke-static {v0}, Lcom/squareup/container/spot/Exchangers;->tweenYAndFade(Z)Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v0

    return-object v0
.end method

.method public static tweenYAndFadeOut()Lcom/squareup/container/spot/ExchangeSet$Exchanger;
    .locals 1

    const/4 v0, 0x0

    .line 42
    invoke-static {v0}, Lcom/squareup/container/spot/Exchangers;->tweenYAndFade(Z)Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v0

    return-object v0
.end method
