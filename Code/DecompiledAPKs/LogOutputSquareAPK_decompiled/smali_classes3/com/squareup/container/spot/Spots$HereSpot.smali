.class Lcom/squareup/container/spot/Spots$HereSpot;
.super Lcom/squareup/container/spot/Spot;
.source "Spots.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/spot/Spots;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HereSpot"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/container/spot/Spots$HereSpot;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/container/spot/Spots$HereSpot;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 165
    new-instance v0, Lcom/squareup/container/spot/Spots$HereSpot;

    invoke-direct {v0}, Lcom/squareup/container/spot/Spots$HereSpot;-><init>()V

    sput-object v0, Lcom/squareup/container/spot/Spots$HereSpot;->INSTANCE:Lcom/squareup/container/spot/Spots$HereSpot;

    .line 186
    sget-object v0, Lcom/squareup/container/spot/Spots$HereSpot;->INSTANCE:Lcom/squareup/container/spot/Spots$HereSpot;

    invoke-static {v0}, Lcom/squareup/container/spot/Spots$HereSpot;->forSpotSingleton(Lcom/squareup/container/spot/Spot;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/container/spot/Spots$HereSpot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 167
    invoke-direct {p0}, Lcom/squareup/container/spot/Spot;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/container/spot/Spots$1;)V
    .locals 0

    .line 164
    invoke-direct {p0}, Lcom/squareup/container/spot/Spots$HereSpot;-><init>()V

    return-void
.end method


# virtual methods
.method protected backwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method protected backwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method protected forwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method protected forwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    return-void
.end method
