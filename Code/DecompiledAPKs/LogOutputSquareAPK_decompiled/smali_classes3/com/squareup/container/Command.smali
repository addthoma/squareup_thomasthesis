.class public final Lcom/squareup/container/Command;
.super Ljava/lang/Object;
.source "Command.java"


# instance fields
.field final direction:Lflow/Direction;

.field final history:Lflow/History;


# direct methods
.method private constructor <init>(Lflow/History;Lflow/Direction;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/container/Command;->history:Lflow/History;

    .line 63
    iput-object p2, p0, Lcom/squareup/container/Command;->direction:Lflow/Direction;

    return-void
.end method

.method public static goBack(Lflow/History;)Lcom/squareup/container/Command;
    .locals 3

    .line 53
    invoke-virtual {p0}, Lflow/History;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 57
    invoke-virtual {p0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p0

    invoke-virtual {p0, v1}, Lflow/History$Builder;->pop(I)Lflow/History$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    .line 58
    sget-object v0, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-static {p0, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0

    .line 54
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot go back from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static pushStack(Lflow/History;Ljava/util/List;)Lcom/squareup/container/Command;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;)",
            "Lcom/squareup/container/Command;"
        }
    .end annotation

    .line 35
    invoke-static {p0, p1}, Lcom/squareup/container/Histories;->pushStack(Lflow/History;Ljava/util/List;)Lkotlin/Pair;

    move-result-object p0

    .line 36
    invoke-virtual {p0}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/History;

    invoke-virtual {p0}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lflow/Direction;

    invoke-static {p1, p0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0
.end method

.method public static set(Lflow/History;Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/Command;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    sget-object p1, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p0, p1}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0

    .line 30
    :cond_0
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/container/Command;->pushStack(Lflow/History;Ljava/util/List;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0
.end method

.method public static setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;
    .locals 2

    .line 43
    new-instance v0, Lcom/squareup/container/Command;

    const-string v1, "history"

    invoke-static {p0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lflow/History;

    const-string v1, "direction"

    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/Direction;

    invoke-direct {v0, p0, p1}, Lcom/squareup/container/Command;-><init>(Lflow/History;Lflow/Direction;)V

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 67
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/container/Command;->direction:Lflow/Direction;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/container/Command;->history:Lflow/History;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "%s: %s to %s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
