.class public final Lcom/squareup/container/AbstractV1PosWorkflowRunner$dedupForLegacyWorkflows$1;
.super Ljava/lang/Object;
.source "AbstractV1PosWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/rx2/RxWorkflowHost;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/AbstractV1PosWorkflowRunner;->dedupForLegacyWorkflows(Lcom/squareup/workflow/rx2/RxWorkflowHost;)Lcom/squareup/workflow/rx2/RxWorkflowHost;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/rx2/RxWorkflowHost<",
        "TO;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000%\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0001J\u0008\u0010\u000c\u001a\u00020\rH\u0016R\u001c\u0010\u0002\u001a\n\u0012\u0006\u0008\u0001\u0012\u00028\u00000\u00038VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005RT\u0010\u0006\u001aB\u0012\u001a\u0008\u0001\u0012\u0016\u0012\u0004\u0012\u00028\u0001 \t*\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00080\u0008 \t* \u0012\u001a\u0008\u0001\u0012\u0016\u0012\u0004\u0012\u00028\u0001 \t*\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00080\u0008\u0018\u00010\u00070\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u000e"
    }
    d2 = {
        "com/squareup/container/AbstractV1PosWorkflowRunner$dedupForLegacyWorkflows$1",
        "Lcom/squareup/workflow/rx2/RxWorkflowHost;",
        "outputs",
        "Lio/reactivex/Flowable;",
        "getOutputs",
        "()Lio/reactivex/Flowable;",
        "renderingsAndSnapshots",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/RenderingAndSnapshot;",
        "kotlin.jvm.PlatformType",
        "getRenderingsAndSnapshots",
        "()Lio/reactivex/Observable;",
        "cancel",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_dedupForLegacyWorkflows:Lcom/squareup/workflow/rx2/RxWorkflowHost;

.field private final renderingsAndSnapshots:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/workflow/RenderingAndSnapshot<",
            "TR;>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/workflow/rx2/RxWorkflowHost;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/rx2/RxWorkflowHost<",
            "+TO;TR;>;)V"
        }
    .end annotation

    .line 98
    iput-object p1, p0, Lcom/squareup/container/AbstractV1PosWorkflowRunner$dedupForLegacyWorkflows$1;->$this_dedupForLegacyWorkflows:Lcom/squareup/workflow/rx2/RxWorkflowHost;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iget-object p1, p0, Lcom/squareup/container/AbstractV1PosWorkflowRunner$dedupForLegacyWorkflows$1;->$this_dedupForLegacyWorkflows:Lcom/squareup/workflow/rx2/RxWorkflowHost;

    invoke-interface {p1}, Lcom/squareup/workflow/rx2/RxWorkflowHost;->getRenderingsAndSnapshots()Lio/reactivex/Observable;

    move-result-object p1

    .line 100
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/container/AbstractV1PosWorkflowRunner$dedupForLegacyWorkflows$1;->renderingsAndSnapshots:Lio/reactivex/Observable;

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/container/AbstractV1PosWorkflowRunner$dedupForLegacyWorkflows$1;->$this_dedupForLegacyWorkflows:Lcom/squareup/workflow/rx2/RxWorkflowHost;

    invoke-interface {v0}, Lcom/squareup/workflow/rx2/RxWorkflowHost;->cancel()V

    return-void
.end method

.method public getOutputs()Lio/reactivex/Flowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Flowable<",
            "+TO;>;"
        }
    .end annotation

    .line 102
    iget-object v0, p0, Lcom/squareup/container/AbstractV1PosWorkflowRunner$dedupForLegacyWorkflows$1;->$this_dedupForLegacyWorkflows:Lcom/squareup/workflow/rx2/RxWorkflowHost;

    invoke-interface {v0}, Lcom/squareup/workflow/rx2/RxWorkflowHost;->getOutputs()Lio/reactivex/Flowable;

    move-result-object v0

    return-object v0
.end method

.method public getRenderingsAndSnapshots()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/workflow/RenderingAndSnapshot<",
            "TR;>;>;"
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/container/AbstractV1PosWorkflowRunner$dedupForLegacyWorkflows$1;->renderingsAndSnapshots:Lio/reactivex/Observable;

    return-object v0
.end method
