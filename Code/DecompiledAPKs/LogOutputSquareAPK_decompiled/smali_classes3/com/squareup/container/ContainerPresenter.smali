.class public abstract Lcom/squareup/container/ContainerPresenter;
.super Lmortar/Presenter;
.source "ContainerPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/squareup/container/ContainerView;",
        ">",
        "Lmortar/Presenter<",
        "TV;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nContainerPresenter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ContainerPresenter.kt\ncom/squareup/container/ContainerPresenter\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 ArraysJVM.kt\nkotlin/collections/ArraysKt__ArraysJVMKt\n*L\n1#1,594:1\n473#2,6:595\n1587#2,3:601\n1642#2,2:604\n1642#2,2:606\n1577#2,4:608\n310#2,7:614\n37#3,2:612\n*E\n*S KotlinDebug\n*F\n+ 1 ContainerPresenter.kt\ncom/squareup/container/ContainerPresenter\n*L\n133#1,6:595\n217#1,3:601\n239#1,2:604\n353#1,2:606\n476#1,4:608\n490#1,7:614\n486#1,2:612\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00e6\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0011\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u001e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008&\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003B!\u0008\u0004\u0012\u0018\u0008\u0002\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u0008\u00a2\u0006\u0002\u0010\tJ\u0018\u0010/\u001a\u0002002\u0006\u00101\u001a\u0002022\u0006\u00103\u001a\u000204H\u0002J\u0016\u00105\u001a\u00020\u00072\u000c\u00106\u001a\u0008\u0012\u0004\u0012\u00020807H\u0014J\u0012\u00109\u001a\u000c\u0012\u0004\u0012\u00020:0\"j\u0002`;H\u0014J\u0018\u0010<\u001a\u00020\u00072\u0006\u00101\u001a\u0002022\u0006\u0010=\u001a\u000204H\u0002J\u0008\u0010>\u001a\u00020\u0007H\u0004J\u0015\u0010?\u001a\u00020\u00072\u0006\u0010@\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010AJ\u0015\u0010B\u001a\u00020C2\u0006\u0010@\u001a\u00028\u0000H\u0014\u00a2\u0006\u0002\u0010DJ \u0010E\u001a\u0004\u0018\u00010F2\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020#0\"2\u0006\u0010G\u001a\u00020\u0015H\u0002J\u001b\u0010H\u001a\u0008\u0012\u0004\u0012\u00020\'0,2\u0006\u0010I\u001a\u00020JH\u0002\u00a2\u0006\u0002\u0010KJ\u0010\u0010L\u001a\u00020F2\u0006\u0010M\u001a\u00020NH\u0014J\u0010\u0010O\u001a\u00020J2\u0006\u0010P\u001a\u00020QH\u0002J\u0008\u0010R\u001a\u00020SH\u0002J\u0006\u0010T\u001a\u00020\u0007J\u0008\u0010U\u001a\u00020\u0007H\u0016J\u0010\u0010V\u001a\u00020\u00072\u0006\u0010W\u001a\u00020\u0011H\u0015J\u0012\u0010X\u001a\u00020\u00072\u0008\u0010Y\u001a\u0004\u0018\u00010ZH\u0014J\u0010\u0010[\u001a\u00020\u00072\u0006\u0010\\\u001a\u00020ZH\u0014J\u0018\u0010]\u001a\u00020\u00072\u0006\u00101\u001a\u0002022\u0006\u0010=\u001a\u000204H\u0002J\u0016\u0010^\u001a\u0008\u0012\u0004\u0012\u00020#072\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010_\u001a\u0002022\u0006\u00101\u001a\u000202H\u0002J\u0008\u0010`\u001a\u00020\u0007H\u0002J\u001c\u0010a\u001a\u0008\u0012\u0004\u0012\u00020#0\"2\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020#0\"H\u0002J\u001c\u0010b\u001a\u0008\u0012\u0004\u0012\u00020Q0c2\u0006\u0010d\u001a\u00020\u00152\u0006\u0010e\u001a\u00020\u0015J!\u0010f\u001a\u0002Hg\"\u0004\u0008\u0001\u0010g2\u000c\u0010h\u001a\u0008\u0012\u0004\u0012\u0002Hg0iH\u0002\u00a2\u0006\u0002\u0010jR\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u00020\rX\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0014\u001a\u00020\u0015X\u00a4\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0017R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u001a\u001a\u0004\u0018\u00010\u000b8TX\u0094\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001b\u0010\u001cR\u0014\u0010\u001d\u001a\u00020\u001e8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001f\u0010 R\u0016\u0010!\u001a\n\u0012\u0004\u0012\u00020#\u0018\u00010\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010&\u001a\u0004\u0018\u00010\'X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010(\u001a\u00020#8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008)\u0010*R\u001a\u0010+\u001a\u0008\u0012\u0004\u0012\u00020\'0,8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008-\u0010.\u00a8\u0006k"
    }
    d2 = {
        "Lcom/squareup/container/ContainerPresenter;",
        "V",
        "Lcom/squareup/container/ContainerView;",
        "Lmortar/Presenter;",
        "defaultBackPressedHandler",
        "Lkotlin/Function1;",
        "Landroid/app/Activity;",
        "",
        "Lcom/squareup/container/DefaultBackPressedHandler;",
        "(Lkotlin/jvm/functions/Function1;)V",
        "_flow",
        "Lflow/Flow;",
        "additionalContainerLayerSetup",
        "Lcom/squareup/container/AdditionalContainerLayerSetup;",
        "getAdditionalContainerLayerSetup",
        "()Lcom/squareup/container/AdditionalContainerLayerSetup;",
        "containerScope",
        "Lmortar/MortarScope;",
        "contextFactory",
        "Lflow/path/PathContextFactory;",
        "defaultHistory",
        "Lflow/History;",
        "getDefaultHistory",
        "()Lflow/History;",
        "dispatcher",
        "Lflow/Dispatcher;",
        "flow",
        "getFlow",
        "()Lflow/Flow;",
        "hasMasterLayout",
        "",
        "getHasMasterLayout",
        "()Z",
        "layers",
        "",
        "Lcom/squareup/container/layer/ContainerLayer;",
        "orientationLock",
        "Lcom/squareup/container/OrientationLock;",
        "orphanContext",
        "Lflow/path/PathContext;",
        "topVisibleLayer",
        "getTopVisibleLayer",
        "()Lcom/squareup/container/layer/ContainerLayer;",
        "visiblePathContexts",
        "",
        "getVisiblePathContexts",
        "()[Lflow/path/PathContext;",
        "blockEventsAndHandleOrientationLock",
        "Lcom/squareup/container/DispatchStep$Result;",
        "traversal",
        "Lflow/Traversal;",
        "originalCallback",
        "Lflow/TraversalCallback;",
        "buildDispatchPipeline",
        "pipeline",
        "",
        "Lcom/squareup/container/DispatchStep;",
        "buildRedirectPipeline",
        "Lcom/squareup/container/RedirectStep;",
        "Lcom/squareup/container/RedirectPipeline;",
        "dispatch",
        "callback",
        "doFinish",
        "dropView",
        "view",
        "(Lcom/squareup/container/ContainerView;)V",
        "extractBundleService",
        "Lmortar/bundler/BundleService;",
        "(Lcom/squareup/container/ContainerView;)Lmortar/bundler/BundleService;",
        "findPathToLock",
        "Lflow/path/Path;",
        "history",
        "getVisiblePathContextsAround",
        "indexOfInterest",
        "",
        "(I)[Lflow/path/PathContext;",
        "initialDetailScreenForAnnotation",
        "annotation",
        "Lcom/squareup/container/layer/Master;",
        "layerIndex",
        "key",
        "Lcom/squareup/container/ContainerTreeKey;",
        "makeRedirectFactory",
        "Lcom/squareup/container/RedirectPipelineFactory;",
        "onActivityFinish",
        "onBackPressed",
        "onEnterScope",
        "scope",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onSave",
        "outState",
        "performTransition",
        "prepareLayers",
        "preprocessFlowTraversal",
        "removeDispatcher",
        "sealLayers",
        "topScreensByLayer",
        "",
        "traversalCompleting",
        "nextHistory",
        "topViewAs",
        "T",
        "type",
        "Ljava/lang/Class;",
        "(Ljava/lang/Class;)Ljava/lang/Object;",
        "container_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private _flow:Lflow/Flow;

.field private final additionalContainerLayerSetup:Lcom/squareup/container/AdditionalContainerLayerSetup;

.field private containerScope:Lmortar/MortarScope;

.field private final contextFactory:Lflow/path/PathContextFactory;

.field private final defaultBackPressedHandler:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/app/Activity;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private dispatcher:Lflow/Dispatcher;

.field private layers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/container/layer/ContainerLayer;",
            ">;"
        }
    .end annotation
.end field

.field private orientationLock:Lcom/squareup/container/OrientationLock;

.field private orphanContext:Lflow/path/PathContext;


# direct methods
.method protected constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/squareup/container/ContainerPresenter;-><init>(Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method protected constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/app/Activity;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "defaultBackPressedHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    iput-object p1, p0, Lcom/squareup/container/ContainerPresenter;->defaultBackPressedHandler:Lkotlin/jvm/functions/Function1;

    .line 127
    sget-object p1, Lcom/squareup/container/AdditionalContainerLayerSetup$None;->INSTANCE:Lcom/squareup/container/AdditionalContainerLayerSetup$None;

    check-cast p1, Lcom/squareup/container/AdditionalContainerLayerSetup;

    iput-object p1, p0, Lcom/squareup/container/ContainerPresenter;->additionalContainerLayerSetup:Lcom/squareup/container/AdditionalContainerLayerSetup;

    .line 137
    new-instance p1, Lcom/squareup/container/ContainerPresenter$1;

    invoke-direct {p1, p0}, Lcom/squareup/container/ContainerPresenter$1;-><init>(Lcom/squareup/container/ContainerPresenter;)V

    check-cast p1, Lcom/squareup/container/ContainerTreeKeyContextFactory$FlowProvider;

    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKeyContextFactory;->forContainerPresenter(Lcom/squareup/container/ContainerTreeKeyContextFactory$FlowProvider;)Lcom/squareup/container/ContainerTreeKeyContextFactory;

    move-result-object p1

    check-cast p1, Lflow/path/PathContextFactory;

    invoke-static {p1}, Lflow/path/Path;->contextFactory(Lflow/path/PathContextFactory;)Lflow/path/PathContextFactory;

    move-result-object p1

    const-string v0, "Path.contextFactory(Cont\u2026tainerPresenter { flow })"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/container/ContainerPresenter;->contextFactory:Lflow/path/PathContextFactory;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 97
    invoke-static {}, Lcom/squareup/container/DefaultBackPressedHandlerKt;->getFinishAfterTransition()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/container/ContainerPresenter;-><init>(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final synthetic access$blockEventsAndHandleOrientationLock(Lcom/squareup/container/ContainerPresenter;Lflow/Traversal;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;
    .locals 0

    .line 95
    invoke-direct {p0, p1, p2}, Lcom/squareup/container/ContainerPresenter;->blockEventsAndHandleOrientationLock(Lflow/Traversal;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$dispatch(Lcom/squareup/container/ContainerPresenter;Lflow/Traversal;Lflow/TraversalCallback;)V
    .locals 0

    .line 95
    invoke-direct {p0, p1, p2}, Lcom/squareup/container/ContainerPresenter;->dispatch(Lflow/Traversal;Lflow/TraversalCallback;)V

    return-void
.end method

.method public static final synthetic access$getContextFactory$p(Lcom/squareup/container/ContainerPresenter;)Lflow/path/PathContextFactory;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/container/ContainerPresenter;->contextFactory:Lflow/path/PathContextFactory;

    return-object p0
.end method

.method public static final synthetic access$getLayers$p(Lcom/squareup/container/ContainerPresenter;)Ljava/util/List;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/container/ContainerPresenter;->layers:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getOrientationLock$p(Lcom/squareup/container/ContainerPresenter;)Lcom/squareup/container/OrientationLock;
    .locals 1

    .line 95
    iget-object p0, p0, Lcom/squareup/container/ContainerPresenter;->orientationLock:Lcom/squareup/container/OrientationLock;

    if-nez p0, :cond_0

    const-string v0, "orientationLock"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getOrphanContext$p(Lcom/squareup/container/ContainerPresenter;)Lflow/path/PathContext;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/container/ContainerPresenter;->orphanContext:Lflow/path/PathContext;

    return-object p0
.end method

.method public static final synthetic access$getView(Lcom/squareup/container/ContainerPresenter;)Lcom/squareup/container/ContainerView;
    .locals 0

    .line 95
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/container/ContainerView;

    return-object p0
.end method

.method public static final synthetic access$getVisiblePathContexts$p(Lcom/squareup/container/ContainerPresenter;)[Lflow/path/PathContext;
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/container/ContainerPresenter;->getVisiblePathContexts()[Lflow/path/PathContext;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$layerIndex(Lcom/squareup/container/ContainerPresenter;Lcom/squareup/container/ContainerTreeKey;)I
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/container/ContainerPresenter;->layerIndex(Lcom/squareup/container/ContainerTreeKey;)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$preprocessFlowTraversal(Lcom/squareup/container/ContainerPresenter;Lflow/Traversal;)Lflow/Traversal;
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/container/ContainerPresenter;->preprocessFlowTraversal(Lflow/Traversal;)Lflow/Traversal;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setLayers$p(Lcom/squareup/container/ContainerPresenter;Ljava/util/List;)V
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/container/ContainerPresenter;->layers:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$setOrientationLock$p(Lcom/squareup/container/ContainerPresenter;Lcom/squareup/container/OrientationLock;)V
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/container/ContainerPresenter;->orientationLock:Lcom/squareup/container/OrientationLock;

    return-void
.end method

.method public static final synthetic access$setOrphanContext$p(Lcom/squareup/container/ContainerPresenter;Lflow/path/PathContext;)V
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/container/ContainerPresenter;->orphanContext:Lflow/path/PathContext;

    return-void
.end method

.method private final blockEventsAndHandleOrientationLock(Lflow/Traversal;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;
    .locals 4

    .line 515
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerView;

    const/4 v1, 0x1

    .line 516
    invoke-interface {v0, v1}, Lcom/squareup/container/ContainerView;->interceptInputEvents(Z)V

    .line 518
    iget-object v2, p0, Lcom/squareup/container/ContainerPresenter;->layers:Ljava/util/List;

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object p1, p1, Lflow/Traversal;->destination:Lflow/History;

    const-string/jumbo v3, "traversal.destination"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2, p1}, Lcom/squareup/container/ContainerPresenter;->findPathToLock(Ljava/util/List;Lflow/History;)Lflow/path/Path;

    move-result-object p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_4

    .line 521
    iget-object v2, p0, Lcom/squareup/container/ContainerPresenter;->orientationLock:Lcom/squareup/container/OrientationLock;

    if-nez v2, :cond_2

    const-string v3, "orientationLock"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v2, p1}, Lcom/squareup/container/OrientationLock;->requestLock(Lflow/path/Path;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 537
    invoke-direct {p0}, Lcom/squareup/container/ContainerPresenter;->getTopVisibleLayer()Lcom/squareup/container/layer/ContainerLayer;

    move-result-object p1

    .line 538
    invoke-interface {p1}, Lcom/squareup/container/layer/ContainerLayer;->isShowing()Z

    move-result p2

    if-eqz p2, :cond_3

    .line 539
    invoke-interface {p1}, Lcom/squareup/container/layer/ContainerLayer;->getVisibleContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lflow/path/PathContext;->get(Landroid/content/Context;)Lflow/path/PathContext;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/container/ContainerPresenter;->orphanContext:Lflow/path/PathContext;

    :cond_3
    const-string p1, "Abort for configuration change"

    .line 542
    invoke-static {p1}, Lcom/squareup/container/DispatchStep$Result;->stop(Ljava/lang/String;)Lcom/squareup/container/DispatchStep$Result;

    move-result-object p1

    const-string p2, "stop(\"Abort for configuration change\")"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 547
    :cond_4
    new-instance p1, Lcom/squareup/container/ContainerPresenter$blockEventsAndHandleOrientationLock$1;

    invoke-direct {p1, p0, v0, p2, v1}, Lcom/squareup/container/ContainerPresenter$blockEventsAndHandleOrientationLock$1;-><init>(Lcom/squareup/container/ContainerPresenter;Lcom/squareup/container/ContainerView;Lflow/TraversalCallback;Z)V

    check-cast p1, Lflow/TraversalCallback;

    const-string p2, "after view transition"

    invoke-static {p2, p1}, Lcom/squareup/container/DispatchStep$Result;->wrap(Ljava/lang/String;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;

    move-result-object p1

    const-string/jumbo p2, "wrap(\"after view transit\u2026ientation()\n      }\n    }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final dispatch(Lflow/Traversal;Lflow/TraversalCallback;)V
    .locals 4

    .line 213
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 214
    invoke-virtual {p0, v0}, Lcom/squareup/container/ContainerPresenter;->buildDispatchPipeline(Ljava/util/List;)V

    .line 217
    move-object v1, p0

    check-cast v1, Lcom/squareup/container/ContainerPresenter;

    .line 218
    check-cast v0, Ljava/lang/Iterable;

    .line 602
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/container/DispatchStep;

    .line 219
    invoke-interface {v2, p1, p2}, Lcom/squareup/container/DispatchStep;->dispatch(Lflow/Traversal;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;

    move-result-object v2

    const-string v3, "step.dispatch(traversal, currentCallback)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    invoke-virtual {v2, p2}, Lcom/squareup/container/DispatchStep$Result;->redirectOrContinue(Lflow/TraversalCallback;)Lflow/TraversalCallback;

    move-result-object p2

    if-eqz p2, :cond_1

    goto :goto_0

    .line 227
    :cond_0
    invoke-direct {v1, p1, p2}, Lcom/squareup/container/ContainerPresenter;->performTransition(Lflow/Traversal;Lflow/TraversalCallback;)V

    :cond_1
    return-void
.end method

.method private final findPathToLock(Ljava/util/List;Lflow/History;)Lflow/path/Path;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/container/layer/ContainerLayer;",
            ">;",
            "Lflow/History;",
            ")",
            "Lflow/path/Path;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 570
    check-cast v0, Lflow/path/Path;

    .line 571
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/container/layer/ContainerLayer;

    .line 572
    invoke-interface {v1, p2}, Lcom/squareup/container/layer/ContainerLayer;->findTopmostScreenToShow(Lflow/History;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 573
    iget-object v2, p0, Lcom/squareup/container/ContainerPresenter;->orientationLock:Lcom/squareup/container/OrientationLock;

    const-string v3, "orientationLock"

    if-nez v2, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v1, Lflow/path/Path;

    invoke-virtual {v2, v1}, Lcom/squareup/container/OrientationLock;->shouldLock(Lflow/path/Path;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    .line 577
    :cond_2
    iget-object v2, p0, Lcom/squareup/container/ContainerPresenter;->orientationLock:Lcom/squareup/container/OrientationLock;

    if-nez v2, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v2, v1}, Lcom/squareup/container/OrientationLock;->getOrientationForPath(Lflow/path/Path;)Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    move-result-object v1

    .line 578
    iget-object v2, p0, Lcom/squareup/container/ContainerPresenter;->orientationLock:Lcom/squareup/container/OrientationLock;

    if-nez v2, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v2, v0}, Lcom/squareup/container/OrientationLock;->getOrientationForPath(Lflow/path/Path;)Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    move-result-object v2

    if-ne v1, v2, :cond_5

    goto :goto_0

    .line 583
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 584
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "There should not be layers requiring conflicting orientation locks. "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x28

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 585
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " / "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v0, 0x29

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 583
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_6
    return-object v0
.end method

.method private final getHasMasterLayout()Z
    .locals 2

    .line 509
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerView;

    const-string/jumbo v1, "view"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/squareup/container/ContainerView;->getMasterLayout()Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final getTopVisibleLayer()Lcom/squareup/container/layer/ContainerLayer;
    .locals 3

    .line 133
    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter;->layers:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 595
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    .line 596
    :cond_1
    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 597
    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v1

    .line 598
    move-object v2, v1

    check-cast v2, Lcom/squareup/container/layer/ContainerLayer;

    .line 133
    invoke-interface {v2}, Lcom/squareup/container/layer/ContainerLayer;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 600
    :goto_0
    check-cast v1, Lcom/squareup/container/layer/ContainerLayer;

    if-eqz v1, :cond_3

    goto :goto_1

    .line 133
    :cond_3
    invoke-static {}, Lcom/squareup/container/ContainerPresenterKt;->access$getNULL_LAYER$p()Lcom/squareup/container/ContainerPresenterKt$NULL_LAYER$1;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/container/layer/ContainerLayer;

    :goto_1
    return-object v1
.end method

.method private final getVisiblePathContexts()[Lflow/path/PathContext;
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter;->layers:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/container/ContainerPresenter;->getVisiblePathContextsAround(I)[Lflow/path/PathContext;

    move-result-object v0

    return-object v0
.end method

.method private final getVisiblePathContextsAround(I)[Lflow/path/PathContext;
    .locals 2

    .line 481
    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter;->layers:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 483
    new-instance v1, Lcom/squareup/container/ContainerPresenter$getVisiblePathContextsAround$1;

    invoke-direct {v1, p1}, Lcom/squareup/container/ContainerPresenter$getVisiblePathContextsAround$1;-><init>(I)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {v0, v1}, Lkotlin/sequences/SequencesKt;->filterIndexed(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function2;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 484
    sget-object v0, Lcom/squareup/container/ContainerPresenter$getVisiblePathContextsAround$2;->INSTANCE:Lcom/squareup/container/ContainerPresenter$getVisiblePathContextsAround$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 485
    invoke-static {p1}, Lkotlin/sequences/SequencesKt;->toList(Lkotlin/sequences/Sequence;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    const/4 v0, 0x0

    new-array v0, v0, [Lflow/path/PathContext;

    .line 613
    invoke-interface {p1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, [Lflow/path/PathContext;

    return-object p1

    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final layerIndex(Lcom/squareup/container/ContainerTreeKey;)I
    .locals 5

    .line 490
    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter;->layers:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 615
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, -0x1

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 616
    check-cast v3, Lcom/squareup/container/layer/ContainerLayer;

    .line 490
    invoke-interface {v3, p1}, Lcom/squareup/container/layer/ContainerLayer;->owns(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, -0x1

    :goto_1
    if-eq v2, v4, :cond_3

    const/4 v1, 0x1

    :cond_3
    if-eqz v1, :cond_4

    return v2

    .line 491
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "key ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ") maps to no layer"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final makeRedirectFactory()Lcom/squareup/container/RedirectPipelineFactory;
    .locals 5

    .line 370
    new-instance v0, Lcom/squareup/container/RedirectPipelineFactory;

    .line 371
    new-instance v1, Lcom/squareup/container/ContainerPresenter$makeRedirectFactory$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/container/ContainerPresenter;

    invoke-direct {v1, v2}, Lcom/squareup/container/ContainerPresenter$makeRedirectFactory$1;-><init>(Lcom/squareup/container/ContainerPresenter;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 372
    invoke-direct {p0}, Lcom/squareup/container/ContainerPresenter;->getHasMasterLayout()Z

    move-result v3

    .line 373
    new-instance v4, Lcom/squareup/container/ContainerPresenter$makeRedirectFactory$2;

    invoke-direct {v4, v2}, Lcom/squareup/container/ContainerPresenter$makeRedirectFactory$2;-><init>(Lcom/squareup/container/ContainerPresenter;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 374
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getAdditionalContainerLayerSetup()Lcom/squareup/container/AdditionalContainerLayerSetup;

    move-result-object v2

    .line 370
    invoke-direct {v0, v1, v3, v4, v2}, Lcom/squareup/container/RedirectPipelineFactory;-><init>(Lkotlin/jvm/functions/Function1;ZLkotlin/jvm/functions/Function1;Lcom/squareup/container/AdditionalContainerLayerSetup;)V

    return-object v0
.end method

.method private final performTransition(Lflow/Traversal;Lflow/TraversalCallback;)V
    .locals 6

    .line 398
    iget-object v0, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/container/WaitForBootstrap;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array p1, v1, [Ljava/lang/Object;

    const-string v0, "Waiting for bootstrap"

    .line 399
    invoke-static {v0, p1}, Ltimber/log/Timber;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 400
    invoke-interface {p2}, Lflow/TraversalCallback;->onTraversalCompleted()V

    return-void

    .line 405
    :cond_0
    iget-object v0, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    sget-object v2, Lcom/squareup/container/Halt;->INSTANCE:Lcom/squareup/container/Halt;

    if-ne v0, v2, :cond_1

    new-array p1, v1, [Ljava/lang/Object;

    const-string v0, "halting"

    .line 406
    invoke-static {v0, p1}, Ltimber/log/Timber;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 410
    invoke-interface {p2}, Lflow/TraversalCallback;->onTraversalCompleted()V

    .line 412
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->doFinish()V

    return-void

    .line 417
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 418
    iget-object v2, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v2}, Lflow/History;->framesFromTop()Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/container/ContainerTreeKey;

    const-string v4, "key"

    .line 419
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420
    invoke-static {v3}, Lcom/squareup/container/layer/HidesMasterKt;->hidesMaster(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 423
    :cond_3
    iget-object v2, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v2}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v2

    .line 424
    invoke-virtual {v2}, Lflow/History$Builder;->clear()Lflow/History$Builder;

    .line 426
    new-instance v3, Lflow/Traversal;

    .line 427
    iget-object v4, p1, Lflow/Traversal;->origin:Lflow/History;

    .line 428
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->asReversedMutable(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {v2, v0}, Lflow/History$Builder;->addAll(Ljava/util/Collection;)Lflow/History$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object v0

    .line 429
    iget-object p1, p1, Lflow/Traversal;->direction:Lflow/Direction;

    .line 426
    invoke-direct {v3, v4, v0, p1}, Lflow/Traversal;-><init>(Lflow/History;Lflow/History;Lflow/Direction;)V

    new-array p1, v1, [Ljava/lang/Object;

    const-string v0, "displaying"

    .line 432
    invoke-static {v0, p1}, Ltimber/log/Timber;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 434
    new-instance p1, Lcom/squareup/container/TraversalCallbackSet;

    invoke-direct {p1, p2}, Lcom/squareup/container/TraversalCallbackSet;-><init>(Lflow/TraversalCallback;)V

    .line 435
    iget-object p2, p0, Lcom/squareup/container/ContainerPresenter;->layers:Ljava/util/List;

    if-nez p2, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    check-cast p2, Ljava/util/Collection;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result p2

    :goto_0
    if-ge v1, p2, :cond_6

    .line 438
    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter;->layers:Ljava/util/List;

    if-nez v0, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/layer/ContainerLayer;

    .line 439
    invoke-virtual {p1}, Lcom/squareup/container/TraversalCallbackSet;->add()Lflow/TraversalCallback;

    move-result-object v2

    invoke-direct {p0, v1}, Lcom/squareup/container/ContainerPresenter;->getVisiblePathContextsAround(I)[Lflow/path/PathContext;

    move-result-object v4

    array-length v5, v4

    invoke-static {v4, v5}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lflow/path/PathContext;

    .line 438
    invoke-interface {v0, v3, v2, v4}, Lcom/squareup/container/layer/ContainerLayer;->dispatchLayer(Lflow/Traversal;Lflow/TraversalCallback;[Lflow/path/PathContext;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 442
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/container/TraversalCallbackSet;->release()V

    return-void
.end method

.method private final prepareLayers(Lflow/path/PathContextFactory;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/path/PathContextFactory;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/container/layer/ContainerLayer;",
            ">;"
        }
    .end annotation

    .line 289
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 291
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/container/ContainerView;

    const-string/jumbo v3, "view"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2}, Lcom/squareup/container/ContainerView;->getContentLayout()Landroid/view/ViewGroup;

    move-result-object v2

    sget-object v4, Lcom/squareup/container/layer/ViewGroupLayer;->NULL_EXPOSER:Lcom/squareup/container/layer/ViewGroupLayer$Exposer;

    invoke-static {p1, v2, v4}, Lcom/squareup/container/layer/ViewGroupLayer;->defaultLayer(Lflow/path/PathContextFactory;Landroid/view/ViewGroup;Lcom/squareup/container/layer/ViewGroupLayer$Exposer;)Lcom/squareup/container/layer/ViewGroupLayer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 296
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/container/ContainerView;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2}, Lcom/squareup/container/ContainerView;->getCardLayout()Landroid/view/ViewGroup;

    move-result-object v2

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    .line 297
    new-instance v5, Lcom/squareup/container/ContainerPresenter$prepareLayers$$inlined$let$lambda$1;

    invoke-direct {v5, p0, v0, p1}, Lcom/squareup/container/ContainerPresenter$prepareLayers$$inlined$let$lambda$1;-><init>(Lcom/squareup/container/ContainerPresenter;Ljava/util/List;Lflow/path/PathContextFactory;)V

    check-cast v5, Lcom/squareup/container/layer/ViewGroupLayer$Exposer;

    .line 301
    const-class v6, Lcom/squareup/container/layer/CardScreen;

    .line 300
    invoke-static {p1, v2, v4, v5, v6}, Lcom/squareup/container/layer/ViewGroupLayer;->withAnnotation(Lflow/path/PathContextFactory;Landroid/view/ViewGroup;ZLcom/squareup/container/layer/ViewGroupLayer$Exposer;Ljava/lang/Class;)Lcom/squareup/container/layer/ViewGroupLayer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 305
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/container/ContainerView;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2}, Lcom/squareup/container/ContainerView;->getFullSheetLayout()Landroid/view/ViewGroup;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 309
    new-instance v5, Lcom/squareup/container/ContainerPresenter$prepareLayers$$inlined$let$lambda$2;

    invoke-direct {v5, p0, v0, p1}, Lcom/squareup/container/ContainerPresenter$prepareLayers$$inlined$let$lambda$2;-><init>(Lcom/squareup/container/ContainerPresenter;Ljava/util/List;Lflow/path/PathContextFactory;)V

    check-cast v5, Lcom/squareup/container/layer/ViewGroupLayer$Exposer;

    .line 313
    const-class v6, Lcom/squareup/container/layer/FullSheet;

    .line 312
    invoke-static {p1, v2, v4, v5, v6}, Lcom/squareup/container/layer/ViewGroupLayer;->withAnnotation(Lflow/path/PathContextFactory;Landroid/view/ViewGroup;ZLcom/squareup/container/layer/ViewGroupLayer$Exposer;Ljava/lang/Class;)Lcom/squareup/container/layer/ViewGroupLayer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 317
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/container/ContainerView;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2}, Lcom/squareup/container/ContainerView;->getCardOverSheetLayout()Landroid/view/ViewGroup;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 318
    new-instance v5, Lcom/squareup/container/ContainerPresenter$prepareLayers$$inlined$let$lambda$3;

    invoke-direct {v5, p0, v0, p1}, Lcom/squareup/container/ContainerPresenter$prepareLayers$$inlined$let$lambda$3;-><init>(Lcom/squareup/container/ContainerPresenter;Ljava/util/List;Lflow/path/PathContextFactory;)V

    check-cast v5, Lcom/squareup/container/layer/ViewGroupLayer$Exposer;

    .line 322
    const-class v6, Lcom/squareup/container/layer/CardOverSheetScreen;

    .line 321
    invoke-static {p1, v2, v4, v5, v6}, Lcom/squareup/container/layer/ViewGroupLayer;->withAnnotation(Lflow/path/PathContextFactory;Landroid/view/ViewGroup;ZLcom/squareup/container/layer/ViewGroupLayer$Exposer;Ljava/lang/Class;)Lcom/squareup/container/layer/ViewGroupLayer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 326
    :cond_2
    new-instance v2, Lcom/squareup/container/layer/DialogLayer;

    const-class v5, Lcom/squareup/container/layer/DialogCardScreen;

    invoke-direct {v2, p1, v5}, Lcom/squareup/container/layer/DialogLayer;-><init>(Lflow/path/PathContextFactory;Ljava/lang/Class;)V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 327
    new-instance v2, Lcom/squareup/container/layer/DialogLayer;

    const-class v5, Lcom/squareup/container/layer/DialogScreen;

    invoke-direct {v2, p1, v5}, Lcom/squareup/container/layer/DialogLayer;-><init>(Lflow/path/PathContextFactory;Ljava/lang/Class;)V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 328
    new-instance v2, Lcom/squareup/container/layer/DialogLayer;

    const-class v5, Lcom/squareup/container/layer/WorkflowDialogScreen;

    invoke-direct {v2, p1, v5}, Lcom/squareup/container/layer/DialogLayer;-><init>(Lflow/path/PathContextFactory;Ljava/lang/Class;)V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 330
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/container/ContainerView;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1}, Lcom/squareup/container/ContainerView;->getMasterLayout()Landroid/view/ViewGroup;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 332
    new-instance v2, Lcom/squareup/container/ContainerPresenter$prepareLayers$$inlined$let$lambda$4;

    invoke-direct {v2, p0, p1, v0}, Lcom/squareup/container/ContainerPresenter$prepareLayers$$inlined$let$lambda$4;-><init>(Lcom/squareup/container/ContainerPresenter;Lflow/path/PathContextFactory;Ljava/util/List;)V

    check-cast v2, Lcom/squareup/container/layer/ViewGroupLayer$Exposer;

    const/4 v3, 0x1

    .line 338
    const-class v5, Lcom/squareup/container/layer/Master;

    invoke-static {p1, v1, v3, v2, v5}, Lcom/squareup/container/layer/ViewGroupLayer;->withAnnotation(Lflow/path/PathContextFactory;Landroid/view/ViewGroup;ZLcom/squareup/container/layer/ViewGroupLayer$Exposer;Ljava/lang/Class;)Lcom/squareup/container/layer/ViewGroupLayer;

    move-result-object v1

    .line 339
    const-class v2, Lcom/squareup/container/layer/WorkflowMaster;

    invoke-virtual {v1, v2}, Lcom/squareup/container/layer/ViewGroupLayer;->plus(Ljava/lang/Class;)Lcom/squareup/container/layer/ViewGroupLayer;

    move-result-object v1

    .line 340
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/container/layer/ContainerLayer;

    invoke-virtual {v1, v2}, Lcom/squareup/container/layer/ViewGroupLayer;->isVisibleWith(Lcom/squareup/container/layer/ContainerLayer;)Lcom/squareup/container/layer/ViewGroupLayer;

    move-result-object v1

    const-string v2, "masterLayer"

    .line 341
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v4, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 342
    :cond_3
    move-object v1, p0

    check-cast v1, Lcom/squareup/container/ContainerPresenter;

    .line 344
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_7

    check-cast v1, Lcom/squareup/container/layer/ViewGroupLayer;

    .line 346
    const-class v2, Lcom/squareup/container/layer/Master;

    invoke-virtual {v1, v2}, Lcom/squareup/container/layer/ViewGroupLayer;->plus(Ljava/lang/Class;)Lcom/squareup/container/layer/ViewGroupLayer;

    move-result-object v1

    .line 347
    const-class v2, Lcom/squareup/container/layer/WorkflowMaster;

    invoke-virtual {v1, v2}, Lcom/squareup/container/layer/ViewGroupLayer;->plus(Ljava/lang/Class;)Lcom/squareup/container/layer/ViewGroupLayer;

    move-result-object v1

    const-string v2, "content\n          .plus(\u2026rkflowMaster::class.java)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 345
    invoke-interface {v0, v4, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 352
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getAdditionalContainerLayerSetup()Lcom/squareup/container/AdditionalContainerLayerSetup;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Lcom/squareup/container/AdditionalContainerLayerSetup;->additionalLayersToSetup(Ljava/util/List;Lflow/path/PathContextFactory;)Ljava/util/List;

    move-result-object p1

    .line 353
    check-cast p1, Ljava/lang/Iterable;

    .line 606
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/container/AdditionalContainerLayer;

    .line 355
    instance-of v2, v1, Lcom/squareup/container/AdditionalContainerLayer$Prepend;

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/squareup/container/AdditionalContainerLayer;->getContainerLayer()Lcom/squareup/container/layer/ContainerLayer;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 356
    :cond_5
    instance-of v2, v1, Lcom/squareup/container/AdditionalContainerLayer$Append;

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/squareup/container/AdditionalContainerLayer;->getContainerLayer()Lcom/squareup/container/layer/ContainerLayer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_6
    return-object v0

    .line 344
    :cond_7
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.container.layer.ViewGroupLayer"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final preprocessFlowTraversal(Lflow/Traversal;)Lflow/Traversal;
    .locals 1

    .line 379
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->buildRedirectPipeline()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/container/RedirectPipelineFactoryKt;->process(Ljava/util/List;Lflow/Traversal;)Lflow/Traversal;

    move-result-object p1

    return-object p1
.end method

.method private final removeDispatcher()V
    .locals 2

    .line 256
    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter;->dispatcher:Lflow/Dispatcher;

    if-eqz v0, :cond_1

    .line 260
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getFlow()Lflow/Flow;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v1, p0, Lcom/squareup/container/ContainerPresenter;->dispatcher:Lflow/Dispatcher;

    invoke-virtual {v0, v1}, Lflow/Flow;->removeDispatcher(Lflow/Dispatcher;)V

    const/4 v0, 0x0

    .line 261
    check-cast v0, Lflow/Dispatcher;

    iput-object v0, p0, Lcom/squareup/container/ContainerPresenter;->dispatcher:Lflow/Dispatcher;

    :cond_1
    return-void
.end method

.method private final sealLayers(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/container/layer/ContainerLayer;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/container/layer/ContainerLayer;",
            ">;"
        }
    .end annotation

    .line 476
    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    .line 608
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    .line 610
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/container/layer/ContainerLayer;

    .line 476
    invoke-interface {v3}, Lcom/squareup/container/layer/ContainerLayer;->isDefault()Z

    move-result v3

    if-eqz v3, :cond_1

    add-int/lit8 v1, v1, 0x1

    if-gez v1, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_0

    :cond_2
    :goto_1
    const/4 v0, 0x1

    if-ne v1, v0, :cond_3

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_4

    .line 477
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    const-string/jumbo v0, "unmodifiableList(layers)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 476
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Must have exactly one default layer: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final topViewAs(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 469
    invoke-direct {p0}, Lcom/squareup/container/ContainerPresenter;->getTopVisibleLayer()Lcom/squareup/container/layer/ContainerLayer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/container/layer/ContainerLayer;->as(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method protected buildDispatchPipeline(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/container/DispatchStep;",
            ">;)V"
        }
    .end annotation

    const-string v0, "pipeline"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 383
    check-cast p1, Ljava/util/Collection;

    new-instance v0, Lcom/squareup/container/ContainerPresenter$buildDispatchPipeline$1;

    move-object v1, p0

    check-cast v1, Lcom/squareup/container/ContainerPresenter;

    invoke-direct {v0, v1}, Lcom/squareup/container/ContainerPresenter$buildDispatchPipeline$1;-><init>(Lcom/squareup/container/ContainerPresenter;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    new-instance v1, Lcom/squareup/container/ContainerPresenterKt$sam$com_squareup_container_DispatchStep$0;

    invoke-direct {v1, v0}, Lcom/squareup/container/ContainerPresenterKt$sam$com_squareup_container_DispatchStep$0;-><init>(Lkotlin/jvm/functions/Function2;)V

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected buildRedirectPipeline()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/container/RedirectStep;",
            ">;"
        }
    .end annotation

    .line 367
    invoke-direct {p0}, Lcom/squareup/container/ContainerPresenter;->makeRedirectFactory()Lcom/squareup/container/RedirectPipelineFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/container/ContainerPresenter;->containerScope:Lmortar/MortarScope;

    if-nez v1, :cond_0

    const-string v2, "containerScope"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, v1}, Lcom/squareup/container/RedirectPipelineFactory;->buildPipeline(Lmortar/MortarScope;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final doFinish()V
    .locals 2

    .line 206
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerView;

    instance-of v1, v0, Landroid/view/View;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/squareup/container/ContainerPresenter;->defaultBackPressedHandler:Lkotlin/jvm/functions/Function1;

    invoke-static {v0}, Lcom/squareup/util/Views;->getActivity(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v0

    invoke-interface {v1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public dropView(Lcom/squareup/container/ContainerView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerView;

    if-ne v0, p1, :cond_2

    .line 233
    invoke-direct {p0}, Lcom/squareup/container/ContainerPresenter;->removeDispatcher()V

    .line 239
    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter;->layers:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v0, Ljava/lang/Iterable;

    .line 604
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/container/layer/ContainerLayer;

    .line 239
    invoke-interface {v1}, Lcom/squareup/container/layer/ContainerLayer;->cleanUp()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 240
    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/container/ContainerPresenter;->layers:Ljava/util/List;

    .line 243
    :cond_2
    invoke-super {p0, p1}, Lmortar/Presenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/container/ContainerView;

    invoke-virtual {p0, p1}, Lcom/squareup/container/ContainerPresenter;->dropView(Lcom/squareup/container/ContainerView;)V

    return-void
.end method

.method protected extractBundleService(Lcom/squareup/container/ContainerView;)Lmortar/bundler/BundleService;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Lmortar/bundler/BundleService;"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    invoke-interface {p1}, Lcom/squareup/container/ContainerView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    const-string v0, "BundleService.getBundleService(view.context)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/container/ContainerView;

    invoke-virtual {p0, p1}, Lcom/squareup/container/ContainerPresenter;->extractBundleService(Lcom/squareup/container/ContainerView;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected getAdditionalContainerLayerSetup()Lcom/squareup/container/AdditionalContainerLayerSetup;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter;->additionalContainerLayerSetup:Lcom/squareup/container/AdditionalContainerLayerSetup;

    return-object v0
.end method

.method protected abstract getDefaultHistory()Lflow/History;
.end method

.method protected getFlow()Lflow/Flow;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter;->_flow:Lflow/Flow;

    return-object v0
.end method

.method protected initialDetailScreenForAnnotation(Lcom/squareup/container/layer/Master;)Lflow/path/Path;
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 460
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Must be implemented by master/detail containers."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final onActivityFinish()V
    .locals 0

    .line 252
    invoke-direct {p0}, Lcom/squareup/container/ContainerPresenter;->removeDispatcher()V

    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .line 273
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerView;

    instance-of v1, v0, Landroid/view/View;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_4

    .line 277
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/container/ContainerView;

    const-string/jumbo v2, "view"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1}, Lcom/squareup/container/ContainerView;->isInterceptingInputEvents()Z

    move-result v1

    if-nez v1, :cond_3

    .line 275
    invoke-static {v0}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->onBackPressed(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 276
    const-class v0, Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/squareup/container/ContainerPresenter;->topViewAs(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->onBackPressed(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 277
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getFlow()Lflow/Flow;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_4

    .line 279
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->doFinish()V

    :cond_4
    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-super {p0, p1}, Lmortar/Presenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 146
    iput-object p1, p0, Lcom/squareup/container/ContainerPresenter;->containerScope:Lmortar/MortarScope;

    .line 147
    invoke-static {p1}, Lcom/squareup/container/OrientationLock;->get(Lmortar/MortarScope;)Lcom/squareup/container/OrientationLock;

    move-result-object p1

    const-string v0, "OrientationLock.get(scope)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/container/ContainerPresenter;->orientationLock:Lcom/squareup/container/OrientationLock;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 151
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getFlow()Lflow/Flow;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    const-string v2, "CURRENT_SCREEN_KEY"

    .line 156
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 157
    new-instance v3, Lcom/squareup/container/PassthroughParcer;

    invoke-direct {v3}, Lcom/squareup/container/PassthroughParcer;-><init>()V

    .line 158
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast v3, Lflow/KeyParceler;

    invoke-static {p1, v3}, Lflow/History;->from(Landroid/os/Parcelable;Lflow/KeyParceler;)Lflow/History;

    move-result-object p1

    const-string v2, "History.from(savedInstan\u2026RENT_SCREEN_KEY), parcer)"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v2, v1, [Ljava/lang/Object;

    aput-object p1, v2, v0

    const-string v3, "Restoring Flow with history %s"

    .line 159
    invoke-static {v3, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    new-array v2, v1, [Ljava/lang/Object;

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 161
    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v2, v0

    const-string p1, "New Flow. Bundle? %s"

    invoke-static {p1, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 162
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getDefaultHistory()Lflow/History;

    move-result-object p1

    .line 166
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/container/ContainerView;

    const-string/jumbo v3, "view"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2}, Lcom/squareup/container/ContainerView;->getMasterLayout()Landroid/view/ViewGroup;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 167
    sget-object v2, Lcom/squareup/container/RedirectPipelineFactory;->Companion:Lcom/squareup/container/RedirectPipelineFactory$Companion;

    invoke-direct {p0}, Lcom/squareup/container/ContainerPresenter;->makeRedirectFactory()Lcom/squareup/container/RedirectPipelineFactory;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Lcom/squareup/container/RedirectPipelineFactory$Companion;->tweakHistoryFromMasterToDetail(Lcom/squareup/container/RedirectPipelineFactory;Lflow/History;)Lflow/History;

    move-result-object v2

    if-eqz v2, :cond_2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v0

    aput-object v2, v3, v1

    const-string p1, "Tweaked history from %s to %s"

    .line 169
    invoke-static {p1, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object p1, v2

    .line 174
    :cond_2
    new-instance v0, Lflow/Flow;

    new-instance v1, Lcom/squareup/container/ContainerPresenter$onLoad$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/container/ContainerPresenter;

    invoke-direct {v1, v2}, Lcom/squareup/container/ContainerPresenter$onLoad$1;-><init>(Lcom/squareup/container/ContainerPresenter;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/container/ContainerPresenterKt$sam$flow_Processor$0;

    invoke-direct {v2, v1}, Lcom/squareup/container/ContainerPresenterKt$sam$flow_Processor$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lflow/Processor;

    invoke-direct {v0, p1, v2}, Lflow/Flow;-><init>(Lflow/History;Lflow/Processor;)V

    iput-object v0, p0, Lcom/squareup/container/ContainerPresenter;->_flow:Lflow/Flow;

    .line 177
    :cond_3
    new-instance p1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter;->contextFactory:Lflow/path/PathContextFactory;

    invoke-direct {p0, v0}, Lcom/squareup/container/ContainerPresenter;->prepareLayers(Lflow/path/PathContextFactory;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/squareup/container/ContainerPresenter;->sealLayers(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/container/ContainerPresenter;->layers:Ljava/util/List;

    .line 184
    new-instance p1, Lcom/squareup/container/ContainerPresenter$onLoad$2;

    invoke-direct {p1, p0}, Lcom/squareup/container/ContainerPresenter$onLoad$2;-><init>(Lcom/squareup/container/ContainerPresenter;)V

    check-cast p1, Lflow/Dispatcher;

    iput-object p1, p0, Lcom/squareup/container/ContainerPresenter;->dispatcher:Lflow/Dispatcher;

    .line 189
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getFlow()Lflow/Flow;

    move-result-object p1

    if-nez p1, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter;->dispatcher:Lflow/Dispatcher;

    invoke-virtual {p1, v0}, Lflow/Flow;->setDispatcher(Lflow/Dispatcher;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 446
    invoke-virtual {p0}, Lcom/squareup/container/ContainerPresenter;->getFlow()Lflow/Flow;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Lflow/Flow;->getHistory()Lflow/History;

    move-result-object v0

    const-string v1, "flow!!.history"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/container/Histories;->scrubbed(Lflow/History;)Lflow/History;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 450
    new-instance v1, Lcom/squareup/container/PassthroughParcer;

    invoke-direct {v1}, Lcom/squareup/container/PassthroughParcer;-><init>()V

    check-cast v1, Lflow/KeyParceler;

    invoke-virtual {v0, v1}, Lflow/History;->getParcelable(Lflow/KeyParceler;)Landroid/os/Parcelable;

    move-result-object v0

    const-string v1, "CURRENT_SCREEN_KEY"

    .line 448
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    return-void
.end method

.method public final topScreensByLayer(Lflow/History;Lflow/History;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            "Lflow/History;",
            ")",
            "Ljava/util/Collection<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "traversalCompleting"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextHistory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 498
    new-instance v0, Lcom/squareup/container/ContainerPresenter$topScreensByLayer$1;

    invoke-direct {v0, p0}, Lcom/squareup/container/ContainerPresenter$topScreensByLayer$1;-><init>(Lcom/squareup/container/ContainerPresenter;)V

    .line 504
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v1, Ljava/util/Set;

    .line 505
    invoke-virtual {v0, p1}, Lcom/squareup/container/ContainerPresenter$topScreensByLayer$1;->invoke(Lflow/History;)Ljava/util/Collection;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 504
    invoke-static {v1, p1}, Lkotlin/collections/SetsKt;->plus(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object p1

    .line 506
    invoke-virtual {v0, p2}, Lcom/squareup/container/ContainerPresenter$topScreensByLayer$1;->invoke(Lflow/History;)Ljava/util/Collection;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 505
    invoke-static {p1, p2}, Lkotlin/collections/SetsKt;->plus(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    return-object p1
.end method
