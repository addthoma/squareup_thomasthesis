.class public final Lcom/squareup/container/Masters;
.super Ljava/lang/Object;
.source "Masters.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMasters.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Masters.kt\ncom/squareup/container/Masters\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,24:1\n119#2:25\n125#2:26\n*E\n*S KotlinDebug\n*F\n+ 1 Masters.kt\ncom/squareup/container/Masters\n*L\n11#1:25\n14#1:26\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0000\u0010\u0003\"\u0018\u0010\u0004\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0003\"\u0018\u0010\u0005\u001a\u00020\u0006*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\"\u001b\u0010\t\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\n*\u00020\u000b8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u000e"
    }
    d2 = {
        "isMasterRequiringDetail",
        "",
        "Lflow/path/Path;",
        "(Lflow/path/Path;)Z",
        "isMasterScreen",
        "masterAnnotation",
        "Lcom/squareup/container/layer/Master;",
        "getMasterAnnotation",
        "(Lflow/path/Path;)Lcom/squareup/container/layer/Master;",
        "section",
        "Ljava/lang/Class;",
        "Lcom/squareup/container/ContainerTreeKey;",
        "getSection",
        "(Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Class;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getMasterAnnotation(Lflow/path/Path;)Lcom/squareup/container/layer/Master;
    .locals 1

    const-string v0, "$this$masterAnnotation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    .line 26
    const-class v0, Lcom/squareup/container/layer/Master;

    invoke-virtual {p0, v0}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 14
    check-cast p0, Lcom/squareup/container/layer/Master;

    return-object p0

    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "@Master required"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final getSection(Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/ContainerTreeKey;",
            ")",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    const-string v0, "$this$section"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    const-class v0, Lcom/squareup/container/layer/InSection;

    invoke-virtual {p0, v0}, Lcom/squareup/container/ContainerTreeKey;->elementOfType(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/container/layer/InSection;

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/squareup/container/layer/InSection;->getSection()Ljava/lang/Class;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final isMasterRequiringDetail(Lflow/path/Path;)Z
    .locals 1

    const-string v0, "$this$isMasterRequiringDetail"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-static {p0}, Lcom/squareup/container/Masters;->isMasterScreen(Lflow/path/Path;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/squareup/container/Masters;->getMasterAnnotation(Lflow/path/Path;)Lcom/squareup/container/layer/Master;

    move-result-object p0

    invoke-interface {p0}, Lcom/squareup/container/layer/Master;->existsWithoutDetail()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isMasterScreen(Lflow/path/Path;)Z
    .locals 1

    const-string v0, "$this$isMasterScreen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    .line 25
    const-class v0, Lcom/squareup/container/layer/Master;

    invoke-static {p0, v0}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result p0

    return p0
.end method
