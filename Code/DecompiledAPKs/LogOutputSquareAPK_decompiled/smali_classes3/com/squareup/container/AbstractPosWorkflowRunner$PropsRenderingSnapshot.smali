.class public final Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;
.super Ljava/lang/Object;
.source "AbstractPosWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/AbstractPosWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "PropsRenderingSnapshot"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PropsT:",
        "Ljava/lang/Object;",
        "RenderingT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0084\u0008\u0018\u0000*\u0006\u0008\u0003\u0010\u0001 \u0001*\u0006\u0008\u0004\u0010\u0002 \u00012\u00020\u0003B\u001d\u0012\u0006\u0010\u0004\u001a\u00028\u0003\u0012\u0006\u0010\u0005\u001a\u00028\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\u000f\u001a\u00028\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\nJ\u000e\u0010\u0010\u001a\u00028\u0004H\u00c6\u0003\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J8\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u00040\u00002\u0008\u0008\u0002\u0010\u0004\u001a\u00028\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00028\u00042\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001\u00a2\u0006\u0002\u0010\u0013J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0003H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001R\u0013\u0010\u0004\u001a\u00028\u0003\u00a2\u0006\n\n\u0002\u0010\u000b\u001a\u0004\u0008\t\u0010\nR\u0013\u0010\u0005\u001a\u00028\u0004\u00a2\u0006\n\n\u0002\u0010\u000b\u001a\u0004\u0008\u000c\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;",
        "PropsT",
        "RenderingT",
        "",
        "props",
        "rendering",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V",
        "getProps",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "getRendering",
        "getSnapshot",
        "()Lcom/squareup/workflow/Snapshot;",
        "component1",
        "component2",
        "component3",
        "copy",
        "(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final props:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TPropsT;"
        }
    .end annotation
.end field

.field private final rendering:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TRenderingT;"
        }
    .end annotation
.end field

.field private final snapshot:Lcom/squareup/workflow/Snapshot;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPropsT;TRenderingT;",
            "Lcom/squareup/workflow/Snapshot;",
            ")V"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->props:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->rendering:Ljava/lang/Object;

    iput-object p3, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->snapshot:Lcom/squareup/workflow/Snapshot;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;ILjava/lang/Object;)Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->props:Ljava/lang/Object;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->rendering:Ljava/lang/Object;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->snapshot:Lcom/squareup/workflow/Snapshot;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->copy(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TPropsT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->props:Ljava/lang/Object;

    return-object v0
.end method

.method public final component2()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRenderingT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->rendering:Ljava/lang/Object;

    return-object v0
.end method

.method public final component3()Lcom/squareup/workflow/Snapshot;
    .locals 1

    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->snapshot:Lcom/squareup/workflow/Snapshot;

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPropsT;TRenderingT;",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
            "TPropsT;TRenderingT;>;"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;

    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->props:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->props:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->rendering:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->rendering:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->snapshot:Lcom/squareup/workflow/Snapshot;

    iget-object p1, p1, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->snapshot:Lcom/squareup/workflow/Snapshot;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getProps()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TPropsT;"
        }
    .end annotation

    .line 148
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->props:Ljava/lang/Object;

    return-object v0
.end method

.method public final getRendering()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRenderingT;"
        }
    .end annotation

    .line 149
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->rendering:Ljava/lang/Object;

    return-object v0
.end method

.method public final getSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->snapshot:Lcom/squareup/workflow/Snapshot;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->props:Ljava/lang/Object;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->rendering:Ljava/lang/Object;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->snapshot:Lcom/squareup/workflow/Snapshot;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PropsRenderingSnapshot(props="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->props:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", rendering="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->rendering:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", snapshot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->snapshot:Lcom/squareup/workflow/Snapshot;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
