.class public final Lcom/squareup/container/WorkflowTreeKey$Companion;
.super Ljava/lang/Object;
.source "WorkflowTreeKey.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/WorkflowTreeKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowTreeKey.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowTreeKey.kt\ncom/squareup/container/WorkflowTreeKey$Companion\n+ 2 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,168:1\n24#2,4:169\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowTreeKey.kt\ncom/squareup/container/WorkflowTreeKey$Companion\n*L\n150#1,4:169\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u00ce\u0001\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0004\"\n\u0008\u0000\u0010\u0005\u0018\u0001*\u00020\u00062\u00b0\u0001\u0008\u0004\u0010\u0007\u001a\u00a9\u0001\u0012\u0013\u0012\u00110\t\u00a2\u0006\u000c\u0008\n\u0012\u0008\u0008\u000b\u0012\u0004\u0008\u0008(\u000c\u0012\u0015\u0012\u0013\u0018\u00010\r\u00a2\u0006\u000c\u0008\n\u0012\u0008\u0008\u000b\u0012\u0004\u0008\u0008(\u000e\u0012\u001f\u0012\u001d\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000fj\u0002`\u0010\u00a2\u0006\u000c\u0008\n\u0012\u0008\u0008\u000b\u0012\u0004\u0008\u0008(\u0011\u0012\u0013\u0012\u00110\u0012\u00a2\u0006\u000c\u0008\n\u0012\u0008\u0008\u000b\u0012\u0004\u0008\u0008(\u0013\u0012\u0013\u0012\u00110\u0001\u00a2\u0006\u000c\u0008\n\u0012\u0008\u0008\u000b\u0012\u0004\u0008\u0008(\u0014\u0012\u0013\u0012\u00110\u0015\u00a2\u0006\u000c\u0008\n\u0012\u0008\u0008\u000b\u0012\u0004\u0008\u0008(\u0016\u0012\u0013\u0012\u00110\u0017\u00a2\u0006\u000c\u0008\n\u0012\u0008\u0008\u000b\u0012\u0004\u0008\u0008(\u0018\u0012\u0004\u0012\u0002H\u00050\u0008H\u0085\u0008\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/container/WorkflowTreeKey$Companion;",
        "",
        "()V",
        "creator",
        "Landroid/os/Parcelable$Creator;",
        "T",
        "Lcom/squareup/container/WorkflowTreeKey;",
        "constructorReference",
        "Lkotlin/Function7;",
        "",
        "Lkotlin/ParameterName;",
        "name",
        "runnerServiceName",
        "Lcom/squareup/container/ContainerTreeKey;",
        "parent",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "screenKey",
        "Lcom/squareup/workflow/Snapshot;",
        "snapshot",
        "props",
        "Lcom/squareup/workflow/ScreenHint;",
        "hint",
        "",
        "isPersistent",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 138
    invoke-direct {p0}, Lcom/squareup/container/WorkflowTreeKey$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic creator(Lkotlin/jvm/functions/Function7;)Landroid/os/Parcelable$Creator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/container/WorkflowTreeKey;",
            ">(",
            "Lkotlin/jvm/functions/Function7<",
            "-",
            "Ljava/lang/String;",
            "-",
            "Lcom/squareup/container/ContainerTreeKey;",
            "-",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;-",
            "Lcom/squareup/workflow/Snapshot;",
            "Ljava/lang/Object;",
            "-",
            "Lcom/squareup/workflow/ScreenHint;",
            "-",
            "Ljava/lang/Boolean;",
            "+TT;>;)",
            "Landroid/os/Parcelable$Creator<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "constructorReference"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    new-instance v0, Lcom/squareup/container/WorkflowTreeKey$Companion$creator$$inlined$pathCreator$1;

    invoke-direct {v0, p1}, Lcom/squareup/container/WorkflowTreeKey$Companion$creator$$inlined$pathCreator$1;-><init>(Lkotlin/jvm/functions/Function7;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    return-object v0
.end method
