.class public Lcom/squareup/container/CardScreenPhoneLayout;
.super Landroid/widget/FrameLayout;
.source "CardScreenPhoneLayout.java"

# interfaces
.implements Lcom/squareup/container/CardScreenContainer;


# instance fields
.field private final bodyLayoutId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    sget-object v0, Lcom/squareup/container/R$styleable;->CardScreenPhoneLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 32
    :try_start_0
    sget p2, Lcom/squareup/container/R$styleable;->CardScreenPhoneLayout_bodyLayout:I

    const/4 v0, -0x1

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/container/CardScreenPhoneLayout;->bodyLayoutId:I

    .line 33
    iget p2, p0, Lcom/squareup/container/CardScreenPhoneLayout;->bodyLayoutId:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq p2, v0, :cond_0

    .line 37
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    .line 34
    :cond_0
    :try_start_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    const-string v0, "Must define bodyLayout"

    invoke-direct {p2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p2

    .line 37
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 38
    throw p2
.end method

.method private getViewCoveredByCard()Landroid/view/View;
    .locals 2

    .line 56
    invoke-virtual {p0}, Lcom/squareup/container/CardScreenPhoneLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget v1, p0, Lcom/squareup/container/CardScreenPhoneLayout;->bodyLayoutId:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 58
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "bodyLayout xml attribute must be set to a peer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public asViewGroup()Landroid/view/ViewGroup;
    .locals 0

    return-object p0
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 42
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/container/CardScreenPhoneLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/container/ContainerBackgroundsService;->getCardBackground(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/container/CardScreenPhoneLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setCardVisible(ZZLflow/TraversalCallback;)V
    .locals 1

    .line 52
    invoke-direct {p0}, Lcom/squareup/container/CardScreenPhoneLayout;->getViewCoveredByCard()Landroid/view/View;

    move-result-object v0

    invoke-static {p0, v0, p1, p2, p3}, Lcom/squareup/container/CardScreenContainers;->setCardVisible(Landroid/view/ViewGroup;Landroid/view/View;ZZLflow/TraversalCallback;)V

    return-void
.end method
