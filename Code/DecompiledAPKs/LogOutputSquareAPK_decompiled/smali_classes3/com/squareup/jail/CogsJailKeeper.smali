.class public final Lcom/squareup/jail/CogsJailKeeper;
.super Ljava/lang/Object;
.source "CogsJailKeeper.kt"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/jailkeeper/JailKeeper;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCogsJailKeeper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CogsJailKeeper.kt\ncom/squareup/jail/CogsJailKeeper\n+ 2 PushMessageDelegate.kt\ncom/squareup/pushmessages/PushMessageDelegateKt\n*L\n1#1,401:1\n16#2:402\n*E\n*S KotlinDebug\n*F\n+ 1 CogsJailKeeper.kt\ncom/squareup/jail/CogsJailKeeper\n*L\n114#1:402\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00d4\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0003\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002B\u00d1\u0001\u0008\u0007\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0004\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u0012\u0006\u0010\u001d\u001a\u00020\u001e\u0012\u0006\u0010\u001f\u001a\u00020 \u0012\u0006\u0010!\u001a\u00020\"\u0012\u0006\u0010#\u001a\u00020$\u0012\u000c\u0010%\u001a\u0008\u0012\u0004\u0012\u00020&0\u0004\u0012\u0008\u0008\u0001\u0010\'\u001a\u00020(\u0012\u0016\u0010)\u001a\u0012\u0012\u0004\u0012\u00020+0*j\u0008\u0012\u0004\u0012\u00020+`,\u0012\u0006\u0010-\u001a\u00020.\u00a2\u0006\u0002\u0010/J\u0008\u0010>\u001a\u00020?H\u0016J\u0010\u00100\u001a\u00020?2\u0006\u0010@\u001a\u000201H\u0002J\u0008\u0010A\u001a\u00020?H\u0002J\u0008\u0010B\u001a\u00020?H\u0002J\u0006\u0010C\u001a\u00020?J\u0010\u0010D\u001a\u00020?2\u0006\u0010;\u001a\u00020:H\u0002J\u0008\u0010E\u001a\u00020:H\u0016J\u0010\u0010F\u001a\u00020?2\u0006\u0010G\u001a\u00020HH\u0002J\u0010\u0010I\u001a\u00020?2\u0006\u0010J\u001a\u00020KH\u0016J\u0008\u0010L\u001a\u00020?H\u0016J\u0010\u0010M\u001a\u00020?2\u0006\u0010@\u001a\u000201H\u0002J\u0010\u0010N\u001a\u00020?2\u0006\u0010O\u001a\u00020PH\u0002J\u0008\u0010Q\u001a\u00020?H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010)\u001a\u0012\u0012\u0004\u0012\u00020+0*j\u0008\u0012\u0004\u0012\u00020+`,X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010%\u001a\u0008\u0012\u0004\u0012\u00020&0\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u000201X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u00102\u001a\u0002038VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u00084\u00105R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020(X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020.X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00106\u001a\u000201X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u00107\u001a\u000208X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010;\u001a\u00020:2\u0006\u00109\u001a\u00020:@RX\u0096\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008<\u0010=R\u000e\u0010#\u001a\u00020$X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006R"
    }
    d2 = {
        "Lcom/squareup/jail/CogsJailKeeper;",
        "Lmortar/Scoped;",
        "Lcom/squareup/jailkeeper/JailKeeper;",
        "authenticator",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/account/LegacyAuthenticator;",
        "eventSink",
        "Lcom/squareup/badbus/BadEventSink;",
        "lazyLoggedInScopeNotifier",
        "Ldagger/Lazy;",
        "Lcom/squareup/LoggedInScopeNotifier;",
        "cogsProvider",
        "Lcom/squareup/cogs/Cogs;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "openTicketsSettings",
        "Lcom/squareup/tickets/OpenTicketsSettings;",
        "catalogFeesPreloader",
        "Lcom/squareup/catalogfees/CatalogFeesPreloader;",
        "orderEntryPages",
        "Lcom/squareup/orderentry/pages/OrderEntryPages;",
        "cashDrawers",
        "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "diningOptionCache",
        "Lcom/squareup/ui/main/DiningOptionCache;",
        "voidCompSettings",
        "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
        "compDiscountsCache",
        "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
        "voidReasonsCache",
        "Lcom/squareup/tickets/voidcomp/VoidReasonsCache;",
        "ticketGroupsCache",
        "Lcom/squareup/tickets/TicketGroupsCache;",
        "ticketCountsCache",
        "Lcom/squareup/tickets/TicketCountsCache;",
        "callbacksProvider",
        "Lcom/squareup/util/RxCallbacks;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "additionalServices",
        "Ljava/util/Set;",
        "Lcom/squareup/jailkeeper/JailKeeperService;",
        "Lcom/squareup/dagger/DaggerSet;",
        "pushMessageDelegate",
        "Lcom/squareup/pushmessages/PushMessageDelegate;",
        "(Ljavax/inject/Provider;Lcom/squareup/badbus/BadEventSink;Ldagger/Lazy;Ljavax/inject/Provider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/catalogfees/CatalogFeesPreloader;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/main/DiningOptionCache;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lcom/squareup/tickets/voidcomp/VoidReasonsCache;Lcom/squareup/tickets/TicketGroupsCache;Lcom/squareup/tickets/TicketCountsCache;Ljavax/inject/Provider;Lio/reactivex/Scheduler;Ljava/util/Set;Lcom/squareup/pushmessages/PushMessageDelegate;)V",
        "dataLoaded",
        "",
        "jailScreen",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "getJailScreen",
        "()Lcom/squareup/ui/main/RegisterTreeKey;",
        "requireCogsSync",
        "serviceDisposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "<set-?>",
        "Lcom/squareup/jailkeeper/JailKeeper$State;",
        "state",
        "getState",
        "()Lcom/squareup/jailkeeper/JailKeeper$State;",
        "backgroundSync",
        "",
        "backgroundSyncAfterLoad",
        "doCogsForegroundSync",
        "doForegroundSync",
        "doNotRequireCogsSync",
        "finishCogsSync",
        "foregroundSync",
        "logOrThrowSyncError",
        "error",
        "Lcom/squareup/shared/catalog/sync/SyncError;",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "preloadData",
        "propagateCatalogException",
        "caught",
        "",
        "reloadData",
        "jail_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final additionalServices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/jailkeeper/JailKeeperService;",
            ">;"
        }
    .end annotation
.end field

.field private final authenticator:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private final callbacksProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/RxCallbacks;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDrawers:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

.field private final catalogFeesPreloader:Lcom/squareup/catalogfees/CatalogFeesPreloader;

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final compDiscountsCache:Lcom/squareup/tickets/voidcomp/CompDiscountsCache;

.field private dataLoaded:Z

.field private final diningOptionCache:Lcom/squareup/ui/main/DiningOptionCache;

.field private final eventSink:Lcom/squareup/badbus/BadEventSink;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final lazyLoggedInScopeNotifier:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/LoggedInScopeNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

.field private final pushMessageDelegate:Lcom/squareup/pushmessages/PushMessageDelegate;

.field private requireCogsSync:Z

.field private final serviceDisposables:Lio/reactivex/disposables/CompositeDisposable;

.field private state:Lcom/squareup/jailkeeper/JailKeeper$State;

.field private final ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

.field private final ticketGroupsCache:Lcom/squareup/tickets/TicketGroupsCache;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

.field private final voidReasonsCache:Lcom/squareup/tickets/voidcomp/VoidReasonsCache;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/badbus/BadEventSink;Ldagger/Lazy;Ljavax/inject/Provider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/catalogfees/CatalogFeesPreloader;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/main/DiningOptionCache;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lcom/squareup/tickets/voidcomp/VoidReasonsCache;Lcom/squareup/tickets/TicketGroupsCache;Lcom/squareup/tickets/TicketCountsCache;Ljavax/inject/Provider;Lio/reactivex/Scheduler;Ljava/util/Set;Lcom/squareup/pushmessages/PushMessageDelegate;)V
    .locals 16
    .param p18    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Ldagger/Lazy<",
            "Lcom/squareup/LoggedInScopeNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/catalogfees/CatalogFeesPreloader;",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/main/DiningOptionCache;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            "Lcom/squareup/tickets/voidcomp/VoidReasonsCache;",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            "Lcom/squareup/tickets/TicketCountsCache;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/RxCallbacks;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Ljava/util/Set<",
            "Lcom/squareup/jailkeeper/JailKeeperService;",
            ">;",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v0, p16

    const-string v0, "authenticator"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventSink"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyLoggedInScopeNotifier"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cogsProvider"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "openTicketsSettings"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "catalogFeesPreloader"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEntryPages"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cashDrawers"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "diningOptionCache"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voidCompSettings"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "compDiscountsCache"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voidReasonsCache"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "ticketGroupsCache"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "ticketCountsCache"

    move-object/from16 v15, p16

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callbacksProvider"

    move-object/from16 v15, p17

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    move-object/from16 v15, p18

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additionalServices"

    move-object/from16 v15, p19

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pushMessageDelegate"

    move-object/from16 v15, p20

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v15, p16

    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper;->authenticator:Ljavax/inject/Provider;

    iput-object v2, v0, Lcom/squareup/jail/CogsJailKeeper;->eventSink:Lcom/squareup/badbus/BadEventSink;

    iput-object v3, v0, Lcom/squareup/jail/CogsJailKeeper;->lazyLoggedInScopeNotifier:Ldagger/Lazy;

    iput-object v4, v0, Lcom/squareup/jail/CogsJailKeeper;->cogsProvider:Ljavax/inject/Provider;

    iput-object v5, v0, Lcom/squareup/jail/CogsJailKeeper;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object v6, v0, Lcom/squareup/jail/CogsJailKeeper;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    iput-object v7, v0, Lcom/squareup/jail/CogsJailKeeper;->catalogFeesPreloader:Lcom/squareup/catalogfees/CatalogFeesPreloader;

    iput-object v8, v0, Lcom/squareup/jail/CogsJailKeeper;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    iput-object v9, v0, Lcom/squareup/jail/CogsJailKeeper;->cashDrawers:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    iput-object v10, v0, Lcom/squareup/jail/CogsJailKeeper;->features:Lcom/squareup/settings/server/Features;

    iput-object v11, v0, Lcom/squareup/jail/CogsJailKeeper;->diningOptionCache:Lcom/squareup/ui/main/DiningOptionCache;

    iput-object v12, v0, Lcom/squareup/jail/CogsJailKeeper;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iput-object v13, v0, Lcom/squareup/jail/CogsJailKeeper;->compDiscountsCache:Lcom/squareup/tickets/voidcomp/CompDiscountsCache;

    iput-object v14, v0, Lcom/squareup/jail/CogsJailKeeper;->voidReasonsCache:Lcom/squareup/tickets/voidcomp/VoidReasonsCache;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper;->ticketGroupsCache:Lcom/squareup/tickets/TicketGroupsCache;

    iput-object v15, v0, Lcom/squareup/jail/CogsJailKeeper;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    move-object/from16 v1, p17

    move-object/from16 v2, p18

    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper;->callbacksProvider:Ljavax/inject/Provider;

    iput-object v2, v0, Lcom/squareup/jail/CogsJailKeeper;->mainScheduler:Lio/reactivex/Scheduler;

    move-object/from16 v1, p19

    move-object/from16 v2, p20

    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper;->additionalServices:Ljava/util/Set;

    iput-object v2, v0, Lcom/squareup/jail/CogsJailKeeper;->pushMessageDelegate:Lcom/squareup/pushmessages/PushMessageDelegate;

    .line 97
    sget-object v1, Lcom/squareup/jailkeeper/JailKeeper$State;->UNKNOWN:Lcom/squareup/jailkeeper/JailKeeper$State;

    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper;->state:Lcom/squareup/jailkeeper/JailKeeper$State;

    .line 100
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper;->serviceDisposables:Lio/reactivex/disposables/CompositeDisposable;

    const/4 v1, 0x1

    .line 103
    iput-boolean v1, v0, Lcom/squareup/jail/CogsJailKeeper;->requireCogsSync:Z

    return-void
.end method

.method public static final synthetic access$dataLoaded(Lcom/squareup/jail/CogsJailKeeper;Z)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lcom/squareup/jail/CogsJailKeeper;->dataLoaded(Z)V

    return-void
.end method

.method public static final synthetic access$doCogsForegroundSync(Lcom/squareup/jail/CogsJailKeeper;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/squareup/jail/CogsJailKeeper;->doCogsForegroundSync()V

    return-void
.end method

.method public static final synthetic access$doForegroundSync(Lcom/squareup/jail/CogsJailKeeper;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/squareup/jail/CogsJailKeeper;->doForegroundSync()V

    return-void
.end method

.method public static final synthetic access$finishCogsSync(Lcom/squareup/jail/CogsJailKeeper;Lcom/squareup/jailkeeper/JailKeeper$State;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lcom/squareup/jail/CogsJailKeeper;->finishCogsSync(Lcom/squareup/jailkeeper/JailKeeper$State;)V

    return-void
.end method

.method public static final synthetic access$getAuthenticator$p(Lcom/squareup/jail/CogsJailKeeper;)Ljavax/inject/Provider;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/squareup/jail/CogsJailKeeper;->authenticator:Ljavax/inject/Provider;

    return-object p0
.end method

.method public static final synthetic access$getRequireCogsSync$p(Lcom/squareup/jail/CogsJailKeeper;)Z
    .locals 0

    .line 72
    iget-boolean p0, p0, Lcom/squareup/jail/CogsJailKeeper;->requireCogsSync:Z

    return p0
.end method

.method public static final synthetic access$getState$p(Lcom/squareup/jail/CogsJailKeeper;)Lcom/squareup/jailkeeper/JailKeeper$State;
    .locals 0

    .line 72
    invoke-virtual {p0}, Lcom/squareup/jail/CogsJailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$logOrThrowSyncError(Lcom/squareup/jail/CogsJailKeeper;Lcom/squareup/shared/catalog/sync/SyncError;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lcom/squareup/jail/CogsJailKeeper;->logOrThrowSyncError(Lcom/squareup/shared/catalog/sync/SyncError;)V

    return-void
.end method

.method public static final synthetic access$preloadData(Lcom/squareup/jail/CogsJailKeeper;Z)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lcom/squareup/jail/CogsJailKeeper;->preloadData(Z)V

    return-void
.end method

.method public static final synthetic access$propagateCatalogException(Lcom/squareup/jail/CogsJailKeeper;Ljava/lang/Throwable;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lcom/squareup/jail/CogsJailKeeper;->propagateCatalogException(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static final synthetic access$reloadData(Lcom/squareup/jail/CogsJailKeeper;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/squareup/jail/CogsJailKeeper;->reloadData()V

    return-void
.end method

.method public static final synthetic access$setRequireCogsSync$p(Lcom/squareup/jail/CogsJailKeeper;Z)V
    .locals 0

    .line 72
    iput-boolean p1, p0, Lcom/squareup/jail/CogsJailKeeper;->requireCogsSync:Z

    return-void
.end method

.method public static final synthetic access$setState$p(Lcom/squareup/jail/CogsJailKeeper;Lcom/squareup/jailkeeper/JailKeeper$State;)V
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/squareup/jail/CogsJailKeeper;->state:Lcom/squareup/jailkeeper/JailKeeper$State;

    return-void
.end method

.method private final dataLoaded(Z)V
    .locals 1

    const/4 v0, 0x1

    .line 353
    iput-boolean v0, p0, Lcom/squareup/jail/CogsJailKeeper;->dataLoaded:Z

    .line 354
    sget-object v0, Lcom/squareup/jailkeeper/JailKeeper$State;->READY:Lcom/squareup/jailkeeper/JailKeeper$State;

    invoke-direct {p0, v0}, Lcom/squareup/jail/CogsJailKeeper;->finishCogsSync(Lcom/squareup/jailkeeper/JailKeeper$State;)V

    if-eqz p1, :cond_0

    .line 356
    invoke-virtual {p0}, Lcom/squareup/jail/CogsJailKeeper;->backgroundSync()V

    :cond_0
    return-void
.end method

.method private final doCogsForegroundSync()V
    .locals 5

    .line 199
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "cogsProvider.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/cogs/Cogs;

    invoke-interface {v0}, Lcom/squareup/cogs/Cogs;->isCloseEnqueued()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 203
    :cond_0
    sget-object v0, Lcom/squareup/jailkeeper/JailKeeper$State;->SYNCING:Lcom/squareup/jailkeeper/JailKeeper$State;

    iput-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->state:Lcom/squareup/jailkeeper/JailKeeper$State;

    .line 204
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->eventSink:Lcom/squareup/badbus/BadEventSink;

    invoke-virtual {p0}, Lcom/squareup/jail/CogsJailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    .line 206
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    .line 209
    iget-object v1, p0, Lcom/squareup/jail/CogsJailKeeper;->serviceDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 210
    invoke-static {}, Lcom/squareup/jail/CogsJailKeeperKt;->access$getMAX_AGE$p()Lcom/squareup/shared/catalog/utils/ElapsedTime;

    move-result-object v2

    .line 211
    iget-object v3, p0, Lcom/squareup/jail/CogsJailKeeper;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v3}, Lcom/squareup/settings/server/AccountStatusSettings;->getItemBatchSize()J

    move-result-wide v3

    .line 209
    invoke-interface {v0, v2, v3, v4}, Lcom/squareup/cogs/Cogs;->foregroundSyncAsSingle(Lcom/squareup/shared/catalog/utils/ElapsedTime;J)Lio/reactivex/Single;

    move-result-object v2

    .line 213
    new-instance v3, Lcom/squareup/jail/CogsJailKeeper$doCogsForegroundSync$1;

    invoke-direct {v3, p0, v0}, Lcom/squareup/jail/CogsJailKeeper$doCogsForegroundSync$1;-><init>(Lcom/squareup/jail/CogsJailKeeper;Lcom/squareup/cogs/Cogs;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v2, "cogs.foregroundSyncAsSin\u2026se)\n          }\n        }"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 209
    invoke-static {v1, v0}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method private final doForegroundSync()V
    .locals 3

    .line 153
    invoke-virtual {p0}, Lcom/squareup/jail/CogsJailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v0

    sget-object v1, Lcom/squareup/jailkeeper/JailKeeper$State;->SYNCING:Lcom/squareup/jailkeeper/JailKeeper$State;

    if-eq v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/squareup/jail/CogsJailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v0

    sget-object v1, Lcom/squareup/jailkeeper/JailKeeper$State;->INITIALIZING:Lcom/squareup/jailkeeper/JailKeeper$State;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 155
    :cond_0
    sget-object v0, Lcom/squareup/jailkeeper/JailKeeper$State;->INITIALIZING:Lcom/squareup/jailkeeper/JailKeeper$State;

    iput-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->state:Lcom/squareup/jailkeeper/JailKeeper$State;

    .line 157
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    .line 159
    iget-boolean v1, p0, Lcom/squareup/jail/CogsJailKeeper;->requireCogsSync:Z

    if-nez v1, :cond_2

    const-string v1, "cogs"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/squareup/cogs/Cogs;->isReady()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 165
    iget-boolean v1, p0, Lcom/squareup/jail/CogsJailKeeper;->dataLoaded:Z

    if-eqz v1, :cond_1

    .line 168
    sget-object v0, Lcom/squareup/jailkeeper/JailKeeper$State;->READY:Lcom/squareup/jailkeeper/JailKeeper$State;

    invoke-direct {p0, v0}, Lcom/squareup/jail/CogsJailKeeper;->finishCogsSync(Lcom/squareup/jailkeeper/JailKeeper$State;)V

    .line 169
    invoke-virtual {p0}, Lcom/squareup/jail/CogsJailKeeper;->backgroundSync()V

    return-void

    :cond_1
    const/4 v1, 0x1

    .line 177
    invoke-direct {p0, v1}, Lcom/squareup/jail/CogsJailKeeper;->preloadData(Z)V

    .line 181
    :cond_2
    iget-object v1, p0, Lcom/squareup/jail/CogsJailKeeper;->eventSink:Lcom/squareup/badbus/BadEventSink;

    invoke-virtual {p0}, Lcom/squareup/jail/CogsJailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    .line 183
    iget-object v1, p0, Lcom/squareup/jail/CogsJailKeeper;->serviceDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {}, Lcom/squareup/jail/CogsJailKeeperKt;->access$getMAX_AGE$p()Lcom/squareup/shared/catalog/utils/ElapsedTime;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/squareup/cogs/Cogs;->shouldForegroundSyncAsSingle(Lcom/squareup/shared/catalog/utils/ElapsedTime;)Lio/reactivex/Single;

    move-result-object v0

    .line 184
    new-instance v2, Lcom/squareup/jail/CogsJailKeeper$doForegroundSync$1;

    invoke-direct {v2, p0}, Lcom/squareup/jail/CogsJailKeeper$doForegroundSync$1;-><init>(Lcom/squareup/jail/CogsJailKeeper;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v2, "cogs.shouldForegroundSyn\u2026se)\n          }\n        }"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    invoke-static {v1, v0}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private final finishCogsSync(Lcom/squareup/jailkeeper/JailKeeper$State;)V
    .locals 1

    .line 393
    iput-object p1, p0, Lcom/squareup/jail/CogsJailKeeper;->state:Lcom/squareup/jailkeeper/JailKeeper$State;

    .line 394
    iget-object p1, p0, Lcom/squareup/jail/CogsJailKeeper;->eventSink:Lcom/squareup/badbus/BadEventSink;

    invoke-virtual {p0}, Lcom/squareup/jail/CogsJailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    return-void
.end method

.method private final logOrThrowSyncError(Lcom/squareup/shared/catalog/sync/SyncError;)V
    .locals 3

    .line 373
    iget-object v0, p1, Lcom/squareup/shared/catalog/sync/SyncError;->errorType:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    if-eqz v0, :cond_0

    sget-object v1, Lcom/squareup/jail/CogsJailKeeper$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 385
    :pswitch_0
    new-instance v0, Lcom/squareup/shared/catalog/CatalogException;

    iget-object p1, p1, Lcom/squareup/shared/catalog/sync/SyncError;->errorMessage:Ljava/lang/String;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/CatalogException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :pswitch_1
    return-void

    .line 379
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/jail/CogsJailKeeper;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance v0, Lcom/squareup/account/AccountEvents$SessionExpired;

    invoke-direct {v0}, Lcom/squareup/account/AccountEvents$SessionExpired;-><init>()V

    invoke-interface {p1, v0}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    :pswitch_3
    return-void

    .line 386
    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 387
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Did not expect error type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/squareup/shared/catalog/sync/SyncError;->errorType:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " with message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/shared/catalog/sync/SyncError;->errorMessage:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 386
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private final preloadData(Z)V
    .locals 10

    .line 279
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "cogsProvider.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/cogs/Cogs;

    invoke-interface {v0}, Lcom/squareup/cogs/Cogs;->isCloseEnqueued()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->cashDrawers:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->cashManagementEnabledAndNeedsLoading()Z

    move-result v0

    .line 284
    iget-object v1, p0, Lcom/squareup/jail/CogsJailKeeper;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->DINING_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    .line 285
    iget-object v2, p0, Lcom/squareup/jail/CogsJailKeeper;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v2}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->shouldSyncCompDiscounts()Z

    move-result v2

    .line 286
    iget-object v3, p0, Lcom/squareup/jail/CogsJailKeeper;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v3}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->shouldSyncVoidReasons()Z

    move-result v3

    .line 287
    iget-object v4, p0, Lcom/squareup/jail/CogsJailKeeper;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v4}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v4

    .line 288
    iget-object v5, p0, Lcom/squareup/jail/CogsJailKeeper;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v5}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v5

    .line 290
    iget-object v6, p0, Lcom/squareup/jail/CogsJailKeeper;->callbacksProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/util/RxCallbacks;

    .line 292
    iget-object v7, p0, Lcom/squareup/jail/CogsJailKeeper;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    invoke-virtual {v6}, Lcom/squareup/util/RxCallbacks;->createCallback()Ljava/lang/Runnable;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lcom/squareup/orderentry/pages/OrderEntryPages;->loadAndPost(Ljava/lang/Runnable;Z)V

    .line 294
    iget-object v7, p0, Lcom/squareup/jail/CogsJailKeeper;->catalogFeesPreloader:Lcom/squareup/catalogfees/CatalogFeesPreloader;

    invoke-virtual {v6}, Lcom/squareup/util/RxCallbacks;->createCallback()Ljava/lang/Runnable;

    move-result-object v8

    const-string v9, "callbacks.createCallback()"

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v7, v8}, Lcom/squareup/catalogfees/CatalogFeesPreloader;->loadAndPost(Ljava/lang/Runnable;)V

    if-eqz v0, :cond_1

    .line 297
    invoke-virtual {v6}, Lcom/squareup/util/RxCallbacks;->createCallback()Ljava/lang/Runnable;

    move-result-object v0

    .line 298
    iget-object v7, p0, Lcom/squareup/jail/CogsJailKeeper;->cashDrawers:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    new-instance v8, Lcom/squareup/jail/CogsJailKeeper$preloadData$1;

    invoke-direct {v8, v0}, Lcom/squareup/jail/CogsJailKeeper$preloadData$1;-><init>(Ljava/lang/Runnable;)V

    check-cast v8, Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;

    invoke-interface {v7, v8}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->loadCurrentCashDrawerShift(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V

    :cond_1
    if-eqz v1, :cond_2

    .line 306
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->diningOptionCache:Lcom/squareup/ui/main/DiningOptionCache;

    invoke-virtual {v6}, Lcom/squareup/util/RxCallbacks;->createCallback()Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/DiningOptionCache;->loadAndPost(Ljava/lang/Runnable;)V

    :cond_2
    if-eqz v2, :cond_3

    .line 310
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->compDiscountsCache:Lcom/squareup/tickets/voidcomp/CompDiscountsCache;

    invoke-virtual {v6}, Lcom/squareup/util/RxCallbacks;->createCallback()Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/tickets/voidcomp/CompDiscountsCache;->loadAndPost(Ljava/lang/Runnable;)V

    :cond_3
    if-eqz v3, :cond_4

    .line 314
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->voidReasonsCache:Lcom/squareup/tickets/voidcomp/VoidReasonsCache;

    invoke-virtual {v6}, Lcom/squareup/util/RxCallbacks;->createCallback()Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/tickets/voidcomp/VoidReasonsCache;->loadAndPost(Ljava/lang/Runnable;)V

    :cond_4
    if-eqz v4, :cond_6

    .line 318
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 319
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->ticketGroupsCache:Lcom/squareup/tickets/TicketGroupsCache;

    invoke-virtual {v6}, Lcom/squareup/util/RxCallbacks;->createCallback()Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/tickets/TicketGroupsCache;->loadAndPost(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 321
    :cond_5
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->ticketGroupsCache:Lcom/squareup/tickets/TicketGroupsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketGroupsCache;->loadAndPost()V

    :cond_6
    :goto_0
    if-eqz v5, :cond_7

    .line 330
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketCountsCache;->loadAndPost()V

    .line 335
    :cond_7
    invoke-virtual {v6}, Lcom/squareup/util/RxCallbacks;->completion()Lrx/Observable;

    move-result-object v0

    .line 336
    invoke-virtual {v0}, Lrx/Observable;->toCompletable()Lrx/Completable;

    move-result-object v0

    const-string v1, "callbacks.completion()\n        .toCompletable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 337
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Completable(Lrx/Completable;)Lio/reactivex/Completable;

    move-result-object v0

    .line 341
    iget-object v1, p0, Lcom/squareup/jail/CogsJailKeeper;->serviceDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 342
    iget-object v2, p0, Lcom/squareup/jail/CogsJailKeeper;->additionalServices:Ljava/util/Set;

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lcom/squareup/jailkeeper/JailKeeperServiceKt;->preloadAll(Ljava/lang/Iterable;)Lio/reactivex/Completable;

    move-result-object v2

    check-cast v2, Lio/reactivex/CompletableSource;

    invoke-virtual {v0, v2}, Lio/reactivex/Completable;->mergeWith(Lio/reactivex/CompletableSource;)Lio/reactivex/Completable;

    move-result-object v0

    .line 343
    iget-object v2, p0, Lcom/squareup/jail/CogsJailKeeper;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v2}, Lio/reactivex/Completable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v0

    .line 345
    new-instance v2, Lcom/squareup/jail/CogsJailKeeper$preloadData$2;

    invoke-direct {v2, p0, p1}, Lcom/squareup/jail/CogsJailKeeper$preloadData$2;-><init>(Lcom/squareup/jail/CogsJailKeeper;Z)V

    check-cast v2, Lio/reactivex/functions/Action;

    .line 346
    new-instance p1, Lcom/squareup/jail/CogsJailKeeper$preloadData$3;

    invoke-direct {p1, p0}, Lcom/squareup/jail/CogsJailKeeper$preloadData$3;-><init>(Lcom/squareup/jail/CogsJailKeeper;)V

    check-cast p1, Lio/reactivex/functions/Consumer;

    .line 344
    invoke-virtual {v0, v2, p1}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v0, "preLoadTasksCompletion\n \u2026throwable)\n            })"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 341
    invoke-static {v1, p1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method private final propagateCatalogException(Ljava/lang/Throwable;)V
    .locals 2

    .line 362
    sget-object v0, Lcom/squareup/jail/CogsJailKeeper$propagateCatalogException$1;->INSTANCE:Lkotlin/reflect/KProperty1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lkotlin/sequences/SequencesKt;->generateSequence(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 363
    sget-object v1, Lcom/squareup/jail/CogsJailKeeper$propagateCatalogException$2;->INSTANCE:Lcom/squareup/jail/CogsJailKeeper$propagateCatalogException$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/sequences/SequencesKt;->mapNotNull(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 364
    invoke-static {v0}, Lkotlin/sequences/SequencesKt;->firstOrNull(Lkotlin/sequences/Sequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/CatalogException;

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Throwable;

    .line 365
    throw v0

    :cond_0
    new-instance v0, Lrx/exceptions/OnErrorNotImplementedException;

    const-string v1, "Caught by JailKeeper"

    invoke-direct {v0, v1, p1}, Lrx/exceptions/OnErrorNotImplementedException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final reloadData()V
    .locals 5

    .line 241
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->refresh()V

    .line 243
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->catalogFeesPreloader:Lcom/squareup/catalogfees/CatalogFeesPreloader;

    invoke-interface {v0}, Lcom/squareup/catalogfees/CatalogFeesPreloader;->loadAndPost()V

    .line 244
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DINING_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->diningOptionCache:Lcom/squareup/ui/main/DiningOptionCache;

    invoke-virtual {v0}, Lcom/squareup/ui/main/DiningOptionCache;->loadAndPost()V

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->shouldSyncCompDiscounts()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 248
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->compDiscountsCache:Lcom/squareup/tickets/voidcomp/CompDiscountsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/CompDiscountsCache;->loadAndPost()V

    .line 250
    :cond_1
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->shouldSyncVoidReasons()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 251
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->voidReasonsCache:Lcom/squareup/tickets/voidcomp/VoidReasonsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidReasonsCache;->loadAndPost()V

    .line 253
    :cond_2
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 254
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->ticketGroupsCache:Lcom/squareup/tickets/TicketGroupsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketGroupsCache;->loadAndPost()V

    .line 256
    :cond_3
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 257
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketCountsCache;->loadAndPost()V

    .line 263
    :cond_4
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->serviceDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/jail/CogsJailKeeper;->additionalServices:Ljava/util/Set;

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lcom/squareup/jailkeeper/JailKeeperServiceKt;->reloadAll(Ljava/lang/Iterable;)Lio/reactivex/Completable;

    move-result-object v1

    .line 264
    iget-object v2, p0, Lcom/squareup/jail/CogsJailKeeper;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v1, v2}, Lio/reactivex/Completable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v1

    .line 266
    sget-object v2, Lcom/squareup/jail/CogsJailKeeper$reloadData$1;->INSTANCE:Lcom/squareup/jail/CogsJailKeeper$reloadData$1;

    check-cast v2, Lio/reactivex/functions/Action;

    .line 267
    new-instance v3, Lcom/squareup/jail/CogsJailKeeper$reloadData$2;

    move-object v4, p0

    check-cast v4, Lcom/squareup/jail/CogsJailKeeper;

    invoke-direct {v3, v4}, Lcom/squareup/jail/CogsJailKeeper$reloadData$2;-><init>(Lcom/squareup/jail/CogsJailKeeper;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    new-instance v4, Lcom/squareup/jail/CogsJailKeeperKt$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v4, v3}, Lcom/squareup/jail/CogsJailKeeperKt$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v4, Lio/reactivex/functions/Consumer;

    .line 265
    invoke-virtual {v1, v2, v4}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "additionalServices.reloa\u2026atalogException\n        )"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 263
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method


# virtual methods
.method public backgroundSync()V
    .locals 3

    .line 128
    invoke-virtual {p0}, Lcom/squareup/jail/CogsJailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v0

    sget-object v1, Lcom/squareup/jailkeeper/JailKeeper$State;->READY:Lcom/squareup/jailkeeper/JailKeeper$State;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 129
    invoke-virtual {p0}, Lcom/squareup/jail/CogsJailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "Ignoring background sync request in state %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->serviceDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/jail/CogsJailKeeper;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cogs/Cogs;

    .line 134
    invoke-interface {v1}, Lcom/squareup/cogs/Cogs;->syncAsSingle()Lio/reactivex/Single;

    move-result-object v1

    .line 135
    new-instance v2, Lcom/squareup/jail/CogsJailKeeper$backgroundSync$1;

    invoke-direct {v2, p0}, Lcom/squareup/jail/CogsJailKeeper$backgroundSync$1;-><init>(Lcom/squareup/jail/CogsJailKeeper;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "cogsProvider.get()\n     \u2026   reloadData()\n        }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public final doNotRequireCogsSync()V
    .locals 1

    const/4 v0, 0x0

    .line 398
    iput-boolean v0, p0, Lcom/squareup/jail/CogsJailKeeper;->requireCogsSync:Z

    return-void
.end method

.method public foregroundSync()Lcom/squareup/jailkeeper/JailKeeper$State;
    .locals 1

    .line 123
    invoke-direct {p0}, Lcom/squareup/jail/CogsJailKeeper;->doForegroundSync()V

    .line 124
    invoke-virtual {p0}, Lcom/squareup/jail/CogsJailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getJailScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 72
    invoke-virtual {p0}, Lcom/squareup/jail/CogsJailKeeper;->getJailScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method

.method public getJailScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 2

    .line 95
    sget-object v0, Lcom/squareup/jail/JailScreen;->INSTANCE:Lcom/squareup/jail/JailScreen;

    const-string v1, "JailScreen.INSTANCE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public getState()Lcom/squareup/jailkeeper/JailKeeper$State;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->state:Lcom/squareup/jailkeeper/JailKeeper$State;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->serviceDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 107
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->serviceDisposables:Lio/reactivex/disposables/CompositeDisposable;

    check-cast v0, Lio/reactivex/disposables/Disposable;

    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->lazyLoggedInScopeNotifier:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/LoggedInScopeNotifier;

    .line 110
    invoke-interface {v0}, Lcom/squareup/LoggedInScopeNotifier;->onLoggedIn()Lrx/Observable;

    move-result-object v0

    .line 111
    new-instance v1, Lcom/squareup/jail/CogsJailKeeper$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/jail/CogsJailKeeper$onEnterScope$1;-><init>(Lcom/squareup/jail/CogsJailKeeper;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "lazyLoggedInScopeNotifie\u2026be { doForegroundSync() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnExit(Lrx/Subscription;Lmortar/MortarScope;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper;->pushMessageDelegate:Lcom/squareup/pushmessages/PushMessageDelegate;

    .line 402
    const-class v1, Lcom/squareup/pushmessages/PushMessage$CogsSyncPushMessage;

    invoke-interface {v0, v1}, Lcom/squareup/pushmessages/PushMessageDelegate;->observe(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lcom/squareup/jail/CogsJailKeeper;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 116
    new-instance v1, Lcom/squareup/jail/CogsJailKeeper$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/jail/CogsJailKeeper$onEnterScope$2;-><init>(Lcom/squareup/jail/CogsJailKeeper;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "pushMessageDelegate.obse\u2026ribe { backgroundSync() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
