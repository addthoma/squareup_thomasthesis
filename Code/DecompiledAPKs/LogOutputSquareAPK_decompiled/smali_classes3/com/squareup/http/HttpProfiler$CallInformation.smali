.class public final Lcom/squareup/http/HttpProfiler$CallInformation;
.super Ljava/lang/Object;
.source "HttpProfiler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/http/HttpProfiler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CallInformation"
.end annotation


# instance fields
.field public final baseUrl:Ljava/lang/String;

.field public final contentType:Ljava/lang/String;

.field public final method:Ljava/lang/String;

.field public final protocol:Ljava/lang/String;

.field public final relativePath:Ljava/lang/String;

.field public final requestLength:J

.field public final responseLength:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V
    .locals 0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/squareup/http/HttpProfiler$CallInformation;->method:Ljava/lang/String;

    .line 72
    iput-object p2, p0, Lcom/squareup/http/HttpProfiler$CallInformation;->protocol:Ljava/lang/String;

    .line 73
    iput-object p3, p0, Lcom/squareup/http/HttpProfiler$CallInformation;->baseUrl:Ljava/lang/String;

    .line 74
    iput-object p4, p0, Lcom/squareup/http/HttpProfiler$CallInformation;->relativePath:Ljava/lang/String;

    .line 75
    iput-wide p5, p0, Lcom/squareup/http/HttpProfiler$CallInformation;->requestLength:J

    .line 76
    iput-wide p7, p0, Lcom/squareup/http/HttpProfiler$CallInformation;->responseLength:J

    .line 77
    iput-object p9, p0, Lcom/squareup/http/HttpProfiler$CallInformation;->contentType:Ljava/lang/String;

    return-void
.end method
