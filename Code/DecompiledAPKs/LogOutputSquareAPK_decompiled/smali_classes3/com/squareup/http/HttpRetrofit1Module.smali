.class public abstract Lcom/squareup/http/HttpRetrofit1Module;
.super Ljava/lang/Object;
.source "HttpRetrofit1Module.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCogsRetrofitClient(Lokhttp3/OkHttpClient;)Lretrofit/client/Client;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 25
    new-instance v0, Lcom/jakewharton/retrofit/Ok3Client;

    invoke-direct {v0, p0}, Lcom/jakewharton/retrofit/Ok3Client;-><init>(Lokhttp3/OkHttpClient;)V

    return-object v0
.end method

.method static provideDiagnosticsRetrofitClient(Lokhttp3/OkHttpClient;)Lretrofit/client/Client;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 30
    new-instance v0, Lcom/jakewharton/retrofit/Ok3Client;

    invoke-direct {v0, p0}, Lcom/jakewharton/retrofit/Ok3Client;-><init>(Lokhttp3/OkHttpClient;)V

    return-object v0
.end method

.method static provideRetrofitClient(Lokhttp3/OkHttpClient;)Lretrofit/client/Client;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 15
    new-instance v0, Lcom/jakewharton/retrofit/Ok3Client;

    invoke-direct {v0, p0}, Lcom/jakewharton/retrofit/Ok3Client;-><init>(Lokhttp3/OkHttpClient;)V

    return-object v0
.end method

.method static provideUnauthenticatedRetrofitClient(Lokhttp3/OkHttpClient;)Lretrofit/client/Client;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 20
    new-instance v0, Lcom/jakewharton/retrofit/Ok3Client;

    invoke-direct {v0, p0}, Lcom/jakewharton/retrofit/Ok3Client;-><init>(Lokhttp3/OkHttpClient;)V

    return-object v0
.end method
