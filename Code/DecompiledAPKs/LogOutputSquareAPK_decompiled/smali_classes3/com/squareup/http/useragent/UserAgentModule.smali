.class public abstract Lcom/squareup/http/useragent/UserAgentModule;
.super Ljava/lang/Object;
.source "UserAgentModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideUserAgent(Lcom/squareup/http/useragent/UserAgentProvider;)Ljava/lang/String;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 19
    invoke-virtual {p0}, Lcom/squareup/http/useragent/UserAgentProvider;->getUserAgentString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static provideUserAgentId(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 14
    sget v0, Lcom/squareup/http/R$string;->user_agent_identifier:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
