.class public Lcom/squareup/http/UserAgentBuilder;
.super Ljava/lang/Object;
.source "UserAgentBuilder.java"


# static fields
.field private static final FORMAT:Ljava/lang/String; = "%s/%s (Android %s; %s; %s) Version/%s PointOfSaleSDK/%s %s"

.field public static final REAL_DEVICE_INFORMATION:Ljava/lang/String;


# instance fields
.field private buildSha:Ljava/lang/String;

.field private deviceInformation:Ljava/lang/String;

.field private environment:Ljava/lang/String;

.field private posSdkVersionName:Ljava/lang/String;

.field private productVersionName:Ljava/lang/String;

.field private readerSdkBucketBinId:Ljava/lang/String;

.field private userAgentId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 24
    invoke-static {}, Lcom/squareup/util/X2Build;->isSquareDevice()Z

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x3

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    sget-object v5, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    aput-object v5, v0, v3

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    aput-object v3, v0, v2

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v2, v0, v1

    sget-object v1, Landroid/os/Build;->ID:Ljava/lang/String;

    aput-object v1, v0, v4

    const-string v1, "%s %s %s %s"

    .line 26
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-array v0, v4, [Ljava/lang/Object;

    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    aput-object v4, v0, v3

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    aput-object v3, v0, v2

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v2, v0, v1

    const-string v1, "%s %s %s"

    .line 27
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    sput-object v0, Lcom/squareup/http/UserAgentBuilder;->REAL_DEVICE_INFORMATION:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    .line 36
    iput-object v0, p0, Lcom/squareup/http/UserAgentBuilder;->environment:Ljava/lang/String;

    return-void
.end method

.method private static uriEncodeWithSpaces(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, " "

    .line 121
    invoke-static {p0, v0}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public build(Ljava/util/Locale;)Ljava/lang/String;
    .locals 4

    .line 102
    iget-object v0, p0, Lcom/squareup/http/UserAgentBuilder;->posSdkVersionName:Ljava/lang/String;

    const-string v1, "posSdkVersionName"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 103
    iget-object v0, p0, Lcom/squareup/http/UserAgentBuilder;->productVersionName:Ljava/lang/String;

    const-string v1, "productVersionName"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 104
    iget-object v0, p0, Lcom/squareup/http/UserAgentBuilder;->userAgentId:Ljava/lang/String;

    const-string/jumbo v1, "userAgentId"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 105
    iget-object v0, p0, Lcom/squareup/http/UserAgentBuilder;->buildSha:Ljava/lang/String;

    const-string v1, "buildSha"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 106
    iget-object v0, p0, Lcom/squareup/http/UserAgentBuilder;->deviceInformation:Ljava/lang/String;

    const-string/jumbo v1, "userAgentDeviceInformation"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 109
    iget-object v0, p0, Lcom/squareup/http/UserAgentBuilder;->readerSdkBucketBinId:Ljava/lang/String;

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/http/UserAgentBuilder;->readerSdkBucketBinId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 112
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/http/UserAgentBuilder;->environment:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 114
    iget-object v3, p0, Lcom/squareup/http/UserAgentBuilder;->userAgentId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/squareup/http/UserAgentBuilder;->buildSha:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/squareup/http/UserAgentBuilder;->deviceInformation:Ljava/lang/String;

    .line 115
    invoke-static {v3}, Lcom/squareup/http/UserAgentBuilder;->uriEncodeWithSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    aput-object p1, v1, v2

    const/4 p1, 0x5

    iget-object v2, p0, Lcom/squareup/http/UserAgentBuilder;->productVersionName:Ljava/lang/String;

    aput-object v2, v1, p1

    const/4 p1, 0x6

    iget-object v2, p0, Lcom/squareup/http/UserAgentBuilder;->posSdkVersionName:Ljava/lang/String;

    aput-object v2, v1, p1

    const/4 p1, 0x7

    aput-object v0, v1, p1

    const-string p1, "%s/%s (Android %s; %s; %s) Version/%s PointOfSaleSDK/%s %s"

    .line 114
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public buildSha(Ljava/lang/String;)Lcom/squareup/http/UserAgentBuilder;
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/http/UserAgentBuilder;->buildSha:Ljava/lang/String;

    return-object p0
.end method

.method public deviceInformation(Ljava/lang/String;)Lcom/squareup/http/UserAgentBuilder;
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/http/UserAgentBuilder;->deviceInformation:Ljava/lang/String;

    return-object p0
.end method

.method public environment(Ljava/lang/String;)Lcom/squareup/http/UserAgentBuilder;
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/squareup/http/UserAgentBuilder;->environment:Ljava/lang/String;

    return-object p0
.end method

.method public posSdkVersionName(Ljava/lang/String;)Lcom/squareup/http/UserAgentBuilder;
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/squareup/http/UserAgentBuilder;->posSdkVersionName:Ljava/lang/String;

    return-object p0
.end method

.method public productVersionName(Ljava/lang/String;)Lcom/squareup/http/UserAgentBuilder;
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/squareup/http/UserAgentBuilder;->productVersionName:Ljava/lang/String;

    return-object p0
.end method

.method public readerSdkBucketBinId(Ljava/lang/String;)Lcom/squareup/http/UserAgentBuilder;
    .locals 0

    .line 91
    iput-object p1, p0, Lcom/squareup/http/UserAgentBuilder;->readerSdkBucketBinId:Ljava/lang/String;

    return-object p0
.end method

.method public userAgentId(Ljava/lang/String;)Lcom/squareup/http/UserAgentBuilder;
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/http/UserAgentBuilder;->userAgentId:Ljava/lang/String;

    return-object p0
.end method
