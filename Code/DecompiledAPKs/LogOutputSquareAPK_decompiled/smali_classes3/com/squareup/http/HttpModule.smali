.class abstract Lcom/squareup/http/HttpModule;
.super Ljava/lang/Object;
.source "HttpModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# static fields
.field private static final COGS_READ_TIMEOUT_SECONDS:J = 0x3cL

.field private static final FELICA_SERVICE_CALL_TIMEOUT_SECONDS:J = 0x5L

.field private static final TRANSACTION_LEDGER_CONNECT_TIMEOUT_SECONDS:J = 0x1eL

.field private static final TRANSACTION_LEDGER_WRITE_TIMEOUT_SECONDS:J = 0x3cL


# direct methods
.method constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCogsOkHttpClient(Lokhttp3/OkHttpClient;)Lokhttp3/OkHttpClient;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 42
    invoke-virtual {p0}, Lokhttp3/OkHttpClient;->newBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object p0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3c

    .line 43
    invoke-virtual {p0, v1, v2, v0}, Lokhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object p0

    .line 44
    invoke-virtual {p0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object p0

    return-object p0
.end method

.method static provideFelicaServiceOkHttpClient(Lokhttp3/OkHttpClient;)Lokhttp3/OkHttpClient;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 65
    invoke-virtual {p0}, Lokhttp3/OkHttpClient;->newBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object p0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x5

    .line 66
    invoke-virtual {p0, v1, v2, v0}, Lokhttp3/OkHttpClient$Builder;->callTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object p0

    .line 67
    invoke-virtual {p0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object p0

    return-object p0
.end method

.method static provideOkHttpClient(Lokhttp3/OkHttpClient$Builder;Lcom/squareup/http/interceptor/AuthHttpInterceptor;)Lokhttp3/OkHttpClient;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 31
    invoke-virtual {p0, p1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object p0

    return-object p0
.end method

.method static provideTransactionLedgerOkHttpClient(Lokhttp3/OkHttpClient;)Lokhttp3/OkHttpClient;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 53
    invoke-virtual {p0}, Lokhttp3/OkHttpClient;->newBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object p0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1e

    .line 54
    invoke-virtual {p0, v1, v2, v0}, Lokhttp3/OkHttpClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object p0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3c

    .line 55
    invoke-virtual {p0, v1, v2, v0}, Lokhttp3/OkHttpClient$Builder;->writeTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object p0

    .line 56
    invoke-virtual {p0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object p0

    return-object p0
.end method

.method static provideUnauthenticatedClient(Lokhttp3/OkHttpClient$Builder;)Lokhttp3/OkHttpClient;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 36
    invoke-virtual {p0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method abstract bindSquareDownloadManager(Lcom/squareup/http/AuthenticatedSquareDownloadManager;)Lcom/squareup/http/SquareDownloadManager;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindSquareHeaders(Lcom/squareup/http/interceptor/RealSquareHeaders;)Lcom/squareup/http/SquareHeaders;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
