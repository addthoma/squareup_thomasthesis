.class public Lcom/squareup/http/ClientErrors;
.super Ljava/lang/Object;
.source "ClientErrors.java"


# static fields
.field private static final NON_RETRYABLE_CLIENT_ERRORS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x12

    new-array v0, v0, [I

    .line 6
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/http/ClientErrors;->NON_RETRYABLE_CLIENT_ERRORS:[I

    return-void

    :array_0
    .array-data 4
        0x190
        0x193
        0x194
        0x195
        0x196
        0x199
        0x19a
        0x19b
        0x19c
        0x19d
        0x19e
        0x19f
        0x1a0
        0x1a1
        0x1a6
        0x1a8
        0x1ac
        0x1af
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isRetryableClientError(I)Z
    .locals 1

    .line 53
    sget-object v0, Lcom/squareup/http/ClientErrors;->NON_RETRYABLE_CLIENT_ERRORS:[I

    invoke-static {v0, p0}, Ljava/util/Arrays;->binarySearch([II)I

    move-result p0

    if-gez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
