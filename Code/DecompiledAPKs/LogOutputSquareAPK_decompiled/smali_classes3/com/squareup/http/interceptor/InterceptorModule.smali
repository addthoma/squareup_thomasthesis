.class public abstract Lcom/squareup/http/interceptor/InterceptorModule;
.super Ljava/lang/Object;
.source "InterceptorModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideGzipInterceptor(Lcom/squareup/http/interceptor/GzipRequestInterceptor;)Lokhttp3/Interceptor;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract provideProfilingInterceptor(Lcom/squareup/http/interceptor/ProfilingInterceptor;)Lokhttp3/Interceptor;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract provideRegisterHttpInterceptor(Lcom/squareup/http/interceptor/RegisterHttpInterceptor;)Lokhttp3/Interceptor;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract provideUrlRedirectInterceptor(Lcom/squareup/http/interceptor/UrlRedirectInterceptor;)Lokhttp3/Interceptor;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
