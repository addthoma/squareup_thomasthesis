.class public final Lcom/squareup/ms/SharedMinesweeperModule_ProvideMsFactory;
.super Ljava/lang/Object;
.source "SharedMinesweeperModule_ProvideMsFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ms/Ms;",
        ">;"
    }
.end annotation


# instance fields
.field private final crashnadoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;"
        }
    .end annotation
.end field

.field private final executorServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final minesweeperLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper$MinesweeperLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper$MinesweeperLogger;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMsFactory;->executorServiceProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMsFactory;->crashnadoProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMsFactory;->minesweeperLoggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ms/SharedMinesweeperModule_ProvideMsFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper$MinesweeperLogger;",
            ">;)",
            "Lcom/squareup/ms/SharedMinesweeperModule_ProvideMsFactory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMsFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMsFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideMs(Lcom/squareup/ms/MinesweeperExecutorService;Lcom/squareup/crashnado/Crashnado;Lcom/squareup/ms/Minesweeper$MinesweeperLogger;)Lcom/squareup/ms/Ms;
    .locals 0

    .line 47
    invoke-static {p0, p1, p2}, Lcom/squareup/ms/SharedMinesweeperModule;->provideMs(Lcom/squareup/ms/MinesweeperExecutorService;Lcom/squareup/crashnado/Crashnado;Lcom/squareup/ms/Minesweeper$MinesweeperLogger;)Lcom/squareup/ms/Ms;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ms/Ms;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ms/Ms;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMsFactory;->executorServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ms/MinesweeperExecutorService;

    iget-object v1, p0, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMsFactory;->crashnadoProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/crashnado/Crashnado;

    iget-object v2, p0, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMsFactory;->minesweeperLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ms/Minesweeper$MinesweeperLogger;

    invoke-static {v0, v1, v2}, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMsFactory;->provideMs(Lcom/squareup/ms/MinesweeperExecutorService;Lcom/squareup/crashnado/Crashnado;Lcom/squareup/ms/Minesweeper$MinesweeperLogger;)Lcom/squareup/ms/Ms;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMsFactory;->get()Lcom/squareup/ms/Ms;

    move-result-object v0

    return-object v0
.end method
