.class public final Lcom/squareup/ms/MinesweeperExecutorService;
.super Ljava/lang/Object;
.source "MinesweeperExecutorService.kt"

# interfaces
.implements Ljava/util/concurrent/ExecutorService;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u0000\n\u0002\u0010\u001f\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0008\u000b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0003J!\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u000e\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tH\u0096\u0001J\u0019\u0010\u000b\u001a\u00020\u000c2\u000e\u0010\u0006\u001a\n \n*\u0004\u0018\u00010\r0\rH\u0096\u0001J\u00dd\u0001\u0010\u000e\u001a^\u0012(\u0012&\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011 \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011\u0018\u00010\u00100\u0010 \n*.\u0012(\u0012&\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011 \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011\u0018\u00010\u00100\u0010\u0018\u00010\u00120\u000f\"\u0010\u0008\u0000\u0010\u0011*\n \n*\u0004\u0018\u00010\u00130\u00132d\u0010\u0006\u001a`\u0012*\u0008\u0001\u0012&\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011 \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011\u0018\u00010\u00150\u0015 \n*.\u0012(\u0012&\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011 \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011\u0018\u00010\u00150\u0015\u0018\u00010\u00160\u0014H\u0096\u0001J\u00f5\u0001\u0010\u000e\u001a^\u0012(\u0012&\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011 \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011\u0018\u00010\u00100\u0010 \n*.\u0012(\u0012&\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011 \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011\u0018\u00010\u00100\u0010\u0018\u00010\u00120\u000f\"\u0010\u0008\u0000\u0010\u0011*\n \n*\u0004\u0018\u00010\u00130\u00132d\u0010\u0006\u001a`\u0012*\u0008\u0001\u0012&\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011 \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011\u0018\u00010\u00150\u0015 \n*.\u0012(\u0012&\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011 \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011\u0018\u00010\u00150\u0015\u0018\u00010\u00160\u00142\u0006\u0010\u0008\u001a\u00020\u00072\u000e\u0010\u0017\u001a\n \n*\u0004\u0018\u00010\t0\tH\u0096\u0001J\u008e\u0001\u0010\u0018\u001a\n \n*\u0004\u0018\u0001H\u0011H\u0011\"\u0010\u0008\u0000\u0010\u0011*\n \n*\u0004\u0018\u00010\u00130\u00132d\u0010\u0006\u001a`\u0012*\u0008\u0001\u0012&\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011 \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011\u0018\u00010\u00150\u0015 \n*.\u0012(\u0012&\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011 \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011\u0018\u00010\u00150\u0015\u0018\u00010\u00160\u0014H\u0096\u0001\u00a2\u0006\u0002\u0010\u0019J\u00a6\u0001\u0010\u0018\u001a\n \n*\u0004\u0018\u0001H\u0011H\u0011\"\u0010\u0008\u0000\u0010\u0011*\n \n*\u0004\u0018\u00010\u00130\u00132d\u0010\u0006\u001a`\u0012*\u0008\u0001\u0012&\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011 \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011\u0018\u00010\u00150\u0015 \n*.\u0012(\u0012&\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011 \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011\u0018\u00010\u00150\u0015\u0018\u00010\u00160\u00142\u0006\u0010\u0008\u001a\u00020\u00072\u000e\u0010\u0017\u001a\n \n*\u0004\u0018\u00010\t0\tH\u0096\u0001\u00a2\u0006\u0002\u0010\u001aJ\t\u0010\u001b\u001a\u00020\u0005H\u0096\u0001J\t\u0010\u001c\u001a\u00020\u0005H\u0096\u0001J\t\u0010\u001d\u001a\u00020\u000cH\u0096\u0001J-\u0010\u001e\u001a&\u0012\u000c\u0012\n \n*\u0004\u0018\u00010\r0\r \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u00010\r0\r\u0018\u00010\u00120\u000fH\u0096\u0001J)\u0010\u001f\u001a\u0012\u0012\u0002\u0008\u0003 \n*\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00100\u00102\u000e\u0010\u0006\u001a\n \n*\u0004\u0018\u00010\r0\rH\u0096\u0001Jd\u0010\u001f\u001a&\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011 \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011\u0018\u00010\u00100\u0010\"\u0010\u0008\u0000\u0010\u0011*\n \n*\u0004\u0018\u00010\u00130\u00132\u000e\u0010\u0006\u001a\n \n*\u0004\u0018\u00010\r0\r2\u000e\u0010\u0008\u001a\n \n*\u0004\u0018\u0001H\u0011H\u0011H\u0096\u0001\u00a2\u0006\u0002\u0010 Jk\u0010\u001f\u001a&\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011 \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011\u0018\u00010\u00100\u0010\"\u0010\u0008\u0000\u0010\u0011*\n \n*\u0004\u0018\u00010\u00130\u00132*\u0010\u0006\u001a&\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011 \n*\u0012\u0012\u000c\u0012\n \n*\u0004\u0018\u0001H\u0011H\u0011\u0018\u00010\u00150\u0015H\u0096\u0001\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/ms/MinesweeperExecutorService;",
        "Ljava/util/concurrent/ExecutorService;",
        "executor",
        "(Ljava/util/concurrent/ExecutorService;)V",
        "awaitTermination",
        "",
        "p0",
        "",
        "p1",
        "Ljava/util/concurrent/TimeUnit;",
        "kotlin.jvm.PlatformType",
        "execute",
        "",
        "Ljava/lang/Runnable;",
        "invokeAll",
        "",
        "Ljava/util/concurrent/Future;",
        "T",
        "",
        "",
        "",
        "Ljava/util/concurrent/Callable;",
        "",
        "p2",
        "invokeAny",
        "(Ljava/util/Collection;)Ljava/lang/Object;",
        "(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;",
        "isShutdown",
        "isTerminated",
        "shutdown",
        "shutdownNow",
        "submit",
        "(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 1

    const-string v0, "executor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ms/MinesweeperExecutorService;->$$delegate_0:Ljava/util/concurrent/ExecutorService;

    return-void
.end method


# virtual methods
.method public awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/ms/MinesweeperExecutorService;->$$delegate_0:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1, p2, p3}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result p1

    return p1
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/ms/MinesweeperExecutorService;->$$delegate_0:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public invokeAll(Ljava/util/Collection;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/util/concurrent/Callable<",
            "TT;>;>;)",
            "Ljava/util/List<",
            "Ljava/util/concurrent/Future<",
            "TT;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ms/MinesweeperExecutorService;->$$delegate_0:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->invokeAll(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public invokeAll(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/util/concurrent/Callable<",
            "TT;>;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Ljava/util/List<",
            "Ljava/util/concurrent/Future<",
            "TT;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ms/MinesweeperExecutorService;->$$delegate_0:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/concurrent/ExecutorService;->invokeAll(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public invokeAny(Ljava/util/Collection;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/util/concurrent/Callable<",
            "TT;>;>;)TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ms/MinesweeperExecutorService;->$$delegate_0:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->invokeAny(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public invokeAny(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/util/concurrent/Callable<",
            "TT;>;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ms/MinesweeperExecutorService;->$$delegate_0:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/concurrent/ExecutorService;->invokeAny(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public isShutdown()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/ms/MinesweeperExecutorService;->$$delegate_0:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    return v0
.end method

.method public isTerminated()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/ms/MinesweeperExecutorService;->$$delegate_0:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isTerminated()Z

    move-result v0

    return v0
.end method

.method public shutdown()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/ms/MinesweeperExecutorService;->$$delegate_0:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    return-void
.end method

.method public shutdownNow()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ms/MinesweeperExecutorService;->$$delegate_0:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            ")",
            "Ljava/util/concurrent/Future<",
            "*>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ms/MinesweeperExecutorService;->$$delegate_0:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "Ljava/util/concurrent/Future<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ms/MinesweeperExecutorService;->$$delegate_0:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1, p2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable<",
            "TT;>;)",
            "Ljava/util/concurrent/Future<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ms/MinesweeperExecutorService;->$$delegate_0:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method
