.class public final Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;
.super Ljava/lang/Object;
.source "RealMerchantProfileUpdater.kt"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/merchantprofile/MerchantProfileUpdater;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealMerchantProfileUpdater.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealMerchantProfileUpdater.kt\ncom/squareup/merchantprofile/RealMerchantProfileUpdater\n*L\n1#1,344:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00ba\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000 H2\u00020\u00012\u00020\u0002:\u0001HBU\u0008\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u000c\u0012\u0008\u0008\u0001\u0010\r\u001a\u00020\u000e\u0012\u0008\u0008\u0001\u0010\u000f\u001a\u00020\u000e\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014BG\u0008\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0015\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u000e\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u00a2\u0006\u0002\u0010\u0018J\u0008\u0010\u001e\u001a\u00020\u001dH\u0002J\u0010\u0010\u001f\u001a\u00020\u001d2\u0006\u0010 \u001a\u00020!H\u0002J\u001c\u0010\"\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020%0$0#2\u0006\u0010 \u001a\u00020!H\u0002J\u0014\u0010&\u001a\u0004\u0018\u00010\'2\u0008\u0010(\u001a\u0004\u0018\u00010)H\u0002J\u0014\u0010*\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020%0$0#H\u0016J\u0010\u0010+\u001a\u00020\u001d2\u0006\u0010,\u001a\u00020-H\u0016J\u0008\u0010.\u001a\u00020\u001dH\u0016J8\u0010/\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020%0$0#2\u0006\u0010 \u001a\u00020!2\u0006\u00100\u001a\u0002012\u0008\u00102\u001a\u0004\u0018\u00010\'2\u0008\u00103\u001a\u0004\u0018\u00010\'H\u0002J\u0008\u00104\u001a\u00020\u001dH\u0002J\u0008\u00105\u001a\u00020\u001dH\u0002J$\u00106\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020%0$0#2\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u00020:H\u0016JA\u0010;\u001a\u00020\u001d2\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u00020:2\'\u0010<\u001a#\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020%0$\u00a2\u0006\u000c\u0008>\u0012\u0008\u0008?\u0012\u0004\u0008\u0008(@\u0012\u0004\u0012\u00020\u001d0=H\u0016J\u001c\u0010A\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020%0$0#2\u0006\u0010 \u001a\u00020!H\u0002J\u001c\u0010B\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020%0$0#2\u0006\u0010 \u001a\u00020!H\u0017J\u001c\u0010C\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020%0$0#2\u0006\u0010 \u001a\u00020!H\u0016J\u0010\u0010D\u001a\u0004\u0018\u00010\'*\u0004\u0018\u00010EH\u0002J\u0010\u0010F\u001a\u0004\u0018\u00010G*\u0004\u0018\u000101H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006I"
    }
    d2 = {
        "Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;",
        "Lmortar/Scoped;",
        "Lcom/squareup/merchantprofile/MerchantProfileUpdater;",
        "context",
        "Landroid/app/Application;",
        "accountService",
        "Lcom/squareup/account/PersistentAccountService;",
        "service",
        "Lcom/squareup/merchantprofile/MerchantProfileService;",
        "notificationManager",
        "Landroid/app/NotificationManager;",
        "fileExecutor",
        "Ljava/util/concurrent/Executor;",
        "rpcScheduler",
        "Lio/reactivex/Scheduler;",
        "mainScheduler",
        "notificationWrapper",
        "Lcom/squareup/notification/NotificationWrapper;",
        "appNameFormatter",
        "Lcom/squareup/util/AppNameFormatter;",
        "(Landroid/app/Application;Lcom/squareup/account/PersistentAccountService;Lcom/squareup/merchantprofile/MerchantProfileService;Landroid/app/NotificationManager;Ljava/util/concurrent/Executor;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/AppNameFormatter;)V",
        "Landroid/content/Context;",
        "errorNotification",
        "Landroid/app/Notification;",
        "(Landroid/content/Context;Lcom/squareup/account/PersistentAccountService;Lcom/squareup/merchantprofile/MerchantProfileService;Landroid/app/NotificationManager;Ljava/util/concurrent/Executor;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Landroid/app/Notification;)V",
        "disposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "updateProfileKillSwitch",
        "Lcom/jakewharton/rxrelay2/Relay;",
        "",
        "cancelErrorNotification",
        "cleanupAfterUpdate",
        "snapshot",
        "Lcom/squareup/merchantprofile/MerchantProfileSnapshot;",
        "doUpdateProfileCall",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/account/MerchantProfileResponse;",
        "getFeaturedImageMultipartBody",
        "Lokhttp3/MultipartBody$Part;",
        "uri",
        "Landroid/net/Uri;",
        "getMerchantProfile",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "postMerchantProfileUpdate",
        "cardColor",
        "",
        "profileImageTypedFile",
        "featuredImageTypedImageUri",
        "sendErrorNotification",
        "stopUpdateProfile",
        "updateBusinessAddress",
        "mobileBusiness",
        "",
        "address",
        "Lcom/squareup/address/Address;",
        "updateBusinessAddressAndSubscribe",
        "action",
        "Lkotlin/Function1;",
        "Lkotlin/ParameterName;",
        "name",
        "response",
        "updateCallFor",
        "updateProfile",
        "updateProfileInForeground",
        "getJpegMultipartBody",
        "Ljava/io/File;",
        "toRequestBody",
        "Lokhttp3/RequestBody;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$Companion;

.field public static final FAILURE:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final JPEG_MEDIA_TYPE:Lokhttp3/MediaType;

.field private static final TEXT_PLAIN:Lokhttp3/MediaType;


# instance fields
.field private final accountService:Lcom/squareup/account/PersistentAccountService;

.field private final context:Landroid/content/Context;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final errorNotification:Landroid/app/Notification;

.field private final fileExecutor:Ljava/util/concurrent/Executor;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final notificationManager:Landroid/app/NotificationManager;

.field private final rpcScheduler:Lio/reactivex/Scheduler;

.field private final service:Lcom/squareup/merchantprofile/MerchantProfileService;

.field private final updateProfileKillSwitch:Lcom/jakewharton/rxrelay2/Relay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/Relay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->Companion:Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$Companion;

    .line 311
    sget-object v0, Lokhttp3/MediaType;->Companion:Lokhttp3/MediaType$Companion;

    const-string v2, "text/plain"

    invoke-virtual {v0, v2}, Lokhttp3/MediaType$Companion;->get(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v0

    sput-object v0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->TEXT_PLAIN:Lokhttp3/MediaType;

    .line 312
    sget-object v0, Lokhttp3/MediaType;->Companion:Lokhttp3/MediaType$Companion;

    const-string v2, "image/jpeg"

    invoke-virtual {v0, v2}, Lokhttp3/MediaType$Companion;->get(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v0

    sput-object v0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->JPEG_MEDIA_TYPE:Lokhttp3/MediaType;

    .line 314
    new-instance v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    new-instance v2, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    new-instance v3, Lcom/squareup/server/account/MerchantProfileResponse;

    const/4 v4, 0x0

    const/4 v5, 0x3

    invoke-direct {v3, v1, v4, v5, v1}, Lcom/squareup/server/account/MerchantProfileResponse;-><init>(Lcom/squareup/server/account/MerchantProfileResponse$Entity;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v2, v3}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;-><init>(Ljava/lang/Object;)V

    check-cast v2, Lcom/squareup/receiving/ReceivedResponse;

    invoke-direct {v0, v2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;-><init>(Lcom/squareup/receiving/ReceivedResponse;)V

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    sput-object v0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->FAILURE:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/squareup/account/PersistentAccountService;Lcom/squareup/merchantprofile/MerchantProfileService;Landroid/app/NotificationManager;Ljava/util/concurrent/Executor;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/AppNameFormatter;)V
    .locals 13
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .param p6    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .param p7    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p1

    move-object/from16 v1, p8

    move-object/from16 v2, p9

    const-string v3, "context"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "accountService"

    move-object v6, p2

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "service"

    move-object/from16 v7, p3

    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "notificationManager"

    move-object/from16 v8, p4

    invoke-static {v8, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "fileExecutor"

    move-object/from16 v9, p5

    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "rpcScheduler"

    move-object/from16 v10, p6

    invoke-static {v10, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "mainScheduler"

    move-object/from16 v11, p7

    invoke-static {v11, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "notificationWrapper"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "appNameFormatter"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    move-object v5, v0

    check-cast v5, Landroid/content/Context;

    .line 96
    sget-object v0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->Companion:Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$Companion;

    invoke-static {v0, v5, v1, v2}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$Companion;->access$buildErrorNotification(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$Companion;Landroid/content/Context;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/AppNameFormatter;)Landroid/app/Notification;

    move-result-object v12

    move-object v4, p0

    .line 88
    invoke-direct/range {v4 .. v12}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;-><init>(Landroid/content/Context;Lcom/squareup/account/PersistentAccountService;Lcom/squareup/merchantprofile/MerchantProfileService;Landroid/app/NotificationManager;Ljava/util/concurrent/Executor;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Landroid/app/Notification;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/squareup/account/PersistentAccountService;Lcom/squareup/merchantprofile/MerchantProfileService;Landroid/app/NotificationManager;Ljava/util/concurrent/Executor;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Landroid/app/Notification;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "service"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationManager"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileExecutor"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rpcScheduler"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorNotification"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->accountService:Lcom/squareup/account/PersistentAccountService;

    iput-object p3, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->service:Lcom/squareup/merchantprofile/MerchantProfileService;

    iput-object p4, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->notificationManager:Landroid/app/NotificationManager;

    iput-object p5, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->fileExecutor:Ljava/util/concurrent/Executor;

    iput-object p6, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->rpcScheduler:Lio/reactivex/Scheduler;

    iput-object p7, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p8, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->errorNotification:Landroid/app/Notification;

    .line 73
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/jakewharton/rxrelay2/Relay;

    iput-object p1, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->updateProfileKillSwitch:Lcom/jakewharton/rxrelay2/Relay;

    .line 74
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    return-void
.end method

.method public static final synthetic access$cancelErrorNotification(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->cancelErrorNotification()V

    return-void
.end method

.method public static final synthetic access$cleanupAfterUpdate(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)V
    .locals 0

    .line 61
    invoke-direct {p0, p1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->cleanupAfterUpdate(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)V

    return-void
.end method

.method public static final synthetic access$doUpdateProfileCall(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)Lio/reactivex/Single;
    .locals 0

    .line 61
    invoke-direct {p0, p1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->doUpdateProfileCall(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAccountService$p(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;)Lcom/squareup/account/PersistentAccountService;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->accountService:Lcom/squareup/account/PersistentAccountService;

    return-object p0
.end method

.method public static final synthetic access$sendErrorNotification(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->sendErrorNotification()V

    return-void
.end method

.method private final cancelErrorNotification()V
    .locals 2

    .line 306
    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->notificationManager:Landroid/app/NotificationManager;

    sget v1, Lcom/squareup/merchantprofile/R$id;->notification_profile_error:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method private final cleanupAfterUpdate(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)V
    .locals 1

    .line 210
    iget-object p1, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->logo:Ljava/io/File;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->fileExecutor:Ljava/util/concurrent/Executor;

    invoke-static {p1, v0}, Lcom/squareup/util/Files;->deleteSilently(Ljava/io/File;Ljava/util/concurrent/Executor;)V

    :cond_0
    return-void
.end method

.method private final doUpdateProfileCall(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/merchantprofile/MerchantProfileSnapshot;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;>;"
        }
    .end annotation

    .line 229
    sget-object v0, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iget v1, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->cardColor:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    const-string v1, "%06X"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "java.lang.String.format(format, *args)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    iget-object v1, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->logo:Ljava/io/File;

    invoke-direct {p0, v1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->getJpegMultipartBody(Ljava/io/File;)Lokhttp3/MultipartBody$Part;

    move-result-object v1

    .line 234
    iget-object v2, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImage:Landroid/net/Uri;

    invoke-direct {p0, v2}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->getFeaturedImageMultipartBody(Landroid/net/Uri;)Lokhttp3/MultipartBody$Part;

    move-result-object v2

    .line 230
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->postMerchantProfileUpdate(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;Ljava/lang/String;Lokhttp3/MultipartBody$Part;Lokhttp3/MultipartBody$Part;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method private final getFeaturedImageMultipartBody(Landroid/net/Uri;)Lokhttp3/MultipartBody$Part;
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 250
    :cond_0
    new-instance v1, Lcom/squareup/server/ContentProviderImageResolver;

    iget-object v2, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->context:Landroid/content/Context;

    invoke-direct {v1, v2, p1}, Lcom/squareup/server/ContentProviderImageResolver;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 252
    :try_start_0
    invoke-virtual {v1}, Lcom/squareup/server/ContentProviderImageResolver;->toByteString()Lokio/ByteString;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_1

    return-object v0

    .line 259
    :cond_1
    sget-object v0, Lokhttp3/MediaType;->Companion:Lokhttp3/MediaType$Companion;

    invoke-virtual {v1}, Lcom/squareup/server/ContentProviderImageResolver;->mimeType()Ljava/lang/String;

    move-result-object v2

    const-string v3, "featuredImageUri.mimeType()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    invoke-virtual {v0, v2}, Lokhttp3/MediaType$Companion;->get(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v0

    .line 261
    sget-object v2, Lokhttp3/MultipartBody$Part;->Companion:Lokhttp3/MultipartBody$Part$Companion;

    .line 262
    invoke-virtual {v1}, Lcom/squareup/server/ContentProviderImageResolver;->fileName()Ljava/lang/String;

    move-result-object v1

    .line 263
    sget-object v3, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    invoke-virtual {v3, p1, v0}, Lokhttp3/RequestBody$Companion;->create(Lokio/ByteString;Lokhttp3/MediaType;)Lokhttp3/RequestBody;

    move-result-object p1

    const-string v0, "featured_image"

    .line 261
    invoke-virtual {v2, v0, v1, p1}, Lokhttp3/MultipartBody$Part$Companion;->createFormData(Ljava/lang/String;Ljava/lang/String;Lokhttp3/RequestBody;)Lokhttp3/MultipartBody$Part;

    move-result-object p1

    return-object p1

    :catch_0
    return-object v0
.end method

.method private final getJpegMultipartBody(Ljava/io/File;)Lokhttp3/MultipartBody$Part;
    .locals 4

    if-eqz p1, :cond_0

    .line 239
    sget-object v0, Lokhttp3/MultipartBody$Part;->Companion:Lokhttp3/MultipartBody$Part$Companion;

    .line 241
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 242
    sget-object v2, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    sget-object v3, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->JPEG_MEDIA_TYPE:Lokhttp3/MediaType;

    invoke-virtual {v2, p1, v3}, Lokhttp3/RequestBody$Companion;->create(Ljava/io/File;Lokhttp3/MediaType;)Lokhttp3/RequestBody;

    move-result-object p1

    const-string v2, "profile_image"

    .line 239
    invoke-virtual {v0, v2, v1, p1}, Lokhttp3/MultipartBody$Part$Companion;->createFormData(Ljava/lang/String;Ljava/lang/String;Lokhttp3/RequestBody;)Lokhttp3/MultipartBody$Part;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final postMerchantProfileUpdate(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;Ljava/lang/String;Lokhttp3/MultipartBody$Part;Lokhttp3/MultipartBody$Part;)Lio/reactivex/Single;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/merchantprofile/MerchantProfileSnapshot;",
            "Ljava/lang/String;",
            "Lokhttp3/MultipartBody$Part;",
            "Lokhttp3/MultipartBody$Part;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    .line 273
    iget-object v2, v0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->service:Lcom/squareup/merchantprofile/MerchantProfileService;

    .line 275
    iget-boolean v3, v1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->appearInMarket:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 276
    iget-boolean v4, v1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->mobileBusiness:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 277
    iget-boolean v5, v1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->publishItems:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v8, p2

    .line 280
    invoke-direct {v0, v8}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v8

    .line 281
    iget-object v9, v1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->businessName:Ljava/lang/String;

    invoke-direct {v0, v9}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v9

    .line 282
    iget-object v10, v1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->address:Lcom/squareup/address/Address;

    iget-object v10, v10, Lcom/squareup/address/Address;->street:Ljava/lang/String;

    invoke-direct {v0, v10}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v10

    .line 283
    iget-object v11, v1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->address:Lcom/squareup/address/Address;

    iget-object v11, v11, Lcom/squareup/address/Address;->apartment:Ljava/lang/String;

    invoke-direct {v0, v11}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v11

    .line 284
    iget-object v12, v1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->address:Lcom/squareup/address/Address;

    iget-object v12, v12, Lcom/squareup/address/Address;->city:Ljava/lang/String;

    invoke-direct {v0, v12}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v12

    .line 285
    iget-object v13, v1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->address:Lcom/squareup/address/Address;

    iget-object v13, v13, Lcom/squareup/address/Address;->state:Ljava/lang/String;

    invoke-direct {v0, v13}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v13

    .line 286
    iget-object v14, v1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->address:Lcom/squareup/address/Address;

    iget-object v14, v14, Lcom/squareup/address/Address;->postalCode:Ljava/lang/String;

    invoke-direct {v0, v14}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v14

    .line 287
    iget-object v15, v1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    iget-object v15, v15, Lcom/squareup/merchantprofile/ContactInfo;->phone:Ljava/lang/String;

    invoke-direct {v0, v15}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v15

    move-object/from16 p3, v2

    .line 288
    iget-object v2, v1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    iget-object v2, v2, Lcom/squareup/merchantprofile/ContactInfo;->email:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v16

    .line 289
    iget-object v2, v1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    iget-object v2, v2, Lcom/squareup/merchantprofile/ContactInfo;->facebook:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v17

    .line 290
    iget-object v2, v1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    iget-object v2, v2, Lcom/squareup/merchantprofile/ContactInfo;->twitter:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v18

    .line 291
    iget-object v2, v1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    iget-object v2, v2, Lcom/squareup/merchantprofile/ContactInfo;->website:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v19

    .line 292
    iget-object v1, v1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->description:Ljava/lang/String;

    .line 293
    invoke-direct {v0, v1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v20

    move-object/from16 v2, p3

    .line 274
    invoke-interface/range {v2 .. v20}, Lcom/squareup/merchantprofile/MerchantProfileService;->updateMerchantProfile(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokhttp3/MultipartBody$Part;Lokhttp3/MultipartBody$Part;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object v1

    .line 295
    invoke-virtual {v1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v1

    .line 296
    new-instance v2, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$postMerchantProfileUpdate$1;

    invoke-direct {v2, v0}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$postMerchantProfileUpdate$1;-><init>(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v1

    const-string v2, "service\n        .updateM\u2026ProfileUpdate()\n        }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method

.method private final sendErrorNotification()V
    .locals 3

    .line 301
    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->notificationManager:Landroid/app/NotificationManager;

    .line 302
    sget v1, Lcom/squareup/merchantprofile/R$id;->notification_profile_error:I

    iget-object v2, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->errorNotification:Landroid/app/Notification;

    .line 301
    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method private final stopUpdateProfile()V
    .locals 2

    .line 206
    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->updateProfileKillSwitch:Lcom/jakewharton/rxrelay2/Relay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/Relay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;
    .locals 2

    if-eqz p1, :cond_0

    .line 308
    sget-object v0, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    sget-object v1, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->TEXT_PLAIN:Lokhttp3/MediaType;

    invoke-virtual {v0, p1, v1}, Lokhttp3/RequestBody$Companion;->create(Ljava/lang/String;Lokhttp3/MediaType;)Lokhttp3/RequestBody;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final updateCallFor(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/merchantprofile/MerchantProfileSnapshot;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;>;"
        }
    .end annotation

    .line 222
    new-instance v0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateCallFor$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateCallFor$1;-><init>(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object p1

    .line 223
    sget-object v0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateCallFor$2;->INSTANCE:Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateCallFor$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single\n        .fromCall\u2026leResponse>>? -> single }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public getMerchantProfile()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;>;"
        }
    .end annotation

    .line 200
    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->service:Lcom/squareup/merchantprofile/MerchantProfileService;

    .line 201
    invoke-interface {v0}, Lcom/squareup/merchantprofile/MerchantProfileService;->getMerchantProfile()Lcom/squareup/server/SimpleStandardResponse;

    move-result-object v0

    .line 202
    invoke-virtual {v0}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 102
    invoke-direct {p0}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->stopUpdateProfile()V

    .line 103
    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public updateBusinessAddress(ZLcom/squareup/address/Address;)Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/address/Address;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "address"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    invoke-direct {p0}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->stopUpdateProfile()V

    .line 132
    iget-object v1, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->service:Lcom/squareup/merchantprofile/MerchantProfileService;

    .line 133
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 134
    iget-object p1, p2, Lcom/squareup/address/Address;->street:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v3

    .line 135
    iget-object p1, p2, Lcom/squareup/address/Address;->apartment:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v4

    .line 136
    iget-object p1, p2, Lcom/squareup/address/Address;->city:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v5

    .line 137
    iget-object p1, p2, Lcom/squareup/address/Address;->state:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v6

    .line 138
    iget-object p1, p2, Lcom/squareup/address/Address;->postalCode:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->toRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v7

    .line 132
    invoke-interface/range {v1 .. v7}, Lcom/squareup/merchantprofile/MerchantProfileService;->updateBusinessAddress(Ljava/lang/Boolean;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p1

    .line 140
    invoke-virtual {p1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 141
    iget-object p2, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->rpcScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    .line 142
    iget-object p2, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    const-string p1, "service.updateBusinessAd\u2026.observeOn(mainScheduler)"

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 147
    iget-object v5, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->mainScheduler:Lio/reactivex/Scheduler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x5

    .line 143
    invoke-static/range {v0 .. v5}, Lcom/squareup/receiving/StandardReceiverKt;->retryExponentialBackoff(Lio/reactivex/Single;IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public updateBusinessAddressAndSubscribe(ZLcom/squareup/address/Address;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/address/Address;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "address"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 157
    invoke-virtual {p0, p1, p2}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->updateBusinessAddress(ZLcom/squareup/address/Address;)Lio/reactivex/Single;

    move-result-object p1

    .line 158
    new-instance p2, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateBusinessAddressAndSubscribe$1;

    invoke-direct {p2, p3}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateBusinessAddressAndSubscribe$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast p2, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 156
    invoke-virtual {v0, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public updateProfile(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/merchantprofile/MerchantProfileSnapshot;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    invoke-virtual {p0, p1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->updateProfileInForeground(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)Lio/reactivex/Single;

    move-result-object v0

    .line 168
    new-instance v1, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$1;

    invoke-direct {v1, p0}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$1;-><init>(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    .line 181
    new-instance v1, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$2;-><init>(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)V

    check-cast v1, Lio/reactivex/functions/Action;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnDispose(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object p1

    .line 182
    sget-object v0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$3;->INSTANCE:Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$3;

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string/jumbo v0, "updateProfileInForegroun\u2026e canceled.\")\n          }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->updateProfileKillSwitch:Lcom/jakewharton/rxrelay2/Relay;

    .line 193
    sget-object v1, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$1;->INSTANCE:Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/Relay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 194
    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    check-cast p1, Lio/reactivex/ObservableSource;

    invoke-virtual {v0, p1}, Lio/reactivex/Observable;->ambWith(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object p1

    const-wide/16 v0, 0x1

    .line 195
    invoke-virtual {p1, v0, v1}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object p1

    .line 196
    sget-object v0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->FAILURE:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->single(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string/jumbo v0, "updateProfileKillSwitch\n\u2026\n        .single(FAILURE)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public updateProfileInForeground(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)Lio/reactivex/Single;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/merchantprofile/MerchantProfileSnapshot;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    invoke-direct {p0}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->stopUpdateProfile()V

    .line 111
    invoke-direct {p0, p1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->updateCallFor(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)Lio/reactivex/Single;

    move-result-object p1

    .line 112
    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->rpcScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    .line 113
    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    const-string/jumbo p1, "updateCallFor(snapshot)\n\u2026.observeOn(mainScheduler)"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 120
    iget-object v6, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->mainScheduler:Lio/reactivex/Scheduler;

    const/4 v2, 0x2

    const-wide/16 v3, 0x5

    .line 116
    invoke-static/range {v1 .. v6}, Lcom/squareup/receiving/StandardReceiverKt;->retryExponentialBackoff(Lio/reactivex/Single;IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
