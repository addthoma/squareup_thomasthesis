.class public Lcom/squareup/merchantprofile/MerchantProfileSnapshot;
.super Ljava/lang/Object;
.source "MerchantProfileSnapshot.java"


# static fields
.field public static final EMPTY:Lcom/squareup/merchantprofile/MerchantProfileSnapshot;


# instance fields
.field public final address:Lcom/squareup/address/Address;

.field public final appearInMarket:Z

.field public final businessName:Ljava/lang/String;

.field public final cardColor:I

.field public final contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

.field public final description:Ljava/lang/String;

.field public final featuredImageHeight:I

.field public final featuredImageUrl:Ljava/lang/String;

.field public final featuredImageWidth:I

.field public final logo:Ljava/io/File;

.field public final mobileBusiness:Z

.field public final pendingFeaturedImage:Landroid/net/Uri;

.field public final pendingFeaturedImageHeight:I

.field public final pendingFeaturedImageWidth:I

.field public final photoOnReceipt:Z

.field public final profileImageUrl:Ljava/lang/String;

.field public final publishItems:Z


# direct methods
.method static constructor <clinit>()V
    .locals 19

    .line 11
    new-instance v18, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;

    move-object/from16 v0, v18

    sget-object v10, Lcom/squareup/address/Address;->EMPTY:Lcom/squareup/address/Address;

    sget-object v11, Lcom/squareup/merchantprofile/ContactInfo;->EMPTY:Lcom/squareup/merchantprofile/ContactInfo;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-direct/range {v0 .. v17}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/io/File;ZILcom/squareup/address/Address;Lcom/squareup/merchantprofile/ContactInfo;Ljava/lang/String;ZZLandroid/net/Uri;II)V

    sput-object v18, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->EMPTY:Lcom/squareup/merchantprofile/MerchantProfileSnapshot;

    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/io/File;ZILcom/squareup/address/Address;Lcom/squareup/merchantprofile/ContactInfo;Ljava/lang/String;ZZLandroid/net/Uri;II)V
    .locals 2

    move-object v0, p0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move v1, p1

    .line 38
    iput-boolean v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->appearInMarket:Z

    move-object v1, p2

    .line 39
    iput-object v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->businessName:Ljava/lang/String;

    move-object v1, p3

    .line 40
    iput-object v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->profileImageUrl:Ljava/lang/String;

    move-object v1, p4

    .line 41
    iput-object v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->featuredImageUrl:Ljava/lang/String;

    move v1, p5

    .line 42
    iput v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->featuredImageWidth:I

    move v1, p6

    .line 43
    iput v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->featuredImageHeight:I

    move-object v1, p7

    .line 44
    iput-object v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->logo:Ljava/io/File;

    move v1, p8

    .line 45
    iput-boolean v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->mobileBusiness:Z

    move v1, p9

    .line 46
    iput v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->cardColor:I

    move-object v1, p10

    .line 47
    iput-object v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->address:Lcom/squareup/address/Address;

    move-object v1, p11

    .line 48
    iput-object v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    move-object v1, p12

    .line 49
    iput-object v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->description:Ljava/lang/String;

    move v1, p13

    .line 50
    iput-boolean v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->publishItems:Z

    move/from16 v1, p14

    .line 51
    iput-boolean v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->photoOnReceipt:Z

    move-object/from16 v1, p15

    .line 52
    iput-object v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImage:Landroid/net/Uri;

    move/from16 v1, p16

    .line 53
    iput v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImageWidth:I

    move/from16 v1, p17

    .line 54
    iput v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImageHeight:I

    return-void
.end method

.method private static redactedOrBlank(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 82
    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    const-string p0, "<blank>"

    goto :goto_0

    :cond_0
    const-string p0, "REDACTED"

    :goto_0
    return-object p0
.end method


# virtual methods
.method public asStringWithoutPIIForLogs()Ljava/lang/String;
    .locals 4

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "appearInMarket: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->appearInMarket:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "\npublishItems: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->publishItems:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "\nprofileImageUrl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nlogo: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->logo:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\nfeaturedImageUrl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->featuredImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->featuredImageWidth:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->featuredImageHeight:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ")\npendingFeaturedImage: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImage:Landroid/net/Uri;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImageWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImageHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")\ncardColor: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->cardColor:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "\nbusinessName: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->businessName:Ljava/lang/String;

    .line 67
    invoke-static {v1}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->redactedOrBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nstreet: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->address:Lcom/squareup/address/Address;

    iget-object v1, v1, Lcom/squareup/address/Address;->street:Ljava/lang/String;

    .line 68
    invoke-static {v1}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->redactedOrBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\napartment: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->address:Lcom/squareup/address/Address;

    iget-object v1, v1, Lcom/squareup/address/Address;->apartment:Ljava/lang/String;

    .line 69
    invoke-static {v1}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->redactedOrBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\ncity: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->address:Lcom/squareup/address/Address;

    iget-object v1, v1, Lcom/squareup/address/Address;->city:Ljava/lang/String;

    .line 70
    invoke-static {v1}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->redactedOrBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nstate: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->address:Lcom/squareup/address/Address;

    iget-object v1, v1, Lcom/squareup/address/Address;->state:Ljava/lang/String;

    .line 71
    invoke-static {v1}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->redactedOrBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\npostalCode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->address:Lcom/squareup/address/Address;

    iget-object v1, v1, Lcom/squareup/address/Address;->postalCode:Ljava/lang/String;

    .line 72
    invoke-static {v1}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->redactedOrBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nphone: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    iget-object v1, v1, Lcom/squareup/merchantprofile/ContactInfo;->phone:Ljava/lang/String;

    .line 73
    invoke-static {v1}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->redactedOrBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nemail: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    iget-object v1, v1, Lcom/squareup/merchantprofile/ContactInfo;->email:Ljava/lang/String;

    .line 74
    invoke-static {v1}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->redactedOrBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nfacebook: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    iget-object v1, v1, Lcom/squareup/merchantprofile/ContactInfo;->facebook:Ljava/lang/String;

    .line 75
    invoke-static {v1}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->redactedOrBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\ntwitter: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    iget-object v1, v1, Lcom/squareup/merchantprofile/ContactInfo;->twitter:Ljava/lang/String;

    .line 76
    invoke-static {v1}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->redactedOrBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nwebsite: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    iget-object v1, v1, Lcom/squareup/merchantprofile/ContactInfo;->website:Ljava/lang/String;

    .line 77
    invoke-static {v1}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->redactedOrBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\ndescription: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->description:Ljava/lang/String;

    .line 78
    invoke-static {v1}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->redactedOrBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_1b

    .line 87
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_8

    .line 89
    :cond_1
    check-cast p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;

    .line 91
    iget-boolean v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->appearInMarket:Z

    iget-boolean v3, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->appearInMarket:Z

    if-eq v2, v3, :cond_2

    return v1

    .line 92
    :cond_2
    iget v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->featuredImageWidth:I

    iget v3, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->featuredImageWidth:I

    if-eq v2, v3, :cond_3

    return v1

    .line 93
    :cond_3
    iget v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->featuredImageHeight:I

    iget v3, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->featuredImageHeight:I

    if-eq v2, v3, :cond_4

    return v1

    .line 94
    :cond_4
    iget-boolean v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->mobileBusiness:Z

    iget-boolean v3, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->mobileBusiness:Z

    if-eq v2, v3, :cond_5

    return v1

    .line 95
    :cond_5
    iget v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->cardColor:I

    iget v3, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->cardColor:I

    if-eq v2, v3, :cond_6

    return v1

    .line 96
    :cond_6
    iget-boolean v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->publishItems:Z

    iget-boolean v3, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->publishItems:Z

    if-eq v2, v3, :cond_7

    return v1

    .line 97
    :cond_7
    iget-boolean v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->photoOnReceipt:Z

    iget-boolean v3, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->photoOnReceipt:Z

    if-eq v2, v3, :cond_8

    return v1

    .line 98
    :cond_8
    iget v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImageWidth:I

    iget v3, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImageWidth:I

    if-eq v2, v3, :cond_9

    return v1

    .line 99
    :cond_9
    iget v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImageHeight:I

    iget v3, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImageHeight:I

    if-eq v2, v3, :cond_a

    return v1

    .line 100
    :cond_a
    iget-object v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->businessName:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v3, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->businessName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    goto :goto_0

    :cond_b
    iget-object v2, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->businessName:Ljava/lang/String;

    if-eqz v2, :cond_c

    :goto_0
    return v1

    .line 104
    :cond_c
    iget-object v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->profileImageUrl:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v3, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    goto :goto_1

    :cond_d
    iget-object v2, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->profileImageUrl:Ljava/lang/String;

    if-eqz v2, :cond_e

    :goto_1
    return v1

    .line 108
    :cond_e
    iget-object v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->featuredImageUrl:Ljava/lang/String;

    if-eqz v2, :cond_f

    iget-object v3, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->featuredImageUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    goto :goto_2

    :cond_f
    iget-object v2, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->featuredImageUrl:Ljava/lang/String;

    if-eqz v2, :cond_10

    :goto_2
    return v1

    .line 112
    :cond_10
    iget-object v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->logo:Ljava/io/File;

    if-eqz v2, :cond_11

    iget-object v3, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->logo:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    goto :goto_3

    :cond_11
    iget-object v2, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->logo:Ljava/io/File;

    if-eqz v2, :cond_12

    :goto_3
    return v1

    .line 113
    :cond_12
    iget-object v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->address:Lcom/squareup/address/Address;

    if-eqz v2, :cond_13

    iget-object v3, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->address:Lcom/squareup/address/Address;

    invoke-virtual {v2, v3}, Lcom/squareup/address/Address;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    goto :goto_4

    :cond_13
    iget-object v2, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->address:Lcom/squareup/address/Address;

    if-eqz v2, :cond_14

    :goto_4
    return v1

    .line 116
    :cond_14
    iget-object v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    if-eqz v2, :cond_15

    iget-object v3, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    invoke-virtual {v2, v3}, Lcom/squareup/merchantprofile/ContactInfo;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    goto :goto_5

    :cond_15
    iget-object v2, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    if-eqz v2, :cond_16

    :goto_5
    return v1

    .line 120
    :cond_16
    iget-object v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->description:Ljava/lang/String;

    if-eqz v2, :cond_17

    iget-object v3, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->description:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    goto :goto_6

    :cond_17
    iget-object v2, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->description:Ljava/lang/String;

    if-eqz v2, :cond_18

    :goto_6
    return v1

    .line 124
    :cond_18
    iget-object v2, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImage:Landroid/net/Uri;

    iget-object p1, p1, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImage:Landroid/net/Uri;

    if-eqz v2, :cond_19

    invoke-virtual {v2, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1a

    goto :goto_7

    :cond_19
    if-eqz p1, :cond_1a

    :goto_7
    return v1

    :cond_1a
    return v0

    :cond_1b
    :goto_8
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/Object;

    .line 133
    iget-boolean v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->appearInMarket:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->businessName:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->profileImageUrl:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->featuredImageUrl:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->featuredImageWidth:I

    .line 134
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->featuredImageHeight:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->logo:Ljava/io/File;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->mobileBusiness:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->cardColor:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x8

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->address:Lcom/squareup/address/Address;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->description:Ljava/lang/String;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->publishItems:Z

    .line 135
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/16 v2, 0xc

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->photoOnReceipt:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/16 v2, 0xd

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImage:Landroid/net/Uri;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImageWidth:I

    .line 136
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xf

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImageHeight:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x10

    aput-object v1, v0, v2

    .line 133
    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
