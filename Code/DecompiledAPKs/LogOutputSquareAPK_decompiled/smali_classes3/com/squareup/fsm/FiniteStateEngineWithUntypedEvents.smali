.class public final Lcom/squareup/fsm/FiniteStateEngineWithUntypedEvents;
.super Lcom/squareup/fsm/AbstractFiniteStateEngine;
.source "FiniteStateEngineWithUntypedEvents.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/fsm/AbstractFiniteStateEngine<",
        "TS;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public varargs constructor <init>([Lcom/squareup/fsm/Rule;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1}, Lcom/squareup/fsm/AbstractFiniteStateEngine;-><init>([Lcom/squareup/fsm/Rule;)V

    return-void
.end method


# virtual methods
.method public onEvent(Ljava/lang/Object;)V
    .locals 0

    .line 19
    invoke-virtual {p0, p1}, Lcom/squareup/fsm/FiniteStateEngineWithUntypedEvents;->doOnEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic peekStateToSave()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-super {p0}, Lcom/squareup/fsm/AbstractFiniteStateEngine;->peekStateToSave()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic startFromState(Ljava/lang/Object;)V
    .locals 0

    .line 10
    invoke-super {p0, p1}, Lcom/squareup/fsm/AbstractFiniteStateEngine;->startFromState(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .line 10
    invoke-super {p0}, Lcom/squareup/fsm/AbstractFiniteStateEngine;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
