.class public final Lcom/squareup/money/WholeUnitMoneyHelper_Factory;
.super Ljava/lang/Object;
.source "WholeUnitMoneyHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/money/WholeUnitMoneyHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleDigitsKeyListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/text/method/DigitsKeyListener;",
            ">;"
        }
    .end annotation
.end field

.field private final shortMoneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/text/method/DigitsKeyListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/money/WholeUnitMoneyHelper_Factory;->moneyLocaleDigitsKeyListenerProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/money/WholeUnitMoneyHelper_Factory;->shortMoneyFormatterProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/money/WholeUnitMoneyHelper_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/money/WholeUnitMoneyHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/text/method/DigitsKeyListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Lcom/squareup/money/WholeUnitMoneyHelper_Factory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/money/WholeUnitMoneyHelper_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/money/WholeUnitMoneyHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/money/WholeUnitMoneyHelper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/text/method/DigitsKeyListener;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/squareup/money/WholeUnitMoneyHelper;"
        }
    .end annotation

    .line 50
    new-instance v0, Lcom/squareup/money/WholeUnitMoneyHelper;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/money/WholeUnitMoneyHelper;-><init>(Ljavax/inject/Provider;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/money/WholeUnitMoneyHelper;
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/squareup/money/WholeUnitMoneyHelper_Factory;->moneyLocaleDigitsKeyListenerProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/money/WholeUnitMoneyHelper_Factory;->shortMoneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/money/WholeUnitMoneyHelper_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/WholeUnitMoneyHelper_Factory;->newInstance(Ljavax/inject/Provider;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/money/WholeUnitMoneyHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/money/WholeUnitMoneyHelper_Factory;->get()Lcom/squareup/money/WholeUnitMoneyHelper;

    move-result-object v0

    return-object v0
.end method
