.class public final Lcom/squareup/money/MoneyMath;
.super Ljava/lang/Object;
.source "MoneyMath.java"


# static fields
.field private static final BASIS_POINTS_PER_HALF_UNIT:I = 0x1388

.field private static final BASIS_POINTS_PER_UNIT:I = 0x2710

.field public static final COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/money/MoneyMath$1;

    invoke-direct {v0}, Lcom/squareup/money/MoneyMath$1;-><init>()V

    sput-object v0, Lcom/squareup/money/MoneyMath;->COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static asDecimalWholeUnits(Lcom/squareup/protos/common/Money;)Ljava/math/BigDecimal;
    .locals 4

    .line 63
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0}, Lcom/squareup/currency_db/Currencies;->getSubunitsPerUnit(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result v0

    .line 64
    new-instance v1, Ljava/math/BigDecimal;

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance p0, Ljava/math/BigDecimal;

    invoke-direct {p0, v0}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v1, p0}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p0

    return-object p0
.end method

.method private static assertSameCurrencies(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 3

    .line 80
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    if-ne v0, v1, :cond_0

    return-void

    .line 81
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot do math with different currencies: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, ", "

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static divide(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lcom/squareup/protos/common/Money;
    .locals 3

    .line 222
    new-instance v0, Ljava/math/BigDecimal;

    iget-object v1, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/math/BigDecimal;-><init>(J)V

    invoke-virtual {v0, p1, p2}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {p1}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide p1

    .line 223
    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, p2, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static divide(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;
    .locals 3

    .line 208
    invoke-static {p0, p1}, Lcom/squareup/money/MoneyMath;->assertSameCurrencies(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    .line 209
    new-instance v0, Ljava/math/BigDecimal;

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance p0, Ljava/math/BigDecimal;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Ljava/math/BigDecimal;-><init>(J)V

    const/4 p1, 0x2

    invoke-virtual {v0, p0, p1, p2}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object p0

    return-object p0
.end method

.method public static greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z
    .locals 3

    .line 248
    invoke-static {p0, p1}, Lcom/squareup/money/MoneyMath;->assertSameCurrencies(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    .line 249
    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object p0, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide p0

    cmp-long v2, v0, p0

    if-lez v2, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static greaterThanNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z
    .locals 0

    if-eqz p0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 257
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p0

    return p0

    :cond_1
    :goto_0
    const/4 p0, 0x0

    return p0
.end method

.method public static greaterThanOrEqualTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z
    .locals 3

    .line 262
    invoke-static {p0, p1}, Lcom/squareup/money/MoneyMath;->assertSameCurrencies(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    .line 263
    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object p0, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide p0

    cmp-long v2, v0, p0

    if-ltz v2, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static greaterThanZero(Lcom/squareup/protos/common/Money;)Z
    .locals 3

    if-eqz p0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    .line 278
    iget-object v2, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p0

    return p0

    .line 276
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Money is null or doesn\'t have a currency code."

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static inRange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z
    .locals 0

    if-eqz p0, :cond_0

    .line 269
    invoke-static {p0, p1}, Lcom/squareup/money/MoneyMath;->greaterThanOrEqualTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 270
    invoke-static {p2, p0}, Lcom/squareup/money/MoneyMath;->greaterThanOrEqualTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isEqual(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z
    .locals 2

    .line 237
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    if-eq v0, v1, :cond_0

    const/4 p0, 0x0

    return p0

    .line 238
    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p0, p1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static isEqualNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z
    .locals 0

    if-eqz p0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 243
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/money/MoneyMath;->isEqual(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p0

    return p0

    :cond_1
    :goto_0
    if-ne p0, p1, :cond_2

    const/4 p0, 0x1

    goto :goto_1

    :cond_2
    const/4 p0, 0x0

    :goto_1
    return p0
.end method

.method public static isNegative(Lcom/squareup/protos/common/Money;)Z
    .locals 4

    if-eqz p0, :cond_0

    .line 44
    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p0, v0, v2

    if-gez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isOneWholeUnitOrMore(Lcom/squareup/protos/common/Money;)Z
    .locals 4

    .line 76
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p0}, Lcom/squareup/currency_db/Currencies;->getSubunitsPerUnit(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result p0

    int-to-long v2, p0

    cmp-long p0, v0, v2

    if-ltz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isPositive(Lcom/squareup/protos/common/Money;)Z
    .locals 4

    if-eqz p0, :cond_0

    .line 40
    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p0, v0, v2

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isWholeUnits(Lcom/squareup/protos/common/Money;)Z
    .locals 4

    .line 68
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p0}, Lcom/squareup/currency_db/Currencies;->getSubunitsPerUnit(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result p0

    int-to-long v2, p0

    rem-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long p0, v0, v2

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isZero(Lcom/squareup/protos/common/Money;)Z
    .locals 4

    if-eqz p0, :cond_0

    .line 48
    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p0, v0, v2

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static max(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 5

    .line 233
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    goto :goto_0

    :cond_0
    move-object p0, p1

    :goto_0
    return-object p0
.end method

.method public static min(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 5

    .line 228
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    goto :goto_0

    :cond_0
    move-object p0, p1

    :goto_0
    return-object p0
.end method

.method public static multiply(Lcom/squareup/protos/common/Money;I)Lcom/squareup/protos/common/Money;
    .locals 4

    .line 152
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    int-to-long v2, p1

    mul-long v0, v0, v2

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static multiply(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lcom/squareup/protos/common/Money;
    .locals 4

    .line 170
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    .line 174
    new-instance v1, Ljava/math/MathContext;

    sget-object v2, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2}, Ljava/math/MathContext;-><init>(ILjava/math/RoundingMode;)V

    .line 175
    invoke-virtual {v0, p1, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;Ljava/math/MathContext;)Ljava/math/BigDecimal;

    move-result-object p1

    .line 178
    new-instance v0, Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, v3, p2}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {p1}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v0, p1, p0}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v0
.end method

.method public static multiplyByBasisPoints(Lcom/squareup/protos/common/Money;I)Lcom/squareup/protos/common/Money;
    .locals 4

    .line 199
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 200
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    int-to-long v2, p1

    mul-long v0, v0, v2

    const-wide/16 v2, 0x1388

    add-long/2addr v0, v2

    const-wide/16 v2, 0x2710

    div-long/2addr v0, v2

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 199
    invoke-static {v0, v1, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static multiplyByItemQuantity(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;)Lcom/squareup/protos/common/Money;
    .locals 3

    .line 188
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 189
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, Lcom/squareup/quantity/ItemQuantities;->DEFAULT_QUANTITY_ROUNDING_MODE:Ljava/math/RoundingMode;

    invoke-static {v0, v1, p1, v2}, Lcom/squareup/calc/util/BigDecimalHelper;->multiply(JLjava/math/BigDecimal;Ljava/math/RoundingMode;)J

    move-result-wide v0

    .line 190
    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    neg-long v0, v0

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static prorateAmountNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 5

    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 293
    iget-object v0, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 296
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 297
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    goto :goto_0

    .line 301
    :cond_0
    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {p0, p1, v0}, Lcom/squareup/money/MoneyMath;->divide(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object p0

    .line 302
    sget-object p1, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {p2, p0, p1}, Lcom/squareup/money/MoneyMath;->divide(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0

    :cond_1
    :goto_0
    return-object p2
.end method

.method public static subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 4

    const-string v0, "a"

    .line 88
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "b"

    .line 89
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 90
    invoke-static {p0, p1}, Lcom/squareup/money/MoneyMath;->assertSameCurrencies(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 1

    const-string v0, "a"

    .line 101
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    if-nez p1, :cond_0

    return-object p0

    .line 105
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static subtractWithZeroMinimum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 4

    .line 110
    invoke-static {p0, p1}, Lcom/squareup/money/MoneyMath;->assertSameCurrencies(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 4

    .line 131
    invoke-static {p0, p1}, Lcom/squareup/money/MoneyMath;->assertSameCurrencies(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static varargs sumAll([Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 7

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 140
    array-length v1, p0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "At least 1 amount should be present."

    invoke-static {v1, v2}, Landroidx/core/util/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    const-wide/16 v1, 0x0

    .line 142
    aget-object v3, p0, v0

    .line 143
    array-length v4, p0

    :goto_1
    if-ge v0, v4, :cond_1

    aget-object v5, p0, v0

    .line 144
    invoke-static {v3, v5}, Lcom/squareup/money/MoneyMath;->assertSameCurrencies(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    .line 145
    iget-object v5, v5, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    add-long/2addr v1, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 147
    :cond_1
    iget-object p0, v3, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1, v2, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 0

    if-nez p0, :cond_0

    return-object p1

    :cond_0
    if-nez p1, :cond_1

    return-object p0

    .line 126
    :cond_1
    invoke-static {p0, p1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method
