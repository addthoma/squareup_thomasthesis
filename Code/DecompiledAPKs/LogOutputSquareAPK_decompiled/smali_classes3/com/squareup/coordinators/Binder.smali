.class final Lcom/squareup/coordinators/Binder;
.super Ljava/lang/Object;
.source "Binder.java"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# instance fields
.field private final provider:Lcom/squareup/coordinators/CoordinatorProvider;


# direct methods
.method constructor <init>(Lcom/squareup/coordinators/CoordinatorProvider;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/coordinators/Binder;->provider:Lcom/squareup/coordinators/CoordinatorProvider;

    return-void
.end method


# virtual methods
.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .line 30
    iget-object p1, p0, Lcom/squareup/coordinators/Binder;->provider:Lcom/squareup/coordinators/CoordinatorProvider;

    invoke-static {p2, p1}, Lcom/squareup/coordinators/Coordinators;->bind(Landroid/view/View;Lcom/squareup/coordinators/CoordinatorProvider;)V

    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    return-void
.end method
