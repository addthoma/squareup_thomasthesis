.class final Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1$1;
.super Ljava/lang/Object;
.source "RealNotificationCenterBadge.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1;->apply(Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState;",
        "generalCount",
        "",
        "apply",
        "(Ljava/lang/Integer;)Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1$1;

    invoke-direct {v0}, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1$1;-><init>()V

    sput-object v0, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1$1;->INSTANCE:Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Integer;)Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState;
    .locals 1

    const-string v0, "generalCount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->compare(II)I

    move-result p1

    if-lez p1, :cond_0

    .line 42
    sget-object p1, Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$ShowGeneral;->INSTANCE:Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$ShowGeneral;

    check-cast p1, Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState;

    goto :goto_0

    .line 44
    :cond_0
    sget-object p1, Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$NoNotification;->INSTANCE:Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$NoNotification;

    check-cast p1, Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1$1;->apply(Ljava/lang/Integer;)Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState;

    move-result-object p1

    return-object p1
.end method
