.class public final Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;
.super Ljava/lang/Object;
.source "RealNotificationCenterBadge.kt"

# interfaces
.implements Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0019\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0007\u001a\u00020\u0008*\u00020\t8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\n\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;",
        "Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;",
        "notificationsRepository",
        "Lcom/squareup/notificationcenterdata/NotificationsRepository;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/notificationcenterdata/NotificationsRepository;Lio/reactivex/Scheduler;)V",
        "isCritical",
        "",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "(Lcom/squareup/notificationcenterdata/Notification;)Z",
        "badge",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final notificationsRepository:Lcom/squareup/notificationcenterdata/NotificationsRepository;


# direct methods
.method public constructor <init>(Lcom/squareup/notificationcenterdata/NotificationsRepository;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "notificationsRepository"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;->notificationsRepository:Lcom/squareup/notificationcenterdata/NotificationsRepository;

    iput-object p2, p0, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;->mainScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method public static final synthetic access$getNotificationsRepository$p(Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;)Lcom/squareup/notificationcenterdata/NotificationsRepository;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;->notificationsRepository:Lcom/squareup/notificationcenterdata/NotificationsRepository;

    return-object p0
.end method

.method public static final synthetic access$isCritical$p(Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;Lcom/squareup/notificationcenterdata/Notification;)Z
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;->isCritical(Lcom/squareup/notificationcenterdata/Notification;)Z

    move-result p0

    return p0
.end method

.method private final isCritical(Lcom/squareup/notificationcenterdata/Notification;)Z
    .locals 1

    .line 60
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getMessageType()Lcom/squareup/communications/Message$Type;

    move-result-object p1

    sget-object v0, Lcom/squareup/communications/Message$Type$AlertHighPriority;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertHighPriority;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method


# virtual methods
.method public badge()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;->notificationsRepository:Lcom/squareup/notificationcenterdata/NotificationsRepository;

    sget-object v1, Lcom/squareup/notificationcenterdata/Notification$Priority$Important;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$Priority$Important;

    check-cast v1, Lcom/squareup/notificationcenterdata/Notification$Priority;

    invoke-interface {v0, v1}, Lcom/squareup/notificationcenterdata/NotificationsRepository;->notifications(Lcom/squareup/notificationcenterdata/Notification$Priority;)Lio/reactivex/Observable;

    move-result-object v0

    .line 25
    new-instance v1, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1;

    invoke-direct {v1, p0}, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1;-><init>(Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "notificationsRepository.\u2026.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
