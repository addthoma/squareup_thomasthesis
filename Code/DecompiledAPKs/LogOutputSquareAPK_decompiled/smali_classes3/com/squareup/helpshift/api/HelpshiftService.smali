.class public interface abstract Lcom/squareup/helpshift/api/HelpshiftService;
.super Ljava/lang/Object;
.source "HelpshiftService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0007\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J \u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\tH&J\u0008\u0010\u000c\u001a\u00020\u0007H&J\u0018\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\tH&\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/helpshift/api/HelpshiftService;",
        "",
        "getNotificationCount",
        "",
        "successHandler",
        "Landroid/os/Handler;",
        "installHelpshift",
        "",
        "apiKey",
        "",
        "domainName",
        "appId",
        "isConversationActive",
        "loginHelpshift",
        "hmacToken",
        "deviceLookupToken",
        "helpshift_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getNotificationCount(Landroid/os/Handler;)V
.end method

.method public abstract installHelpshift(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract isConversationActive()Z
.end method

.method public abstract loginHelpshift(Ljava/lang/String;Ljava/lang/String;)V
.end method
