.class public final Lcom/squareup/jedi/CustomJediComponentItemsFactory;
.super Ljava/lang/Object;
.source "CustomJediComponentItemsFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u000e\u0010\n\u001a\u00020\u000bX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/jedi/CustomJediComponentItemsFactory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "noResults",
        "",
        "Lcom/squareup/jedi/ui/JediComponentItem;",
        "getNoResults",
        "()Ljava/util/List;",
        "noResultsName",
        "",
        "noResultsSubtext",
        "noResultsTitle",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final noResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">;"
        }
    .end annotation
.end field

.field private final noResultsName:Ljava/lang/String;

.field private final noResultsSubtext:Ljava/lang/String;

.field private final noResultsTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    sget v0, Lcom/squareup/jedi/impl/R$string;->no_results:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/jedi/CustomJediComponentItemsFactory;->noResultsTitle:Ljava/lang/String;

    .line 16
    sget v0, Lcom/squareup/jedi/impl/R$string;->no_results_subtext:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/jedi/CustomJediComponentItemsFactory;->noResultsSubtext:Ljava/lang/String;

    const-string p1, ""

    .line 17
    iput-object p1, p0, Lcom/squareup/jedi/CustomJediComponentItemsFactory;->noResultsName:Ljava/lang/String;

    .line 20
    new-instance p1, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;

    .line 21
    new-instance v0, Lcom/squareup/protos/jedi/service/Component;

    .line 22
    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->PARAGRAPH:Lcom/squareup/protos/jedi/service/ComponentKind;

    .line 23
    iget-object v2, p0, Lcom/squareup/jedi/CustomJediComponentItemsFactory;->noResultsName:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Lkotlin/Pair;

    .line 25
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/squareup/jedi/CustomJediComponentItemsFactory;->noResultsTitle:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "<br>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/squareup/jedi/CustomJediComponentItemsFactory;->noResultsSubtext:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "text"

    invoke-static {v5, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-string v4, "emphasis"

    const-string v5, "high"

    .line 26
    invoke-static {v4, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v3, v5

    const-string v4, "is_html"

    const-string/jumbo v5, "true"

    .line 27
    invoke-static {v4, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v4

    const/4 v5, 0x2

    aput-object v4, v3, v5

    .line 24
    invoke-static {v3}, Lkotlin/collections/MapsKt;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v3

    .line 29
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v4

    .line 21
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/jedi/service/Component;-><init>(Lcom/squareup/protos/jedi/service/ComponentKind;Ljava/lang/String;Ljava/util/Map;Ljava/util/List;)V

    .line 20
    invoke-direct {p1, v0}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    .line 19
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/jedi/CustomJediComponentItemsFactory;->noResults:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final getNoResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/jedi/CustomJediComponentItemsFactory;->noResults:Ljava/util/List;

    return-object v0
.end method
