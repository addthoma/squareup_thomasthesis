.class public final Lcom/squareup/jedi/JediWorkflowViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "JediWorkflowViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/jedi/JediWorkflowViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "messagingController",
        "Lcom/squareup/ui/help/MessagingController;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lcom/squareup/ui/help/MessagingController;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/Device;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final messagingController:Lcom/squareup/ui/help/MessagingController;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/MessagingController;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/Device;)V
    .locals 20
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    const-string v4, "messagingController"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "glassSpinner"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "device"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 19
    sget-object v5, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 20
    sget-object v6, Lcom/squareup/jedi/JediWorkflowScreen;->Companion:Lcom/squareup/jedi/JediWorkflowScreen$Companion;

    invoke-virtual {v6}, Lcom/squareup/jedi/JediWorkflowScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v6

    .line 21
    new-instance v19, Lcom/squareup/workflow/ScreenHint;

    sget-object v13, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x1df

    const/16 v18, 0x0

    move-object/from16 v7, v19

    invoke-direct/range {v7 .. v18}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 22
    sget v7, Lcom/squareup/jedi/impl/R$layout;->jedi_help_section:I

    .line 23
    new-instance v8, Lcom/squareup/jedi/JediWorkflowViewFactory$1;

    invoke-direct {v8, v1, v2, v3}, Lcom/squareup/jedi/JediWorkflowViewFactory$1;-><init>(Lcom/squareup/ui/help/MessagingController;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/Device;)V

    move-object v10, v8

    check-cast v10, Lkotlin/jvm/functions/Function1;

    const/16 v11, 0x8

    move-object/from16 v8, v19

    .line 19
    invoke-static/range {v5 .. v12}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 18
    invoke-direct {v0, v4}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    iput-object v1, v0, Lcom/squareup/jedi/JediWorkflowViewFactory;->messagingController:Lcom/squareup/ui/help/MessagingController;

    iput-object v2, v0, Lcom/squareup/jedi/JediWorkflowViewFactory;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iput-object v3, v0, Lcom/squareup/jedi/JediWorkflowViewFactory;->device:Lcom/squareup/util/Device;

    return-void
.end method
