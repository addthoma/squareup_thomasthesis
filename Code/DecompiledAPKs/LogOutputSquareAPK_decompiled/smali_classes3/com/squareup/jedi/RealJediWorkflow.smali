.class public final Lcom/squareup/jedi/RealJediWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealJediWorkflow.kt"

# interfaces
.implements Lcom/squareup/jedi/JediWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/jedi/JediWorkflowProps;",
        "Lcom/squareup/jedi/JediWorkflowState;",
        "Lcom/squareup/jedi/JediWorkflowOutput;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/jedi/JediWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealJediWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealJediWorkflow.kt\ncom/squareup/jedi/RealJediWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,179:1\n32#2,12:180\n85#3:192\n85#3:195\n85#3:198\n240#4:193\n240#4:196\n240#4:199\n276#5:194\n276#5:197\n276#5:200\n149#6,5:201\n*E\n*S KotlinDebug\n*F\n+ 1 RealJediWorkflow.kt\ncom/squareup/jedi/RealJediWorkflow\n*L\n46#1,12:180\n63#1:192\n84#1:195\n107#1:198\n63#1:193\n84#1:196\n107#1:199\n63#1:194\n84#1:197\n107#1:200\n176#1,5:201\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012@\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u0002B\u0017\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u001a\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00032\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J4\u0010\u0014\u001a\u0018\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00160\u0008j\u0008\u0012\u0004\u0012\u00020\u0015`\u00172\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u00192\u0006\u0010\u001b\u001a\u00020\u0004H\u0002JR\u0010\u001c\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n2\u0006\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u00042\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001eH\u0016J\u0010\u0010\u001f\u001a\u00020\u00132\u0006\u0010\u001b\u001a\u00020\u0004H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/jedi/RealJediWorkflow;",
        "Lcom/squareup/jedi/JediWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/jedi/JediWorkflowProps;",
        "Lcom/squareup/jedi/JediWorkflowState;",
        "Lcom/squareup/jedi/JediWorkflowOutput;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "jediWorkflowService",
        "Lcom/squareup/jedi/JediWorkflowService;",
        "resources",
        "Landroid/content/res/Resources;",
        "(Lcom/squareup/jedi/JediWorkflowService;Landroid/content/res/Resources;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "legacyWorkflowScreen",
        "Lcom/squareup/jedi/JediWorkflowScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/jedi/JediWorkflowAction;",
        "state",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final jediWorkflowService:Lcom/squareup/jedi/JediWorkflowService;

.field private final resources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/squareup/jedi/JediWorkflowService;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "jediWorkflowService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/jedi/RealJediWorkflow;->jediWorkflowService:Lcom/squareup/jedi/JediWorkflowService;

    iput-object p2, p0, Lcom/squareup/jedi/RealJediWorkflow;->resources:Landroid/content/res/Resources;

    return-void
.end method

.method private final legacyWorkflowScreen(Lcom/squareup/workflow/Sink;Lcom/squareup/jedi/JediWorkflowState;)Lcom/squareup/workflow/legacy/Screen;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/jedi/JediWorkflowAction;",
            ">;",
            "Lcom/squareup/jedi/JediWorkflowState;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    .line 136
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/jedi/JediWorkflowState;->getScreenDataStack()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-le v2, v4, :cond_0

    const/4 v7, 0x1

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    .line 139
    :goto_0
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/jedi/JediWorkflowState;->getCurrentScreenData()Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->getHeader()Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;->text()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-ne v2, v4, :cond_4

    .line 140
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/jedi/JediWorkflowState;->getCurrentScreenData()Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-virtual {v2}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->getHeader()Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    move-result-object v2

    if-nez v2, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-virtual {v2}, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;->text()Ljava/lang/String;

    move-result-object v2

    move-object v8, v2

    move-object/from16 v2, p0

    goto :goto_2

    :cond_4
    move-object/from16 v2, p0

    .line 142
    iget-object v5, v2, Lcom/squareup/jedi/RealJediWorkflow;->resources:Landroid/content/res/Resources;

    sget v6, Lcom/squareup/jedi/impl/R$string;->help:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v8, v5

    .line 146
    :goto_2
    instance-of v5, v1, Lcom/squareup/jedi/JediWorkflowState$InShowingState;

    if-nez v5, :cond_5

    instance-of v6, v1, Lcom/squareup/jedi/JediWorkflowState$InErrorState;

    if-nez v6, :cond_5

    const/4 v6, 0x1

    goto :goto_3

    :cond_5
    const/4 v6, 0x0

    .line 147
    :goto_3
    new-instance v16, Lcom/squareup/jedi/JediWorkflowScreen;

    if-nez v6, :cond_6

    if-nez v7, :cond_6

    const/4 v12, 0x1

    goto :goto_4

    :cond_6
    const/4 v12, 0x0

    .line 152
    :goto_4
    new-instance v3, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$1;

    invoke-direct {v3, v0, v7}, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$1;-><init>(Lcom/squareup/workflow/Sink;Z)V

    move-object v9, v3

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 155
    new-instance v3, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$2;

    invoke-direct {v3, v0}, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$2;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v10, v3

    check-cast v10, Lkotlin/jvm/functions/Function1;

    .line 158
    new-instance v3, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$3;

    invoke-direct {v3, v0}, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$3;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v11, v3

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 161
    new-instance v3, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$4;

    invoke-direct {v3, v6, v0}, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$4;-><init>(ZLcom/squareup/workflow/Sink;)V

    move-object v14, v3

    check-cast v14, Lkotlin/jvm/functions/Function1;

    .line 166
    new-instance v3, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$5;

    invoke-direct {v3, v0, v7}, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$5;-><init>(Lcom/squareup/workflow/Sink;Z)V

    move-object v15, v3

    check-cast v15, Lkotlin/jvm/functions/Function1;

    if-eqz v5, :cond_7

    .line 170
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/jedi/JediWorkflowState;->getScreenDataStack()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/jedi/JediHelpScreenData;

    :goto_5
    move-object v13, v0

    goto :goto_7

    .line 171
    :cond_7
    instance-of v0, v1, Lcom/squareup/jedi/JediWorkflowState$InErrorState;

    if-eqz v0, :cond_8

    move-object v0, v1

    check-cast v0, Lcom/squareup/jedi/JediWorkflowState$InErrorState;

    invoke-virtual {v0}, Lcom/squareup/jedi/JediWorkflowState$InErrorState;->getErrorScreenData()Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;

    move-result-object v0

    check-cast v0, Lcom/squareup/jedi/JediHelpScreenData;

    goto :goto_5

    .line 172
    :cond_8
    instance-of v0, v1, Lcom/squareup/jedi/JediWorkflowState$UninitializedState;

    if-eqz v0, :cond_9

    goto :goto_6

    :cond_9
    instance-of v0, v1, Lcom/squareup/jedi/JediWorkflowState$InLoadingState;

    if-eqz v0, :cond_a

    goto :goto_6

    :cond_a
    instance-of v0, v1, Lcom/squareup/jedi/JediWorkflowState$InSearchingState;

    if-eqz v0, :cond_b

    .line 173
    :goto_6
    sget-object v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpLoadingScreenData;->INSTANCE:Lcom/squareup/jedi/JediHelpScreenData$JediHelpLoadingScreenData;

    check-cast v0, Lcom/squareup/jedi/JediHelpScreenData;

    goto :goto_5

    :goto_7
    move-object/from16 v5, v16

    .line 147
    invoke-direct/range {v5 .. v15}, Lcom/squareup/jedi/JediWorkflowScreen;-><init>(ZZLjava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZLcom/squareup/jedi/JediHelpScreenData;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    move-object/from16 v0, v16

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 202
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 203
    const-class v3, Lcom/squareup/jedi/JediWorkflowScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, ""

    invoke-static {v3, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 204
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 202
    invoke-direct {v1, v3, v0, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v1

    .line 173
    :cond_b
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/jedi/JediWorkflowProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/jedi/JediWorkflowState;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 180
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v2

    :goto_1
    if-eqz p2, :cond_3

    .line 185
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 187
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 188
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 189
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 191
    :cond_3
    check-cast v2, Lcom/squareup/jedi/JediWorkflowState;

    if-eqz v2, :cond_4

    goto :goto_2

    .line 46
    :cond_4
    new-instance p2, Lcom/squareup/jedi/JediWorkflowState$UninitializedState;

    invoke-virtual {p1}, Lcom/squareup/jedi/JediWorkflowProps;->getPanelToken()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/jedi/JediWorkflowState$UninitializedState;-><init>(Ljava/lang/String;)V

    move-object v2, p2

    check-cast v2, Lcom/squareup/jedi/JediWorkflowState;

    :goto_2
    return-object v2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/jedi/JediWorkflowProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/jedi/RealJediWorkflow;->initialState(Lcom/squareup/jedi/JediWorkflowProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/jedi/JediWorkflowState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/jedi/JediWorkflowProps;

    check-cast p2, Lcom/squareup/jedi/JediWorkflowState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/jedi/RealJediWorkflow;->render(Lcom/squareup/jedi/JediWorkflowProps;Lcom/squareup/jedi/JediWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/jedi/JediWorkflowProps;Lcom/squareup/jedi/JediWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/jedi/JediWorkflowProps;",
            "Lcom/squareup/jedi/JediWorkflowState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/jedi/JediWorkflowState;",
            "-",
            "Lcom/squareup/jedi/JediWorkflowOutput;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    .line 60
    instance-of v0, p2, Lcom/squareup/jedi/JediWorkflowState$UninitializedState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/squareup/jedi/RealJediWorkflow;->jediWorkflowService:Lcom/squareup/jedi/JediWorkflowService;

    move-object v2, p2

    check-cast v2, Lcom/squareup/jedi/JediWorkflowState$UninitializedState;

    invoke-virtual {v2}, Lcom/squareup/jedi/JediWorkflowState$UninitializedState;->getPanelToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/jedi/JediWorkflowService;->startSessionAndGetPanel(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 192
    sget-object v2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v2, Lcom/squareup/jedi/RealJediWorkflow$render$$inlined$asWorker$1;

    invoke-direct {v2, v0, v1}, Lcom/squareup/jedi/RealJediWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 193
    invoke-static {v2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 194
    const-class v1, Lcom/squareup/jedi/JediHelpScreenData;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 64
    new-instance v0, Lcom/squareup/jedi/RealJediWorkflow$render$1;

    invoke-direct {v0, p2}, Lcom/squareup/jedi/RealJediWorkflow$render$1;-><init>(Lcom/squareup/jedi/JediWorkflowState;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p3

    .line 61
    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 81
    :cond_0
    instance-of v0, p2, Lcom/squareup/jedi/JediWorkflowState$InLoadingState;

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Lcom/squareup/jedi/RealJediWorkflow;->jediWorkflowService:Lcom/squareup/jedi/JediWorkflowService;

    move-object v2, p2

    check-cast v2, Lcom/squareup/jedi/JediWorkflowState$InLoadingState;

    invoke-virtual {v2}, Lcom/squareup/jedi/JediWorkflowState$InLoadingState;->getJediInput()Lcom/squareup/jedi/JediInput;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/jedi/JediWorkflowService;->getNextPanel(Lcom/squareup/jedi/JediInput;)Lio/reactivex/Single;

    move-result-object v0

    .line 195
    sget-object v2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v2, Lcom/squareup/jedi/RealJediWorkflow$render$$inlined$asWorker$2;

    invoke-direct {v2, v0, v1}, Lcom/squareup/jedi/RealJediWorkflow$render$$inlined$asWorker$2;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 196
    invoke-static {v2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 197
    const-class v1, Lcom/squareup/jedi/JediHelpScreenData;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 85
    new-instance v0, Lcom/squareup/jedi/RealJediWorkflow$render$2;

    invoke-direct {v0, p2}, Lcom/squareup/jedi/RealJediWorkflow$render$2;-><init>(Lcom/squareup/jedi/JediWorkflowState;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p3

    .line 82
    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    goto :goto_0

    .line 102
    :cond_1
    instance-of v0, p2, Lcom/squareup/jedi/JediWorkflowState$InSearchingState;

    if-eqz v0, :cond_3

    .line 105
    iget-object v0, p0, Lcom/squareup/jedi/RealJediWorkflow;->jediWorkflowService:Lcom/squareup/jedi/JediWorkflowService;

    .line 106
    invoke-virtual {p2}, Lcom/squareup/jedi/JediWorkflowState;->getCurrentScreenData()Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 107
    :cond_2
    move-object v3, p2

    check-cast v3, Lcom/squareup/jedi/JediWorkflowState$InSearchingState;

    invoke-virtual {v3}, Lcom/squareup/jedi/JediWorkflowState$InSearchingState;->getSearchTerm()Ljava/lang/String;

    move-result-object v3

    .line 105
    invoke-virtual {v0, v2, v3}, Lcom/squareup/jedi/JediWorkflowService;->searching(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 198
    sget-object v2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v2, Lcom/squareup/jedi/RealJediWorkflow$render$$inlined$asWorker$3;

    invoke-direct {v2, v0, v1}, Lcom/squareup/jedi/RealJediWorkflow$render$$inlined$asWorker$3;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 199
    invoke-static {v2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 200
    const-class v1, Lcom/squareup/jedi/JediHelpScreenData;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 108
    new-instance v0, Lcom/squareup/jedi/RealJediWorkflow$render$3;

    invoke-direct {v0, p2}, Lcom/squareup/jedi/RealJediWorkflow$render$3;-><init>(Lcom/squareup/jedi/JediWorkflowState;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p3

    .line 103
    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 125
    :cond_3
    :goto_0
    sget-object p3, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    invoke-direct {p0, p1, p2}, Lcom/squareup/jedi/RealJediWorkflow;->legacyWorkflowScreen(Lcom/squareup/workflow/Sink;Lcom/squareup/jedi/JediWorkflowState;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/jedi/JediWorkflowState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/jedi/JediWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/jedi/RealJediWorkflow;->snapshotState(Lcom/squareup/jedi/JediWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
