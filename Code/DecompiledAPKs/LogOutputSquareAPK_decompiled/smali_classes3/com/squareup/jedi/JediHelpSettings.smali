.class public interface abstract Lcom/squareup/jedi/JediHelpSettings;
.super Ljava/lang/Object;
.source "JediHelpSettings.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/jedi/JediHelpSettings$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u000b\u001a\u00020\u0006H\u0016R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0002\u0010\u0004R\u0012\u0010\u0005\u001a\u00020\u0006X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u0012\u0010\t\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u0004\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/jedi/JediHelpSettings;",
        "",
        "isJediEnabled",
        "",
        "()Z",
        "jediClientKey",
        "",
        "getJediClientKey",
        "()Ljava/lang/String;",
        "showInlineLinks",
        "getShowInlineLinks",
        "getCallUsPanelToken",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getCallUsPanelToken()Ljava/lang/String;
.end method

.method public abstract getJediClientKey()Ljava/lang/String;
.end method

.method public abstract getShowInlineLinks()Z
.end method

.method public abstract isJediEnabled()Z
.end method
