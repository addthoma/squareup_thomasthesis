.class public Lcom/squareup/jedi/ui/components/JediIconComponentItem;
.super Lcom/squareup/jedi/ui/JediComponentItem;
.source "JediIconComponentItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/jedi/service/Component;)V
    .locals 1

    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/jedi/ui/JediComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    .line 34
    iget-object p1, p1, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    sget-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->ICON:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/jedi/service/ComponentKind;->equals(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    return-void
.end method

.method static synthetic lambda$type$0(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;
    .locals 0

    .line 38
    invoke-static {p0}, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;->access$000(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public type()Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;
    .locals 3

    .line 38
    sget-object v0, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;->SUCCESSFUL_SUBMISSION:Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    sget-object v1, Lcom/squareup/jedi/ui/components/-$$Lambda$JediIconComponentItem$KYpmO_YBv8KdLCupfTJDbJOwSbM;->INSTANCE:Lcom/squareup/jedi/ui/components/-$$Lambda$JediIconComponentItem$KYpmO_YBv8KdLCupfTJDbJOwSbM;

    const-string/jumbo v2, "type"

    invoke-virtual {p0, v2, v0, v1}, Lcom/squareup/jedi/ui/components/JediIconComponentItem;->getParameterOrDefault(Ljava/lang/String;Ljava/lang/Object;Lrx/functions/Func1;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    return-object v0
.end method
