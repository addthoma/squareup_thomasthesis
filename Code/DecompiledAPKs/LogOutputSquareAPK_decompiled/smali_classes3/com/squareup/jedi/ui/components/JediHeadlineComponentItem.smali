.class public Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;
.super Lcom/squareup/jedi/ui/JediComponentItem;
.source "JediHeadlineComponentItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem$HeadlineType;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/jedi/service/Component;)V
    .locals 1

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/jedi/ui/JediComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    .line 36
    iget-object p1, p1, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    sget-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->HEADLINE:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/jedi/service/ComponentKind;->equals(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    return-void
.end method

.method static synthetic lambda$type$0(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem$HeadlineType;
    .locals 0

    .line 44
    invoke-static {p0}, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem$HeadlineType;->access$000(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem$HeadlineType;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public text()Ljava/lang/String;
    .locals 1

    const-string v0, "text"

    .line 40
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem$HeadlineType;
    .locals 3

    .line 44
    sget-object v0, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem$HeadlineType;->HEADING:Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem$HeadlineType;

    sget-object v1, Lcom/squareup/jedi/ui/components/-$$Lambda$JediHeadlineComponentItem$F2BHZUk69bAsIM2qC9IiH4p05_w;->INSTANCE:Lcom/squareup/jedi/ui/components/-$$Lambda$JediHeadlineComponentItem$F2BHZUk69bAsIM2qC9IiH4p05_w;

    const-string/jumbo v2, "type"

    invoke-virtual {p0, v2, v0, v1}, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;->getParameterOrDefault(Ljava/lang/String;Ljava/lang/Object;Lrx/functions/Func1;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem$HeadlineType;

    return-object v0
.end method
