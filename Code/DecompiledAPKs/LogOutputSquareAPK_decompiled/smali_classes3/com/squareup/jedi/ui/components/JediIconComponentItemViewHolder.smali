.class public Lcom/squareup/jedi/ui/components/JediIconComponentItemViewHolder;
.super Lcom/squareup/jedi/ui/JediComponentItemViewHolder;
.source "JediIconComponentItemViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/jedi/ui/JediComponentItemViewHolder<",
        "Lcom/squareup/jedi/ui/components/JediIconComponentItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final iconGlyph:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;",
            "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
            ">;"
        }
    .end annotation
.end field

.field private static final iconGlyphColor:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 22
    sget-object v0, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;->SUCCESSFUL_SUBMISSION:Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 23
    invoke-static {v0, v1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/squareup/jedi/ui/components/JediIconComponentItemViewHolder;->iconGlyph:Ljava/util/Map;

    .line 24
    sget-object v0, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;->SUCCESSFUL_SUBMISSION:Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    sget v1, Lcom/squareup/marin/R$color;->marin_green:I

    .line 25
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/squareup/jedi/ui/components/JediIconComponentItemViewHolder;->iconGlyphColor:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    .line 28
    sget v0, Lcom/squareup/jedi/impl/R$layout;->jedi_icon_component_view:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/jedi/ui/JediComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic onBind(Lcom/squareup/jedi/ui/JediComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/jedi/ui/components/JediIconComponentItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/jedi/ui/components/JediIconComponentItemViewHolder;->onBind(Lcom/squareup/jedi/ui/components/JediIconComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V

    return-void
.end method

.method public onBind(Lcom/squareup/jedi/ui/components/JediIconComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 1

    .line 33
    iget-object p2, p0, Lcom/squareup/jedi/ui/components/JediIconComponentItemViewHolder;->itemView:Landroid/view/View;

    sget p3, Lcom/squareup/jedi/impl/R$id;->icon_glyph:I

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/glyph/SquareGlyphView;

    .line 34
    iget-object p3, p0, Lcom/squareup/jedi/ui/components/JediIconComponentItemViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    .line 36
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediIconComponentItem;->type()Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    move-result-object p1

    .line 37
    sget-object v0, Lcom/squareup/jedi/ui/components/JediIconComponentItemViewHolder;->iconGlyph:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p2, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 38
    sget-object v0, Lcom/squareup/jedi/ui/components/JediIconComponentItemViewHolder;->iconGlyphColor:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p3, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {p2, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    return-void
.end method
