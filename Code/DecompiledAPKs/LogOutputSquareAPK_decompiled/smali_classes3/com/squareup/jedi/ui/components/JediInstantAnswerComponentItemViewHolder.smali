.class public Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItemViewHolder;
.super Lcom/squareup/jedi/ui/JediComponentItemViewHolder;
.source "JediInstantAnswerComponentItemViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/jedi/ui/JediComponentItemViewHolder<",
        "Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    .line 17
    sget v0, Lcom/squareup/jedi/impl/R$layout;->jedi_instant_answer_component_view:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/jedi/ui/JediComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;I)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onBind$0$JediInstantAnswerComponentItemViewHolder(Lcom/squareup/jedi/JediComponentInputHandler;Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;Landroid/view/View;)V
    .locals 0

    .line 32
    iget-object p3, p0, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItemViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-virtual {p2}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->url()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p3, p2}, Lcom/squareup/jedi/JediComponentInputHandler;->onLink(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic onBind(Lcom/squareup/jedi/ui/JediComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItemViewHolder;->onBind(Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V

    return-void
.end method

.method public onBind(Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 4

    .line 22
    iget-object v0, p0, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItemViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/jedi/impl/R$id;->title:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 23
    iget-object v1, p0, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItemViewHolder;->itemView:Landroid/view/View;

    sget v2, Lcom/squareup/jedi/impl/R$id;->text:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 24
    iget-object v2, p0, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItemViewHolder;->itemView:Landroid/view/View;

    sget v3, Lcom/squareup/jedi/impl/R$id;->label:I

    invoke-static {v2, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 26
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->title()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 27
    iget-boolean p2, p2, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->showInlineLinks:Z

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->htmlText()Ljava/lang/CharSequence;

    move-result-object p2

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->htmlTextWithoutLinks()Ljava/lang/CharSequence;

    move-result-object p2

    :goto_0
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->hasUrl()Z

    move-result p2

    invoke-static {v2, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 30
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->hasUrl()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->label()Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    :cond_1
    const-string p2, ""

    :goto_1
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->hasUrl()Z

    move-result p2

    if-eqz p2, :cond_2

    new-instance p2, Lcom/squareup/jedi/ui/components/-$$Lambda$JediInstantAnswerComponentItemViewHolder$op3TLQW8mpbjKIXXf52thK8cy0M;

    invoke-direct {p2, p0, p3, p1}, Lcom/squareup/jedi/ui/components/-$$Lambda$JediInstantAnswerComponentItemViewHolder$op3TLQW8mpbjKIXXf52thK8cy0M;-><init>(Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItemViewHolder;Lcom/squareup/jedi/JediComponentInputHandler;Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;)V

    .line 32
    invoke-static {p2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    .line 31
    :goto_2
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
