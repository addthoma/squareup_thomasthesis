.class public final Lcom/squareup/leakcanary/ScopedObjectWatcher;
.super Ljava/lang/Object;
.source "ScopedObjectWatcher.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/leakcanary/ScopedObjectWatcher$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \n2\u00020\u0001:\u0001\nB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0001R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/leakcanary/ScopedObjectWatcher;",
        "",
        "objectWatcher",
        "Lleakcanary/ObjectWatcher;",
        "(Lleakcanary/ObjectWatcher;)V",
        "watch",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "ref",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/leakcanary/ScopedObjectWatcher$Companion;

.field private static final SERVICE_NAME:Ljava/lang/String;


# instance fields
.field private final objectWatcher:Lleakcanary/ObjectWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/leakcanary/ScopedObjectWatcher$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/leakcanary/ScopedObjectWatcher$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/leakcanary/ScopedObjectWatcher;->Companion:Lcom/squareup/leakcanary/ScopedObjectWatcher$Companion;

    .line 25
    const-class v0, Lcom/squareup/leakcanary/ScopedObjectWatcher;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ScopedObjectWatcher::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/leakcanary/ScopedObjectWatcher;->SERVICE_NAME:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lleakcanary/ObjectWatcher;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/leakcanary/ScopedObjectWatcher;->objectWatcher:Lleakcanary/ObjectWatcher;

    return-void
.end method

.method public synthetic constructor <init>(Lleakcanary/ObjectWatcher;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 9
    invoke-direct {p0, p1}, Lcom/squareup/leakcanary/ScopedObjectWatcher;-><init>(Lleakcanary/ObjectWatcher;)V

    return-void
.end method

.method public static final synthetic access$getObjectWatcher$p(Lcom/squareup/leakcanary/ScopedObjectWatcher;)Lleakcanary/ObjectWatcher;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/squareup/leakcanary/ScopedObjectWatcher;->objectWatcher:Lleakcanary/ObjectWatcher;

    return-object p0
.end method

.method public static final synthetic access$getSERVICE_NAME$cp()Ljava/lang/String;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/leakcanary/ScopedObjectWatcher;->SERVICE_NAME:Ljava/lang/String;

    return-object v0
.end method

.method public static final addService(Lmortar/MortarScope$Builder;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/leakcanary/ScopedObjectWatcher;->Companion:Lcom/squareup/leakcanary/ScopedObjectWatcher$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/leakcanary/ScopedObjectWatcher$Companion;->addService(Lmortar/MortarScope$Builder;)V

    return-void
.end method

.method public static final watchForLeaks(Lmortar/MortarScope;Ljava/lang/Object;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/leakcanary/ScopedObjectWatcher;->Companion:Lcom/squareup/leakcanary/ScopedObjectWatcher$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/leakcanary/ScopedObjectWatcher$Companion;->watchForLeaks(Lmortar/MortarScope;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final watch(Lmortar/MortarScope;Ljava/lang/Object;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ref"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    new-instance v0, Lcom/squareup/leakcanary/ScopedObjectWatcher$watch$1;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/leakcanary/ScopedObjectWatcher$watch$1;-><init>(Lcom/squareup/leakcanary/ScopedObjectWatcher;Ljava/lang/Object;Lmortar/MortarScope;)V

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
