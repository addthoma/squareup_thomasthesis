.class public final Lcom/squareup/feedback/StarsView;
.super Landroid/widget/LinearLayout;
.source "StarsView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/feedback/StarsView$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStarsView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StarsView.kt\ncom/squareup/feedback/StarsView\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,94:1\n1360#2:95\n1429#2,3:96\n1577#2,4:99\n*E\n*S KotlinDebug\n*F\n+ 1 StarsView.kt\ncom/squareup/feedback/StarsView\n*L\n75#1:95\n75#1,3:96\n75#1,4:99\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u00182\u00020\u0001:\u0001\u0018B\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u000f\u001a\u00020\u0010H\u0002J\u0008\u0010\u0011\u001a\u00020\u0008H\u0002J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0014J\u0008\u0010\u0016\u001a\u00020\u0015H\u0014J\u0008\u0010\u0017\u001a\u00020\u0013H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00080\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/feedback/StarsView;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "starsCount",
        "",
        "starsSelected",
        "Lio/reactivex/Observable;",
        "getStarsSelected",
        "()Lio/reactivex/Observable;",
        "starsSelectedRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "createStar",
        "Landroid/view/View;",
        "getNumStarsSelected",
        "onRestoreInstanceState",
        "",
        "bundle",
        "Landroid/os/Parcelable;",
        "onSaveInstanceState",
        "setUpOnClickListeners",
        "Companion",
        "feedback_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/feedback/StarsView$Companion;

.field private static final KEY_STARS_SELECTED_NUM:Ljava/lang/String; = "starsSelectedNumber"


# instance fields
.field private starsCount:I

.field private final starsSelected:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final starsSelectedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/feedback/StarsView$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/feedback/StarsView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/feedback/StarsView;->Companion:Lcom/squareup/feedback/StarsView$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x5

    .line 36
    iput v0, p0, Lcom/squareup/feedback/StarsView;->starsCount:I

    const/4 v0, 0x0

    .line 38
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    const-string v2, "BehaviorRelay.createDefault(0)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/squareup/feedback/StarsView;->starsSelectedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 39
    iget-object v1, p0, Lcom/squareup/feedback/StarsView;->starsSelectedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v1, Lio/reactivex/Observable;

    iput-object v1, p0, Lcom/squareup/feedback/StarsView;->starsSelected:Lio/reactivex/Observable;

    .line 42
    invoke-virtual {p0, v0}, Lcom/squareup/feedback/StarsView;->setOrientation(I)V

    .line 44
    sget-object v0, Lcom/squareup/feedback/R$styleable;->StarsView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 45
    sget p2, Lcom/squareup/feedback/R$styleable;->StarsView_starsCount:I

    iget v0, p0, Lcom/squareup/feedback/StarsView;->starsCount:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/feedback/StarsView;->starsCount:I

    .line 47
    iget p2, p0, Lcom/squareup/feedback/StarsView;->starsCount:I

    const/4 v0, 0x1

    if-gt v0, p2, :cond_0

    .line 48
    :goto_0
    invoke-direct {p0}, Lcom/squareup/feedback/StarsView;->createStar()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/squareup/feedback/StarsView;->addView(Landroid/view/View;)V

    if-eq v0, p2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50
    :cond_0
    invoke-direct {p0}, Lcom/squareup/feedback/StarsView;->setUpOnClickListeners()V

    .line 52
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 28
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/feedback/StarsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static final synthetic access$getNumStarsSelected(Lcom/squareup/feedback/StarsView;)I
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/feedback/StarsView;->getNumStarsSelected()I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getStarsSelectedRelay$p(Lcom/squareup/feedback/StarsView;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/feedback/StarsView;->starsSelectedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method private final createStar()Landroid/view/View;
    .locals 2

    .line 91
    invoke-virtual {p0}, Lcom/squareup/feedback/StarsView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v1, Lcom/squareup/feedback/R$layout;->star_view:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private final getNumStarsSelected()I
    .locals 4

    .line 75
    invoke-virtual {p0}, Lcom/squareup/feedback/StarsView;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 95
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 96
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v3, v0

    check-cast v3, Lkotlin/collections/IntIterator;

    invoke-virtual {v3}, Lkotlin/collections/IntIterator;->nextInt()I

    move-result v3

    .line 75
    invoke-virtual {p0, v3}, Lcom/squareup/feedback/StarsView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 98
    :cond_0
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 99
    instance-of v0, v2, Ljava/util/Collection;

    if-eqz v0, :cond_1

    move-object v0, v2

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_2

    .line 101
    :cond_1
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    const-string v3, "it"

    .line 75
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/view/View;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_2

    add-int/lit8 v1, v1, 0x1

    if-gez v1, :cond_2

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_1

    :cond_3
    :goto_2
    return v1
.end method

.method private final setUpOnClickListeners()V
    .locals 4

    .line 78
    invoke-virtual {p0}, Lcom/squareup/feedback/StarsView;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 80
    invoke-virtual {p0, v1}, Lcom/squareup/feedback/StarsView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/squareup/feedback/StarsView$setUpOnClickListeners$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/squareup/feedback/StarsView$setUpOnClickListeners$1;-><init>(Lcom/squareup/feedback/StarsView;II)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final getStarsSelected()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/squareup/feedback/StarsView;->starsSelected:Lio/reactivex/Observable;

    return-object v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 4

    const-string v0, "bundle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    instance-of v0, p1, Lcom/squareup/mortar/BundleSavedState;

    if-eqz v0, :cond_1

    .line 63
    check-cast p1, Lcom/squareup/mortar/BundleSavedState;

    iget-object v0, p1, Lcom/squareup/mortar/BundleSavedState;->bundle:Landroid/os/Bundle;

    const-string v1, "starsSelectedNumber"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 65
    invoke-virtual {p0, v1}, Lcom/squareup/feedback/StarsView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const-string v3, "getChildAt(i)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setSelected(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/squareup/feedback/StarsView;->starsSelectedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 68
    invoke-virtual {p1}, Lcom/squareup/mortar/BundleSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_1

    .line 70
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_1
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 56
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 57
    invoke-direct {p0}, Lcom/squareup/feedback/StarsView;->getNumStarsSelected()I

    move-result v1

    const-string v2, "starsSelectedNumber"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 58
    new-instance v1, Lcom/squareup/mortar/BundleSavedState;

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/squareup/mortar/BundleSavedState;-><init>(Landroid/os/Parcelable;Landroid/os/Bundle;)V

    check-cast v1, Landroid/os/Parcelable;

    return-object v1
.end method
