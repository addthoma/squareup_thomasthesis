.class public final Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;
.super Ljava/lang/Object;
.source "AppFeedbackCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/feedback/AppFeedbackCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final appNameFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final glassSpinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/AppFeedbackScreen$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/AppFeedbackScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;->appNameFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/AppFeedbackScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)",
            "Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;"
        }
    .end annotation

    .line 50
    new-instance v6, Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/feedback/AppFeedbackScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/AppNameFormatter;)Lcom/squareup/feedback/AppFeedbackCoordinator;
    .locals 7

    .line 55
    new-instance v6, Lcom/squareup/feedback/AppFeedbackCoordinator;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/feedback/AppFeedbackCoordinator;-><init>(Lcom/squareup/feedback/AppFeedbackScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/AppNameFormatter;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/feedback/AppFeedbackCoordinator;
    .locals 5

    .line 43
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/feedback/AppFeedbackScreen$Runner;

    iget-object v1, p0, Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Device;

    iget-object v3, p0, Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/register/widgets/GlassSpinner;

    iget-object v4, p0, Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;->appNameFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/util/AppNameFormatter;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;->newInstance(Lcom/squareup/feedback/AppFeedbackScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/AppNameFormatter;)Lcom/squareup/feedback/AppFeedbackCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/feedback/AppFeedbackCoordinator_Factory;->get()Lcom/squareup/feedback/AppFeedbackCoordinator;

    move-result-object v0

    return-object v0
.end method
