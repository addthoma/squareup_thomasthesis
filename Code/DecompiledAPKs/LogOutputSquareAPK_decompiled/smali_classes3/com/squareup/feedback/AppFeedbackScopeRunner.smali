.class public final Lcom/squareup/feedback/AppFeedbackScopeRunner;
.super Ljava/lang/Object;
.source "AppFeedbackScopeRunner.kt"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/feedback/AppFeedbackScreen$Runner;
.implements Lcom/squareup/feedback/AppFeedbackConfirmationScreen$Runner;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/feedback/AppFeedbackScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/feedback/AppFeedbackScopeRunner$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000 42\u00020\u00012\u00020\u00022\u00020\u0003:\u00014BG\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J\u000e\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020!0 H\u0016J\u000e\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020$0#H\u0016J\u0008\u0010%\u001a\u00020&H\u0016J\u0008\u0010\'\u001a\u00020&H\u0016J\u0010\u0010(\u001a\u00020&2\u0006\u0010)\u001a\u00020*H\u0016J\u0008\u0010+\u001a\u00020&H\u0016J\u0010\u0010,\u001a\u00020&2\u0006\u0010-\u001a\u00020.H\u0016J\u0018\u0010/\u001a\u00020&2\u0006\u00100\u001a\u0002012\u0006\u00102\u001a\u000203H\u0016R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u0017\u001a&\u0012\u000c\u0012\n \u001a*\u0004\u0018\u00010\u00190\u0019 \u001a*\u0012\u0012\u000c\u0012\n \u001a*\u0004\u0018\u00010\u00190\u0019\u0018\u00010\u00180\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u001b\u001a\u0010\u0012\u000c\u0012\n \u001a*\u0004\u0018\u00010\u00190\u00190\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/feedback/AppFeedbackScopeRunner;",
        "Lmortar/Scoped;",
        "Lcom/squareup/feedback/AppFeedbackScreen$Runner;",
        "Lcom/squareup/feedback/AppFeedbackConfirmationScreen$Runner;",
        "flow",
        "Lflow/Flow;",
        "standardReceiver",
        "Lcom/squareup/receiving/StandardReceiver;",
        "appFeedbackSubmitter",
        "Lcom/squareup/feedback/AppFeedbackSubmitter;",
        "failureMessageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "feedbackSubmittedCompleter",
        "Lcom/squareup/feedback/FeedbackSubmittedCompleter;",
        "confirmationScreenDataFactory",
        "Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;",
        "intentAvailabilityManager",
        "Lcom/squareup/util/IntentAvailabilityManager;",
        "playStoreIntentCreator",
        "Lcom/squareup/feedback/PlayStoreIntentCreator;",
        "(Lflow/Flow;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/feedback/AppFeedbackSubmitter;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/feedback/FeedbackSubmittedCompleter;Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;Lcom/squareup/util/IntentAvailabilityManager;Lcom/squareup/feedback/PlayStoreIntentCreator;)V",
        "appFeedbackScope",
        "Lcom/squareup/feedback/AppFeedbackScope;",
        "isBusy",
        "Lcom/jakewharton/rxrelay/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "isMaxRating",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "submitFeedbackSubscription",
        "Lrx/subscriptions/SerialSubscription;",
        "appFeedbackScreenData",
        "Lrx/Observable;",
        "Lcom/squareup/feedback/AppFeedbackScreen$ScreenData;",
        "confirmationScreenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;",
        "goBackFromAppFeedbackConfirmationScreen",
        "",
        "goBackFromFeedbackScreen",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "rateOnPlayStore",
        "viewContext",
        "Landroid/content/Context;",
        "submitFeedbackAndRating",
        "numStars",
        "",
        "feedback",
        "",
        "Companion",
        "feedback_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/feedback/AppFeedbackScopeRunner$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final MAX_RATING:I = 0x5


# instance fields
.field private appFeedbackScope:Lcom/squareup/feedback/AppFeedbackScope;

.field private final appFeedbackSubmitter:Lcom/squareup/feedback/AppFeedbackSubmitter;

.field private final confirmationScreenDataFactory:Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;

.field private final failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final feedbackSubmittedCompleter:Lcom/squareup/feedback/FeedbackSubmittedCompleter;

.field private final flow:Lflow/Flow;

.field private final intentAvailabilityManager:Lcom/squareup/util/IntentAvailabilityManager;

.field private final isBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final isMaxRating:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final playStoreIntentCreator:Lcom/squareup/feedback/PlayStoreIntentCreator;

.field private final standardReceiver:Lcom/squareup/receiving/StandardReceiver;

.field private final submitFeedbackSubscription:Lrx/subscriptions/SerialSubscription;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/feedback/AppFeedbackScopeRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/feedback/AppFeedbackScopeRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->Companion:Lcom/squareup/feedback/AppFeedbackScopeRunner$Companion;

    return-void
.end method

.method public constructor <init>(Lflow/Flow;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/feedback/AppFeedbackSubmitter;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/feedback/FeedbackSubmittedCompleter;Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;Lcom/squareup/util/IntentAvailabilityManager;Lcom/squareup/feedback/PlayStoreIntentCreator;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "standardReceiver"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appFeedbackSubmitter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failureMessageFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "feedbackSubmittedCompleter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "confirmationScreenDataFactory"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intentAvailabilityManager"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "playStoreIntentCreator"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    iput-object p3, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->appFeedbackSubmitter:Lcom/squareup/feedback/AppFeedbackSubmitter;

    iput-object p4, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object p5, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->feedbackSubmittedCompleter:Lcom/squareup/feedback/FeedbackSubmittedCompleter;

    iput-object p6, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;

    iput-object p7, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->intentAvailabilityManager:Lcom/squareup/util/IntentAvailabilityManager;

    iput-object p8, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->playStoreIntentCreator:Lcom/squareup/feedback/PlayStoreIntentCreator;

    .line 40
    new-instance p1, Lrx/subscriptions/SerialSubscription;

    invoke-direct {p1}, Lrx/subscriptions/SerialSubscription;-><init>()V

    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->submitFeedbackSubscription:Lrx/subscriptions/SerialSubscription;

    const/4 p1, 0x0

    .line 41
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->isBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 42
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "com.jakewharton.rxrelay2\u2026orRelay.create<Boolean>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->isMaxRating:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$Companion()Lcom/squareup/feedback/AppFeedbackScopeRunner$Companion;
    .locals 1

    sget-object v0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->Companion:Lcom/squareup/feedback/AppFeedbackScopeRunner$Companion;

    return-object v0
.end method

.method public static final synthetic access$getAppFeedbackScope$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;)Lcom/squareup/feedback/AppFeedbackScope;
    .locals 1

    .line 26
    iget-object p0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->appFeedbackScope:Lcom/squareup/feedback/AppFeedbackScope;

    if-nez p0, :cond_0

    const-string v0, "appFeedbackScope"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getConfirmationScreenDataFactory$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;)Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;

    return-object p0
.end method

.method public static final synthetic access$getFailureMessageFactory$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;)Lcom/squareup/receiving/FailureMessageFactory;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    return-object p0
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;)Lflow/Flow;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->flow:Lflow/Flow;

    return-object p0
.end method

.method public static final synthetic access$getIntentAvailabilityManager$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;)Lcom/squareup/util/IntentAvailabilityManager;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->intentAvailabilityManager:Lcom/squareup/util/IntentAvailabilityManager;

    return-object p0
.end method

.method public static final synthetic access$getPlayStoreIntentCreator$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;)Lcom/squareup/feedback/PlayStoreIntentCreator;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->playStoreIntentCreator:Lcom/squareup/feedback/PlayStoreIntentCreator;

    return-object p0
.end method

.method public static final synthetic access$isBusy$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->isBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$isMaxRating$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->isMaxRating:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$setAppFeedbackScope$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;Lcom/squareup/feedback/AppFeedbackScope;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->appFeedbackScope:Lcom/squareup/feedback/AppFeedbackScope;

    return-void
.end method


# virtual methods
.method public appFeedbackScreenData()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/feedback/AppFeedbackScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->isBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/feedback/AppFeedbackScopeRunner$appFeedbackScreenData$1;->INSTANCE:Lcom/squareup/feedback/AppFeedbackScopeRunner$appFeedbackScreenData$1;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const-string v1, "isBusy.map { AppFeedbackScreen.ScreenData(it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public confirmationScreenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->isMaxRating:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/feedback/AppFeedbackScopeRunner$confirmationScreenData$1;

    invoke-direct {v1, p0}, Lcom/squareup/feedback/AppFeedbackScopeRunner$confirmationScreenData$1;-><init>(Lcom/squareup/feedback/AppFeedbackScopeRunner;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "isMaxRating.map {\n      \u2026          )\n      )\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public goBackFromAppFeedbackConfirmationScreen()V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->feedbackSubmittedCompleter:Lcom/squareup/feedback/FeedbackSubmittedCompleter;

    invoke-interface {v0}, Lcom/squareup/feedback/FeedbackSubmittedCompleter;->exitFeedback()V

    return-void
.end method

.method public goBackFromFeedbackScreen()V
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/feedback/AppFeedbackScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    const-string v0, "RegisterTreeKey.get(scope)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/feedback/AppFeedbackScope;

    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->appFeedbackScope:Lcom/squareup/feedback/AppFeedbackScope;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public rateOnPlayStore(Landroid/content/Context;)V
    .locals 1

    const-string/jumbo v0, "viewContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->playStoreIntentCreator:Lcom/squareup/feedback/PlayStoreIntentCreator;

    invoke-virtual {v0}, Lcom/squareup/feedback/PlayStoreIntentCreator;->create()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public submitFeedbackAndRating(ILjava/lang/String;)V
    .locals 3

    const-string v0, "feedback"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->submitFeedbackSubscription:Lrx/subscriptions/SerialSubscription;

    .line 64
    iget-object v1, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->appFeedbackSubmitter:Lcom/squareup/feedback/AppFeedbackSubmitter;

    invoke-interface {v1, p1, p2}, Lcom/squareup/feedback/AppFeedbackSubmitter;->submit(ILjava/lang/String;)Lrx/Observable;

    move-result-object p2

    .line 65
    new-instance v1, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$1;

    invoke-direct {v1, p0}, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$1;-><init>(Lcom/squareup/feedback/AppFeedbackScopeRunner;)V

    check-cast v1, Lrx/functions/Action0;

    invoke-virtual {p2, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p2

    .line 66
    new-instance v1, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$2;

    invoke-direct {v1, p0}, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$2;-><init>(Lcom/squareup/feedback/AppFeedbackScopeRunner;)V

    check-cast v1, Lrx/functions/Action0;

    invoke-virtual {p2, v1}, Lrx/Observable;->doOnTerminate(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p2

    .line 67
    iget-object v1, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v2, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$3;->INSTANCE:Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$3;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {p2, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p2

    .line 68
    new-instance v1, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4;-><init>(Lcom/squareup/feedback/AppFeedbackScopeRunner;I)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {p2, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 63
    invoke-virtual {v0, p1}, Lrx/subscriptions/SerialSubscription;->set(Lrx/Subscription;)V

    return-void
.end method
