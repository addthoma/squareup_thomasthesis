.class final enum Lcom/squareup/currency_db/LocaleInfo;
.super Ljava/lang/Enum;
.source "LocaleInfo.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/currency_db/LocaleInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AA_DJ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AA_ER:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AA_ET:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AF:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AF_NA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AF_ZA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AK_GH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AM_ET:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_AE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_BH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_DJ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_DZ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_EG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_EH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_ER:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_IL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_IQ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_JO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_KM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_KW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_LB:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_LY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_MA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_MR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_OM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_PS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_QA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_SA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_SD:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_SO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_SS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_SY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_TD:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_TN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AR_YE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AS_IN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum AZ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum BE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum BE_BY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum BG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum BG_BG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum BM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum BN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum BN_BD:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum BN_IN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum BO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum BO_CN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum BO_IN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum BR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum BR_FR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum BS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum CA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum CA_AD:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum CA_ES:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum CA_FR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum CA_IT:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum CS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum CS_CZ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum CY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum CY_GB:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum DA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum DA_DK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum DA_GL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum DE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum DE_AT:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum DE_BE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum DE_CH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum DE_DE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum DE_LI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum DE_LU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum DZ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum DZ_BT:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EE_GH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EE_TG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EL_CY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EL_GR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_AG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_AI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_AS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_AU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_BB:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_BE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_BM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_BS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_BW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_BZ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_CA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_CC:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_CK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_CM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_CX:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_DG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_DM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_ER:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_FJ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_FK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_FM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_GB:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_GD:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_GG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_GH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_GI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_GM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_GU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_GY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_HK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_IE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_IM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_IN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_IO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_JE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_JM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_KE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_KI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_KN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_KY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_LC:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_LR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_LS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_MG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_MH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_MO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_MP:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_MS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_MT:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_MU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_MW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_MY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_NA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_NF:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_NG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_NR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_NU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_NZ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_PG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_PH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_PK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_PN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_PR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_PW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_RW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_SB:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_SC:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_SD:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_SG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_SH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_SL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_SS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_SX:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_SZ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_TC:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_TK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_TO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_TT:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_TV:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_TZ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_UG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_UM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_US:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_VC:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_VG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_VI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_VU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_WS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_ZA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_ZM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EN_ZW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_AR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_BO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_CL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_CO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_CR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_CU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_DO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_EA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_EC:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_ES:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_GQ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_GT:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_HN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_IC:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_MX:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_NI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_PA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_PE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_PH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_PR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_PY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_SV:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_US:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_UY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ES_VE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ET:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ET_EE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum EU_ES:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FA_AF:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FA_IR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FF:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FF_CM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FF_GN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FF_MR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FF_SN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FI_FI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FO_FO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_BE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_BF:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_BI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_BJ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_BL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_CA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_CD:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_CF:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_CG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_CH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_CI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_CM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_DJ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_DZ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_FR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_GA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_GF:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_GN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_GP:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_GQ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_HT:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_KM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_LU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_MA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_MC:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_MF:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_MG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_ML:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_MQ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_MR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_MU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_NC:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_NE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_PF:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_PM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_RE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_RW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_SC:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_SN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_SY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_TD:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_TG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_TN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_VU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_WF:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FR_YT:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum FY_NL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum GA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum GA_IE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum GD:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum GD_GB:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum GL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum GL_ES:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum GU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum GU_IN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum GV:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum GV_IM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum HA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum HE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum HE_IL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum HI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum HI_IN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum HR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum HR_BA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum HR_HR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum HU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum HU_HU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum HY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum HY_AM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum IA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum IA_FR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ID:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ID_ID:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum IG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum IG_NG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum II:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum II_CN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum IS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum IS_IS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum IT:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum IT_CH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum IT_IT:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum IT_SM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum JA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum JA_JP:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KA_GE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KI_KE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KL_GL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KM_KH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KN_IN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KO_KP:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KO_KR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KW_GB:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum KY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LB:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LB_LU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LG_UG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LN_AO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LN_CD:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LN_CF:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LN_CG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LO_LA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LT:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LT_LT:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LU_CD:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LV:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum LV_LV:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum MG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum MG_MG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum MK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum MK_MK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ML:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ML_IN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum MN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum MR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum MR_IN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum MS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum MT:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum MT_MT:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum MY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum MY_MM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NB:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NB_NO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NB_SJ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ND:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ND_ZW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NE_IN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NE_NP:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NL_AW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NL_BE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NL_BQ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NL_CW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NL_NL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NL_SR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NL_SX:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NN_NO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum NR_ZA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum OM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum OM_ET:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum OM_KE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum OR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum OR_IN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum OS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum OS_GE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum OS_RU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum PA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum PL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum PL_PL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum PS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum PS_AF:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum PT:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum PT_AO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum PT_BR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum PT_CV:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum PT_GW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum PT_MO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum PT_MZ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum PT_PT:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum PT_ST:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum PT_TL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum QU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum QU_BO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum QU_EC:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum QU_PE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum RM:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum RM_CH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum RN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum RN_BI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum RO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum RO_MD:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum RO_RO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum RU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum RU_BY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum RU_KG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum RU_KZ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum RU_MD:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum RU_RU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum RU_UA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum RW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum RW_RW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SE_FI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SE_NO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SE_SE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SG_CF:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SI_LK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SK_SK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SL_SI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SN_ZW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SO_DJ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SO_ET:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SO_KE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SO_SO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SQ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SQ_AL:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SQ_MK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SQ_XK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SS_SZ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SS_ZA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SV:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SV_AX:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SV_FI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SV_SE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SW_KE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SW_TZ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum SW_UG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TA_IN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TA_LK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TA_MY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TA_SG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TE_IN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TH_TH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TI_ER:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TI_ET:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TN_BW:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TN_ZA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TO_TO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TR_CY:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TR_TR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TS:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum TS_ZA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum UG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum UK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum UK_UA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum UR:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum UR_IN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum UR_PK:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum UZ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum VE:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum VE_ZA:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum VI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum VI_VN:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum VO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum YI:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum YO:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum YO_BJ:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum YO_NG:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ZH:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ZU:Lcom/squareup/currency_db/LocaleInfo;

.field public static final enum ZU_ZA:Lcom/squareup/currency_db/LocaleInfo;


# instance fields
.field final currencyPattern:Ljava/lang/String;

.field final decimalSeparator:C

.field final groupingSeparator:C

.field final numberPattern:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 4
    new-instance v7, Lcom/squareup/currency_db/LocaleInfo;

    const-string v1, "AA"

    const/4 v2, 0x0

    const/16 v3, 0x2e

    const/16 v4, 0x2c

    const-string/jumbo v5, "\u00a4#,##0.00"

    const-string v6, "#,##0.###"

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v7, Lcom/squareup/currency_db/LocaleInfo;->AA:Lcom/squareup/currency_db/LocaleInfo;

    .line 5
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AA_DJ"

    const/4 v10, 0x1

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AA_DJ:Lcom/squareup/currency_db/LocaleInfo;

    .line 6
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AA_ER"

    const/4 v3, 0x2

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AA_ER:Lcom/squareup/currency_db/LocaleInfo;

    .line 7
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AA_ET"

    const/4 v10, 0x3

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AA_ET:Lcom/squareup/currency_db/LocaleInfo;

    .line 8
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AF"

    const/4 v3, 0x4

    const/16 v4, 0x2c

    const/16 v5, 0xa0

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AF:Lcom/squareup/currency_db/LocaleInfo;

    .line 9
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AF_NA"

    const/4 v10, 0x5

    const/16 v11, 0x2c

    const/16 v12, 0xa0

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AF_NA:Lcom/squareup/currency_db/LocaleInfo;

    .line 10
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AF_ZA"

    const/4 v3, 0x6

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AF_ZA:Lcom/squareup/currency_db/LocaleInfo;

    .line 11
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AK"

    const/4 v10, 0x7

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AK:Lcom/squareup/currency_db/LocaleInfo;

    .line 12
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AK_GH"

    const/16 v3, 0x8

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AK_GH:Lcom/squareup/currency_db/LocaleInfo;

    .line 13
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AM"

    const/16 v10, 0x9

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AM:Lcom/squareup/currency_db/LocaleInfo;

    .line 14
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AM_ET"

    const/16 v3, 0xa

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AM_ET:Lcom/squareup/currency_db/LocaleInfo;

    .line 15
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AR"

    const/16 v10, 0xb

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR:Lcom/squareup/currency_db/LocaleInfo;

    .line 16
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AR_AE"

    const/16 v3, 0xc

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_AE:Lcom/squareup/currency_db/LocaleInfo;

    .line 17
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AR_BH"

    const/16 v10, 0xd

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_BH:Lcom/squareup/currency_db/LocaleInfo;

    .line 18
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AR_DJ"

    const/16 v3, 0xe

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_DJ:Lcom/squareup/currency_db/LocaleInfo;

    .line 19
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AR_DZ"

    const/16 v10, 0xf

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_DZ:Lcom/squareup/currency_db/LocaleInfo;

    .line 20
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AR_EG"

    const/16 v3, 0x10

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_EG:Lcom/squareup/currency_db/LocaleInfo;

    .line 21
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AR_EH"

    const/16 v10, 0x11

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_EH:Lcom/squareup/currency_db/LocaleInfo;

    .line 22
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AR_ER"

    const/16 v3, 0x12

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_ER:Lcom/squareup/currency_db/LocaleInfo;

    .line 23
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AR_IL"

    const/16 v10, 0x13

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_IL:Lcom/squareup/currency_db/LocaleInfo;

    .line 24
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AR_IQ"

    const/16 v3, 0x14

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_IQ:Lcom/squareup/currency_db/LocaleInfo;

    .line 25
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AR_JO"

    const/16 v10, 0x15

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_JO:Lcom/squareup/currency_db/LocaleInfo;

    .line 26
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AR_KM"

    const/16 v3, 0x16

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_KM:Lcom/squareup/currency_db/LocaleInfo;

    .line 27
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AR_KW"

    const/16 v10, 0x17

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_KW:Lcom/squareup/currency_db/LocaleInfo;

    .line 28
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AR_LB"

    const/16 v3, 0x18

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_LB:Lcom/squareup/currency_db/LocaleInfo;

    .line 29
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AR_LY"

    const/16 v10, 0x19

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_LY:Lcom/squareup/currency_db/LocaleInfo;

    .line 30
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AR_MA"

    const/16 v3, 0x1a

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_MA:Lcom/squareup/currency_db/LocaleInfo;

    .line 31
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AR_MR"

    const/16 v10, 0x1b

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_MR:Lcom/squareup/currency_db/LocaleInfo;

    .line 32
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AR_OM"

    const/16 v3, 0x1c

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_OM:Lcom/squareup/currency_db/LocaleInfo;

    .line 33
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AR_PS"

    const/16 v10, 0x1d

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_PS:Lcom/squareup/currency_db/LocaleInfo;

    .line 34
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AR_QA"

    const/16 v3, 0x1e

    const-string/jumbo v6, "\u00a4#0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_QA:Lcom/squareup/currency_db/LocaleInfo;

    .line 35
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AR_SA"

    const/16 v10, 0x1f

    const-string/jumbo v13, "\u00a4#0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_SA:Lcom/squareup/currency_db/LocaleInfo;

    .line 36
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AR_SD"

    const/16 v3, 0x20

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_SD:Lcom/squareup/currency_db/LocaleInfo;

    .line 37
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AR_SO"

    const/16 v10, 0x21

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_SO:Lcom/squareup/currency_db/LocaleInfo;

    .line 38
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AR_SS"

    const/16 v3, 0x22

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_SS:Lcom/squareup/currency_db/LocaleInfo;

    .line 39
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AR_SY"

    const/16 v10, 0x23

    const-string/jumbo v13, "\u00a4#0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_SY:Lcom/squareup/currency_db/LocaleInfo;

    .line 40
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AR_TD"

    const/16 v3, 0x24

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_TD:Lcom/squareup/currency_db/LocaleInfo;

    .line 41
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AR_TN"

    const/16 v10, 0x25

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string/jumbo v13, "\u00a4#0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_TN:Lcom/squareup/currency_db/LocaleInfo;

    .line 42
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AR_YE"

    const/16 v3, 0x26

    const-string/jumbo v6, "\u00a4#0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AR_YE:Lcom/squareup/currency_db/LocaleInfo;

    .line 43
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AS"

    const/16 v10, 0x27

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4\u00a0#,##,##0.00"

    const-string v14, "#,##,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AS:Lcom/squareup/currency_db/LocaleInfo;

    .line 44
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "AS_IN"

    const/16 v3, 0x28

    const-string/jumbo v6, "\u00a4\u00a0#,##,##0.00"

    const-string v7, "#,##,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AS_IN:Lcom/squareup/currency_db/LocaleInfo;

    .line 45
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "AZ"

    const/16 v10, 0x29

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->AZ:Lcom/squareup/currency_db/LocaleInfo;

    .line 46
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "BE"

    const/16 v3, 0x2a

    const/16 v4, 0x2c

    const/16 v5, 0xa0

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->BE:Lcom/squareup/currency_db/LocaleInfo;

    .line 47
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "BE_BY"

    const/16 v10, 0x2b

    const/16 v12, 0xa0

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->BE_BY:Lcom/squareup/currency_db/LocaleInfo;

    .line 48
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "BG"

    const/16 v3, 0x2c

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->BG:Lcom/squareup/currency_db/LocaleInfo;

    .line 49
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "BG_BG"

    const/16 v10, 0x2d

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->BG_BG:Lcom/squareup/currency_db/LocaleInfo;

    .line 50
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "BM"

    const/16 v3, 0x2e

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->BM:Lcom/squareup/currency_db/LocaleInfo;

    .line 51
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "BN"

    const/16 v10, 0x2f

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string v13, "#,##,##0.00\u00a4"

    const-string v14, "#,##,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->BN:Lcom/squareup/currency_db/LocaleInfo;

    .line 52
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "BN_BD"

    const/16 v3, 0x30

    const-string v6, "#,##,##0.00\u00a4"

    const-string v7, "#,##,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->BN_BD:Lcom/squareup/currency_db/LocaleInfo;

    .line 53
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "BN_IN"

    const/16 v10, 0x31

    const-string v13, "#,##,##0.00\u00a4"

    const-string v14, "#,##,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->BN_IN:Lcom/squareup/currency_db/LocaleInfo;

    .line 54
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "BO"

    const/16 v3, 0x32

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->BO:Lcom/squareup/currency_db/LocaleInfo;

    .line 55
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "BO_CN"

    const/16 v10, 0x33

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->BO_CN:Lcom/squareup/currency_db/LocaleInfo;

    .line 56
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "BO_IN"

    const/16 v3, 0x34

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->BO_IN:Lcom/squareup/currency_db/LocaleInfo;

    .line 57
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "BR"

    const/16 v10, 0x35

    const/16 v11, 0x2c

    const/16 v12, 0xa0

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->BR:Lcom/squareup/currency_db/LocaleInfo;

    .line 58
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "BR_FR"

    const/16 v3, 0x36

    const/16 v4, 0x2c

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->BR_FR:Lcom/squareup/currency_db/LocaleInfo;

    .line 59
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "BS"

    const/16 v10, 0x37

    const/16 v12, 0x2e

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->BS:Lcom/squareup/currency_db/LocaleInfo;

    .line 60
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "CA"

    const/16 v3, 0x38

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->CA:Lcom/squareup/currency_db/LocaleInfo;

    .line 61
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "CA_AD"

    const/16 v10, 0x39

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->CA_AD:Lcom/squareup/currency_db/LocaleInfo;

    .line 62
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "CA_ES"

    const/16 v3, 0x3a

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->CA_ES:Lcom/squareup/currency_db/LocaleInfo;

    .line 63
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "CA_FR"

    const/16 v10, 0x3b

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->CA_FR:Lcom/squareup/currency_db/LocaleInfo;

    .line 64
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "CA_IT"

    const/16 v3, 0x3c

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->CA_IT:Lcom/squareup/currency_db/LocaleInfo;

    .line 65
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "CS"

    const/16 v10, 0x3d

    const/16 v12, 0xa0

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->CS:Lcom/squareup/currency_db/LocaleInfo;

    .line 66
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "CS_CZ"

    const/16 v3, 0x3e

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->CS_CZ:Lcom/squareup/currency_db/LocaleInfo;

    .line 67
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "CY"

    const/16 v10, 0x3f

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->CY:Lcom/squareup/currency_db/LocaleInfo;

    .line 68
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "CY_GB"

    const/16 v3, 0x40

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->CY_GB:Lcom/squareup/currency_db/LocaleInfo;

    .line 69
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "DA"

    const/16 v10, 0x41

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->DA:Lcom/squareup/currency_db/LocaleInfo;

    .line 70
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "DA_DK"

    const/16 v3, 0x42

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->DA_DK:Lcom/squareup/currency_db/LocaleInfo;

    .line 71
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "DA_GL"

    const/16 v10, 0x43

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->DA_GL:Lcom/squareup/currency_db/LocaleInfo;

    .line 72
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "DE"

    const/16 v3, 0x44

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->DE:Lcom/squareup/currency_db/LocaleInfo;

    .line 73
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "DE_AT"

    const/16 v10, 0x45

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->DE_AT:Lcom/squareup/currency_db/LocaleInfo;

    .line 74
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "DE_BE"

    const/16 v3, 0x46

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->DE_BE:Lcom/squareup/currency_db/LocaleInfo;

    .line 75
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "DE_CH"

    const/16 v10, 0x47

    const/16 v11, 0x2e

    const/16 v12, 0x27

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00;\u00a4-#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->DE_CH:Lcom/squareup/currency_db/LocaleInfo;

    .line 76
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "DE_DE"

    const/16 v3, 0x48

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->DE_DE:Lcom/squareup/currency_db/LocaleInfo;

    .line 77
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "DE_LI"

    const/16 v10, 0x49

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->DE_LI:Lcom/squareup/currency_db/LocaleInfo;

    .line 78
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "DE_LU"

    const/16 v3, 0x4a

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->DE_LU:Lcom/squareup/currency_db/LocaleInfo;

    .line 79
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "DZ"

    const/16 v10, 0x4b

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##,##0.00"

    const-string v14, "#,##,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->DZ:Lcom/squareup/currency_db/LocaleInfo;

    .line 80
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "DZ_BT"

    const/16 v3, 0x4c

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##,##0.00"

    const-string v7, "#,##,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->DZ_BT:Lcom/squareup/currency_db/LocaleInfo;

    .line 81
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EE"

    const/16 v10, 0x4d

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EE:Lcom/squareup/currency_db/LocaleInfo;

    .line 82
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EE_GH"

    const/16 v3, 0x4e

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EE_GH:Lcom/squareup/currency_db/LocaleInfo;

    .line 83
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EE_TG"

    const/16 v10, 0x4f

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EE_TG:Lcom/squareup/currency_db/LocaleInfo;

    .line 84
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EL"

    const/16 v3, 0x50

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EL:Lcom/squareup/currency_db/LocaleInfo;

    .line 85
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EL_CY"

    const/16 v10, 0x51

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EL_CY:Lcom/squareup/currency_db/LocaleInfo;

    .line 86
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EL_GR"

    const/16 v3, 0x52

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EL_GR:Lcom/squareup/currency_db/LocaleInfo;

    .line 87
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN"

    const/16 v10, 0x53

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN:Lcom/squareup/currency_db/LocaleInfo;

    .line 88
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_AG"

    const/16 v3, 0x54

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_AG:Lcom/squareup/currency_db/LocaleInfo;

    .line 89
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_AI"

    const/16 v10, 0x55

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_AI:Lcom/squareup/currency_db/LocaleInfo;

    .line 90
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_AS"

    const/16 v3, 0x56

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_AS:Lcom/squareup/currency_db/LocaleInfo;

    .line 91
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_AU"

    const/16 v10, 0x57

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_AU:Lcom/squareup/currency_db/LocaleInfo;

    .line 92
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_BB"

    const/16 v3, 0x58

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_BB:Lcom/squareup/currency_db/LocaleInfo;

    .line 93
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_BE"

    const/16 v10, 0x59

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_BE:Lcom/squareup/currency_db/LocaleInfo;

    .line 94
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_BM"

    const/16 v3, 0x5a

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_BM:Lcom/squareup/currency_db/LocaleInfo;

    .line 95
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_BS"

    const/16 v10, 0x5b

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_BS:Lcom/squareup/currency_db/LocaleInfo;

    .line 96
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_BW"

    const/16 v3, 0x5c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_BW:Lcom/squareup/currency_db/LocaleInfo;

    .line 97
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_BZ"

    const/16 v10, 0x5d

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_BZ:Lcom/squareup/currency_db/LocaleInfo;

    .line 98
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_CA"

    const/16 v3, 0x5e

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_CA:Lcom/squareup/currency_db/LocaleInfo;

    .line 99
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_CC"

    const/16 v10, 0x5f

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_CC:Lcom/squareup/currency_db/LocaleInfo;

    .line 100
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_CK"

    const/16 v3, 0x60

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_CK:Lcom/squareup/currency_db/LocaleInfo;

    .line 101
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_CM"

    const/16 v10, 0x61

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_CM:Lcom/squareup/currency_db/LocaleInfo;

    .line 102
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_CX"

    const/16 v3, 0x62

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_CX:Lcom/squareup/currency_db/LocaleInfo;

    .line 103
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_DG"

    const/16 v10, 0x63

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_DG:Lcom/squareup/currency_db/LocaleInfo;

    .line 104
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_DM"

    const/16 v3, 0x64

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_DM:Lcom/squareup/currency_db/LocaleInfo;

    .line 105
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_ER"

    const/16 v10, 0x65

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_ER:Lcom/squareup/currency_db/LocaleInfo;

    .line 106
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_FJ"

    const/16 v3, 0x66

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_FJ:Lcom/squareup/currency_db/LocaleInfo;

    .line 107
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_FK"

    const/16 v10, 0x67

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_FK:Lcom/squareup/currency_db/LocaleInfo;

    .line 108
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_FM"

    const/16 v3, 0x68

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_FM:Lcom/squareup/currency_db/LocaleInfo;

    .line 109
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_GB"

    const/16 v10, 0x69

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_GB:Lcom/squareup/currency_db/LocaleInfo;

    .line 110
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_GD"

    const/16 v3, 0x6a

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_GD:Lcom/squareup/currency_db/LocaleInfo;

    .line 111
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_GG"

    const/16 v10, 0x6b

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_GG:Lcom/squareup/currency_db/LocaleInfo;

    .line 112
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_GH"

    const/16 v3, 0x6c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_GH:Lcom/squareup/currency_db/LocaleInfo;

    .line 113
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_GI"

    const/16 v10, 0x6d

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_GI:Lcom/squareup/currency_db/LocaleInfo;

    .line 114
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_GM"

    const/16 v3, 0x6e

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_GM:Lcom/squareup/currency_db/LocaleInfo;

    .line 115
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_GU"

    const/16 v10, 0x6f

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_GU:Lcom/squareup/currency_db/LocaleInfo;

    .line 116
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_GY"

    const/16 v3, 0x70

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_GY:Lcom/squareup/currency_db/LocaleInfo;

    .line 117
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_HK"

    const/16 v10, 0x71

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_HK:Lcom/squareup/currency_db/LocaleInfo;

    .line 118
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_IE"

    const/16 v3, 0x72

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_IE:Lcom/squareup/currency_db/LocaleInfo;

    .line 119
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_IM"

    const/16 v10, 0x73

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_IM:Lcom/squareup/currency_db/LocaleInfo;

    .line 120
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_IN"

    const/16 v3, 0x74

    const-string/jumbo v6, "\u00a4\u00a0#,##,##0.00"

    const-string v7, "#,##,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_IN:Lcom/squareup/currency_db/LocaleInfo;

    .line 121
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_IO"

    const/16 v10, 0x75

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_IO:Lcom/squareup/currency_db/LocaleInfo;

    .line 122
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_JE"

    const/16 v3, 0x76

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_JE:Lcom/squareup/currency_db/LocaleInfo;

    .line 123
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_JM"

    const/16 v10, 0x77

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_JM:Lcom/squareup/currency_db/LocaleInfo;

    .line 124
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_KE"

    const/16 v3, 0x78

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_KE:Lcom/squareup/currency_db/LocaleInfo;

    .line 125
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_KI"

    const/16 v10, 0x79

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_KI:Lcom/squareup/currency_db/LocaleInfo;

    .line 126
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_KN"

    const/16 v3, 0x7a

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_KN:Lcom/squareup/currency_db/LocaleInfo;

    .line 127
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_KY"

    const/16 v10, 0x7b

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_KY:Lcom/squareup/currency_db/LocaleInfo;

    .line 128
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_LC"

    const/16 v3, 0x7c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_LC:Lcom/squareup/currency_db/LocaleInfo;

    .line 129
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_LR"

    const/16 v10, 0x7d

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_LR:Lcom/squareup/currency_db/LocaleInfo;

    .line 130
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_LS"

    const/16 v3, 0x7e

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_LS:Lcom/squareup/currency_db/LocaleInfo;

    .line 131
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_MG"

    const/16 v10, 0x7f

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_MG:Lcom/squareup/currency_db/LocaleInfo;

    .line 132
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_MH"

    const/16 v3, 0x80

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_MH:Lcom/squareup/currency_db/LocaleInfo;

    .line 133
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_MO"

    const/16 v10, 0x81

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_MO:Lcom/squareup/currency_db/LocaleInfo;

    .line 134
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_MP"

    const/16 v3, 0x82

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_MP:Lcom/squareup/currency_db/LocaleInfo;

    .line 135
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_MS"

    const/16 v10, 0x83

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_MS:Lcom/squareup/currency_db/LocaleInfo;

    .line 136
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_MT"

    const/16 v3, 0x84

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_MT:Lcom/squareup/currency_db/LocaleInfo;

    .line 137
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_MU"

    const/16 v10, 0x85

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_MU:Lcom/squareup/currency_db/LocaleInfo;

    .line 138
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_MW"

    const/16 v3, 0x86

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_MW:Lcom/squareup/currency_db/LocaleInfo;

    .line 139
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_MY"

    const/16 v10, 0x87

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_MY:Lcom/squareup/currency_db/LocaleInfo;

    .line 140
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_NA"

    const/16 v3, 0x88

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_NA:Lcom/squareup/currency_db/LocaleInfo;

    .line 141
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_NF"

    const/16 v10, 0x89

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_NF:Lcom/squareup/currency_db/LocaleInfo;

    .line 142
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_NG"

    const/16 v3, 0x8a

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_NG:Lcom/squareup/currency_db/LocaleInfo;

    .line 143
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_NR"

    const/16 v10, 0x8b

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_NR:Lcom/squareup/currency_db/LocaleInfo;

    .line 144
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_NU"

    const/16 v3, 0x8c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_NU:Lcom/squareup/currency_db/LocaleInfo;

    .line 145
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_NZ"

    const/16 v10, 0x8d

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_NZ:Lcom/squareup/currency_db/LocaleInfo;

    .line 146
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_PG"

    const/16 v3, 0x8e

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_PG:Lcom/squareup/currency_db/LocaleInfo;

    .line 147
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_PH"

    const/16 v10, 0x8f

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_PH:Lcom/squareup/currency_db/LocaleInfo;

    .line 148
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_PK"

    const/16 v3, 0x90

    const-string/jumbo v6, "\u00a4\u00a0#,##,##0.00"

    const-string v7, "#,##,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_PK:Lcom/squareup/currency_db/LocaleInfo;

    .line 149
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_PN"

    const/16 v10, 0x91

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_PN:Lcom/squareup/currency_db/LocaleInfo;

    .line 150
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_PR"

    const/16 v3, 0x92

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_PR:Lcom/squareup/currency_db/LocaleInfo;

    .line 151
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_PW"

    const/16 v10, 0x93

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_PW:Lcom/squareup/currency_db/LocaleInfo;

    .line 152
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_RW"

    const/16 v3, 0x94

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_RW:Lcom/squareup/currency_db/LocaleInfo;

    .line 153
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_SB"

    const/16 v10, 0x95

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_SB:Lcom/squareup/currency_db/LocaleInfo;

    .line 154
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_SC"

    const/16 v3, 0x96

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_SC:Lcom/squareup/currency_db/LocaleInfo;

    .line 155
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_SD"

    const/16 v10, 0x97

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_SD:Lcom/squareup/currency_db/LocaleInfo;

    .line 156
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_SG"

    const/16 v3, 0x98

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_SG:Lcom/squareup/currency_db/LocaleInfo;

    .line 157
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_SH"

    const/16 v10, 0x99

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_SH:Lcom/squareup/currency_db/LocaleInfo;

    .line 158
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_SL"

    const/16 v3, 0x9a

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_SL:Lcom/squareup/currency_db/LocaleInfo;

    .line 159
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_SS"

    const/16 v10, 0x9b

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_SS:Lcom/squareup/currency_db/LocaleInfo;

    .line 160
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_SX"

    const/16 v3, 0x9c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_SX:Lcom/squareup/currency_db/LocaleInfo;

    .line 161
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_SZ"

    const/16 v10, 0x9d

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_SZ:Lcom/squareup/currency_db/LocaleInfo;

    .line 162
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_TC"

    const/16 v3, 0x9e

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_TC:Lcom/squareup/currency_db/LocaleInfo;

    .line 163
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_TK"

    const/16 v10, 0x9f

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_TK:Lcom/squareup/currency_db/LocaleInfo;

    .line 164
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_TO"

    const/16 v3, 0xa0

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_TO:Lcom/squareup/currency_db/LocaleInfo;

    .line 165
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_TT"

    const/16 v10, 0xa1

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_TT:Lcom/squareup/currency_db/LocaleInfo;

    .line 166
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_TV"

    const/16 v3, 0xa2

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_TV:Lcom/squareup/currency_db/LocaleInfo;

    .line 167
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_TZ"

    const/16 v10, 0xa3

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_TZ:Lcom/squareup/currency_db/LocaleInfo;

    .line 168
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_UG"

    const/16 v3, 0xa4

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_UG:Lcom/squareup/currency_db/LocaleInfo;

    .line 169
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_UM"

    const/16 v10, 0xa5

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_UM:Lcom/squareup/currency_db/LocaleInfo;

    .line 170
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_US"

    const/16 v3, 0xa6

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_US:Lcom/squareup/currency_db/LocaleInfo;

    .line 171
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_VC"

    const/16 v10, 0xa7

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_VC:Lcom/squareup/currency_db/LocaleInfo;

    .line 172
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_VG"

    const/16 v3, 0xa8

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_VG:Lcom/squareup/currency_db/LocaleInfo;

    .line 173
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_VI"

    const/16 v10, 0xa9

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_VI:Lcom/squareup/currency_db/LocaleInfo;

    .line 174
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_VU"

    const/16 v3, 0xaa

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_VU:Lcom/squareup/currency_db/LocaleInfo;

    .line 175
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_WS"

    const/16 v10, 0xab

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_WS:Lcom/squareup/currency_db/LocaleInfo;

    .line 176
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_ZA"

    const/16 v3, 0xac

    const/16 v4, 0x2c

    const/16 v5, 0xa0

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_ZA:Lcom/squareup/currency_db/LocaleInfo;

    .line 177
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EN_ZM"

    const/16 v10, 0xad

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_ZM:Lcom/squareup/currency_db/LocaleInfo;

    .line 178
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EN_ZW"

    const/16 v3, 0xae

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EN_ZW:Lcom/squareup/currency_db/LocaleInfo;

    .line 179
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EO"

    const/16 v10, 0xaf

    const/16 v11, 0x2c

    const/16 v12, 0xa0

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EO:Lcom/squareup/currency_db/LocaleInfo;

    .line 180
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ES"

    const/16 v3, 0xb0

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES:Lcom/squareup/currency_db/LocaleInfo;

    .line 181
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ES_AR"

    const/16 v10, 0xb1

    const/16 v12, 0x2e

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_AR:Lcom/squareup/currency_db/LocaleInfo;

    .line 182
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ES_BO"

    const/16 v3, 0xb2

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_BO:Lcom/squareup/currency_db/LocaleInfo;

    .line 183
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ES_CL"

    const/16 v10, 0xb3

    const-string/jumbo v13, "\u00a4#,##0.00;\u00a4-#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_CL:Lcom/squareup/currency_db/LocaleInfo;

    .line 184
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ES_CO"

    const/16 v3, 0xb4

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_CO:Lcom/squareup/currency_db/LocaleInfo;

    .line 185
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ES_CR"

    const/16 v10, 0xb5

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_CR:Lcom/squareup/currency_db/LocaleInfo;

    .line 186
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ES_CU"

    const/16 v3, 0xb6

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_CU:Lcom/squareup/currency_db/LocaleInfo;

    .line 187
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ES_DO"

    const/16 v10, 0xb7

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_DO:Lcom/squareup/currency_db/LocaleInfo;

    .line 188
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ES_EA"

    const/16 v3, 0xb8

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_EA:Lcom/squareup/currency_db/LocaleInfo;

    .line 189
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ES_EC"

    const/16 v10, 0xb9

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string/jumbo v13, "\u00a4#,##0.00;\u00a4-#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_EC:Lcom/squareup/currency_db/LocaleInfo;

    .line 190
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ES_ES"

    const/16 v3, 0xba

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_ES:Lcom/squareup/currency_db/LocaleInfo;

    .line 191
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ES_GQ"

    const/16 v10, 0xbb

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_GQ:Lcom/squareup/currency_db/LocaleInfo;

    .line 192
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ES_GT"

    const/16 v3, 0xbc

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_GT:Lcom/squareup/currency_db/LocaleInfo;

    .line 193
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ES_HN"

    const/16 v10, 0xbd

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_HN:Lcom/squareup/currency_db/LocaleInfo;

    .line 194
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ES_IC"

    const/16 v3, 0xbe

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_IC:Lcom/squareup/currency_db/LocaleInfo;

    .line 195
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ES_MX"

    const/16 v10, 0xbf

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_MX:Lcom/squareup/currency_db/LocaleInfo;

    .line 196
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ES_NI"

    const/16 v3, 0xc0

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_NI:Lcom/squareup/currency_db/LocaleInfo;

    .line 197
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ES_PA"

    const/16 v10, 0xc1

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_PA:Lcom/squareup/currency_db/LocaleInfo;

    .line 198
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ES_PE"

    const/16 v3, 0xc2

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_PE:Lcom/squareup/currency_db/LocaleInfo;

    .line 199
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ES_PH"

    const/16 v10, 0xc3

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_PH:Lcom/squareup/currency_db/LocaleInfo;

    .line 200
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ES_PR"

    const/16 v3, 0xc4

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_PR:Lcom/squareup/currency_db/LocaleInfo;

    .line 201
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ES_PY"

    const/16 v10, 0xc5

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00;\u00a4\u00a0-#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_PY:Lcom/squareup/currency_db/LocaleInfo;

    .line 202
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ES_SV"

    const/16 v3, 0xc6

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_SV:Lcom/squareup/currency_db/LocaleInfo;

    .line 203
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ES_US"

    const/16 v10, 0xc7

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_US:Lcom/squareup/currency_db/LocaleInfo;

    .line 204
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ES_UY"

    const/16 v3, 0xc8

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_UY:Lcom/squareup/currency_db/LocaleInfo;

    .line 205
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ES_VE"

    const/16 v10, 0xc9

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string/jumbo v13, "\u00a4#,##0.00;\u00a4-#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ES_VE:Lcom/squareup/currency_db/LocaleInfo;

    .line 206
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ET"

    const/16 v3, 0xca

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ET:Lcom/squareup/currency_db/LocaleInfo;

    .line 207
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ET_EE"

    const/16 v10, 0xcb

    const/16 v12, 0xa0

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ET_EE:Lcom/squareup/currency_db/LocaleInfo;

    .line 208
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "EU"

    const/16 v3, 0xcc

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EU:Lcom/squareup/currency_db/LocaleInfo;

    .line 209
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "EU_ES"

    const/16 v10, 0xcd

    const/16 v12, 0x2e

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->EU_ES:Lcom/squareup/currency_db/LocaleInfo;

    .line 210
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FA"

    const/16 v3, 0xce

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u200e\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FA:Lcom/squareup/currency_db/LocaleInfo;

    .line 211
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FA_AF"

    const/16 v10, 0xcf

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u200e\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FA_AF:Lcom/squareup/currency_db/LocaleInfo;

    .line 212
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FA_IR"

    const/16 v3, 0xd0

    const-string/jumbo v6, "\u200e\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FA_IR:Lcom/squareup/currency_db/LocaleInfo;

    .line 213
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FF"

    const/16 v10, 0xd1

    const/16 v11, 0x2c

    const/16 v12, 0xa0

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FF:Lcom/squareup/currency_db/LocaleInfo;

    .line 214
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FF_CM"

    const/16 v3, 0xd2

    const/16 v4, 0x2c

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FF_CM:Lcom/squareup/currency_db/LocaleInfo;

    .line 215
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FF_GN"

    const/16 v10, 0xd3

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FF_GN:Lcom/squareup/currency_db/LocaleInfo;

    .line 216
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FF_MR"

    const/16 v3, 0xd4

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FF_MR:Lcom/squareup/currency_db/LocaleInfo;

    .line 217
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FF_SN"

    const/16 v10, 0xd5

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FF_SN:Lcom/squareup/currency_db/LocaleInfo;

    .line 218
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FI"

    const/16 v3, 0xd6

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FI:Lcom/squareup/currency_db/LocaleInfo;

    .line 219
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FI_FI"

    const/16 v10, 0xd7

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FI_FI:Lcom/squareup/currency_db/LocaleInfo;

    .line 220
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FO"

    const/16 v3, 0xd8

    const/16 v5, 0x2e

    const-string/jumbo v6, "\u00a4#,##0.00;\u00a4-#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FO:Lcom/squareup/currency_db/LocaleInfo;

    .line 221
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FO_FO"

    const/16 v10, 0xd9

    const/16 v12, 0x2e

    const-string/jumbo v13, "\u00a4#,##0.00;\u00a4-#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FO_FO:Lcom/squareup/currency_db/LocaleInfo;

    .line 222
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR"

    const/16 v3, 0xda

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR:Lcom/squareup/currency_db/LocaleInfo;

    .line 223
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_BE"

    const/16 v10, 0xdb

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_BE:Lcom/squareup/currency_db/LocaleInfo;

    .line 224
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_BF"

    const/16 v3, 0xdc

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_BF:Lcom/squareup/currency_db/LocaleInfo;

    .line 225
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_BI"

    const/16 v10, 0xdd

    const/16 v12, 0xa0

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_BI:Lcom/squareup/currency_db/LocaleInfo;

    .line 226
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_BJ"

    const/16 v3, 0xde

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_BJ:Lcom/squareup/currency_db/LocaleInfo;

    .line 227
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_BL"

    const/16 v10, 0xdf

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_BL:Lcom/squareup/currency_db/LocaleInfo;

    .line 228
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_CA"

    const/16 v3, 0xe0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_CA:Lcom/squareup/currency_db/LocaleInfo;

    .line 229
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_CD"

    const/16 v10, 0xe1

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_CD:Lcom/squareup/currency_db/LocaleInfo;

    .line 230
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_CF"

    const/16 v3, 0xe2

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_CF:Lcom/squareup/currency_db/LocaleInfo;

    .line 231
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_CG"

    const/16 v10, 0xe3

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_CG:Lcom/squareup/currency_db/LocaleInfo;

    .line 232
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_CH"

    const/16 v3, 0xe4

    const/16 v4, 0x2e

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00;\u00a4-#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_CH:Lcom/squareup/currency_db/LocaleInfo;

    .line 233
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_CI"

    const/16 v10, 0xe5

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_CI:Lcom/squareup/currency_db/LocaleInfo;

    .line 234
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_CM"

    const/16 v3, 0xe6

    const/16 v4, 0x2c

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_CM:Lcom/squareup/currency_db/LocaleInfo;

    .line 235
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_DJ"

    const/16 v10, 0xe7

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_DJ:Lcom/squareup/currency_db/LocaleInfo;

    .line 236
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_DZ"

    const/16 v3, 0xe8

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_DZ:Lcom/squareup/currency_db/LocaleInfo;

    .line 237
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_FR"

    const/16 v10, 0xe9

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_FR:Lcom/squareup/currency_db/LocaleInfo;

    .line 238
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_GA"

    const/16 v3, 0xea

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_GA:Lcom/squareup/currency_db/LocaleInfo;

    .line 239
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_GF"

    const/16 v10, 0xeb

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_GF:Lcom/squareup/currency_db/LocaleInfo;

    .line 240
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_GN"

    const/16 v3, 0xec

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_GN:Lcom/squareup/currency_db/LocaleInfo;

    .line 241
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_GP"

    const/16 v10, 0xed

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_GP:Lcom/squareup/currency_db/LocaleInfo;

    .line 242
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_GQ"

    const/16 v3, 0xee

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_GQ:Lcom/squareup/currency_db/LocaleInfo;

    .line 243
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_HT"

    const/16 v10, 0xef

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_HT:Lcom/squareup/currency_db/LocaleInfo;

    .line 244
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_KM"

    const/16 v3, 0xf0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_KM:Lcom/squareup/currency_db/LocaleInfo;

    .line 245
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_LU"

    const/16 v10, 0xf1

    const/16 v12, 0x2e

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_LU:Lcom/squareup/currency_db/LocaleInfo;

    .line 246
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_MA"

    const/16 v3, 0xf2

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_MA:Lcom/squareup/currency_db/LocaleInfo;

    .line 247
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_MC"

    const/16 v10, 0xf3

    const/16 v12, 0xa0

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_MC:Lcom/squareup/currency_db/LocaleInfo;

    .line 248
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_MF"

    const/16 v3, 0xf4

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_MF:Lcom/squareup/currency_db/LocaleInfo;

    .line 249
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_MG"

    const/16 v10, 0xf5

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_MG:Lcom/squareup/currency_db/LocaleInfo;

    .line 250
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_ML"

    const/16 v3, 0xf6

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_ML:Lcom/squareup/currency_db/LocaleInfo;

    .line 251
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_MQ"

    const/16 v10, 0xf7

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_MQ:Lcom/squareup/currency_db/LocaleInfo;

    .line 252
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_MR"

    const/16 v3, 0xf8

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_MR:Lcom/squareup/currency_db/LocaleInfo;

    .line 253
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_MU"

    const/16 v10, 0xf9

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_MU:Lcom/squareup/currency_db/LocaleInfo;

    .line 254
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_NC"

    const/16 v3, 0xfa

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_NC:Lcom/squareup/currency_db/LocaleInfo;

    .line 255
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_NE"

    const/16 v10, 0xfb

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_NE:Lcom/squareup/currency_db/LocaleInfo;

    .line 256
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_PF"

    const/16 v3, 0xfc

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_PF:Lcom/squareup/currency_db/LocaleInfo;

    .line 257
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_PM"

    const/16 v10, 0xfd

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_PM:Lcom/squareup/currency_db/LocaleInfo;

    .line 258
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_RE"

    const/16 v3, 0xfe

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_RE:Lcom/squareup/currency_db/LocaleInfo;

    .line 259
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_RW"

    const/16 v10, 0xff

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_RW:Lcom/squareup/currency_db/LocaleInfo;

    .line 260
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_SC"

    const/16 v3, 0x100

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_SC:Lcom/squareup/currency_db/LocaleInfo;

    .line 261
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_SN"

    const/16 v10, 0x101

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_SN:Lcom/squareup/currency_db/LocaleInfo;

    .line 262
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_SY"

    const/16 v3, 0x102

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_SY:Lcom/squareup/currency_db/LocaleInfo;

    .line 263
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_TD"

    const/16 v10, 0x103

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_TD:Lcom/squareup/currency_db/LocaleInfo;

    .line 264
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_TG"

    const/16 v3, 0x104

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_TG:Lcom/squareup/currency_db/LocaleInfo;

    .line 265
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_TN"

    const/16 v10, 0x105

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_TN:Lcom/squareup/currency_db/LocaleInfo;

    .line 266
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_VU"

    const/16 v3, 0x106

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_VU:Lcom/squareup/currency_db/LocaleInfo;

    .line 267
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FR_WF"

    const/16 v10, 0x107

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_WF:Lcom/squareup/currency_db/LocaleInfo;

    .line 268
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FR_YT"

    const/16 v3, 0x108

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FR_YT:Lcom/squareup/currency_db/LocaleInfo;

    .line 269
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "FY"

    const/16 v10, 0x109

    const/16 v12, 0x2e

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00;\u00a4\u00a0#,##0.00-"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FY:Lcom/squareup/currency_db/LocaleInfo;

    .line 270
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "FY_NL"

    const/16 v3, 0x10a

    const/16 v5, 0x2e

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00;\u00a4\u00a0#,##0.00-"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->FY_NL:Lcom/squareup/currency_db/LocaleInfo;

    .line 271
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "GA"

    const/16 v10, 0x10b

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->GA:Lcom/squareup/currency_db/LocaleInfo;

    .line 272
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "GA_IE"

    const/16 v3, 0x10c

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->GA_IE:Lcom/squareup/currency_db/LocaleInfo;

    .line 273
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "GD"

    const/16 v10, 0x10d

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->GD:Lcom/squareup/currency_db/LocaleInfo;

    .line 274
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "GD_GB"

    const/16 v3, 0x10e

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->GD_GB:Lcom/squareup/currency_db/LocaleInfo;

    .line 275
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "GL"

    const/16 v10, 0x10f

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->GL:Lcom/squareup/currency_db/LocaleInfo;

    .line 276
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "GL_ES"

    const/16 v3, 0x110

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->GL_ES:Lcom/squareup/currency_db/LocaleInfo;

    .line 277
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "GU"

    const/16 v10, 0x111

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##,##0.00"

    const-string v14, "#,##,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->GU:Lcom/squareup/currency_db/LocaleInfo;

    .line 278
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "GU_IN"

    const/16 v3, 0x112

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##,##0.00"

    const-string v7, "#,##,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->GU_IN:Lcom/squareup/currency_db/LocaleInfo;

    .line 279
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "GV"

    const/16 v10, 0x113

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->GV:Lcom/squareup/currency_db/LocaleInfo;

    .line 280
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "GV_IM"

    const/16 v3, 0x114

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->GV_IM:Lcom/squareup/currency_db/LocaleInfo;

    .line 281
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "HA"

    const/16 v10, 0x115

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->HA:Lcom/squareup/currency_db/LocaleInfo;

    .line 282
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "HE"

    const/16 v3, 0x116

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->HE:Lcom/squareup/currency_db/LocaleInfo;

    .line 283
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "HE_IL"

    const/16 v10, 0x117

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->HE_IL:Lcom/squareup/currency_db/LocaleInfo;

    .line 284
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "HI"

    const/16 v3, 0x118

    const-string/jumbo v6, "\u00a4#,##,##0.00"

    const-string v7, "#,##,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->HI:Lcom/squareup/currency_db/LocaleInfo;

    .line 285
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "HI_IN"

    const/16 v10, 0x119

    const-string/jumbo v13, "\u00a4#,##,##0.00"

    const-string v14, "#,##,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->HI_IN:Lcom/squareup/currency_db/LocaleInfo;

    .line 286
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "HR"

    const/16 v3, 0x11a

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->HR:Lcom/squareup/currency_db/LocaleInfo;

    .line 287
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "HR_BA"

    const/16 v10, 0x11b

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->HR_BA:Lcom/squareup/currency_db/LocaleInfo;

    .line 288
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "HR_HR"

    const/16 v3, 0x11c

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->HR_HR:Lcom/squareup/currency_db/LocaleInfo;

    .line 289
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "HU"

    const/16 v10, 0x11d

    const/16 v12, 0xa0

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->HU:Lcom/squareup/currency_db/LocaleInfo;

    .line 290
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "HU_HU"

    const/16 v3, 0x11e

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->HU_HU:Lcom/squareup/currency_db/LocaleInfo;

    .line 291
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "HY"

    const/16 v10, 0x11f

    const/16 v12, 0x2e

    const-string v13, "#0.00\u00a0\u00a4"

    const-string v14, "#0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->HY:Lcom/squareup/currency_db/LocaleInfo;

    .line 292
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "HY_AM"

    const/16 v3, 0x120

    const/16 v5, 0x2e

    const-string v6, "#0.00\u00a0\u00a4"

    const-string v7, "#0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->HY_AM:Lcom/squareup/currency_db/LocaleInfo;

    .line 293
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "IA"

    const/16 v10, 0x121

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->IA:Lcom/squareup/currency_db/LocaleInfo;

    .line 294
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "IA_FR"

    const/16 v3, 0x122

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->IA_FR:Lcom/squareup/currency_db/LocaleInfo;

    .line 295
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ID"

    const/16 v10, 0x123

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ID:Lcom/squareup/currency_db/LocaleInfo;

    .line 296
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ID_ID"

    const/16 v3, 0x124

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ID_ID:Lcom/squareup/currency_db/LocaleInfo;

    .line 297
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "IG"

    const/16 v10, 0x125

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->IG:Lcom/squareup/currency_db/LocaleInfo;

    .line 298
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "IG_NG"

    const/16 v3, 0x126

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->IG_NG:Lcom/squareup/currency_db/LocaleInfo;

    .line 299
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "II"

    const/16 v10, 0x127

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->II:Lcom/squareup/currency_db/LocaleInfo;

    .line 300
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "II_CN"

    const/16 v3, 0x128

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->II_CN:Lcom/squareup/currency_db/LocaleInfo;

    .line 301
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "IS"

    const/16 v10, 0x129

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->IS:Lcom/squareup/currency_db/LocaleInfo;

    .line 302
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "IS_IS"

    const/16 v3, 0x12a

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->IS_IS:Lcom/squareup/currency_db/LocaleInfo;

    .line 303
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "IT"

    const/16 v10, 0x12b

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->IT:Lcom/squareup/currency_db/LocaleInfo;

    .line 304
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "IT_CH"

    const/16 v3, 0x12c

    const/16 v4, 0x2e

    const/16 v5, 0x27

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00;\u00a4-#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->IT_CH:Lcom/squareup/currency_db/LocaleInfo;

    .line 305
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "IT_IT"

    const/16 v10, 0x12d

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->IT_IT:Lcom/squareup/currency_db/LocaleInfo;

    .line 306
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "IT_SM"

    const/16 v3, 0x12e

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->IT_SM:Lcom/squareup/currency_db/LocaleInfo;

    .line 307
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "JA"

    const/16 v10, 0x12f

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->JA:Lcom/squareup/currency_db/LocaleInfo;

    .line 308
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "JA_JP"

    const/16 v3, 0x130

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->JA_JP:Lcom/squareup/currency_db/LocaleInfo;

    .line 309
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "KA"

    const/16 v10, 0x131

    const/16 v11, 0x2c

    const/16 v12, 0xa0

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KA:Lcom/squareup/currency_db/LocaleInfo;

    .line 310
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "KA_GE"

    const/16 v3, 0x132

    const/16 v4, 0x2c

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KA_GE:Lcom/squareup/currency_db/LocaleInfo;

    .line 311
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "KI"

    const/16 v10, 0x133

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KI:Lcom/squareup/currency_db/LocaleInfo;

    .line 312
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "KI_KE"

    const/16 v3, 0x134

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KI_KE:Lcom/squareup/currency_db/LocaleInfo;

    .line 313
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "KK"

    const/16 v10, 0x135

    const/16 v11, 0x2c

    const/16 v12, 0xa0

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KK:Lcom/squareup/currency_db/LocaleInfo;

    .line 314
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "KL"

    const/16 v3, 0x136

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string/jumbo v6, "\u00a4#,##0.00;\u00a4-#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KL:Lcom/squareup/currency_db/LocaleInfo;

    .line 315
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "KL_GL"

    const/16 v10, 0x137

    const/16 v12, 0x2e

    const-string/jumbo v13, "\u00a4#,##0.00;\u00a4-#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KL_GL:Lcom/squareup/currency_db/LocaleInfo;

    .line 316
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "KM"

    const/16 v3, 0x138

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KM:Lcom/squareup/currency_db/LocaleInfo;

    .line 317
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "KM_KH"

    const/16 v10, 0x139

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KM_KH:Lcom/squareup/currency_db/LocaleInfo;

    .line 318
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "KN"

    const/16 v3, 0x13a

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KN:Lcom/squareup/currency_db/LocaleInfo;

    .line 319
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "KN_IN"

    const/16 v10, 0x13b

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KN_IN:Lcom/squareup/currency_db/LocaleInfo;

    .line 320
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "KO"

    const/16 v3, 0x13c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KO:Lcom/squareup/currency_db/LocaleInfo;

    .line 321
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "KO_KP"

    const/16 v10, 0x13d

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KO_KP:Lcom/squareup/currency_db/LocaleInfo;

    .line 322
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "KO_KR"

    const/16 v3, 0x13e

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KO_KR:Lcom/squareup/currency_db/LocaleInfo;

    .line 323
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "KS"

    const/16 v10, 0x13f

    const-string/jumbo v13, "\u00a4\u00a0#,##,##0.00"

    const-string v14, "#,##,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KS:Lcom/squareup/currency_db/LocaleInfo;

    .line 324
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "KW"

    const/16 v3, 0x140

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KW:Lcom/squareup/currency_db/LocaleInfo;

    .line 325
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "KW_GB"

    const/16 v10, 0x141

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KW_GB:Lcom/squareup/currency_db/LocaleInfo;

    .line 326
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "KY"

    const/16 v3, 0x142

    const/16 v4, 0x2c

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->KY:Lcom/squareup/currency_db/LocaleInfo;

    .line 327
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "LB"

    const/16 v10, 0x143

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LB:Lcom/squareup/currency_db/LocaleInfo;

    .line 328
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "LB_LU"

    const/16 v3, 0x144

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LB_LU:Lcom/squareup/currency_db/LocaleInfo;

    .line 329
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "LG"

    const/16 v10, 0x145

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string v13, "#,##0.00\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LG:Lcom/squareup/currency_db/LocaleInfo;

    .line 330
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "LG_UG"

    const/16 v3, 0x146

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string v6, "#,##0.00\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LG_UG:Lcom/squareup/currency_db/LocaleInfo;

    .line 331
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "LN"

    const/16 v10, 0x147

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LN:Lcom/squareup/currency_db/LocaleInfo;

    .line 332
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "LN_AO"

    const/16 v3, 0x148

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LN_AO:Lcom/squareup/currency_db/LocaleInfo;

    .line 333
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "LN_CD"

    const/16 v10, 0x149

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LN_CD:Lcom/squareup/currency_db/LocaleInfo;

    .line 334
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "LN_CF"

    const/16 v3, 0x14a

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LN_CF:Lcom/squareup/currency_db/LocaleInfo;

    .line 335
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "LN_CG"

    const/16 v10, 0x14b

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LN_CG:Lcom/squareup/currency_db/LocaleInfo;

    .line 336
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "LO"

    const/16 v3, 0x14c

    const-string/jumbo v6, "\u00a4#,##0.00;\u00a4-#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LO:Lcom/squareup/currency_db/LocaleInfo;

    .line 337
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "LO_LA"

    const/16 v10, 0x14d

    const-string/jumbo v13, "\u00a4#,##0.00;\u00a4-#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LO_LA:Lcom/squareup/currency_db/LocaleInfo;

    .line 338
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "LT"

    const/16 v3, 0x14e

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LT:Lcom/squareup/currency_db/LocaleInfo;

    .line 339
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "LT_LT"

    const/16 v10, 0x14f

    const/16 v12, 0xa0

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LT_LT:Lcom/squareup/currency_db/LocaleInfo;

    .line 340
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "LU"

    const/16 v3, 0x150

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LU:Lcom/squareup/currency_db/LocaleInfo;

    .line 341
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "LU_CD"

    const/16 v10, 0x151

    const/16 v12, 0x2e

    const-string v13, "#,##0.00\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LU_CD:Lcom/squareup/currency_db/LocaleInfo;

    .line 342
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "LV"

    const/16 v3, 0x152

    const/16 v5, 0xa0

    const-string/jumbo v6, "\u00a4#0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LV:Lcom/squareup/currency_db/LocaleInfo;

    .line 343
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "LV_LV"

    const/16 v10, 0x153

    const/16 v12, 0xa0

    const-string/jumbo v13, "\u00a4#0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->LV_LV:Lcom/squareup/currency_db/LocaleInfo;

    .line 344
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "MG"

    const/16 v3, 0x154

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->MG:Lcom/squareup/currency_db/LocaleInfo;

    .line 345
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "MG_MG"

    const/16 v10, 0x155

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->MG_MG:Lcom/squareup/currency_db/LocaleInfo;

    .line 346
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "MK"

    const/16 v3, 0x156

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->MK:Lcom/squareup/currency_db/LocaleInfo;

    .line 347
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "MK_MK"

    const/16 v10, 0x157

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->MK_MK:Lcom/squareup/currency_db/LocaleInfo;

    .line 348
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ML"

    const/16 v3, 0x158

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ML:Lcom/squareup/currency_db/LocaleInfo;

    .line 349
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ML_IN"

    const/16 v10, 0x159

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ML_IN:Lcom/squareup/currency_db/LocaleInfo;

    .line 350
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "MN"

    const/16 v3, 0x15a

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->MN:Lcom/squareup/currency_db/LocaleInfo;

    .line 351
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "MR"

    const/16 v10, 0x15b

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->MR:Lcom/squareup/currency_db/LocaleInfo;

    .line 352
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "MR_IN"

    const/16 v3, 0x15c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->MR_IN:Lcom/squareup/currency_db/LocaleInfo;

    .line 353
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "MS"

    const/16 v10, 0x15d

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->MS:Lcom/squareup/currency_db/LocaleInfo;

    .line 354
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "MT"

    const/16 v3, 0x15e

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->MT:Lcom/squareup/currency_db/LocaleInfo;

    .line 355
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "MT_MT"

    const/16 v10, 0x15f

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->MT_MT:Lcom/squareup/currency_db/LocaleInfo;

    .line 356
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "MY"

    const/16 v3, 0x160

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->MY:Lcom/squareup/currency_db/LocaleInfo;

    .line 357
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "MY_MM"

    const/16 v10, 0x161

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->MY_MM:Lcom/squareup/currency_db/LocaleInfo;

    .line 358
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "NB"

    const/16 v3, 0x162

    const/16 v4, 0x2c

    const/16 v5, 0xa0

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NB:Lcom/squareup/currency_db/LocaleInfo;

    .line 359
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "NB_NO"

    const/16 v10, 0x163

    const/16 v11, 0x2c

    const/16 v12, 0xa0

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NB_NO:Lcom/squareup/currency_db/LocaleInfo;

    .line 360
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "NB_SJ"

    const/16 v3, 0x164

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NB_SJ:Lcom/squareup/currency_db/LocaleInfo;

    .line 361
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ND"

    const/16 v10, 0x165

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ND:Lcom/squareup/currency_db/LocaleInfo;

    .line 362
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ND_ZW"

    const/16 v3, 0x166

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ND_ZW:Lcom/squareup/currency_db/LocaleInfo;

    .line 363
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "NE"

    const/16 v10, 0x167

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NE:Lcom/squareup/currency_db/LocaleInfo;

    .line 364
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "NE_IN"

    const/16 v3, 0x168

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NE_IN:Lcom/squareup/currency_db/LocaleInfo;

    .line 365
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "NE_NP"

    const/16 v10, 0x169

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NE_NP:Lcom/squareup/currency_db/LocaleInfo;

    .line 366
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "NL"

    const/16 v3, 0x16a

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00;\u00a4\u00a0#,##0.00-"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NL:Lcom/squareup/currency_db/LocaleInfo;

    .line 367
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "NL_AW"

    const/16 v10, 0x16b

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00;\u00a4\u00a0#,##0.00-"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NL_AW:Lcom/squareup/currency_db/LocaleInfo;

    .line 368
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "NL_BE"

    const/16 v3, 0x16c

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NL_BE:Lcom/squareup/currency_db/LocaleInfo;

    .line 369
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "NL_BQ"

    const/16 v10, 0x16d

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00;\u00a4\u00a0#,##0.00-"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NL_BQ:Lcom/squareup/currency_db/LocaleInfo;

    .line 370
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "NL_CW"

    const/16 v3, 0x16e

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00;\u00a4\u00a0#,##0.00-"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NL_CW:Lcom/squareup/currency_db/LocaleInfo;

    .line 371
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "NL_NL"

    const/16 v10, 0x16f

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00;\u00a4\u00a0#,##0.00-"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NL_NL:Lcom/squareup/currency_db/LocaleInfo;

    .line 372
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "NL_SR"

    const/16 v3, 0x170

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00;\u00a4\u00a0#,##0.00-"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NL_SR:Lcom/squareup/currency_db/LocaleInfo;

    .line 373
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "NL_SX"

    const/16 v10, 0x171

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00;\u00a4\u00a0#,##0.00-"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NL_SX:Lcom/squareup/currency_db/LocaleInfo;

    .line 374
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "NN"

    const/16 v3, 0x172

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NN:Lcom/squareup/currency_db/LocaleInfo;

    .line 375
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "NN_NO"

    const/16 v10, 0x173

    const/16 v12, 0xa0

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NN_NO:Lcom/squareup/currency_db/LocaleInfo;

    .line 376
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "NR"

    const/16 v3, 0x174

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NR:Lcom/squareup/currency_db/LocaleInfo;

    .line 377
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "NR_ZA"

    const/16 v10, 0x175

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->NR_ZA:Lcom/squareup/currency_db/LocaleInfo;

    .line 378
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "OM"

    const/16 v3, 0x176

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->OM:Lcom/squareup/currency_db/LocaleInfo;

    .line 379
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "OM_ET"

    const/16 v10, 0x177

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->OM_ET:Lcom/squareup/currency_db/LocaleInfo;

    .line 380
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "OM_KE"

    const/16 v3, 0x178

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->OM_KE:Lcom/squareup/currency_db/LocaleInfo;

    .line 381
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "OR"

    const/16 v10, 0x179

    const-string/jumbo v13, "\u00a4\u00a0#,##,##0.00"

    const-string v14, "#,##,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->OR:Lcom/squareup/currency_db/LocaleInfo;

    .line 382
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "OR_IN"

    const/16 v3, 0x17a

    const-string/jumbo v6, "\u00a4\u00a0#,##,##0.00"

    const-string v7, "#,##,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->OR_IN:Lcom/squareup/currency_db/LocaleInfo;

    .line 383
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "OS"

    const/16 v10, 0x17b

    const/16 v11, 0x2c

    const/16 v12, 0xa0

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->OS:Lcom/squareup/currency_db/LocaleInfo;

    .line 384
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "OS_GE"

    const/16 v3, 0x17c

    const/16 v4, 0x2c

    const/16 v5, 0xa0

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->OS_GE:Lcom/squareup/currency_db/LocaleInfo;

    .line 385
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "OS_RU"

    const/16 v10, 0x17d

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->OS_RU:Lcom/squareup/currency_db/LocaleInfo;

    .line 386
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "PA"

    const/16 v3, 0x17e

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4\u00a0#,##,##0.00"

    const-string v7, "#,##,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->PA:Lcom/squareup/currency_db/LocaleInfo;

    .line 387
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "PL"

    const/16 v10, 0x17f

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->PL:Lcom/squareup/currency_db/LocaleInfo;

    .line 388
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "PL_PL"

    const/16 v3, 0x180

    const/16 v4, 0x2c

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->PL_PL:Lcom/squareup/currency_db/LocaleInfo;

    .line 389
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "PS"

    const/16 v10, 0x181

    const/16 v12, 0x2e

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->PS:Lcom/squareup/currency_db/LocaleInfo;

    .line 390
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "PS_AF"

    const/16 v3, 0x182

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->PS_AF:Lcom/squareup/currency_db/LocaleInfo;

    .line 391
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "PT"

    const/16 v10, 0x183

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->PT:Lcom/squareup/currency_db/LocaleInfo;

    .line 392
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "PT_AO"

    const/16 v3, 0x184

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->PT_AO:Lcom/squareup/currency_db/LocaleInfo;

    .line 393
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "PT_BR"

    const/16 v10, 0x185

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->PT_BR:Lcom/squareup/currency_db/LocaleInfo;

    .line 394
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "PT_CV"

    const/16 v3, 0x186

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->PT_CV:Lcom/squareup/currency_db/LocaleInfo;

    .line 395
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "PT_GW"

    const/16 v10, 0x187

    const/16 v12, 0xa0

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->PT_GW:Lcom/squareup/currency_db/LocaleInfo;

    .line 396
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "PT_MO"

    const/16 v3, 0x188

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->PT_MO:Lcom/squareup/currency_db/LocaleInfo;

    .line 397
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "PT_MZ"

    const/16 v10, 0x189

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->PT_MZ:Lcom/squareup/currency_db/LocaleInfo;

    .line 398
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "PT_PT"

    const/16 v3, 0x18a

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->PT_PT:Lcom/squareup/currency_db/LocaleInfo;

    .line 399
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "PT_ST"

    const/16 v10, 0x18b

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->PT_ST:Lcom/squareup/currency_db/LocaleInfo;

    .line 400
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "PT_TL"

    const/16 v3, 0x18c

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->PT_TL:Lcom/squareup/currency_db/LocaleInfo;

    .line 401
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "QU"

    const/16 v10, 0x18d

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->QU:Lcom/squareup/currency_db/LocaleInfo;

    .line 402
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "QU_BO"

    const/16 v3, 0x18e

    const/16 v5, 0x2e

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->QU_BO:Lcom/squareup/currency_db/LocaleInfo;

    .line 403
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "QU_EC"

    const/16 v10, 0x18f

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->QU_EC:Lcom/squareup/currency_db/LocaleInfo;

    .line 404
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "QU_PE"

    const/16 v3, 0x190

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->QU_PE:Lcom/squareup/currency_db/LocaleInfo;

    .line 405
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "RM"

    const/16 v10, 0x191

    const/16 v12, 0x2019

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->RM:Lcom/squareup/currency_db/LocaleInfo;

    .line 406
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "RM_CH"

    const/16 v3, 0x192

    const/16 v5, 0x2019

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->RM_CH:Lcom/squareup/currency_db/LocaleInfo;

    .line 407
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "RN"

    const/16 v10, 0x193

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string v13, "#,##0.00\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->RN:Lcom/squareup/currency_db/LocaleInfo;

    .line 408
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "RN_BI"

    const/16 v3, 0x194

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->RN_BI:Lcom/squareup/currency_db/LocaleInfo;

    .line 409
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "RO"

    const/16 v10, 0x195

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->RO:Lcom/squareup/currency_db/LocaleInfo;

    .line 410
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "RO_MD"

    const/16 v3, 0x196

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->RO_MD:Lcom/squareup/currency_db/LocaleInfo;

    .line 411
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "RO_RO"

    const/16 v10, 0x197

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->RO_RO:Lcom/squareup/currency_db/LocaleInfo;

    .line 412
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "RU"

    const/16 v3, 0x198

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->RU:Lcom/squareup/currency_db/LocaleInfo;

    .line 413
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "RU_BY"

    const/16 v10, 0x199

    const/16 v12, 0xa0

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->RU_BY:Lcom/squareup/currency_db/LocaleInfo;

    .line 414
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "RU_KG"

    const/16 v3, 0x19a

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->RU_KG:Lcom/squareup/currency_db/LocaleInfo;

    .line 415
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "RU_KZ"

    const/16 v10, 0x19b

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->RU_KZ:Lcom/squareup/currency_db/LocaleInfo;

    .line 416
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "RU_MD"

    const/16 v3, 0x19c

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->RU_MD:Lcom/squareup/currency_db/LocaleInfo;

    .line 417
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "RU_RU"

    const/16 v10, 0x19d

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->RU_RU:Lcom/squareup/currency_db/LocaleInfo;

    .line 418
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "RU_UA"

    const/16 v3, 0x19e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->RU_UA:Lcom/squareup/currency_db/LocaleInfo;

    .line 419
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "RW"

    const/16 v10, 0x19f

    const/16 v12, 0x2e

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->RW:Lcom/squareup/currency_db/LocaleInfo;

    .line 420
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "RW_RW"

    const/16 v3, 0x1a0

    const/16 v5, 0x2e

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->RW_RW:Lcom/squareup/currency_db/LocaleInfo;

    .line 421
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SE"

    const/16 v10, 0x1a1

    const/16 v12, 0xa0

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SE:Lcom/squareup/currency_db/LocaleInfo;

    .line 422
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SE_FI"

    const/16 v3, 0x1a2

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SE_FI:Lcom/squareup/currency_db/LocaleInfo;

    .line 423
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SE_NO"

    const/16 v10, 0x1a3

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SE_NO:Lcom/squareup/currency_db/LocaleInfo;

    .line 424
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SE_SE"

    const/16 v3, 0x1a4

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SE_SE:Lcom/squareup/currency_db/LocaleInfo;

    .line 425
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SG"

    const/16 v10, 0x1a5

    const/16 v12, 0x2e

    const-string/jumbo v13, "\u00a4#,##0.00;\u00a4-#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SG:Lcom/squareup/currency_db/LocaleInfo;

    .line 426
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SG_CF"

    const/16 v3, 0x1a6

    const/16 v5, 0x2e

    const-string/jumbo v6, "\u00a4#,##0.00;\u00a4-#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SG_CF:Lcom/squareup/currency_db/LocaleInfo;

    .line 427
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SI"

    const/16 v10, 0x1a7

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SI:Lcom/squareup/currency_db/LocaleInfo;

    .line 428
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SI_LK"

    const/16 v3, 0x1a8

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SI_LK:Lcom/squareup/currency_db/LocaleInfo;

    .line 429
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SK"

    const/16 v10, 0x1a9

    const/16 v11, 0x2c

    const/16 v12, 0xa0

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SK:Lcom/squareup/currency_db/LocaleInfo;

    .line 430
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SK_SK"

    const/16 v3, 0x1aa

    const/16 v4, 0x2c

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SK_SK:Lcom/squareup/currency_db/LocaleInfo;

    .line 431
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SL"

    const/16 v10, 0x1ab

    const/16 v12, 0x2e

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SL:Lcom/squareup/currency_db/LocaleInfo;

    .line 432
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SL_SI"

    const/16 v3, 0x1ac

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SL_SI:Lcom/squareup/currency_db/LocaleInfo;

    .line 433
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SN"

    const/16 v10, 0x1ad

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SN:Lcom/squareup/currency_db/LocaleInfo;

    .line 434
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SN_ZW"

    const/16 v3, 0x1ae

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SN_ZW:Lcom/squareup/currency_db/LocaleInfo;

    .line 435
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SO"

    const/16 v10, 0x1af

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SO:Lcom/squareup/currency_db/LocaleInfo;

    .line 436
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SO_DJ"

    const/16 v3, 0x1b0

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SO_DJ:Lcom/squareup/currency_db/LocaleInfo;

    .line 437
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SO_ET"

    const/16 v10, 0x1b1

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SO_ET:Lcom/squareup/currency_db/LocaleInfo;

    .line 438
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SO_KE"

    const/16 v3, 0x1b2

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SO_KE:Lcom/squareup/currency_db/LocaleInfo;

    .line 439
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SO_SO"

    const/16 v10, 0x1b3

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SO_SO:Lcom/squareup/currency_db/LocaleInfo;

    .line 440
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SQ"

    const/16 v3, 0x1b4

    const/16 v4, 0x2c

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SQ:Lcom/squareup/currency_db/LocaleInfo;

    .line 441
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SQ_AL"

    const/16 v10, 0x1b5

    const/16 v11, 0x2c

    const/16 v12, 0xa0

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SQ_AL:Lcom/squareup/currency_db/LocaleInfo;

    .line 442
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SQ_MK"

    const/16 v3, 0x1b6

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SQ_MK:Lcom/squareup/currency_db/LocaleInfo;

    .line 443
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SQ_XK"

    const/16 v10, 0x1b7

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SQ_XK:Lcom/squareup/currency_db/LocaleInfo;

    .line 444
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SR"

    const/16 v3, 0x1b8

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SR:Lcom/squareup/currency_db/LocaleInfo;

    .line 445
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SS"

    const/16 v10, 0x1b9

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SS:Lcom/squareup/currency_db/LocaleInfo;

    .line 446
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SS_SZ"

    const/16 v3, 0x1ba

    const/16 v5, 0xa0

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SS_SZ:Lcom/squareup/currency_db/LocaleInfo;

    .line 447
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SS_ZA"

    const/16 v10, 0x1bb

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SS_ZA:Lcom/squareup/currency_db/LocaleInfo;

    .line 448
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SV"

    const/16 v3, 0x1bc

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SV:Lcom/squareup/currency_db/LocaleInfo;

    .line 449
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SV_AX"

    const/16 v10, 0x1bd

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SV_AX:Lcom/squareup/currency_db/LocaleInfo;

    .line 450
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SV_FI"

    const/16 v3, 0x1be

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SV_FI:Lcom/squareup/currency_db/LocaleInfo;

    .line 451
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SV_SE"

    const/16 v10, 0x1bf

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SV_SE:Lcom/squareup/currency_db/LocaleInfo;

    .line 452
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SW"

    const/16 v3, 0x1c0

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SW:Lcom/squareup/currency_db/LocaleInfo;

    .line 453
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SW_KE"

    const/16 v10, 0x1c1

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SW_KE:Lcom/squareup/currency_db/LocaleInfo;

    .line 454
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "SW_TZ"

    const/16 v3, 0x1c2

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SW_TZ:Lcom/squareup/currency_db/LocaleInfo;

    .line 455
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "SW_UG"

    const/16 v10, 0x1c3

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->SW_UG:Lcom/squareup/currency_db/LocaleInfo;

    .line 456
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "TA"

    const/16 v3, 0x1c4

    const-string/jumbo v6, "\u00a4\u00a0#,##,##0.00"

    const-string v7, "#,##,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TA:Lcom/squareup/currency_db/LocaleInfo;

    .line 457
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "TA_IN"

    const/16 v10, 0x1c5

    const-string/jumbo v13, "\u00a4\u00a0#,##,##0.00"

    const-string v14, "#,##,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TA_IN:Lcom/squareup/currency_db/LocaleInfo;

    .line 458
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "TA_LK"

    const/16 v3, 0x1c6

    const-string/jumbo v6, "\u00a4\u00a0#,##,##0.00"

    const-string v7, "#,##,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TA_LK:Lcom/squareup/currency_db/LocaleInfo;

    .line 459
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "TA_MY"

    const/16 v10, 0x1c7

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TA_MY:Lcom/squareup/currency_db/LocaleInfo;

    .line 460
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "TA_SG"

    const/16 v3, 0x1c8

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TA_SG:Lcom/squareup/currency_db/LocaleInfo;

    .line 461
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "TE"

    const/16 v10, 0x1c9

    const-string/jumbo v13, "\u00a4#,##,##0.00"

    const-string v14, "#,##,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TE:Lcom/squareup/currency_db/LocaleInfo;

    .line 462
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "TE_IN"

    const/16 v3, 0x1ca

    const-string/jumbo v6, "\u00a4#,##,##0.00"

    const-string v7, "#,##,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TE_IN:Lcom/squareup/currency_db/LocaleInfo;

    .line 463
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "TH"

    const/16 v10, 0x1cb

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TH:Lcom/squareup/currency_db/LocaleInfo;

    .line 464
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "TH_TH"

    const/16 v3, 0x1cc

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TH_TH:Lcom/squareup/currency_db/LocaleInfo;

    .line 465
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "TI"

    const/16 v10, 0x1cd

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TI:Lcom/squareup/currency_db/LocaleInfo;

    .line 466
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "TI_ER"

    const/16 v3, 0x1ce

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TI_ER:Lcom/squareup/currency_db/LocaleInfo;

    .line 467
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "TI_ET"

    const/16 v10, 0x1cf

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TI_ET:Lcom/squareup/currency_db/LocaleInfo;

    .line 468
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "TN"

    const/16 v3, 0x1d0

    const/16 v5, 0xa0

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TN:Lcom/squareup/currency_db/LocaleInfo;

    .line 469
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "TN_BW"

    const/16 v10, 0x1d1

    const/16 v12, 0xa0

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TN_BW:Lcom/squareup/currency_db/LocaleInfo;

    .line 470
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "TN_ZA"

    const/16 v3, 0x1d2

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TN_ZA:Lcom/squareup/currency_db/LocaleInfo;

    .line 471
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "TO"

    const/16 v10, 0x1d3

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TO:Lcom/squareup/currency_db/LocaleInfo;

    .line 472
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "TO_TO"

    const/16 v3, 0x1d4

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TO_TO:Lcom/squareup/currency_db/LocaleInfo;

    .line 473
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "TR"

    const/16 v10, 0x1d5

    const/16 v11, 0x2c

    const/16 v12, 0x2e

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TR:Lcom/squareup/currency_db/LocaleInfo;

    .line 474
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "TR_CY"

    const/16 v3, 0x1d6

    const/16 v4, 0x2c

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TR_CY:Lcom/squareup/currency_db/LocaleInfo;

    .line 475
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "TR_TR"

    const/16 v10, 0x1d7

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TR_TR:Lcom/squareup/currency_db/LocaleInfo;

    .line 476
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "TS"

    const/16 v3, 0x1d8

    const/16 v5, 0xa0

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TS:Lcom/squareup/currency_db/LocaleInfo;

    .line 477
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "TS_ZA"

    const/16 v10, 0x1d9

    const/16 v12, 0xa0

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->TS_ZA:Lcom/squareup/currency_db/LocaleInfo;

    .line 478
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "UG"

    const/16 v3, 0x1da

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->UG:Lcom/squareup/currency_db/LocaleInfo;

    .line 479
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "UK"

    const/16 v10, 0x1db

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->UK:Lcom/squareup/currency_db/LocaleInfo;

    .line 480
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "UK_UA"

    const/16 v3, 0x1dc

    const/16 v4, 0x2c

    const/16 v5, 0xa0

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->UK_UA:Lcom/squareup/currency_db/LocaleInfo;

    .line 481
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "UR"

    const/16 v10, 0x1dd

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4\u00a0#,##,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->UR:Lcom/squareup/currency_db/LocaleInfo;

    .line 482
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "UR_IN"

    const/16 v3, 0x1de

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4\u00a0#,##,##0.00"

    const-string v7, "#,##,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->UR_IN:Lcom/squareup/currency_db/LocaleInfo;

    .line 483
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "UR_PK"

    const/16 v10, 0x1df

    const-string/jumbo v13, "\u00a4\u00a0#,##,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->UR_PK:Lcom/squareup/currency_db/LocaleInfo;

    .line 484
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "UZ"

    const/16 v3, 0x1e0

    const/16 v4, 0x2c

    const/16 v5, 0xa0

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->UZ:Lcom/squareup/currency_db/LocaleInfo;

    .line 485
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "VE"

    const/16 v10, 0x1e1

    const/16 v11, 0x2c

    const/16 v12, 0xa0

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->VE:Lcom/squareup/currency_db/LocaleInfo;

    .line 486
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "VE_ZA"

    const/16 v3, 0x1e2

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->VE_ZA:Lcom/squareup/currency_db/LocaleInfo;

    .line 487
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "VI"

    const/16 v10, 0x1e3

    const/16 v12, 0x2e

    const-string v13, "#,##0.00\u00a0\u00a4"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->VI:Lcom/squareup/currency_db/LocaleInfo;

    .line 488
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "VI_VN"

    const/16 v3, 0x1e4

    const/16 v5, 0x2e

    const-string v6, "#,##0.00\u00a0\u00a4"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->VI_VN:Lcom/squareup/currency_db/LocaleInfo;

    .line 489
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "VO"

    const/16 v10, 0x1e5

    const/16 v11, 0x2e

    const/16 v12, 0x2c

    const-string/jumbo v13, "\u00a4\u00a0#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->VO:Lcom/squareup/currency_db/LocaleInfo;

    .line 490
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "YI"

    const/16 v3, 0x1e6

    const/16 v4, 0x2e

    const/16 v5, 0x2c

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->YI:Lcom/squareup/currency_db/LocaleInfo;

    .line 491
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "YO"

    const/16 v10, 0x1e7

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->YO:Lcom/squareup/currency_db/LocaleInfo;

    .line 492
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "YO_BJ"

    const/16 v3, 0x1e8

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->YO_BJ:Lcom/squareup/currency_db/LocaleInfo;

    .line 493
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "YO_NG"

    const/16 v10, 0x1e9

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->YO_NG:Lcom/squareup/currency_db/LocaleInfo;

    .line 494
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ZH"

    const/16 v3, 0x1ea

    const-string/jumbo v6, "\u00a4\u00a0#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ZH:Lcom/squareup/currency_db/LocaleInfo;

    .line 495
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v9, "ZU"

    const/16 v10, 0x1eb

    const-string/jumbo v13, "\u00a4#,##0.00"

    const-string v14, "#,##0.###"

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ZU:Lcom/squareup/currency_db/LocaleInfo;

    .line 496
    new-instance v0, Lcom/squareup/currency_db/LocaleInfo;

    const-string v2, "ZU_ZA"

    const/16 v3, 0x1ec

    const-string/jumbo v6, "\u00a4#,##0.00"

    const-string v7, "#,##0.###"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/currency_db/LocaleInfo;-><init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->ZU_ZA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v0, 0x1ed

    new-array v0, v0, [Lcom/squareup/currency_db/LocaleInfo;

    .line 3
    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AA:Lcom/squareup/currency_db/LocaleInfo;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AA_DJ:Lcom/squareup/currency_db/LocaleInfo;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AA_ER:Lcom/squareup/currency_db/LocaleInfo;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AA_ET:Lcom/squareup/currency_db/LocaleInfo;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AF:Lcom/squareup/currency_db/LocaleInfo;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AF_NA:Lcom/squareup/currency_db/LocaleInfo;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AF_ZA:Lcom/squareup/currency_db/LocaleInfo;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AK:Lcom/squareup/currency_db/LocaleInfo;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AK_GH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AM_ET:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_AE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_BH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_DJ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_DZ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_EG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_EH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_ER:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_IL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_IQ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_JO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_KM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_KW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_LB:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_LY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_MA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_MR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_OM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_PS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_QA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_SA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_SD:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_SO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_SS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_SY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_TD:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_TN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AR_YE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AS_IN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->AZ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->BE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->BE_BY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->BG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->BG_BG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->BM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->BN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->BN_BD:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->BN_IN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->BO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->BO_CN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->BO_IN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->BR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->BR_FR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->BS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->CA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->CA_AD:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->CA_ES:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->CA_FR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->CA_IT:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->CS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->CS_CZ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->CY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->CY_GB:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->DA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->DA_DK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->DA_GL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->DE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->DE_AT:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->DE_BE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->DE_CH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->DE_DE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->DE_LI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->DE_LU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->DZ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->DZ_BT:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EE_GH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EE_TG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EL_CY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EL_GR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_AG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_AI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_AS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_AU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_BB:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_BE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_BM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_BS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_BW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_BZ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_CA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_CC:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_CK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_CM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_CX:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_DG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_DM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_ER:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_FJ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_FK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_FM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_GB:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_GD:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_GG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_GH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_GI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_GM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_GU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_GY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_HK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_IE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_IM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_IN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_IO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_JE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_JM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_KE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_KI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_KN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_KY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_LC:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_LR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_LS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_MG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_MH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_MO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_MP:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_MS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_MT:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_MU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_MW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_MY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_NA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_NF:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_NG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_NR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_NU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_NZ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_PG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_PH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_PK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_PN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_PR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_PW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_RW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_SB:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_SC:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_SD:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_SG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_SH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_SL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_SS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_SX:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_SZ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_TC:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_TK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_TO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_TT:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_TV:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_TZ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_UG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_UM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_US:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_VC:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_VG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_VI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_VU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_WS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_ZA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_ZM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EN_ZW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_AR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_BO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xb2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_CL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xb3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_CO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xb4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_CR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xb5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_CU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xb6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_DO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xb7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_EA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xb8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_EC:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xb9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_ES:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xba

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_GQ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xbb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_GT:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xbc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_HN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xbd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_IC:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xbe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_MX:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xbf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_NI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xc0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_PA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xc1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_PE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xc2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_PH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xc3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_PR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xc4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_PY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xc5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_SV:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xc6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_US:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xc7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_UY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xc8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ES_VE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xc9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ET:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xca

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ET_EE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xcb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xcc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->EU_ES:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xcd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xce

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FA_AF:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xcf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FA_IR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xd0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FF:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xd1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FF_CM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xd2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FF_GN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xd3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FF_MR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xd4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FF_SN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xd5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xd6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FI_FI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xd7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xd8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FO_FO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xd9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xda

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_BE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xdb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_BF:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xdc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_BI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xdd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_BJ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xde

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_BL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xdf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_CA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xe0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_CD:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xe1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_CF:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xe2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_CG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xe3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_CH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xe4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_CI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xe5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_CM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xe6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_DJ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xe7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_DZ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xe8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_FR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xe9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_GA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xea

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_GF:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xeb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_GN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xec

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_GP:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xed

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_GQ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xee

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_HT:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xef

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_KM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xf0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_LU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xf1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_MA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xf2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_MC:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xf3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_MF:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xf4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_MG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xf5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_ML:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xf6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_MQ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xf7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_MR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xf8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_MU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xf9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_NC:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xfa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_NE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xfb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_PF:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xfc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_PM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xfd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_RE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xfe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_RW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0xff

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_SC:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x100

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_SN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x101

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_SY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x102

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_TD:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x103

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_TG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x104

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_TN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x105

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_VU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x106

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_WF:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x107

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FR_YT:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x108

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x109

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->FY_NL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x10a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->GA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x10b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->GA_IE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x10c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->GD:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x10d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->GD_GB:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x10e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->GL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x10f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->GL_ES:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x110

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->GU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x111

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->GU_IN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x112

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->GV:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x113

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->GV_IM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x114

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->HA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x115

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->HE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x116

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->HE_IL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x117

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->HI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x118

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->HI_IN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x119

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->HR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x11a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->HR_BA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x11b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->HR_HR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x11c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->HU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x11d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->HU_HU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x11e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->HY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x11f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->HY_AM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x120

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->IA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x121

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->IA_FR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x122

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ID:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x123

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ID_ID:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x124

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->IG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x125

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->IG_NG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x126

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->II:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x127

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->II_CN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x128

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->IS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x129

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->IS_IS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x12a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->IT:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x12b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->IT_CH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x12c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->IT_IT:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x12d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->IT_SM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x12e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->JA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x12f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->JA_JP:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x130

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x131

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KA_GE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x132

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x133

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KI_KE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x134

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x135

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x136

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KL_GL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x137

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x138

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KM_KH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x139

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x13a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KN_IN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x13b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x13c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KO_KP:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x13d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KO_KR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x13e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x13f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x140

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KW_GB:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x141

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->KY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x142

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LB:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x143

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LB_LU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x144

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x145

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LG_UG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x146

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x147

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LN_AO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x148

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LN_CD:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x149

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LN_CF:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x14a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LN_CG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x14b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x14c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LO_LA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x14d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LT:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x14e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LT_LT:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x14f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x150

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LU_CD:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x151

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LV:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x152

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->LV_LV:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x153

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->MG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x154

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->MG_MG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x155

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->MK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x156

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->MK_MK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x157

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ML:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x158

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ML_IN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x159

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->MN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x15a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->MR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x15b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->MR_IN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x15c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->MS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x15d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->MT:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x15e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->MT_MT:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x15f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->MY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x160

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->MY_MM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x161

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NB:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x162

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NB_NO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x163

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NB_SJ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x164

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ND:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x165

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ND_ZW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x166

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x167

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NE_IN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x168

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NE_NP:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x169

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x16a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NL_AW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x16b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NL_BE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x16c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NL_BQ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x16d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NL_CW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x16e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NL_NL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x16f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NL_SR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x170

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NL_SX:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x171

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x172

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NN_NO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x173

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x174

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->NR_ZA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x175

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->OM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x176

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->OM_ET:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x177

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->OM_KE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x178

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->OR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x179

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->OR_IN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x17a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->OS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x17b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->OS_GE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x17c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->OS_RU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x17d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->PA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x17e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->PL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x17f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->PL_PL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x180

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->PS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x181

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->PS_AF:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x182

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->PT:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x183

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->PT_AO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x184

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->PT_BR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x185

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->PT_CV:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x186

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->PT_GW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x187

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->PT_MO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x188

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->PT_MZ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x189

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->PT_PT:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x18a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->PT_ST:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x18b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->PT_TL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x18c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->QU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x18d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->QU_BO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x18e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->QU_EC:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x18f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->QU_PE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x190

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->RM:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x191

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->RM_CH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x192

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->RN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x193

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->RN_BI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x194

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->RO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x195

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->RO_MD:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x196

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->RO_RO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x197

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->RU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x198

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->RU_BY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x199

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->RU_KG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x19a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->RU_KZ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x19b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->RU_MD:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x19c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->RU_RU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x19d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->RU_UA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x19e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->RW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x19f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->RW_RW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1a0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1a1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SE_FI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1a2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SE_NO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1a3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SE_SE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1a4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1a5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SG_CF:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1a6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1a7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SI_LK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1a8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1a9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SK_SK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1aa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1ab

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SL_SI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1ac

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1ad

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SN_ZW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1ae

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1af

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SO_DJ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1b0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SO_ET:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1b1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SO_KE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1b2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SO_SO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1b3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SQ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1b4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SQ_AL:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1b5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SQ_MK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1b6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SQ_XK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1b7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1b8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1b9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SS_SZ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1ba

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SS_ZA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1bb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SV:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1bc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SV_AX:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1bd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SV_FI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1be

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SV_SE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1bf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1c0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SW_KE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1c1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SW_TZ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1c2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->SW_UG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1c3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1c4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TA_IN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1c5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TA_LK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1c6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TA_MY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1c7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TA_SG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1c8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1c9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TE_IN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1ca

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1cb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TH_TH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1cc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1cd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TI_ER:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1ce

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TI_ET:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1cf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1d0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TN_BW:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1d1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TN_ZA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1d2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1d3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TO_TO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1d4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1d5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TR_CY:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1d6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TR_TR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1d7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TS:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1d8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->TS_ZA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1d9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->UG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1da

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->UK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1db

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->UK_UA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1dc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->UR:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1dd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->UR_IN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1de

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->UR_PK:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1df

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->UZ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1e0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->VE:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1e1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->VE_ZA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1e2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->VI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1e3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->VI_VN:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1e4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->VO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1e5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->YI:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1e6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->YO:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1e7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->YO_BJ:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1e8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->YO_NG:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1e9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ZH:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1ea

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ZU:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1eb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/LocaleInfo;->ZU_ZA:Lcom/squareup/currency_db/LocaleInfo;

    const/16 v2, 0x1ec

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/currency_db/LocaleInfo;->$VALUES:[Lcom/squareup/currency_db/LocaleInfo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ICCLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(CC",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 503
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 504
    iput-char p3, p0, Lcom/squareup/currency_db/LocaleInfo;->decimalSeparator:C

    .line 505
    iput-char p4, p0, Lcom/squareup/currency_db/LocaleInfo;->groupingSeparator:C

    .line 506
    iput-object p5, p0, Lcom/squareup/currency_db/LocaleInfo;->currencyPattern:Ljava/lang/String;

    .line 507
    iput-object p6, p0, Lcom/squareup/currency_db/LocaleInfo;->numberPattern:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/currency_db/LocaleInfo;
    .locals 1

    .line 3
    const-class v0, Lcom/squareup/currency_db/LocaleInfo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/currency_db/LocaleInfo;

    return-object p0
.end method

.method public static values()[Lcom/squareup/currency_db/LocaleInfo;
    .locals 1

    .line 3
    sget-object v0, Lcom/squareup/currency_db/LocaleInfo;->$VALUES:[Lcom/squareup/currency_db/LocaleInfo;

    invoke-virtual {v0}, [Lcom/squareup/currency_db/LocaleInfo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/currency_db/LocaleInfo;

    return-object v0
.end method
