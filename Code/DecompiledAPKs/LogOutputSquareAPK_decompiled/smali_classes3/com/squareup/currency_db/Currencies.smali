.class public Lcom/squareup/currency_db/Currencies;
.super Ljava/lang/Object;
.source "Currencies.java"


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Told you not to instantiate it!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static getCurrencyInfo(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/currency_db/CurrencyInfo;
    .locals 0

    .line 33
    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/currency_db/CurrencyInfo;->valueOf(Ljava/lang/String;)Lcom/squareup/currency_db/CurrencyInfo;

    move-result-object p0

    return-object p0
.end method

.method public static getCurrencySubunitSymbol(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;
    .locals 0

    .line 13
    invoke-static {p0}, Lcom/squareup/currency_db/Currencies;->getCurrencyInfo(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/currency_db/CurrencyInfo;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/currency_db/CurrencyInfo;->subunit_symbol:Ljava/lang/String;

    return-object p0
.end method

.method public static getCurrencySymbol(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/currency_db/Currencies;->getCurrencyInfo(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/currency_db/CurrencyInfo;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/currency_db/CurrencyInfo;->symbol:Ljava/lang/String;

    return-object p0
.end method

.method public static getDenominations(Lcom/squareup/protos/common/CurrencyCode;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 21
    invoke-static {p0}, Lcom/squareup/currency_db/Currencies;->getCurrencyInfo(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/currency_db/CurrencyInfo;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/currency_db/CurrencyInfo;->denominations:Ljava/util/List;

    return-object p0
.end method

.method public static getFractionDigits(Lcom/squareup/protos/common/CurrencyCode;)I
    .locals 0

    .line 17
    invoke-static {p0}, Lcom/squareup/currency_db/Currencies;->getCurrencyInfo(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/currency_db/CurrencyInfo;

    move-result-object p0

    iget p0, p0, Lcom/squareup/currency_db/CurrencyInfo;->fractionalDigits:I

    return p0
.end method

.method public static getSubunitsPerUnit(Lcom/squareup/protos/common/CurrencyCode;)I
    .locals 0

    .line 29
    invoke-static {p0}, Lcom/squareup/currency_db/Currencies;->getCurrencyInfo(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/currency_db/CurrencyInfo;

    move-result-object p0

    iget p0, p0, Lcom/squareup/currency_db/CurrencyInfo;->subunitsPerUnit:I

    return p0
.end method

.method public static getSwedishRoundingInterval(Lcom/squareup/protos/common/CurrencyCode;)I
    .locals 0

    .line 25
    invoke-static {p0}, Lcom/squareup/currency_db/Currencies;->getCurrencyInfo(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/currency_db/CurrencyInfo;

    move-result-object p0

    iget p0, p0, Lcom/squareup/currency_db/CurrencyInfo;->swedishRoundingInterval:I

    return p0
.end method
