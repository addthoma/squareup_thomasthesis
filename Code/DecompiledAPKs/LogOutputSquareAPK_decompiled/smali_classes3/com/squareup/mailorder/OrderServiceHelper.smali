.class public final Lcom/squareup/mailorder/OrderServiceHelper;
.super Ljava/lang/Object;
.source "OrderServiceHelper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;,
        Lcom/squareup/mailorder/OrderServiceHelper$OrderResult;,
        Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;,
        Lcom/squareup/mailorder/OrderServiceHelper$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderServiceHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderServiceHelper.kt\ncom/squareup/mailorder/OrderServiceHelper\n*L\n1#1,276:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008e\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 .2\u00020\u0001:\u0004./01B/\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0013\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u0000\u00a2\u0006\u0002\u0008\u0010J5\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000e2\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0000\u00a2\u0006\u0002\u0008\u001aJ\u0008\u0010\u001b\u001a\u00020\u001cH\u0002J\u001b\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u000e2\u0006\u0010\u0016\u001a\u00020\u0017H\u0000\u00a2\u0006\u0002\u0008\u001fJ\u0014\u0010 \u001a\u00020!*\u00020\u00172\u0006\u0010\u0002\u001a\u00020\u0003H\u0002J\u001a\u0010\"\u001a\u00020#*\u00020\t2\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020&0%H\u0002J\u0012\u0010\"\u001a\u00020#*\u0008\u0012\u0004\u0012\u00020&0%H\u0002J\u000c\u0010\"\u001a\u00020#*\u00020\'H\u0002J\u001a\u0010(\u001a\u00020)*\u00020\t2\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020*0%H\u0002J\u000c\u0010(\u001a\u00020)*\u00020\'H\u0002J\u0012\u0010+\u001a\u00020\u001e*\u0008\u0012\u0004\u0012\u00020*0%H\u0002J\u001a\u0010,\u001a\u00020\u001e*\u0008\u0012\u0004\u0012\u00020*0-2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00062"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderServiceHelper;",
        "",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "profileUpdater",
        "Lcom/squareup/merchantprofile/MerchantProfileUpdater;",
        "resources",
        "Landroid/content/res/Resources;",
        "messageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "endpoints",
        "Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/merchantprofile/MerchantProfileUpdater;Landroid/content/res/Resources;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;)V",
        "getMerchantProfile",
        "Lio/reactivex/Single;",
        "Lcom/squareup/server/account/MerchantProfileResponse;",
        "getMerchantProfile$mail_order_release",
        "sendShippingAddress",
        "Lcom/squareup/mailorder/OrderServiceHelper$OrderResult;",
        "itemToken",
        "",
        "verifiedAddressToken",
        "contactInfo",
        "Lcom/squareup/mailorder/ContactInfo;",
        "globalAddress",
        "Lcom/squareup/protos/common/location/GlobalAddress;",
        "sendShippingAddress$mail_order_release",
        "verificationServiceError",
        "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServiceError;",
        "verifyShippingAddress",
        "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;",
        "verifyShippingAddress$mail_order_release",
        "createVerifyRequest",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;",
        "toFailure",
        "Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;",
        "showFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "Lcom/squareup/server/shipping/UpdateAddressResponse;",
        "Lcom/squareup/widgets/warning/Warning;",
        "toServerError",
        "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServerError;",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;",
        "toVerificationFailure",
        "toVerificationResult",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;",
        "Companion",
        "Endpoints",
        "OrderResult",
        "VerificationResult",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/mailorder/OrderServiceHelper$Companion;

.field public static final NO_VERIFIED_ADDRESS_TOKEN:Ljava/lang/String; = ""


# instance fields
.field private final endpoints:Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;

.field private final messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final profileUpdater:Lcom/squareup/merchantprofile/MerchantProfileUpdater;

.field private final resources:Landroid/content/res/Resources;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/mailorder/OrderServiceHelper$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/mailorder/OrderServiceHelper$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/mailorder/OrderServiceHelper;->Companion:Lcom/squareup/mailorder/OrderServiceHelper$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/merchantprofile/MerchantProfileUpdater;Landroid/content/res/Resources;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "profileUpdater"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messageFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "endpoints"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/mailorder/OrderServiceHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p2, p0, Lcom/squareup/mailorder/OrderServiceHelper;->profileUpdater:Lcom/squareup/merchantprofile/MerchantProfileUpdater;

    iput-object p3, p0, Lcom/squareup/mailorder/OrderServiceHelper;->resources:Landroid/content/res/Resources;

    iput-object p4, p0, Lcom/squareup/mailorder/OrderServiceHelper;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object p5, p0, Lcom/squareup/mailorder/OrderServiceHelper;->endpoints:Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;

    return-void
.end method

.method public static final synthetic access$toFailure(Lcom/squareup/mailorder/OrderServiceHelper;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/mailorder/OrderServiceHelper;->toFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toVerificationFailure(Lcom/squareup/mailorder/OrderServiceHelper;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/mailorder/OrderServiceHelper;->toVerificationFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toVerificationResult(Lcom/squareup/mailorder/OrderServiceHelper;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;Lcom/squareup/mailorder/ContactInfo;)Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Lcom/squareup/mailorder/OrderServiceHelper;->toVerificationResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;Lcom/squareup/mailorder/ContactInfo;)Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;

    move-result-object p0

    return-object p0
.end method

.method private final createVerifyRequest(Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;
    .locals 3

    .line 267
    invoke-virtual {p1}, Lcom/squareup/mailorder/ContactInfo;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 268
    new-instance v1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;-><init>()V

    .line 269
    invoke-static {v0}, Lcom/squareup/mailorder/ShippingNames;->getFirstName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->first_name(Ljava/lang/String;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    move-result-object v1

    .line 270
    invoke-static {v0}, Lcom/squareup/mailorder/ShippingNames;->getLastName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->last_name(Ljava/lang/String;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    move-result-object v0

    .line 271
    invoke-virtual {p1}, Lcom/squareup/mailorder/ContactInfo;->getAddress()Lcom/squareup/address/Address;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p2

    const-string v2, "settings.userSettings"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object p2

    const-string v2, "settings.userSettings.countryCode"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Lcom/squareup/address/Address;->toGlobalAddress(Lcom/squareup/CountryCode;)Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->shipping_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    move-result-object p2

    .line 272
    new-instance v0, Lcom/squareup/protos/common/location/Phone$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/location/Phone$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/mailorder/ContactInfo;->getPhone()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/common/location/Phone$Builder;->number(Ljava/lang/String;)Lcom/squareup/protos/common/location/Phone$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/common/location/Phone$Builder;->build()Lcom/squareup/protos/common/location/Phone;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->phone(Lcom/squareup/protos/common/location/Phone;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    move-result-object p1

    .line 273
    invoke-virtual {p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->build()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;

    move-result-object p1

    const-string p2, "VerifyShippingAddressReq\u2026build())\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 267
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final toFailure(Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+",
            "Lcom/squareup/server/shipping/UpdateAddressResponse;",
            ">;)",
            "Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;"
        }
    .end annotation

    .line 260
    sget v0, Lcom/squareup/common/strings/R$string;->error_default:I

    sget-object v1, Lcom/squareup/mailorder/OrderServiceHelper$toFailure$2;->INSTANCE:Lcom/squareup/mailorder/OrderServiceHelper$toFailure$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2, v0, v1}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 262
    new-instance p2, Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p2
.end method

.method private final toFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+",
            "Lcom/squareup/server/shipping/UpdateAddressResponse;",
            ">;)",
            "Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;"
        }
    .end annotation

    .line 250
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper;->endpoints:Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;

    invoke-interface {v0}, Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;->placeOrderServerError()Lcom/squareup/widgets/warning/Warning;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 251
    invoke-direct {p0, v0}, Lcom/squareup/mailorder/OrderServiceHelper;->toFailure(Lcom/squareup/widgets/warning/Warning;)Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    invoke-direct {p0, v0, p1}, Lcom/squareup/mailorder/OrderServiceHelper;->toFailure(Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private final toFailure(Lcom/squareup/widgets/warning/Warning;)Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;
    .locals 3

    .line 255
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper;->resources:Landroid/content/res/Resources;

    invoke-interface {p1, v0}, Lcom/squareup/widgets/warning/Warning;->getStrings(Landroid/content/res/Resources;)Lcom/squareup/widgets/warning/Warning$Strings;

    move-result-object p1

    new-instance v0, Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;

    iget-object v1, p1, Lcom/squareup/widgets/warning/Warning$Strings;->title:Ljava/lang/String;

    const-string v2, "it.title"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lcom/squareup/widgets/warning/Warning$Strings;->body:Ljava/lang/String;

    const-string v2, "it.body"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, p1}, Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private final toServerError(Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServerError;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;",
            ">;)",
            "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServerError;"
        }
    .end annotation

    .line 240
    sget v0, Lcom/squareup/common/strings/R$string;->error_default:I

    sget-object v1, Lcom/squareup/mailorder/OrderServiceHelper$toServerError$2;->INSTANCE:Lcom/squareup/mailorder/OrderServiceHelper$toServerError$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2, v0, v1}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 242
    new-instance p2, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServerError;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServerError;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p2
.end method

.method private final toServerError(Lcom/squareup/widgets/warning/Warning;)Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServerError;
    .locals 3

    .line 234
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper;->resources:Landroid/content/res/Resources;

    invoke-interface {p1, v0}, Lcom/squareup/widgets/warning/Warning;->getStrings(Landroid/content/res/Resources;)Lcom/squareup/widgets/warning/Warning$Strings;

    move-result-object p1

    new-instance v0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServerError;

    iget-object v1, p1, Lcom/squareup/widgets/warning/Warning$Strings;->title:Ljava/lang/String;

    const-string v2, "it.title"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lcom/squareup/widgets/warning/Warning$Strings;->body:Ljava/lang/String;

    const-string v2, "it.body"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServerError;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private final toVerificationFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;",
            ">;)",
            "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;"
        }
    .end annotation

    .line 223
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object v0

    instance-of v1, v0, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;

    if-eqz v0, :cond_1

    .line 224
    iget-object v1, v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->verification_status:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    sget-object v2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->FAILED:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    if-ne v1, v2, :cond_1

    iget-object v1, v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->token:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 225
    new-instance p1, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderServiceHelper;->resources:Landroid/content/res/Resources;

    iget-object v0, v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->token:Ljava/lang/String;

    const-string v2, "response.token"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, v1, v0}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;-><init>(Landroid/content/res/Resources;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;

    return-object p1

    .line 229
    :cond_1
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper;->endpoints:Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;

    invoke-interface {v0}, Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;->verifyShippingAddressServerError()Lcom/squareup/widgets/warning/Warning;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 230
    invoke-direct {p0, v0}, Lcom/squareup/mailorder/OrderServiceHelper;->toServerError(Lcom/squareup/widgets/warning/Warning;)Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServerError;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    invoke-direct {p0, v0, p1}, Lcom/squareup/mailorder/OrderServiceHelper;->toServerError(Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServerError;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;

    :goto_0
    return-object v0
.end method

.method private final toVerificationResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;Lcom/squareup/mailorder/ContactInfo;)Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;",
            ">;",
            "Lcom/squareup/mailorder/ContactInfo;",
            ")",
            "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;"
        }
    .end annotation

    .line 198
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;

    .line 199
    iget-object v0, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->verification_status:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    const/4 v2, 0x0

    .line 200
    sget-object v3, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->UNKNOWN_STATUS:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    aput-object v3, v1, v2

    sget-object v2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->VALIDATION_SERVICE_ERROR:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v1}, Lkotlin/collections/SetsKt;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    .line 202
    iget-object v2, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->corrected_field:Ljava/util/List;

    const-string v4, "response.corrected_field"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/2addr v2, v3

    const-string v3, "response.token"

    const-string v4, "response.corrected_address"

    if-eqz v2, :cond_1

    new-instance p2, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;

    .line 203
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper;->resources:Landroid/content/res/Resources;

    iget-object v1, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->corrected_address:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->token:Ljava/lang/String;

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    invoke-direct {p2, v0, v1, p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;-><init>(Landroid/content/res/Resources;Lcom/squareup/protos/common/location/GlobalAddress;Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;

    goto :goto_1

    .line 205
    :cond_1
    sget-object v2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->VALIDATED:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    if-ne v0, v2, :cond_2

    new-instance v0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Correct;

    .line 206
    iget-object v1, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->token:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->corrected_address:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    invoke-direct {v0, v1, p2, p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Correct;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;)V

    move-object p2, v0

    check-cast p2, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;

    goto :goto_1

    .line 208
    :cond_2
    sget-object p2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->FAILED:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    if-ne v0, p2, :cond_4

    .line 209
    iget-object p2, p0, Lcom/squareup/mailorder/OrderServiceHelper;->resources:Landroid/content/res/Resources;

    .line 210
    iget-object p1, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->token:Ljava/lang/String;

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    const-string p1, ""

    .line 208
    :goto_0
    new-instance v0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;

    invoke-direct {v0, p2, p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;-><init>(Landroid/content/res/Resources;Ljava/lang/String;)V

    move-object p2, v0

    check-cast p2, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;

    goto :goto_1

    .line 212
    :cond_4
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_5

    invoke-direct {p0}, Lcom/squareup/mailorder/OrderServiceHelper;->verificationServiceError()Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServiceError;

    move-result-object p1

    move-object p2, p1

    check-cast p2, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;

    :goto_1
    return-object p2

    .line 213
    :cond_5
    new-instance p2, Ljava/lang/IllegalStateException;

    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected VerifyShippingAddressResponse "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 213
    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method private final verificationServiceError()Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServiceError;
    .locals 3

    .line 246
    new-instance v0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServiceError;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderServiceHelper;->resources:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/squareup/mailorder/OrderServiceHelper;->endpoints:Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;

    invoke-interface {v2}, Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;->verifyShippingAddressServiceError()Lcom/squareup/widgets/warning/Warning;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServiceError;-><init>(Landroid/content/res/Resources;Lcom/squareup/widgets/warning/Warning;)V

    return-object v0
.end method


# virtual methods
.method public final getMerchantProfile$mail_order_release()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;"
        }
    .end annotation

    .line 161
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper;->profileUpdater:Lcom/squareup/merchantprofile/MerchantProfileUpdater;

    invoke-interface {v0}, Lcom/squareup/merchantprofile/MerchantProfileUpdater;->getMerchantProfile()Lio/reactivex/Single;

    move-result-object v0

    .line 162
    sget-object v1, Lcom/squareup/mailorder/OrderServiceHelper$getMerchantProfile$1;->INSTANCE:Lcom/squareup/mailorder/OrderServiceHelper$getMerchantProfile$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "profileUpdater.getMercha\u2026e()\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final sendShippingAddress$mail_order_release(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/mailorder/ContactInfo;",
            "Lcom/squareup/protos/common/location/GlobalAddress;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/mailorder/OrderServiceHelper$OrderResult;",
            ">;"
        }
    .end annotation

    const-string v0, "itemToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "verifiedAddressToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactInfo"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper;->endpoints:Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;->placeOrder(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;)Lio/reactivex/Single;

    move-result-object p1

    .line 187
    new-instance p2, Lcom/squareup/mailorder/OrderServiceHelper$sendShippingAddress$1;

    invoke-direct {p2, p0}, Lcom/squareup/mailorder/OrderServiceHelper$sendShippingAddress$1;-><init>(Lcom/squareup/mailorder/OrderServiceHelper;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "endpoints.placeOrder(ite\u2026e()\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final verifyShippingAddress$mail_order_release(Lcom/squareup/mailorder/ContactInfo;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/ContactInfo;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;",
            ">;"
        }
    .end annotation

    const-string v0, "contactInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper;->endpoints:Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderServiceHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-direct {p0, p1, v1}, Lcom/squareup/mailorder/OrderServiceHelper;->createVerifyRequest(Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;->verifyShippingAddress(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;)Lio/reactivex/Single;

    move-result-object v0

    .line 172
    new-instance v1, Lcom/squareup/mailorder/OrderServiceHelper$verifyShippingAddress$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/mailorder/OrderServiceHelper$verifyShippingAddress$1;-><init>(Lcom/squareup/mailorder/OrderServiceHelper;Lcom/squareup/mailorder/ContactInfo;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "endpoints.verifyShipping\u2026e()\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
