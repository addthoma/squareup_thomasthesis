.class public interface abstract Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;
.super Ljava/lang/Object;
.source "OrderServiceHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderServiceHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Endpoints"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mailorder/OrderServiceHelper$Endpoints$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J6\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH&J\n\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J\u001c\u0010\u000f\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00100\u00040\u00032\u0006\u0010\u0011\u001a\u00020\u0012H&J\n\u0010\u0013\u001a\u0004\u0018\u00010\u000eH\u0016J\u0008\u0010\u0014\u001a\u00020\u000eH&\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;",
        "",
        "placeOrder",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/shipping/UpdateAddressResponse;",
        "itemToken",
        "",
        "verifiedAddressToken",
        "contactInfo",
        "Lcom/squareup/mailorder/ContactInfo;",
        "globalAddress",
        "Lcom/squareup/protos/common/location/GlobalAddress;",
        "placeOrderServerError",
        "Lcom/squareup/widgets/warning/Warning;",
        "verifyShippingAddress",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;",
        "body",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;",
        "verifyShippingAddressServerError",
        "verifyShippingAddressServiceError",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract placeOrder(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/mailorder/ContactInfo;",
            "Lcom/squareup/protos/common/location/GlobalAddress;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/shipping/UpdateAddressResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract placeOrderServerError()Lcom/squareup/widgets/warning/Warning;
.end method

.method public abstract verifyShippingAddress(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract verifyShippingAddressServerError()Lcom/squareup/widgets/warning/Warning;
.end method

.method public abstract verifyShippingAddressServiceError()Lcom/squareup/widgets/warning/Warning;
.end method
