.class public final Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;
.super Lcom/squareup/mailorder/OrderReactor$OrderState;
.source "OrderReactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderReactor$OrderState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SendingOrder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$Creator;,
        Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0014\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0087\u0008\u0018\u00002\u00020\u0001:\u0001-B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\nH\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u000cH\u00c6\u0003JE\u0010\u001f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cH\u00c6\u0001J\t\u0010 \u001a\u00020!H\u00d6\u0001J\u0013\u0010\"\u001a\u00020#2\u0008\u0010$\u001a\u0004\u0018\u00010%H\u00d6\u0003J\t\u0010&\u001a\u00020!H\u00d6\u0001J\t\u0010\'\u001a\u00020\u0003H\u00d6\u0001J\u0019\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020!H\u00d6\u0001R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0015\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        "itemToken",
        "",
        "cardCustomizationOption",
        "Lcom/squareup/mailorder/CardCustomizationOption;",
        "shippingToken",
        "contactInfo",
        "Lcom/squareup/mailorder/ContactInfo;",
        "globalAddress",
        "Lcom/squareup/protos/common/location/GlobalAddress;",
        "originationFlow",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;",
        "(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;)V",
        "getCardCustomizationOption",
        "()Lcom/squareup/mailorder/CardCustomizationOption;",
        "getContactInfo",
        "()Lcom/squareup/mailorder/ContactInfo;",
        "getGlobalAddress",
        "()Lcom/squareup/protos/common/location/GlobalAddress;",
        "getItemToken",
        "()Ljava/lang/String;",
        "getOriginationFlow",
        "()Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;",
        "getShippingToken",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "OriginationFlow",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final cardCustomizationOption:Lcom/squareup/mailorder/CardCustomizationOption;

.field private final contactInfo:Lcom/squareup/mailorder/ContactInfo;

.field private final globalAddress:Lcom/squareup/protos/common/location/GlobalAddress;

.field private final itemToken:Ljava/lang/String;

.field private final originationFlow:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;

.field private final shippingToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$Creator;

    invoke-direct {v0}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$Creator;-><init>()V

    sput-object v0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;)V
    .locals 1

    const-string v0, "itemToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardCustomizationOption"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "shippingToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactInfo"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "globalAddress"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originationFlow"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 132
    invoke-direct {p0, v0}, Lcom/squareup/mailorder/OrderReactor$OrderState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->itemToken:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->cardCustomizationOption:Lcom/squareup/mailorder/CardCustomizationOption;

    iput-object p3, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->shippingToken:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    iput-object p5, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->globalAddress:Lcom/squareup/protos/common/location/GlobalAddress;

    iput-object p6, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->originationFlow:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;ILjava/lang/Object;)Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getItemToken()Ljava/lang/String;

    move-result-object p1

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object p2

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->shippingToken:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->globalAddress:Lcom/squareup/protos/common/location/GlobalAddress;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->originationFlow:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->copy(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;)Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getItemToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/mailorder/CardCustomizationOption;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->shippingToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Lcom/squareup/mailorder/ContactInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    return-object v0
.end method

.method public final component5()Lcom/squareup/protos/common/location/GlobalAddress;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->globalAddress:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object v0
.end method

.method public final component6()Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->originationFlow:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;)Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;
    .locals 8

    const-string v0, "itemToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardCustomizationOption"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "shippingToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactInfo"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "globalAddress"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originationFlow"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getItemToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getItemToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->shippingToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->shippingToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    iget-object v1, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->globalAddress:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v1, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->globalAddress:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->originationFlow:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;

    iget-object p1, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->originationFlow:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->cardCustomizationOption:Lcom/squareup/mailorder/CardCustomizationOption;

    return-object v0
.end method

.method public final getContactInfo()Lcom/squareup/mailorder/ContactInfo;
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    return-object v0
.end method

.method public final getGlobalAddress()Lcom/squareup/protos/common/location/GlobalAddress;
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->globalAddress:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object v0
.end method

.method public getItemToken()Ljava/lang/String;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->itemToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getOriginationFlow()Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->originationFlow:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;

    return-object v0
.end method

.method public final getShippingToken()Ljava/lang/String;
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->shippingToken:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getItemToken()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->shippingToken:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->globalAddress:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->originationFlow:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SendingOrder(itemToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getItemToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", cardCustomizationOption="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", shippingToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->shippingToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", contactInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", globalAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->globalAddress:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", originationFlow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->originationFlow:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->itemToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->cardCustomizationOption:Lcom/squareup/mailorder/CardCustomizationOption;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->shippingToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->globalAddress:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->originationFlow:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
