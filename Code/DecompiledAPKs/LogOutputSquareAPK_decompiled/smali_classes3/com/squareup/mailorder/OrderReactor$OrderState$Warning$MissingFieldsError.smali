.class public final Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;
.super Lcom/squareup/mailorder/OrderReactor$OrderState$Warning;
.source "OrderReactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderReactor$OrderState$Warning;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MissingFieldsError"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J1\u0010\u0013\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0003H\u00c6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u0003H\u00d6\u0001J\u0019\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0015H\u00d6\u0001R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0007\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0014\u0010\u0006\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000c\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$Warning;",
        "itemToken",
        "",
        "cardCustomizationOption",
        "Lcom/squareup/mailorder/CardCustomizationOption;",
        "title",
        "message",
        "(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Ljava/lang/String;)V",
        "getCardCustomizationOption",
        "()Lcom/squareup/mailorder/CardCustomizationOption;",
        "getItemToken",
        "()Ljava/lang/String;",
        "getMessage",
        "getTitle",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final cardCustomizationOption:Lcom/squareup/mailorder/CardCustomizationOption;

.field private final itemToken:Ljava/lang/String;

.field private final message:Ljava/lang/String;

.field private final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError$Creator;

    invoke-direct {v0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError$Creator;-><init>()V

    sput-object v0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "itemToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardCustomizationOption"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 192
    invoke-direct {p0, v0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->itemToken:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->cardCustomizationOption:Lcom/squareup/mailorder/CardCustomizationOption;

    iput-object p3, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->title:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->message:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getItemToken()Ljava/lang/String;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object p2

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getTitle()Ljava/lang/String;

    move-result-object p3

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getMessage()Ljava/lang/String;

    move-result-object p4

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->copy(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getItemToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/mailorder/CardCustomizationOption;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;
    .locals 1

    const-string v0, "itemToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardCustomizationOption"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getItemToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getItemToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->cardCustomizationOption:Lcom/squareup/mailorder/CardCustomizationOption;

    return-object v0
.end method

.method public getItemToken()Ljava/lang/String;
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->itemToken:Ljava/lang/String;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getItemToken()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getMessage()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MissingFieldsError(itemToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getItemToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", cardCustomizationOption="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->itemToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->cardCustomizationOption:Lcom/squareup/mailorder/CardCustomizationOption;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->message:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
