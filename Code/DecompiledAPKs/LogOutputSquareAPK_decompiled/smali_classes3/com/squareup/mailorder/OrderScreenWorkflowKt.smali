.class public final Lcom/squareup/mailorder/OrderScreenWorkflowKt;
.super Ljava/lang/Object;
.source "OrderScreenWorkflow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000*F\u0010\u0000\"\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003`\u000122\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u00060\u0005\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0004\u00a8\u0006\n"
    }
    d2 = {
        "OrderScreenWorkflow",
        "Lcom/squareup/workflow/rx1/ModalScreenWorkflow;",
        "",
        "Lcom/squareup/mailorder/OrderWorkflowResult;",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "mail-order_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
