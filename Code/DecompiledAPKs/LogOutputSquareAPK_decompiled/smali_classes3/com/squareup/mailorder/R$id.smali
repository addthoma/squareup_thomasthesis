.class public final Lcom/squareup/mailorder/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final address_selector_container:I = 0x7f0a0181

.field public static final address_selector_is_recommended:I = 0x7f0a0182

.field public static final address_selector_name:I = 0x7f0a0183

.field public static final address_selector_radio_button:I = 0x7f0a0184

.field public static final address_selector_text_container:I = 0x7f0a0185

.field public static final modified_warning:I = 0x7f0a09e3

.field public static final order_button:I = 0x7f0a0ac4

.field public static final order_confirmed_help:I = 0x7f0a0ac5

.field public static final order_confirmed_submessage:I = 0x7f0a0ac6

.field public static final order_done:I = 0x7f0a0aca

.field public static final order_glyph:I = 0x7f0a0ace

.field public static final order_glyph_message:I = 0x7f0a0acf

.field public static final order_message_for_no_title:I = 0x7f0a0ad5

.field public static final order_message_for_title:I = 0x7f0a0ad6

.field public static final order_title:I = 0x7f0a0af1

.field public static final phone_number:I = 0x7f0a0c25

.field public static final select_address_1:I = 0x7f0a0e3e

.field public static final select_address_2:I = 0x7f0a0e3f

.field public static final select_address_order_button:I = 0x7f0a0e40

.field public static final shipping_address:I = 0x7f0a0e7b

.field public static final shipping_name:I = 0x7f0a0e7c

.field public static final street:I = 0x7f0a0f41

.field public static final uncorrectable_warning:I = 0x7f0a10ad

.field public static final unverified_address:I = 0x7f0a10bc

.field public static final unverified_address_order_free_card:I = 0x7f0a10bd

.field public static final unverified_address_re_enter_address:I = 0x7f0a10be


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
