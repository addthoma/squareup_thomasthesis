.class final Lcom/squareup/mailorder/OrderReactor$onReact$6$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mailorder/OrderReactor$onReact$6;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$ConfirmationDoneClicked;",
        "Lcom/squareup/workflow/legacy/FinishWith<",
        "+",
        "Lcom/squareup/mailorder/OrderWorkflowResult$FinishedWithOrder;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/FinishWith;",
        "Lcom/squareup/mailorder/OrderWorkflowResult$FinishedWithOrder;",
        "it",
        "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$ConfirmationDoneClicked;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/mailorder/OrderReactor$onReact$6;


# direct methods
.method constructor <init>(Lcom/squareup/mailorder/OrderReactor$onReact$6;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$6$1;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$6;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$ConfirmationDoneClicked;)Lcom/squareup/workflow/legacy/FinishWith;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$ConfirmationDoneClicked;",
            ")",
            "Lcom/squareup/workflow/legacy/FinishWith<",
            "Lcom/squareup/mailorder/OrderWorkflowResult$FinishedWithOrder;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 345
    iget-object p1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$6$1;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$6;

    iget-object p1, p1, Lcom/squareup/mailorder/OrderReactor$onReact$6;->this$0:Lcom/squareup/mailorder/OrderReactor;

    invoke-static {p1}, Lcom/squareup/mailorder/OrderReactor;->access$getAnalytics$p(Lcom/squareup/mailorder/OrderReactor;)Lcom/squareup/mailorder/MailOrderAnalytics;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/mailorder/MailOrderAnalytics;->logShippingOrderConfirmedScreen()V

    .line 346
    iget-object p1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$6$1;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$6;

    iget-object p1, p1, Lcom/squareup/mailorder/OrderReactor$onReact$6;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object p1

    sget-object v0, Lcom/squareup/mailorder/OrderReactor$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/mailorder/CardCustomizationOption;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 349
    :cond_0
    iget-object p1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$6$1;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$6;

    iget-object p1, p1, Lcom/squareup/mailorder/OrderReactor$onReact$6;->this$0:Lcom/squareup/mailorder/OrderReactor;

    invoke-static {p1}, Lcom/squareup/mailorder/OrderReactor;->access$getAnalytics$p(Lcom/squareup/mailorder/OrderReactor;)Lcom/squareup/mailorder/MailOrderAnalytics;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/mailorder/MailOrderAnalytics;->logShippingOrderConfirmedWithStampsAndSignature()V

    goto :goto_0

    .line 348
    :cond_1
    iget-object p1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$6$1;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$6;

    iget-object p1, p1, Lcom/squareup/mailorder/OrderReactor$onReact$6;->this$0:Lcom/squareup/mailorder/OrderReactor;

    invoke-static {p1}, Lcom/squareup/mailorder/OrderReactor;->access$getAnalytics$p(Lcom/squareup/mailorder/OrderReactor;)Lcom/squareup/mailorder/MailOrderAnalytics;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/mailorder/MailOrderAnalytics;->logShippingOrderConfirmedWithStampsOnly()V

    goto :goto_0

    .line 347
    :cond_2
    iget-object p1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$6$1;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$6;

    iget-object p1, p1, Lcom/squareup/mailorder/OrderReactor$onReact$6;->this$0:Lcom/squareup/mailorder/OrderReactor;

    invoke-static {p1}, Lcom/squareup/mailorder/OrderReactor;->access$getAnalytics$p(Lcom/squareup/mailorder/OrderReactor;)Lcom/squareup/mailorder/MailOrderAnalytics;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/mailorder/MailOrderAnalytics;->logShippingOrderConfirmedWithSignatureOnly()V

    .line 354
    :goto_0
    new-instance p1, Lcom/squareup/workflow/legacy/FinishWith;

    sget-object v0, Lcom/squareup/mailorder/OrderWorkflowResult$FinishedWithOrder;->INSTANCE:Lcom/squareup/mailorder/OrderWorkflowResult$FinishedWithOrder;

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$ConfirmationDoneClicked;

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/OrderReactor$onReact$6$1;->invoke(Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$ConfirmationDoneClicked;)Lcom/squareup/workflow/legacy/FinishWith;

    move-result-object p1

    return-object p1
.end method
