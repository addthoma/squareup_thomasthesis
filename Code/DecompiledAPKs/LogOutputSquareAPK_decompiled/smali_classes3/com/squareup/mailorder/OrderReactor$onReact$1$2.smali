.class final Lcom/squareup/mailorder/OrderReactor$onReact$1$2;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mailorder/OrderReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/mailorder/ContactInfo;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000e\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;",
        "contactInfo",
        "Lcom/squareup/mailorder/ContactInfo;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/mailorder/OrderReactor$onReact$1;


# direct methods
.method constructor <init>(Lcom/squareup/mailorder/OrderReactor$onReact$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$1$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/mailorder/ContactInfo;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/ContactInfo;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;",
            ">;"
        }
    .end annotation

    .line 221
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 222
    new-instance v8, Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;

    .line 223
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$1$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/mailorder/OrderReactor$onReact$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState;->getItemToken()Ljava/lang/String;

    move-result-object v2

    .line 224
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$1$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/mailorder/OrderReactor$onReact$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v3

    const-string v1, "contactInfo"

    .line 225
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, v8

    move-object v4, p1

    .line 222
    invoke-direct/range {v1 .. v7}, Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 221
    invoke-direct {v0, v8}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/mailorder/ContactInfo;

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/OrderReactor$onReact$1$2;->invoke(Lcom/squareup/mailorder/ContactInfo;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
