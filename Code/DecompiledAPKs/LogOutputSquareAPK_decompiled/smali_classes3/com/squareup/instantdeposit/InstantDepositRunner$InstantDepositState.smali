.class public final enum Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;
.super Ljava/lang/Enum;
.source "InstantDepositRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/instantdeposit/InstantDepositRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InstantDepositState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0008\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;",
        "",
        "(Ljava/lang/String;I)V",
        "LOADING",
        "CAN_MAKE_DEPOSIT",
        "NOT_ELIGIBLE",
        "DEPOSIT_SUCCEEDED",
        "DEPOSIT_FAILED",
        "SERVER_CALL_FAILED",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

.field public static final enum CAN_MAKE_DEPOSIT:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

.field public static final enum DEPOSIT_FAILED:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

.field public static final enum DEPOSIT_SUCCEEDED:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

.field public static final enum LOADING:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

.field public static final enum NOT_ELIGIBLE:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

.field public static final enum SERVER_CALL_FAILED:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    new-instance v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    const/4 v2, 0x0

    const-string v3, "LOADING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->LOADING:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    const/4 v2, 0x1

    const-string v3, "CAN_MAKE_DEPOSIT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->CAN_MAKE_DEPOSIT:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    const/4 v2, 0x2

    const-string v3, "NOT_ELIGIBLE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->NOT_ELIGIBLE:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    const/4 v2, 0x3

    const-string v3, "DEPOSIT_SUCCEEDED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->DEPOSIT_SUCCEEDED:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    const/4 v2, 0x4

    const-string v3, "DEPOSIT_FAILED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->DEPOSIT_FAILED:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    const/4 v2, 0x5

    const-string v3, "SERVER_CALL_FAILED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->SERVER_CALL_FAILED:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->$VALUES:[Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;
    .locals 1

    const-class v0, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;
    .locals 1

    sget-object v0, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->$VALUES:[Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    invoke-virtual {v0}, [Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    return-object v0
.end method
