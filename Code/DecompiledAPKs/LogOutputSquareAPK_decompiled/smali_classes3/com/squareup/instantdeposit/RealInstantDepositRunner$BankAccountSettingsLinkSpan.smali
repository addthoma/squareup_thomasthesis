.class final Lcom/squareup/instantdeposit/RealInstantDepositRunner$BankAccountSettingsLinkSpan;
.super Lcom/squareup/ui/LinkSpan;
.source "RealInstantDepositRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/instantdeposit/RealInstantDepositRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BankAccountSettingsLinkSpan"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/instantdeposit/RealInstantDepositRunner$BankAccountSettingsLinkSpan;",
        "Lcom/squareup/ui/LinkSpan;",
        "(Lcom/squareup/instantdeposit/RealInstantDepositRunner;)V",
        "onClick",
        "",
        "widget",
        "Landroid/view/View;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/instantdeposit/RealInstantDepositRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/instantdeposit/RealInstantDepositRunner;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 249
    iput-object p1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner$BankAccountSettingsLinkSpan;->this$0:Lcom/squareup/instantdeposit/RealInstantDepositRunner;

    .line 250
    invoke-static {p1}, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->access$getRes$p(Lcom/squareup/instantdeposit/RealInstantDepositRunner;)Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/ui/LinkSpan;->DEFAULT_COLOR_ID:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    .line 249
    invoke-direct {p0, p1}, Lcom/squareup/ui/LinkSpan;-><init>(I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "widget"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 253
    iget-object p1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner$BankAccountSettingsLinkSpan;->this$0:Lcom/squareup/instantdeposit/RealInstantDepositRunner;

    invoke-static {p1}, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->access$getSettingsAppletGateway$p(Lcom/squareup/instantdeposit/RealInstantDepositRunner;)Lcom/squareup/ui/settings/SettingsAppletGateway;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/settings/SettingsAppletGateway;->activateBankAccountSettings()V

    return-void
.end method
