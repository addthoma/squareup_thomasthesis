.class public Lcom/squareup/instantdeposit/InstantDepositCountryResources;
.super Ljava/lang/Object;
.source "InstantDepositCountryResources.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static instantDepositButtonHint(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/CountryCode;->GB:Lcom/squareup/CountryCode;

    if-ne p0, v0, :cond_0

    sget p0, Lcom/squareup/instantdeposit/R$string;->instant_deposits_button_hint_uk:I

    goto :goto_0

    :cond_0
    sget p0, Lcom/squareup/instantdeposit/R$string;->instant_deposits_button_hint:I

    :goto_0
    return p0
.end method

.method public static instantDepositSuccessTitle(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/CountryCode;->GB:Lcom/squareup/CountryCode;

    if-ne p0, v0, :cond_0

    sget p0, Lcom/squareup/instantdeposit/R$string;->instant_deposits_success_title_uk:I

    goto :goto_0

    :cond_0
    sget p0, Lcom/squareup/instantdeposit/R$string;->instant_deposits_success_title:I

    :goto_0
    return p0
.end method
