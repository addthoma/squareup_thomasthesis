.class public interface abstract Lcom/squareup/instantdeposit/InstantDepositAnalytics;
.super Ljava/lang/Object;
.source "InstantDepositAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/instantdeposit/InstantDepositAnalytics$DepositAvailableFundsTapEvent;,
        Lcom/squareup/instantdeposit/InstantDepositAnalytics$DepositingErrorEvent;,
        Lcom/squareup/instantdeposit/InstantDepositAnalytics$LearnMoreErrorEvent;,
        Lcom/squareup/instantdeposit/InstantDepositAnalytics$LinkCardFailedErrorEvent;,
        Lcom/squareup/instantdeposit/InstantDepositAnalytics$InstantDepositToggleAction;,
        Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0019\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\r\u0008f\u0018\u00002\u00020\u0001:\u000656789:J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J \u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0008H&J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u000b\u001a\u00020\u0003H&J\u0008\u0010\u000c\u001a\u00020\u0003H&J \u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0008H&J\u0010\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u0012H&J\u0010\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u0005H&J\u0010\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u0005H&J\u0010\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u0008H&J\u0008\u0010\u0019\u001a\u00020\u0003H&J\u0008\u0010\u001a\u001a\u00020\u0003H&J\u0008\u0010\u001b\u001a\u00020\u0003H&J\u0008\u0010\u001c\u001a\u00020\u0003H&J\u0008\u0010\u001d\u001a\u00020\u0003H&J\u0008\u0010\u001e\u001a\u00020\u0003H&J\u0008\u0010\u001f\u001a\u00020\u0003H&J\u0008\u0010 \u001a\u00020\u0003H&J\u0008\u0010!\u001a\u00020\u0003H&J\u0008\u0010\"\u001a\u00020\u0003H&J\u0008\u0010#\u001a\u00020\u0003H&J\u0008\u0010$\u001a\u00020\u0003H&J\u0008\u0010%\u001a\u00020\u0003H&J\u0008\u0010&\u001a\u00020\u0003H&J\u0008\u0010\'\u001a\u00020\u0003H&J\u0018\u0010(\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0008H&J\u0008\u0010)\u001a\u00020\u0003H&J \u0010*\u001a\u00020\u00032\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u00020\u0005H&J \u00100\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0008H&J\u0008\u00101\u001a\u00020\u0003H&J\u0008\u00102\u001a\u00020\u0003H&J\u0008\u00103\u001a\u00020\u0003H&J\u0018\u00104\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0016\u001a\u00020\u0005H&\u00a8\u0006;"
    }
    d2 = {
        "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
        "",
        "cancelCardLinking",
        "",
        "hasLinkedCard",
        "",
        "cardLinkFailure",
        "title",
        "",
        "message",
        "cardLinkSuccess",
        "headerDisplayedShowingAvailableBalance",
        "headerDisplayedShowingError",
        "instantDepositFailed",
        "registerErrorName",
        "Lcom/squareup/analytics/RegisterErrorName;",
        "instantDepositSucceeded",
        "registerViewName",
        "Lcom/squareup/analytics/RegisterViewName;",
        "instantDepositToggled",
        "checked",
        "logBalanceAppletAddDebitCardAttempt",
        "isSuccessful",
        "logBalanceAppletAddDebitCardFailure",
        "failureMessage",
        "logBalanceAppletAddDebitCardSuccess",
        "logBalanceAppletDisplayedAvailableBalance",
        "logBalanceAppletInstantTransferClick",
        "logBalanceAppletInstantTransferSuccess",
        "logBalanceAppletLinkDebitCardCancel",
        "logBalanceAppletSetUpInstantTransfer",
        "logBalanceAppletTransferEstimatedFeesClick",
        "logBalanceAppletTransferEstimatedFeesLearnMoreClick",
        "logBalanceAppletTransferReportsDisplayedAvailableBalance",
        "logBalanceAppletTransferReportsInstantTransferClick",
        "logBalanceAppletTransferReportsInstantTransferSuccess",
        "logTransferReportsActiveSalesClick",
        "logTransferReportsPendingDepositClick",
        "logTransferReportsSentDepositClick",
        "logTransferReportsSummaryView",
        "resendEmailFailure",
        "resendEmailSuccess",
        "tapDepositAvailableFunds",
        "registerTapName",
        "Lcom/squareup/analytics/RegisterTapName;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "isTransferReports",
        "tapLearnMore",
        "tapLinkDifferentDebitCard",
        "transferReportsDisplayedAvailableBalance",
        "transferReportsDisplayedError",
        "tryToLinkCard",
        "DepositAvailableFundsTapEvent",
        "DepositingErrorEvent",
        "InstantDepositToggleAction",
        "LearnMoreErrorEvent",
        "LinkCardFailedErrorEvent",
        "NoInstantDepositAnalytics",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract cancelCardLinking(Z)V
.end method

.method public abstract cardLinkFailure(ZLjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract cardLinkSuccess(Z)V
.end method

.method public abstract headerDisplayedShowingAvailableBalance()V
.end method

.method public abstract headerDisplayedShowingError()V
.end method

.method public abstract instantDepositFailed(Lcom/squareup/analytics/RegisterErrorName;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract instantDepositSucceeded(Lcom/squareup/analytics/RegisterViewName;)V
.end method

.method public abstract instantDepositToggled(Z)V
.end method

.method public abstract logBalanceAppletAddDebitCardAttempt(Z)V
.end method

.method public abstract logBalanceAppletAddDebitCardFailure(Ljava/lang/String;)V
.end method

.method public abstract logBalanceAppletAddDebitCardSuccess()V
.end method

.method public abstract logBalanceAppletDisplayedAvailableBalance()V
.end method

.method public abstract logBalanceAppletInstantTransferClick()V
.end method

.method public abstract logBalanceAppletInstantTransferSuccess()V
.end method

.method public abstract logBalanceAppletLinkDebitCardCancel()V
.end method

.method public abstract logBalanceAppletSetUpInstantTransfer()V
.end method

.method public abstract logBalanceAppletTransferEstimatedFeesClick()V
.end method

.method public abstract logBalanceAppletTransferEstimatedFeesLearnMoreClick()V
.end method

.method public abstract logBalanceAppletTransferReportsDisplayedAvailableBalance()V
.end method

.method public abstract logBalanceAppletTransferReportsInstantTransferClick()V
.end method

.method public abstract logBalanceAppletTransferReportsInstantTransferSuccess()V
.end method

.method public abstract logTransferReportsActiveSalesClick()V
.end method

.method public abstract logTransferReportsPendingDepositClick()V
.end method

.method public abstract logTransferReportsSentDepositClick()V
.end method

.method public abstract logTransferReportsSummaryView()V
.end method

.method public abstract resendEmailFailure(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract resendEmailSuccess()V
.end method

.method public abstract tapDepositAvailableFunds(Lcom/squareup/analytics/RegisterTapName;Lcom/squareup/protos/common/Money;Z)V
.end method

.method public abstract tapLearnMore(Lcom/squareup/analytics/RegisterErrorName;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract tapLinkDifferentDebitCard()V
.end method

.method public abstract transferReportsDisplayedAvailableBalance()V
.end method

.method public abstract transferReportsDisplayedError()V
.end method

.method public abstract tryToLinkCard(ZZ)V
.end method
