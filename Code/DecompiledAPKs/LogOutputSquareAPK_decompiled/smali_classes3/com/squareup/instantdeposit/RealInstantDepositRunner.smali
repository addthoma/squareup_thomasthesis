.class public final Lcom/squareup/instantdeposit/RealInstantDepositRunner;
.super Ljava/lang/Object;
.source "RealInstantDepositRunner.kt"

# interfaces
.implements Lcom/squareup/instantdeposit/InstantDepositRunner;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/instantdeposit/RealInstantDepositRunner$DepositsSettingsLinkSpan;,
        Lcom/squareup/instantdeposit/RealInstantDepositRunner$BankAccountSettingsLinkSpan;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008\u0007\u0018\u00002\u00020\u0001:\u0002./B7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0016\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u001c\u0010\u001a\u001a\u00020\u001b2\u0008\u0008\u0001\u0010\u001c\u001a\u00020\u001d2\u0008\u0008\u0001\u0010\u001e\u001a\u00020\u001dH\u0002J\u0010\u0010\u001f\u001a\u00020 2\u0006\u0010\u0013\u001a\u00020\u0015H\u0016J\u0016\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00172\u0006\u0010\"\u001a\u00020\u001bH\u0002J\u0016\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J\u001c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00172\u000c\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\'0&H\u0002J\u0016\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00172\u0006\u0010\"\u001a\u00020\'H\u0002J\u000e\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110)H\u0016J\u001c\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00172\u000c\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\u001b0&H\u0002J\u000e\u0010+\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0017H\u0002J\u000e\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0017H\u0016J\u0008\u0010-\u001a\u00020\u0011H\u0016J\u000e\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150)H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000f\u001a\u0010\u0012\u000c\u0012\n \u0012*\u0004\u0018\u00010\u00110\u00110\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0013\u001a\u0010\u0012\u000c\u0012\n \u0012*\u0004\u0018\u00010\u00150\u00150\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/instantdeposit/RealInstantDepositRunner;",
        "Lcom/squareup/instantdeposit/InstantDepositRunner;",
        "application",
        "Landroid/app/Application;",
        "res",
        "Landroid/content/res/Resources;",
        "instantDepositsService",
        "Lcom/squareup/instantdeposit/InstantDepositsService;",
        "transfersService",
        "Lcom/squareup/balance/core/server/transfers/TransfersService;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "settingsAppletGateway",
        "Lcom/squareup/ui/settings/SettingsAppletGateway;",
        "(Landroid/app/Application;Landroid/content/res/Resources;Lcom/squareup/instantdeposit/InstantDepositsService;Lcom/squareup/balance/core/server/transfers/TransfersService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SettingsAppletGateway;)V",
        "onInstantDepositMade",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "snapshot",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
        "checkIfEligibleForInstantDeposit",
        "Lio/reactivex/Single;",
        "includeRecentActivity",
        "",
        "createErrorResponse",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;",
        "titleId",
        "",
        "descriptionId",
        "instantDepositHint",
        "",
        "onCanMakeDeposit",
        "response",
        "onCheckEligibility",
        "onDepositFailed",
        "received",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "Lcom/squareup/protos/deposits/CreateTransferResponse;",
        "onDepositSucceeded",
        "Lio/reactivex/Observable;",
        "onNotEligible",
        "onSendInstantDeposit",
        "sendInstantDeposit",
        "setIsConfirmingInstantTransfer",
        "BankAccountSettingsLinkSpan",
        "DepositsSettingsLinkSpan",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final application:Landroid/app/Application;

.field private final instantDepositsService:Lcom/squareup/instantdeposit/InstantDepositsService;

.field private final onInstantDepositMade:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Landroid/content/res/Resources;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

.field private final snapshot:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation
.end field

.field private final transfersService:Lcom/squareup/balance/core/server/transfers/TransfersService;


# direct methods
.method public constructor <init>(Landroid/app/Application;Landroid/content/res/Resources;Lcom/squareup/instantdeposit/InstantDepositsService;Lcom/squareup/balance/core/server/transfers/TransfersService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SettingsAppletGateway;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instantDepositsService"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transfersService"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settingsAppletGateway"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->application:Landroid/app/Application;

    iput-object p2, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->res:Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->instantDepositsService:Lcom/squareup/instantdeposit/InstantDepositsService;

    iput-object p4, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->transfersService:Lcom/squareup/balance/core/server/transfers/TransfersService;

    iput-object p5, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p6, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

    .line 57
    new-instance p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xf

    const/4 v7, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;-><init>(Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(Snapshot())"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->snapshot:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 58
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->onInstantDepositMade:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$getOnInstantDepositMade$p(Lcom/squareup/instantdeposit/RealInstantDepositRunner;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->onInstantDepositMade:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/instantdeposit/RealInstantDepositRunner;)Landroid/content/res/Resources;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->res:Landroid/content/res/Resources;

    return-object p0
.end method

.method public static final synthetic access$getSettingsAppletGateway$p(Lcom/squareup/instantdeposit/RealInstantDepositRunner;)Lcom/squareup/ui/settings/SettingsAppletGateway;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

    return-object p0
.end method

.method public static final synthetic access$onCanMakeDeposit(Lcom/squareup/instantdeposit/RealInstantDepositRunner;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;)Lio/reactivex/Single;
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->onCanMakeDeposit(Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onDepositFailed(Lcom/squareup/instantdeposit/RealInstantDepositRunner;Lcom/squareup/receiving/ReceivedResponse;)Lio/reactivex/Single;
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->onDepositFailed(Lcom/squareup/receiving/ReceivedResponse;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onDepositSucceeded(Lcom/squareup/instantdeposit/RealInstantDepositRunner;Lcom/squareup/protos/deposits/CreateTransferResponse;)Lio/reactivex/Single;
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->onDepositSucceeded(Lcom/squareup/protos/deposits/CreateTransferResponse;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onNotEligible(Lcom/squareup/instantdeposit/RealInstantDepositRunner;Lcom/squareup/receiving/ReceivedResponse;)Lio/reactivex/Single;
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->onNotEligible(Lcom/squareup/receiving/ReceivedResponse;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final createErrorResponse(II)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;
    .locals 2

    .line 217
    new-instance v0, Lcom/squareup/protos/client/Status$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/Status$Builder;-><init>()V

    const/4 v1, 0x0

    .line 218
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/Status$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/Status$Builder;

    move-result-object v0

    .line 219
    iget-object v1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->res:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/Status$Builder;->localized_title(Ljava/lang/String;)Lcom/squareup/protos/client/Status$Builder;

    move-result-object p1

    .line 220
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->res:Landroid/content/res/Resources;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/Status$Builder;->localized_description(Ljava/lang/String;)Lcom/squareup/protos/client/Status$Builder;

    move-result-object p1

    .line 221
    invoke-virtual {p1}, Lcom/squareup/protos/client/Status$Builder;->build()Lcom/squareup/protos/client/Status;

    move-result-object p1

    .line 223
    new-instance p2, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;-><init>()V

    .line 224
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->eligibility_status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;

    move-result-object p1

    .line 225
    sget-object p2, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->UNKNOWN:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->eligibility_blocker(Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;)Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;

    move-result-object p1

    .line 226
    invoke-virtual {p1}, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->build()Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    move-result-object p1

    .line 228
    iget-object p2, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->snapshot:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p2

    if-nez p2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast p2, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    iget-object p2, p2, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    if-eqz p2, :cond_1

    .line 231
    invoke-virtual {p2}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->newBuilder()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;

    move-result-object p2

    goto :goto_0

    .line 233
    :cond_1
    new-instance p2, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;-><init>()V

    .line 237
    :goto_0
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->instant_deposit_details(Lcom/squareup/protos/client/deposits/InstantDepositDetails;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;

    move-result-object p1

    .line 238
    invoke-virtual {p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->build()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    move-result-object p1

    const-string p2, "getBalanceSummaryRespons\u2026Details)\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onCanMakeDeposit(Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;)Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation

    .line 141
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->snapshot:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    .line 142
    sget-object v2, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->CAN_MAKE_DEPOSIT:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v3, p1

    .line 141
    invoke-static/range {v1 .. v7}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->copy$default(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;ZILjava/lang/Object;)Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    move-result-object p1

    .line 145
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->snapshot:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 146
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "just(newSnapshot)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onCheckEligibility(Z)Lio/reactivex/Single;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation

    .line 127
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->snapshot:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v2, v1

    check-cast v2, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    sget-object v3, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->LOADING:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->copy$default(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;ZILjava/lang/Object;)Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 128
    new-instance v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;-><init>(Ljava/lang/Boolean;)V

    .line 130
    iget-object p1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->instantDepositsService:Lcom/squareup/instantdeposit/InstantDepositsService;

    invoke-interface {p1, v0}, Lcom/squareup/instantdeposit/InstantDepositsService;->getBalanceSummary(Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;)Lcom/squareup/instantdeposit/InstantDepositsService$GetBalanceSummaryStandardResponse;

    move-result-object p1

    .line 131
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositsService$GetBalanceSummaryStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 132
    new-instance v0, Lcom/squareup/instantdeposit/RealInstantDepositRunner$onCheckEligibility$1;

    invoke-direct {v0, p0}, Lcom/squareup/instantdeposit/RealInstantDepositRunner$onCheckEligibility$1;-><init>(Lcom/squareup/instantdeposit/RealInstantDepositRunner;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "instantDepositsService.g\u2026ed)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onDepositFailed(Lcom/squareup/receiving/ReceivedResponse;)Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/deposits/CreateTransferResponse;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation

    .line 203
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->snapshot:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    .line 204
    sget-object v2, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->DEPOSIT_FAILED:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    const/4 v3, 0x0

    .line 205
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    move-object v4, p1

    check-cast v4, Lcom/squareup/protos/deposits/CreateTransferResponse;

    const/4 v5, 0x0

    const/16 v6, 0xa

    const/4 v7, 0x0

    .line 203
    invoke-static/range {v1 .. v7}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->copy$default(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;ZILjava/lang/Object;)Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    move-result-object p1

    goto :goto_0

    .line 207
    :cond_1
    iget-object p1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->snapshot:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    sget-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->SERVER_CALL_FAILED:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->copy$default(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;ZILjava/lang/Object;)Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    move-result-object p1

    .line 209
    :goto_0
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->snapshot:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 210
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "just(newSnapshot)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onDepositSucceeded(Lcom/squareup/protos/deposits/CreateTransferResponse;)Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/deposits/CreateTransferResponse;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation

    .line 191
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->snapshot:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    .line 192
    sget-object v2, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->DEPOSIT_SUCCEEDED:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xa

    const/4 v7, 0x0

    move-object v4, p1

    .line 191
    invoke-static/range {v1 .. v7}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->copy$default(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;ZILjava/lang/Object;)Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    move-result-object p1

    .line 195
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->snapshot:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 196
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "just(newSnapshot)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onNotEligible(Lcom/squareup/receiving/ReceivedResponse;)Lio/reactivex/Single;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation

    .line 153
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    :goto_0
    move-object v2, p1

    goto :goto_1

    .line 154
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz p1, :cond_1

    .line 155
    sget p1, Lcom/squareup/common/strings/R$string;->instant_deposits_network_error_title:I

    .line 156
    sget v0, Lcom/squareup/common/strings/R$string;->instant_deposits_network_error_message:I

    .line 154
    invoke-direct {p0, p1, v0}, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->createErrorResponse(II)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    move-result-object p1

    goto :goto_0

    .line 159
    :cond_1
    sget p1, Lcom/squareup/common/strings/R$string;->instant_deposits_unavailable:I

    .line 160
    sget v0, Lcom/squareup/common/strings/R$string;->instant_deposits_unavailable_hint:I

    .line 158
    invoke-direct {p0, p1, v0}, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->createErrorResponse(II)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    move-result-object p1

    goto :goto_0

    .line 163
    :goto_1
    iget-object p1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->snapshot:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    .line 164
    sget-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->NOT_ELIGIBLE:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    .line 163
    invoke-static/range {v0 .. v6}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->copy$default(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;ZILjava/lang/Object;)Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    move-result-object p1

    .line 167
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->snapshot:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 168
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "just(newSnapshot)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onSendInstantDeposit()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation

    .line 172
    new-instance v0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;-><init>()V

    .line 173
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->idempotence_key(Ljava/lang/String;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    move-result-object v0

    .line 174
    iget-object v1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    const-string v2, "settings.userSettings"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    move-result-object v0

    .line 175
    iget-object v1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->snapshot:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    invoke-virtual {v1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->depositAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    move-result-object v0

    .line 176
    sget-object v1, Lcom/squareup/protos/deposits/BalanceActivityType;->INSTANT_PAYOUT:Lcom/squareup/protos/deposits/BalanceActivityType;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->balance_activity_type(Lcom/squareup/protos/deposits/BalanceActivityType;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    move-result-object v0

    .line 177
    invoke-virtual {v0}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->build()Lcom/squareup/protos/deposits/CreateTransferRequest;

    move-result-object v0

    .line 179
    iget-object v1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->transfersService:Lcom/squareup/balance/core/server/transfers/TransfersService;

    const-string v2, "request"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lcom/squareup/balance/core/server/transfers/TransfersService;->createTransfer(Lcom/squareup/protos/deposits/CreateTransferRequest;)Lcom/squareup/balance/core/server/transfers/TransfersService$CreateTransferStandardResponse;

    move-result-object v0

    .line 180
    invoke-virtual {v0}, Lcom/squareup/balance/core/server/transfers/TransfersService$CreateTransferStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 181
    new-instance v1, Lcom/squareup/instantdeposit/RealInstantDepositRunner$onSendInstantDeposit$1;

    invoke-direct {v1, p0}, Lcom/squareup/instantdeposit/RealInstantDepositRunner$onSendInstantDeposit$1;-><init>(Lcom/squareup/instantdeposit/RealInstantDepositRunner;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string/jumbo v1, "transfersService.createT\u2026ed)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public checkIfEligibleForInstantDeposit(Z)Lio/reactivex/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation

    .line 67
    invoke-direct {p0, p1}, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->onCheckEligibility(Z)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public instantDepositHint(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Ljava/lang/CharSequence;
    .locals 8

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    const-string v1, "settings.userSettings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v0

    .line 76
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->linkedCard()Lcom/squareup/protos/client/deposits/CardInfo;

    move-result-object v1

    .line 77
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->linkedBankAccount()Lcom/squareup/protos/client/bankaccount/BankAccount;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 80
    iget-object v4, v1, Lcom/squareup/protos/client/deposits/CardInfo;->supports_instant_deposit:Ljava/lang/Boolean;

    goto :goto_0

    :cond_0
    move-object v4, v3

    :goto_0
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    const-string v6, "LinkSpan.Builder(applica\u2026              .asPhrase()"

    const-string v7, "Phrase.from(res, instant\u2026settings)\n              )"

    if-eqz v4, :cond_2

    .line 81
    iget-object v2, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

    invoke-interface {v2}, Lcom/squareup/ui/settings/SettingsAppletGateway;->isInstantDepositsVisible()Z

    move-result v2

    const-string v3, "deposits_settings"

    if-eqz v2, :cond_1

    .line 82
    new-instance v2, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v4, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->application:Landroid/app/Application;

    check-cast v4, Landroid/content/Context;

    invoke-direct {v2, v4}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 83
    invoke-static {v0}, Lcom/squareup/instantdeposit/InstantDepositCountryResources;->instantDepositButtonHint(Lcom/squareup/CountryCode;)I

    move-result v0

    invoke-virtual {v2, v0, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 84
    sget v2, Lcom/squareup/common/strings/R$string;->deposits_settings:I

    invoke-virtual {v0, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 85
    new-instance v2, Lcom/squareup/instantdeposit/RealInstantDepositRunner$DepositsSettingsLinkSpan;

    invoke-direct {v2, p0}, Lcom/squareup/instantdeposit/RealInstantDepositRunner$DepositsSettingsLinkSpan;-><init>(Lcom/squareup/instantdeposit/RealInstantDepositRunner;)V

    check-cast v2, Lcom/squareup/ui/LinkSpan;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asPhrase()Lcom/squareup/phrase/Phrase;

    move-result-object v0

    invoke-static {v0, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 88
    :cond_1
    iget-object v2, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->res:Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/squareup/instantdeposit/InstantDepositCountryResources;->instantDepositButtonHint(Lcom/squareup/CountryCode;)I

    move-result v0

    invoke-static {v2, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 91
    iget-object v2, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->res:Landroid/content/res/Resources;

    sget v4, Lcom/squareup/common/strings/R$string;->deposits_settings:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 89
    invoke-virtual {v0, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    :goto_1
    iget-object v1, v1, Lcom/squareup/protos/client/deposits/CardInfo;->pan_suffix:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "pan_suffix"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    goto :goto_3

    :cond_2
    if-eqz v2, :cond_3

    .line 96
    iget-object v3, v2, Lcom/squareup/protos/client/bankaccount/BankAccount;->supports_instant_deposit:Ljava/lang/Boolean;

    :cond_3
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 97
    iget-object v1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

    invoke-interface {v1}, Lcom/squareup/ui/settings/SettingsAppletGateway;->isBankAccountsVisible()Z

    move-result v1

    const-string v3, "bank_account_settings"

    if-eqz v1, :cond_4

    .line 98
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v4, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->application:Landroid/app/Application;

    check-cast v4, Landroid/content/Context;

    invoke-direct {v1, v4}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 99
    invoke-static {v0}, Lcom/squareup/instantdeposit/InstantDepositCountryResources;->instantDepositButtonHint(Lcom/squareup/CountryCode;)I

    move-result v0

    invoke-virtual {v1, v0, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 100
    sget v1, Lcom/squareup/common/strings/R$string;->bank_account_settings:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 101
    new-instance v1, Lcom/squareup/instantdeposit/RealInstantDepositRunner$BankAccountSettingsLinkSpan;

    invoke-direct {v1, p0}, Lcom/squareup/instantdeposit/RealInstantDepositRunner$BankAccountSettingsLinkSpan;-><init>(Lcom/squareup/instantdeposit/RealInstantDepositRunner;)V

    check-cast v1, Lcom/squareup/ui/LinkSpan;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asPhrase()Lcom/squareup/phrase/Phrase;

    move-result-object v0

    invoke-static {v0, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 104
    :cond_4
    iget-object v1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->res:Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/squareup/instantdeposit/InstantDepositCountryResources;->instantDepositButtonHint(Lcom/squareup/CountryCode;)I

    move-result v0

    invoke-static {v1, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 107
    iget-object v1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->res:Landroid/content/res/Resources;

    sget v4, Lcom/squareup/common/strings/R$string;->bank_account_settings:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 105
    invoke-virtual {v0, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    :goto_2
    iget-object v1, v2, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_name:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    const-string v3, "bank_name"

    invoke-virtual {v0, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 112
    iget-object v2, v2, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_number_suffix:Ljava/lang/String;

    check-cast v2, Ljava/lang/CharSequence;

    const-string v3, "account_number_suffix"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    .line 118
    :goto_3
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->eligibilityDescription()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "balance_description"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 119
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "hintPhrase\n        .put(\u2026tion())\n        .format()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 114
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->eligibilityDescription()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method public onInstantDepositMade()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->onInstantDepositMade:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public sendInstantDeposit()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation

    .line 70
    invoke-direct {p0}, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->onSendInstantDeposit()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public setIsConfirmingInstantTransfer()V
    .locals 9

    .line 123
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->snapshot:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v2, v1

    check-cast v2, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x7

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->copy$default(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;ZILjava/lang/Object;)Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public snapshot()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->snapshot:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
