.class public final Lcom/squareup/logging/BluetoothAclConnectionReceiver;
.super Ljava/lang/Object;
.source "BluetoothAclConnectionReceiver.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\nH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/logging/BluetoothAclConnectionReceiver;",
        "Lmortar/Scoped;",
        "cardReaderListeners",
        "Lcom/squareup/cardreader/RealCardReaderListeners;",
        "bluetoothDevicesCountInitHelper",
        "Lcom/squareup/logging/BluetoothDevicesCountInitializer;",
        "application",
        "Landroid/app/Application;",
        "(Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/logging/BluetoothDevicesCountInitializer;Landroid/app/Application;)V",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final application:Landroid/app/Application;

.field private final bluetoothDevicesCountInitHelper:Lcom/squareup/logging/BluetoothDevicesCountInitializer;

.field private final cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/logging/BluetoothDevicesCountInitializer;Landroid/app/Application;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardReaderListeners"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bluetoothDevicesCountInitHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "application"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/logging/BluetoothAclConnectionReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    iput-object p2, p0, Lcom/squareup/logging/BluetoothAclConnectionReceiver;->bluetoothDevicesCountInitHelper:Lcom/squareup/logging/BluetoothDevicesCountInitializer;

    iput-object p3, p0, Lcom/squareup/logging/BluetoothAclConnectionReceiver;->application:Landroid/app/Application;

    return-void
.end method

.method public static final synthetic access$getCardReaderListeners$p(Lcom/squareup/logging/BluetoothAclConnectionReceiver;)Lcom/squareup/cardreader/RealCardReaderListeners;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/logging/BluetoothAclConnectionReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    return-object p0
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/logging/BluetoothAclConnectionReceiver;->bluetoothDevicesCountInitHelper:Lcom/squareup/logging/BluetoothDevicesCountInitializer;

    invoke-interface {v0}, Lcom/squareup/logging/BluetoothDevicesCountInitializer;->setupInitialConnectedDevices()V

    .line 24
    iget-object v0, p0, Lcom/squareup/logging/BluetoothAclConnectionReceiver;->application:Landroid/app/Application;

    check-cast v0, Landroid/content/Context;

    const-string v1, "android.bluetooth.device.action.ACL_CONNECTED"

    const-string v2, "android.bluetooth.device.action.ACL_DISCONNECTED"

    .line 26
    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    .line 24
    invoke-static {v0, v1}, Lcom/squareup/util/RxBroadcastReceiver;->registerForIntents(Landroid/content/Context;[Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/squareup/logging/BluetoothAclConnectionReceiver$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/logging/BluetoothAclConnectionReceiver$onEnterScope$1;-><init>(Lcom/squareup/logging/BluetoothAclConnectionReceiver;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "application.registerForI\u2026)\n        }\n      }\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
