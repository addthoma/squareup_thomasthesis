.class public final Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;
.super Ljava/lang/Object;
.source "ScheduledPollingIntervalProvider.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000 \n2\u00020\u0001:\u0001\nB\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;",
        "",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "pushServiceRegistrar",
        "Lcom/squareup/pushmessages/PushServiceRegistrar;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/pushmessages/PushServiceRegistrar;)V",
        "pollingInterval",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$Companion;

.field private static final DEFAULT_INTERVAL:Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;

.field private static final TIME_UNIT:Ljava/util/concurrent/TimeUnit;


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final pushServiceRegistrar:Lcom/squareup/pushmessages/PushServiceRegistrar;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->Companion:Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$Companion;

    .line 37
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sput-object v0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    .line 38
    new-instance v0, Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;

    sget-object v1, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3c

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;-><init>(JLjava/util/concurrent/TimeUnit;)V

    sput-object v0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->DEFAULT_INTERVAL:Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/pushmessages/PushServiceRegistrar;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountStatusSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pushServiceRegistrar"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p2, p0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->pushServiceRegistrar:Lcom/squareup/pushmessages/PushServiceRegistrar;

    return-void
.end method

.method public static final synthetic access$getDEFAULT_INTERVAL$cp()Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->DEFAULT_INTERVAL:Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;

    return-object v0
.end method

.method public static final synthetic access$getPushServiceRegistrar$p(Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;)Lcom/squareup/pushmessages/PushServiceRegistrar;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->pushServiceRegistrar:Lcom/squareup/pushmessages/PushServiceRegistrar;

    return-object p0
.end method

.method public static final synthetic access$getTIME_UNIT$cp()Ljava/util/concurrent/TimeUnit;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    return-object v0
.end method


# virtual methods
.method public final pollingInterval()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailableRx2()Lio/reactivex/Observable;

    move-result-object v0

    .line 22
    sget-object v1, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$pollingInterval$1;->INSTANCE:Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$pollingInterval$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 23
    new-instance v1, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$pollingInterval$2;

    invoke-direct {v1, p0}, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$pollingInterval$2;-><init>(Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "accountStatusSettings.se\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
