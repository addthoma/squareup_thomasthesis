.class final Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$pollingInterval$2;
.super Ljava/lang/Object;
.source "ScheduledPollingIntervalProvider.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->pollingInterval()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nScheduledPollingIntervalProvider.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ScheduledPollingIntervalProvider.kt\ncom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$pollingInterval$2\n*L\n1#1,47:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;",
        "it",
        "Lcom/squareup/settings/server/OrderHubSettings;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;


# direct methods
.method constructor <init>(Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$pollingInterval$2;->this$0:Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/settings/server/OrderHubSettings;)Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$pollingInterval$2;->this$0:Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;

    invoke-static {v0}, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->access$getPushServiceRegistrar$p(Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;)Lcom/squareup/pushmessages/PushServiceRegistrar;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/pushmessages/PushServiceRegistrar;->getPushServiceEnabled()Z

    move-result v0

    const-string v1, "Required value was null."

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/settings/server/OrderHubSettings;->getOrderHubSyncPeriodWithNotifications()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 27
    invoke-virtual {p1}, Lcom/squareup/settings/server/OrderHubSettings;->getOrderHubSyncPeriodWithNotifications()Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-static {}, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->access$getTIME_UNIT$cp()Ljava/util/concurrent/TimeUnit;

    move-result-object p1

    new-instance v2, Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;

    invoke-direct {v2, v0, v1, p1}, Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;-><init>(JLjava/util/concurrent/TimeUnit;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 28
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/settings/server/OrderHubSettings;->getOrderHubSyncPeriodWithoutNotifications()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 29
    invoke-virtual {p1}, Lcom/squareup/settings/server/OrderHubSettings;->getOrderHubSyncPeriodWithoutNotifications()Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_2

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-static {}, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->access$getTIME_UNIT$cp()Ljava/util/concurrent/TimeUnit;

    move-result-object p1

    new-instance v2, Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;

    invoke-direct {v2, v0, v1, p1}, Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;-><init>(JLjava/util/concurrent/TimeUnit;)V

    goto :goto_0

    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 31
    :cond_3
    sget-object p1, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->Companion:Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$Companion;

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$Companion;->getDEFAULT_INTERVAL()Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;

    move-result-object v2

    :goto_0
    return-object v2
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/settings/server/OrderHubSettings;

    invoke-virtual {p0, p1}, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$pollingInterval$2;->apply(Lcom/squareup/settings/server/OrderHubSettings;)Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;

    move-result-object p1

    return-object p1
.end method
