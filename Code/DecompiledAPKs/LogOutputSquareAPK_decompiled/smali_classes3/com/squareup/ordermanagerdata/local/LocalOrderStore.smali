.class public interface abstract Lcom/squareup/ordermanagerdata/local/LocalOrderStore;
.super Ljava/lang/Object;
.source "LocalOrderStore.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003H&J\u0014\u0010\u0006\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003H&J\u0008\u0010\u0007\u001a\u00020\u0008H&J\u0016\u0010\t\u001a\u00020\u00082\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH&J\u0014\u0010\r\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003H&J&\u0010\u000e\u001a\u00020\u00082\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H&J\u0014\u0010\u0014\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003H&J\u0014\u0010\u0015\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003H&J\u0014\u0010\u0016\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003H&J\u0010\u0010\u0017\u001a\u00020\u00082\u0006\u0010\u0018\u001a\u00020\u0005H&J\u001e\u0010\u0019\u001a\u00020\u00082\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u001b\u001a\u00020\u001cH&\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/ordermanagerdata/local/LocalOrderStore;",
        "",
        "activeOrders",
        "Lio/reactivex/Observable;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "completedOrders",
        "markAllOrdersAsKnown",
        "Lio/reactivex/Completable;",
        "markOrdersAsPrinted",
        "orderIds",
        "",
        "",
        "ordersFromMostRecentSearch",
        "saveSearchOrders",
        "orders",
        "query",
        "Lcom/squareup/ordermanagerdata/SearchQuery;",
        "isPaginationResult",
        "",
        "unknownOrders",
        "unprintedNewOrders",
        "upcomingOrders",
        "updateOrder",
        "order",
        "updateOrders",
        "updatedOrders",
        "updateMode",
        "Lcom/squareup/ordermanagerdata/local/UpdateMode;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract activeOrders()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract completedOrders()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract markAllOrdersAsKnown()Lio/reactivex/Completable;
.end method

.method public abstract markOrdersAsPrinted(Ljava/util/Set;)Lio/reactivex/Completable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation
.end method

.method public abstract ordersFromMostRecentSearch()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract saveSearchOrders(Ljava/util/List;Lcom/squareup/ordermanagerdata/SearchQuery;Z)Lio/reactivex/Completable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;",
            "Lcom/squareup/ordermanagerdata/SearchQuery;",
            "Z)",
            "Lio/reactivex/Completable;"
        }
    .end annotation
.end method

.method public abstract unknownOrders()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract unprintedNewOrders()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract upcomingOrders()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract updateOrder(Lcom/squareup/orders/model/Order;)Lio/reactivex/Completable;
.end method

.method public abstract updateOrders(Ljava/util/List;Lcom/squareup/ordermanagerdata/local/UpdateMode;)Lio/reactivex/Completable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;",
            "Lcom/squareup/ordermanagerdata/local/UpdateMode;",
            ")",
            "Lio/reactivex/Completable;"
        }
    .end annotation
.end method
