.class final Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markAllOrdersAsKnown$1;
.super Lkotlin/jvm/internal/Lambda;
.source "InMemoryLocalOrderStore.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->markAllOrdersAsKnown()Lio/reactivex/Completable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInMemoryLocalOrderStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InMemoryLocalOrderStore.kt\ncom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markAllOrdersAsKnown$1\n+ 2 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,528:1\n151#2,2:529\n*E\n*S KotlinDebug\n*F\n+ 1 InMemoryLocalOrderStore.kt\ncom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markAllOrdersAsKnown$1\n*L\n300#1,2:529\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;


# direct methods
.method constructor <init>(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markAllOrdersAsKnown$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 42
    invoke-virtual {p0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markAllOrdersAsKnown$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 300
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markAllOrdersAsKnown$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getOrders$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Ljava/util/Map;

    move-result-object v0

    .line 529
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    const/4 v2, 0x1

    .line 301
    invoke-virtual {v1, v2}, Lcom/squareup/ordermanagerdata/local/WrappedOrder;->setKnown(Z)V

    goto :goto_0

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markAllOrdersAsKnown$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getUnknownNewOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
