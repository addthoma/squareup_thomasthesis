.class final Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;
.super Lkotlin/jvm/internal/Lambda;
.source "InMemoryLocalOrderStore.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->updateOrder(Lcom/squareup/orders/model/Order;)Lio/reactivex/Completable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $order:Lcom/squareup/orders/model/Order;

.field final synthetic this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;


# direct methods
.method constructor <init>(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;Lcom/squareup/orders/model/Order;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    iput-object p2, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->$order:Lcom/squareup/orders/model/Order;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 42
    invoke-virtual {p0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 5

    .line 99
    sget-object v0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->Companion:Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;

    .line 100
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getOrders$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->$order:Lcom/squareup/orders/model/Order;

    iget-object v2, v2, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    .line 101
    iget-object v2, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->$order:Lcom/squareup/orders/model/Order;

    const/4 v3, 0x0

    .line 99
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;->create(Lcom/squareup/ordermanagerdata/local/WrappedOrder;Lcom/squareup/orders/model/Order;Z)Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getOrders$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->$order:Lcom/squareup/orders/model/Order;

    iget-object v2, v2, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    const-string v3, "order.id"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->getUpdatedWrappedOrder()Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->$order:Lcom/squareup/orders/model/Order;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/orders/OrderGroup;->ACTIVE:Lcom/squareup/protos/client/orders/OrderGroup;

    if-ne v1, v2, :cond_0

    .line 108
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getActiveOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->$order:Lcom/squareup/orders/model/Order;

    invoke-static {}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$getACTIVE_ORDER_COMPARATOR$p()Ljava/util/Comparator;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$acceptWithUpdatedOrder(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;Ljava/util/Comparator;)V

    goto :goto_0

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getActiveOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->$order:Lcom/squareup/orders/model/Order;

    invoke-static {v1, v2}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$acceptWithRemovedOrder(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;)V

    .line 113
    :goto_0
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->$order:Lcom/squareup/orders/model/Order;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/orders/OrderGroup;->UPCOMING:Lcom/squareup/protos/client/orders/OrderGroup;

    if-ne v1, v2, :cond_1

    .line 114
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getUpcomingOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->$order:Lcom/squareup/orders/model/Order;

    invoke-static {}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$getACTIVE_ORDER_COMPARATOR$p()Ljava/util/Comparator;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$acceptWithUpdatedOrder(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;Ljava/util/Comparator;)V

    goto :goto_1

    .line 116
    :cond_1
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getUpcomingOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->$order:Lcom/squareup/orders/model/Order;

    invoke-static {v1, v2}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$acceptWithRemovedOrder(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;)V

    .line 119
    :goto_1
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->$order:Lcom/squareup/orders/model/Order;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    if-ne v1, v2, :cond_2

    .line 120
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getCompletedOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->$order:Lcom/squareup/orders/model/Order;

    invoke-static {}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$getCOMPLETED_ORDER_COMPARATOR$p()Ljava/util/Comparator;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$acceptWithUpdatedOrder(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;Ljava/util/Comparator;)V

    goto :goto_2

    .line 122
    :cond_2
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getCompletedOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->$order:Lcom/squareup/orders/model/Order;

    invoke-static {v1, v2}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$acceptWithRemovedOrder(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;)V

    .line 125
    :goto_2
    invoke-virtual {v0}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnknownOrder()Z

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eqz v1, :cond_3

    .line 126
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getUnknownNewOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iget-object v4, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->$order:Lcom/squareup/orders/model/Order;

    invoke-static {v1, v4, v3, v2, v3}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->acceptWithUpdatedOrder$default(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;Ljava/util/Comparator;ILjava/lang/Object;)V

    .line 129
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnprintedNewOrder()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 130
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getUnprintedNewOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->$order:Lcom/squareup/orders/model/Order;

    invoke-static {v0, v1, v3, v2, v3}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->acceptWithUpdatedOrder$default(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;Ljava/util/Comparator;ILjava/lang/Object;)V

    .line 133
    :cond_4
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;->$order:Lcom/squareup/orders/model/Order;

    invoke-static {v0, v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$updateSearchResultsWithOrder(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;Lcom/squareup/orders/model/Order;)V

    return-void
.end method
