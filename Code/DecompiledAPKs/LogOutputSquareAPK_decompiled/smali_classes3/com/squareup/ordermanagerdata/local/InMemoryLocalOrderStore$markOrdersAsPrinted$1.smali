.class final Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markOrdersAsPrinted$1;
.super Lkotlin/jvm/internal/Lambda;
.source "InMemoryLocalOrderStore.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->markOrdersAsPrinted(Ljava/util/Set;)Lio/reactivex/Completable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInMemoryLocalOrderStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InMemoryLocalOrderStore.kt\ncom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markOrdersAsPrinted$1\n+ 2 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 3 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,528:1\n501#2:529\n486#2,6:530\n151#3,2:536\n*E\n*S KotlinDebug\n*F\n+ 1 InMemoryLocalOrderStore.kt\ncom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markOrdersAsPrinted$1\n*L\n314#1:529\n314#1,6:530\n315#1,2:536\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $orderIds:Ljava/util/Set;

.field final synthetic this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;


# direct methods
.method constructor <init>(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;Ljava/util/Set;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markOrdersAsPrinted$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    iput-object p2, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markOrdersAsPrinted$1;->$orderIds:Ljava/util/Set;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 42
    invoke-virtual {p0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markOrdersAsPrinted$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 6

    .line 311
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markOrdersAsPrinted$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getUnprintedNewOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 313
    :goto_0
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markOrdersAsPrinted$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getOrders$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Ljava/util/Map;

    move-result-object v1

    .line 529
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v2, Ljava/util/Map;

    .line 530
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 314
    iget-object v4, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markOrdersAsPrinted$1;->$orderIds:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 532
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 536
    :cond_2
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    const/4 v3, 0x1

    .line 316
    invoke-virtual {v2, v3}, Lcom/squareup/ordermanagerdata/local/WrappedOrder;->setPrinted(Z)V

    .line 317
    invoke-virtual {v2}, Lcom/squareup/ordermanagerdata/local/WrappedOrder;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$removeOrder(Ljava/util/List;Lcom/squareup/orders/model/Order;)V

    goto :goto_2

    .line 321
    :cond_3
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markOrdersAsPrinted$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getUnprintedNewOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
