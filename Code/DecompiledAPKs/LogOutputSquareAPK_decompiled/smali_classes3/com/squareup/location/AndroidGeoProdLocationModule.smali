.class public abstract Lcom/squareup/location/AndroidGeoProdLocationModule;
.super Ljava/lang/Object;
.source "AndroidGeoProdLocationModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideAddressProvider(Lcom/squareup/location/AndroidGeoAddressProvider;)Lcom/squareup/core/location/providers/AddressProvider;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideContinuousLocationMonitor(Lcom/squareup/location/AndroidGeoLocationMonitor;)Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideCountryCodeGuesser(Lcom/squareup/location/AndroidGeoCountryCodeGuesser;)Lcom/squareup/location/CountryCodeGuesser;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
