.class public final Lcom/squareup/intermission/IntermissionHelperKt;
.super Ljava/lang/Object;
.source "IntermissionHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\t\n\u0002\u0008\u0008\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a$\u0010\u0000\u001a\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0004\u0012\u00020\u00080\u0005*\u00020\u00082\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u0012\u0010\t\u001a\u00020\u0008*\u00020\u00072\u0006\u0010\n\u001a\u00020\u0008\u001a\n\u0010\t\u001a\u00020\u0008*\u00020\u0002\u001a\n\u0010\u000b\u001a\u00020\u0008*\u00020\u0007\u001a\n\u0010\u000b\u001a\u00020\u0008*\u00020\u0002\u001a\n\u0010\u000c\u001a\u00020\u0008*\u00020\u0007\u001a\n\u0010\u000c\u001a\u00020\u0008*\u00020\u0002\u001a\u0014\u0010\r\u001a\u00020\u0008*\u00020\u00082\u0006\u0010\u000e\u001a\u00020\u000fH\u0002\u001a\u001e\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u0005*\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u0008\u001a\u0012\u0010\u0010\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0008\u001a&\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u0005*\u00020\u00072\u0006\u0010\u0014\u001a\u00020\u00082\u0006\u0010\n\u001a\u00020\u0008\u001a\u0012\u0010\u0013\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0008\u001a&\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u0005*\u00020\u00072\u0006\u0010\u0016\u001a\u00020\u00082\u0006\u0010\n\u001a\u00020\u0008\u001a\u0012\u0010\u0015\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0008\u00a8\u0006\u0017"
    }
    d2 = {
        "gapTimeToggled",
        "",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;",
        "checked",
        "",
        "Lkotlin/Pair;",
        "",
        "Lcom/squareup/api/items/Intermission;",
        "Lorg/threeten/bp/Duration;",
        "getFinalDuration",
        "totalDuration",
        "getGapDuration",
        "getInitialDuration",
        "roundMinuteToNearest",
        "rounder",
        "",
        "updateFinalTime",
        "newFinalDuration",
        "newDuration",
        "updateGapTime",
        "newGapDuration",
        "updateInitialTime",
        "newInitialDuration",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final gapTimeToggled(Lorg/threeten/bp/Duration;Z)Lkotlin/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/Duration;",
            "Z)",
            "Lkotlin/Pair<",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;",
            "Lorg/threeten/bp/Duration;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$gapTimeToggled"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_1

    const-wide/16 v0, 0xf

    .line 56
    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMinutes(J)Lorg/threeten/bp/Duration;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/threeten/bp/Duration;->compareTo(Lorg/threeten/bp/Duration;)I

    move-result p1

    if-gez p1, :cond_0

    .line 57
    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMinutes(J)Lorg/threeten/bp/Duration;

    move-result-object p0

    :cond_0
    const-wide/16 v0, 0x3

    .line 62
    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/Duration;->dividedBy(J)Lorg/threeten/bp/Duration;

    move-result-object p1

    const-string v0, "newDuration\n        .dividedBy(3L)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x5

    .line 63
    invoke-static {p1, v0, v1}, Lcom/squareup/intermission/IntermissionHelperKt;->roundMinuteToNearest(Lorg/threeten/bp/Duration;J)Lorg/threeten/bp/Duration;

    move-result-object p1

    const-wide/16 v0, 0x2

    .line 64
    invoke-virtual {p1, v0, v1}, Lorg/threeten/bp/Duration;->multipliedBy(J)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 66
    new-instance v1, Lcom/squareup/api/items/Intermission$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/Intermission$Builder;-><init>()V

    .line 67
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v2

    long-to-int p1, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/api/items/Intermission$Builder;->start_offset(Ljava/lang/Integer;)Lcom/squareup/api/items/Intermission$Builder;

    move-result-object p1

    .line 68
    invoke-virtual {v0}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v0

    long-to-int v1, v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/Intermission$Builder;->end_offset(Ljava/lang/Integer;)Lcom/squareup/api/items/Intermission$Builder;

    move-result-object p1

    .line 69
    invoke-virtual {p1}, Lcom/squareup/api/items/Intermission$Builder;->build()Lcom/squareup/api/items/Intermission;

    move-result-object p1

    .line 70
    new-instance v0, Lkotlin/Pair;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1, p0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 72
    :cond_1
    new-instance v0, Lkotlin/Pair;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1, p0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-object v0
.end method

.method public static final gapTimeToggled(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Z)V
    .locals 2

    const-string v0, "$this$gapTimeToggled"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getDuration()J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object v0

    const-string v1, "Duration.ofMillis(duration)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/intermission/IntermissionHelperKt;->gapTimeToggled(Lorg/threeten/bp/Duration;Z)Lkotlin/Pair;

    move-result-object p1

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/threeten/bp/Duration;

    .line 133
    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setIntermissions(Ljava/util/List;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 134
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setDuration(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    return-void
.end method

.method public static final getFinalDuration(Lcom/squareup/api/items/Intermission;Lorg/threeten/bp/Duration;)Lorg/threeten/bp/Duration;
    .locals 2

    const-string v0, "$this$getFinalDuration"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "totalDuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object p0, p0, Lcom/squareup/api/items/Intermission;->end_offset:Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    int-to-long v0, p0

    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object p0

    invoke-virtual {p1, p0}, Lorg/threeten/bp/Duration;->minus(Lorg/threeten/bp/Duration;)Lorg/threeten/bp/Duration;

    move-result-object p0

    const-string/jumbo p1, "totalDuration.minus(Dura\u2026lis(end_offset.toLong()))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getFinalDuration(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lorg/threeten/bp/Duration;
    .locals 3

    const-string v0, "$this$getFinalDuration"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getIntermissions()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "intermissions[0]"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/api/items/Intermission;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getDuration()J

    move-result-wide v1

    invoke-static {v1, v2}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object p0

    const-string v1, "Duration.ofMillis(duration)"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p0}, Lcom/squareup/intermission/IntermissionHelperKt;->getFinalDuration(Lcom/squareup/api/items/Intermission;Lorg/threeten/bp/Duration;)Lorg/threeten/bp/Duration;

    move-result-object p0

    return-object p0
.end method

.method public static final getGapDuration(Lcom/squareup/api/items/Intermission;)Lorg/threeten/bp/Duration;
    .locals 4

    const-string v0, "$this$getGapDuration"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/api/items/Intermission;->end_offset:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    iget-object p0, p0, Lcom/squareup/api/items/Intermission;->start_offset:Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    int-to-long v2, p0

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object p0

    const-string v0, "Duration.ofMillis(end_of\u2026 - start_offset.toLong())"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getGapDuration(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lorg/threeten/bp/Duration;
    .locals 1

    const-string v0, "$this$getGapDuration"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getIntermissions()Ljava/util/List;

    move-result-object p0

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    const-string v0, "intermissions[0]"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lcom/squareup/api/items/Intermission;

    invoke-static {p0}, Lcom/squareup/intermission/IntermissionHelperKt;->getGapDuration(Lcom/squareup/api/items/Intermission;)Lorg/threeten/bp/Duration;

    move-result-object p0

    return-object p0
.end method

.method public static final getInitialDuration(Lcom/squareup/api/items/Intermission;)Lorg/threeten/bp/Duration;
    .locals 2

    const-string v0, "$this$getInitialDuration"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object p0, p0, Lcom/squareup/api/items/Intermission;->start_offset:Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    int-to-long v0, p0

    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object p0

    const-string v0, "Duration.ofMillis(start_offset.toLong())"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getInitialDuration(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lorg/threeten/bp/Duration;
    .locals 1

    const-string v0, "$this$getInitialDuration"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getIntermissions()Ljava/util/List;

    move-result-object p0

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    const-string v0, "intermissions[0]"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lcom/squareup/api/items/Intermission;

    invoke-static {p0}, Lcom/squareup/intermission/IntermissionHelperKt;->getInitialDuration(Lcom/squareup/api/items/Intermission;)Lorg/threeten/bp/Duration;

    move-result-object p0

    return-object p0
.end method

.method private static final roundMinuteToNearest(Lorg/threeten/bp/Duration;J)Lorg/threeten/bp/Duration;
    .locals 2

    .line 172
    invoke-virtual {p0}, Lorg/threeten/bp/Duration;->toMinutes()J

    move-result-wide v0

    div-long/2addr v0, p1

    mul-long v0, v0, p1

    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMinutes(J)Lorg/threeten/bp/Duration;

    move-result-object p0

    const-string p1, "Duration.ofMinutes((toMi\u2026s() / rounder) * rounder)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final updateFinalTime(Lcom/squareup/api/items/Intermission;Lorg/threeten/bp/Duration;)Lkotlin/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/Intermission;",
            "Lorg/threeten/bp/Duration;",
            ")",
            "Lkotlin/Pair<",
            "Lcom/squareup/api/items/Intermission;",
            "Lorg/threeten/bp/Duration;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$updateFinalTime"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newFinalDuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/api/items/Intermission;->end_offset:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/threeten/bp/Duration;->plus(Lorg/threeten/bp/Duration;)Lorg/threeten/bp/Duration;

    move-result-object p1

    .line 115
    new-instance v0, Lkotlin/Pair;

    invoke-direct {v0, p0, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static final updateFinalTime(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lorg/threeten/bp/Duration;)V
    .locals 2

    const-string v0, "$this$updateFinalTime"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newDuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getIntermissions()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "intermissions[0]"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/api/items/Intermission;

    invoke-static {v0, p1}, Lcom/squareup/intermission/IntermissionHelperKt;->updateFinalTime(Lcom/squareup/api/items/Intermission;Lorg/threeten/bp/Duration;)Lkotlin/Pair;

    move-result-object p1

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/threeten/bp/Duration;

    .line 157
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setDuration(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    return-void
.end method

.method public static final updateGapTime(Lcom/squareup/api/items/Intermission;Lorg/threeten/bp/Duration;Lorg/threeten/bp/Duration;)Lkotlin/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/Intermission;",
            "Lorg/threeten/bp/Duration;",
            "Lorg/threeten/bp/Duration;",
            ")",
            "Lkotlin/Pair<",
            "Lcom/squareup/api/items/Intermission;",
            "Lorg/threeten/bp/Duration;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$updateGapTime"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newGapDuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "totalDuration"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/squareup/api/items/Intermission;->end_offset:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lcom/squareup/api/items/Intermission;->start_offset:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/Duration;->minus(Lorg/threeten/bp/Duration;)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 102
    invoke-virtual {p1, v0}, Lorg/threeten/bp/Duration;->minus(Lorg/threeten/bp/Duration;)Lorg/threeten/bp/Duration;

    move-result-object p1

    .line 103
    new-instance v0, Lcom/squareup/api/items/Intermission$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Intermission$Builder;-><init>()V

    .line 104
    iget-object v1, p0, Lcom/squareup/api/items/Intermission;->start_offset:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Intermission$Builder;->start_offset(Ljava/lang/Integer;)Lcom/squareup/api/items/Intermission$Builder;

    move-result-object v0

    .line 105
    iget-object p0, p0, Lcom/squareup/api/items/Intermission;->end_offset:Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v1

    long-to-int v2, v1

    add-int/2addr p0, v2

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/api/items/Intermission$Builder;->end_offset(Ljava/lang/Integer;)Lcom/squareup/api/items/Intermission$Builder;

    move-result-object p0

    .line 106
    invoke-virtual {p0}, Lcom/squareup/api/items/Intermission$Builder;->build()Lcom/squareup/api/items/Intermission;

    move-result-object p0

    .line 107
    invoke-virtual {p2, p1}, Lorg/threeten/bp/Duration;->plus(Lorg/threeten/bp/Duration;)Lorg/threeten/bp/Duration;

    move-result-object p1

    .line 109
    new-instance p2, Lkotlin/Pair;

    invoke-direct {p2, p0, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p2
.end method

.method public static final updateGapTime(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lorg/threeten/bp/Duration;)V
    .locals 3

    const-string v0, "$this$updateGapTime"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newDuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getIntermissions()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "intermissions[0]"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/api/items/Intermission;

    .line 148
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getDuration()J

    move-result-wide v1

    invoke-static {v1, v2}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object v1

    const-string v2, "Duration.ofMillis(duration)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    invoke-static {v0, p1, v1}, Lcom/squareup/intermission/IntermissionHelperKt;->updateGapTime(Lcom/squareup/api/items/Intermission;Lorg/threeten/bp/Duration;Lorg/threeten/bp/Duration;)Lkotlin/Pair;

    move-result-object p1

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Intermission;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/threeten/bp/Duration;

    .line 151
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setIntermissions(Ljava/util/List;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 152
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setDuration(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    return-void
.end method

.method public static final updateInitialTime(Lcom/squareup/api/items/Intermission;Lorg/threeten/bp/Duration;Lorg/threeten/bp/Duration;)Lkotlin/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/Intermission;",
            "Lorg/threeten/bp/Duration;",
            "Lorg/threeten/bp/Duration;",
            ")",
            "Lkotlin/Pair<",
            "Lcom/squareup/api/items/Intermission;",
            "Lorg/threeten/bp/Duration;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$updateInitialTime"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newInitialDuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "totalDuration"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/api/items/Intermission;->start_offset:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 81
    invoke-virtual {p1, v0}, Lorg/threeten/bp/Duration;->minus(Lorg/threeten/bp/Duration;)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 83
    iget-object p0, p0, Lcom/squareup/api/items/Intermission;->end_offset:Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    int-to-long v1, p0

    invoke-static {v1, v2}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object p0

    .line 84
    invoke-virtual {p0, v0}, Lorg/threeten/bp/Duration;->plus(Lorg/threeten/bp/Duration;)Lorg/threeten/bp/Duration;

    move-result-object p0

    .line 86
    invoke-virtual {p2, v0}, Lorg/threeten/bp/Duration;->plus(Lorg/threeten/bp/Duration;)Lorg/threeten/bp/Duration;

    move-result-object p2

    .line 88
    new-instance v0, Lcom/squareup/api/items/Intermission$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Intermission$Builder;-><init>()V

    .line 89
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v1

    long-to-int p1, v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Intermission$Builder;->start_offset(Ljava/lang/Integer;)Lcom/squareup/api/items/Intermission$Builder;

    move-result-object p1

    .line 90
    invoke-virtual {p0}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v0

    long-to-int p0, v0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/squareup/api/items/Intermission$Builder;->end_offset(Ljava/lang/Integer;)Lcom/squareup/api/items/Intermission$Builder;

    move-result-object p0

    .line 91
    invoke-virtual {p0}, Lcom/squareup/api/items/Intermission$Builder;->build()Lcom/squareup/api/items/Intermission;

    move-result-object p0

    .line 93
    new-instance p1, Lkotlin/Pair;

    invoke-direct {p1, p0, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method

.method public static final updateInitialTime(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lorg/threeten/bp/Duration;)V
    .locals 3

    const-string v0, "$this$updateInitialTime"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newDuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getIntermissions()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "intermissions[0]"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/api/items/Intermission;

    .line 139
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getDuration()J

    move-result-wide v1

    invoke-static {v1, v2}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object v1

    const-string v2, "Duration.ofMillis(duration)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    invoke-static {v0, p1, v1}, Lcom/squareup/intermission/IntermissionHelperKt;->updateInitialTime(Lcom/squareup/api/items/Intermission;Lorg/threeten/bp/Duration;Lorg/threeten/bp/Duration;)Lkotlin/Pair;

    move-result-object p1

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Intermission;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/threeten/bp/Duration;

    .line 142
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setIntermissions(Ljava/util/List;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 143
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setDuration(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    return-void
.end method
