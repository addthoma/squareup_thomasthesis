.class public final Lcom/squareup/jailkeeper/JailScreenConfiguration$NoNetworkSettingsButton;
.super Ljava/lang/Object;
.source "JailScreenConfiguration.kt"

# interfaces
.implements Lcom/squareup/jailkeeper/JailScreenConfiguration;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/jailkeeper/JailScreenConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoNetworkSettingsButton"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/jailkeeper/JailScreenConfiguration$NoNetworkSettingsButton;",
        "Lcom/squareup/jailkeeper/JailScreenConfiguration;",
        "()V",
        "networkSettingsIntent",
        "Landroid/content/Intent;",
        "shouldShowNetworkSettingsButton",
        "",
        "jail_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public networkSettingsIntent()Landroid/content/Intent;
    .locals 1

    .line 21
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public shouldShowNetworkSettingsButton()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
