.class public final Lcom/squareup/ordermagstripe/OrderMagstripeModule_ProvideOrderCoodinatorConfigurationFactory;
.super Ljava/lang/Object;
.source "OrderMagstripeModule_ProvideOrderCoodinatorConfigurationFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ordermagstripe/OrderMagstripeModule_ProvideOrderCoodinatorConfigurationFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/mailorder/OrderCoordinator$Configuration;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/ordermagstripe/OrderMagstripeModule_ProvideOrderCoodinatorConfigurationFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/ordermagstripe/OrderMagstripeModule_ProvideOrderCoodinatorConfigurationFactory$InstanceHolder;->access$000()Lcom/squareup/ordermagstripe/OrderMagstripeModule_ProvideOrderCoodinatorConfigurationFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideOrderCoodinatorConfiguration()Lcom/squareup/mailorder/OrderCoordinator$Configuration;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/ordermagstripe/OrderMagstripeModule;->provideOrderCoodinatorConfiguration()Lcom/squareup/mailorder/OrderCoordinator$Configuration;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mailorder/OrderCoordinator$Configuration;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/mailorder/OrderCoordinator$Configuration;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/ordermagstripe/OrderMagstripeModule_ProvideOrderCoodinatorConfigurationFactory;->provideOrderCoodinatorConfiguration()Lcom/squareup/mailorder/OrderCoordinator$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ordermagstripe/OrderMagstripeModule_ProvideOrderCoodinatorConfigurationFactory;->get()Lcom/squareup/mailorder/OrderCoordinator$Configuration;

    move-result-object v0

    return-object v0
.end method
