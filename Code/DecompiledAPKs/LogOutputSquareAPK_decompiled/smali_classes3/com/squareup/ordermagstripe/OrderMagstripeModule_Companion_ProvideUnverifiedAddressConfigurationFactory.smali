.class public final Lcom/squareup/ordermagstripe/OrderMagstripeModule_Companion_ProvideUnverifiedAddressConfigurationFactory;
.super Ljava/lang/Object;
.source "OrderMagstripeModule_Companion_ProvideUnverifiedAddressConfigurationFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ordermagstripe/OrderMagstripeModule_Companion_ProvideUnverifiedAddressConfigurationFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/ordermagstripe/OrderMagstripeModule_Companion_ProvideUnverifiedAddressConfigurationFactory;
    .locals 1

    .line 24
    invoke-static {}, Lcom/squareup/ordermagstripe/OrderMagstripeModule_Companion_ProvideUnverifiedAddressConfigurationFactory$InstanceHolder;->access$000()Lcom/squareup/ordermagstripe/OrderMagstripeModule_Companion_ProvideUnverifiedAddressConfigurationFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideUnverifiedAddressConfiguration()Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;
    .locals 2

    .line 28
    sget-object v0, Lcom/squareup/ordermagstripe/OrderMagstripeModule;->Companion:Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;

    invoke-virtual {v0}, Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;->provideUnverifiedAddressConfiguration()Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/ordermagstripe/OrderMagstripeModule_Companion_ProvideUnverifiedAddressConfigurationFactory;->provideUnverifiedAddressConfiguration()Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ordermagstripe/OrderMagstripeModule_Companion_ProvideUnverifiedAddressConfigurationFactory;->get()Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;

    move-result-object v0

    return-object v0
.end method
