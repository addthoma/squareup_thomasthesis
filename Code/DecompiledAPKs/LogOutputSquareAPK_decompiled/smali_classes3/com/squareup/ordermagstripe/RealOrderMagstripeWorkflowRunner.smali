.class public final Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;
.super Lcom/squareup/container/SimpleWorkflowRunner;
.source "RealOrderMagstripeWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$StarterAdapter;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u00020\u0004:\u0001\u0015B5\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u000c\u0010\t\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0014J\u0008\u0010\u0014\u001a\u00020\u0011H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;",
        "Lcom/squareup/container/SimpleWorkflowRunner;",
        "",
        "Lcom/squareup/mailorder/OrderWorkflowResult;",
        "Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner;",
        "viewFactoryFactory",
        "Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;",
        "orderWorkflowStarter",
        "Lcom/squareup/mailorder/OrderScreenWorkflowStarter;",
        "section",
        "Ljava/lang/Class;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "setupGuideIntegrationRunner",
        "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
        "(Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;Lcom/squareup/mailorder/OrderScreenWorkflowStarter;Ljava/lang/Class;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/setupguide/SetupGuideIntegrationRunner;)V",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "startWorkflow",
        "StarterAdapter",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final setupGuideIntegrationRunner:Lcom/squareup/setupguide/SetupGuideIntegrationRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;Lcom/squareup/mailorder/OrderScreenWorkflowStarter;Ljava/lang/Class;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/setupguide/SetupGuideIntegrationRunner;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;",
            "Lcom/squareup/mailorder/OrderScreenWorkflowStarter;",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "viewFactoryFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderWorkflowStarter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "setupGuideIntegrationRunner"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    sget-object v0, Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner;->Companion:Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v2

    .line 51
    invoke-interface {p4}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 52
    invoke-virtual {p1, p3}, Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;->create(Ljava/lang/Class;)Lcom/squareup/mailorder/OrderWorkflowViewFactory;

    move-result-object p1

    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    .line 53
    new-instance p1, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$StarterAdapter;

    invoke-direct {p1, p2}, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$StarterAdapter;-><init>(Lcom/squareup/mailorder/OrderScreenWorkflowStarter;)V

    move-object v5, p1

    check-cast v5, Lcom/squareup/container/PosWorkflowStarter;

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, p0

    .line 49
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/SimpleWorkflowRunner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;Lcom/squareup/container/PosWorkflowStarter;Lkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p4, p0, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p5, p0, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;->setupGuideIntegrationRunner:Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-object p0
.end method

.method public static final synthetic access$getSetupGuideIntegrationRunner$p(Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;)Lcom/squareup/setupguide/SetupGuideIntegrationRunner;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;->setupGuideIntegrationRunner:Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    return-object p0
.end method


# virtual methods
.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-super {p0, p1}, Lcom/squareup/container/SimpleWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V

    .line 68
    invoke-virtual {p0}, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onUpdateScreens().subscribe(container::pushStack)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 71
    invoke-virtual {p0}, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    .line 72
    new-instance v1, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMapCompletable(Lio/reactivex/functions/Function;)Lio/reactivex/Completable;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lio/reactivex/Completable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onResult()\n        .flat\u2026   }\n        .subscribe()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public startWorkflow()V
    .locals 0

    .line 57
    invoke-virtual {p0}, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;->ensureWorkflow()V

    return-void
.end method
