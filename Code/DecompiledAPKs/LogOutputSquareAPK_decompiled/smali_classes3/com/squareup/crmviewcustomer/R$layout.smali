.class public final Lcom/squareup/crmviewcustomer/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crmviewcustomer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final crm_bill_history_card_view:I = 0x7f0d0115

.field public static final crm_customer_all_appointments_view:I = 0x7f0d0129

.field public static final crm_customer_invoice_view:I = 0x7f0d012c

.field public static final crm_frequent_item_section_row:I = 0x7f0d0144

.field public static final crm_frequent_items_section_view:I = 0x7f0d0145

.field public static final crm_notes_section_view_v2:I = 0x7f0d014f

.field public static final crm_review_customer_for_merging_view:I = 0x7f0d015a

.field public static final crm_update_loyalty_phone:I = 0x7f0d0167

.field public static final crm_v2_activity_list_section:I = 0x7f0d0168

.field public static final crm_v2_choose_customer2_view:I = 0x7f0d016b

.field public static final crm_v2_choose_customer3_view:I = 0x7f0d016c

.field public static final crm_v2_choose_customer_to_save_card_view:I = 0x7f0d016e

.field public static final crm_v2_loyalty_section_dropdown_dialog:I = 0x7f0d0185

.field public static final crm_v2_loyalty_section_view:I = 0x7f0d0186

.field public static final crm_v2_profile_rows_and_button_section:I = 0x7f0d0190

.field public static final crm_v2_profile_section_card_on_file:I = 0x7f0d0191

.field public static final crm_v2_profile_smartlinerow:I = 0x7f0d0193

.field public static final crm_v2_rewards_section_view:I = 0x7f0d0194

.field public static final crm_v2_simple_section:I = 0x7f0d0196

.field public static final crm_v2_smartlinerow_list_row:I = 0x7f0d0197

.field public static final crm_v2_view_customer:I = 0x7f0d019b

.field public static final crm_v2_view_customer_drop_down_container:I = 0x7f0d019c

.field public static final crm_v2_view_customer_drop_down_content:I = 0x7f0d019d


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
