.class Lcom/squareup/feetutorial/RatesTourView$1;
.super Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;
.source "RatesTourView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/feetutorial/RatesTourView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/feetutorial/RatesTourView;


# direct methods
.method constructor <init>(Lcom/squareup/feetutorial/RatesTourView;)V
    .locals 0

    .line 78
    iput-object p1, p0, Lcom/squareup/feetutorial/RatesTourView$1;->this$0:Lcom/squareup/feetutorial/RatesTourView;

    invoke-direct {p0}, Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView$1;->this$0:Lcom/squareup/feetutorial/RatesTourView;

    iget-object v0, v0, Lcom/squareup/feetutorial/RatesTourView;->presenter:Lcom/squareup/feetutorial/RatesTourScreen$Presenter;

    iget-object v1, p0, Lcom/squareup/feetutorial/RatesTourView$1;->this$0:Lcom/squareup/feetutorial/RatesTourView;

    invoke-static {v1}, Lcom/squareup/feetutorial/RatesTourView;->access$000(Lcom/squareup/feetutorial/RatesTourView;)Lcom/squareup/feetutorial/RateTourAdapter;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/feetutorial/RateTourAdapter;->pages:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/feetutorial/RatePage;

    iget-object p1, p1, Lcom/squareup/feetutorial/RatePage;->category:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v0, p1}, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->setCurrentCategory(Lcom/squareup/feetutorial/RateCategory;)V

    return-void
.end method
