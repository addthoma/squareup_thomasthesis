.class Lcom/squareup/feetutorial/RateTours;
.super Ljava/lang/Object;
.source "RateTours.java"


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/feetutorial/RateTours;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 32
    iput-object p2, p0, Lcom/squareup/feetutorial/RateTours;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method private buildItems()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/feetutorial/RatePage;",
            ">;"
        }
    .end annotation

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 42
    iget-object v1, p0, Lcom/squareup/feetutorial/RateTours;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {v1}, Lcom/squareup/feetutorial/RateCategory;->categories(Lcom/squareup/settings/server/AccountStatusSettings;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/feetutorial/RateCategory;

    .line 43
    invoke-virtual {p0, v2}, Lcom/squareup/feetutorial/RateTours;->create(Lcom/squareup/feetutorial/RateCategory;)Lcom/squareup/feetutorial/RatePage;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private static createCnpPage(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;)Lcom/squareup/feetutorial/RatePage;
    .locals 4

    .line 112
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    .line 113
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->getCnpFee()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v1

    .line 115
    sget-object v2, Lcom/squareup/feetutorial/RateCategory;->MANUAL:Lcom/squareup/feetutorial/RateCategory;

    sget-object v3, Lcom/squareup/feetutorial/RateCategory;->MANUAL:Lcom/squareup/feetutorial/RateCategory;

    invoke-static {v3, v0}, Lcom/squareup/feetutorial/RateImages;->imageFor(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/protos/common/CurrencyCode;)I

    move-result v0

    sget-object v3, Lcom/squareup/feetutorial/RateCategory;->MANUAL:Lcom/squareup/feetutorial/RateCategory;

    .line 116
    invoke-static {v3, p0}, Lcom/squareup/feetutorial/RateText;->textFor(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RateText;

    move-result-object p0

    .line 115
    invoke-static {v2, p1, v0, p0, v1}, Lcom/squareup/feetutorial/RatePage;->pageMaybeBottomCropOrCenter(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/util/Device;ILcom/squareup/feetutorial/RateText;Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/feetutorial/RatePage;

    move-result-object p0

    return-object p0
.end method

.method private static createDipR12Page(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RatePage;
    .locals 4

    .line 92
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    .line 93
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->getCpFee()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v1

    .line 94
    sget-object v2, Lcom/squareup/feetutorial/RateCategory;->DIP_R12:Lcom/squareup/feetutorial/RateCategory;

    sget-object v3, Lcom/squareup/feetutorial/RateCategory;->DIP_R12:Lcom/squareup/feetutorial/RateCategory;

    invoke-static {v3, v0}, Lcom/squareup/feetutorial/RateImages;->imageFor(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/protos/common/CurrencyCode;)I

    move-result v0

    sget-object v3, Lcom/squareup/feetutorial/RateCategory;->DIP_R12:Lcom/squareup/feetutorial/RateCategory;

    invoke-static {v3, p0}, Lcom/squareup/feetutorial/RateText;->textFor(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RateText;

    move-result-object p0

    invoke-static {v2, v0, p0, v1}, Lcom/squareup/feetutorial/RatePage;->pageCenter(Lcom/squareup/feetutorial/RateCategory;ILcom/squareup/feetutorial/RateText;Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/feetutorial/RatePage;

    move-result-object p0

    return-object p0
.end method

.method private static createDipR6JCBPage(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RatePage;
    .locals 4

    .line 84
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    .line 86
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->getJCBFee()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v1

    .line 87
    sget-object v2, Lcom/squareup/feetutorial/RateCategory;->DIP_R6_JCB:Lcom/squareup/feetutorial/RateCategory;

    sget-object v3, Lcom/squareup/feetutorial/RateCategory;->DIP_R6_JCB:Lcom/squareup/feetutorial/RateCategory;

    invoke-static {v3, v0}, Lcom/squareup/feetutorial/RateImages;->imageFor(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/protos/common/CurrencyCode;)I

    move-result v0

    sget-object v3, Lcom/squareup/feetutorial/RateCategory;->DIP_R6_JCB:Lcom/squareup/feetutorial/RateCategory;

    invoke-static {v3, p0}, Lcom/squareup/feetutorial/RateText;->textFor(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RateText;

    move-result-object p0

    invoke-static {v2, v0, p0, v1}, Lcom/squareup/feetutorial/RatePage;->pageBottom(Lcom/squareup/feetutorial/RateCategory;ILcom/squareup/feetutorial/RateText;Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/feetutorial/RatePage;

    move-result-object p0

    return-object p0
.end method

.method private static createDipR6Page(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RatePage;
    .locals 4

    .line 78
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    .line 79
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->getCpFee()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v1

    .line 80
    sget-object v2, Lcom/squareup/feetutorial/RateCategory;->DIP_R6:Lcom/squareup/feetutorial/RateCategory;

    sget-object v3, Lcom/squareup/feetutorial/RateCategory;->DIP_R6:Lcom/squareup/feetutorial/RateCategory;

    invoke-static {v3, v0}, Lcom/squareup/feetutorial/RateImages;->imageFor(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/protos/common/CurrencyCode;)I

    move-result v0

    sget-object v3, Lcom/squareup/feetutorial/RateCategory;->DIP_R6:Lcom/squareup/feetutorial/RateCategory;

    invoke-static {v3, p0}, Lcom/squareup/feetutorial/RateText;->textFor(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RateText;

    move-result-object p0

    invoke-static {v2, v0, p0, v1}, Lcom/squareup/feetutorial/RatePage;->pageBottom(Lcom/squareup/feetutorial/RateCategory;ILcom/squareup/feetutorial/RateText;Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/feetutorial/RatePage;

    move-result-object p0

    return-object p0
.end method

.method private static createSwipePage(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RatePage;
    .locals 4

    .line 120
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    .line 121
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->getCpFee()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v1

    .line 123
    sget-object v2, Lcom/squareup/feetutorial/RateCategory;->SWIPE:Lcom/squareup/feetutorial/RateCategory;

    sget-object v3, Lcom/squareup/feetutorial/RateCategory;->SWIPE:Lcom/squareup/feetutorial/RateCategory;

    invoke-static {v3, v0}, Lcom/squareup/feetutorial/RateImages;->imageFor(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/protos/common/CurrencyCode;)I

    move-result v0

    sget-object v3, Lcom/squareup/feetutorial/RateCategory;->SWIPE:Lcom/squareup/feetutorial/RateCategory;

    invoke-static {v3, p0}, Lcom/squareup/feetutorial/RateText;->textFor(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RateText;

    move-result-object p0

    invoke-static {v2, v0, p0, v1}, Lcom/squareup/feetutorial/RatePage;->pageBottom(Lcom/squareup/feetutorial/RateCategory;ILcom/squareup/feetutorial/RateText;Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/feetutorial/RatePage;

    move-result-object p0

    return-object p0
.end method

.method private static createTapInteracPage(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RatePage;
    .locals 4

    .line 98
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    .line 99
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->getInteracFee()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v1

    .line 100
    sget-object v2, Lcom/squareup/feetutorial/RateCategory;->TAP_INTERAC:Lcom/squareup/feetutorial/RateCategory;

    sget-object v3, Lcom/squareup/feetutorial/RateCategory;->TAP_INTERAC:Lcom/squareup/feetutorial/RateCategory;

    invoke-static {v3, v0}, Lcom/squareup/feetutorial/RateImages;->imageFor(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/protos/common/CurrencyCode;)I

    move-result v0

    sget-object v3, Lcom/squareup/feetutorial/RateCategory;->TAP_INTERAC:Lcom/squareup/feetutorial/RateCategory;

    invoke-static {v3, p0}, Lcom/squareup/feetutorial/RateText;->textFor(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RateText;

    move-result-object p0

    invoke-static {v2, v0, p0, v1}, Lcom/squareup/feetutorial/RatePage;->pageBottom(Lcom/squareup/feetutorial/RateCategory;ILcom/squareup/feetutorial/RateText;Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/feetutorial/RatePage;

    move-result-object p0

    return-object p0
.end method

.method private static createTapPage(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;)Lcom/squareup/feetutorial/RatePage;
    .locals 4

    .line 105
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    .line 106
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->getCpFee()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v1

    .line 107
    sget-object v2, Lcom/squareup/feetutorial/RateCategory;->TAP:Lcom/squareup/feetutorial/RateCategory;

    sget-object v3, Lcom/squareup/feetutorial/RateCategory;->TAP:Lcom/squareup/feetutorial/RateCategory;

    invoke-static {v3, v0}, Lcom/squareup/feetutorial/RateImages;->imageFor(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/protos/common/CurrencyCode;)I

    move-result v0

    sget-object v3, Lcom/squareup/feetutorial/RateCategory;->TAP:Lcom/squareup/feetutorial/RateCategory;

    .line 108
    invoke-static {v3, p0}, Lcom/squareup/feetutorial/RateText;->textFor(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RateText;

    move-result-object p0

    .line 107
    invoke-static {v2, p1, v0, p0, v1}, Lcom/squareup/feetutorial/RatePage;->pageMaybeBottomCropOrCenter(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/util/Device;ILcom/squareup/feetutorial/RateText;Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/feetutorial/RatePage;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method create(Lcom/squareup/feetutorial/RateCategory;)Lcom/squareup/feetutorial/RatePage;
    .locals 3

    .line 50
    sget-object v0, Lcom/squareup/feetutorial/RateTours$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    invoke-virtual {p1}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 73
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected category: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/feetutorial/RateTours;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {p1}, Lcom/squareup/feetutorial/RateTours;->createSwipePage(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RatePage;

    move-result-object p1

    return-object p1

    .line 67
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/feetutorial/RateTours;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/feetutorial/RateTours;->device:Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/feetutorial/RateTours;->createCnpPage(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;)Lcom/squareup/feetutorial/RatePage;

    move-result-object p1

    return-object p1

    .line 64
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/feetutorial/RateTours;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {p1}, Lcom/squareup/feetutorial/RateTours;->createTapInteracPage(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RatePage;

    move-result-object p1

    return-object p1

    .line 61
    :pswitch_3
    iget-object p1, p0, Lcom/squareup/feetutorial/RateTours;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/feetutorial/RateTours;->device:Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/feetutorial/RateTours;->createTapPage(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;)Lcom/squareup/feetutorial/RatePage;

    move-result-object p1

    return-object p1

    .line 58
    :pswitch_4
    iget-object p1, p0, Lcom/squareup/feetutorial/RateTours;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {p1}, Lcom/squareup/feetutorial/RateTours;->createDipR12Page(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RatePage;

    move-result-object p1

    return-object p1

    .line 55
    :pswitch_5
    iget-object p1, p0, Lcom/squareup/feetutorial/RateTours;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {p1}, Lcom/squareup/feetutorial/RateTours;->createDipR6JCBPage(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RatePage;

    move-result-object p1

    return-object p1

    .line 52
    :pswitch_6
    iget-object p1, p0, Lcom/squareup/feetutorial/RateTours;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {p1}, Lcom/squareup/feetutorial/RateTours;->createDipR6Page(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RatePage;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method pages()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/feetutorial/RatePage;",
            ">;>;"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Lcom/squareup/feetutorial/RateTours;->buildItems()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
