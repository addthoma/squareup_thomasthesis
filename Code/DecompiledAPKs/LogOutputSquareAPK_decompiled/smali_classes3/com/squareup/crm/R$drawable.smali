.class public final Lcom/squareup/crm/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final conversation_bubble_left:I = 0x7f0800fc

.field public static final conversation_bubble_right:I = 0x7f0800fd

.field public static final crm_action_arrow:I = 0x7f080102

.field public static final crm_action_close:I = 0x7f080103

.field public static final crm_action_overflow:I = 0x7f080104

.field public static final crm_appointment_cancelled:I = 0x7f080105

.field public static final crm_appointment_no_show:I = 0x7f080106

.field public static final crm_card_divider:I = 0x7f080107

.field public static final crm_file_document:I = 0x7f080108

.field public static final crm_file_image:I = 0x7f080109

.field public static final crm_initial_circle_bg:I = 0x7f08010a

.field public static final crm_payment_amex:I = 0x7f08010c

.field public static final crm_payment_dinersclub:I = 0x7f08010d

.field public static final crm_payment_discover:I = 0x7f08010e

.field public static final crm_payment_generic:I = 0x7f08010f

.field public static final crm_payment_giftcard:I = 0x7f080110

.field public static final crm_payment_interac:I = 0x7f080111

.field public static final crm_payment_jcb:I = 0x7f080112

.field public static final crm_payment_mastercard:I = 0x7f080113

.field public static final crm_payment_unionpay:I = 0x7f080114

.field public static final crm_payment_visa:I = 0x7f080115


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
