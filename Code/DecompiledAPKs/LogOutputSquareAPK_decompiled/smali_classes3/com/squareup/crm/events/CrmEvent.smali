.class public Lcom/squareup/crm/events/CrmEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "CrmEvent.java"


# instance fields
.field public final crm_register_event:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterActionName;)V
    .locals 1

    const-string v0, "crm_register"

    .line 10
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    .line 11
    iget-object p1, p1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/crm/events/CrmEvent;->crm_register_event:Ljava/lang/String;

    return-void
.end method
