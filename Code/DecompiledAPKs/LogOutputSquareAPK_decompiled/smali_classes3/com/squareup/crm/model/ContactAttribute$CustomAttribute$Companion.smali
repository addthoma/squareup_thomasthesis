.class public final Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;
.super Ljava/lang/Object;
.source "ContactAttribute.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nContactAttribute.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ContactAttribute.kt\ncom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion\n*L\n1#1,322:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0002\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;",
        "",
        "()V",
        "requireType",
        "",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
        "type",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 304
    invoke-direct {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$requireType(Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;)V
    .locals 0

    .line 304
    invoke-direct {p0, p1, p2}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;->requireType(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;)V

    return-void
.end method

.method private final requireType(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;)V
    .locals 1

    .line 306
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    return-void

    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Original attribute should be of type "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method
