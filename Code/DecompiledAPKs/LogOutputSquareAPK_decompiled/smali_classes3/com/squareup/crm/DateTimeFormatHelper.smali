.class public Lcom/squareup/crm/DateTimeFormatHelper;
.super Ljava/lang/Object;
.source "DateTimeFormatHelper.java"


# static fields
.field private static final COUPON_EXPIRATION_FORMAT:Ljava/lang/String; = "MMMM yyyy"

.field private static final FILENAME_DATE_FORMAT:Ljava/lang/String; = "yyyy-MM-dd h:mm a"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static formatCouponAlreadyRedeemedDate(Lcom/squareup/util/Res;Lcom/squareup/protos/client/ISO8601Date;Ljava/util/Locale;)Ljava/lang/String;
    .locals 2

    .line 82
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMMM yyyy"

    invoke-direct {v0, v1, p2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 83
    iget-object p1, p1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    .line 85
    sget p2, Lcom/squareup/crm/R$string;->crm_coupon_redeemed_date:I

    invoke-interface {p0, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string p2, "redeemed_date"

    .line 86
    invoke-virtual {p0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 87
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formatCouponAwardedDate(Lcom/squareup/util/Res;Lcom/squareup/protos/client/ISO8601Date;Ljava/util/Locale;)Ljava/lang/String;
    .locals 2

    .line 91
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMMM yyyy"

    invoke-direct {v0, v1, p2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 92
    iget-object p1, p1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    .line 94
    sget p2, Lcom/squareup/crm/R$string;->crm_coupon_creation:I

    invoke-interface {p0, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string p2, "creation_date"

    .line 95
    invoke-virtual {p0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 96
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formatCouponDetails(Lcom/squareup/util/Res;Lcom/squareup/protos/client/coupons/Coupon;Ljava/util/Locale;)Ljava/lang/String;
    .locals 3

    .line 100
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon;->code:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_0

    .line 101
    sget v0, Lcom/squareup/crm/R$string;->crm_coupon_details:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/Coupon;->code:Ljava/lang/String;

    const-string v2, "code"

    .line 102
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/Coupon;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 103
    invoke-static {p0, v1, p2}, Lcom/squareup/crm/DateTimeFormatHelper;->formatCouponExpirationDate(Lcom/squareup/util/Res;Lcom/squareup/protos/client/ISO8601Date;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "expiration"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/coupons/Coupon;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 104
    invoke-static {p0, p1, p2}, Lcom/squareup/crm/DateTimeFormatHelper;->formatCouponAwardedDate(Lcom/squareup/util/Res;Lcom/squareup/protos/client/ISO8601Date;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "creation"

    invoke-virtual {v0, p1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 105
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 106
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_1

    .line 107
    iget-object p1, p1, Lcom/squareup/protos/client/coupons/Coupon;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-static {p0, p1, p2}, Lcom/squareup/crm/DateTimeFormatHelper;->formatCouponExpirationDate(Lcom/squareup/util/Res;Lcom/squareup/protos/client/ISO8601Date;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static formatCouponExpirationDate(Lcom/squareup/util/Res;Lcom/squareup/protos/client/ISO8601Date;Ljava/util/Locale;)Ljava/lang/String;
    .locals 2

    .line 73
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMMM yyyy"

    invoke-direct {v0, v1, p2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 74
    iget-object p1, p1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    .line 76
    sget p2, Lcom/squareup/crm/R$string;->crm_coupon_expiration:I

    invoke-interface {p0, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string p2, "expiration_date"

    .line 77
    invoke-virtual {p0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 78
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formatDateForInvoice(Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/util/Res;Ljava/text/DateFormat;)Ljava/lang/String;
    .locals 2

    .line 40
    iget-object p0, p0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Times;->Iso8601ToNormalizedDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    .line 41
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/squareup/util/Times;->getRelativeDate(Ljava/util/Date;J)Lcom/squareup/util/Times$RelativeDate;

    move-result-object v0

    .line 43
    sget-object v1, Lcom/squareup/crm/DateTimeFormatHelper$1;->$SwitchMap$com$squareup$util$Times$RelativeDate:[I

    invoke-virtual {v0}, Lcom/squareup/util/Times$RelativeDate;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 49
    invoke-virtual {p2, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 47
    :cond_0
    sget p0, Lcom/squareup/utilities/R$string;->yesterday:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 45
    :cond_1
    sget p0, Lcom/squareup/utilities/R$string;->today:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formatDateForTransaction(Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/lang/String;
    .locals 1

    if-nez p2, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 29
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 30
    invoke-static {p2, p3}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p2

    .line 32
    invoke-static {v0, p2}, Lcom/squareup/util/Times;->onDifferentDay(Ljava/util/Date;Ljava/util/Date;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 33
    invoke-virtual {p0, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 35
    :cond_1
    invoke-virtual {p1, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formatDateTimeForProfileAttachments(Lcom/squareup/util/Res;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
    .locals 0

    .line 55
    invoke-static {p4, p3}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p3

    .line 56
    sget p4, Lcom/squareup/crm/R$string;->date_format:I

    invoke-interface {p0, p4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 57
    invoke-virtual {p1, p3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string p4, "date"

    invoke-virtual {p0, p4, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 58
    invoke-virtual {p2, p3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string/jumbo p2, "time"

    invoke-virtual {p0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 59
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formatDateTimeForProfileAttachmentsFilename(Lcom/squareup/util/Res;Ljava/util/Date;Ljava/util/Locale;)Ljava/lang/String;
    .locals 2

    .line 64
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd h:mm a"

    invoke-direct {v0, v1, p2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 65
    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    .line 67
    sget p2, Lcom/squareup/crm/R$string;->crm_profile_attachments_filename_template:I

    invoke-interface {p0, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string p2, "filename"

    .line 68
    invoke-virtual {p0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 69
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
