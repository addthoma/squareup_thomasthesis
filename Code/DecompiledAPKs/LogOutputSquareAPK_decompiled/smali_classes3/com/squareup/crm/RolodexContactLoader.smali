.class public Lcom/squareup/crm/RolodexContactLoader;
.super Lcom/squareup/datafetch/Rx1AbstractLoader;
.source "RolodexContactLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/RolodexContactLoader$Input;,
        Lcom/squareup/crm/RolodexContactLoader$SearchTerm;,
        Lcom/squareup/crm/RolodexContactLoader$SharedScope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/datafetch/Rx1AbstractLoader<",
        "Lcom/squareup/crm/RolodexContactLoader$Input;",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        ">;"
    }
.end annotation


# static fields
.field private static final EMPTY_CONTACTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final filterList:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;>;"
        }
    .end annotation
.end field

.field private final groupToken:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private restrictToSearchingOnly:Z

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private searchDelayMs:J

.field private final searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/crm/RolodexContactLoader$SearchTerm;",
            ">;"
        }
    .end annotation
.end field

.field private final sortType:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/ListContactsSortType;",
            ">;"
        }
    .end annotation
.end field

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 40
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/crm/RolodexContactLoader;->EMPTY_CONTACTS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/RolodexServiceHelper;)V
    .locals 0
    .param p2    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 121
    invoke-direct {p0, p1, p2}, Lcom/squareup/datafetch/Rx1AbstractLoader;-><init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;)V

    const/4 p1, 0x0

    .line 46
    move-object p2, p1

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/crm/RolodexContactLoader;->groupToken:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 47
    move-object p2, p1

    check-cast p2, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;

    invoke-static {p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/crm/RolodexContactLoader;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 48
    move-object p2, p1

    check-cast p2, Ljava/util/List;

    invoke-static {p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/crm/RolodexContactLoader;->filterList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 49
    check-cast p1, Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    .line 50
    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/crm/RolodexContactLoader;->sortType:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 122
    iput-object p3, p0, Lcom/squareup/crm/RolodexContactLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    .line 123
    iput-object p4, p0, Lcom/squareup/crm/RolodexContactLoader;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    return-void
.end method

.method static synthetic lambda$fetch$4(Lcom/squareup/crm/RolodexContactLoader$Input;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/datafetch/Rx1AbstractLoader$Response;
    .locals 3

    .line 219
    instance-of v0, p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/rolodex/ListContactsResponse;

    .line 221
    new-instance v0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/ListContactsResponse;->contact:Ljava/util/List;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/ListContactsResponse;->paging_key:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Ljava/util/List;Ljava/lang/String;)V

    return-object v0

    .line 223
    :cond_0
    new-instance p2, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;

    new-instance v0, Lcom/squareup/datafetch/LoaderError$ThrowableError;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Request failed."

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/squareup/datafetch/LoaderError$ThrowableError;-><init>(Ljava/lang/Throwable;)V

    invoke-direct {p2, p0, p1, v0}, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lcom/squareup/datafetch/LoaderError;)V

    return-object p2
.end method

.method static synthetic lambda$input$2(Lcom/squareup/crm/RolodexContactLoader$SearchTerm;)Ljava/lang/String;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 197
    :cond_0
    iget-object p0, p0, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;->term:Ljava/lang/String;

    :goto_0
    return-object p0
.end method

.method static synthetic lambda$input$3(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 198
    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    :cond_0
    return-object p0
.end method


# virtual methods
.method protected fetch(Lcom/squareup/crm/RolodexContactLoader$Input;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lrx/functions/Action0;)Lrx/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/RolodexContactLoader$Input;",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;",
            "Lrx/functions/Action0;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Response<",
            "Lcom/squareup/crm/RolodexContactLoader$Input;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;"
        }
    .end annotation

    .line 210
    iget-boolean v0, p0, Lcom/squareup/crm/RolodexContactLoader;->restrictToSearchingOnly:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/crm/RolodexContactLoader$Input;->searchTerm:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    new-instance p3, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;

    sget-object v0, Lcom/squareup/crm/RolodexContactLoader;->EMPTY_CONTACTS:Ljava/util/List;

    const/4 v1, 0x0

    invoke-direct {p3, p1, p2, v0, v1}, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Ljava/util/List;Ljava/lang/String;)V

    invoke-static {p3}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, p1, Lcom/squareup/crm/RolodexContactLoader$Input;->groupToken:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/crm/RolodexContactLoader$Input;->searchTerm:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/crm/RolodexContactLoader$Input;->filterList:Ljava/util/List;

    iget-object v4, p1, Lcom/squareup/crm/RolodexContactLoader$Input;->sortType:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    iget-object v5, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    iget-object v6, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;->pageSize:Ljava/lang/Integer;

    invoke-interface/range {v0 .. v6}, Lcom/squareup/crm/RolodexServiceHelper;->listContacts(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/rolodex/ListContactsSortType;Ljava/lang/String;Ljava/lang/Integer;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object v0

    .line 216
    invoke-virtual {v0, p3}, Lrx/Single;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Single;

    move-result-object p3

    .line 217
    invoke-virtual {p3}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object p3

    new-instance v0, Lcom/squareup/crm/-$$Lambda$RolodexContactLoader$kgQFiw2ggQAJMiNbd-FKisn3QYY;

    invoke-direct {v0, p1, p2}, Lcom/squareup/crm/-$$Lambda$RolodexContactLoader$kgQFiw2ggQAJMiNbd-FKisn3QYY;-><init>(Lcom/squareup/crm/RolodexContactLoader$Input;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;)V

    .line 218
    invoke-virtual {p3, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fetch(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lrx/functions/Action0;)Lrx/Observable;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/crm/RolodexContactLoader$Input;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/crm/RolodexContactLoader;->fetch(Lcom/squareup/crm/RolodexContactLoader$Input;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method getFilterList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 173
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 174
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->filterList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method getGroupToken()Ljava/lang/String;
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 152
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->groupToken:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method getSearchTerm()Ljava/lang/String;
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 163
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;

    iget-object v0, v0, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;->term:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method getSortType()Lcom/squareup/protos/client/rolodex/ListContactsSortType;
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 185
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->sortType:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    return-object v0
.end method

.method protected input()Lrx/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/crm/RolodexContactLoader$Input;",
            ">;"
        }
    .end annotation

    .line 189
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->groupToken:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 191
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/crm/RolodexContactLoader;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 193
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/crm/-$$Lambda$RolodexContactLoader$0rs04tKn8ZlbYMU9cbV6CKU8mA4;

    invoke-direct {v2, p0}, Lcom/squareup/crm/-$$Lambda$RolodexContactLoader$0rs04tKn8ZlbYMU9cbV6CKU8mA4;-><init>(Lcom/squareup/crm/RolodexContactLoader;)V

    .line 194
    invoke-virtual {v1, v2}, Lrx/Observable;->debounce(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/crm/-$$Lambda$RolodexContactLoader$plOT1GB1LOFKKxJQ8dHJAoQHgI4;->INSTANCE:Lcom/squareup/crm/-$$Lambda$RolodexContactLoader$plOT1GB1LOFKKxJQ8dHJAoQHgI4;

    .line 197
    invoke-virtual {v1, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/crm/-$$Lambda$RolodexContactLoader$nKpt3EYmrS0VNUUbOOhWmr3vf-4;->INSTANCE:Lcom/squareup/crm/-$$Lambda$RolodexContactLoader$nKpt3EYmrS0VNUUbOOhWmr3vf-4;

    .line 198
    invoke-virtual {v1, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    .line 199
    invoke-virtual {v1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/crm/RolodexContactLoader;->filterList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 201
    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/crm/RolodexContactLoader;->sortType:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 203
    invoke-virtual {v3}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v3

    sget-object v4, Lcom/squareup/crm/-$$Lambda$xHm1GiUExe5YpsD1jtGdv2DGXZs;->INSTANCE:Lcom/squareup/crm/-$$Lambda$xHm1GiUExe5YpsD1jtGdv2DGXZs;

    .line 189
    invoke-static {v0, v1, v2, v3, v4}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func4;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$input$1$RolodexContactLoader(Lcom/squareup/crm/RolodexContactLoader$SearchTerm;)Lrx/Observable;
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 194
    iget-boolean p1, p1, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;->debounce:Z

    if-eqz p1, :cond_0

    check-cast v0, Ljava/lang/Void;

    .line 195
    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    iget-wide v0, p0, Lcom/squareup/crm/RolodexContactLoader;->searchDelayMs:J

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/squareup/crm/RolodexContactLoader;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {p1, v0, v1, v2, v3}, Lrx/Observable;->delay(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    .line 196
    :cond_0
    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public synthetic lambda$onEnterScope$0$RolodexContactLoader(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 131
    invoke-virtual {p0}, Lcom/squareup/crm/RolodexContactLoader;->refresh()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 127
    invoke-super {p0, p1}, Lcom/squareup/datafetch/Rx1AbstractLoader;->onEnterScope(Lmortar/MortarScope;)V

    .line 129
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 130
    invoke-interface {v0}, Lcom/squareup/crm/RolodexServiceHelper;->onContactsAddedOrRemoved()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/crm/-$$Lambda$RolodexContactLoader$pczsTKn7ZIo-k_MS7-3U1gdxHfY;

    invoke-direct {v1, p0}, Lcom/squareup/crm/-$$Lambda$RolodexContactLoader$pczsTKn7ZIo-k_MS7-3U1gdxHfY;-><init>(Lcom/squareup/crm/RolodexContactLoader;)V

    .line 131
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 129
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public setFilterList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;)V"
        }
    .end annotation

    .line 167
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 168
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->filterList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setGroupToken(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_1

    .line 144
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 145
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 146
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->groupToken:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setRestrictToSearchingOnly(Z)V
    .locals 0

    .line 140
    iput-boolean p1, p0, Lcom/squareup/crm/RolodexContactLoader;->restrictToSearchingOnly:Z

    return-void
.end method

.method public setSearchDelayMs(J)V
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 135
    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 136
    iput-wide p1, p0, Lcom/squareup/crm/RolodexContactLoader;->searchDelayMs:J

    return-void
.end method

.method public setSearchTerm(Lcom/squareup/crm/RolodexContactLoader$SearchTerm;)V
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 157
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setSortType(Lcom/squareup/protos/client/rolodex/ListContactsSortType;)V
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 179
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader;->sortType:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
