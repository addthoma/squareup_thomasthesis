.class public Lcom/squareup/crm/filters/MultiOptionFilterHelper;
.super Ljava/lang/Object;
.source "MultiOptionFilterHelper.java"


# instance fields
.field private final groupLoader:Lcom/squareup/crm/RolodexGroupLoader;


# direct methods
.method constructor <init>(Lcom/squareup/crm/RolodexGroupLoader;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    return-void
.end method

.method private add(Ljava/util/List;Lcom/squareup/protos/client/rolodex/Filter$Option;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 152
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private add(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 158
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private getManualGroups()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation

    .line 179
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 181
    iget-object v1, p0, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    invoke-interface {v1}, Lcom/squareup/crm/RolodexGroupLoader;->success()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/crm/filters/-$$Lambda$MultiOptionFilterHelper$-IrhCCDZ1Cvb3Z0UnDHmDOF--zE;

    invoke-direct {v2, v0}, Lcom/squareup/crm/filters/-$$Lambda$MultiOptionFilterHelper$-IrhCCDZ1Cvb3Z0UnDHmDOF--zE;-><init>(Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 182
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 183
    invoke-interface {v1}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 185
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->emptyIfNull(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$getManualGroups$0(Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/protos/client/rolodex/GroupType;

    .line 182
    sget-object v1, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lcom/squareup/crm/util/RolodexGroupHelper;->filterByTypeAndSort(Ljava/util/List;[Lcom/squareup/protos/client/rolodex/GroupType;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private remove(Ljava/util/List;Lcom/squareup/protos/client/rolodex/Filter$Option;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 164
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 165
    iget-object v2, v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    iget-object v3, p2, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 166
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private remove(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 173
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 174
    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-object v0
.end method


# virtual methods
.method public getAvailableOptions(Lcom/squareup/protos/client/rolodex/Filter;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation

    .line 26
    sget-object v0, Lcom/squareup/crm/filters/MultiOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 45
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types_options:Ljava/util/List;

    return-object p1

    .line 48
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    .line 42
    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations_options:Ljava/util/List;

    return-object p1

    .line 39
    :cond_2
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_options:Ljava/util/List;

    return-object p1

    .line 28
    :cond_3
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 29
    invoke-direct {p0}, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->getManualGroups()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Group;

    .line 30
    new-instance v2, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;-><init>()V

    iget-object v3, v1, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    .line 31
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;->value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;

    move-result-object v2

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    .line 32
    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;->label(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;

    move-result-object v1

    .line 33
    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;->build()Lcom/squareup/protos/client/rolodex/Filter$Option;

    move-result-object v1

    .line 30
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    return-object p1
.end method

.method public getSelected(Lcom/squareup/protos/client/rolodex/Filter;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation

    .line 122
    sget-object v0, Lcom/squareup/crm/filters/MultiOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 143
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types:Ljava/util/List;

    return-object p1

    .line 146
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    .line 140
    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations:Ljava/util/List;

    return-object p1

    .line 137
    :cond_2
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_values:Ljava/util/List;

    return-object p1

    .line 124
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 125
    invoke-direct {p0}, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->getManualGroups()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Group;

    .line 126
    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->mg_group_tokens:Ljava/util/List;

    iget-object v4, v2, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 127
    new-instance v3, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;-><init>()V

    iget-object v4, v2, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    .line 128
    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;->value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;

    move-result-object v3

    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    .line 129
    invoke-virtual {v3, v2}, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;->label(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;

    move-result-object v2

    .line 130
    invoke-virtual {v2}, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;->build()Lcom/squareup/protos/client/rolodex/Filter$Option;

    move-result-object v2

    .line 127
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    return-object v0
.end method

.method public isSelected(Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/protos/client/rolodex/Filter$Option;)Z
    .locals 2

    .line 53
    invoke-virtual {p0, p1}, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->getSelected(Lcom/squareup/protos/client/rolodex/Filter;)Ljava/util/List;

    move-result-object p1

    .line 54
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 55
    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public isValid(Lcom/squareup/protos/client/rolodex/Filter;)Z
    .locals 0

    .line 117
    invoke-virtual {p0, p1}, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->getSelected(Lcom/squareup/protos/client/rolodex/Filter;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public select(Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter;
    .locals 3

    .line 63
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Filter;->newBuilder()Lcom/squareup/protos/client/rolodex/Filter$Builder;

    move-result-object v0

    .line 65
    sget-object v1, Lcom/squareup/crm/filters/MultiOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v2}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 79
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->add(Ljava/util/List;Lcom/squareup/protos/client/rolodex/Filter$Option;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cs_creation_source_types(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto :goto_0

    .line 83
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    .line 75
    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->add(Ljava/util/List;Lcom/squareup/protos/client/rolodex/Filter$Option;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hvl_locations(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto :goto_0

    .line 71
    :cond_2
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_values:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->add(Ljava/util/List;Lcom/squareup/protos/client/rolodex/Filter$Option;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_values(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto :goto_0

    .line 67
    :cond_3
    iget-object p1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->mg_group_tokens:Ljava/util/List;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->add(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->mg_group_tokens(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    .line 86
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->build()Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object p1

    return-object p1
.end method

.method public unselect(Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter;
    .locals 3

    .line 90
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Filter;->newBuilder()Lcom/squareup/protos/client/rolodex/Filter$Builder;

    move-result-object v0

    .line 92
    sget-object v1, Lcom/squareup/crm/filters/MultiOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v2}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 106
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->remove(Ljava/util/List;Lcom/squareup/protos/client/rolodex/Filter$Option;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cs_creation_source_types(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto :goto_0

    .line 110
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    .line 102
    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->remove(Ljava/util/List;Lcom/squareup/protos/client/rolodex/Filter$Option;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hvl_locations(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto :goto_0

    .line 98
    :cond_2
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_values:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->remove(Ljava/util/List;Lcom/squareup/protos/client/rolodex/Filter$Option;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_values(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto :goto_0

    .line 94
    :cond_3
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->mg_group_tokens:Ljava/util/List;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->remove(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->mg_group_tokens(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    .line 113
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->build()Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object p1

    return-object p1
.end method
