.class synthetic Lcom/squareup/crm/filters/SingleOptionFilterHelper$1;
.super Ljava/lang/Object;
.source "SingleOptionFilterHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/filters/SingleOptionFilterHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 26
    invoke-static {}, Lcom/squareup/protos/client/rolodex/Filter$Type;->values()[Lcom/squareup/protos/client/rolodex/Filter$Type;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/crm/filters/SingleOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    :try_start_0
    sget-object v0, Lcom/squareup/crm/filters/SingleOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->LAST_PAYMENT_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/crm/filters/SingleOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->NO_PAYMENT_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/crm/filters/SingleOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->HAS_CARD_ON_FILE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/crm/filters/SingleOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_BOOLEAN:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v0, Lcom/squareup/crm/filters/SingleOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->FEEDBACK:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lcom/squareup/crm/filters/SingleOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->HAS_LOYALTY:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v0, Lcom/squareup/crm/filters/SingleOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->IS_INSTANT_PROFILE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    return-void
.end method
