.class public final Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner;
.super Ljava/lang/Object;
.source "ChooseGroupsLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsRendering;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseGroupsLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseGroupsLayoutRunner.kt\ncom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n+ 5 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt\n+ 6 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 7 BinderRowSpec.kt\ncom/squareup/cycler/BinderRowSpec\n*L\n1#1,102:1\n49#2:103\n50#2,3:109\n53#2:166\n599#3,4:104\n601#3:108\n43#4,2:112\n133#5:114\n147#5,5:115\n153#5:131\n134#5:132\n81#5,5:133\n87#5:148\n166#5,5:149\n172#5:165\n328#6:120\n342#6,5:121\n344#6,4:126\n329#6:130\n342#6,5:138\n344#6,2:143\n347#6:147\n328#6:154\n342#6,5:155\n344#6,4:160\n329#6:164\n24#7,2:145\n*E\n*S KotlinDebug\n*F\n+ 1 ChooseGroupsLayoutRunner.kt\ncom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner\n*L\n50#1:103\n50#1,3:109\n50#1:166\n50#1,4:104\n50#1:108\n50#1,2:112\n50#1:114\n50#1,5:115\n50#1:131\n50#1:132\n50#1,5:133\n50#1:148\n50#1,5:149\n50#1:165\n50#1:120\n50#1,5:121\n50#1,4:126\n50#1:130\n50#1,5:138\n50#1,2:143\n50#1:147\n50#1:154\n50#1,5:155\n50#1,4:160\n50#1:164\n50#1,2:145\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0015B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0014H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\n \u000f*\u0004\u0018\u00010\u000e0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsRendering;",
        "view",
        "Landroid/view/View;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsRendering$Row;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "kotlin.jvm.PlatformType",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsRendering$Row;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner;->view:Landroid/view/View;

    .line 48
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 49
    iget-object p1, p0, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/crm/groups/impl/R$id;->crm_groups_recycler_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 50
    iget-object p1, p0, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    sget-object v0, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 104
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 105
    new-instance v0, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v0}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 109
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 110
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 112
    new-instance p2, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {p2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/4 v1, 0x0

    .line 52
    invoke-virtual {p2, v1}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    .line 53
    check-cast p2, Lcom/squareup/cycler/ExtensionSpec;

    .line 112
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 55
    sget-object p2, Lcom/squareup/noho/NohoButtonType;->SECONDARY:Lcom/squareup/noho/NohoButtonType;

    .line 114
    new-instance v1, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$$special$$inlined$nohoButton$1;

    invoke-direct {v1}, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$$special$$inlined$nohoButton$1;-><init>()V

    check-cast v1, Lkotlin/jvm/functions/Function3;

    .line 116
    new-instance v2, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$$special$$inlined$nohoButton$2;

    invoke-direct {v2, p2}, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$$special$$inlined$nohoButton$2;-><init>(Lcom/squareup/noho/NohoButtonType;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 122
    new-instance p2, Lcom/squareup/cycler/BinderRowSpec;

    .line 126
    sget-object v3, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$$special$$inlined$nohoButton$3;->INSTANCE:Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$$special$$inlined$nohoButton$3;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 122
    invoke-direct {p2, v3, v2}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 120
    invoke-virtual {p2, v1}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 125
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 121
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 60
    sget-object p2, Lcom/squareup/noho/CheckType$CHECK;->INSTANCE:Lcom/squareup/noho/CheckType$CHECK;

    check-cast p2, Lcom/squareup/noho/CheckType;

    .line 134
    new-instance v1, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$$special$$inlined$nohoRow$1;

    invoke-direct {v1, p2}, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$$special$$inlined$nohoRow$1;-><init>(Lcom/squareup/noho/CheckType;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 139
    new-instance p2, Lcom/squareup/cycler/BinderRowSpec;

    .line 143
    sget-object v2, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$$special$$inlined$nohoRow$2;->INSTANCE:Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$$special$$inlined$nohoRow$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 139
    invoke-direct {p2, v2, v1}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 145
    new-instance v1, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$$special$$inlined$bind$1;

    invoke-direct {v1}, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$$special$$inlined$bind$1;-><init>()V

    check-cast v1, Lkotlin/jvm/functions/Function3;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 68
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 138
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 71
    sget-object p2, Lcom/squareup/noho/NohoLabel$Type;->LABEL:Lcom/squareup/noho/NohoLabel$Type;

    sget-object v1, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$recycler$1$4;->INSTANCE:Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$recycler$1$4;

    check-cast v1, Lkotlin/jvm/functions/Function3;

    .line 150
    new-instance v2, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$$special$$inlined$nohoLabel$1;

    invoke-direct {v2, p2}, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$$special$$inlined$nohoLabel$1;-><init>(Lcom/squareup/noho/NohoLabel$Type;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 156
    new-instance p2, Lcom/squareup/cycler/BinderRowSpec;

    .line 160
    sget-object v3, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$$special$$inlined$nohoLabel$2;->INSTANCE:Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$$special$$inlined$nohoLabel$2;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 156
    invoke-direct {p2, v3, v2}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 154
    invoke-virtual {p2, v1}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 159
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 155
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 107
    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    .line 78
    iget-object p1, p0, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 79
    new-instance p2, Lcom/squareup/ui/DividerDecoration;

    .line 80
    iget-object v0, p0, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/noho/R$dimen;->noho_spacing_small:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 79
    invoke-direct {p2, v0}, Lcom/squareup/ui/DividerDecoration;-><init>(I)V

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    .line 78
    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    return-void

    .line 104
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public showRendering(Lcom/squareup/crm/groups/choose/ChooseGroupsRendering;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 3

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-object p2, p0, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/crm/groups/choose/ChooseGroupsRendering;->getOnClose()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 97
    iget-object p2, p0, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 91
    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 92
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v2, Lcom/squareup/crm/groups/impl/R$string;->crm_groups_choose_groups_title:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 94
    sget-object v1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    .line 95
    invoke-virtual {p1}, Lcom/squareup/crm/groups/choose/ChooseGroupsRendering;->getOnClose()Lkotlin/jvm/functions/Function0;

    move-result-object v2

    .line 93
    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 99
    iget-object p2, p0, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    invoke-virtual {p1}, Lcom/squareup/crm/groups/choose/ChooseGroupsRendering;->getRows()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/cycler/Recycler;->setData(Lcom/squareup/cycler/DataSource;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/crm/groups/choose/ChooseGroupsRendering;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner;->showRendering(Lcom/squareup/crm/groups/choose/ChooseGroupsRendering;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
