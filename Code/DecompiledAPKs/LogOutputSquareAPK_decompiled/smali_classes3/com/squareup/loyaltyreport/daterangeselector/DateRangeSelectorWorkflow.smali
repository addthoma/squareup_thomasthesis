.class public final Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "DateRangeSelectorWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorProps;",
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;",
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDateRangeSelectorWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DateRangeSelectorWorkflow.kt\ncom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,69:1\n149#2,5:70\n*E\n*S KotlinDebug\n*F\n+ 1 DateRangeSelectorWorkflow.kt\ncom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow\n*L\n51#1,5:70\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u001c\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u001c\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u000e2\u0006\u0010\u0012\u001a\u00020\u0010H\u0002J\u001a\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00022\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016JN\u0010\u0017\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00032\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u0003H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorProps;",
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;",
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "cancel",
        "Lcom/squareup/workflow/WorkflowAction;",
        "initialDateRange",
        "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;",
        "exitWithSelection",
        "selectedDateRange",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static final synthetic access$cancel(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;->cancel(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$exitWithSelection(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;->exitWithSelection(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method private final cancel(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;",
            "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;",
            ">;"
        }
    .end annotation

    .line 63
    new-instance v0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$cancel$1;

    invoke-direct {v0, p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$cancel$1;-><init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final exitWithSelection(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;",
            "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;",
            ">;"
        }
    .end annotation

    .line 57
    new-instance v0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$exitWithSelection$1;

    invoke-direct {v0, p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$exitWithSelection$1;-><init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance p2, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;

    invoke-virtual {p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorProps;->getInitialDateRange()Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;-><init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)V

    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;->initialState(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorProps;

    check-cast p2, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;->render(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorProps;Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorProps;Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorProps;",
            "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;",
            "-",
            "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p3

    .line 41
    new-instance v0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;

    .line 42
    invoke-virtual {p2}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;->getSelectedDateRange()Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    move-result-object p2

    .line 43
    new-instance v1, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$render$1;

    invoke-direct {v1, p0, p3}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$render$1;-><init>(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 47
    new-instance v2, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$render$2;

    invoke-direct {v2, p0, p3, p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$render$2;-><init>(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorProps;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 41
    invoke-direct {v0, p2, v1, v2}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;-><init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 71
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 72
    const-class p2, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string p3, ""

    invoke-static {p2, p3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 73
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 71
    invoke-direct {p1, p2, v0, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 52
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;->snapshotState(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
