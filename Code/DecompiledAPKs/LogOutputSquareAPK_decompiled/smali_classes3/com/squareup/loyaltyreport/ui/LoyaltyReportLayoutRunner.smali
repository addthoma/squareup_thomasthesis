.class public final Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;
.super Ljava/lang/Object;
.source "LoyaltyReportLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/loyaltyreport/LoyaltyReportScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoyaltyReportLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoyaltyReportLayoutRunner.kt\ncom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner\n*L\n1#1,106:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0002H\u0002J\u0010\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u0002H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/loyaltyreport/LoyaltyReportScreen;",
        "view",
        "Landroid/view/View;",
        "loyaltyReportRecyclerHelper",
        "Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper;",
        "(Landroid/view/View;Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "progressBar",
        "Landroid/widget/ProgressBar;",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "updateRecycler",
        "loyaltyReportScreen",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final progressBar:Landroid/widget/ProgressBar;

.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyReportRecyclerHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->view:Landroid/view/View;

    .line 41
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 42
    iget-object p1, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/loyaltyreport/impl/R$id;->loyalty_report_recycler_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.l\u2026lty_report_recycler_view)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 43
    iget-object p1, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/loyaltyreport/impl/R$id;->loyalty_report_progress_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.l\u2026alty_report_progress_bar)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->progressBar:Landroid/widget/ProgressBar;

    .line 46
    iget-object p1, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p2, p1}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper;->adoptFor(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    .line 51
    iget-object p1, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 49
    new-instance p2, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 50
    new-instance v0, Lcom/squareup/resources/ResourceString;

    sget v1, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_title:I

    invoke-direct {v0, v1}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v0, Lcom/squareup/resources/TextModel;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p2

    .line 51
    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateActionBar(Lcom/squareup/loyaltyreport/LoyaltyReportScreen;)V
    .locals 10

    .line 82
    iget-object v0, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 69
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 72
    sget v3, Lcom/squareup/vectoricons/R$drawable;->ui_settings_24:I

    .line 73
    new-instance v2, Lcom/squareup/resources/ResourceString;

    sget v4, Lcom/squareup/loyaltyreport/impl/R$string;->date_range_tooltip:I

    invoke-direct {v2, v4}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/resources/TextModel;

    .line 74
    invoke-virtual {p1}, Lcom/squareup/loyaltyreport/LoyaltyReportScreen;->getOnDatePickerSelected()Lkotlin/jvm/functions/Function0;

    move-result-object v7

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    move-object v2, v1

    .line 71
    invoke-static/range {v2 .. v9}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionIcon$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;ILcom/squareup/resources/TextModel;ZILkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 76
    iget-object v2, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->view:Landroid/view/View;

    invoke-static {v2}, Lcom/squareup/container/ContainerKt;->getScreenSize(Landroid/view/View;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/util/DeviceScreenSizeInfo;->isMasterDetail()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 77
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 79
    :cond_0
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    invoke-virtual {p1}, Lcom/squareup/loyaltyreport/LoyaltyReportScreen;->getOnExit()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 82
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateRecycler(Lcom/squareup/loyaltyreport/LoyaltyReportScreen;)V
    .locals 3

    .line 86
    invoke-virtual {p1}, Lcom/squareup/loyaltyreport/LoyaltyReportScreen;->getContent()Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;

    move-result-object v0

    .line 87
    instance-of v1, v0, Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content$Loaded;

    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    new-instance v2, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$updateRecycler$1;

    invoke-direct {v2, v0, p1}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$updateRecycler$1;-><init>(Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;Lcom/squareup/loyaltyreport/LoyaltyReportScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/loyaltyreport/LoyaltyReportScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 2

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->updateActionBar(Lcom/squareup/loyaltyreport/LoyaltyReportScreen;)V

    .line 59
    iget-object p2, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/loyaltyreport/LoyaltyReportScreen;->getOnExit()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 61
    invoke-virtual {p1}, Lcom/squareup/loyaltyreport/LoyaltyReportScreen;->getContent()Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;

    move-result-object p2

    .line 62
    iget-object v0, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->progressBar:Landroid/widget/ProgressBar;

    check-cast v0, Landroid/view/View;

    instance-of v1, p2, Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content$Loading;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 63
    iget-object v0, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    check-cast v0, Landroid/view/View;

    instance-of p2, p2, Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content$Loaded;

    invoke-static {v0, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 65
    invoke-direct {p0, p1}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->updateRecycler(Lcom/squareup/loyaltyreport/LoyaltyReportScreen;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/loyaltyreport/LoyaltyReportScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->showRendering(Lcom/squareup/loyaltyreport/LoyaltyReportScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
