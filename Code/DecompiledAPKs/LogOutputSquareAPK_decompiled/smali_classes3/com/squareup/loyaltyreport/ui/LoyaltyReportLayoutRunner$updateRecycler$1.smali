.class final Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$updateRecycler$1;
.super Lkotlin/jvm/internal/Lambda;
.source "LoyaltyReportLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner;->updateRecycler(Lcom/squareup/loyaltyreport/LoyaltyReportScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cycler/Update<",
        "Ljava/lang/Object;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoyaltyReportLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoyaltyReportLayoutRunner.kt\ncom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$updateRecycler$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,106:1\n1360#2:107\n1429#2,3:108\n*E\n*S KotlinDebug\n*F\n+ 1 LoyaltyReportLayoutRunner.kt\ncom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$updateRecycler$1\n*L\n90#1:107\n90#1,3:108\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/Update;",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $content:Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;

.field final synthetic $loyaltyReportScreen:Lcom/squareup/loyaltyreport/LoyaltyReportScreen;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;Lcom/squareup/loyaltyreport/LoyaltyReportScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$updateRecycler$1;->$content:Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;

    iput-object p2, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$updateRecycler$1;->$loyaltyReportScreen:Lcom/squareup/loyaltyreport/LoyaltyReportScreen;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/cycler/Update;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$updateRecycler$1;->invoke(Lcom/squareup/cycler/Update;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/Update;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Update<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$updateRecycler$1;->$content:Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;

    check-cast v0, Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content$Loaded;

    invoke-virtual {v0}, Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content$Loaded;->getLoyaltyReport()Lcom/squareup/loyaltyreport/ui/LoyaltyReport;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyaltyreport/ui/LoyaltyReport;->getRows()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 107
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 108
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 109
    check-cast v2, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;

    .line 91
    instance-of v3, v2, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$ViewInDashboard;

    if-eqz v3, :cond_0

    .line 92
    new-instance v3, Lcom/squareup/loyaltyreport/ui/ViewInDashboardRecyclerRow;

    check-cast v2, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$ViewInDashboard;

    new-instance v4, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$updateRecycler$1$$special$$inlined$map$lambda$1;

    invoke-direct {v4, p0}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$updateRecycler$1$$special$$inlined$map$lambda$1;-><init>(Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$updateRecycler$1;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-direct {v3, v2, v4}, Lcom/squareup/loyaltyreport/ui/ViewInDashboardRecyclerRow;-><init>(Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$ViewInDashboard;Lkotlin/jvm/functions/Function1;)V

    move-object v2, v3

    .line 97
    :cond_0
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 110
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 100
    invoke-static {v1}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Update;->setData(Lcom/squareup/cycler/DataSource;)V

    .line 101
    sget-object v0, Lcom/squareup/loyaltyreport/ui/SpaceRecyclerRow;->INSTANCE:Lcom/squareup/loyaltyreport/ui/SpaceRecyclerRow;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Update;->setExtraItem(Ljava/lang/Object;)V

    return-void
.end method
