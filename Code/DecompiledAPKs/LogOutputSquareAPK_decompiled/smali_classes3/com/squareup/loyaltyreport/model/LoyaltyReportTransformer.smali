.class public final Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;
.super Ljava/lang/Object;
.source "LoyaltyReportTransformer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoyaltyReportTransformer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoyaltyReportTransformer.kt\ncom/squareup/loyaltyreport/model/LoyaltyReportTransformer\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,249:1\n250#2,2:250\n250#2,2:252\n959#2:254\n1360#2:255\n1429#2,3:256\n1642#2,2:259\n959#2:261\n1360#2:262\n1429#2,3:263\n*E\n*S KotlinDebug\n*F\n+ 1 LoyaltyReportTransformer.kt\ncom/squareup/loyaltyreport/model/LoyaltyReportTransformer\n*L\n145#1,2:250\n161#1,2:252\n175#1:254\n178#1:255\n178#1,3:256\n218#1,2:259\n222#1:261\n225#1:262\n225#1,3:263\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0004\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001BM\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000e\u0008\u0001\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\u001e\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u00172\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u001e\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u00172\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0010\u0010\u001f\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020!H\u0002J\u0016\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%2\u0006\u0010\u0019\u001a\u00020\u001aJ$\u0010&\u001a\u00020\u001c*\n\u0012\u0004\u0012\u00020\'\u0018\u00010\u00172\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020\u001cH\u0002J$\u0010+\u001a\u00020\u001c*\n\u0012\u0004\u0012\u00020\'\u0018\u00010\u00172\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020\u001cH\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;",
        "",
        "dateTimeFormatter",
        "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "singleDigitFormatter",
        "",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "res",
        "Lcom/squareup/util/Res;",
        "currentTimeZone",
        "Lcom/squareup/time/CurrentTimeZone;",
        "customerInitialsHelper",
        "Lcom/squareup/crm/util/CustomerInitialsHelper;",
        "(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/crm/util/CustomerInitialsHelper;)V",
        "dateRangePhrase",
        "Lcom/squareup/phrase/Phrase;",
        "visitsPhrasePlural",
        "visitsPhraseSingular",
        "createRewardsRedeemedList",
        "",
        "Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;",
        "getLoyaltyReportResponse",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;",
        "notAvailable",
        "",
        "createTopCustomersList",
        "Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;",
        "numberOfVisitsTerm",
        "numberOfVisits",
        "",
        "transform",
        "Lcom/squareup/loyaltyreport/ui/LoyaltyReport;",
        "requestedDateRange",
        "Lcom/squareup/protos/common/time/DateTimeInterval;",
        "formattedCount",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;",
        "cohortType",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;",
        "naString",
        "formattedMoney",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

.field private final customerInitialsHelper:Lcom/squareup/crm/util/CustomerInitialsHelper;

.field private final dateRangePhrase:Lcom/squareup/phrase/Phrase;

.field private final dateTimeFormatter:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final singleDigitFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field private final visitsPhrasePlural:Lcom/squareup/phrase/Phrase;

.field private final visitsPhraseSingular:Lcom/squareup/phrase/Phrase;


# direct methods
.method public constructor <init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/crm/util/CustomerInitialsHelper;)V
    .locals 1
    .param p3    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/text/SingleDigitDecimalNumberFormatter;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/time/CurrentTimeZone;",
            "Lcom/squareup/crm/util/CustomerInitialsHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "dateTimeFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "singleDigitFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltySettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTimeZone"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerInitialsHelper"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->dateTimeFormatter:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;

    iput-object p2, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->singleDigitFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    iput-object p5, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->res:Lcom/squareup/util/Res;

    iput-object p6, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    iput-object p7, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->customerInitialsHelper:Lcom/squareup/crm/util/CustomerInitialsHelper;

    .line 38
    iget-object p1, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_date_range:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->dateRangePhrase:Lcom/squareup/phrase/Phrase;

    .line 40
    iget-object p1, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_customer_visits_singular:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->visitsPhraseSingular:Lcom/squareup/phrase/Phrase;

    .line 42
    iget-object p1, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_customer_visits_plural:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->visitsPhrasePlural:Lcom/squareup/phrase/Phrase;

    return-void
.end method

.method private final createRewardsRedeemedList(Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;",
            ">;"
        }
    .end annotation

    .line 216
    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->top_reward_counts:Ljava/util/List;

    const-string/jumbo v0, "topRewardCounts"

    .line 218
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 259
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;

    .line 219
    iget-object v3, v3, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->count:Ljava/lang/Long;

    const-string v4, "frequencyCount.count"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    add-long/2addr v1, v3

    goto :goto_0

    .line 261
    :cond_0
    new-instance v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer$createRewardsRedeemedList$$inlined$sortedByDescending$1;

    invoke-direct {v0}, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer$createRewardsRedeemedList$$inlined$sortedByDescending$1;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    check-cast p1, Ljava/lang/Iterable;

    .line 262
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {p1, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 263
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 264
    check-cast v4, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;

    .line 226
    new-instance v11, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;

    .line 227
    iget-object v6, v4, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->name:Ljava/lang/String;

    const-string v5, "frequencyCount.name"

    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    iget-object v5, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->singleDigitFormatter:Lcom/squareup/text/Formatter;

    iget-object v4, v4, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->count:Ljava/lang/Long;

    if-eqz v4, :cond_1

    goto :goto_2

    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :goto_2
    check-cast v4, Ljava/lang/Number;

    invoke-interface {v5, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object v5, v11

    .line 226
    invoke-direct/range {v5 .. v10}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;-><init>(Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 229
    invoke-interface {v3, v11}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 265
    :cond_2
    check-cast v3, Ljava/util/List;

    goto :goto_3

    :cond_3
    const/4 v3, 0x0

    .line 231
    :goto_3
    check-cast v3, Ljava/util/Collection;

    const/4 p1, 0x1

    if-eqz v3, :cond_4

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_4
    const/4 v0, 0x1

    :cond_5
    if-eqz v0, :cond_6

    .line 233
    new-instance p1, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    const-string v6, ""

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v4 .. v9}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;-><init>(Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 232
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_4

    .line 240
    :cond_6
    new-instance p2, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;

    .line 241
    iget-object v0, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_total:I

    invoke-interface {v0, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 242
    iget-object v4, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->singleDigitFormatter:Lcom/squareup/text/Formatter;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v4, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 240
    invoke-direct {p2, v0, v1, p1}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 239
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {v3, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    :goto_4
    return-object p1
.end method

.method private final createTopCustomersList(Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;",
            ">;"
        }
    .end annotation

    .line 178
    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->top_customer_counts:Ljava/util/List;

    if-eqz p1, :cond_1

    check-cast p1, Ljava/lang/Iterable;

    .line 254
    new-instance v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer$createTopCustomersList$$inlined$sortedByDescending$1;

    invoke-direct {v0}, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer$createTopCustomersList$$inlined$sortedByDescending$1;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Ljava/lang/Iterable;

    .line 255
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 256
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 257
    check-cast v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;

    .line 179
    iget-object v2, v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->name:Ljava/lang/String;

    .line 180
    iget-object v3, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->customerInitialsHelper:Lcom/squareup/crm/util/CustomerInitialsHelper;

    invoke-virtual {v3, v2}, Lcom/squareup/crm/util/CustomerInitialsHelper;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 181
    new-instance v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$IconInitialsTitleValue;

    .line 182
    iget-object v4, v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->name:Ljava/lang/String;

    const-string v5, "frequencyCount.name"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    iget-object v1, v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->count:Ljava/lang/Long;

    const-string v5, "frequencyCount.count"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-direct {p0, v5, v6}, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->numberOfVisitsTerm(J)Ljava/lang/String;

    move-result-object v1

    .line 181
    invoke-direct {v3, v4, v1, v2}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$IconInitialsTitleValue;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 258
    :cond_0
    check-cast v0, Ljava/util/List;

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    .line 188
    :goto_1
    move-object p1, v0

    check-cast p1, Ljava/util/Collection;

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 p1, 0x1

    :goto_3
    if-eqz p1, :cond_4

    .line 190
    new-instance p1, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    const-string v3, ""

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;-><init>(Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 189
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :cond_4
    return-object v0
.end method

.method private final formattedCount(Ljava/util/List;Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;",
            ">;",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    if-nez p1, :cond_0

    return-object p3

    .line 145
    :cond_0
    check-cast p1, Ljava/lang/Iterable;

    .line 250
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;

    .line 146
    iget-object v1, v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->cohort_type:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    if-ne v1, p2, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    .line 251
    :goto_1
    check-cast v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;

    if-eqz v0, :cond_4

    iget-object p1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->count:Ljava/lang/Double;

    if-eqz p1, :cond_4

    .line 149
    iget-object p2, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->singleDigitFormatter:Lcom/squareup/text/Formatter;

    check-cast p1, Ljava/lang/Number;

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 150
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_4

    goto :goto_2

    :cond_4
    move-object p1, p3

    :goto_2
    return-object p1
.end method

.method private final formattedMoney(Ljava/util/List;Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;",
            ">;",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    if-nez p1, :cond_0

    return-object p3

    .line 161
    :cond_0
    check-cast p1, Ljava/lang/Iterable;

    .line 252
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;

    .line 162
    iget-object v1, v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->cohort_type:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    if-ne v1, p2, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    .line 253
    :goto_1
    check-cast v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;

    if-eqz v0, :cond_4

    iget-object p1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;->money_amount:Lcom/squareup/protos/common/Money;

    if-eqz p1, :cond_4

    .line 165
    iget-object p2, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 166
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_4

    goto :goto_2

    :cond_4
    move-object p1, p3

    :goto_2
    return-object p1
.end method

.method private final numberOfVisitsTerm(J)Ljava/lang/String;
    .locals 4

    .line 201
    iget-object v0, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->singleDigitFormatter:Lcom/squareup/text/Formatter;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 202
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x1

    cmp-long v3, p1, v1

    if-nez v3, :cond_0

    .line 204
    iget-object p1, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->visitsPhraseSingular:Lcom/squareup/phrase/Phrase;

    goto :goto_0

    .line 206
    :cond_0
    iget-object p1, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->visitsPhrasePlural:Lcom/squareup/phrase/Phrase;

    .line 207
    :goto_0
    check-cast v0, Ljava/lang/CharSequence;

    const-string p2, "number"

    invoke-virtual {p1, p2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 208
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 209
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public final transform(Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;)Lcom/squareup/loyaltyreport/ui/LoyaltyReport;
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const-string v3, "requestedDateRange"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "getLoyaltyReportResponse"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v3, v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_summary:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 49
    iget-object v4, v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_average_visits:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 50
    iget-object v5, v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_average_spend:I

    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 51
    iget-object v6, v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->res:Lcom/squareup/util/Res;

    sget v7, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_top_customers:I

    invoke-interface {v6, v7}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 52
    iget-object v7, v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->res:Lcom/squareup/util/Res;

    sget v8, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_rewards_redeemed:I

    invoke-interface {v7, v8}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 54
    iget-object v8, v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->res:Lcom/squareup/util/Res;

    sget v9, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_not_available_shorthand:I

    invoke-interface {v8, v9}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 55
    iget-object v9, v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->res:Lcom/squareup/util/Res;

    sget v10, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_loyalty_customers:I

    invoke-interface {v9, v10}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 56
    iget-object v10, v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->res:Lcom/squareup/util/Res;

    sget v11, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_non_loyalty_customers:I

    invoke-interface {v10, v11}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 57
    iget-object v11, v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->res:Lcom/squareup/util/Res;

    sget v12, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_not_available:I

    invoke-interface {v11, v12}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 59
    iget-object v11, v1, Lcom/squareup/protos/common/time/DateTimeInterval;->inclusive_start:Lcom/squareup/protos/common/time/DateTime;

    const-string v12, "requestedDateRange.inclusive_start"

    invoke-static {v11, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v12, v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    invoke-interface {v12}, Lcom/squareup/time/CurrentTimeZone;->zoneId()Lorg/threeten/bp/ZoneId;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/squareup/util/threeten/DateTimesKt;->toLocalDateTime(Lcom/squareup/protos/common/time/DateTime;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v11

    .line 60
    iget-object v1, v1, Lcom/squareup/protos/common/time/DateTimeInterval;->exclusive_end:Lcom/squareup/protos/common/time/DateTime;

    const-string v12, "requestedDateRange.exclusive_end"

    invoke-static {v1, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v12, v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    invoke-interface {v12}, Lcom/squareup/time/CurrentTimeZone;->zoneId()Lorg/threeten/bp/ZoneId;

    move-result-object v12

    invoke-static {v1, v12}, Lcom/squareup/util/threeten/DateTimesKt;->toLocalDateTime(Lcom/squareup/protos/common/time/DateTime;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    .line 62
    iget-object v12, v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v12}, Lcom/squareup/loyalty/LoyaltySettings;->getSubunitName()Ljava/lang/String;

    move-result-object v12

    const-string v13, ""

    if-eqz v12, :cond_0

    goto :goto_0

    :cond_0
    move-object v12, v13

    :goto_0
    const-string v14, "loyaltySettings.subunitName ?: \"\""

    invoke-static {v12, v14}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v14, v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->dateRangePhrase:Lcom/squareup/phrase/Phrase;

    move-object/from16 v16, v15

    .line 65
    iget-object v15, v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->dateTimeFormatter:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;

    invoke-virtual {v15, v11}, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;->format(Lorg/threeten/bp/LocalDateTime;)Ljava/lang/String;

    move-result-object v11

    check-cast v11, Ljava/lang/CharSequence;

    const-string v15, "start_date"

    invoke-virtual {v14, v15, v11}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v11

    .line 66
    iget-object v14, v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->dateTimeFormatter:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;

    invoke-virtual {v14, v1}, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;->format(Lorg/threeten/bp/LocalDateTime;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v14, "end_date"

    invoke-virtual {v11, v14, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 67
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 68
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 70
    iget-object v11, v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->singleDigitFormatter:Lcom/squareup/text/Formatter;

    .line 71
    iget-object v14, v2, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->loyalty_customer_count:Ljava/lang/Long;

    const/4 v15, 0x0

    if-eqz v14, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    :goto_1
    check-cast v14, Ljava/lang/Number;

    .line 70
    invoke-interface {v11, v14}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v11

    .line 73
    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    .line 75
    iget-object v14, v2, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->average_visits:Ljava/util/List;

    .line 76
    sget-object v15, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->COHORT_LOYALTY:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    invoke-direct {v0, v14, v15, v8}, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->formattedCount(Ljava/util/List;Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v18, v7

    .line 77
    sget-object v7, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->COHORT_NON_LOYALTY:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    invoke-direct {v0, v14, v7, v8}, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->formattedCount(Ljava/util/List;Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 79
    iget-object v14, v2, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->average_spend:Ljava/util/List;

    .line 80
    sget-object v2, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->COHORT_LOYALTY:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    invoke-direct {v0, v14, v2, v8}, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->formattedMoney(Ljava/util/List;Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v19, v6

    .line 81
    sget-object v6, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->COHORT_NON_LOYALTY:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    invoke-direct {v0, v14, v6, v8}, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->formattedMoney(Ljava/util/List;Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/16 v8, 0xa

    new-array v8, v8, [Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;

    .line 84
    new-instance v14, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$DateRangeTitle;

    invoke-direct {v14, v1, v12}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$DateRangeTitle;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v14, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;

    const/4 v1, 0x0

    aput-object v14, v8, v1

    const/4 v1, 0x1

    .line 88
    new-instance v12, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$SectionHeader;

    invoke-direct {v12, v3}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$SectionHeader;-><init>(Ljava/lang/String;)V

    check-cast v12, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;

    aput-object v12, v8, v1

    const/4 v1, 0x2

    .line 91
    new-instance v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$Summary;

    invoke-direct {v3, v11, v15, v2}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$Summary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;

    aput-object v3, v8, v1

    const/4 v1, 0x3

    .line 96
    new-instance v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$ViewInDashboard;

    invoke-direct {v3, v13}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$ViewInDashboard;-><init>(Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;

    aput-object v3, v8, v1

    const/4 v1, 0x4

    .line 99
    new-instance v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$SectionHeader;

    invoke-direct {v3, v4}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$SectionHeader;-><init>(Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;

    aput-object v3, v8, v1

    const/4 v1, 0x5

    .line 102
    new-instance v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;

    const/4 v14, 0x0

    const/4 v4, 0x4

    const/16 v17, 0x0

    move-object v11, v3

    move-object v12, v9

    move-object v13, v15

    move-object/from16 v20, v16

    move v15, v4

    move-object/from16 v16, v17

    invoke-direct/range {v11 .. v16}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;-><init>(Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;

    aput-object v3, v8, v1

    const/4 v1, 0x6

    .line 106
    new-instance v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;

    const/4 v15, 0x0

    const/16 v16, 0x4

    move-object v12, v3

    move-object v13, v10

    move-object v14, v7

    invoke-direct/range {v12 .. v17}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;-><init>(Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;

    aput-object v3, v8, v1

    const/4 v1, 0x7

    .line 110
    new-instance v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$SectionHeader;

    invoke-direct {v3, v5}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$SectionHeader;-><init>(Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;

    aput-object v3, v8, v1

    const/16 v1, 0x8

    .line 113
    new-instance v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;

    const/4 v14, 0x0

    const/4 v15, 0x4

    const/16 v16, 0x0

    move-object v11, v3

    move-object v12, v9

    move-object v13, v2

    invoke-direct/range {v11 .. v16}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;-><init>(Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;

    aput-object v3, v8, v1

    const/16 v1, 0x9

    .line 117
    new-instance v2, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;

    const/4 v15, 0x0

    const/16 v16, 0x4

    move-object v12, v2

    move-object v13, v10

    move-object v14, v6

    invoke-direct/range {v12 .. v17}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;-><init>(Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v2, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;

    aput-object v2, v8, v1

    .line 83
    invoke-static {v8}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 124
    new-instance v2, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$SectionHeader;

    move-object/from16 v3, v19

    invoke-direct {v2, v3}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$SectionHeader;-><init>(Ljava/lang/String;)V

    .line 123
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    move-object/from16 v3, p2

    move-object/from16 v4, v20

    .line 127
    invoke-direct {v0, v3, v4}, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->createTopCustomersList(Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/lang/Iterable;

    invoke-static {v2, v5}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    .line 130
    new-instance v5, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$SectionHeader;

    move-object/from16 v6, v18

    invoke-direct {v5, v6}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$SectionHeader;-><init>(Ljava/lang/String;)V

    .line 129
    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/util/Collection;

    .line 133
    invoke-direct {v0, v3, v4}, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->createRewardsRedeemedList(Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v5, v3}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v3

    .line 135
    new-instance v4, Lcom/squareup/loyaltyreport/ui/LoyaltyReport;

    check-cast v1, Ljava/util/Collection;

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v4, v1}, Lcom/squareup/loyaltyreport/ui/LoyaltyReport;-><init>(Ljava/util/List;)V

    return-object v4
.end method
