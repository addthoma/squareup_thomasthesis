.class public interface abstract Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$Runner;
.super Ljava/lang/Object;
.source "RecordPaymentScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J\u0008\u0010\u0005\u001a\u00020\u0003H&J\u000e\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000bH&J\u0010\u0010\u000c\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u000eH&J\u0010\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0011H&\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$Runner;",
        "",
        "choosePaymentMethod",
        "",
        "goBackFromRecordPaymentScreen",
        "recordPayment",
        "recordPaymentScreenData",
        "Lrx/Observable;",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;",
        "updateAmount",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "updateNote",
        "note",
        "",
        "updateSendReceipt",
        "sendReceipt",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract choosePaymentMethod()V
.end method

.method public abstract goBackFromRecordPaymentScreen()V
.end method

.method public abstract recordPayment()V
.end method

.method public abstract recordPaymentScreenData()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateAmount(Lcom/squareup/protos/common/Money;)V
.end method

.method public abstract updateNote(Ljava/lang/String;)V
.end method

.method public abstract updateSendReceipt(Z)V
.end method
