.class final Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPaymentMethodScreenData$1;
.super Ljava/lang/Object;
.source "RecordPaymentScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->recordPaymentMethodScreenData()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;",
        "it",
        "Lcom/squareup/invoices/RecordPaymentInfo;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPaymentMethodScreenData$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPaymentMethodScreenData$1;

    invoke-direct {v0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPaymentMethodScreenData$1;-><init>()V

    sput-object v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPaymentMethodScreenData$1;->INSTANCE:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPaymentMethodScreenData$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/invoices/RecordPaymentInfo;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;
    .locals 2

    .line 129
    new-instance v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;

    .line 130
    invoke-static {}, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->values()[Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    move-result-object v1

    invoke-static {v1}, Lkotlin/collections/ArraysKt;->toList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/invoices/RecordPaymentInfo;->getPaymentMethod()Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    move-result-object p1

    .line 129
    invoke-direct {v0, v1, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;-><init>(Ljava/util/List;Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/invoices/RecordPaymentInfo;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPaymentMethodScreenData$1;->call(Lcom/squareup/invoices/RecordPaymentInfo;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;

    move-result-object p1

    return-object p1
.end method
