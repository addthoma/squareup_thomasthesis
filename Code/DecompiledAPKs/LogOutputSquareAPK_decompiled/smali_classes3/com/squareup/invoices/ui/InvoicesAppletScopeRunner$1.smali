.class synthetic Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;
.super Ljava/lang/Object;
.source "InvoicesAppletScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

.field static final synthetic $SwitchMap$com$squareup$protos$client$invoice$InvoiceTimeline$CallToActionTarget:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 1118
    invoke-static {}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->values()[Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceTimeline$CallToActionTarget:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceTimeline$CallToActionTarget:[I

    sget-object v2, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->SEND_REMINDER:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceTimeline$CallToActionTarget:[I

    sget-object v3, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->URL:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    invoke-virtual {v3}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceTimeline$CallToActionTarget:[I

    sget-object v4, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->LINK:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    invoke-virtual {v4}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    .line 563
    :catch_2
    invoke-static {}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->values()[Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

    :try_start_3
    sget-object v3, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

    sget-object v4, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->SHARE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    invoke-virtual {v4}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

    sget-object v3, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->SEND_REMINDER:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    invoke-virtual {v3}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ordinal()I

    move-result v3

    aput v1, v0, v3
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->VIEW_TRANSACTION:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->DUPLICATE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->CANCEL:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ISSUE_REFUND:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    :try_start_9
    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->VIEW_IN_SERIES:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->END_SERIES:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    :try_start_b
    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->MORE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :catch_b
    :try_start_c
    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->TIMELINE_VIEW_ALL_ACTIVITY:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_c

    :catch_c
    :try_start_d
    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->DOWNLOAD_INVOICE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_d

    :catch_d
    :try_start_e
    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->DELETE_DRAFT:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_e

    :catch_e
    :try_start_f
    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ARCHIVE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_f

    :catch_f
    :try_start_10
    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->UNARCHIVE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_10

    :catch_10
    return-void
.end method
