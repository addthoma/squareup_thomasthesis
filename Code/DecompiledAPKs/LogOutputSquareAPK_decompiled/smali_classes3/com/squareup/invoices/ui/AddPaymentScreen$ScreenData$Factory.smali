.class public final Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;
.super Ljava/lang/Object;
.source "AddPaymentScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAddPaymentScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AddPaymentScreen.kt\ncom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory\n*L\n1#1,144:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0018\u00002\u00020\u0001B-\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u000e\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fJ\u0008\u0010\u0010\u001a\u00020\u0011H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;",
        "",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "res",
        "Lcom/squareup/util/Res;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V",
        "create",
        "Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;",
        "invoiceDisplayDetails",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
        "handCoinIcon",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p2, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private final handCoinIcon()I
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    sget-object v1, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 100
    sget v0, Lcom/squareup/features/invoices/R$drawable;->hand_coin:I

    goto :goto_0

    .line 99
    :cond_0
    sget v0, Lcom/squareup/features/invoices/R$drawable;->hand_coin_pound:I

    goto :goto_0

    .line 98
    :cond_1
    sget v0, Lcom/squareup/features/invoices/R$drawable;->hand_coin_yen:I

    :goto_0
    return v0
.end method


# virtual methods
.method public final create(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "invoiceDisplayDetails"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/List;

    .line 53
    iget-object v3, v0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RECORD_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v3, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 56
    new-instance v3, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;

    .line 57
    iget-object v4, v0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/features/invoices/R$string;->record_payment:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 58
    iget-object v4, v0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/features/invoices/R$string;->record_payment_description:I

    invoke-interface {v4, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 59
    invoke-direct/range {p0 .. p0}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->handCoinIcon()I

    move-result v7

    .line 60
    new-instance v8, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;

    iget-object v4, v0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v9, Lcom/squareup/features/invoices/R$string;->record_payment:I

    invoke-interface {v4, v9}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v9, Lcom/squareup/invoices/ui/AddPaymentScreen$SectionActionType;->RECORD_PAYMENT:Lcom/squareup/invoices/ui/AddPaymentScreen$SectionActionType;

    invoke-direct {v8, v4, v9}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;-><init>(Ljava/lang/String;Lcom/squareup/invoices/ui/AddPaymentScreen$SectionActionType;)V

    const/4 v9, 0x0

    const/16 v10, 0x10

    const/4 v11, 0x0

    move-object v4, v3

    .line 56
    invoke-direct/range {v4 .. v11}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 55
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    :cond_0
    iget-object v3, v0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1, v3}, Lcom/squareup/invoices/Invoices;->isPartiallyPaid(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/CurrencyCode;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 68
    iget-object v3, v0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v3, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 69
    iget-object v3, v0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->charge_amount_now:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    const-string v4, "amount"

    .line 70
    invoke-virtual {v3, v4, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 71
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 72
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 74
    new-instance v1, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;

    .line 76
    iget-object v3, v0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/features/invoices/R$string;->charge_amount_now_description:I

    invoke-interface {v3, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 77
    sget v6, Lcom/squareup/features/invoices/R$drawable;->card_chip:I

    .line 78
    new-instance v7, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;

    sget-object v3, Lcom/squareup/invoices/ui/AddPaymentScreen$SectionActionType;->CHARGE_NOW:Lcom/squareup/invoices/ui/AddPaymentScreen$SectionActionType;

    invoke-direct {v7, v4, v3}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;-><init>(Ljava/lang/String;Lcom/squareup/invoices/ui/AddPaymentScreen$SectionActionType;)V

    const/4 v8, 0x0

    const/16 v9, 0x10

    const/4 v10, 0x0

    move-object v3, v1

    .line 74
    invoke-direct/range {v3 .. v10}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 73
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 83
    :cond_1
    new-instance v1, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;

    .line 84
    iget-object v3, v0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->payment_type_need_to_charge_card:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 85
    iget-object v3, v0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->payment_type_need_to_charge_card_subtitle:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 86
    sget v14, Lcom/squareup/features/invoices/R$drawable;->card_chip:I

    .line 87
    new-instance v15, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;

    .line 88
    iget-object v3, v0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->payment_type_share_payment_link:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/squareup/invoices/ui/AddPaymentScreen$SectionActionType;->SHARE_LINK:Lcom/squareup/invoices/ui/AddPaymentScreen$SectionActionType;

    .line 87
    invoke-direct {v15, v3, v4}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;-><init>(Ljava/lang/String;Lcom/squareup/invoices/ui/AddPaymentScreen$SectionActionType;)V

    .line 90
    iget-object v3, v0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->charge_now_disabled_description:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object v11, v1

    .line 83
    invoke-direct/range {v11 .. v16}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;Ljava/lang/String;)V

    .line 82
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    :goto_0
    new-instance v1, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;

    invoke-direct {v1, v2}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;-><init>(Ljava/util/List;)V

    return-object v1
.end method
