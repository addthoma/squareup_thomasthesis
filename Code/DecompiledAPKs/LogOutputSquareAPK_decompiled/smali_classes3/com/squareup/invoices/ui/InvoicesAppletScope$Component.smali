.class public interface abstract Lcom/squareup/invoices/ui/InvoicesAppletScope$Component;
.super Ljava/lang/Object;
.source "InvoicesAppletScope.java"

# interfaces
.implements Lcom/squareup/ui/crm/flow/CrmScope$ParentInvoicesAppletScopeComponent;
.implements Lcom/squareup/ui/activity/IssueRefundScope$ParentComponent;
.implements Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope$ParentComponent;
.implements Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$ParentComponent;
.implements Lcom/squareup/invoices/ui/CancelInvoiceScreen$ParentComponent;
.implements Lcom/squareup/invoices/InvoiceTimelineScreen$ParentComponent;
.implements Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ParentComponent;
.implements Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner$ParentComponent;
.implements Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$ParentComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/invoices/ui/InvoicesAppletScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoicesAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract addPaymentCoordinator()Lcom/squareup/invoices/ui/AddPaymentCoordinator;
.end method

.method public abstract bottomDialogCoordinator()Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;
.end method

.method public abstract inject(Lcom/squareup/ui/cart/CartEntryView;)V
.end method

.method public abstract invoiceBillHistoryScope()Lcom/squareup/invoices/ui/InvoiceBillHistoryScope$Component;
.end method

.method public abstract invoiceDetailScreen()Lcom/squareup/invoices/ui/InvoiceDetailScreen$Component;
.end method

.method public abstract invoiceFilterMasterScreen()Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Component;
.end method

.method public abstract invoiceHistoryScreen()Lcom/squareup/invoices/ui/InvoiceHistoryScreen$Component;
.end method

.method public abstract scopeRunner()Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;
.end method

.method public abstract sendReminderCoordinator()Lcom/squareup/invoices/ui/SendReminderCoordinator;
.end method
