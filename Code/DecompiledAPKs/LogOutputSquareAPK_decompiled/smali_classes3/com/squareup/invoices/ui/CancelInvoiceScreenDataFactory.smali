.class public final Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;
.super Ljava/lang/Object;
.source "CancelInvoiceScreen.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCancelInvoiceScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CancelInvoiceScreen.kt\ncom/squareup/invoices/ui/CancelInvoiceScreenDataFactory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,110:1\n250#2,2:111\n*E\n*S KotlinDebug\n*F\n+ 1 CancelInvoiceScreen.kt\ncom/squareup/invoices/ui/CancelInvoiceScreenDataFactory\n*L\n83#1,2:111\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J \u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "create",
        "Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;",
        "invoiceDisplayDetails",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
        "issueRefundAfterCancel",
        "",
        "isBusy",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static synthetic create$default(Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;ZZILjava/lang/Object;)Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 76
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;->create(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;ZZ)Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final create(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;ZZ)Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;
    .locals 12

    const-string v0, "invoiceDisplayDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->forInvoiceDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/InvoiceDisplayState;

    move-result-object v0

    .line 81
    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->SCHEDULED:Lcom/squareup/invoices/InvoiceDisplayState;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->RECURRING:Lcom/squareup/invoices/InvoiceDisplayState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->UNDELIVERED:Lcom/squareup/invoices/InvoiceDisplayState;

    if-eq v0, v1, :cond_0

    const/4 v9, 0x1

    goto :goto_0

    :cond_0
    const/4 v9, 0x0

    .line 82
    :goto_0
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    const-string v0, "invoiceDisplayDetails\n  \u2026 .invoice.payment_request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 111
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 83
    iget-object v4, v4, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v5, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne v4, v5, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_1

    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 112
    :goto_2
    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    if-eqz v0, :cond_4

    iget-object v1, v0, Lcom/squareup/protos/client/invoice/PaymentRequest;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    :cond_4
    sget-object p1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eq v1, p1, :cond_5

    const/4 v10, 0x1

    goto :goto_3

    :cond_5
    const/4 v10, 0x0

    .line 86
    :goto_3
    iget-object p1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_edit_cancel:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    if-eqz p2, :cond_6

    .line 88
    iget-object p1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->cancel_and_issue_refund:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    .line 90
    :cond_6
    iget-object p1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_edit_cancel:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_4
    move-object v6, p1

    if-eqz p2, :cond_7

    .line 93
    iget-object p1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;->res:Lcom/squareup/util/Res;

    .line 94
    sget v0, Lcom/squareup/features/invoices/R$string;->cancel_invoice_confirmation_text_partial_payment:I

    .line 93
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_5

    .line 97
    :cond_7
    iget-object p1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->cancel_invoice_confirmation_text:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_5
    move-object v7, p1

    .line 99
    new-instance p1, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;

    move-object v4, p1

    move v8, p2

    move v11, p3

    invoke-direct/range {v4 .. v11}, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    return-object p1
.end method
