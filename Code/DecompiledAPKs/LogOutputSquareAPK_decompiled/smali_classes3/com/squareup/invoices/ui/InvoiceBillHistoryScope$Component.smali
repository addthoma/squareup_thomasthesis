.class public interface abstract Lcom/squareup/invoices/ui/InvoiceBillHistoryScope$Component;
.super Ljava/lang/Object;
.source "InvoiceBillHistoryScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/invoices/ui/InvoiceBillHistoryScope$BillHistoryModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract billHistoryDetailForInvoiceScreen()Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Component;
.end method

.method public abstract invoiceBillHistoryScopeRunner()Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;
.end method

.method public abstract issueReceiptScreen()Lcom/squareup/ui/activity/IssueReceiptScreen$Component;
.end method
