.class public interface abstract Lcom/squareup/invoices/ui/InvoiceHistoryScreen$Component;
.super Ljava/lang/Object;
.source "InvoiceHistoryScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceHistoryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/invoices/ClientInvoiceServiceHelper;)V
.end method

.method public abstract inject(Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;)V
.end method

.method public abstract inject(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V
.end method
