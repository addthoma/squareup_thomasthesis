.class final Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator$configureActionBar$configBuilder$1;
.super Ljava/lang/Object;
.source "RecordPaymentMethodCoordinator.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->configureActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator$configureActionBar$configBuilder$1;->this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 61
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator$configureActionBar$configBuilder$1;->this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->access$getRunner$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;

    move-result-object v0

    .line 62
    invoke-static {}, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->values()[Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator$configureActionBar$configBuilder$1;->this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;

    invoke-static {v2}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->access$getPaymentMethodContainer$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;)Lcom/squareup/widgets/CheckableGroup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/widgets/CheckableGroup;->getCheckedId()I

    move-result v2

    aget-object v1, v1, v2

    .line 61
    invoke-interface {v0, v1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;->goBackFromRecordPaymentMethodScreen(Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;)V

    return-void
.end method
