.class public abstract Lcom/squareup/invoices/ui/InvoiceBillHistoryScope$BillHistoryModule;
.super Ljava/lang/Object;
.source "InvoiceBillHistoryScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BillHistoryModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideBillHistoryCardController(Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;)Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideBillHistoryController(Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;)Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTenderWithCustomerInfoCache(Lcom/squareup/ui/activity/billhistory/RealTenderWithCustomerInfoCache;)Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
