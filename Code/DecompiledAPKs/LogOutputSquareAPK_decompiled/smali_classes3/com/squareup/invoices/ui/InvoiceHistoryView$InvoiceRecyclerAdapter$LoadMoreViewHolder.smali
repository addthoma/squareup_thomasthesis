.class final Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$LoadMoreViewHolder;
.super Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;
.source "InvoiceHistoryView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LoadMoreViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\u000f\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$LoadMoreViewHolder;",
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;",
        "loadMoreRow",
        "Lcom/squareup/ui/LoadMoreRow;",
        "(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Lcom/squareup/ui/LoadMoreRow;)V",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Lcom/squareup/ui/LoadMoreRow;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/LoadMoreRow;",
            ")V"
        }
    .end annotation

    const-string v0, "loadMoreRow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 823
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$LoadMoreViewHolder;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    .line 824
    move-object v0, p2

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 827
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v0}, Lcom/squareup/ui/LoadMoreRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 828
    invoke-static {p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->access$getLoadMoreState$p(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;)Lcom/squareup/ui/LoadMoreState;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/ui/LoadMoreRow;->setLoadMore(Lcom/squareup/ui/LoadMoreState;)V

    return-void
.end method
