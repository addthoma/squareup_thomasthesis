.class public final Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory_Factory;
.super Ljava/lang/Object;
.source "InvoicePaymentAmountViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final otherAmountDialogFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory_Factory;->otherAmountDialogFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;",
            ">;)",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;-><init>(Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory_Factory;->otherAmountDialogFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;

    invoke-static {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory_Factory;->newInstance(Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory_Factory;->get()Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;

    move-result-object v0

    return-object v0
.end method
