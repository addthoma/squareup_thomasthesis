.class public final Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;
.super Lcom/squareup/invoices/ui/InInvoicesAppletScope;
.source "RecordPaymentScope.kt"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope$ParentComponent;,
        Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope$Component;,
        Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope$AddPaymentModule;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecordPaymentScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecordPaymentScope.kt\ncom/squareup/invoices/ui/recordpayment/RecordPaymentScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,59:1\n35#2:60\n24#3,4:61\n*E\n*S KotlinDebug\n*F\n+ 1 RecordPaymentScope.kt\ncom/squareup/invoices/ui/recordpayment/RecordPaymentScope\n*L\n23#1:60\n57#1,4:61\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u00c7\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0003\n\u000b\u000cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016R\u0016\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;",
        "Lcom/squareup/invoices/ui/InInvoicesAppletScope;",
        "Lcom/squareup/container/RegistersInScope;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "register",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "AddPaymentModule",
        "Component",
        "ParentComponent",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;

    invoke-direct {v0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;-><init>()V

    sput-object v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;->INSTANCE:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;

    .line 61
    new-instance v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 64
    sput-object v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InInvoicesAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    const-class v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 23
    check-cast v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope$Component;

    .line 24
    invoke-interface {v0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope$Component;->addPaymentScopeRunner()Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;

    move-result-object v0

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
