.class public final Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;
.super Ljava/lang/Object;
.source "InvoiceBillHistoryScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceBillHistoryLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceBillLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceIssueRefundStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final orderPrintingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceBillLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;->invoiceBillHistoryLoaderProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;->invoiceIssueRefundStarterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceBillLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;",
            ">;)",
            "Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;"
        }
    .end annotation

    .line 55
    new-instance v7, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/invoices/ui/InvoiceBillLoader;Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;)Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;
    .locals 8

    .line 62
    new-instance v7, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;-><init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/invoices/ui/InvoiceBillLoader;Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;
    .locals 7

    .line 47
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;->invoiceBillHistoryLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/invoices/ui/InvoiceBillLoader;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;->invoiceIssueRefundStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;

    invoke-static/range {v1 .. v6}, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;->newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/invoices/ui/InvoiceBillLoader;Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;)Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner_Factory;->get()Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;

    move-result-object v0

    return-object v0
.end method
