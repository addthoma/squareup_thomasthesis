.class public final Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;
.super Ljava/lang/Object;
.source "invoiceDetailTimelineDataFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;",
        "",
        "()V",
        "create",
        "Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;",
        "invoice",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
        "minify",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final create(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Z)Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;
    .locals 8

    const-string v0, "invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-static {p1}, Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactoryKt;->timelineBackgroundColor(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)I

    move-result v4

    .line 20
    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;

    .line 21
    sget-object v2, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->TIMELINE_CTA:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    .line 22
    sget-object v3, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->TIMELINE_VIEW_ALL_ACTIVITY:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    .line 24
    iget-object v5, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    const-string v1, "invoice.invoice_timeline"

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object v6, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->event:Ljava/util/List;

    const-string p1, "invoice.event"

    invoke-static {v6, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    move v7, p2

    .line 20
    invoke-direct/range {v1 .. v7}, Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;-><init>(Ljava/lang/Object;Ljava/lang/Object;ILcom/squareup/protos/client/invoice/InvoiceTimeline;Ljava/util/List;Z)V

    return-object v0
.end method
