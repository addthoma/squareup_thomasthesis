.class public interface abstract Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;
.super Ljava/lang/Object;
.source "InvoicesAppletConfirmationScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;",
        "",
        "confirmationScreenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;",
        "goBackFromConfirmationScreen",
        "",
        "success",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract confirmationScreenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract goBackFromConfirmationScreen(Z)V
.end method
