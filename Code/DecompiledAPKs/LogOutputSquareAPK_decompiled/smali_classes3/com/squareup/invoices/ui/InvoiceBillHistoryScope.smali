.class public Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;
.super Lcom/squareup/invoices/ui/InInvoicesAppletScope;
.source "InvoiceBillHistoryScope.java"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/invoices/ui/InvoiceBillHistoryScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/InvoiceBillHistoryScope$Component;,
        Lcom/squareup/invoices/ui/InvoiceBillHistoryScope$BillHistoryModule;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;

    invoke-direct {v0}, Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;-><init>()V

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;

    .line 50
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InInvoicesAppletScope;-><init>()V

    return-void
.end method
