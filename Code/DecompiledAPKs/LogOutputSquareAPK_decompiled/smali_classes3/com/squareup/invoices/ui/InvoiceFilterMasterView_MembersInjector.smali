.class public final Lcom/squareup/invoices/ui/InvoiceFilterMasterView_MembersInjector;
.super Ljava/lang/Object;
.source "InvoiceFilterMasterView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/invoices/ui/InvoiceFilterMasterView;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarNavigationHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final appletSelectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final badgePresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView_MembersInjector;->badgePresenterProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView_MembersInjector;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView_MembersInjector;->appletSelectionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/invoices/ui/InvoiceFilterMasterView;",
            ">;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView_MembersInjector;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectActionBarNavigationHelper(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;Lcom/squareup/applet/ActionBarNavigationHelper;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    return-void
.end method

.method public static injectAppletSelection(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;Lcom/squareup/applet/AppletSelection;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->appletSelection:Lcom/squareup/applet/AppletSelection;

    return-void
.end method

.method public static injectBadgePresenter(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;Lcom/squareup/applet/BadgePresenter;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;Ljava/lang/Object;)V
    .locals 0

    .line 61
    check-cast p1, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->presenter:Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;)V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView_MembersInjector;->badgePresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/BadgePresenter;

    invoke-static {p1, v0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView_MembersInjector;->injectBadgePresenter(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;Lcom/squareup/applet/BadgePresenter;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView_MembersInjector;->injectPresenter(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;Ljava/lang/Object;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView_MembersInjector;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/ActionBarNavigationHelper;

    invoke-static {p1, v0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView_MembersInjector;->injectActionBarNavigationHelper(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;Lcom/squareup/applet/ActionBarNavigationHelper;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView_MembersInjector;->appletSelectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletSelection;

    invoke-static {p1, v0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView_MembersInjector;->injectAppletSelection(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;Lcom/squareup/applet/AppletSelection;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView_MembersInjector;->injectMembers(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;)V

    return-void
.end method
