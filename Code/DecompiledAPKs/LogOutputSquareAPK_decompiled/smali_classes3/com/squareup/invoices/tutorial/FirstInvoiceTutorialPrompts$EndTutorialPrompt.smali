.class Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EndTutorialPrompt;
.super Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$FirstInvoiceTutorialPrompt;
.source "FirstInvoiceTutorialPrompts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "EndTutorialPrompt"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EndTutorialPrompt;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final content:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 126
    new-instance v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EndTutorialPrompt$1;

    invoke-direct {v0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EndTutorialPrompt$1;-><init>()V

    sput-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EndTutorialPrompt;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(I)V
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$FirstInvoiceTutorialPrompt;-><init>()V

    .line 81
    iput p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EndTutorialPrompt;->content:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$FirstInvoiceTutorialPrompt;-><init>()V

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EndTutorialPrompt;->content:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$1;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EndTutorialPrompt;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    .line 116
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_2

    return v1

    .line 117
    :cond_2
    check-cast p1, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EndTutorialPrompt;

    .line 118
    iget v2, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EndTutorialPrompt;->content:I

    iget p1, p1, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EndTutorialPrompt;->content:I

    if-eq v2, p1, :cond_3

    return v1

    :cond_3
    return v0
.end method

.method public getContent(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1

    .line 93
    iget v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EndTutorialPrompt;->content:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getPrimaryButton()I
    .locals 1

    .line 97
    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_done:I

    return v0
.end method

.method public getSecondaryButton()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1

    .line 89
    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_title:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method handleTap(Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;)V
    .locals 0

    .line 106
    invoke-virtual {p2}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->exitQuietly()V

    return-void
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 110
    iget v1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EndTutorialPrompt;->content:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 123
    iget p2, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EndTutorialPrompt;->content:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
