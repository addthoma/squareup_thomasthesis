.class final Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$2;
.super Ljava/lang/Object;
.source "InvoiceDefaultMessageUpdater.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;->update(Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/protos/client/invoice/UpdateUnitMetadataResponse;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0012\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/invoice/UpdateUnitMetadataResponse;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $message:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$2;->this$0:Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;

    iput-object p2, p0, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$2;->$message:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/UpdateUnitMetadataResponse;",
            ">;)V"
        }
    .end annotation

    .line 38
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$2;->this$0:Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;

    invoke-static {p1}, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;->access$getInvoiceUnitCache$p(Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;)Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$2;->$message:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->updateDefaultMessage(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$2;->accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method
