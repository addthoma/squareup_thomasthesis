.class public abstract Lcom/squareup/invoices/NoEstimatesModule;
.super Ljava/lang/Object;
.source "NoEstimatesModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH!\u00a2\u0006\u0002\u0008\u000cJ\u0015\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H!\u00a2\u0006\u0002\u0008\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/invoices/NoEstimatesModule;",
        "",
        "()V",
        "provideEstimateFileAttachmentService",
        "Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;",
        "noEstimatesFileAttachments",
        "Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper$NoEstimateFileAttachmentServiceHelper;",
        "provideEstimateFileAttachmentService$invoices_hairball_release",
        "provideEstimatesGateway",
        "Lcom/squareup/invoices/timeline/EstimatesGateway;",
        "gateway",
        "Lcom/squareup/invoices/timeline/NoEstimatesGateway;",
        "provideEstimatesGateway$invoices_hairball_release",
        "provideTimelineCtaEnabled",
        "Lcom/squareup/TimelineCtaEnabled;",
        "noEstimatesTimelineCtaEnabled",
        "Lcom/squareup/invoices/timeline/NoEstimatesTimelineCtaEnabled;",
        "provideTimelineCtaEnabled$invoices_hairball_release",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract provideEstimateFileAttachmentService$invoices_hairball_release(Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper$NoEstimateFileAttachmentServiceHelper;)Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract provideEstimatesGateway$invoices_hairball_release(Lcom/squareup/invoices/timeline/NoEstimatesGateway;)Lcom/squareup/invoices/timeline/EstimatesGateway;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract provideTimelineCtaEnabled$invoices_hairball_release(Lcom/squareup/invoices/timeline/NoEstimatesTimelineCtaEnabled;)Lcom/squareup/TimelineCtaEnabled;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
