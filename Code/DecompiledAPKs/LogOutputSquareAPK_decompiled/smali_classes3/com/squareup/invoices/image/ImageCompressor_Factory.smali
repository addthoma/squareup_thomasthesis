.class public final Lcom/squareup/invoices/image/ImageCompressor_Factory;
.super Ljava/lang/Object;
.source "ImageCompressor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/image/ImageCompressor;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final fileThreadSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final tempPhotoDirProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/invoices/image/ImageCompressor_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/invoices/image/ImageCompressor_Factory;->fileThreadSchedulerProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/invoices/image/ImageCompressor_Factory;->tempPhotoDirProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/invoices/image/ImageCompressor_Factory;->applicationProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/image/ImageCompressor_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/invoices/image/ImageCompressor_Factory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/invoices/image/ImageCompressor_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/image/ImageCompressor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Ljava/io/File;Landroid/app/Application;)Lcom/squareup/invoices/image/ImageCompressor;
    .locals 1

    .line 49
    new-instance v0, Lcom/squareup/invoices/image/ImageCompressor;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/image/ImageCompressor;-><init>(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Ljava/io/File;Landroid/app/Application;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/image/ImageCompressor;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/invoices/image/ImageCompressor_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/Scheduler;

    iget-object v1, p0, Lcom/squareup/invoices/image/ImageCompressor_Factory;->fileThreadSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/Scheduler;

    iget-object v2, p0, Lcom/squareup/invoices/image/ImageCompressor_Factory;->tempPhotoDirProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    iget-object v3, p0, Lcom/squareup/invoices/image/ImageCompressor_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Application;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/invoices/image/ImageCompressor_Factory;->newInstance(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Ljava/io/File;Landroid/app/Application;)Lcom/squareup/invoices/image/ImageCompressor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/invoices/image/ImageCompressor_Factory;->get()Lcom/squareup/invoices/image/ImageCompressor;

    move-result-object v0

    return-object v0
.end method
