.class final Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$2;
.super Ljava/lang/Object;
.source "ImageUploader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/image/ImageUploader;->uploadFileToInvoice(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/io/File;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/invoices/image/FileUploadResult;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/invoices/InvoiceFileUploadResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $file:Ljava/io/File;

.field final synthetic $invoiceTokenType:Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

.field final synthetic this$0:Lcom/squareup/invoices/image/ImageUploader;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$2;->this$0:Lcom/squareup/invoices/image/ImageUploader;

    iput-object p2, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$2;->$invoiceTokenType:Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    iput-object p3, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$2;->$file:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/invoices/image/FileUploadResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+",
            "Lcom/squareup/server/invoices/InvoiceFileUploadResponse;",
            ">;)",
            "Lcom/squareup/invoices/image/FileUploadResult;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$2;->this$0:Lcom/squareup/invoices/image/ImageUploader;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    iget-object v1, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$2;->$invoiceTokenType:Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    iget-object v2, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$2;->$file:Ljava/io/File;

    invoke-static {v0, p1, v1, v2}, Lcom/squareup/invoices/image/ImageUploader;->access$toUploadSuccessResult(Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/io/File;)Lcom/squareup/invoices/image/FileUploadResult$Success;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/image/FileUploadResult;

    goto :goto_0

    .line 134
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$2;->this$0:Lcom/squareup/invoices/image/ImageUploader;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-static {v0, p1}, Lcom/squareup/invoices/image/ImageUploader;->access$toUploadFailureResult(Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/image/FileUploadResult$Failure;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/image/FileUploadResult;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$2;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/invoices/image/FileUploadResult;

    move-result-object p1

    return-object p1
.end method
