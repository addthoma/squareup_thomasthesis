.class public final Lcom/squareup/invoices/image/AttachmentFileViewer;
.super Ljava/lang/Object;
.source "AttachmentFileViewer.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J$\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/invoices/image/AttachmentFileViewer;",
        "",
        "fileViewer",
        "Lcom/squareup/invoices/image/FileViewer;",
        "imageDownloader",
        "Lcom/squareup/invoices/image/ImageDownloader;",
        "invoiceFileHelper",
        "Lcom/squareup/invoices/image/InvoiceFileHelper;",
        "(Lcom/squareup/invoices/image/FileViewer;Lcom/squareup/invoices/image/ImageDownloader;Lcom/squareup/invoices/image/InvoiceFileHelper;)V",
        "viewAttachment",
        "Lio/reactivex/Single;",
        "",
        "invoiceTokenType",
        "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
        "attachmentToken",
        "",
        "mimeType",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final fileViewer:Lcom/squareup/invoices/image/FileViewer;

.field private final imageDownloader:Lcom/squareup/invoices/image/ImageDownloader;

.field private final invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/image/FileViewer;Lcom/squareup/invoices/image/ImageDownloader;Lcom/squareup/invoices/image/InvoiceFileHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "fileViewer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "imageDownloader"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceFileHelper"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/image/AttachmentFileViewer;->fileViewer:Lcom/squareup/invoices/image/FileViewer;

    iput-object p2, p0, Lcom/squareup/invoices/image/AttachmentFileViewer;->imageDownloader:Lcom/squareup/invoices/image/ImageDownloader;

    iput-object p3, p0, Lcom/squareup/invoices/image/AttachmentFileViewer;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    return-void
.end method

.method public static final synthetic access$getFileViewer$p(Lcom/squareup/invoices/image/AttachmentFileViewer;)Lcom/squareup/invoices/image/FileViewer;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/squareup/invoices/image/AttachmentFileViewer;->fileViewer:Lcom/squareup/invoices/image/FileViewer;

    return-object p0
.end method

.method public static final synthetic access$getInvoiceFileHelper$p(Lcom/squareup/invoices/image/AttachmentFileViewer;)Lcom/squareup/invoices/image/InvoiceFileHelper;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/squareup/invoices/image/AttachmentFileViewer;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    return-object p0
.end method


# virtual methods
.method public final viewAttachment(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "invoiceTokenType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attachmentToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mimeType"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lcom/squareup/invoices/image/AttachmentFileViewer;->imageDownloader:Lcom/squareup/invoices/image/ImageDownloader;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoices/image/ImageDownloader;->download(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 27
    new-instance p2, Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1;

    invoke-direct {p2, p0, p3}, Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1;-><init>(Lcom/squareup/invoices/image/AttachmentFileViewer;Ljava/lang/String;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "imageDownloader.download\u2026  }\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
