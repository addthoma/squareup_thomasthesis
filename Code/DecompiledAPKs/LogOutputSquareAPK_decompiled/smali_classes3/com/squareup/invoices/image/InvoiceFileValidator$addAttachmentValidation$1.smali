.class final Lcom/squareup/invoices/image/InvoiceFileValidator$addAttachmentValidation$1;
.super Ljava/lang/Object;
.source "InvoiceFileValidator.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/image/InvoiceFileValidator;->addAttachmentValidation(I)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/invoices/image/FileValidationResult;",
        "it",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $totalAttachmentCountOnInvoice:I

.field final synthetic this$0:Lcom/squareup/invoices/image/InvoiceFileValidator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/image/InvoiceFileValidator;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/image/InvoiceFileValidator$addAttachmentValidation$1;->this$0:Lcom/squareup/invoices/image/InvoiceFileValidator;

    iput p2, p0, Lcom/squareup/invoices/image/InvoiceFileValidator$addAttachmentValidation$1;->$totalAttachmentCountOnInvoice:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;)Lcom/squareup/invoices/image/FileValidationResult;
    .locals 4

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->max_count:Ljava/lang/Integer;

    .line 50
    iget v0, p0, Lcom/squareup/invoices/image/InvoiceFileValidator$addAttachmentValidation$1;->$totalAttachmentCountOnInvoice:I

    const-string v1, "countLimit"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->compare(II)I

    move-result v0

    if-ltz v0, :cond_0

    .line 51
    new-instance v0, Lcom/squareup/invoices/image/FileValidationResult$Failure;

    .line 52
    iget-object v1, p0, Lcom/squareup/invoices/image/InvoiceFileValidator$addAttachmentValidation$1;->this$0:Lcom/squareup/invoices/image/InvoiceFileValidator;

    invoke-static {v1}, Lcom/squareup/invoices/image/InvoiceFileValidator;->access$getRes$p(Lcom/squareup/invoices/image/InvoiceFileValidator;)Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_file_attachment_count_limit_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 53
    iget-object v2, p0, Lcom/squareup/invoices/image/InvoiceFileValidator$addAttachmentValidation$1;->this$0:Lcom/squareup/invoices/image/InvoiceFileValidator;

    invoke-static {v2}, Lcom/squareup/invoices/image/InvoiceFileValidator;->access$getRes$p(Lcom/squareup/invoices/image/InvoiceFileValidator;)Lcom/squareup/util/Res;

    move-result-object v2

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_file_attachment_count_limit_description:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 54
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const-string v3, "max"

    invoke-virtual {v2, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 55
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 56
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 51
    invoke-direct {v0, v1, p1}, Lcom/squareup/invoices/image/FileValidationResult$Failure;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/invoices/image/FileValidationResult;

    goto :goto_0

    .line 59
    :cond_0
    sget-object p1, Lcom/squareup/invoices/image/FileValidationResult$Success;->INSTANCE:Lcom/squareup/invoices/image/FileValidationResult$Success;

    move-object v0, p1

    check-cast v0, Lcom/squareup/invoices/image/FileValidationResult;

    :goto_0
    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/image/InvoiceFileValidator$addAttachmentValidation$1;->apply(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;)Lcom/squareup/invoices/image/FileValidationResult;

    move-result-object p1

    return-object p1
.end method
