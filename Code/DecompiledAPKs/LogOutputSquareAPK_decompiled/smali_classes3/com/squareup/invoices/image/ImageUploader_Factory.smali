.class public final Lcom/squareup/invoices/image/ImageUploader_Factory;
.super Ljava/lang/Object;
.source "ImageUploader_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/image/ImageUploader;",
        ">;"
    }
.end annotation


# instance fields
.field private final failureMessageFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final fileAttachmentServiceHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/FileAttachmentServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final fileValidatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileValidator;",
            ">;"
        }
    .end annotation
.end field

.field private final imageCompressorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ImageCompressor;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceFileHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ImageCompressor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/FileAttachmentServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileValidator;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/invoices/image/ImageUploader_Factory;->invoiceFileHelperProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/invoices/image/ImageUploader_Factory;->failureMessageFactoryProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/invoices/image/ImageUploader_Factory;->resProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/invoices/image/ImageUploader_Factory;->imageCompressorProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/invoices/image/ImageUploader_Factory;->fileAttachmentServiceHelperProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p6, p0, Lcom/squareup/invoices/image/ImageUploader_Factory;->fileValidatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/image/ImageUploader_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ImageCompressor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/FileAttachmentServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileValidator;",
            ">;)",
            "Lcom/squareup/invoices/image/ImageUploader_Factory;"
        }
    .end annotation

    .line 53
    new-instance v7, Lcom/squareup/invoices/image/ImageUploader_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/invoices/image/ImageUploader_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/util/Res;Lcom/squareup/invoices/image/ImageCompressor;Lcom/squareup/invoices/image/FileAttachmentServiceHelper;Lcom/squareup/invoices/image/InvoiceFileValidator;)Lcom/squareup/invoices/image/ImageUploader;
    .locals 8

    .line 59
    new-instance v7, Lcom/squareup/invoices/image/ImageUploader;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/invoices/image/ImageUploader;-><init>(Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/util/Res;Lcom/squareup/invoices/image/ImageCompressor;Lcom/squareup/invoices/image/FileAttachmentServiceHelper;Lcom/squareup/invoices/image/InvoiceFileValidator;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/image/ImageUploader;
    .locals 7

    .line 45
    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader_Factory;->invoiceFileHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/invoices/image/InvoiceFileHelper;

    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader_Factory;->failureMessageFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/receiving/FailureMessageFactory;

    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader_Factory;->imageCompressorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/invoices/image/ImageCompressor;

    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader_Factory;->fileAttachmentServiceHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/invoices/image/FileAttachmentServiceHelper;

    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader_Factory;->fileValidatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/invoices/image/InvoiceFileValidator;

    invoke-static/range {v1 .. v6}, Lcom/squareup/invoices/image/ImageUploader_Factory;->newInstance(Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/util/Res;Lcom/squareup/invoices/image/ImageCompressor;Lcom/squareup/invoices/image/FileAttachmentServiceHelper;Lcom/squareup/invoices/image/InvoiceFileValidator;)Lcom/squareup/invoices/image/ImageUploader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/invoices/image/ImageUploader_Factory;->get()Lcom/squareup/invoices/image/ImageUploader;

    move-result-object v0

    return-object v0
.end method
