.class public final Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;
.super Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;
.source "AutomaticRemindersWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RemindersToggled"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00052\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "remindersEnabled",
        "",
        "defaultList",
        "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "(Lcom/squareup/analytics/Analytics;ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;)V",
        "getAnalytics",
        "()Lcom/squareup/analytics/Analytics;",
        "getDefaultList",
        "()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "getRemindersEnabled",
        "()Z",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

.field private final remindersEnabled:Z


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;)V
    .locals 1

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultList"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 89
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->analytics:Lcom/squareup/analytics/Analytics;

    iput-boolean p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->remindersEnabled:Z

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;Lcom/squareup/analytics/Analytics;ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->analytics:Lcom/squareup/analytics/Analytics;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->remindersEnabled:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->copy(Lcom/squareup/analytics/Analytics;ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/analytics/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->analytics:Lcom/squareup/analytics/Analytics;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->remindersEnabled:Z

    return v0
.end method

.method public final component3()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    return-object v0
.end method

.method public final copy(Lcom/squareup/analytics/Analytics;ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;
    .locals 1

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultList"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;-><init>(Lcom/squareup/analytics/Analytics;ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->remindersEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->remindersEnabled:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAnalytics()Lcom/squareup/analytics/Analytics;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->analytics:Lcom/squareup/analytics/Analytics;

    return-object v0
.end method

.method public final getDefaultList()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    return-object v0
.end method

.method public final getRemindersEnabled()Z
    .locals 1

    .line 87
    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->remindersEnabled:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->remindersEnabled:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RemindersToggled(analytics="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", remindersEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->remindersEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", defaultList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
