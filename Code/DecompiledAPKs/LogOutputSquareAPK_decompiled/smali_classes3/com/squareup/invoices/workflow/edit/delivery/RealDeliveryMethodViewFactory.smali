.class public final Lcom/squareup/invoices/workflow/edit/delivery/RealDeliveryMethodViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealDeliveryMethodViewFactory.kt"

# interfaces
.implements Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/delivery/RealDeliveryMethodViewFactory;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "()V",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 9
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 10
    sget-object v2, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethod;->INSTANCE:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethod;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethod;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 11
    sget v3, Lcom/squareup/features/invoices/R$layout;->invoice_delivery_method_view:I

    .line 12
    sget-object v4, Lcom/squareup/invoices/workflow/edit/delivery/RealDeliveryMethodViewFactory$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/delivery/RealDeliveryMethodViewFactory$1;

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 9
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 8
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
