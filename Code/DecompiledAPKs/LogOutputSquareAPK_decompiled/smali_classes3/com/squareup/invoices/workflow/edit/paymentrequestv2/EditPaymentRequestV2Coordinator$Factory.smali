.class public final Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$Factory;
.super Ljava/lang/Object;
.source "EditPaymentRequestV2Coordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J*\u0010\u0003\u001a\u00020\u00042\"\u0010\u0005\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\t0\u0007j\u0008\u0012\u0004\u0012\u00020\u0008`\n0\u0006\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$Factory;",
        "",
        "()V",
        "build",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;",
        "screen",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final build(Lio/reactivex/Observable;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;

    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$Factory$build$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$Factory$build$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v1, "screen.map { it.unwrapV2Screen }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;-><init>(Lio/reactivex/Observable;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
