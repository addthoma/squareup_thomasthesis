.class public abstract Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;
.super Ljava/lang/Object;
.source "EditPaymentRequestInfo.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;,
        Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;,
        Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00082\u00020\u0001:\u0003\u0007\u0008\tB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0002\n\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;",
        "",
        "paymentRequest",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "(Lcom/squareup/protos/client/invoice/PaymentRequest;)V",
        "getPaymentRequest",
        "()Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "Balance",
        "Companion",
        "Deposit",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Companion;

.field private static final EMPTY_BALANCE:Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;


# instance fields
.field private final paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;->Companion:Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Companion;

    .line 51
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    .line 52
    new-instance v1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->build()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v1

    const-string v2, "PaymentRequest.Builder().build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    new-instance v2, Lcom/squareup/protos/common/Money$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/common/Money$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/squareup/protos/common/Money$Builder;->build()Lcom/squareup/protos/common/Money;

    move-result-object v2

    const-string v3, "Money.Builder().build()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    new-instance v3, Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;-><init>()V

    invoke-virtual {v3}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->build()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    const-string v4, "YearMonthDay.Builder().build()"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;->EMPTY_BALANCE:Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/protos/client/invoice/PaymentRequest;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;->paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/invoice/PaymentRequest;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;)V

    return-void
.end method

.method public static final synthetic access$getEMPTY_BALANCE$cp()Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;
    .locals 1

    .line 26
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;->EMPTY_BALANCE:Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    return-object v0
.end method


# virtual methods
.method public getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;->paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;

    return-object v0
.end method
