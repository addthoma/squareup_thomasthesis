.class public abstract Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
.super Ljava/lang/Object;
.source "InvoiceRemindersListInfo.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;,
        Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;,
        Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceRemindersListInfo.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceRemindersListInfo.kt\ncom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,89:1\n950#2:90\n950#2:91\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceRemindersListInfo.kt\ncom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo\n*L\n80#1:90\n81#1:91\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u000f2\u00020\u0001:\u0003\u000f\u0010\u0011B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0008\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0005J\u000e\u0010\n\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\u000cJ\u0016\u0010\r\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\u000cJ\u0006\u0010\u000e\u001a\u00020\u0000R\u0017\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007\u0082\u0001\u0002\u0012\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "Landroid/os/Parcelable;",
        "()V",
        "reminders",
        "",
        "Lcom/squareup/invoices/workflow/edit/InvoiceReminder;",
        "getReminders",
        "()Ljava/util/List;",
        "addReminder",
        "reminder",
        "removeAt",
        "index",
        "",
        "setReminder",
        "sortByDays",
        "Companion",
        "ConfigsInfo",
        "InstancesInfo",
        "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$Companion;

.field private static final NEW_REMINDER_INDEX:I = -0x1


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;->Companion:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;-><init>()V

    return-void
.end method


# virtual methods
.method public final addReminder(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 1

    const-string v0, "reminder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, -0x1

    .line 37
    invoke-virtual {p0, p1, v0}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;->setReminder(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;I)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object p1

    return-object p1
.end method

.method public final getReminders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/InvoiceReminder;",
            ">;"
        }
    .end annotation

    .line 30
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;->getConfigs()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 31
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->getInstances()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public final removeAt(I)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 7

    .line 72
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;->getConfigs()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;->copy(Ljava/util/List;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    goto :goto_0

    .line 73
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    if-eqz v0, :cond_1

    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->getInstances()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->copy$default(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final setReminder(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;I)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 8

    const-string v0, "reminder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    const/4 v1, -0x1

    if-eqz v0, :cond_1

    .line 51
    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;

    .line 52
    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;->getConfigs()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    if-ne p2, v1, :cond_0

    .line 54
    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v2, p2, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 56
    :goto_0
    invoke-virtual {v0, v2}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;->copy(Ljava/util/List;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    goto :goto_2

    .line 58
    :cond_1
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    if-eqz v0, :cond_3

    .line 59
    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    .line 60
    move-object v2, p0

    check-cast v2, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->getInstances()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v3

    if-ne p2, v1, :cond_2

    .line 62
    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-interface {v3, p2, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :goto_1
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    .line 64
    invoke-static/range {v2 .. v7}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->copy$default(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    :goto_2
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final sortByDays()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 7

    .line 80
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;->getConfigs()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 90
    new-instance v2, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$sortByDays$$inlined$sortedBy$1;

    invoke-direct {v2}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$sortByDays$$inlined$sortedBy$1;-><init>()V

    check-cast v2, Ljava/util/Comparator;

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v1

    .line 80
    invoke-virtual {v0, v1}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;->copy(Ljava/util/List;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    goto :goto_0

    .line 81
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    if-eqz v0, :cond_1

    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->getInstances()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 91
    new-instance v2, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$sortByDays$$inlined$sortedBy$2;

    invoke-direct {v2}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$sortByDays$$inlined$sortedBy$2;-><init>()V

    check-cast v2, Ljava/util/Comparator;

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    .line 81
    invoke-static/range {v1 .. v6}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->copy$default(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
