.class public abstract Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;
.super Ljava/lang/Object;
.source "InvoiceAttachmentEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$TakePhoto;,
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$ChoosePhoto;,
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$AttachPdf;,
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$DialogDismissed;,
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$BackPressed;,
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$AttachmentTitleUpdated;,
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$UploadPhoto;,
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$UpdatePhoto;,
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$RemoveImage;,
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$ErrorDialogDismissed;,
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$UploadSuccessDone;,
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$ViewExternal;,
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$Start;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\r\u0003\u0004\u0005\u0006\u0007\u0008\t\n\u000b\u000c\r\u000e\u000fB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\r\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        "",
        "()V",
        "AttachPdf",
        "AttachmentTitleUpdated",
        "BackPressed",
        "ChoosePhoto",
        "DialogDismissed",
        "ErrorDialogDismissed",
        "RemoveImage",
        "Start",
        "TakePhoto",
        "UpdatePhoto",
        "UploadPhoto",
        "UploadSuccessDone",
        "ViewExternal",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$TakePhoto;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$ChoosePhoto;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$AttachPdf;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$DialogDismissed;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$BackPressed;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$AttachmentTitleUpdated;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$UploadPhoto;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$UpdatePhoto;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$RemoveImage;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$ErrorDialogDismissed;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$UploadSuccessDone;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$ViewExternal;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$Start;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 5
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;-><init>()V

    return-void
.end method
