.class public final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenDataKt;
.super Ljava/lang/Object;
.source "InvoiceImageScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceImageScreenData.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceImageScreenData.kt\ncom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenDataKt\n*L\n1#1,133:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002\u001a\u000c\u0010\u0003\u001a\u00020\u0004*\u00020\u0002H\u0002\u001a\u000c\u0010\u0005\u001a\u00020\u0006*\u00020\u0002H\u0002\u001a\u000c\u0010\u0007\u001a\u00020\u0001*\u00020\u0002H\u0002\u001a\u000c\u0010\u0008\u001a\u00020\t*\u00020\u0002H\u0002\u001a\u0014\u0010\n\u001a\u00020\u0004*\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u0001H\u0002\u00a8\u0006\r"
    }
    d2 = {
        "extension",
        "",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
        "previewImage",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;",
        "showProgress",
        "",
        "title",
        "toConfig",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;",
        "toPreviewImage",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;",
        "mimeType",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$extension(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenDataKt;->extension(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$previewImage(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenDataKt;->previewImage(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$showProgress(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenDataKt;->showProgress(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$title(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenDataKt;->title(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toConfig(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenDataKt;->toConfig(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    move-result-object p0

    return-object p0
.end method

.method private static final extension(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Ljava/lang/String;
    .locals 1

    .line 78
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getExtensionToPreserve()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 79
    :cond_0
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;->getExtensionToPreserve()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 80
    :cond_1
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    if-eqz v0, :cond_2

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getExtensionToPreserve()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 81
    :cond_2
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;

    if-eqz v0, :cond_3

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getExtensionToPreserve()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 82
    :cond_3
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;

    if-eqz v0, :cond_4

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getExtensionToPreserve()Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0

    .line 83
    :cond_4
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected state on the InvoiceImageScreen."

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method private static final previewImage(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;
    .locals 1

    .line 70
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;

    if-eqz v0, :cond_1

    :goto_0
    sget-object p0, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$Downloading;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$Downloading;

    check-cast p0, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    goto :goto_1

    .line 71
    :cond_1
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;

    if-eqz v0, :cond_2

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;->getAttachmentStatus()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getMimeType()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenDataKt;->toPreviewImage(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    move-result-object p0

    goto :goto_1

    .line 72
    :cond_2
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    if-eqz v0, :cond_3

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;->getAttachmentStatus()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getMimeType()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenDataKt;->toPreviewImage(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    move-result-object p0

    goto :goto_1

    .line 73
    :cond_3
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;

    if-eqz v0, :cond_4

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;->getAttachmentStatus()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getMimeType()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenDataKt;->toPreviewImage(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    move-result-object p0

    :goto_1
    return-object p0

    .line 74
    :cond_4
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected state on the InvoiceImageScreen."

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method private static final showProgress(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Z
    .locals 1

    .line 67
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    if-nez v0, :cond_1

    instance-of p0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static final title(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Ljava/lang/String;
    .locals 1

    .line 87
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getTitle()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 88
    :cond_0
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;->getTitle()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 89
    :cond_1
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    if-eqz v0, :cond_2

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getTitle()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 90
    :cond_2
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;

    if-eqz v0, :cond_3

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getTitle()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 91
    :cond_3
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;

    if-eqz v0, :cond_4

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getTitle()Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0

    .line 92
    :cond_4
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected state on the InvoiceImageScreen."

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method private static final toConfig(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;
    .locals 1

    .line 96
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;

    if-eqz v0, :cond_2

    .line 97
    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;->getDownloadingImageType()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType;

    move-result-object p0

    .line 98
    sget-object v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType$Update;->INSTANCE:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType$Update;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Update;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Update;

    check-cast p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    goto/16 :goto_1

    .line 99
    :cond_0
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType$ReadOnly;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$ReadOnly;

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType$ReadOnly;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType$ReadOnly;->getUploadedAt()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$ReadOnly;-><init>(Ljava/lang/String;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    goto/16 :goto_1

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 102
    :cond_2
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_3
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    if-eqz v0, :cond_4

    :goto_0
    sget-object p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Upload;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Upload;

    check-cast p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    goto :goto_1

    .line 103
    :cond_4
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;

    if-eqz v0, :cond_8

    .line 104
    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;->getViewingImageType()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;

    move-result-object p0

    .line 105
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$Upload;

    if-eqz v0, :cond_5

    sget-object p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Upload;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Upload;

    check-cast p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    goto :goto_1

    .line 106
    :cond_5
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$Update;

    if-eqz v0, :cond_6

    sget-object p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Update;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Update;

    check-cast p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    goto :goto_1

    .line 107
    :cond_6
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$ReadOnly;

    if-eqz v0, :cond_7

    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$ReadOnly;

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$ReadOnly;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$ReadOnly;->getUploadedAt()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$ReadOnly;-><init>(Ljava/lang/String;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    goto :goto_1

    :cond_7
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 110
    :cond_8
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;

    if-eqz v0, :cond_c

    .line 111
    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;->getViewingImageType()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;

    move-result-object p0

    .line 112
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$Upload;

    if-eqz v0, :cond_9

    sget-object p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Upload;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Upload;

    check-cast p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    goto :goto_1

    .line 113
    :cond_9
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$Update;

    if-eqz v0, :cond_a

    sget-object p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Update;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Update;

    check-cast p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    goto :goto_1

    .line 114
    :cond_a
    instance-of v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$ReadOnly;

    if-eqz v0, :cond_b

    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$ReadOnly;

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$ReadOnly;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$ReadOnly;->getUploadedAt()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$ReadOnly;-><init>(Ljava/lang/String;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    :goto_1
    return-object p0

    :cond_b
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 117
    :cond_c
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected state on the InvoiceImageScreen."

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method private static final toPreviewImage(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;
    .locals 1

    .line 122
    invoke-static {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/SupportedFileFormatsKt;->isSupportedImageMimeType(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string p0, "application/pdf"

    .line 123
    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    sget-object p0, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$PdfIcon;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$PdfIcon;

    check-cast p0, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    goto :goto_0

    .line 124
    :cond_0
    sget-object p0, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$FileIcon;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$FileIcon;

    check-cast p0, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    goto :goto_0

    .line 126
    :cond_1
    instance-of p1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;

    if-eqz p1, :cond_2

    new-instance p1, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$PreviewableFile;

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;->getFile()Ljava/io/File;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$PreviewableFile;-><init>(Ljava/io/File;)V

    move-object p0, p1

    check-cast p0, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    goto :goto_0

    .line 127
    :cond_2
    instance-of p1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;

    if-eqz p1, :cond_3

    new-instance p1, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$PreviewableUri;

    check-cast p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;

    invoke-virtual {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;->getUri()Landroid/net/Uri;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$PreviewableUri;-><init>(Landroid/net/Uri;)V

    move-object p0, p1

    check-cast p0, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    goto :goto_0

    .line 128
    :cond_3
    instance-of p1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Downloading;

    if-eqz p1, :cond_4

    sget-object p0, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$Downloading;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$Downloading;

    check-cast p0, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    goto :goto_0

    .line 129
    :cond_4
    instance-of p1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Error;

    if-eqz p1, :cond_5

    sget-object p0, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$Error;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$Error;

    check-cast p0, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    :goto_0
    return-object p0

    .line 130
    :cond_5
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Can\'t convert to "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, " to PreviewImage."

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
