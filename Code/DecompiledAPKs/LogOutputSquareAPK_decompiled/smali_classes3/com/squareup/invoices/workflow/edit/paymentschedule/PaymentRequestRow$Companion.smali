.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$Companion;
.super Ljava/lang/Object;
.source "PaymentRequestRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPaymentRequestRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PaymentRequestRow.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$Companion\n*L\n1#1,299:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$Companion;",
        "",
        "()V",
        "ALLOWED_PERCENTAGE_CHARS",
        "",
        "inflateWithParent",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;",
        "parent",
        "Landroid/view/ViewGroup;",
        "moneyKeyListenerFactory",
        "Lcom/squareup/money/MoneyDigitsKeyListenerFactory;",
        "unitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 250
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final inflateWithParent(Landroid/view/ViewGroup;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;)Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;
    .locals 3

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyKeyListenerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unitFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 257
    sget v1, Lcom/squareup/features/invoices/R$layout;->payment_request_row:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;

    .line 258
    invoke-virtual {p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->setMoneyKeyListener$invoices_hairball_release(Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;)V

    return-object p1

    .line 257
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.paymentschedule.PaymentRequestRow"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
