.class public final Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;
.super Ljava/lang/Object;
.source "InvoiceDateOptions.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceDateOptions.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceDateOptions.kt\ncom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions\n*L\n1#1,177:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\tH\u0002J\u0016\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\tH\u0002J\u000e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u0002J\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\tJ\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\tJ\u001c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\u0010J\u001c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\u0010R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "forBalancePaymentRequestDue",
        "",
        "Lcom/squareup/invoices/workflow/edit/DateOption;",
        "startDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "forDepositPaymentRequestDue",
        "forInstallmentOrBalanceDue",
        "forInvoiceDue",
        "forInvoiceScheduled",
        "forPaymentRequest",
        "amountType",
        "Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;",
        "forPaymentRequestV2",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private final forBalancePaymentRequestDue(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/invoices/workflow/edit/DateOption;

    .line 105
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    .line 106
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->payment_request_in_thirty_days:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x1e

    .line 105
    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 108
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    .line 109
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->payment_request_in_sixty_days:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x3c

    .line 108
    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 111
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    .line 112
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->payment_request_in_ninety_days:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x5a

    .line 111
    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 114
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    .line 116
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    .line 117
    sget v3, Lcom/squareup/common/invoices/R$string;->payment_request_in_one_hundred_twenty_days:I

    .line 116
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x78

    .line 114
    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 120
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    .line 121
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptionsKt;->getRelativeDaysToEndOfMonth(Lcom/squareup/protos/common/time/YearMonthDay;)J

    move-result-wide v2

    .line 122
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/invoices/R$string;->invoice_end_of_month:I

    invoke-interface {p1, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 120
    invoke-direct {v1, v2, v3, p1}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 p1, 0x4

    aput-object v1, v0, p1

    .line 124
    new-instance p1, Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/invoices/R$string;->invoice_custom:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v1, 0x5

    aput-object p1, v0, v1

    .line 104
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private final forDepositPaymentRequestDue(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;"
        }
    .end annotation

    .line 91
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->forInvoiceDue(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private final forInstallmentOrBalanceDue()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/invoices/workflow/edit/DateOption;

    .line 134
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/features/invoices/R$string;->within_thirty_days:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x1e

    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 135
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/features/invoices/R$string;->within_sixty_days:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x3c

    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 136
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/features/invoices/R$string;->within_ninety_days:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x5a

    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 137
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/features/invoices/R$string;->within_hundred_twenty_days:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x78

    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 138
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/features/invoices/R$string;->within_hundred_fifty_days:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x96

    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 139
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_custom:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 133
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final forInvoiceDue(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;"
        }
    .end annotation

    const-string v0, "startDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/invoices/workflow/edit/DateOption;

    .line 65
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_upon_receipt:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 66
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_in_seven_days:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x7

    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 67
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    .line 68
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_in_fourteen_days:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0xe

    .line 67
    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 70
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    .line 71
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_in_thirty_days:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x1e

    .line 70
    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 73
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    .line 74
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptionsKt;->getRelativeDaysToEndOfMonth(Lcom/squareup/protos/common/time/YearMonthDay;)J

    move-result-wide v2

    .line 75
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/invoices/R$string;->invoice_end_of_month:I

    invoke-interface {p1, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 73
    invoke-direct {v1, v2, v3, p1}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 p1, 0x4

    aput-object v1, v0, p1

    .line 77
    new-instance p1, Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/invoices/R$string;->invoice_custom:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v1, 0x5

    aput-object p1, v0, v1

    .line 64
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final forInvoiceScheduled(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;"
        }
    .end annotation

    const-string v0, "startDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/invoices/workflow/edit/DateOption;

    .line 36
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    .line 37
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_send_immediately:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x0

    .line 36
    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 39
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_in_seven_days:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x7

    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 40
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    .line 41
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_in_fourteen_days:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0xe

    .line 40
    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 43
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    .line 44
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_in_thirty_days:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x1e

    .line 43
    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 46
    new-instance v1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    .line 47
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptionsKt;->getRelativeDaysToEndOfMonth(Lcom/squareup/protos/common/time/YearMonthDay;)J

    move-result-wide v2

    .line 48
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/invoices/R$string;->invoice_end_of_month:I

    invoke-interface {p1, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 46
    invoke-direct {v1, v2, v3, p1}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 p1, 0x4

    aput-object v1, v0, p1

    .line 50
    new-instance p1, Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/invoices/R$string;->invoice_custom:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/DateOption;

    const/4 v1, 0x5

    aput-object p1, v0, v1

    .line 35
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final forPaymentRequest(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;"
        }
    .end annotation

    const-string v0, "startDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    sget-object v0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 156
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->forBalancePaymentRequestDue(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    .line 157
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Can\'t create DateOptions for amount type "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 155
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->forDepositPaymentRequestDue(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public final forPaymentRequestV2(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;"
        }
    .end annotation

    const-string v0, "startDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    sget-object v0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    .line 167
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->forInstallmentOrBalanceDue()Ljava/util/List;

    move-result-object p1

    goto :goto_0

    .line 166
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->forDepositPaymentRequestDue(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;

    move-result-object p1

    :goto_0
    return-object p1
.end method
