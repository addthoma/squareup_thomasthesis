.class final Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3$4;
.super Lkotlin/jvm/internal/Lambda;
.source "EditRecurringReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$NumberClicked;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;",
        "it",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$NumberClicked;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3$4;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$NumberClicked;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$NumberClicked;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3$4;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3;->$state:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;->getRecurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->getRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 115
    :cond_0
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 116
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3$4;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3;->$state:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    check-cast v1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;

    .line 117
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3$4;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3;

    iget-object v2, v2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3;->$state:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    check-cast v2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;->getRecurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object v3

    .line 118
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3$4;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3;

    iget-object v2, v2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3;->$state:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    check-cast v2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;->getCachedCount()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->endCount(I)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    .line 117
    invoke-static/range {v3 .. v8}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->copy$default(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Ljava/util/Date;ZILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object p1

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    .line 116
    invoke-static {v1, p1, v2, v3, v4}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;->copy$default(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;IILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;

    move-result-object p1

    .line 115
    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$NumberClicked;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3$4;->invoke(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$NumberClicked;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
