.class public final Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory;
.super Ljava/lang/Object;
.source "RemindersListCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J*\u0010\u0005\u001a\u00020\u00062\"\u0010\u0007\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tj\u0008\u0012\u0004\u0012\u00020\n`\u000c0\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory;",
        "",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Lcom/squareup/recycler/RecyclerFactory;)V",
        "create",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;


# direct methods
.method public constructor <init>(Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "recyclerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;

    .line 41
    sget-object v1, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory$create$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory$create$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v1, "screens.map { it.unwrapV2Screen }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 40
    invoke-direct {v0, p1, v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;)V

    return-object v0
.end method
