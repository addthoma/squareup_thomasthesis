.class public final Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$Converter;
.super Ljava/lang/Object;
.source "ChooseDateOutput.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Converter"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$Converter;",
        "",
        "()V",
        "selectedDateFromOutput",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "output",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$Converter;-><init>()V

    return-void
.end method


# virtual methods
.method public final selectedDateFromOutput(Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$NoDateSelected;

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 13
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$SelectedDate;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$SelectedDate;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$SelectedDate;->getSelectedDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
