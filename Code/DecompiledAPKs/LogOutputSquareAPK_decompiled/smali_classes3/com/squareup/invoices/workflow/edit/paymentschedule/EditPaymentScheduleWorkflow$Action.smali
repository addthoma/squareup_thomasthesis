.class public abstract Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;
.super Ljava/lang/Object;
.source "EditPaymentScheduleWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$StartEditingDepositBalanceRequest;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$StartEditingInstallmentRequest;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestCanceled;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestRemoved;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestEdited;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$ToggleDepositRow;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$ToggleSplitBalanceRow;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$DepositPercentageChanged;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$InstallmentPercentageChanged;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$DepositMoneyChanged;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$InstallmentMoneyChanged;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AmountTypeChanged;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AddInstallment;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Cancel;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$CancelValidation;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;",
        "Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentScheduleWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentScheduleWorkflow.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 ProtosPure.kt\ncom/squareup/util/ProtosPure\n*L\n1#1,516:1\n1550#2,3:517\n1577#2,4:520\n1550#2,3:524\n1577#2,4:527\n1360#2:566\n1429#2,2:567\n1431#2:572\n310#2,7:573\n310#2,7:580\n145#3,3:531\n132#3,3:534\n148#3:537\n145#3,3:538\n132#3,3:541\n148#3:544\n145#3,3:545\n132#3,3:548\n148#3:551\n145#3,3:552\n132#3,3:555\n148#3:558\n145#3,3:559\n132#3,3:562\n148#3:565\n132#3,3:569\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentScheduleWorkflow.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action\n*L\n173#1,3:517\n174#1,4:520\n185#1,3:524\n186#1,4:527\n318#1:566\n318#1,2:567\n318#1:572\n334#1,7:573\n339#1,7:580\n248#1,3:531\n248#1,3:534\n248#1:537\n259#1,3:538\n259#1,3:541\n259#1:544\n270#1,3:545\n270#1,3:548\n270#1:551\n281#1,3:552\n281#1,3:555\n281#1:558\n311#1,3:559\n311#1,3:562\n311#1:565\n318#1,3:569\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0010\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f !\"#$B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016J$\u0010\u0007\u001a\u00020\u0008*\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0012\u0010\u000f\u001a\u00020\u0010*\u0008\u0012\u0004\u0012\u00020\u00120\u0011H\u0002J\u001a\u0010\u0013\u001a\u00020\u0010*\u0008\u0012\u0004\u0012\u00020\u00120\u00112\u0006\u0010\u0014\u001a\u00020\u0010H\u0002\u0082\u0001\u0010%&\'()*+,-./01234\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;",
        "Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "convertType",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;",
        "isPercentage",
        "",
        "isDeposit",
        "invoiceAmount",
        "Lcom/squareup/protos/common/Money;",
        "depositIndex",
        "",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "installmentIndex",
        "installmentNumber",
        "AddInstallment",
        "AmountTypeChanged",
        "Cancel",
        "CancelValidation",
        "DepositMoneyChanged",
        "DepositPercentageChanged",
        "InstallmentMoneyChanged",
        "InstallmentPercentageChanged",
        "PaymentRequestCanceled",
        "PaymentRequestEdited",
        "PaymentRequestRemoved",
        "Save",
        "StartEditingDepositBalanceRequest",
        "StartEditingInstallmentRequest",
        "ToggleDepositRow",
        "ToggleSplitBalanceRow",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$StartEditingDepositBalanceRequest;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$StartEditingInstallmentRequest;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestCanceled;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestRemoved;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestEdited;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$ToggleDepositRow;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$ToggleSplitBalanceRow;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$DepositPercentageChanged;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$InstallmentPercentageChanged;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$DepositMoneyChanged;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$InstallmentMoneyChanged;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AmountTypeChanged;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AddInstallment;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Cancel;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$CancelValidation;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;-><init>()V

    return-void
.end method

.method private final convertType(Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;ZZLcom/squareup/protos/common/Money;)V
    .locals 3

    if-eqz p2, :cond_1

    if-eqz p3, :cond_0

    .line 351
    sget-object p2, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_INSTALLMENT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    goto :goto_0

    :cond_1
    if-eqz p3, :cond_2

    .line 353
    sget-object p2, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->FIXED_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    goto :goto_0

    :cond_2
    sget-object p2, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->FIXED_INSTALLMENT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    .line 355
    :goto_0
    iput-object p2, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    .line 356
    sget-object p3, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->ordinal()I

    move-result p2

    aget p2, p3, p2

    const/4 p3, 0x1

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    if-eq p2, p3, :cond_7

    const/4 p3, 0x2

    if-eq p2, p3, :cond_7

    const/4 p3, 0x3

    if-eq p2, p3, :cond_4

    const/4 p3, 0x4

    if-ne p2, p3, :cond_3

    goto :goto_1

    .line 368
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Invalid deposit type."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 365
    :cond_4
    :goto_1
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    iget-object p2, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-eqz p2, :cond_6

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->longValue()J

    move-result-wide p2

    const-wide/16 v1, 0x64

    cmp-long p4, p2, v1

    if-lez p4, :cond_5

    goto :goto_2

    :cond_5
    move-wide v1, p2

    :cond_6
    :goto_2
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    iput-object p2, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount:Ljava/lang/Long;

    .line 366
    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    goto :goto_4

    .line 359
    :cond_7
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount:Ljava/lang/Long;

    if-eqz p2, :cond_8

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 360
    :cond_8
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    if-eqz p2, :cond_9

    iget-object p2, p2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    if-eqz p2, :cond_9

    goto :goto_3

    :cond_9
    iget-object p2, p4, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string p3, "invoiceAmount.currency_code"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 358
    :goto_3
    invoke-static {v1, v2, p2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    iput-object p2, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    .line 362
    check-cast v0, Ljava/lang/Long;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount:Ljava/lang/Long;

    :goto_4
    return-void
.end method

.method private final depositIndex(Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)I"
        }
    .end annotation

    .line 574
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, -0x1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 575
    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 334
    invoke-static {v1}, Lcom/squareup/invoices/PaymentRequestsKt;->isDepositRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    :goto_1
    if-eq v0, v2, :cond_2

    return v0

    .line 335
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "No deposit to update."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final installmentIndex(Ljava/util/List;I)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;I)I"
        }
    .end annotation

    .line 581
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, -0x1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 582
    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 339
    invoke-static {v1}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    :goto_1
    if-eq v0, v2, :cond_2

    add-int/2addr v0, p2

    return v0

    .line 340
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "No deposit to update."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Cancel;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult$Canceled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult$Canceled;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;

    return-object p1

    .line 160
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v4, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.paymentschedule.EditPaymentScheduleState.Viewing"

    if-eqz v0, :cond_f

    .line 161
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_e

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    .line 162
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;->getPaymentRequestList()Ljava/util/List;

    move-result-object v0

    .line 163
    move-object v4, p0

    check-cast v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;

    invoke-virtual {v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->getValidator()Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;

    move-result-object v5

    invoke-virtual {v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->getInvoiceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->validate(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult;

    move-result-object v5

    .line 166
    sget-object v6, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Success;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Success;

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 167
    sget-object p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer;

    .line 168
    invoke-virtual {v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isTippingEnabled()Z

    move-result v1

    .line 167
    invoke-virtual {p1, v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer;->sanitize(Ljava/util/List;Z)Ljava/util/List;

    move-result-object p1

    .line 170
    invoke-virtual {v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object v1

    .line 172
    invoke-virtual {v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isNew()Z

    move-result v6

    .line 173
    check-cast v0, Ljava/lang/Iterable;

    .line 517
    instance-of v4, v0, Ljava/util/Collection;

    if-eqz v4, :cond_2

    move-object v5, v0

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    .line 518
    :cond_2
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 173
    invoke-static {v7}, Lcom/squareup/invoices/PaymentRequestsKt;->isDepositRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v7, 0x1

    :goto_0
    if-eqz v4, :cond_4

    .line 520
    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v8, 0x0

    goto :goto_2

    .line 522
    :cond_4
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 174
    invoke-static {v2}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v2

    if-eqz v2, :cond_5

    add-int/lit8 v3, v3, 0x1

    if-gez v3, :cond_5

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_1

    :cond_6
    move v8, v3

    :goto_2
    const/4 v9, 0x1

    .line 171
    new-instance v0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;

    const-string v10, ""

    move-object v5, v0

    invoke-direct/range {v5 .. v10}, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;-><init>(ZZIZLjava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 170
    invoke-interface {v1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 179
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult$Saved;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult$Saved;-><init>(Ljava/util/List;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;

    return-object v0

    .line 181
    :cond_7
    instance-of v6, v5, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;

    if-eqz v6, :cond_36

    .line 182
    invoke-virtual {v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object v6

    .line 184
    invoke-virtual {v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isNew()Z

    move-result v8

    .line 185
    move-object v4, v0

    check-cast v4, Ljava/lang/Iterable;

    .line 524
    instance-of v7, v4, Ljava/util/Collection;

    if-eqz v7, :cond_9

    move-object v9, v4

    check-cast v9, Ljava/util/Collection;

    invoke-interface {v9}, Ljava/util/Collection;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_9

    :cond_8
    const/4 v9, 0x0

    goto :goto_3

    .line 525
    :cond_9
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_a
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 185
    invoke-static {v10}, Lcom/squareup/invoices/PaymentRequestsKt;->isDepositRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v10

    if-eqz v10, :cond_a

    const/4 v9, 0x1

    :goto_3
    if-eqz v7, :cond_b

    .line 527
    move-object v2, v4

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v10, 0x0

    goto :goto_5

    .line 529
    :cond_b
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 186
    invoke-static {v4}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v4

    if-eqz v4, :cond_c

    add-int/lit8 v3, v3, 0x1

    if-gez v3, :cond_c

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_4

    :cond_d
    move v10, v3

    :goto_5
    const/4 v11, 0x0

    .line 188
    check-cast v5, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;

    invoke-virtual {v5}, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;->getTitle()Ljava/lang/String;

    move-result-object v12

    .line 183
    new-instance v2, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;

    move-object v7, v2

    invoke-direct/range {v7 .. v12}, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;-><init>(ZZIZLjava/lang/String;)V

    check-cast v2, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 182
    invoke-interface {v6, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 191
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$ShowingError;

    invoke-virtual {v5}, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$ShowingError;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_b

    .line 161
    :cond_e
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 196
    :cond_f
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$CancelValidation;

    if-eqz v0, :cond_11

    .line 197
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_10

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$ShowingError;

    .line 198
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$ShowingError;->getPaymentRequestList()Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_b

    .line 197
    :cond_10
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.paymentschedule.EditPaymentScheduleState.ShowingError"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 201
    :cond_11
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestCanceled;

    const-string v5, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.paymentschedule.EditPaymentScheduleState.EditingPaymentRequest"

    if-eqz v0, :cond_13

    .line 202
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_12

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$EditingPaymentRequest;

    .line 203
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$EditingPaymentRequest;->getPaymentRequestList()Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_b

    .line 202
    :cond_12
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v5}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 206
    :cond_13
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestRemoved;

    if-eqz v0, :cond_15

    .line 207
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_14

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$EditingPaymentRequest;

    .line 208
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$EditingPaymentRequest;->getPaymentRequestList()Ljava/util/List;

    move-result-object v0

    .line 209
    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 211
    move-object v2, p0

    check-cast v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestRemoved;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestRemoved;->getIndex()I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 212
    sget-object v2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 213
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    invoke-direct {v2, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_b

    .line 207
    :cond_14
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v5}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 216
    :cond_15
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestEdited;

    if-eqz v0, :cond_17

    .line 217
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_16

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$EditingPaymentRequest;

    .line 218
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$EditingPaymentRequest;->getPaymentRequestList()Ljava/util/List;

    move-result-object v0

    .line 219
    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 221
    move-object v2, p0

    check-cast v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestEdited;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestEdited;->getIndex()I

    move-result v3

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestEdited;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v2

    invoke-interface {v0, v3, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 222
    sget-object v2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 223
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    invoke-direct {v2, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_b

    .line 217
    :cond_16
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v5}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 226
    :cond_17
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$ToggleDepositRow;

    if-eqz v0, :cond_1a

    .line 227
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_19

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;->getPaymentRequestList()Ljava/util/List;

    move-result-object v0

    .line 228
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    .line 229
    move-object v3, p0

    check-cast v3, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$ToggleDepositRow;

    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$ToggleDepositRow;->getOn()Z

    move-result v4

    if-eqz v4, :cond_18

    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$ToggleDepositRow;->getDefaultConfigs()Ljava/util/List;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/squareup/invoices/PaymentRequestListMutatorsKt;->addDeposit(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_6

    .line 230
    :cond_18
    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestListMutatorsKt;->removeDeposit(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 228
    :goto_6
    invoke-direct {v2, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_b

    .line 227
    :cond_19
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 234
    :cond_1a
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$ToggleSplitBalanceRow;

    if-eqz v0, :cond_1d

    .line 235
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1c

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;->getPaymentRequestList()Ljava/util/List;

    move-result-object v0

    .line 237
    new-instance v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    .line 238
    move-object v5, p0

    check-cast v5, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$ToggleSplitBalanceRow;

    invoke-virtual {v5}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$ToggleSplitBalanceRow;->getOn()Z

    move-result v6

    if-eqz v6, :cond_1b

    invoke-virtual {v5}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$ToggleSplitBalanceRow;->getDefaultConfigs()Ljava/util/List;

    move-result-object v5

    invoke-static {v0, v3, v5, v2, v1}, Lcom/squareup/invoices/PaymentRequestListMutatorsKt;->addDefaultInstallments$default(Ljava/util/List;ILjava/util/List;ILjava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_7

    .line 239
    :cond_1b
    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestListMutatorsKt;->removeInstallments(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 237
    :goto_7
    invoke-direct {v4, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v4}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_b

    .line 235
    :cond_1c
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 243
    :cond_1d
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$DepositMoneyChanged;

    const-string v3, "null cannot be cast to non-null type B"

    if-eqz v0, :cond_20

    .line 244
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1f

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    .line 245
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;->getPaymentRequestList()Ljava/util/List;

    move-result-object v0

    .line 248
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;->depositIndex(Ljava/util/List;)I

    move-result v2

    .line 531
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/wire/Message;

    .line 532
    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 535
    invoke-virtual {v4}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v4

    if-eqz v4, :cond_1e

    move-object v3, v4

    check-cast v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 249
    move-object v5, p0

    check-cast v5, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$DepositMoneyChanged;

    invoke-virtual {v5}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$DepositMoneyChanged;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v5

    iput-object v5, v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    .line 250
    move-object v5, v1

    check-cast v5, Ljava/lang/Long;

    iput-object v5, v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount:Ljava/lang/Long;

    .line 536
    invoke-virtual {v4}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v3

    .line 533
    invoke-interface {v0, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 247
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    invoke-direct {v2, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_b

    .line 535
    :cond_1e
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 244
    :cond_1f
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 254
    :cond_20
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$DepositPercentageChanged;

    if-eqz v0, :cond_23

    .line 255
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_22

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    .line 256
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;->getPaymentRequestList()Ljava/util/List;

    move-result-object v0

    .line 259
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;->depositIndex(Ljava/util/List;)I

    move-result v2

    .line 538
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/wire/Message;

    .line 539
    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 542
    invoke-virtual {v4}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v4

    if-eqz v4, :cond_21

    move-object v3, v4

    check-cast v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 260
    move-object v5, p0

    check-cast v5, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$DepositPercentageChanged;

    invoke-virtual {v5}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$DepositPercentageChanged;->getPercentage()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount:Ljava/lang/Long;

    .line 261
    move-object v5, v1

    check-cast v5, Lcom/squareup/protos/common/Money;

    iput-object v5, v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    .line 543
    invoke-virtual {v4}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v3

    .line 540
    invoke-interface {v0, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 258
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    invoke-direct {v2, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_b

    .line 542
    :cond_21
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 255
    :cond_22
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 265
    :cond_23
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$InstallmentMoneyChanged;

    if-eqz v0, :cond_26

    .line 266
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_25

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    .line 267
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;->getPaymentRequestList()Ljava/util/List;

    move-result-object v0

    .line 270
    move-object v2, p0

    check-cast v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$InstallmentMoneyChanged;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$InstallmentMoneyChanged;->getIndex()I

    move-result v4

    invoke-direct {p0, v0, v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;->installmentIndex(Ljava/util/List;I)I

    move-result v4

    .line 545
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/wire/Message;

    .line 546
    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 549
    invoke-virtual {v5}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v5

    if-eqz v5, :cond_24

    move-object v3, v5

    check-cast v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 271
    move-object v6, v1

    check-cast v6, Ljava/lang/Long;

    iput-object v6, v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount:Ljava/lang/Long;

    .line 272
    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$InstallmentMoneyChanged;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iput-object v2, v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    .line 550
    invoke-virtual {v5}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v2

    .line 547
    invoke-interface {v0, v4, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 269
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    invoke-direct {v2, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_b

    .line 549
    :cond_24
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 266
    :cond_25
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 276
    :cond_26
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$InstallmentPercentageChanged;

    if-eqz v0, :cond_29

    .line 277
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_28

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    .line 278
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;->getPaymentRequestList()Ljava/util/List;

    move-result-object v0

    .line 281
    move-object v2, p0

    check-cast v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$InstallmentPercentageChanged;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$InstallmentPercentageChanged;->getIndex()I

    move-result v4

    invoke-direct {p0, v0, v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;->installmentIndex(Ljava/util/List;I)I

    move-result v4

    .line 552
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/wire/Message;

    .line 553
    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 556
    invoke-virtual {v5}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v5

    if-eqz v5, :cond_27

    move-object v3, v5

    check-cast v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 282
    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$InstallmentPercentageChanged;->getPercentage()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount:Ljava/lang/Long;

    .line 283
    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/common/Money;

    iput-object v2, v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    .line 557
    invoke-virtual {v5}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v2

    .line 554
    invoke-interface {v0, v4, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 280
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    invoke-direct {v2, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_b

    .line 556
    :cond_27
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 277
    :cond_28
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 287
    :cond_29
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$StartEditingDepositBalanceRequest;

    if-eqz v0, :cond_2b

    .line 288
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2a

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    .line 289
    move-object v3, p0

    check-cast v3, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$StartEditingDepositBalanceRequest;

    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$StartEditingDepositBalanceRequest;->isDeposit()Z

    move-result v3

    xor-int/2addr v2, v3

    .line 290
    new-instance v3, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$EditingPaymentRequest;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;->getPaymentRequestList()Ljava/util/List;

    move-result-object v0

    invoke-direct {v3, v2, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$EditingPaymentRequest;-><init>(ILjava/util/List;)V

    invoke-virtual {p1, v3}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_b

    .line 288
    :cond_2a
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 293
    :cond_2b
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$StartEditingInstallmentRequest;

    if-eqz v0, :cond_2d

    .line 294
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2c

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    .line 295
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;->getPaymentRequestList()Ljava/util/List;

    move-result-object v0

    .line 296
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$EditingPaymentRequest;

    move-object v3, p0

    check-cast v3, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$StartEditingInstallmentRequest;

    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$StartEditingInstallmentRequest;->getIndex()I

    move-result v3

    invoke-direct {p0, v0, v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;->installmentIndex(Ljava/util/List;I)I

    move-result v3

    invoke-direct {v2, v3, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$EditingPaymentRequest;-><init>(ILjava/util/List;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_b

    .line 294
    :cond_2c
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 299
    :cond_2d
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AddInstallment;

    if-eqz v0, :cond_2f

    .line 300
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2e

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;->getPaymentRequestList()Ljava/util/List;

    move-result-object v0

    .line 301
    new-instance v3, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    .line 302
    move-object v4, p0

    check-cast v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AddInstallment;

    invoke-virtual {v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AddInstallment;->getDefaultConfigs()Ljava/util/List;

    move-result-object v4

    invoke-static {v0, v2, v4}, Lcom/squareup/invoices/PaymentRequestListMutatorsKt;->addDefaultInstallments(Ljava/util/List;ILjava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 301
    invoke-direct {v3, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v3}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_b

    .line 300
    :cond_2e
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 306
    :cond_2f
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AmountTypeChanged;

    if-eqz v0, :cond_36

    .line 307
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_35

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    .line 308
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;->getPaymentRequestList()Ljava/util/List;

    move-result-object v0

    .line 310
    move-object v2, p0

    check-cast v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AmountTypeChanged;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AmountTypeChanged;->isDeposit()Z

    move-result v4

    if-eqz v4, :cond_31

    .line 311
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;->depositIndex(Ljava/util/List;)I

    move-result v4

    .line 559
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/wire/Message;

    .line 560
    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 563
    invoke-virtual {v5}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v5

    if-eqz v5, :cond_30

    move-object v3, v5

    check-cast v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 313
    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AmountTypeChanged;->isPercentage()Z

    move-result v6

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AmountTypeChanged;->isDeposit()Z

    move-result v7

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AmountTypeChanged;->getInvoiceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 312
    invoke-direct {p0, v3, v6, v7, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;->convertType(Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;ZZLcom/squareup/protos/common/Money;)V

    .line 564
    invoke-virtual {v5}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v2

    .line 561
    invoke-interface {v0, v4, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_a

    .line 563
    :cond_30
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 317
    :cond_31
    check-cast v0, Ljava/lang/Iterable;

    .line 566
    new-instance v4, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v0, v5}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 567
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_34

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 568
    check-cast v5, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 320
    invoke-static {v5}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v6

    if-eqz v6, :cond_33

    .line 321
    check-cast v5, Lcom/squareup/wire/Message;

    .line 570
    invoke-virtual {v5}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v5

    if-eqz v5, :cond_32

    move-object v6, v5

    check-cast v6, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 321
    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AmountTypeChanged;->isPercentage()Z

    move-result v7

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AmountTypeChanged;->isDeposit()Z

    move-result v8

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AmountTypeChanged;->getInvoiceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v9

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;->convertType(Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;ZZLcom/squareup/protos/common/Money;)V

    .line 571
    invoke-virtual {v5}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/client/invoice/PaymentRequest;

    goto :goto_9

    .line 570
    :cond_32
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 322
    :cond_33
    :goto_9
    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 572
    :cond_34
    move-object v0, v4

    check-cast v0, Ljava/util/List;

    .line 326
    :goto_a
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    invoke-direct {v2, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_b

    .line 307
    :cond_35
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_36
    :goto_b
    return-object v1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 86
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;",
            "-",
            "Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
