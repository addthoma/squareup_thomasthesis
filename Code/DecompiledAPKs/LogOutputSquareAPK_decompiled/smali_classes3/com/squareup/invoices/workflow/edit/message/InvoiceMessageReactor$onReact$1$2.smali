.class final Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$2;
.super Lkotlin/jvm/internal/Lambda;
.source "InvoiceMessageReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent$CacheDefaultMessageChanged;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
        "+",
        "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
        "it",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent$CacheDefaultMessageChanged;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$2;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent$CacheDefaultMessageChanged;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent$CacheDefaultMessageChanged;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$2;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;

    iget-object v0, v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;->isDefaultMessageEnabled()Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    move-result-object v0

    .line 76
    sget-object v1, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Disabled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Disabled;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 77
    instance-of v0, v0, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 78
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$2;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    check-cast v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;

    .line 79
    new-instance v2, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent$CacheDefaultMessageChanged;->getNewDefaultMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1}, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;-><init>(Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    const/4 p1, 0x1

    const/4 v3, 0x0

    .line 78
    invoke-static {v1, v3, v2, p1, v3}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;->copy$default(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;

    move-result-object p1

    .line 77
    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Reaction;

    return-object v0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 76
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Default message not supported."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent$CacheDefaultMessageChanged;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$2;->invoke(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent$CacheDefaultMessageChanged;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method
