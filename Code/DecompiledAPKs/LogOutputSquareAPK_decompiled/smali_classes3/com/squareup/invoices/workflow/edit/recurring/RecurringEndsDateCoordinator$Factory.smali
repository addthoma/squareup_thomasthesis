.class public final Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$Factory;
.super Ljava/lang/Object;
.source "RecurringEndsDateCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J$\u0010\u0005\u001a\u00020\u00062\u001c\u0010\u0007\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tj\u0002`\u000c0\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$Factory;",
        "",
        "locale",
        "Ljava/util/Locale;",
        "(Ljava/util/Locale;)V",
        "build",
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateScreen;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final locale:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Ljava/util/Locale;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$Factory;->locale:Ljava/util/Locale;

    return-void
.end method


# virtual methods
.method public final build(Lio/reactivex/Observable;)Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;>;)",
            "Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$Factory;->locale:Ljava/util/Locale;

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;-><init>(Lio/reactivex/Observable;Ljava/util/Locale;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
