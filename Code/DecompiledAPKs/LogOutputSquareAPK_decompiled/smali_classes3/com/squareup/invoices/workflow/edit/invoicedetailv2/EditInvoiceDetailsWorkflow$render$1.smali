.class final Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditInvoiceDetailsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;->render(Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;",
        "+",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult;",
        "event",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$render$1;->$state:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event$SavePressed;

    if-eqz v0, :cond_1

    .line 72
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event$SavePressed;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event$SavePressed;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event$SavePressed;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event$SavePressed;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event$SavePressed;->getSaveMessageAsDefault()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 74
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$SavingMessage;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$render$1;->$state:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;->getDisableId()Z

    move-result v2

    invoke-direct {v1, v0, v2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$SavingMessage;-><init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;Z)V

    const/4 v0, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v1, v2, v0, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 76
    :cond_0
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult$Saved;

    invoke-direct {v1, v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult$Saved;-><init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;)V

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    .line 79
    :cond_1
    instance-of p1, p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event$CancelClicked;

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult$Canceled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult$Canceled;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$render$1;->invoke(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
