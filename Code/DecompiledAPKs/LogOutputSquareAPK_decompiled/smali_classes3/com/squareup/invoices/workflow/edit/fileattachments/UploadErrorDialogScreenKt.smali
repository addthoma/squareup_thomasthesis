.class public final Lcom/squareup/invoices/workflow/edit/fileattachments/UploadErrorDialogScreenKt;
.super Ljava/lang/Object;
.source "UploadErrorDialogScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a,\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u00042\u0006\u0010\u0005\u001a\u00020\u00022\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007*\"\u0010\u0000\"\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001\u00a8\u0006\t"
    }
    d2 = {
        "ErrorDialogScreen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/ScreenData;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$ErrorDialogDismissed;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/ErrorDialogScreen;",
        "data",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final ErrorDialogScreen(Lcom/squareup/invoices/workflow/edit/fileattachments/ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/ScreenData;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/ScreenData;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$ErrorDialogDismissed;",
            ">;"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/ErrorDialog;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/ErrorDialog;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/fileattachments/ErrorDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method
