.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;
.super Ljava/lang/Object;
.source "EditPaymentScheduleScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0015\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u001c\u0010\u0006\u001a\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u000b\u001a\u00020\u0004R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;",
        "",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/text/Formatter;)V",
        "from",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData;",
        "paymentRequests",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "invoiceAmount",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method public final from(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData;"
        }
    .end annotation

    const-string v0, "paymentRequests"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-static {p1, p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateAmounts(Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/common/Money;

    .line 148
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData;

    .line 149
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 150
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    .line 148
    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
