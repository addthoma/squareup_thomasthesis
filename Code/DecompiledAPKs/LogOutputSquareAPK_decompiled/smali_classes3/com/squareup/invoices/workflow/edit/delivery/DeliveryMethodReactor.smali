.class public final Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;
.super Ljava/lang/Object;
.source "DeliveryMethodReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/rx2/Reactor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor<",
        "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
        "Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDeliveryMethodReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DeliveryMethodReactor.kt\ncom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,138:1\n310#2,7:139\n*E\n*S KotlinDebug\n*F\n+ 1 DeliveryMethodReactor.kt\ncom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor\n*L\n126#1,7:139\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u00192\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0001\u0019B\u000f\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0006\u0010\u0008\u001a\u00020\tJ:\u0010\n\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u000c0\u000b2\u0006\u0010\r\u001a\u00020\u00022\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u001c\u0010\u0012\u001a\u00020\u0013*\u0008\u0012\u0004\u0012\u00020\u00150\u00142\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0002J\u000c\u0010\u0018\u001a\u00020\u0004*\u00020\u0002H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
        "Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;",
        "cnpFeesMessageHelper",
        "Lcom/squareup/cnp/CnpFeesMessageHelper;",
        "(Lcom/squareup/cnp/CnpFeesMessageHelper;)V",
        "asWorkflow",
        "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodWorkflow;",
        "onReact",
        "Lio/reactivex/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "state",
        "events",
        "Lcom/squareup/workflow/legacy/rx2/EventChannel;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "indexWithToken",
        "",
        "",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "token",
        "",
        "toResult",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$Companion;


# instance fields
.field private final cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;->Companion:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cnp/CnpFeesMessageHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cnpFeesMessageHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;->cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

    return-void
.end method

.method public static final synthetic access$getCnpFeesMessageHelper$p(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;)Lcom/squareup/cnp/CnpFeesMessageHelper;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;->cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

    return-object p0
.end method

.method public static final synthetic access$indexWithToken(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;Ljava/util/List;Ljava/lang/String;)I
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;->indexWithToken(Ljava/util/List;Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$toResult(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;)Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;->toResult(Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;)Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;

    move-result-object p0

    return-object p0
.end method

.method private final indexWithToken(Ljava/util/List;Ljava/lang/String;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    const/4 v0, -0x1

    if-eqz p2, :cond_1

    const/4 v1, 0x0

    .line 140
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 141
    check-cast v2, Lcom/squareup/protos/client/instruments/InstrumentSummary;

    .line 126
    iget-object v2, v2, Lcom/squareup/protos/client/instruments/InstrumentSummary;->instrument_token:Ljava/lang/String;

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return v0
.end method

.method private final toResult(Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;)Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;
    .locals 3

    .line 113
    instance-of v0, p1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;->getCurrentPaymentMethod()Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;->getCurrentInstrumentToken()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;-><init>(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/String;)V

    goto :goto_0

    .line 114
    :cond_0
    instance-of v0, p1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getPaymentMethod()Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 119
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 116
    :pswitch_1
    new-instance v0, Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;

    .line 117
    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getInstruments()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getSelectedInstrumentIndex()I

    move-result p1

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/instruments/InstrumentSummary;

    iget-object p1, p1, Lcom/squareup/protos/client/instruments/InstrumentSummary;->instrument_token:Ljava/lang/String;

    .line 116
    invoke-direct {v0, v1, p1}, Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;-><init>(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :pswitch_2
    new-instance v0, Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getPaymentMethod()Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object p1

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {v0, p1, v2, v1, v2}, Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;-><init>(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    .line 121
    :cond_1
    instance-of v0, p1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$BadState;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$BadState;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$BadState;->getPaymentMethod()Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$BadState;->getInstrumentToken()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;-><init>(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final asWorkflow()Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodWorkflow;
    .locals 1

    .line 103
    new-instance v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$asWorkflow$1;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$asWorkflow$1;-><init>(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;)V

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodWorkflow;

    return-object v0
.end method

.method public launch(Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
            "Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;",
            ">;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-static {p0, p1, p2}, Lcom/squareup/workflow/legacy/rx2/Reactor$DefaultImpls;->launch(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;->launch(Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public onReact(Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
            "Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    instance-of p3, p1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    if-eqz p3, :cond_0

    new-instance p3, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;

    invoke-direct {p3, p0, p1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;-><init>(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 68
    :cond_0
    instance-of p3, p1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    if-eqz p3, :cond_1

    new-instance p3, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;

    invoke-direct {p3, p0, p1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;-><init>(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 99
    :cond_1
    instance-of p2, p1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$BadState;

    if-eqz p2, :cond_2

    new-instance p2, Lcom/squareup/workflow/legacy/FinishWith;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;->toResult(Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;)Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    invoke-static {p2}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.just(FinishWith(state.toResult()))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onReact(Ljava/lang/Object;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;->onReact(Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
