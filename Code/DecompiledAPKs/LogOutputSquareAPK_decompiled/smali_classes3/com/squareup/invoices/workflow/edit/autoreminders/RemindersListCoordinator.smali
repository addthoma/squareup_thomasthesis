.class public final Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "RemindersListCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRemindersListCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RemindersListCoordinator.kt\ncom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n*L\n1#1,141:1\n1103#2,7:142\n49#3:149\n50#3,3:155\n53#3:164\n599#4,4:150\n601#4:154\n310#5,6:158\n*E\n*S KotlinDebug\n*F\n+ 1 RemindersListCoordinator.kt\ncom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator\n*L\n88#1,7:142\n92#1:149\n92#1,3:155\n92#1:164\n92#1,4:150\n92#1:154\n92#1,6:158\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0001\"B\u001b\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0017\u001a\u00020\u000f2\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0010\u0010\u001a\u001a\u00020\u000f2\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J\u0010\u0010\u001b\u001a\u00020\u000f2\u0006\u0010\u001c\u001a\u00020\u0019H\u0002J\u0010\u0010\u001d\u001a\u00020\u000f2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0018\u0010 \u001a\u00020\u000f2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010!\u001a\u00020\u0004H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "addReminder",
        "Lcom/squareup/marketfont/MarketButton;",
        "reminderPressed",
        "Lkotlin/Function1;",
        "",
        "",
        "remindersExplanation",
        "Lcom/squareup/widgets/MessageView;",
        "remindersList",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;",
        "toggle",
        "Lcom/squareup/widgets/list/ToggleButtonRow;",
        "attach",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "createRecycler",
        "coordinatorView",
        "showRemindersList",
        "show",
        "",
        "update",
        "data",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private addReminder:Lcom/squareup/marketfont/MarketButton;

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private reminderPressed:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private remindersExplanation:Lcom/squareup/widgets/MessageView;

.field private remindersList:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;",
            ">;"
        }
    .end annotation
.end field

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;",
            ">;"
        }
    .end annotation
.end field

.field private toggle:Lcom/squareup/widgets/list/ToggleButtonRow;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;",
            ">;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 52
    sget-object p1, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$reminderPressed$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$reminderPressed$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->reminderPressed:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$getReminderPressed$p(Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;)Lkotlin/jvm/functions/Function1;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->reminderPressed:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method

.method public static final synthetic access$setReminderPressed$p(Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->reminderPressed:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;Landroid/view/View;Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->update(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 133
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.findById<ActionBarV\u2026r)\n            .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 135
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_automatic_reminders_toggle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->toggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 136
    sget v0, Lcom/squareup/features/invoices/R$id;->automatic_reminders_explanation:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->remindersExplanation:Lcom/squareup/widgets/MessageView;

    .line 137
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_add_reminder:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->addReminder:Lcom/squareup/marketfont/MarketButton;

    .line 138
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->createRecycler(Landroid/view/View;)V

    return-void
.end method

.method private final createRecycler(Landroid/view/View;)V
    .locals 3

    .line 92
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 93
    sget v1, Lcom/squareup/features/invoices/R$id;->invoice_automatic_reminders_list_container:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    .line 149
    sget-object v1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 150
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 151
    new-instance v1, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v1}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 155
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 156
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 159
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$$special$$inlined$row$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 96
    new-instance v2, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 159
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 158
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 153
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->remindersList:Lcom/squareup/cycler/Recycler;

    return-void

    .line 150
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final showRemindersList(Z)V
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->remindersList:Lcom/squareup/cycler/Recycler;

    if-nez v0, :cond_0

    const-string v1, "remindersList"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;)V
    .locals 3

    .line 66
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;->getReminderPressed()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->reminderPressed:Lkotlin/jvm/functions/Function1;

    .line 68
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$update$1;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$update$1;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 70
    :cond_0
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 71
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_automatic_reminders:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const/4 v1, 0x1

    .line 72
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 73
    new-instance v1, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$update$2;

    invoke-direct {v1, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$update$2;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 74
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->toggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    const-string/jumbo v0, "toggle"

    if-nez p1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;->getRemindersEnabled()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 77
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->toggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$update$3;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$update$3;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;)V

    check-cast v0, Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 81
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;->getRemindersEnabled()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->showRemindersList(Z)V

    .line 82
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->remindersExplanation:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_3

    const-string v0, "remindersExplanation"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;->getHelperText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setTextAndVisibility(Ljava/lang/CharSequence;)V

    .line 83
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->remindersList:Lcom/squareup/cycler/Recycler;

    if-nez p1, :cond_4

    const-string v0, "remindersList"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$update$4;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$update$4;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    .line 87
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->addReminder:Lcom/squareup/marketfont/MarketButton;

    const-string v0, "addReminder"

    if-nez p1, :cond_5

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast p1, Landroid/view/View;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;->getShowAddReminder()Z

    move-result v1

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 88
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->addReminder:Lcom/squareup/marketfont/MarketButton;

    if-nez p1, :cond_6

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p1, Landroid/view/View;

    .line 142
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 56
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->bindViews(Landroid/view/View;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$attach$1;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
