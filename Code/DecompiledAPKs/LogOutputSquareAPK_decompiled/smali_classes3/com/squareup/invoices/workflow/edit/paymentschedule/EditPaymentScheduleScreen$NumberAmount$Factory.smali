.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;
.super Ljava/lang/Object;
.source "EditPaymentScheduleScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0000\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000e\u0008\u0001\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cJ\u0018\u0010\u0008\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cR\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;",
        "",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "percentageFormatter",
        "Lcom/squareup/util/Percentage;",
        "(Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V",
        "from",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;",
        "money",
        "disabled",
        "",
        "percentage",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V
    .locals 1
    .param p2    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/text/ForPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;->percentageFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static synthetic from$default(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;IZILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 182
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;->from(IZ)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic from$default(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;Lcom/squareup/protos/common/Money;ZILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 172
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;->from(Lcom/squareup/protos/common/Money;Z)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final from(IZ)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;
    .locals 2

    .line 184
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;->percentageFormatter:Lcom/squareup/text/Formatter;

    sget-object v1, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/util/Percentage$Companion;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 185
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    if-eqz p2, :cond_0

    .line 186
    new-instance p2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Disabled;

    invoke-direct {p2, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Disabled;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$PercentageAmount;

    invoke-direct {p2, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$PercentageAmount;-><init>(Ljava/lang/String;)V

    :goto_0
    check-cast p2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    return-object p2
.end method

.method public final from(Lcom/squareup/protos/common/Money;Z)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;
    .locals 1

    const-string v0, "money"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 175
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    if-eqz p2, :cond_0

    .line 176
    new-instance p2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Disabled;

    invoke-direct {p2, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Disabled;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$MoneyAmount;

    invoke-direct {p2, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$MoneyAmount;-><init>(Ljava/lang/String;)V

    :goto_0
    check-cast p2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    return-object p2
.end method
