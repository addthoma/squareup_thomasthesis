.class public final Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$Companion;
.super Ljava/lang/Object;
.source "EditInvoiceDetailsState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceDetailsState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceDetailsState.kt\ncom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,89:1\n180#2:90\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoiceDetailsState.kt\ncom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$Companion\n*L\n50#1:90\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$Companion;",
        "",
        "()V",
        "restoreSnapshot",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final restoreSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;
    .locals 4

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    .line 90
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 51
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 52
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsStateKt;->readInvoiceDetailsInfo(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    move-result-object v1

    .line 53
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v2

    .line 55
    const-class v3, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ShowDetails;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 56
    new-instance p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ShowDetails;

    invoke-direct {p1, v1, v2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ShowDetails;-><init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;Z)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;

    goto :goto_0

    .line 58
    :cond_0
    const-class v3, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$SavingMessage;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 59
    new-instance p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$SavingMessage;

    invoke-direct {p1, v1, v2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$SavingMessage;-><init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;Z)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;

    goto :goto_0

    .line 61
    :cond_1
    const-class v3, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ErrorSavingMessage;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 62
    new-instance v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ErrorSavingMessage;

    .line 63
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v3

    .line 64
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p1

    .line 62
    invoke-direct {v0, v3, p1, v1, v2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ErrorSavingMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;Z)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;

    :goto_0
    return-object p1

    .line 69
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown state type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
