.class public abstract Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;
.super Ljava/lang/Object;
.source "EditInvoiceResult.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecurringResult;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DeliveryResult;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$ScheduledDateResult;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$ExpiryDateResult;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$MessageResult;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AttachmentResult;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$InvoiceDetailsResult;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AutoRemindersResult;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecipientsResult;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentRequestResult;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AutomaticPaymentsResult;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentScheduleResult;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\r\u0003\u0004\u0005\u0006\u0007\u0008\t\n\u000b\u000c\r\u000e\u000fB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\r\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
        "",
        "()V",
        "AttachmentResult",
        "AutoRemindersResult",
        "AutomaticPaymentsResult",
        "DeliveryResult",
        "DueDateResult",
        "ExpiryDateResult",
        "InvoiceDetailsResult",
        "MessageResult",
        "PaymentRequestResult",
        "PaymentScheduleResult",
        "RecipientsResult",
        "RecurringResult",
        "ScheduledDateResult",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecurringResult;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DeliveryResult;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$ScheduledDateResult;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$ExpiryDateResult;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$MessageResult;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AttachmentResult;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$InvoiceDetailsResult;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AutoRemindersResult;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecipientsResult;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentRequestResult;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AutomaticPaymentsResult;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentScheduleResult;",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 5
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;-><init>()V

    return-void
.end method
