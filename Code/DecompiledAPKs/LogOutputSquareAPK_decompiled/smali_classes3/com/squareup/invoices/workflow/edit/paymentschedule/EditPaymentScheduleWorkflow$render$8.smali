.class final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$8;
.super Lkotlin/jvm/internal/Lambda;
.source "EditPaymentScheduleWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->render(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic $state:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$8;->this$0:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$8;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$8;->$state:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$8;->$props:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 73
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$8;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 9

    .line 434
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$8;->$sink:Lcom/squareup/workflow/Sink;

    .line 435
    new-instance v8, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;

    .line 436
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$8;->$state:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;

    check-cast v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;->getPaymentRequestList()Ljava/util/List;

    move-result-object v2

    .line 437
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$8;->this$0:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;

    invoke-static {v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->access$getValidator$p(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;)Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;

    move-result-object v3

    .line 438
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$8;->$props:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;->getPaymentRequests()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->tipping_enabled:Ljava/lang/Boolean;

    const-string v4, "props.paymentRequests.last().tipping_enabled"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 439
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$8;->$props:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;->getInvoiceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 440
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$8;->this$0:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;

    invoke-static {v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->access$getAnalytics$p(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v6

    .line 441
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$8;->$props:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;->isNew()Z

    move-result v7

    move-object v1, v8

    .line 435
    invoke-direct/range {v1 .. v7}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;-><init>(Ljava/util/List;Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;ZLcom/squareup/protos/common/Money;Lcom/squareup/analytics/Analytics;Z)V

    .line 434
    invoke-interface {v0, v8}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
