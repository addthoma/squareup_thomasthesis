.class public final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "InvoiceImageCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;,
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceImageCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceImageCoordinator.kt\ncom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator\n*L\n1#1,291:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a6\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0002?@B5\u0008\u0002\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u0010\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u001e\u0010\u001f\u001a\u00020\u001b2\u0006\u0010 \u001a\u00020\u00052\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00060\"H\u0002J\u001e\u0010#\u001a\u00020\u001b2\u0006\u0010$\u001a\u00020%2\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00060\"H\u0002J&\u0010&\u001a\u00020\u001b2\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00060\"2\u0006\u0010\'\u001a\u00020%2\u0006\u0010(\u001a\u00020)H\u0002J\u0010\u0010*\u001a\u00020\u001b2\u0006\u0010$\u001a\u00020%H\u0002J\u0010\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020\u0005H\u0002J.\u0010.\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010(\u001a\u00020)2\u0006\u0010-\u001a\u00020\u00052\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00060\"H\u0002J\u0018\u0010/\u001a\u00020\u001b2\u0006\u00100\u001a\u0002012\u0006\u00102\u001a\u000201H\u0003J\"\u00103\u001a\u00020\u001b*\u00020\u00122\u0006\u00104\u001a\u0002012\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00060\"H\u0002J\u000c\u00105\u001a\u00020\u001b*\u000206H\u0002J2\u00107\u001a\u000208*\u0002082\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00060\"2\u0006\u0010$\u001a\u00020%2\u0006\u0010(\u001a\u00020)2\u0006\u00109\u001a\u00020:H\u0002J\"\u0010;\u001a\u000208*\u0002082\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00060\"2\u0006\u00109\u001a\u00020:H\u0002J*\u0010<\u001a\u000208*\u0002082\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00060\"2\u0006\u00109\u001a\u00020:2\u0006\u0010(\u001a\u00020)H\u0002J\u000c\u0010=\u001a\u00020\u001b*\u00020>H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0008\u0018\u00010\u0010R\u00020\u0000X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006A"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreen;",
        "picasso",
        "Lcom/squareup/picasso/Picasso;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "(Lio/reactivex/Observable;Lcom/squareup/picasso/Picasso;Lcom/squareup/register/widgets/GlassSpinner;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "extensionPreservationTextWatcher",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;",
        "fileTitleEditText",
        "Lcom/squareup/widgets/SelectableEditText;",
        "imageContainerView",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;",
        "legalHelperMessageView",
        "Lcom/squareup/widgets/MessageView;",
        "removeAttachmentConfirmableButton",
        "Lcom/squareup/ui/ConfirmButton;",
        "uploadedAtMessageView",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "setListeners",
        "data",
        "workflowInput",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "setRemoveAttachmentButton",
        "config",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;",
        "setUpActionBar",
        "screenConfig",
        "photoState",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;",
        "setUploadedTimeStamp",
        "spinnerData",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "screenData",
        "update",
        "updateFileTitleEditText",
        "title",
        "",
        "extension",
        "addExtensionPreservationTextWatcher",
        "extensionToPreserve",
        "displayPhoto",
        "Lcom/squareup/picasso/RequestCreator;",
        "setPrimaryButton",
        "Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;",
        "resources",
        "Landroid/content/res/Resources;",
        "setPrimaryButtonAsUpdate",
        "setPrimaryButtonAsUpload",
        "showImage",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;",
        "ExtensionPreservationTextWatcher",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private extensionPreservationTextWatcher:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;

.field private fileTitleEditText:Lcom/squareup/widgets/SelectableEditText;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private imageContainerView:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;

.field private legalHelperMessageView:Lcom/squareup/widgets/MessageView;

.field private final picasso:Lcom/squareup/picasso/Picasso;

.field private removeAttachmentConfirmableButton:Lcom/squareup/ui/ConfirmButton;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private uploadedAtMessageView:Lcom/squareup/widgets/MessageView;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/picasso/Picasso;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;>;",
            "Lcom/squareup/picasso/Picasso;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ")V"
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->picasso:Lcom/squareup/picasso/Picasso;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/picasso/Picasso;Lcom/squareup/register/widgets/GlassSpinner;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/picasso/Picasso;Lcom/squareup/register/widgets/GlassSpinner;)V

    return-void
.end method

.method public static final synthetic access$spinnerData(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->spinnerData(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;Landroid/view/View;Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->update(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method private final addExtensionPreservationTextWatcher(Lcom/squareup/widgets/SelectableEditText;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/widgets/SelectableEditText;",
            "Ljava/lang/String;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;)V"
        }
    .end annotation

    .line 190
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->extensionPreservationTextWatcher:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 193
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;Lcom/squareup/widgets/SelectableEditText;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->extensionPreservationTextWatcher:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;

    .line 195
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->extensionPreservationTextWatcher:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;

    check-cast p2, Landroid/text/TextWatcher;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 255
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 256
    sget v0, Lcom/squareup/features/invoices/R$id;->image_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->imageContainerView:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;

    .line 257
    sget v0, Lcom/squareup/features/invoices/R$id;->file_name:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->fileTitleEditText:Lcom/squareup/widgets/SelectableEditText;

    .line 258
    sget v0, Lcom/squareup/features/invoices/R$id;->remove_attachment_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->removeAttachmentConfirmableButton:Lcom/squareup/ui/ConfirmButton;

    .line 259
    sget v0, Lcom/squareup/features/invoices/R$id;->legal_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->legalHelperMessageView:Lcom/squareup/widgets/MessageView;

    .line 260
    sget v0, Lcom/squareup/features/invoices/R$id;->uploaded_time_stamp:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->uploadedAtMessageView:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final displayPhoto(Lcom/squareup/picasso/RequestCreator;)V
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->imageContainerView:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;

    if-nez v0, :cond_0

    const-string v1, "imageContainerView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/picasso/RequestCreator;->skipMemoryCache()Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/picasso/RequestCreator;->noFade()Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    const-string v1, "skipMemoryCache().noFade()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->loadPhoto(Lcom/squareup/picasso/RequestCreator;)V

    return-void
.end method

.method private final setListeners(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;)V"
        }
    .end annotation

    .line 171
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->getConfig()Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    move-result-object v0

    .line 172
    instance-of v0, v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$ReadOnly;

    const-string v1, "fileTitleEditText"

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->fileTitleEditText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    goto :goto_0

    .line 174
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->fileTitleEditText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 175
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->getExtensionToPreserve()Ljava/lang/String;

    move-result-object p1

    .line 174
    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->addExtensionPreservationTextWatcher(Lcom/squareup/widgets/SelectableEditText;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 180
    :goto_0
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->imageContainerView:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;

    if-nez p1, :cond_3

    const-string v0, "imageContainerView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$setListeners$1;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$setListeners$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->setImageAndIconOnClicks(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final setPrimaryButton(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;",
            "Landroid/content/res/Resources;",
            ")",
            "Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;"
        }
    .end annotation

    .line 228
    sget-object v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Upload;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Upload;

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2, p5, p4}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->setPrimaryButtonAsUpload(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    goto :goto_0

    .line 229
    :cond_0
    sget-object p4, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Update;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Update;

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_1

    invoke-direct {p0, p1, p2, p5}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->setPrimaryButtonAsUpdate(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    goto :goto_0

    .line 230
    :cond_1
    instance-of p2, p3, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$ReadOnly;

    if-eqz p2, :cond_2

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const-string p2, "hidePrimaryButton()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final setPrimaryButtonAsUpdate(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;",
            "Landroid/content/res/Resources;",
            ")",
            "Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;"
        }
    .end annotation

    .line 238
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$setPrimaryButtonAsUpdate$1;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$setPrimaryButtonAsUpdate$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 239
    sget p2, Lcom/squareup/features/invoices/R$string;->update_image:I

    invoke-virtual {p3, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const/4 p2, 0x1

    .line 240
    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const-string p2, "showPrimaryButton { work\u2026rimaryButtonEnabled(true)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final setPrimaryButtonAsUpload(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;",
            ")",
            "Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;"
        }
    .end annotation

    .line 248
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$setPrimaryButtonAsUpload$1;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$setPrimaryButtonAsUpload$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 249
    sget p2, Lcom/squareup/features/invoices/R$string;->image_upload:I

    invoke-virtual {p3, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 251
    sget-object p2, Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;->LOADED:Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;

    if-ne p4, p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const-string p2, "showPrimaryButton { work\u2026led(photoState == LOADED)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final setRemoveAttachmentButton(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;)V"
        }
    .end annotation

    .line 159
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->removeAttachmentConfirmableButton:Lcom/squareup/ui/ConfirmButton;

    const-string v1, "removeAttachmentConfirmableButton"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    instance-of p1, p1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Update;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz p1, :cond_2

    .line 161
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->removeAttachmentConfirmableButton:Lcom/squareup/ui/ConfirmButton;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$setRemoveAttachmentButton$1;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$setRemoveAttachmentButton$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    :cond_2
    return-void
.end method

.method private final setUpActionBar(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;",
            ")V"
        }
    .end annotation

    .line 203
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 210
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "actionBar.presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 206
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v3, Lcom/squareup/features/invoices/R$string;->image_attachment:I

    invoke-virtual {v7, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 205
    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 208
    new-instance v2, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$setUpActionBar$1;

    invoke-direct {v2, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$setUpActionBar$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v3

    const-string v1, "MarinActionBar.Config.Bu\u2026.sendEvent(BackPressed) }"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "resources"

    .line 209
    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v2 .. v7}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->setPrimaryButton(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 210
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final setUploadedTimeStamp(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;)V
    .locals 3

    .line 147
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$ReadOnly;

    const-string/jumbo v1, "uploadedAtMessageView"

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$ReadOnly;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$ReadOnly;->getUploadedAt()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v2, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    .line 148
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->uploadedAtMessageView:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v2}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->uploadedAtMessageView:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$ReadOnly;->getUploadedAt()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 151
    :cond_3
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->uploadedAtMessageView:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method private final showImage(Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;)V
    .locals 2

    .line 114
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$PreviewableFile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->picasso:Lcom/squareup/picasso/Picasso;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$PreviewableFile;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$PreviewableFile;->getFile()Ljava/io/File;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/picasso/Picasso;->load(Ljava/io/File;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    const-string v0, "picasso.load(file)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->displayPhoto(Lcom/squareup/picasso/RequestCreator;)V

    goto :goto_0

    .line 115
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$PreviewableUri;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->picasso:Lcom/squareup/picasso/Picasso;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$PreviewableUri;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$PreviewableUri;->getUri()Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/picasso/Picasso;->load(Landroid/net/Uri;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    const-string v0, "picasso.load(uri)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->displayPhoto(Lcom/squareup/picasso/RequestCreator;)V

    goto :goto_0

    .line 116
    :cond_1
    sget-object v0, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$PdfIcon;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$PdfIcon;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "imageContainerView"

    if-eqz v0, :cond_3

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->imageContainerView:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;

    if-nez p1, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->showPdfPlaceHolderImage()V

    goto :goto_0

    .line 117
    :cond_3
    sget-object v0, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$FileIcon;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$FileIcon;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->imageContainerView:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;

    if-nez p1, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->showBlankFilePlaceHolderImage()V

    goto :goto_0

    .line 118
    :cond_5
    sget-object v0, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$Downloading;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$Downloading;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->imageContainerView:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;

    if-nez p1, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->displayProgressBar()V

    goto :goto_0

    .line 119
    :cond_7
    sget-object v0, Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$Error;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage$Error;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_9

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->imageContainerView:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;

    if-nez p1, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->displayError()V

    :cond_9
    :goto_0
    return-void
.end method

.method private final spinnerData(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 4

    .line 127
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->getShowProgress()Z

    move-result p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, p1, v1, v2, v3}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method private final update(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;)V"
        }
    .end annotation

    .line 98
    invoke-virtual {p3}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->getConfig()Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    move-result-object v0

    .line 100
    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$update$1;

    invoke-direct {v1, p4}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$update$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 101
    invoke-direct {p0, p4, v0, p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->setUpActionBar(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;)V

    .line 102
    invoke-direct {p0, p3, p4}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->setListeners(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 103
    invoke-direct {p0, v0, p4}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->setRemoveAttachmentButton(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 104
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->setUploadedTimeStamp(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;)V

    .line 105
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->legalHelperMessageView:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_0

    const-string p2, "legalHelperMessageView"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Landroid/view/View;

    instance-of p2, v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig$Upload;

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 107
    invoke-virtual {p3}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->getPreviewImage()Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->showImage(Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;)V

    .line 109
    invoke-virtual {p3}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->getTitle()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->getExtensionToPreserve()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->updateFileTitleEditText(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final updateFileTitleEditText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .line 136
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->fileTitleEditText:Lcom/squareup/widgets/SelectableEditText;

    const-string v1, "fileTitleEditText"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 139
    invoke-static {p1, p2, v0, v2, v3}, Lkotlin/text/StringsKt;->endsWith$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 140
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x2e

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 142
    :goto_0
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->fileTitleEditText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p2, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->bindViews(Landroid/view/View;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "glassSpinner.showOrHideSpinner(view.context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 78
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 80
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 81
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->screens:Lio/reactivex/Observable;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->imageContainerView:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;

    if-nez v2, :cond_0

    const-string v3, "imageContainerView"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->photoLoadedState()Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "imageContainerView.photoLoadedState()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    new-instance v2, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$attach$1;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$attach$1;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;)V

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2(Lrx/functions/Func1;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 84
    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$attach$2;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "Observables\n        .com\u2026low\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
