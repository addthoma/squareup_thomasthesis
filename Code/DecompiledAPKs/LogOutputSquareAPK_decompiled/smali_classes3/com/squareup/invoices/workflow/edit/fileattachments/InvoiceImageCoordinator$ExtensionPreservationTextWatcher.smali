.class public final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "InvoiceImageCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ExtensionPreservationTextWatcher"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceImageCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceImageCoordinator.kt\ncom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher\n*L\n1#1,291:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0004\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\tJ\u0012\u0010\n\u001a\u00020\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "editText",
        "Lcom/squareup/widgets/SelectableEditText;",
        "extensionToPreserve",
        "",
        "workflowInput",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        "(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;Lcom/squareup/widgets/SelectableEditText;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowInput;)V",
        "afterTextChanged",
        "",
        "editable",
        "Landroid/text/Editable;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final editText:Lcom/squareup/widgets/SelectableEditText;

.field private final extensionToPreserve:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;

.field private final workflowInput:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;Lcom/squareup/widgets/SelectableEditText;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/widgets/SelectableEditText;",
            "Ljava/lang/String;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "editText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "extensionToPreserve"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflowInput"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;

    .line 273
    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;->editText:Lcom/squareup/widgets/SelectableEditText;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;->extensionToPreserve:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;->workflowInput:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .line 276
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 277
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 278
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;->extensionToPreserve:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 281
    invoke-static {p1, v0, v3, v1, v2}, Lkotlin/text/StringsKt;->endsWith$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 283
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;->editText:Lcom/squareup/widgets/SelectableEditText;

    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 284
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;->editText:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    check-cast p1, Landroid/text/Spannable;

    invoke-static {p1, v3}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    move-object p1, v0

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$ExtensionPreservationTextWatcher;->workflowInput:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$AttachmentTitleUpdated;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$AttachmentTitleUpdated;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void

    .line 277
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
