.class public final Lcom/squareup/invoices/workflow/edit/InvoiceReminderKt;
.super Ljava/lang/Object;
.source "InvoiceReminder.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0000\u001a\u00020\u0003*\u00020\u0004\u001a\n\u0010\u0005\u001a\u00020\u0002*\u00020\u0001\u001a\n\u0010\u0005\u001a\u00020\u0004*\u00020\u0003\u00a8\u0006\u0006"
    }
    d2 = {
        "toProto",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;",
        "toReminder",
        "invoices-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toProto(Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;
    .locals 2

    const-string v0, "$this$toProto"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;-><init>()V

    .line 80
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;->getConfigToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;

    move-result-object v0

    .line 81
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;->getRelativeDays()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->relative_days(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;

    move-result-object v0

    .line 82
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;->getCustomMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->custom_message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;

    move-result-object p0

    .line 83
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    move-result-object p0

    const-string v0, "InvoiceReminderConfig.Bu\u2026omMessage)\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toProto(Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;
    .locals 2

    const-string v0, "$this$toProto"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;-><init>()V

    .line 88
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getConfigToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->reminder_config_token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    move-result-object v0

    .line 89
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getRelativeDays()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->relative_days(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    move-result-object v0

    .line 90
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getCustomMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->custom_message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    move-result-object v0

    .line 91
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getServerState()Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->state(Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    move-result-object v0

    .line 92
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getSentOn()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->sent_on(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    move-result-object v0

    .line 93
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getToken()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    move-result-object p0

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object p0

    const-string v0, "InvoiceReminderInstance.\u2026ken(token)\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toReminder(Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;
    .locals 4

    const-string v0, "$this$toReminder"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    new-instance v0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;->token:Ljava/lang/String;

    .line 73
    iget-object v2, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;->relative_days:Ljava/lang/Integer;

    const-string v3, "relative_days"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 74
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;->custom_message:Ljava/lang/String;

    .line 71
    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-object v0
.end method

.method public static final toReminder(Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;
    .locals 8

    const-string v0, "$this$toReminder"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    new-instance v0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    .line 61
    iget-object v2, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->reminder_config_token:Ljava/lang/String;

    .line 62
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->relative_days:Ljava/lang/Integer;

    const-string v3, "relative_days"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 63
    iget-object v4, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->custom_message:Ljava/lang/String;

    .line 64
    iget-object v5, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 65
    iget-object v6, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->state:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    .line 66
    iget-object v7, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->token:Ljava/lang/String;

    move-object v1, v0

    .line 60
    invoke-direct/range {v1 .. v7}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;Ljava/lang/String;)V

    return-object v0
.end method
