.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;
.super Ljava/lang/Object;
.source "PaymentScheduleValidator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPaymentScheduleValidator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PaymentScheduleValidator.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,250:1\n1360#2:251\n1429#2,3:252\n1360#2:255\n1429#2,3:256\n1866#2,7:259\n1360#2:266\n1429#2,3:267\n1642#2,2:270\n1360#2:272\n1429#2,3:273\n1529#2,3:276\n1360#2:279\n1429#2,3:280\n1866#2,7:283\n1360#2:290\n1429#2,3:291\n1866#2,7:294\n*E\n*S KotlinDebug\n*F\n+ 1 PaymentScheduleValidator.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator\n*L\n122#1:251\n122#1,3:252\n127#1:255\n127#1,3:256\n142#1,7:259\n158#1:266\n158#1,3:267\n160#1,2:270\n167#1:272\n167#1,3:273\n192#1,3:276\n198#1:279\n198#1,3:280\n199#1,7:283\n207#1:290\n207#1,3:291\n208#1,7:294\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B5\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000e\u0008\u0001\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u001c\u0010\u0011\u001a\u00020\u00122\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00142\u0006\u0010\u0016\u001a\u00020\u0006J\u001a\u0010\u0017\u001a\u00020\u0018*\u0008\u0012\u0004\u0012\u00020\u00150\u00142\u0006\u0010\u0016\u001a\u00020\u0006H\u0002J\u0012\u0010\u0019\u001a\u00020\u001a*\u0008\u0012\u0004\u0012\u00020\u00150\u0014H\u0002J\u0012\u0010\u001b\u001a\u00020\u001a*\u0008\u0012\u0004\u0012\u00020\u00150\u0014H\u0002J\u000c\u0010\u001c\u001a\u00020\u001a*\u00020\u0006H\u0002J\u001a\u0010\u001d\u001a\u00020\u001a*\u0008\u0012\u0004\u0012\u00020\u00150\u00142\u0006\u0010\u0016\u001a\u00020\u0006H\u0002J\u001a\u0010\u001e\u001a\u00020\u0018*\u0008\u0012\u0004\u0012\u00020\u00150\u00142\u0006\u0010\u001f\u001a\u00020\u0006H\u0002J\u0015\u0010 \u001a\u00020\u0006*\u00020\u00062\u0006\u0010!\u001a\u00020\u0006H\u0082\u0002R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000c\u001a\n \u000e*\u0004\u0018\u00010\r0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n \u000e*\u0004\u0018\u00010\r0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\n \u000e*\u0004\u0018\u00010\r0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;",
        "",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "percentageFormatter",
        "Lcom/squareup/util/Percentage;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V",
        "oneHundredPercent",
        "",
        "kotlin.jvm.PlatformType",
        "zeroDollar",
        "zeroPercent",
        "validate",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult;",
        "paymentRequests",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "invoiceAmount",
        "compareTotalInstallmentsToBalance",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;",
        "containsConflictingDueDates",
        "",
        "depositDueAfterBalance",
        "equalsZero",
        "hasZeroAmount",
        "installmentsEqualBalance",
        "balanceAmount",
        "minus",
        "money",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final oneHundredPercent:Ljava/lang/CharSequence;

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final zeroDollar:Ljava/lang/CharSequence;

.field private final zeroPercent:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V
    .locals 1
    .param p3    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/text/ForPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->percentageFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->res:Lcom/squareup/util/Res;

    .line 41
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->moneyFormatter:Lcom/squareup/text/Formatter;

    const-wide/16 p3, 0x0

    invoke-static {p3, p4, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->zeroDollar:Ljava/lang/CharSequence;

    .line 42
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->percentageFormatter:Lcom/squareup/text/Formatter;

    sget-object p2, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    const/4 p3, 0x0

    invoke-virtual {p2, p3}, Lcom/squareup/util/Percentage$Companion;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->zeroPercent:Ljava/lang/CharSequence;

    .line 43
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->percentageFormatter:Lcom/squareup/text/Formatter;

    sget-object p2, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    const/16 p3, 0x64

    invoke-virtual {p2, p3}, Lcom/squareup/util/Percentage$Companion;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->oneHundredPercent:Ljava/lang/CharSequence;

    return-void
.end method

.method private final compareTotalInstallmentsToBalance(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;"
        }
    .end annotation

    .line 174
    invoke-static {p1, p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateBalanceAmount(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 175
    sget-object v0, Lcom/squareup/invoices/PaymentRequestsConfig;->Companion:Lcom/squareup/invoices/PaymentRequestsConfig$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/PaymentRequestsConfig$Companion;->fromPaymentRequests(Ljava/util/List;)Lcom/squareup/invoices/PaymentRequestsConfig;

    move-result-object v0

    .line 176
    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {v0}, Lcom/squareup/invoices/PaymentRequestsConfig;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 183
    sget-object p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$Equal;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$Equal;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;

    goto :goto_0

    .line 181
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->installmentsEqualBalance(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;

    move-result-object p1

    goto :goto_0

    .line 178
    :cond_1
    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->drop(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->installmentsEqualBalance(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final containsConflictingDueDates(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)Z"
        }
    .end annotation

    .line 122
    check-cast p1, Ljava/lang/Iterable;

    .line 251
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 252
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 253
    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 122
    iget-object v1, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 254
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 123
    move-object p1, v0

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p1

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-eq p1, v0, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method private final depositDueAfterBalance(Ljava/util/List;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)Z"
        }
    .end annotation

    .line 146
    sget-object v0, Lcom/squareup/invoices/PaymentRequestsConfig;->Companion:Lcom/squareup/invoices/PaymentRequestsConfig$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/PaymentRequestsConfig$Companion;->fromPaymentRequests(Ljava/util/List;)Lcom/squareup/invoices/PaymentRequestsConfig;

    move-result-object v0

    .line 147
    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/invoices/PaymentRequestsConfig;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_7

    const/4 v3, 0x2

    if-eq v0, v3, :cond_6

    const/4 v3, 0x3

    const/16 v4, 0xa

    if-eq v0, v3, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_7

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 167
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid config: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    check-cast p1, Ljava/lang/Iterable;

    .line 272
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {p1, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 273
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 274
    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 167
    iget-object v2, v2, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 275
    :cond_0
    check-cast v1, Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 167
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 156
    :cond_2
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    .line 157
    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->drop(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 266
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {p1, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 267
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 268
    check-cast v4, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 158
    iget-object v4, v4, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 269
    :cond_3
    check-cast v3, Ljava/util/List;

    .line 160
    check-cast v3, Ljava/lang/Iterable;

    .line 270
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 161
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-string v6, "it"

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-lez v3, :cond_4

    return v2

    :cond_5
    return v1

    .line 150
    :cond_6
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    .line 151
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    .line 153
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-string v0, "remainderDueDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long p1, v3, v5

    if-lez p1, :cond_7

    const/4 v1, 0x1

    :cond_7
    return v1
.end method

.method private final equalsZero(Lcom/squareup/protos/common/Money;)Z
    .locals 3

    .line 187
    iget-object v0, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v1, "currency_code"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v1, 0x0

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private final hasZeroAmount(Ljava/util/List;Lcom/squareup/protos/common/Money;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")Z"
        }
    .end annotation

    .line 127
    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    .line 255
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 256
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 257
    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 128
    iget-object v5, v2, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eqz v5, :cond_8

    sget-object v6, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v5}, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->ordinal()I

    move-result v5

    aget v5, v6, v5

    const-string v6, "it.fixed_amount"

    if-eq v5, v4, :cond_5

    const/4 v7, 0x2

    const-wide/16 v8, 0x0

    if-eq v5, v7, :cond_3

    const/4 v7, 0x3

    if-eq v5, v7, :cond_2

    const/4 v6, 0x4

    if-eq v5, v6, :cond_0

    const/4 v2, 0x5

    if-ne v5, v2, :cond_8

    .line 139
    invoke-static {p1, p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateBalanceAmount(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->equalsZero(Lcom/squareup/protos/common/Money;)Z

    move-result v3

    goto :goto_3

    .line 138
    :cond_0
    iget-object v2, v2, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    if-nez v2, :cond_1

    goto :goto_3

    :cond_1
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v2, v5, v8

    if-nez v2, :cond_7

    goto :goto_2

    .line 137
    :cond_2
    iget-object v2, v2, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->equalsZero(Lcom/squareup/protos/common/Money;)Z

    move-result v3

    goto :goto_3

    .line 135
    :cond_3
    iget-object v5, v2, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    if-nez v5, :cond_4

    goto :goto_1

    :cond_4
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v7, v5, v8

    if-eqz v7, :cond_6

    :goto_1
    iget-object v2, v2, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-wide/16 v7, 0x64

    cmp-long v2, v5, v7

    if-ltz v2, :cond_7

    goto :goto_2

    .line 131
    :cond_5
    iget-object v5, v2, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v5}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->equalsZero(Lcom/squareup/protos/common/Money;)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, p2}, Lcom/squareup/money/MoneyMathOperatorsKt;->compareTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)I

    move-result v2

    if-ltz v2, :cond_7

    :cond_6
    :goto_2
    const/4 v3, 0x1

    .line 141
    :cond_7
    :goto_3
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 140
    :cond_8
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "payment request must have amount type"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 258
    :cond_9
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 259
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 260
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_d

    .line 261
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    .line 262
    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 263
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-nez p2, :cond_b

    if-eqz v0, :cond_a

    goto :goto_5

    :cond_a
    const/4 p2, 0x0

    goto :goto_6

    :cond_b
    :goto_5
    const/4 p2, 0x1

    .line 142
    :goto_6
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    goto :goto_4

    .line 265
    :cond_c
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1

    .line 260
    :cond_d
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Empty collection can\'t be reduced."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final installmentsEqualBalance(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;"
        }
    .end annotation

    .line 192
    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    .line 276
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 277
    :cond_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 192
    invoke-static {v4}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_f

    .line 195
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v1, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_INSTALLMENT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne p1, v1, :cond_3

    const/4 v2, 0x1

    :cond_3
    const-string p1, "Empty collection can\'t be reduced."

    const/16 v1, 0xa

    if-eqz v2, :cond_9

    .line 279
    new-instance p2, Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p2, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 280
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 281
    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 198
    iget-object v1, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    long-to-int v2, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 282
    :cond_4
    check-cast p2, Ljava/util/List;

    check-cast p2, Ljava/lang/Iterable;

    .line 283
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    .line 284
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 285
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    .line 286
    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 287
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    add-int/2addr p1, v0

    .line 199
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_2

    .line 198
    :cond_5
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    const/16 p2, 0x64

    if-ne p1, p2, :cond_6

    .line 202
    sget-object p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$Equal;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$Equal;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;

    goto/16 :goto_5

    :cond_6
    if-le p1, p2, :cond_7

    .line 203
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentGreaterPercentage;

    sub-int/2addr p1, p2

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentGreaterPercentage;-><init>(I)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;

    goto/16 :goto_5

    .line 204
    :cond_7
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentLessPercentage;

    sub-int/2addr p2, p1

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentLessPercentage;-><init>(I)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;

    goto/16 :goto_5

    .line 284
    :cond_8
    new-instance p2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p2, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 290
    :cond_9
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 291
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 292
    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 207
    iget-object v1, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 293
    :cond_a
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 294
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 295
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 296
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    .line 297
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 298
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Money;

    check-cast p1, Lcom/squareup/protos/common/Money;

    .line 208
    invoke-static {p1, v1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_4

    .line 207
    :cond_b
    check-cast p1, Lcom/squareup/protos/common/Money;

    .line 211
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    sget-object p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$Equal;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$Equal;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;

    goto :goto_5

    :cond_c
    const-string/jumbo v0, "totalMoney"

    .line 212
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, p2}, Lcom/squareup/money/MoneyMathOperatorsKt;->compareTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)I

    move-result v0

    if-lez v0, :cond_d

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentGreaterMoney;

    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->minus(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentGreaterMoney;-><init>(Lcom/squareup/protos/common/Money;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;

    goto :goto_5

    .line 213
    :cond_d
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentLessMoney;

    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->minus(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentLessMoney;-><init>(Lcom/squareup/protos/common/Money;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;

    :goto_5
    return-object p1

    .line 295
    :cond_e
    new-instance p2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p2, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 193
    :cond_f
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Must only contain installments."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final minus(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 1

    const-string v0, "$this$minus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 219
    invoke-static {p1, p2}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    const-string p2, "MoneyMath.subtract(this, money)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final validate(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult;"
        }
    .end annotation

    const-string v0, "paymentRequests"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->hasZeroAmount(Ljava/util/List;Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;

    .line 52
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->payment_schedule_validation_invalid_amount:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 53
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->payment_schedule_validation_invalid_amount_body:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->zeroDollar:Ljava/lang/CharSequence;

    const-string/jumbo v2, "zero_money"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->zeroPercent:Ljava/lang/CharSequence;

    const-string/jumbo v2, "zero_percent"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 51
    invoke-direct {p1, p2, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult;

    goto/16 :goto_1

    .line 60
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->compareTotalInstallmentsToBalance(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$Equal;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$Equal;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    .line 61
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->compareTotalInstallmentsToBalance(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;

    move-result-object v0

    .line 62
    invoke-static {p1, p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateBalanceAmount(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 64
    instance-of p2, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentGreaterMoney;

    const-string v1, "balance_money"

    const-string v2, "money"

    if-eqz p2, :cond_1

    .line 65
    new-instance p2, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;

    .line 66
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->payment_schedule_validation_balance_high:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 67
    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/features/invoices/R$string;->payment_schedule_validation_balance_high_money:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    .line 68
    iget-object v5, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->moneyFormatter:Lcom/squareup/text/Formatter;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentGreaterMoney;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentGreaterMoney;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {v5, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v4, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 69
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 70
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 71
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 65
    invoke-direct {p2, v3, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 74
    :cond_1
    instance-of p2, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentGreaterPercentage;

    const-string v3, "one_hundred_percent"

    if-eqz p2, :cond_2

    .line 75
    new-instance p2, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;

    .line 76
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->payment_schedule_validation_balance_high:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 77
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->payment_schedule_validation_balance_one_hundred_percent:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->oneHundredPercent:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 75
    invoke-direct {p2, p1, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 83
    :cond_2
    instance-of p2, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentLessMoney;

    if-eqz p2, :cond_3

    .line 84
    new-instance p2, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;

    .line 85
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->payment_schedule_validation_balance_low:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 86
    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/features/invoices/R$string;->payment_schedule_validation_balance_low_body:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    .line 87
    iget-object v5, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->moneyFormatter:Lcom/squareup/text/Formatter;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentLessMoney;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentLessMoney;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {v5, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v4, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 88
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 89
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 90
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 84
    invoke-direct {p2, v3, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :cond_3
    instance-of p1, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentLessPercentage;

    if-eqz p1, :cond_4

    .line 94
    new-instance p2, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;

    .line 95
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->payment_schedule_validation_balance_low:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 96
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->payment_schedule_validation_balance_one_hundred_percent:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 97
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->oneHundredPercent:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 94
    invoke-direct {p2, p1, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :goto_0
    move-object p1, p2

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult;

    goto :goto_1

    .line 102
    :cond_4
    sget-object p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$Equal;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$Equal;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot be equal"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 105
    :cond_6
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->containsConflictingDueDates(Ljava/util/List;)Z

    move-result p2

    if-eqz p2, :cond_7

    .line 106
    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;

    .line 107
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->payment_schedule_validation_conflicting_due_date:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 108
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->payment_schedule_validation_conflicting_due_dates_body:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 106
    invoke-direct {p1, p2, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult;

    goto :goto_1

    .line 111
    :cond_7
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->depositDueAfterBalance(Ljava/util/List;)Z

    move-result p1

    if-eqz p1, :cond_8

    .line 112
    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;

    .line 113
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->payment_schedule_validation_deposit_after_balance:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string v0, ""

    .line 112
    invoke-direct {p1, p2, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Error;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult;

    goto :goto_1

    .line 117
    :cond_8
    sget-object p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Success;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult$Success;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/ValidationResult;

    :goto_1
    return-object p1
.end method
