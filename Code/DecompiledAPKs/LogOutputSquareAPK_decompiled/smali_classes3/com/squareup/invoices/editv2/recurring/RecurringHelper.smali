.class public final Lcom/squareup/invoices/editv2/recurring/RecurringHelper;
.super Ljava/lang/Object;
.source "RecurringHelper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/editv2/recurring/RecurringHelper$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eJ\u000e\u0010\u000f\u001a\u00020\n*\u0004\u0018\u00010\u0006H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00068BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/recurring/RecurringHelper;",
        "",
        "clock",
        "Lcom/squareup/util/Clock;",
        "(Lcom/squareup/util/Clock;)V",
        "today",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "getToday",
        "()Lcom/squareup/protos/common/time/YearMonthDay;",
        "willSeriesRecurEndOfMonth",
        "",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "rule",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
        "isOverByMonthDayLimit",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/editv2/recurring/RecurringHelper$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final RECURRING_BYMONTHDAY_LIMIT:I = 0x1c


# instance fields
.field private final clock:Lcom/squareup/util/Clock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/editv2/recurring/RecurringHelper$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/editv2/recurring/RecurringHelper$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/editv2/recurring/RecurringHelper;->Companion:Lcom/squareup/invoices/editv2/recurring/RecurringHelper$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Clock;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "clock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/recurring/RecurringHelper;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method private final getToday()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 2

    .line 15
    iget-object v0, p0, Lcom/squareup/invoices/editv2/recurring/RecurringHelper;->clock:Lcom/squareup/util/Clock;

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDateUtility;->getToday(Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    const-string v1, "getToday(clock)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final isOverByMonthDayLimit(Lcom/squareup/protos/common/time/YearMonthDay;)Z
    .locals 1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 34
    :cond_0
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/recurring/RecurringHelper;->getToday()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    :goto_0
    iget-object p1, p1, Lcom/squareup/protos/common/time/YearMonthDay;->day_of_month:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/16 v0, 0x1c

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->compare(II)I

    move-result p1

    if-lez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method


# virtual methods
.method public final willSeriesRecurEndOfMonth(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Z
    .locals 1

    const-string v0, "invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rule"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/recurring/RecurringHelper;->isOverByMonthDayLimit(Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->isFrequencyMonthly()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
