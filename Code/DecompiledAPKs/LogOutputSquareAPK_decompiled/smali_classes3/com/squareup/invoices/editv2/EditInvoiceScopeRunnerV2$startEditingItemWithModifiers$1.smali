.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$startEditingItemWithModifiers$1;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->startEditingItemWithModifiers(Lcom/squareup/configure/item/WorkingItem;ZLcom/squareup/ui/main/RegisterTreeKey;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/Command;",
        "kotlin.jvm.PlatformType",
        "history",
        "Lflow/History;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $parentPath:Lcom/squareup/ui/main/RegisterTreeKey;

.field final synthetic $showPriceScreenFirst:Z

.field final synthetic $workingItem:Lcom/squareup/configure/item/WorkingItem;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$startEditingItemWithModifiers$1;->$parentPath:Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$startEditingItemWithModifiers$1;->$workingItem:Lcom/squareup/configure/item/WorkingItem;

    iput-boolean p3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$startEditingItemWithModifiers$1;->$showPriceScreenFirst:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lflow/History;)Lcom/squareup/container/Command;
    .locals 3

    const-string v0, "history"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 454
    new-instance v0, Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$startEditingItemWithModifiers$1;->$parentPath:Lcom/squareup/ui/main/RegisterTreeKey;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$startEditingItemWithModifiers$1;->$workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-direct {v0, v1, v2}, Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;)V

    .line 455
    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    .line 456
    invoke-virtual {p1, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    .line 457
    iget-boolean v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$startEditingItemWithModifiers$1;->$showPriceScreenFirst:Z

    if-eqz v1, :cond_0

    .line 458
    new-instance v1, Lcom/squareup/configure/item/ConfigureItemPriceScreen;

    invoke-virtual {v0}, Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;->getParentKey()Lcom/squareup/configure/item/ConfigureItemScope;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    invoke-virtual {p1, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 460
    :cond_0
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    sget-object v0, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {p1, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method
