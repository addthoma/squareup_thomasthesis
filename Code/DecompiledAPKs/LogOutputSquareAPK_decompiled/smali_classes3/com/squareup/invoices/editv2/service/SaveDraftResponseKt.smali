.class public final Lcom/squareup/invoices/editv2/service/SaveDraftResponseKt;
.super Ljava/lang/Object;
.source "SaveDraftResponse.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "format",
        "Lcom/squareup/receiving/FailureMessage;",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "saveDraftResponse",
        "Lcom/squareup/invoices/editv2/service/SaveDraftResponse;",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final format(Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)Lcom/squareup/receiving/FailureMessage;
    .locals 2

    const-string v0, "$this$format"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "saveDraftResponse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    instance-of v0, p1, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$SingleInvoice;

    const-string v1, "SaveDraftResponse was a success - cannot format failure message"

    if-eqz v0, :cond_2

    .line 68
    check-cast p1, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$SingleInvoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$SingleInvoice;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    .line 70
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-static {p1, p0}, Lcom/squareup/invoices/editv2/service/EditInvoiceFailureUtilsKt;->toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;Lcom/squareup/receiving/FailureMessageFactory;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p0

    goto :goto_0

    .line 71
    :cond_0
    instance-of p0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz p0, :cond_1

    new-instance p0, Ljava/lang/IllegalStateException;

    invoke-direct {p0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 76
    :cond_2
    instance-of v0, p1, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;

    if-eqz v0, :cond_5

    .line 77
    check-cast p1, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    .line 79
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-static {p1, p0}, Lcom/squareup/invoices/editv2/service/EditInvoiceFailureUtilsKt;->toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;Lcom/squareup/receiving/FailureMessageFactory;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p0

    :goto_0
    return-object p0

    .line 80
    :cond_3
    instance-of p0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz p0, :cond_4

    new-instance p0, Ljava/lang/IllegalStateException;

    invoke-direct {p0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    :cond_4
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 78
    :cond_5
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
