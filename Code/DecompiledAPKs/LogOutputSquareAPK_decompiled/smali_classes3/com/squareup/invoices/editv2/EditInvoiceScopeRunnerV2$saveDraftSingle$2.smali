.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraftSingle$2;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->saveDraftSingle()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/invoices/editv2/service/SaveDraftResponse;",
        "+",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceScopeRunnerV2.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceScopeRunnerV2.kt\ncom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraftSingle$2\n+ 2 ProtosPure.kt\ncom/squareup/util/ProtosPure\n*L\n1#1,1749:1\n132#2,3:1750\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoiceScopeRunnerV2.kt\ncom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraftSingle$2\n*L\n914#1,3:1750\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012&\u0010\u0002\u001a\"\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0006*\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/invoices/editv2/service/SaveDraftResponse;",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraftSingle$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 236
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraftSingle$2;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/invoices/editv2/service/SaveDraftResponse;",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/editv2/service/SaveDraftResponse;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/invoice/Invoice;

    .line 912
    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse;->isSuccess()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 913
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraftSingle$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getInvoiceUnitCache$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->setHasInvoicesTrue()V

    .line 914
    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p1

    check-cast p1, Lcom/squareup/wire/Message;

    .line 1751
    invoke-virtual {p1}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object p1

    if-eqz p1, :cond_2

    move-object v1, p1

    check-cast v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 916
    instance-of v2, v0, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;

    if-eqz v2, :cond_1

    .line 918
    check-cast v0, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;->recurring_invoice:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/RecurringSeries;->recurrence_schedule:Lcom/squareup/protos/client/invoice/RecurringSchedule;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/RecurringSchedule;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v0, v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.receiving.StandardReceiver.SuccessOrFailure.HandleSuccess<com.squareup.protos.client.invoice.SaveDraftRecurringSeriesResponse>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1752
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p1

    .line 914
    check-cast p1, Lcom/squareup/protos/client/invoice/Invoice;

    .line 925
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraftSingle$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getWorkingInvoiceEditor$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    move-result-object v0

    const-string v1, "invoiceToSet"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraftSingle$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceContext$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->set(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V

    goto :goto_1

    .line 1751
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type B"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    return-void
.end method
