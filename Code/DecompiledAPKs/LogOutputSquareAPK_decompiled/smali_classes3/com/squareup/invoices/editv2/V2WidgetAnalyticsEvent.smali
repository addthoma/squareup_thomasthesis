.class abstract Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "EditInvoiceScopeRunnerV2.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$OldWidgets;,
        Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$NewWidgets;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u00020\u0001:\u0002\u0005\u0006B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u0082\u0001\u0002\u0007\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent;",
        "Lcom/squareup/analytics/event/v1/ActionEvent;",
        "eventNamedAction",
        "Lcom/squareup/analytics/EventNamedAction;",
        "(Lcom/squareup/analytics/EventNamedAction;)V",
        "NewWidgets",
        "OldWidgets",
        "Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$OldWidgets;",
        "Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$NewWidgets;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/squareup/analytics/EventNamedAction;)V
    .locals 0

    .line 1742
    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/analytics/EventNamedAction;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1740
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    return-void
.end method
