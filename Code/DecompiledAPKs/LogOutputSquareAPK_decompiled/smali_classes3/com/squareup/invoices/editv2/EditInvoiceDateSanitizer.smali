.class public final Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;
.super Ljava/lang/Object;
.source "EditInvoiceDateSanitizer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceDateSanitizer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceDateSanitizer.kt\ncom/squareup/invoices/editv2/EditInvoiceDateSanitizer\n+ 2 ProtosPure.kt\ncom/squareup/util/ProtosPure\n*L\n1#1,56:1\n132#2,3:57\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoiceDateSanitizer.kt\ncom/squareup/invoices/editv2/EditInvoiceDateSanitizer\n*L\n30#1,3:57\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000eJ\u000e\u0010\u000f\u001a\u00020\u0010*\u0004\u0018\u00010\u0006H\u0002J\u000c\u0010\u0011\u001a\u00020\u0010*\u00020\u000eH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u00068BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;",
        "",
        "clock",
        "Lcom/squareup/util/Clock;",
        "(Lcom/squareup/util/Clock;)V",
        "today",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "kotlin.jvm.PlatformType",
        "getToday",
        "()Lcom/squareup/protos/common/time/YearMonthDay;",
        "sanitize",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "invoice",
        "editInvoiceContext",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
        "beforeToday",
        "",
        "shouldUpdateDates",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "clock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method private final beforeToday(Lcom/squareup/protos/common/time/YearMonthDay;)Z
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;->clock:Lcom/squareup/util/Clock;

    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->beforeToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Z

    move-result p1

    return p1
.end method

.method private final getToday()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;->clock:Lcom/squareup/util/Clock;

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDateUtility;->getToday(Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    return-object v0
.end method

.method private final shouldUpdateDates(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z
    .locals 3

    .line 42
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    goto :goto_0

    .line 43
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isDraft()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 44
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    if-eqz v0, :cond_2

    .line 45
    check-cast p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;->getInvoiceDisplayDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 46
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->FAILED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    if-eq p1, v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNDELIVERED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    if-ne p1, v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :cond_3
    :goto_0
    return v2
.end method


# virtual methods
.method public final sanitize(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Lcom/squareup/protos/client/invoice/Invoice;
    .locals 2

    const-string v0, "invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editInvoiceContext"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    check-cast p1, Lcom/squareup/wire/Message;

    .line 58
    invoke-virtual {p1}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object p1

    if-eqz p1, :cond_2

    move-object v0, p1

    check-cast v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 33
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-direct {p0, v1}, Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;->beforeToday(Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, p2}, Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;->shouldUpdateDates(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 35
    :cond_0
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;->getToday()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p2

    iput-object p2, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 59
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p1

    const-string p2, "invoice.buildUpon {\n    \u2026cheduled_at = today\n    }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/protos/client/invoice/Invoice;

    return-object p1

    .line 58
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type B"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
