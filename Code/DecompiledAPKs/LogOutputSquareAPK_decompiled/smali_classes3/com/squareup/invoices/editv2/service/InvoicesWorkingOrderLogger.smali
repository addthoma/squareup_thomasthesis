.class public final Lcom/squareup/invoices/editv2/service/InvoicesWorkingOrderLogger;
.super Ljava/lang/Object;
.source "InvoicesWorkingOrderLogger.kt"

# interfaces
.implements Lcom/squareup/invoices/order/WorkingOrderLogger;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\tH\u0016J\u0008\u0010\n\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/service/InvoicesWorkingOrderLogger;",
        "Lcom/squareup/invoices/order/WorkingOrderLogger;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "logAddDiscount",
        "",
        "logAddItem",
        "itemId",
        "",
        "logAddOrEditCustomAmount",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/service/InvoicesWorkingOrderLogger;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public logAddDiscount()V
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/invoices/editv2/service/InvoicesWorkingOrderLogger;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->ADD_DISCOUNT_EVENT:Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logAddItem(Ljava/lang/String;)V
    .locals 2

    const-string v0, "itemId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iget-object v0, p0, Lcom/squareup/invoices/editv2/service/InvoicesWorkingOrderLogger;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logAddOrEditCustomAmount()V
    .locals 2

    .line 20
    iget-object v0, p0, Lcom/squareup/invoices/editv2/service/InvoicesWorkingOrderLogger;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->ADD_EDIT_CUSTOM_AMOUNT_EVENT:Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
