.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice2ScreenData$1;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoice2ScreenData()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001ah\u0012d\u0012b\u0012\u0004\u0012\u00020\u0003\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00060\u0006\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u0004 \u0007*0\u0012\u0004\u0012\u00020\u0003\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00060\u0006\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u0004\u0018\u00010\u00020\u00020\u00012&\u0010\t\u001a\"\u0012\u0004\u0012\u00020\u0003\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00060\u00060\nH\n\u00a2\u0006\u0002\u0008\u000b"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/util/tuple/Quartet;",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
        "",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Triple;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice2ScreenData$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Triple;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Triple<",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            "+",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/tuple/Quartet<",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;>;>;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Triple;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {p1}, Lkotlin/Triple;->component2()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Optional;

    invoke-virtual {p1}, Lkotlin/Triple;->component3()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    .line 724
    iget-object v2, v0, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    if-eqz v2, :cond_0

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/InvoiceContact;->contact_token:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 725
    :goto_0
    invoke-static {v0}, Lcom/squareup/invoices/Invoices;->getInstrumentToken(Lcom/squareup/protos/client/invoice/Invoice;)Ljava/lang/String;

    move-result-object v3

    .line 727
    iget-object v4, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice2ScreenData$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v4}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getInstrumentsStore$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;->findInstrument(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v2

    .line 728
    new-instance v3, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice2ScreenData$1$1;

    invoke-direct {v3, v0, v1, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice2ScreenData$1$1;-><init>(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/util/Optional;Ljava/lang/Boolean;)V

    check-cast v3, Lio/reactivex/functions/Function;

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 236
    check-cast p1, Lkotlin/Triple;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice2ScreenData$1;->apply(Lkotlin/Triple;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
