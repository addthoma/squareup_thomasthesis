.class public Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;
.super Lmortar/ViewPresenter;
.source "ItemSelectCardPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/invoices/edit/items/ItemSelectCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final entryHandler:Lcom/squareup/librarylist/SimpleEntryHandler;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final itemSelectScreenTutorialRunner:Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;

.field private final libraryListAssistant:Lcom/squareup/librarylist/SimpleLibraryListAssistant;

.field private final libraryListManager:Lcom/squareup/librarylist/LibraryListManager;

.field private final onUpButtonClicked:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEditor:Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;

.field private final passcodeGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;

.field private screen:Lcom/squareup/invoices/edit/items/ItemSelectScreen;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;Ljavax/inject/Provider;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListManager;Lcom/squareup/permissions/PermissionGatekeeper;Lflow/Flow;Lcom/squareup/librarylist/SimpleEntryHandler;Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;Lcom/squareup/librarylist/SimpleLibraryListAssistant;Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/librarylist/LibraryListManager;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lflow/Flow;",
            "Lcom/squareup/librarylist/SimpleEntryHandler;",
            "Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;",
            "Lcom/squareup/librarylist/SimpleLibraryListAssistant;",
            "Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 94
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 82
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->onUpButtonClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    move-object v1, p1

    .line 95
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->res:Lcom/squareup/util/Res;

    move-object v1, p2

    .line 96
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->orderEditor:Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;

    move-object v1, p4

    .line 97
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object v1, p5

    .line 98
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    move-object v1, p6

    .line 99
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p7

    .line 100
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->libraryListManager:Lcom/squareup/librarylist/LibraryListManager;

    move-object v1, p8

    .line 101
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->passcodeGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object v1, p3

    .line 102
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->cogsProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 103
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->flow:Lflow/Flow;

    move-object v1, p10

    .line 104
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->entryHandler:Lcom/squareup/librarylist/SimpleEntryHandler;

    move-object v1, p11

    .line 105
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->itemSelectScreenTutorialRunner:Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;

    move-object v1, p12

    .line 106
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->libraryListAssistant:Lcom/squareup/librarylist/SimpleLibraryListAssistant;

    move-object v1, p13

    .line 107
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->runner:Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;

    move-object/from16 v1, p14

    .line 108
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    move-object/from16 v1, p15

    .line 109
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v1, p16

    .line 110
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/shared/catalog/models/CatalogDiscount;)V
    .locals 0

    .line 64
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->addDiscount(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/shared/catalog/models/CatalogDiscount;)V

    return-void
.end method

.method private addCustomAmount()V
    .locals 3

    .line 189
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->orderEditor:Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;

    invoke-direct {p0}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->buildCustomItem()Lcom/squareup/checkout/CartItem;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;->addItemToCart(Lcom/squareup/checkout/CartItem;)V

    .line 190
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->itemSelectScreenTutorialRunner:Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;->setCreatingCustomAmount(Z)V

    .line 191
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->runner:Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;

    iget-object v2, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->orderEditor:Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;

    invoke-interface {v2}, Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-interface {v0, v2}, Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;->startEditingOrder(I)V

    return-void
.end method

.method private addDiscount(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/shared/catalog/models/CatalogDiscount;)V
    .locals 5

    .line 328
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getDiscountType()Lcom/squareup/api/items/Discount$DiscountType;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 329
    sget-object v1, Lcom/squareup/api/items/Discount$DiscountType;->FIXED:Lcom/squareup/api/items/Discount$DiscountType;

    if-eq v0, v1, :cond_1

    .line 331
    new-instance v0, Lcom/squareup/configure/item/WorkingDiscount;

    iget-object v1, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v0, p2, v1}, Lcom/squareup/configure/item/WorkingDiscount;-><init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 333
    invoke-virtual {v0}, Lcom/squareup/configure/item/WorkingDiscount;->isPercentage()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 334
    new-instance p2, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    iget-object v1, v0, Lcom/squareup/configure/item/WorkingDiscount;->percentageString:Ljava/lang/String;

    sget-object v2, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    .line 335
    invoke-static {v1, v2}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v1

    .line 336
    invoke-virtual {v0}, Lcom/squareup/configure/item/WorkingDiscount;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->apply:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p2, v1, v2, v3}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;-><init>(Lcom/squareup/util/Percentage;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 338
    :cond_0
    new-instance p2, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    iget-object v1, v0, Lcom/squareup/configure/item/WorkingDiscount;->amountMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0}, Lcom/squareup/configure/item/WorkingDiscount;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->apply:I

    .line 339
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p2, v1, v2, v3}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    :goto_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->runner:Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;

    invoke-interface {v1, v0, p2, p1}, Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;->startEditingDiscount(Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;Lcom/squareup/ui/main/RegisterTreeKey;)V

    goto :goto_1

    .line 344
    :cond_1
    sget-object p1, Lcom/squareup/checkout/Discount$Scope;->CART:Lcom/squareup/checkout/Discount$Scope;

    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    .line 345
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    invoke-static {p2, p1, v0}, Lcom/squareup/checkout/Discounts;->toAppliedDiscount(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/Discount$Scope;Z)Lcom/squareup/checkout/Discount;

    move-result-object p1

    .line 346
    iget-object p2, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->orderEditor:Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;

    invoke-interface {p2, p1}, Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;->addDiscountToCart(Lcom/squareup/checkout/Discount;)V

    .line 347
    iget-object p1, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->runner:Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;

    invoke-interface {p1}, Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;->goBackFromItemSelect()V

    :goto_1
    return-void
.end method

.method private buildCustomItem()Lcom/squareup/checkout/CartItem;
    .locals 5

    .line 357
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 358
    iget-object v1, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getAvailableTaxes()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Tax;

    .line 359
    iget-boolean v3, v2, Lcom/squareup/checkout/Tax;->enabled:Z

    if-eqz v3, :cond_0

    iget-boolean v3, v2, Lcom/squareup/checkout/Tax;->appliesToCustomAmounts:Z

    if-eqz v3, :cond_0

    .line 361
    iget-object v3, v2, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 365
    :cond_1
    new-instance v1, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v1}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;-><init>()V

    new-instance v3, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;-><init>()V

    sget-object v4, Lcom/squareup/checkout/OrderVariation;->CUSTOM_ITEM_VARIATION:Lcom/squareup/checkout/OrderVariation;

    .line 369
    invoke-virtual {v4}, Lcom/squareup/checkout/OrderVariation;->getItemVariation()Lcom/squareup/api/items/ItemVariation;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 368
    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->item_variation(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;

    move-result-object v3

    .line 370
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    move-result-object v3

    .line 367
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->available_options(Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    move-result-object v2

    .line 371
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    move-result-object v2

    .line 366
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->backingDetails(Lcom/squareup/protos/client/bills/Itemization$BackingDetails;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->orderEditor:Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;

    .line 372
    invoke-interface {v2}, Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->appliedDiscounts(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    sget-object v2, Lcom/squareup/checkout/OrderVariation;->CUSTOM_ITEM_VARIATION:Lcom/squareup/checkout/OrderVariation;

    .line 373
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation(Lcom/squareup/checkout/OrderVariation;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    .line 374
    invoke-virtual {v1, v0}, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    const-wide/16 v1, 0x0

    iget-object v3, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->orderEditor:Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;

    .line 375
    invoke-interface {v3}, Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/Order;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_ITEM_VARIATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    .line 376
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->backingType(Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 377
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    return-object v0
.end method

.method private getActionBarConfigForMode(Lcom/squareup/librarylist/LibraryListState;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 4

    .line 153
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState;->getFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v0

    sget-object v1, Lcom/squareup/librarylist/LibraryListState$Filter;->SINGLE_CATEGORY:Lcom/squareup/librarylist/LibraryListState$Filter;

    if-ne v0, v1, :cond_0

    .line 154
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState;->getCurrentCategoryName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->res:Lcom/squareup/util/Res;

    .line 156
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState;->getFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/squareup/librarylist/LibraryListState$Filter;->getStringResId(Lcom/squareup/catalogapi/CatalogIntegrationController;Z)I

    move-result v1

    .line 155
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 158
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState;->isTopLevel()Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_1

    :cond_1
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 162
    :goto_1
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 163
    invoke-virtual {v1, p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/invoices/edit/items/-$$Lambda$HE2Q5NWRQP3f95cvmK9_nOjODOY;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/items/-$$Lambda$HE2Q5NWRQP3f95cvmK9_nOjODOY;-><init>(Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;)V

    .line 164
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 165
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method private getCartItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V
    .locals 3

    .line 200
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object p1

    .line 201
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->screen:Lcom/squareup/invoices/edit/items/ItemSelectScreen;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/items/ItemSelectScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    .line 203
    iget-object v1, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cogs/Cogs;

    new-instance v2, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$0QGZqZERUj73TW-cH7DXc5gOoho;

    invoke-direct {v2, p1}, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$0QGZqZERUj73TW-cH7DXc5gOoho;-><init>(Ljava/lang/String;)V

    new-instance p1, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$sSlduxGU52XtWkVkOj9c_RSnU6Y;

    invoke-direct {p1, p0, v0}, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$sSlduxGU52XtWkVkOj9c_RSnU6Y;-><init>(Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-interface {v1, v2, p1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method private getDiscount(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V
    .locals 3

    .line 308
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object p1

    .line 309
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->screen:Lcom/squareup/invoices/edit/items/ItemSelectScreen;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/items/ItemSelectScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    .line 310
    iget-object v1, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cogs/Cogs;

    new-instance v2, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$OVYgL9wUvz3VzHwCYk2AOl9keuE;

    invoke-direct {v2, p1}, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$OVYgL9wUvz3VzHwCYk2AOl9keuE;-><init>(Ljava/lang/String;)V

    new-instance p1, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$F1yrDwtKN_qRbJdIlidntFFUV_E;

    invoke-direct {p1, p0, v0}, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$F1yrDwtKN_qRbJdIlidntFFUV_E;-><init>(Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-interface {v1, v2, p1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method static synthetic lambda$getCartItem$5(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;
    .locals 0

    .line 204
    invoke-static {p0, p1}, Lcom/squareup/librarylist/CogsSelectedEntryDataLocator;->getEntryDataForItem(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$getDiscount$7(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/models/CatalogDiscount;
    .locals 1

    .line 311
    const-class v0, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-interface {p1, v0, p0}, Lcom/squareup/shared/catalog/Catalog$Local;->findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    return-object p0
.end method

.method public static synthetic lambda$pF54NdAbS5uWKjX4kCMFakca3H4(Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->onEntryClicked(Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction;)V

    return-void
.end method

.method private onEntryClicked(Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction;)V
    .locals 3

    .line 169
    sget-object v0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$CustomAmount;->INSTANCE:Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$CustomAmount;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    invoke-direct {p0}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->addCustomAmount()V

    return-void

    .line 174
    :cond_0
    instance-of v0, p1, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Item;

    if-eqz v0, :cond_1

    .line 175
    check-cast p1, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Item;

    invoke-virtual {p1}, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Item;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->getCartItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    goto :goto_0

    .line 176
    :cond_1
    instance-of v0, p1, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;

    if-eqz v0, :cond_2

    .line 177
    check-cast p1, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;

    invoke-virtual {p1}, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->getDiscount(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    :goto_0
    return-void

    .line 179
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected entry type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private skipConfigureScreenForClickedItem(ZLcom/squareup/configure/item/WorkingItem;)V
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->screen:Lcom/squareup/invoices/edit/items/ItemSelectScreen;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/items/ItemSelectScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 292
    iget-object p1, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->runner:Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;

    invoke-interface {p1, p2, v0}, Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;->startEditingItemVariablePrice(Lcom/squareup/configure/item/WorkingItem;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void

    .line 297
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->orderEditor:Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;

    invoke-virtual {p2}, Lcom/squareup/configure/item/WorkingItem;->finishWithOnlyFixedPrice()Lcom/squareup/checkout/CartItem;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;->addItemToCart(Lcom/squareup/checkout/CartItem;)V

    .line 298
    iget-object p1, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->runner:Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;

    invoke-interface {p1}, Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;->goBackFromItemSelect()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$getCartItem$6$ItemSelectCardPresenter(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 206
    invoke-interface/range {p2 .. p2}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;

    .line 207
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getItem()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v4

    .line 210
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getItemImageOrNull()Lcom/squareup/shared/catalog/models/CatalogItemImage;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getItemImageOrNull()Lcom/squareup/shared/catalog/models/CatalogItemImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemImage;->getUrl()Ljava/lang/String;

    move-result-object v3

    :goto_0
    move-object v5, v3

    .line 212
    iget-object v3, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v3}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v15

    .line 214
    new-instance v14, Lcom/squareup/configure/item/WorkingItem;

    move-object v3, v14

    .line 215
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getBackingDetails()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    move-result-object v6

    .line 216
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getCategory()Lcom/squareup/api/items/MenuCategory;

    move-result-object v7

    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getTaxes()Ljava/util/List;

    move-result-object v8

    iget-object v9, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->orderEditor:Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;

    .line 217
    invoke-interface {v9}, Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v9

    invoke-virtual {v9}, Lcom/squareup/payment/Order;->getAvailableTaxRules()Ljava/util/List;

    move-result-object v9

    iget-object v10, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->orderEditor:Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;

    .line 218
    invoke-interface {v10}, Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v10

    invoke-virtual {v10}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v10

    iget-object v11, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->orderEditor:Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;

    .line 219
    invoke-interface {v11}, Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v11

    invoke-virtual {v11}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscountEvents()Ljava/util/Map;

    move-result-object v11

    .line 220
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object v12

    .line 221
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getModifierLists()Ljava/util/Map;

    move-result-object v13

    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getModifierOverrides()Ljava/util/Map;

    move-result-object v16

    move-object/from16 p2, v14

    move-object/from16 v14, v16

    const/16 v16, 0x0

    move-object v1, v15

    move-object/from16 v15, v16

    iget-object v15, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->res:Lcom/squareup/util/Res;

    move-object/from16 v16, v15

    .line 222
    invoke-virtual {v1, v15}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProto(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object v17

    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getMeasurementUnits()Ljava/util/Map;

    move-result-object v18

    .line 223
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getItemOptions()Ljava/util/List;

    move-result-object v19

    const/4 v15, 0x0

    invoke-direct/range {v3 .. v19}, Lcom/squareup/configure/item/WorkingItem;-><init>(Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;Lcom/squareup/api/items/MenuCategory;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/checkout/DiningOption;Lcom/squareup/util/Res;Lcom/squareup/protos/client/Employee;Ljava/util/Map;Ljava/util/List;)V

    .line 231
    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->isSkipModifierDetailScreenEnabled()Z

    move-result v1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    .line 232
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/configure/item/WorkingItem;->skipsConfigureItemDetailScreen()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 234
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result v1

    move-object/from16 v4, p2

    .line 235
    invoke-direct {v0, v1, v4}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->skipConfigureScreenForClickedItem(ZLcom/squareup/configure/item/WorkingItem;)V

    return-void

    :cond_1
    move-object/from16 v4, p2

    .line 244
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v5, 0x1

    if-ne v1, v5, :cond_3

    .line 245
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 246
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getModifierLists()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-gtz v1, :cond_2

    iget-object v1, v4, Lcom/squareup/configure/item/WorkingItem;->variations:Ljava/util/List;

    .line 247
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 250
    :cond_2
    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v4, v1}, Lcom/squareup/configure/item/WorkingItem;->setZeroCurrentAmount(Lcom/squareup/protos/common/CurrencyCode;)V

    .line 251
    invoke-virtual {v4}, Lcom/squareup/configure/item/WorkingItem;->selectOnlyVariation()V

    .line 252
    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->runner:Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;

    move-object/from16 v6, p1

    invoke-interface {v1, v4, v5, v6}, Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;->startEditingItemWithModifiers(Lcom/squareup/configure/item/WorkingItem;ZLcom/squareup/ui/main/RegisterTreeKey;)V

    return-void

    :cond_3
    move-object/from16 v6, p1

    .line 258
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v5, :cond_6

    .line 259
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getModifierLists()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-gtz v1, :cond_6

    iget-object v1, v4, Lcom/squareup/configure/item/WorkingItem;->variations:Ljava/util/List;

    .line 260
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_1

    .line 271
    :cond_4
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v5, :cond_5

    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object v1

    .line 272
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 273
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result v1

    if-nez v1, :cond_5

    .line 274
    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->orderEditor:Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;

    invoke-virtual {v4}, Lcom/squareup/configure/item/WorkingItem;->finishWithOnlyFixedPrice()Lcom/squareup/checkout/CartItem;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;->addItemToCart(Lcom/squareup/checkout/CartItem;)V

    .line 275
    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->runner:Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;

    invoke-interface {v1}, Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;->goBackFromItemSelect()V

    return-void

    .line 281
    :cond_5
    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->runner:Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;

    invoke-interface {v1, v4, v6}, Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;->startEditingItemVariablePrice(Lcom/squareup/configure/item/WorkingItem;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void

    .line 262
    :cond_6
    :goto_1
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result v1

    if-nez v1, :cond_7

    .line 263
    invoke-virtual {v4, v3}, Lcom/squareup/configure/item/WorkingItem;->selectVariation(I)V

    .line 265
    :cond_7
    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->runner:Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;

    invoke-interface {v1, v4, v3, v6}, Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;->startEditingItemWithModifiers(Lcom/squareup/configure/item/WorkingItem;ZLcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public synthetic lambda$getDiscount$8$ItemSelectCardPresenter(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 3

    .line 313
    invoke-interface {p2}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 314
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getPasscodeRequired()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->passcodeGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->USE_PASSCODE_RESTRICTED_DISCOUNT:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter$1;-><init>(Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/shared/catalog/models/CatalogDiscount;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_0

    .line 322
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->addDiscount(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/shared/catalog/models/CatalogDiscount;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$0$ItemSelectCardPresenter(Lcom/squareup/invoices/edit/items/ItemSelectCardView;Lcom/squareup/librarylist/LibraryListState;)V
    .locals 0

    .line 127
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->updateActionBar(Lcom/squareup/invoices/edit/items/ItemSelectCardView;Lcom/squareup/librarylist/LibraryListState;)V

    return-void
.end method

.method public synthetic lambda$null$3$ItemSelectCardPresenter(Lkotlin/Pair;)V
    .locals 0

    .line 137
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListResults;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListResults;->component1()Lcom/squareup/librarylist/LibraryListState;

    move-result-object p1

    .line 138
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState;->isTopLevel()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 139
    iget-object p1, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    goto :goto_0

    .line 141
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->libraryListManager:Lcom/squareup/librarylist/LibraryListManager;

    invoke-interface {p1}, Lcom/squareup/librarylist/LibraryListManager;->goBack()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onLoad$1$ItemSelectCardPresenter(Lcom/squareup/invoices/edit/items/ItemSelectCardView;)Lrx/Subscription;
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->libraryListManager:Lcom/squareup/librarylist/LibraryListManager;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListManager;->getResults()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/edit/items/-$$Lambda$6V81QNASOXAyiyYS8-wFGsjh5kE;->INSTANCE:Lcom/squareup/invoices/edit/items/-$$Lambda$6V81QNASOXAyiyYS8-wFGsjh5kE;

    .line 125
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$cAKxdUYUyZ3VycMGH9JtWf0QWsU;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$cAKxdUYUyZ3VycMGH9JtWf0QWsU;-><init>(Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;Lcom/squareup/invoices/edit/items/ItemSelectCardView;)V

    .line 127
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$2$ItemSelectCardPresenter()Lrx/Subscription;
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->entryHandler:Lcom/squareup/librarylist/SimpleEntryHandler;

    invoke-virtual {v0}, Lcom/squareup/librarylist/SimpleEntryHandler;->getLibraryEntryAction()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$pF54NdAbS5uWKjX4kCMFakca3H4;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$pF54NdAbS5uWKjX4kCMFakca3H4;-><init>(Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;)V

    .line 131
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$4$ItemSelectCardPresenter()Lrx/Subscription;
    .locals 3

    .line 134
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->onUpButtonClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v1, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->libraryListManager:Lcom/squareup/librarylist/LibraryListManager;

    .line 135
    invoke-interface {v1}, Lcom/squareup/librarylist/LibraryListManager;->getResults()Lrx/Observable;

    move-result-object v1

    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/jakewharton/rxrelay/PublishRelay;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$-bn_h-6GQ44SDF48MEcI0EQHkA0;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$-bn_h-6GQ44SDF48MEcI0EQHkA0;-><init>(Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;)V

    .line 136
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()Z
    .locals 2

    .line 184
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->onUpButtonClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 114
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 115
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/items/ItemSelectScreen;

    iput-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->screen:Lcom/squareup/invoices/edit/items/ItemSelectScreen;

    .line 116
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->libraryListManager:Lcom/squareup/librarylist/LibraryListManager;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 120
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 121
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/edit/items/ItemSelectCardView;

    .line 123
    new-instance v0, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$I7_bmuTa9nm1f64Ov3U8yZdcskM;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$I7_bmuTa9nm1f64Ov3U8yZdcskM;-><init>(Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;Lcom/squareup/invoices/edit/items/ItemSelectCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 129
    new-instance v0, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$JA9cPWouTrfKJGA2n_e-laOoyKs;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$JA9cPWouTrfKJGA2n_e-laOoyKs;-><init>(Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 133
    new-instance v0, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$eLs7bHFPv1ReBNRlOehpd8Qgkjk;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardPresenter$eLs7bHFPv1ReBNRlOehpd8Qgkjk;-><init>(Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public onTextSearched(Ljava/lang/String;)V
    .locals 2

    .line 352
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->libraryListAssistant:Lcom/squareup/librarylist/SimpleLibraryListAssistant;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/librarylist/SimpleLibraryListAssistant;->setHasSearchText(Z)V

    .line 353
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->libraryListManager:Lcom/squareup/librarylist/LibraryListManager;

    invoke-interface {v0, p1}, Lcom/squareup/librarylist/LibraryListManager;->search(Ljava/lang/String;)V

    return-void
.end method

.method updateActionBar(Lcom/squareup/invoices/edit/items/ItemSelectCardView;Lcom/squareup/librarylist/LibraryListState;)V
    .locals 0

    .line 149
    invoke-direct {p0, p2}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->getActionBarConfigForMode(Lcom/squareup/librarylist/LibraryListState;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p2

    .line 148
    invoke-virtual {p1, p2}, Lcom/squareup/invoices/edit/items/ItemSelectCardView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
