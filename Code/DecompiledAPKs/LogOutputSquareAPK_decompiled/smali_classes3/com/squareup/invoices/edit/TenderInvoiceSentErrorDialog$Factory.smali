.class public Lcom/squareup/invoices/edit/TenderInvoiceSentErrorDialog$Factory;
.super Ljava/lang/Object;
.source "TenderInvoiceSentErrorDialog.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/TenderInvoiceSentErrorDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 31
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 23
    const-class v0, Lcom/squareup/invoices/edit/EditInvoiceScope$Component;

    .line 24
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/EditInvoiceScope$Component;

    .line 25
    invoke-interface {v0}, Lcom/squareup/invoices/edit/EditInvoiceScope$Component;->editInvoiceScopeRunner()Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 28
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getLatestTenderErrorTitle()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 29
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getLatestTenderErrorDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->ok:I

    sget-object v1, Lcom/squareup/invoices/edit/-$$Lambda$TenderInvoiceSentErrorDialog$Factory$Zm0Psof6kEeey-8izs-jFJX7Daw;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$TenderInvoiceSentErrorDialog$Factory$Zm0Psof6kEeey-8izs-jFJX7Daw;

    .line 30
    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 31
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 32
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 33
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
