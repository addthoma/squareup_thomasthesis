.class public Lcom/squareup/invoices/edit/items/ItemSelectCardView;
.super Landroid/widget/LinearLayout;
.source "ItemSelectCardView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# static fields
.field private static final SEARCH_DELAY_MS:J = 0x64L


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field presenter:Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private searchBar:Lcom/squareup/ui/XableEditText;

.field private final searchRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const-class p2, Lcom/squareup/invoices/edit/items/ItemSelectScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/edit/items/ItemSelectScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/invoices/edit/items/ItemSelectScreen$Component;->inject(Lcom/squareup/invoices/edit/items/ItemSelectCardView;)V

    .line 31
    new-instance p1, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardView$tcXbUjYFcd-PWeNMyjzAvsLG61A;

    invoke-direct {p1, p0}, Lcom/squareup/invoices/edit/items/-$$Lambda$ItemSelectCardView$tcXbUjYFcd-PWeNMyjzAvsLG61A;-><init>(Lcom/squareup/invoices/edit/items/ItemSelectCardView;)V

    iput-object p1, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardView;->searchRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/invoices/edit/items/ItemSelectCardView;)Ljava/lang/Runnable;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardView;->searchRunnable:Ljava/lang/Runnable;

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$new$0$ItemSelectCardView()V
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardView;->presenter:Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;

    iget-object v1, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardView;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v1}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->onTextSearched(Ljava/lang/String;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 54
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 55
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardView;->presenter:Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardView;->presenter:Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardView;->presenter:Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;->dropView(Ljava/lang/Object;)V

    .line 60
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 35
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 36
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_item_search:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardView;->searchBar:Lcom/squareup/ui/XableEditText;

    .line 37
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 39
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/items/ItemSelectCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardView;->searchBar:Lcom/squareup/ui/XableEditText;

    sget v2, Lcom/squareup/configure/item/R$string;->search_library_hint_all_items:I

    .line 41
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 40
    invoke-virtual {v1, v0}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardView;->searchBar:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/invoices/edit/items/ItemSelectCardView$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/items/ItemSelectCardView$1;-><init>(Lcom/squareup/invoices/edit/items/ItemSelectCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/ItemSelectCardView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
