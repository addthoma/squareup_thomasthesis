.class public final Lcom/squareup/invoices/edit/EditInvoiceGateway;
.super Ljava/lang/Object;
.source "EditInvoiceGateway.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceGateway.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceGateway.kt\ncom/squareup/invoices/edit/EditInvoiceGateway\n*L\n1#1,149:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u001d\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J(\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0016H\u0002J\u000e\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0013\u001a\u00020\u0014J \u0010\u001a\u001a\u00020\u00192\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0017\u001a\u00020\u0016H\u0002J\u000e\u0010\u001b\u001a\u00020\u00192\u0006\u0010\u001c\u001a\u00020\u0016J\u000e\u0010\u001d\u001a\u00020\u00192\u0006\u0010\u001c\u001a\u00020\u0016J\u0006\u0010\u001e\u001a\u00020\u0019J\u0010\u0010\u001f\u001a\u00020\u00192\u0006\u0010 \u001a\u00020!H\u0016J\u0008\u0010\"\u001a\u00020\u0019H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0008\u001a\u00020\u00068BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u000b\u0010\u000c\u001a\u0004\u0008\t\u0010\nR\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/invoices/edit/EditInvoiceGateway;",
        "Lmortar/Scoped;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "lazyFlow",
        "Ldagger/Lazy;",
        "Lflow/Flow;",
        "(Lcom/squareup/settings/server/Features;Ldagger/Lazy;)V",
        "flow",
        "getFlow",
        "()Lflow/Flow;",
        "flow$delegate",
        "Ldagger/Lazy;",
        "serialDisposable",
        "Lio/reactivex/disposables/SerialDisposable;",
        "firstScreen",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "type",
        "Lcom/squareup/invoices/edit/EditInvoiceScope$Type;",
        "context",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
        "showV2",
        "",
        "needsInvoiceDefaults",
        "goToEditInvoiceInApplet",
        "",
        "goToFirstScreen",
        "goToNewInvoiceFromApplet",
        "isRecurring",
        "goToNewInvoiceFromPlus",
        "goToNewInvoiceInTenderFlow",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow$delegate:Ldagger/Lazy;

.field private final serialDisposable:Lio/reactivex/disposables/SerialDisposable;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/invoices/edit/EditInvoiceGateway;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "flow"

    const-string v4, "getFlow()Lflow/Flow;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/invoices/edit/EditInvoiceGateway;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Ldagger/Lazy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyFlow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway;->features:Lcom/squareup/settings/server/Features;

    .line 41
    iput-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway;->flow$delegate:Ldagger/Lazy;

    .line 42
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway;->serialDisposable:Lio/reactivex/disposables/SerialDisposable;

    return-void
.end method

.method public static final synthetic access$firstScreen(Lcom/squareup/invoices/edit/EditInvoiceGateway;Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;ZZ)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/invoices/edit/EditInvoiceGateway;->firstScreen(Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;ZZ)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getFeatures$p(Lcom/squareup/invoices/edit/EditInvoiceGateway;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/invoices/edit/EditInvoiceGateway;)Lflow/Flow;
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceGateway;->getFlow()Lflow/Flow;

    move-result-object p0

    return-object p0
.end method

.method private final firstScreen(Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;ZZ)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    if-eqz p3, :cond_0

    .line 136
    sget-object p3, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;->Companion:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Companion;

    invoke-virtual {p3, p1, p2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Companion;->fromType(Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    move-result-object p1

    .line 137
    new-instance p2, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen;

    invoke-direct {p2, p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V

    check-cast p2, Lcom/squareup/ui/main/RegisterTreeKey;

    goto :goto_0

    .line 139
    :cond_0
    new-instance p3, Lcom/squareup/invoices/edit/EditInvoiceScreen;

    new-instance v0, Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/edit/EditInvoiceScope;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V

    invoke-direct {p3, v0}, Lcom/squareup/invoices/edit/EditInvoiceScreen;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V

    move-object p2, p3

    check-cast p2, Lcom/squareup/ui/main/RegisterTreeKey;

    :goto_0
    if-eqz p4, :cond_1

    .line 143
    new-instance p1, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen;

    invoke-direct {p1, p2}, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    move-object p2, p1

    check-cast p2, Lcom/squareup/ui/main/RegisterTreeKey;

    :cond_1
    return-object p2
.end method

.method private final getFlow()Lflow/Flow;
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway;->flow$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/invoices/edit/EditInvoiceGateway;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    return-object v0
.end method

.method private final goToFirstScreen(Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)V
    .locals 3

    .line 99
    sget-object v0, Lcom/squareup/invoices/edit/EditInvoiceScope$Type;->EDIT_INVOICE_IN_TENDER:Lcom/squareup/invoices/edit/EditInvoiceScope$Type;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 101
    :goto_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->enabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Single;

    move-result-object v1

    .line 102
    new-instance v2, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$1;

    invoke-direct {v2, p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$1;-><init>(Lcom/squareup/invoices/edit/EditInvoiceGateway;Z)V

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 110
    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$2;-><init>(Lcom/squareup/invoices/edit/EditInvoiceGateway;Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 119
    iget-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway;->serialDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {p2, p1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method


# virtual methods
.method public final goToEditInvoiceInApplet(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    sget-object v0, Lcom/squareup/invoices/edit/EditInvoiceScope$Type;->EDIT_INVOICE_IN_APPLET:Lcom/squareup/invoices/edit/EditInvoiceScope$Type;

    const/4 v1, 0x0

    .line 86
    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/invoices/edit/EditInvoiceGateway;->goToFirstScreen(Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)V

    return-void
.end method

.method public final goToNewInvoiceFromApplet(Z)V
    .locals 3

    if-eqz p1, :cond_0

    .line 66
    sget-object p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->Factory:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;->newRecurringSeriesContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;

    move-result-object p1

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->Factory:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;->newSingleInvoiceContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;

    move-result-object p1

    :goto_0
    check-cast p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    .line 69
    sget-object v0, Lcom/squareup/invoices/edit/EditInvoiceScope$Type;->EDIT_INVOICE_IN_APPLET:Lcom/squareup/invoices/edit/EditInvoiceScope$Type;

    .line 71
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_INVOICE_DEFAULTS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    .line 68
    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/invoices/edit/EditInvoiceGateway;->goToFirstScreen(Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)V

    return-void
.end method

.method public final goToNewInvoiceFromPlus(Z)V
    .locals 3

    if-eqz p1, :cond_0

    .line 54
    sget-object p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->Factory:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;->newRecurringSeriesContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;

    move-result-object p1

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->Factory:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;->newSingleInvoiceContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;

    move-result-object p1

    :goto_0
    check-cast p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    .line 57
    sget-object v0, Lcom/squareup/invoices/edit/EditInvoiceScope$Type;->EDIT_INVOICE_FROM_PLUS:Lcom/squareup/invoices/edit/EditInvoiceScope$Type;

    .line 59
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_INVOICE_DEFAULTS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    .line 56
    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/invoices/edit/EditInvoiceGateway;->goToFirstScreen(Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)V

    return-void
.end method

.method public final goToNewInvoiceInTenderFlow()V
    .locals 4

    .line 78
    sget-object v0, Lcom/squareup/invoices/edit/EditInvoiceScope$Type;->EDIT_INVOICE_IN_TENDER:Lcom/squareup/invoices/edit/EditInvoiceScope$Type;

    .line 79
    sget-object v1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->Factory:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;->newSingleInvoiceContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    .line 80
    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->USE_INVOICE_DEFAULTS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    .line 77
    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/invoices/edit/EditInvoiceGateway;->goToFirstScreen(Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway;->serialDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    return-void
.end method
