.class Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "GenericSaveDraftResponse"
.end annotation


# instance fields
.field final displayDetails:Lcom/squareup/invoices/DisplayDetails;

.field final status:Lcom/squareup/protos/client/Status;


# direct methods
.method private constructor <init>(Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/DisplayDetails;)V
    .locals 0

    .line 2032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2033
    iput-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;->displayDetails:Lcom/squareup/invoices/DisplayDetails;

    .line 2034
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;->status:Lcom/squareup/protos/client/Status;

    return-void
.end method

.method static createGenericSaveDraftResponse(Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;)Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;
    .locals 2

    .line 2050
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    .line 2051
    iget-object v1, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    if-eqz v1, :cond_0

    .line 2052
    new-instance v1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/DisplayDetails$Invoice;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 2054
    :goto_0
    new-instance p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/DisplayDetails;)V

    return-object p0
.end method

.method static createGenericSaveDraftResponse(Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;)Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;
    .locals 2

    .line 2040
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;->status:Lcom/squareup/protos/client/Status;

    .line 2041
    iget-object v1, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;->recurring_invoice:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    if-eqz v1, :cond_0

    .line 2042
    new-instance v1, Lcom/squareup/invoices/DisplayDetails$Recurring;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;->recurring_invoice:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/DisplayDetails$Recurring;-><init>(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 2044
    :goto_0
    new-instance p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/DisplayDetails;)V

    return-object p0
.end method
