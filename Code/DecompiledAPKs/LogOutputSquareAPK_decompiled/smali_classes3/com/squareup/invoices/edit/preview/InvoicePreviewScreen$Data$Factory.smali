.class public final Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$Factory;
.super Ljava/lang/Object;
.source "InvoicePreviewScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J.\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u00082\u0006\u0010\u000c\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$Factory;",
        "",
        "invoiceUrlHelper",
        "Lcom/squareup/invoices/InvoiceUrlHelper;",
        "(Lcom/squareup/invoices/InvoiceUrlHelper;)V",
        "create",
        "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;",
        "serverToken",
        "",
        "previewType",
        "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$PreviewType;",
        "title",
        "sendButtonText",
        "isBusy",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/InvoiceUrlHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "invoiceUrlHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$Factory;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/String;Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$PreviewType;Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;
    .locals 2

    const-string v0, "serverToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previewType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sendButtonText"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$Factory;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    .line 68
    sget-object v1, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$Factory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$PreviewType;->ordinal()I

    move-result p2

    aget p2, v1, p2

    const/4 v1, 0x1

    if-eq p2, v1, :cond_2

    const/4 v1, 0x2

    if-eq p2, v1, :cond_1

    const/4 v1, 0x3

    if-ne p2, v1, :cond_0

    .line 71
    sget-object p2, Lcom/squareup/url/UrlType;->RECURRING_SERIES:Lcom/squareup/url/UrlType;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 70
    :cond_1
    sget-object p2, Lcom/squareup/url/UrlType;->ESTIMATE:Lcom/squareup/url/UrlType;

    goto :goto_0

    .line 69
    :cond_2
    sget-object p2, Lcom/squareup/url/UrlType;->SINGLE_INVOICE:Lcom/squareup/url/UrlType;

    .line 66
    :goto_0
    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoices/InvoiceUrlHelper;->invoiceUrl(Ljava/lang/String;Lcom/squareup/url/UrlType;)Ljava/lang/String;

    move-result-object p1

    .line 74
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/preview"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 76
    new-instance p2, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;

    invoke-direct {p2, p1, p3, p4, p5}, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object p2
.end method
