.class public final Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "InvoicePreviewCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoicePreviewCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoicePreviewCoordinator.kt\ncom/squareup/invoices/edit/preview/InvoicePreviewCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,121:1\n1103#2,7:122\n*E\n*S KotlinDebug\n*F\n+ 1 InvoicePreviewCoordinator.kt\ncom/squareup/invoices/edit/preview/InvoicePreviewCoordinator\n*L\n55#1,7:122\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u000cH\u0017J\u0010\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u000cH\u0002J\u0010\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0016\u0010\u001b\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u000c2\u0006\u0010\u001c\u001a\u00020\u001dJ\u0010\u0010\u001e\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020 H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;",
        "invoiceTutorialRunner",
        "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "progressBar",
        "Landroid/view/View;",
        "saveDraftButton",
        "shortAnimTimeMs",
        "",
        "webProgressBar",
        "Landroid/widget/ProgressBar;",
        "webView",
        "Landroid/webkit/WebView;",
        "attach",
        "",
        "view",
        "bindViews",
        "showProgress",
        "visible",
        "",
        "update",
        "data",
        "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;",
        "updateUrl",
        "newUrl",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

.field private progressBar:Landroid/view/View;

.field private final runner:Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;

.field private saveDraftButton:Landroid/view/View;

.field private final shortAnimTimeMs:I

.field private webProgressBar:Landroid/widget/ProgressBar;

.field private webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceTutorialRunner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->runner:Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;

    iput-object p2, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    const/high16 p1, 0x10e0000

    .line 36
    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->shortAnimTimeMs:I

    return-void
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;)Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->runner:Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$getWebProgressBar$p(Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;)Landroid/widget/ProgressBar;
    .locals 1

    .line 23
    iget-object p0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->webProgressBar:Landroid/widget/ProgressBar;

    if-nez p0, :cond_0

    const-string/jumbo v0, "webProgressBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setWebProgressBar$p(Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;Landroid/widget/ProgressBar;)V
    .locals 0

    .line 23
    iput-object p1, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->webProgressBar:Landroid/widget/ProgressBar;

    return-void
.end method

.method public static final synthetic access$updateUrl(Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;Ljava/lang/String;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->updateUrl(Ljava/lang/String;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 113
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 115
    sget v0, Lcom/squareup/features/invoices/R$id;->preview_webview:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->webView:Landroid/webkit/WebView;

    .line 116
    sget v0, Lcom/squareup/features/invoices/R$id;->preview_web_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->webProgressBar:Landroid/widget/ProgressBar;

    .line 117
    sget v0, Lcom/squareup/features/invoices/R$id;->preview_invoice_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->progressBar:Landroid/view/View;

    .line 118
    sget v0, Lcom/squareup/features/invoices/R$id;->preview_save_draft:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->saveDraftButton:Landroid/view/View;

    return-void
.end method

.method private final showProgress(Z)V
    .locals 1

    const-string v0, "progressBar"

    if-eqz p1, :cond_1

    .line 106
    iget-object p1, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->progressBar:Landroid/view/View;

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 108
    :cond_1
    iget-object p1, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->progressBar:Landroid/view/View;

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    :goto_0
    return-void
.end method

.method private final updateUrl(Ljava/lang/String;)V
    .locals 3

    .line 90
    iget-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->webView:Landroid/webkit/WebView;

    const-string/jumbo v1, "webView"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$updateUrl$1;

    invoke-direct {v2, p1}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$updateUrl$1;-><init>(Ljava/lang/String;)V

    check-cast v2, Landroid/webkit/WebViewClient;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 101
    iget-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->webView:Landroid/webkit/WebView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->bindViews(Landroid/view/View;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->webView:Landroid/webkit/WebView;

    const-string/jumbo v1, "webView"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const-string/jumbo v2, "webView.settings"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 44
    iget-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->webView:Landroid/webkit/WebView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$1;-><init>(Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;)V

    check-cast v1, Landroid/webkit/WebChromeClient;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->saveDraftButton:Landroid/view/View;

    if-nez v0, :cond_2

    const-string v1, "saveDraftButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 122
    :cond_2
    new-instance v1, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->runner:Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;->invoicePreviewScreenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 58
    new-instance v1, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$3;-><init>(Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "runner.invoicePreviewScr\u2026ribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->runner:Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;->invoicePreviewScreenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 62
    sget-object v1, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$4;->INSTANCE:Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$4;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 64
    new-instance v1, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$5;

    move-object v2, p0

    check-cast v2, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$5;-><init>(Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "runner.invoicePreviewScr\u2026  .subscribe(::updateUrl)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method

.method public final update(Landroid/view/View;Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 72
    :cond_0
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 73
    invoke-virtual {p2}, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;->getTitle()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 74
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 75
    new-instance v3, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$update$1;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$update$1;-><init>(Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 76
    invoke-virtual {p2}, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;->getSendButtonText()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 77
    new-instance v3, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$update$2;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$update$2;-><init>(Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 78
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 80
    invoke-virtual {p2}, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;->isBusy()Z

    move-result v0

    xor-int/2addr v0, v2

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 81
    invoke-virtual {p2}, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;->isBusy()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->showProgress(Z)V

    .line 83
    iget-object p1, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    invoke-virtual {p2}, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;->getSendButtonText()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->updateInvoicePreviewBanner(Ljava/lang/String;)V

    return-void
.end method
