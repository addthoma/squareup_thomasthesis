.class public final synthetic Lcom/squareup/invoices/edit/-$$Lambda$ofUFZ-ETHMpL_HtCgXYT72-Gu5I;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Func1;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$ofUFZ-ETHMpL_HtCgXYT72-Gu5I;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/edit/-$$Lambda$ofUFZ-ETHMpL_HtCgXYT72-Gu5I;

    invoke-direct {v0}, Lcom/squareup/invoices/edit/-$$Lambda$ofUFZ-ETHMpL_HtCgXYT72-Gu5I;-><init>()V

    sput-object v0, Lcom/squareup/invoices/edit/-$$Lambda$ofUFZ-ETHMpL_HtCgXYT72-Gu5I;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$ofUFZ-ETHMpL_HtCgXYT72-Gu5I;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;

    invoke-static {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;->createGenericSaveDraftResponse(Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;)Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;

    move-result-object p1

    return-object p1
.end method
