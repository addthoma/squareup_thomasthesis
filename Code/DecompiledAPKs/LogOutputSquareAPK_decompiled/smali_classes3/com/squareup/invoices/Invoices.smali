.class public final Lcom/squareup/invoices/Invoices;
.super Ljava/lang/Object;
.source "Invoices.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoices.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Invoices.kt\ncom/squareup/invoices/Invoices\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 ProtosPure.kt\ncom/squareup/util/ProtosPure\n*L\n1#1,413:1\n704#2:414\n777#2,2:415\n1360#2:417\n1429#2,2:418\n1431#2:423\n747#2:427\n769#2,2:428\n1360#2:430\n1429#2,3:431\n1866#2,7:434\n132#3,3:420\n132#3,3:424\n*E\n*S KotlinDebug\n*F\n+ 1 Invoices.kt\ncom/squareup/invoices/Invoices\n*L\n189#1:414\n189#1,2:415\n205#1:417\n205#1,2:418\n205#1:423\n248#1:427\n248#1,2:428\n362#1:430\n362#1,3:431\n363#1,7:434\n205#1,3:420\n218#1,3:424\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000z\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\u001a \u0010\u0000\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0001\u001a \u0010\u0000\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0008\u001a \u0010\u0000\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\t2\u0006\u0010\u0006\u001a\u00020\u0001\u001a \u0010\u0000\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\t2\u0006\u0010\u0007\u001a\u00020\u0008\u001a\"\u0010\u0000\u001a\u00020\u00012\u0008\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0006\u001a\u00020\u0001\u001a\"\u0010\u0000\u001a\u00020\u00012\u0008\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0007\u001a\u00020\u0008\u001a\u000c\u0010\r\u001a\u0004\u0018\u00010\u000e*\u00020\u0005\u001a\u000c\u0010\r\u001a\u0004\u0018\u00010\u000e*\u00020\t\u001a\u0016\u0010\r\u001a\u0004\u0018\u00010\u000e*\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000fH\u0002\u001a\n\u0010\u0011\u001a\u00020\u0012*\u00020\u0005\u001a\n\u0010\u0011\u001a\u00020\u0012*\u00020\t\u001a\u0012\u0010\u0013\u001a\u00020\u0001*\u00020\u00052\u0006\u0010\u0014\u001a\u00020\u0001\u001a\u0012\u0010\u0013\u001a\u00020\u0001*\u00020\t2\u0006\u0010\u0014\u001a\u00020\u0001\u001a\u0012\u0010\u0013\u001a\u00020\u0001*\u00020\u000b2\u0006\u0010\u0007\u001a\u00020\u0008\u001a\n\u0010\u0015\u001a\u00020\u0016*\u00020\u0005\u001a\n\u0010\u0015\u001a\u00020\u0016*\u00020\t\u001a\u0012\u0010\u0017\u001a\u00020\u0018*\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u001a\u001a\n\u0010\u001b\u001a\u00020\u0018*\u00020\u0005\u001a\n\u0010\u001b\u001a\u00020\u0018*\u00020\t\u001a\u000e\u0010\u001c\u001a\u0004\u0018\u00010\u001d*\u0004\u0018\u00010\u001d\u001a\u001e\u0010\u001e\u001a\u00020\t*\u00020\t2\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00180 \u001a\u0012\u0010!\u001a\u00020\u0018*\u00020\t2\u0006\u0010\"\u001a\u00020#\u001a\u0012\u0010$\u001a\u00020\t*\u00020\t2\u0006\u0010%\u001a\u00020\u0018\u001a\u000e\u0010&\u001a\u0004\u0018\u00010\u001d*\u0004\u0018\u00010#\u001a\n\u0010\'\u001a\u00020\u0016*\u00020\u000b\u001a\n\u0010(\u001a\u00020\u0016*\u00020\u000b\u001a#\u0010)\u001a\u00020\t*\u00020\t2\u0017\u0010*\u001a\u0013\u0012\u0004\u0012\u00020+\u0012\u0004\u0012\u00020,0 \u00a2\u0006\u0002\u0008-\u001a\u001a\u0010.\u001a\u00020\t*\u00020\t2\u0006\u0010\u0014\u001a\u00020\u00012\u0006\u0010/\u001a\u00020\u0001\u001a\u0014\u00100\u001a\u00020\t*\u00020\t2\u0008\u00101\u001a\u0004\u0018\u00010\u000e\u001a#\u00102\u001a\u00020\t*\u00020\t2\u0017\u0010*\u001a\u0013\u0012\u0004\u0012\u00020+\u0012\u0004\u0012\u00020,0 \u00a2\u0006\u0002\u0008-\u001a\u0012\u00103\u001a\u00020\t*\u00020\t2\u0006\u00104\u001a\u00020\u0012\u001a#\u00105\u001a\u00020\t*\u00020\t2\u0017\u0010*\u001a\u0013\u0012\u0004\u0012\u00020+\u0012\u0004\u0012\u00020,0 \u00a2\u0006\u0002\u0008-\u00a8\u00066"
    }
    d2 = {
        "getFirstSentDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "detailsWrapper",
        "Lcom/squareup/invoices/DisplayDetails;",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "today",
        "clock",
        "Lcom/squareup/util/Clock;",
        "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
        "invoiceDisplayDetails",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
        "scheduledAt",
        "getInstrumentToken",
        "",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "getPaymentMethod",
        "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
        "getRemainderDueOn",
        "firstSentDate",
        "getTotalWithoutTip",
        "Lcom/squareup/protos/common/Money;",
        "isPartiallyPaid",
        "",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "isTippingEnabled",
        "nullIfAllFieldsNull",
        "Lcom/squareup/protos/client/invoice/InvoiceContact;",
        "removePaymentRequests",
        "predicate",
        "Lkotlin/Function1;",
        "setPayerFromContact",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "setTippingEnabled",
        "allowTips",
        "toInvoiceContact",
        "totalPaid",
        "totalRemaining",
        "updateAllPaymentRequests",
        "block",
        "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "updateDueDate",
        "newDueDate",
        "updateInstrumentToken",
        "instrumentToken",
        "updateLastPaymentRequest",
        "updatePaymentMethod",
        "paymentMethod",
        "updateRemainderPaymentRequest",
        "invoices_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getFirstSentDate(Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 2

    const-string v0, "invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "today"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 285
    instance-of v0, p0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object p0, v1

    :cond_0
    check-cast p0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v1

    :cond_1
    iget-object p0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 284
    invoke-static {v1, p0, p2}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method

.method public static final getFirstSentDate(Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 2

    const-string v0, "invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 267
    instance-of v0, p0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object p0, v1

    :cond_0
    check-cast p0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v1

    :cond_1
    iget-object p0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {p2}, Lcom/squareup/invoices/InvoiceDateUtility;->getToday(Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    const-string p2, "getToday(clock)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    invoke-static {v1, p0, p1}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method

.method public static final getFirstSentDate(Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 2

    const-string v0, "invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "today"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 276
    instance-of v0, p0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object p0, v1

    :cond_0
    check-cast p0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v1

    :cond_1
    iget-object p0, p1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 275
    invoke-static {v1, p0, p2}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method

.method public static final getFirstSentDate(Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 2

    const-string v0, "invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 304
    instance-of v0, p0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object p0, v1

    :cond_0
    check-cast p0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v1

    :cond_1
    iget-object p0, p1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {p2}, Lcom/squareup/invoices/InvoiceDateUtility;->getToday(Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    const-string p2, "getToday(clock)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 303
    invoke-static {v1, p0, p1}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method

.method public static final getFirstSentDate(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    const-string/jumbo v0, "today"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_0

    .line 329
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    move-object p0, p1

    :goto_0
    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    move-object p0, p2

    :goto_1
    return-object p0
.end method

.method public static final getFirstSentDate(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    const-string v0, "clock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 322
    invoke-static {p2}, Lcom/squareup/invoices/InvoiceDateUtility;->getToday(Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p2

    const-string v0, "getToday(clock)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1, p2}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method

.method public static final getInstrumentToken(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getInstrumentToken"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    invoke-static {p0}, Lcom/squareup/invoices/Invoices;->getInstrumentToken(Ljava/util/List;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final getInstrumentToken(Lcom/squareup/protos/client/invoice/Invoice;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getInstrumentToken"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {p0}, Lcom/squareup/invoices/Invoices;->getInstrumentToken(Ljava/util/List;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final getInstrumentToken(Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 393
    invoke-static {p0}, Lcom/squareup/invoices/PaymentRequestsConfigKt;->isFull(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->instrument_token:Ljava/lang/String;

    move-object v0, p0

    :cond_0
    return-object v0
.end method

.method public static final getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;
    .locals 1

    const-string v0, "$this$getPaymentMethod"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    const-string v0, "payment_request"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    const-string v0, "payment_request.last().payment_method"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;
    .locals 1

    const-string v0, "$this$getPaymentMethod"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    const-string v0, "payment_request"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    const-string v0, "payment_request.last().payment_method"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getRemainderDueOn(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    const-string v0, "$this$getRemainderDueOn"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firstSentDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    invoke-static {p0}, Lcom/squareup/invoices/PaymentRequestsKt;->getRemainderPaymentRequest(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p0

    .line 42
    invoke-static {p0, p1}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method

.method public static final getRemainderDueOn(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    const-string v0, "$this$getRemainderDueOn"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firstSentDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {p0}, Lcom/squareup/invoices/PaymentRequestsKt;->getRemainderPaymentRequest(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p0

    .line 92
    invoke-static {p0, p1}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method

.method public static final getRemainderDueOn(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 2

    const-string v0, "$this$getRemainderDueOn"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsKt;->getRemainderPaymentRequest(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {p0, v1, p1}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method

.method public static final getTotalWithoutTip(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/common/Money;
    .locals 1

    const-string v0, "$this$getTotalWithoutTip"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 241
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p0}, Lcom/squareup/money/MoneyMath;->subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    const-string v0, "subtractNullSafe(cart.am\u2026, cart.amounts.tip_money)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getTotalWithoutTip(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/common/Money;
    .locals 1

    const-string v0, "$this$getTotalWithoutTip"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p0}, Lcom/squareup/money/MoneyMath;->subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    const-string v0, "subtractNullSafe(cart.am\u2026, cart.amounts.tip_money)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final isPartiallyPaid(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/CurrencyCode;)Z
    .locals 3

    const-string v0, "$this$isPartiallyPaid"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 335
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payments:Ljava/util/List;

    const-string v1, "payments"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refund:Ljava/util/List;

    const-string v2, "refund"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    const-string v2, "invoice.cart.amounts.total_money"

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, p0, p1}, Lcom/squareup/invoices/PartialTransactionsKt;->isPartiallyPaid(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/CurrencyCode;)Z

    move-result p0

    return p0
.end method

.method public static final isTippingEnabled(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Z
    .locals 1

    const-string v0, "$this$isTippingEnabled"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    const-string v0, "payment_request"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->tipping_enabled:Ljava/lang/Boolean;

    const-string v0, "payment_request.last().tipping_enabled"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method public static final isTippingEnabled(Lcom/squareup/protos/client/invoice/Invoice;)Z
    .locals 1

    const-string v0, "$this$isTippingEnabled"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    const-string v0, "payment_request"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->tipping_enabled:Ljava/lang/Boolean;

    const-string v0, "payment_request.last().tipping_enabled"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method public static final nullIfAllFieldsNull(Lcom/squareup/protos/client/invoice/InvoiceContact;)Lcom/squareup/protos/client/invoice/InvoiceContact;
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_1

    .line 405
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_billing_address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_company_name:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_phone_number:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceContact;->contact_token:Ljava/lang/String;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, p0

    :cond_1
    :goto_0
    return-object v0
.end method

.method public static final removePaymentRequests(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/Invoice$Builder;"
        }
    .end annotation

    const-string v0, "$this$removePaymentRequests"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "predicate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    const-string v1, "payment_request"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 427
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 428
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 429
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 248
    invoke-virtual {p0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request(Ljava/util/List;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p0

    const-string p1, "payment_request(payment_\u2026est.filterNot(predicate))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final setPayerFromContact(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/client/rolodex/Contact;)Z
    .locals 4

    const-string v0, "$this$setPayerFromContact"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contact"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 347
    invoke-static {p1}, Lcom/squareup/crm/util/RolodexContactHelper;->getDisplayNameOrNull(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v0

    .line 348
    invoke-static {p1}, Lcom/squareup/crm/util/RolodexContactHelper;->getEmail(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v1

    .line 349
    check-cast v0, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_4

    check-cast v1, Ljava/lang/CharSequence;

    if-eqz v1, :cond_3

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_4

    return v2

    .line 352
    :cond_4
    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->toInvoiceContact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/client/invoice/InvoiceContact;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer(Lcom/squareup/protos/client/invoice/InvoiceContact;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    return v3
.end method

.method public static final setTippingEnabled(Lcom/squareup/protos/client/invoice/Invoice$Builder;Z)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 1

    const-string v0, "$this$setTippingEnabled"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    new-instance v0, Lcom/squareup/invoices/Invoices$setTippingEnabled$1;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/Invoices$setTippingEnabled$1;-><init>(Z)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v0}, Lcom/squareup/invoices/Invoices;->updateLastPaymentRequest(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static final toInvoiceContact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/client/invoice/InvoiceContact;
    .locals 4

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 377
    :cond_0
    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->getDisplayNameOrNull(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v0

    .line 378
    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->getEmail(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v1

    .line 380
    new-instance v2, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;-><init>()V

    .line 381
    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v3, v3, Lcom/squareup/protos/client/rolodex/CustomerProfile;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_billing_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;

    move-result-object v2

    .line 382
    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v3, v3, Lcom/squareup/protos/client/rolodex/CustomerProfile;->company_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_company_name(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;

    move-result-object v2

    .line 383
    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_email(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;

    move-result-object v1

    .line 384
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_name(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;

    move-result-object v0

    .line 385
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;

    move-result-object v0

    .line 386
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;

    move-result-object p0

    .line 387
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceContact;

    move-result-object p0

    return-object p0
.end method

.method public static final totalPaid(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/protos/common/Money;
    .locals 3

    const-string v0, "$this$totalPaid"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 357
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 358
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const-wide/16 v1, 0x0

    const-string p0, "currencyCode"

    .line 359
    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    goto :goto_2

    .line 361
    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payments:Ljava/util/List;

    const-string v0, "payments"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Iterable;

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 430
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 431
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 432
    check-cast v1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;

    .line 362
    iget-object v1, v1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->total_amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 433
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 434
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    .line 435
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 436
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 437
    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 438
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Money;

    check-cast v0, Lcom/squareup/protos/common/Money;

    const-string v2, "acc"

    .line 363
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "money"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMathOperatorsKt;->plus(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_1

    :cond_2
    const-string p0, "payments.toList()\n      \u2026c, money -> acc + money }"

    .line 440
    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/protos/common/Money;

    :goto_2
    return-object p0

    .line 435
    :cond_3
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Empty collection can\'t be reduced."

    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final totalRemaining(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/protos/common/Money;
    .locals 2

    const-string v0, "$this$totalRemaining"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 368
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    const-string v1, "invoice"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/invoices/Invoices;->getTotalWithoutTip(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {p0}, Lcom/squareup/invoices/Invoices;->totalPaid(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/squareup/money/MoneyMathOperatorsKt;->minus(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static final updateAllPaymentRequests(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/Invoice$Builder;"
        }
    .end annotation

    const-string v0, "$this$updateAllPaymentRequests"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    const-string v1, "payment_request"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 417
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 418
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 419
    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 206
    check-cast v2, Lcom/squareup/wire/Message;

    .line 421
    invoke-virtual {v2}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {p1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 422
    invoke-virtual {v2}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 206
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 421
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type B"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 423
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 205
    invoke-virtual {p0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request(Ljava/util/List;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    return-object p0
.end method

.method public static final updateDueDate(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 1

    const-string v0, "$this$updateDueDate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firstSentDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newDueDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    new-instance v0, Lcom/squareup/invoices/Invoices$updateDueDate$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/Invoices$updateDueDate$1;-><init>(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v0}, Lcom/squareup/invoices/Invoices;->updateRemainderPaymentRequest(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static final updateInstrumentToken(Lcom/squareup/protos/client/invoice/Invoice$Builder;Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 1

    const-string v0, "$this$updateInstrumentToken"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    new-instance v0, Lcom/squareup/invoices/Invoices$updateInstrumentToken$1;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/Invoices$updateInstrumentToken$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v0}, Lcom/squareup/invoices/Invoices;->updateAllPaymentRequests(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static final updateLastPaymentRequest(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/Invoice$Builder;"
        }
    .end annotation

    const-string v0, "$this$updateLastPaymentRequest"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    const-string v1, "payment_request"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 219
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 220
    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 221
    check-cast v1, Lcom/squareup/wire/Message;

    .line 425
    invoke-virtual {v1}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    invoke-virtual {v1}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p1

    .line 221
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    iput-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    return-object p0

    .line 425
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type B"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final updatePaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 2

    const-string v0, "$this$updatePaymentMethod"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentMethod"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne p1, v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    const-string v1, "payment_request"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestListMutatorsKt;->removeDeposit(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 140
    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestListMutatorsKt;->removeInstallments(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 142
    invoke-static {p0, v0}, Lcom/squareup/invoices/Invoices;->updateInstrumentToken(Lcom/squareup/protos/client/invoice/Invoice$Builder;Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 145
    :goto_0
    new-instance v0, Lcom/squareup/invoices/Invoices$updatePaymentMethod$1;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/Invoices$updatePaymentMethod$1;-><init>(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v0}, Lcom/squareup/invoices/Invoices;->updateAllPaymentRequests(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static final updateRemainderPaymentRequest(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/Invoice$Builder;"
        }
    .end annotation

    const-string v0, "$this$updateRemainderPaymentRequest"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsKt;->getRemainderPaymentRequest(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    .line 184
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/PaymentRequest;->newBuilder()Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    const-string v1, "requestToUpdate"

    .line 186
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    iget-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    const-string v1, "payment_request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 414
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 415
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 189
    iget-object v3, v3, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v4, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eq v3, v4, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 416
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 190
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    .line 191
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->build()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request(Ljava/util/List;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    return-object p0
.end method
