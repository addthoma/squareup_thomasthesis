.class public interface abstract Lcom/squareup/encryption/CryptoPrimitive;
.super Ljava/lang/Object;
.source "CryptoPrimitive.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract compute([B)Lcom/squareup/encryption/CryptoResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Lcom/squareup/encryption/CryptoResult<",
            "TK;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation
.end method

.method public abstract getKey()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation
.end method

.method public abstract isExpired()Z
.end method
