.class public interface abstract Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;
.super Ljava/lang/Object;
.source "LoyaltySellerCartBannerFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003H\u0016J\u0008\u0010\u0007\u001a\u00020\u0006H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;",
        "",
        "banner",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBanner;",
        "isCheckInFlowActive",
        "",
        "isCheckInFlowActiveSynchronous",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract banner()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBanner;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isCheckInFlowActive()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isCheckInFlowActiveSynchronous()Z
.end method
