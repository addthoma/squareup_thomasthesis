.class public final Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter;
.super Ljava/lang/Object;
.source "LoyaltyBuyerBlockingCartDiffBannerFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoyaltyBuyerBlockingCartDiffBannerFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoyaltyBuyerBlockingCartDiffBannerFormatter.kt\ncom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,64:1\n1412#2,9:65\n1642#2,2:74\n1421#2:76\n*E\n*S KotlinDebug\n*F\n+ 1 LoyaltyBuyerBlockingCartDiffBannerFormatter.kt\ncom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter\n*L\n44#1,9:65\n44#1,2:74\n44#1:76\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u0010B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cJ\u000c\u0010\u000e\u001a\u00020\r*\u00020\u000fH\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "loyaltyFrontOfTransactionSetting",
        "Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;",
        "blockingCartDiff",
        "Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents;",
        "buyerCartFormatter",
        "Lcom/squareup/ui/cart/BuyerCartFormatter;",
        "(Lcom/squareup/util/Res;Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents;Lcom/squareup/ui/cart/BuyerCartFormatter;)V",
        "banner",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner;",
        "toBanner",
        "Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents$CartDiffState;",
        "BuyerBlockingCartDiffBanner",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final blockingCartDiff:Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents;

.field private final buyerCartFormatter:Lcom/squareup/ui/cart/BuyerCartFormatter;

.field private final loyaltyFrontOfTransactionSetting:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents;Lcom/squareup/ui/cart/BuyerCartFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyFrontOfTransactionSetting"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "blockingCartDiff"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerCartFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter;->loyaltyFrontOfTransactionSetting:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;

    iput-object p3, p0, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter;->blockingCartDiff:Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents;

    iput-object p4, p0, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter;->buyerCartFormatter:Lcom/squareup/ui/cart/BuyerCartFormatter;

    return-void
.end method

.method public static final synthetic access$toBanner(Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter;Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents$CartDiffState;)Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner;
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter;->toBanner(Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents$CartDiffState;)Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner;

    move-result-object p0

    return-object p0
.end method

.method private final toBanner(Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents$CartDiffState;)Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner;
    .locals 4

    .line 38
    sget-object v0, Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents$CartDiffState$NoCartDiff;->INSTANCE:Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents$CartDiffState$NoCartDiff;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner$NoBanner;->INSTANCE:Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner$NoBanner;

    check-cast p1, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner;

    goto :goto_2

    .line 40
    :cond_0
    instance-of v0, p1, Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents$CartDiffState$CartDiff;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/squareup/comms/protos/seller/BlockingCartDiff$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/seller/BlockingCartDiff$Builder;-><init>()V

    .line 41
    iget-object v1, p0, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/loyaltycheckin/R$string;->loyalty_cart_diff_banner:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/BlockingCartDiff$Builder;->text(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/BlockingCartDiff$Builder;

    move-result-object v0

    .line 44
    check-cast p1, Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents$CartDiffState$CartDiff;

    invoke-virtual {p1}, Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents$CartDiffState$CartDiff;->getChangedEvents()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 65
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 74
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 73
    check-cast v2, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    .line 45
    iget-object v3, p0, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter;->buyerCartFormatter:Lcom/squareup/ui/cart/BuyerCartFormatter;

    invoke-virtual {v3, v2}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getCartChangedBanner(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)Lcom/squareup/comms/protos/seller/DisplayBanner;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, v2, Lcom/squareup/comms/protos/seller/DisplayBanner;->item_client_id:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_1

    .line 73
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 76
    :cond_3
    check-cast v1, Ljava/util/List;

    .line 42
    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/BlockingCartDiff$Builder;->item_client_ids(Ljava/util/List;)Lcom/squareup/comms/protos/seller/BlockingCartDiff$Builder;

    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lcom/squareup/comms/protos/seller/BlockingCartDiff$Builder;->build()Lcom/squareup/comms/protos/seller/BlockingCartDiff;

    move-result-object p1

    .line 40
    new-instance v0, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner$Banner;

    invoke-direct {v0, p1}, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner$Banner;-><init>(Lcom/squareup/comms/protos/seller/BlockingCartDiff;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner;

    :goto_2
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public final banner()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter;->loyaltyFrontOfTransactionSetting:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;

    invoke-interface {v0}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;->getValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter;->blockingCartDiff:Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents;

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents;->cartDiffs()Lio/reactivex/Observable;

    move-result-object v0

    .line 28
    sget-object v1, Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents$CartDiffState$NoCartDiff;->INSTANCE:Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents$CartDiffState$NoCartDiff;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 29
    new-instance v1, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$banner$1;

    invoke-direct {v1, p0}, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$banner$1;-><init>(Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "blockingCartDiff.cartDif\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 32
    :cond_0
    sget-object v0, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner$NoBanner;->INSTANCE:Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner$NoBanner;

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.just(NoBanner)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method
