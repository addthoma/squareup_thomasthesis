.class public final Lcom/squareup/loyaltycheckin/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyaltycheckin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final loyalty_cart_diff_banner:I = 0x7f120f0d

.field public static final loyalty_check_in_balance_text:I = 0x7f120f12

.field public static final loyalty_check_in_error_check_in:I = 0x7f120f13

.field public static final loyalty_check_in_error_redeeming:I = 0x7f120f14

.field public static final loyalty_check_in_redeem_card_points_text:I = 0x7f120f15

.field public static final loyalty_check_in_redeemed_reward:I = 0x7f120f16

.field public static final loyalty_check_in_welcome_back_rewards_subtitle:I = 0x7f120f17

.field public static final x2_buyer_loyalty_check_in_tier_reward_redeemed:I = 0x7f121bea

.field public static final x2_buyer_loyalty_check_in_tier_reward_redeemed_multiple:I = 0x7f121beb


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
