.class public final Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule;
.super Ljava/lang/Object;
.source "OrderHubDeviceSettingsModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007H\u0007J\u0018\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007H\u0007J\u0018\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u00042\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007H\u0007J\u0018\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007H\u0007\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule;",
        "",
        "()V",
        "privateOrderHubQuickActionsEnabledPreference",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "prefs",
        "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
        "provideOrderHubAlertsEnabledPreference",
        "provideOrderHubAlertsFrequencyPreference",
        "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
        "provideOrderHubPrintingEnabledPreference",
        "settings_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule;

    invoke-direct {v0}, Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule;-><init>()V

    sput-object v0, Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule;->INSTANCE:Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final privateOrderHubQuickActionsEnabledPreference(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .param p0    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/orderhub/settings/OrderHubQuickActionsEnabledPreference;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "prefs"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 49
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "orderhub-quick-actions-enabled"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    const-string v0, "prefs.getBoolean(\"orderh\u2026-actions-enabled\", false)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final provideOrderHubAlertsEnabledPreference(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .param p0    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/orderhub/settings/OrderHubAlertsEnabledPreference;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "prefs"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 17
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "orderhub-notifications-enabled"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    const-string v0, "prefs.getBoolean(\"orderh\u2026ications-enabled\", false)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final provideOrderHubAlertsFrequencyPreference(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 3
    .param p0    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/orderhub/settings/OrderHubAlertsFrequencyPreference;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "prefs"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    sget-object v0, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->Companion:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Companion;

    invoke-virtual {v0}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Companion;->getDEFAULT()Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    move-result-object v0

    .line 30
    sget-object v1, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Converter;->INSTANCE:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Converter;

    check-cast v1, Lcom/f2prateek/rx/preferences2/Preference$Converter;

    const-string v2, "orderhub-notifications-frequency"

    .line 27
    invoke-virtual {p0, v2, v0, v1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getObject(Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Converter;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    const-string v0, "prefs.getObject(\n       \u2026Frequency.Converter\n    )"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final provideOrderHubPrintingEnabledPreference(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .param p0    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/orderhub/settings/OrderHubPrintingEnabledPreference;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "prefs"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 40
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "orderhub-printing-enabled"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    const-string v0, "prefs.getBoolean(\"orderh\u2026printing-enabled\", false)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
