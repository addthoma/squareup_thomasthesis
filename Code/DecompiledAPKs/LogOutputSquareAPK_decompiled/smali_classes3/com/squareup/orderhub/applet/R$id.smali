.class public final Lcom/squareup/orderhub/applet/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderhub/applet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final orderhub_add_note_animator:I = 0x7f0a0af6

.field public static final orderhub_add_note_card:I = 0x7f0a0af7

.field public static final orderhub_add_note_spinner:I = 0x7f0a0af8

.field public static final orderhub_add_tracking_animator:I = 0x7f0a0af9

.field public static final orderhub_add_tracking_carrier_recycler_view:I = 0x7f0a0afa

.field public static final orderhub_add_tracking_delete_button:I = 0x7f0a0afb

.field public static final orderhub_add_tracking_number:I = 0x7f0a0afc

.field public static final orderhub_add_tracking_other_carrier:I = 0x7f0a0afd

.field public static final orderhub_add_tracking_scrollview:I = 0x7f0a0afe

.field public static final orderhub_bill_history_progress_bar:I = 0x7f0a0b06

.field public static final orderhub_bill_history_view:I = 0x7f0a0b07

.field public static final orderhub_delete_note_button:I = 0x7f0a0b08

.field public static final orderhub_detail_container:I = 0x7f0a0b09

.field public static final orderhub_detail_customer_name:I = 0x7f0a0b0a

.field public static final orderhub_detail_filter_row:I = 0x7f0a0b0b

.field public static final orderhub_detail_filters_container:I = 0x7f0a0b0c

.field public static final orderhub_detail_filters_dropdown_arrow:I = 0x7f0a0b0d

.field public static final orderhub_detail_filters_dropdown_container:I = 0x7f0a0b0e

.field public static final orderhub_detail_filters_recycler_view:I = 0x7f0a0b0f

.field public static final orderhub_detail_filters_title:I = 0x7f0a0b10

.field public static final orderhub_detail_header_row:I = 0x7f0a0b11

.field public static final orderhub_detail_icon:I = 0x7f0a0b12

.field public static final orderhub_detail_message_view:I = 0x7f0a0b13

.field public static final orderhub_detail_order_display_state:I = 0x7f0a0b14

.field public static final orderhub_detail_order_id:I = 0x7f0a0b15

.field public static final orderhub_detail_order_placed_at:I = 0x7f0a0b16

.field public static final orderhub_detail_order_quick_action:I = 0x7f0a0b17

.field public static final orderhub_detail_order_quick_action_failure:I = 0x7f0a0b18

.field public static final orderhub_detail_order_row:I = 0x7f0a0b19

.field public static final orderhub_detail_order_title:I = 0x7f0a0b1a

.field public static final orderhub_detail_orders_not_syncing_message:I = 0x7f0a0b1b

.field public static final orderhub_detail_orders_not_syncing_title:I = 0x7f0a0b1c

.field public static final orderhub_detail_orders_recycler_view:I = 0x7f0a0b1d

.field public static final orderhub_detail_search:I = 0x7f0a0b1e

.field public static final orderhub_detail_search_error_message:I = 0x7f0a0b1f

.field public static final orderhub_detail_search_instructions:I = 0x7f0a0b20

.field public static final orderhub_detail_view:I = 0x7f0a0b21

.field public static final orderhub_filter_recycler_view:I = 0x7f0a0b22

.field public static final orderhub_fulfillments_section:I = 0x7f0a0b23

.field public static final orderhub_item_selection_items_list:I = 0x7f0a0b24

.field public static final orderhub_items_section:I = 0x7f0a0b25

.field public static final orderhub_items_section_header:I = 0x7f0a0b26

.field public static final orderhub_master_filter_row:I = 0x7f0a0b27

.field public static final orderhub_note_characters_remaining:I = 0x7f0a0b28

.field public static final orderhub_note_edit_text:I = 0x7f0a0b29

.field public static final orderhub_order_add_tracking:I = 0x7f0a0b2a

.field public static final orderhub_order_adjust_fulfillment_time_button:I = 0x7f0a0b2b

.field public static final orderhub_order_adjust_pickup_time_recycler_view:I = 0x7f0a0b2c

.field public static final orderhub_order_buttons_container:I = 0x7f0a0b2d

.field public static final orderhub_order_cancelation_reason_recycler_view:I = 0x7f0a0b2e

.field public static final orderhub_order_cancellation_spinner_container:I = 0x7f0a0b2f

.field public static final orderhub_order_cancellation_spinner_message:I = 0x7f0a0b30

.field public static final orderhub_order_cancellation_spinner_refund_selection:I = 0x7f0a0b31

.field public static final orderhub_order_container:I = 0x7f0a0b32

.field public static final orderhub_order_created_date:I = 0x7f0a0b33

.field public static final orderhub_order_customer_container:I = 0x7f0a0b34

.field public static final orderhub_order_customer_email:I = 0x7f0a0b35

.field public static final orderhub_order_customer_header:I = 0x7f0a0b36

.field public static final orderhub_order_customer_name:I = 0x7f0a0b37

.field public static final orderhub_order_customer_phone:I = 0x7f0a0b38

.field public static final orderhub_order_detail_animator:I = 0x7f0a0b39

.field public static final orderhub_order_detail_scrollview:I = 0x7f0a0b3a

.field public static final orderhub_order_detail_spinner:I = 0x7f0a0b3b

.field public static final orderhub_order_fulfillment_time:I = 0x7f0a0b3c

.field public static final orderhub_order_fulfillment_time_container:I = 0x7f0a0b3d

.field public static final orderhub_order_fulfillment_time_header:I = 0x7f0a0b3e

.field public static final orderhub_order_id:I = 0x7f0a0b3f

.field public static final orderhub_order_id_container:I = 0x7f0a0b40

.field public static final orderhub_order_id_header:I = 0x7f0a0b41

.field public static final orderhub_order_item_name_description_section:I = 0x7f0a0b42

.field public static final orderhub_order_item_row:I = 0x7f0a0b43

.field public static final orderhub_order_item_row_item_name:I = 0x7f0a0b44

.field public static final orderhub_order_item_row_item_price:I = 0x7f0a0b45

.field public static final orderhub_order_item_row_item_quantity:I = 0x7f0a0b46

.field public static final orderhub_order_item_row_item_subtitle:I = 0x7f0a0b47

.field public static final orderhub_order_items_container:I = 0x7f0a0b48

.field public static final orderhub_order_items_header:I = 0x7f0a0b49

.field public static final orderhub_order_items_header_detail:I = 0x7f0a0b4a

.field public static final orderhub_order_note:I = 0x7f0a0b4b

.field public static final orderhub_order_note_container:I = 0x7f0a0b4c

.field public static final orderhub_order_price_container:I = 0x7f0a0b4d

.field public static final orderhub_order_price_row_label:I = 0x7f0a0b4e

.field public static final orderhub_order_price_row_value:I = 0x7f0a0b4f

.field public static final orderhub_order_primary_action:I = 0x7f0a0b50

.field public static final orderhub_order_recipient_address_body:I = 0x7f0a0b51

.field public static final orderhub_order_recipient_address_container:I = 0x7f0a0b52

.field public static final orderhub_order_recipient_address_header:I = 0x7f0a0b53

.field public static final orderhub_order_recipient_address_name:I = 0x7f0a0b54

.field public static final orderhub_order_secondary_action:I = 0x7f0a0b55

.field public static final orderhub_order_secondary_action_button_container:I = 0x7f0a0b56

.field public static final orderhub_order_shipment_method:I = 0x7f0a0b57

.field public static final orderhub_order_shipment_method_container:I = 0x7f0a0b58

.field public static final orderhub_order_shipment_method_header:I = 0x7f0a0b59

.field public static final orderhub_order_shipping_spinner_container:I = 0x7f0a0b5a

.field public static final orderhub_order_source_icon:I = 0x7f0a0b5b

.field public static final orderhub_order_spacing_before_price:I = 0x7f0a0b5c

.field public static final orderhub_order_spacing_before_tenders:I = 0x7f0a0b5d

.field public static final orderhub_order_status:I = 0x7f0a0b5e

.field public static final orderhub_order_status_view:I = 0x7f0a0b5f

.field public static final orderhub_order_tender_amount:I = 0x7f0a0b60

.field public static final orderhub_order_tender_details:I = 0x7f0a0b61

.field public static final orderhub_order_tenders_container:I = 0x7f0a0b62

.field public static final orderhub_order_tracking_container:I = 0x7f0a0b63

.field public static final orderhub_order_tracking_edit:I = 0x7f0a0b64

.field public static final orderhub_order_tracking_info:I = 0x7f0a0b65

.field public static final orderhub_order_tracking_label:I = 0x7f0a0b66

.field public static final orderhub_order_tracking_spinner:I = 0x7f0a0b67

.field public static final orderhub_recycler_title_row:I = 0x7f0a0b6c

.field public static final orderhub_reveal_search_icon:I = 0x7f0a0b6d

.field public static final orderhub_search_results_spinner:I = 0x7f0a0b6e

.field public static final quick_action_spinner:I = 0x7f0a0ca7


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
