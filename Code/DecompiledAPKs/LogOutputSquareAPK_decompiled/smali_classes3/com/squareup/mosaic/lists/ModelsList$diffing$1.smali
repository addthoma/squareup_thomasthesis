.class final Lcom/squareup/mosaic/lists/ModelsList$diffing$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ModelsList.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mosaic/lists/ModelsList;->diffing(Ljava/util/List;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u000e\u0008\u0000\u0010\u0002*\u0008\u0012\u0004\u0012\u0002H\u00040\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "Lcom/squareup/mosaic/core/UiModel;",
        "P",
        "Lcom/squareup/mosaic/lists/IdParams;",
        "unchangedOldPos",
        "",
        "unchangedNewPos",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $newList:Ljava/util/List;

.field final synthetic $oldList:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/mosaic/lists/ModelsList;


# direct methods
.method constructor <init>(Lcom/squareup/mosaic/lists/ModelsList;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$1;->this$0:Lcom/squareup/mosaic/lists/ModelsList;

    iput-object p2, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$1;->$oldList:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$1;->$newList:Ljava/util/List;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/lists/ModelsList$diffing$1;->invoke(II)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(II)V
    .locals 3

    .line 166
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$1;->this$0:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {v0}, Lcom/squareup/mosaic/lists/ModelsList;->getCallback()Lcom/squareup/mosaic/lists/ModelsList$Changes;

    move-result-object v0

    .line 168
    iget-object v1, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$1;->$oldList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 170
    iget-object v2, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$1;->$newList:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 166
    invoke-interface {v0, p1, v1, p2, v2}, Lcom/squareup/mosaic/lists/ModelsList$Changes;->onUnchanged(ILjava/lang/Object;ILjava/lang/Object;)V

    return-void
.end method
