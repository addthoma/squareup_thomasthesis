.class public final Lcom/squareup/mosaic/lists/ModelsList$diffing$results$1;
.super Ljava/lang/Object;
.source "ModelsList.kt"

# interfaces
.implements Lcom/squareup/mosaic/lists/DiffUtilX$Results;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mosaic/lists/ModelsList;->diffing(Ljava/util/List;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/mosaic/lists/DiffUtilX$Results<",
        "TT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nModelsList.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ModelsList.kt\ncom/squareup/mosaic/lists/ModelsList$diffing$results$1\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,196:1\n8918#2:197\n9251#2,3:198\n8918#2:204\n9251#2,3:205\n1600#3,3:201\n*E\n*S KotlinDebug\n*F\n+ 1 ModelsList.kt\ncom/squareup/mosaic/lists/ModelsList$diffing$results$1\n*L\n131#1:197\n131#1,3:198\n143#1:204\n143#1,3:205\n132#1,3:201\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0015\n\u0002\u0008\u0004*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00028\u00000\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0018\u0010\u0008\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0018\u0010\t\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u0005H\u0016\u00a8\u0006\u000b"
    }
    d2 = {
        "com/squareup/mosaic/lists/ModelsList$diffing$results$1",
        "Lcom/squareup/mosaic/lists/DiffUtilX$Results;",
        "onChanged",
        "",
        "position",
        "",
        "positionsInNewList",
        "",
        "onInserted",
        "onRemoved",
        "count",
        "mosaic-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $newList:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/mosaic/lists/ModelsList;


# direct methods
.method constructor <init>(Lcom/squareup/mosaic/lists/ModelsList;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List;",
            ")V"
        }
    .end annotation

    .line 125
    iput-object p1, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$results$1;->this$0:Lcom/squareup/mosaic/lists/ModelsList;

    iput-object p2, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$results$1;->$newList:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged(I[I)V
    .locals 6

    const-string v0, "positionsInNewList"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 198
    array-length v1, p2

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    aget v4, p2, v3

    .line 131
    iget-object v5, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$results$1;->$newList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/mosaic/core/UiModel;

    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 200
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 132
    move-object p2, v0

    check-cast p2, Ljava/lang/Iterable;

    .line 202
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v3, v2, 0x1

    if-gez v2, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_1
    check-cast v1, Lcom/squareup/mosaic/core/UiModel;

    .line 133
    iget-object v4, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$results$1;->this$0:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {v4}, Lcom/squareup/mosaic/lists/ModelsList;->getSubViews()Ljava/util/List;

    move-result-object v4

    add-int/2addr v2, p1

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/mosaic/core/ViewRef;

    .line 134
    invoke-static {v2, v1}, Lcom/squareup/mosaic/core/ViewRefKt;->castAndBind(Lcom/squareup/mosaic/core/ViewRef;Lcom/squareup/mosaic/core/UiModel;)V

    move v2, v3

    goto :goto_1

    .line 136
    :cond_2
    iget-object p2, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$results$1;->this$0:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {p2}, Lcom/squareup/mosaic/lists/ModelsList;->getCallback()Lcom/squareup/mosaic/lists/ModelsList$Changes;

    move-result-object p2

    invoke-interface {p2, p1, v0}, Lcom/squareup/mosaic/lists/ModelsList$Changes;->onChanged(ILjava/util/List;)V

    return-void
.end method

.method public onInserted(I[I)V
    .locals 5

    const-string v0, "positionsInNewList"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 205
    array-length v1, p2

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget v3, p2, v2

    .line 143
    iget-object v4, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$results$1;->$newList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/mosaic/core/UiModel;

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 207
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 144
    iget-object p2, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$results$1;->this$0:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {p2, v0}, Lcom/squareup/mosaic/lists/ModelsList;->toViews(Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    .line 145
    iget-object v1, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$results$1;->this$0:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {v1}, Lcom/squareup/mosaic/lists/ModelsList;->getSubViews()Ljava/util/List;

    move-result-object v1

    move-object v2, p2

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v1, p1, v2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 146
    iget-object v1, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$results$1;->this$0:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {v1}, Lcom/squareup/mosaic/lists/ModelsList;->getCallback()Lcom/squareup/mosaic/lists/ModelsList$Changes;

    move-result-object v1

    .line 149
    check-cast p2, Ljava/lang/Iterable;

    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object p2

    sget-object v2, Lcom/squareup/mosaic/lists/ModelsList$diffing$results$1$onInserted$1;->INSTANCE:Lkotlin/reflect/KProperty1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v2}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p2

    .line 146
    invoke-interface {v1, p1, v0, p2}, Lcom/squareup/mosaic/lists/ModelsList$Changes;->onInserted(ILjava/util/List;Lkotlin/sequences/Sequence;)V

    return-void
.end method

.method public onRemoved(II)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    .line 158
    iget-object v1, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$results$1;->this$0:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {v1}, Lcom/squareup/mosaic/lists/ModelsList;->getSubViews()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList$diffing$results$1;->this$0:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {v0}, Lcom/squareup/mosaic/lists/ModelsList;->getCallback()Lcom/squareup/mosaic/lists/ModelsList$Changes;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/mosaic/lists/ModelsList$Changes;->onRemoved(II)V

    return-void
.end method
