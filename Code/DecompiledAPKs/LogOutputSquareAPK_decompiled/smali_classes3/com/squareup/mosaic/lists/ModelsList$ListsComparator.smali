.class final Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;
.super Ljava/lang/Object;
.source "ModelsList.kt"

# interfaces
.implements Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mosaic/lists/ModelsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ListsComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/mosaic/core/UiModel<",
        "TP;>;P::",
        "Lcom/squareup/mosaic/lists/IdParams;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nModelsList.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ModelsList.kt\ncom/squareup/mosaic/lists/ModelsList$ListsComparator\n*L\n1#1,196:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0015\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008\u0002\u0018\u0000*\u000e\u0008\u0002\u0010\u0001*\u0008\u0012\u0004\u0012\u0002H\u00030\u0002*\u0008\u0008\u0003\u0010\u0003*\u00020\u00042\u00020\u0005BQ\u0012\u0014\u0010\u0006\u001a\u0010\u0012\u000c\u0012\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00080\u0007\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00028\u00020\u0007\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00028\u00020\u0007\u0012\u0018\u0010\u000b\u001a\u0014\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000c\u00a2\u0006\u0002\u0010\u000fJ\u0018\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\r2\u0006\u0010$\u001a\u00020\rH\u0016J\u0018\u0010%\u001a\u00020\"2\u0006\u0010#\u001a\u00020\r2\u0006\u0010$\u001a\u00020\rH\u0016R\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00028\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\u00020\rX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0017\u0010\t\u001a\u0008\u0012\u0004\u0012\u00028\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0011R\u0014\u0010\u001a\u001a\u00020\rX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0014R\u0011\u0010\u001c\u001a\u00020\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u0018R\u001f\u0010\u0006\u001a\u0010\u0012\u000c\u0012\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u0011R#\u0010\u000b\u001a\u0014\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 \u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;",
        "T",
        "Lcom/squareup/mosaic/core/UiModel;",
        "P",
        "Lcom/squareup/mosaic/lists/IdParams;",
        "Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;",
        "subViewRefs",
        "",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "oldList",
        "newList",
        "unchangedBlock",
        "Lkotlin/Function2;",
        "",
        "",
        "(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lkotlin/jvm/functions/Function2;)V",
        "getNewList",
        "()Ljava/util/List;",
        "newListSize",
        "getNewListSize",
        "()I",
        "newMatched",
        "",
        "getNewMatched",
        "()[I",
        "getOldList",
        "oldListSize",
        "getOldListSize",
        "oldMatched",
        "getOldMatched",
        "getSubViewRefs",
        "getUnchangedBlock",
        "()Lkotlin/jvm/functions/Function2;",
        "areContentsTheSame",
        "",
        "oldPos",
        "newPos",
        "areItemsTheSame",
        "mosaic-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final newList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final newListSize:I

.field private final newMatched:[I

.field private final oldList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final oldListSize:I

.field private final oldMatched:[I

.field private final subViewRefs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;>;"
        }
    .end annotation
.end field

.field private final unchangedBlock:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;>;",
            "Ljava/util/List<",
            "+TT;>;",
            "Ljava/util/List<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "subViewRefs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "oldList"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newList"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unchangedBlock"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->subViewRefs:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->oldList:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->newList:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->unchangedBlock:Lkotlin/jvm/functions/Function2;

    .line 69
    iget-object p1, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->oldList:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    iput p1, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->oldListSize:I

    .line 70
    iget-object p1, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->newList:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    iput p1, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->newListSize:I

    .line 73
    iget-object p1, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->oldList:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    new-array p2, p1, [I

    const/4 p3, 0x0

    const/4 p4, 0x0

    :goto_0
    const/4 v0, -0x1

    if-ge p4, p1, :cond_0

    aput v0, p2, p4

    add-int/lit8 p4, p4, 0x1

    goto :goto_0

    :cond_0
    iput-object p2, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->oldMatched:[I

    .line 75
    iget-object p1, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->newList:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    new-array p2, p1, [I

    :goto_1
    if-ge p3, p1, :cond_1

    aput v0, p2, p3

    add-int/lit8 p3, p3, 0x1

    goto :goto_1

    :cond_1
    iput-object p2, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->newMatched:[I

    return-void
.end method


# virtual methods
.method public areContentsTheSame(II)Z
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->oldList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mosaic/core/UiModel;

    iget-object v1, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->newList:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/mosaic/core/UiModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v1, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->unchangedBlock:Lkotlin/jvm/functions/Function2;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v1, p1, p2}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return v0
.end method

.method public areItemsTheSame(II)Z
    .locals 5

    .line 91
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->oldMatched:[I

    aget v0, v0, p1

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->newMatched:[I

    aget v0, v0, p2

    if-eq v0, v3, :cond_0

    goto :goto_1

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->oldList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mosaic/core/UiModel;

    .line 96
    iget-object v3, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->newList:Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/mosaic/core/UiModel;

    .line 97
    iget-object v4, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->subViewRefs:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/mosaic/core/ViewRef;

    .line 99
    invoke-static {v0, v3}, Lcom/squareup/mosaic/lists/IdParamsKt;->sameIdentityAs(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v4, v3}, Lcom/squareup/mosaic/core/ViewRef;->canAccept(Lcom/squareup/mosaic/core/UiModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    .line 101
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->oldMatched:[I

    aput p2, v0, p1

    .line 102
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->newMatched:[I

    aput p1, v0, p2

    :cond_2
    return v1

    .line 92
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->oldMatched:[I

    aget v0, v0, p1

    if-ne v0, p2, :cond_4

    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->newMatched:[I

    aget p2, v0, p2

    if-ne p2, p1, :cond_4

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    :goto_2
    return v1
.end method

.method public final getNewList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->newList:Ljava/util/List;

    return-object v0
.end method

.method public getNewListSize()I
    .locals 1

    .line 70
    iget v0, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->newListSize:I

    return v0
.end method

.method public final getNewMatched()[I
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->newMatched:[I

    return-object v0
.end method

.method public final getOldList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->oldList:Ljava/util/List;

    return-object v0
.end method

.method public getOldListSize()I
    .locals 1

    .line 69
    iget v0, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->oldListSize:I

    return v0
.end method

.method public final getOldMatched()[I
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->oldMatched:[I

    return-object v0
.end method

.method public final getSubViewRefs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;>;"
        }
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->subViewRefs:Ljava/util/List;

    return-object v0
.end method

.method public final getUnchangedBlock()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;->unchangedBlock:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method
