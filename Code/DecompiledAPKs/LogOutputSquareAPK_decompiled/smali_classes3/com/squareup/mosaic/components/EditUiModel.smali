.class public final Lcom/squareup/mosaic/components/EditUiModel;
.super Lcom/squareup/mosaic/core/StandardUiModel;
.source "EditUiModel.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/mosaic/core/StandardUiModel<",
        "Lcom/squareup/noho/NohoEditRow;",
        "TP;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u001f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002H\u00010\u0003B[\u0012\u0006\u0010\u0005\u001a\u00028\u0000\u0012\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u000e\u0008\u0002\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\u0016\u0008\u0002\u0010\u000c\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\r\u0012\u0008\u0008\u0003\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u000e\u0010(\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010 J\u000f\u0010)\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u00c6\u0003J\u000f\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u00c6\u0003J\u000b\u0010+\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003J\u0017\u0010,\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\rH\u00c6\u0003J\t\u0010-\u001a\u00020\u0011H\u00c6\u0003Jl\u0010.\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0008\u0008\u0002\u0010\u0005\u001a\u00028\u00002\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u000e\u0008\u0002\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0016\u0008\u0002\u0010\u000c\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\r2\u0008\u0008\u0003\u0010\u0010\u001a\u00020\u0011H\u00c6\u0001\u00a2\u0006\u0002\u0010/J\u0018\u00100\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003012\u0006\u00102\u001a\u000203H\u0016J\u0013\u00104\u001a\u0002052\u0008\u00106\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u00107\u001a\u00020\u0011H\u00d6\u0001J\t\u00108\u001a\u00020\u000eH\u00d6\u0001R \u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R\u001a\u0010\u0010\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\"\u0004\u0008\u0019\u0010\u001aR(\u0010\u000c\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\"\u0004\u0008\u001d\u0010\u001eR\u0016\u0010\u0005\u001a\u00028\u0000X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010!\u001a\u0004\u0008\u001f\u0010 R\u001c\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\"\u0010#\"\u0004\u0008$\u0010%R \u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008&\u0010\u0014\"\u0004\u0008\'\u0010\u0016\u00a8\u00069"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/EditUiModel;",
        "P",
        "",
        "Lcom/squareup/mosaic/core/StandardUiModel;",
        "Lcom/squareup/noho/NohoEditRow;",
        "params",
        "hint",
        "Lcom/squareup/resources/TextModel;",
        "",
        "text",
        "position",
        "Lcom/squareup/noho/NohoEditRow$PositionInList;",
        "onChange",
        "Lkotlin/Function1;",
        "",
        "",
        "leftIcon",
        "",
        "(Ljava/lang/Object;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoEditRow$PositionInList;Lkotlin/jvm/functions/Function1;I)V",
        "getHint",
        "()Lcom/squareup/resources/TextModel;",
        "setHint",
        "(Lcom/squareup/resources/TextModel;)V",
        "getLeftIcon",
        "()I",
        "setLeftIcon",
        "(I)V",
        "getOnChange",
        "()Lkotlin/jvm/functions/Function1;",
        "setOnChange",
        "(Lkotlin/jvm/functions/Function1;)V",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "getPosition",
        "()Lcom/squareup/noho/NohoEditRow$PositionInList;",
        "setPosition",
        "(Lcom/squareup/noho/NohoEditRow$PositionInList;)V",
        "getText",
        "setText",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "(Ljava/lang/Object;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoEditRow$PositionInList;Lkotlin/jvm/functions/Function1;I)Lcom/squareup/mosaic/components/EditUiModel;",
        "createViewRef",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "context",
        "Landroid/content/Context;",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private hint:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private leftIcon:I

.field private onChange:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private position:Lcom/squareup/noho/NohoEditRow$PositionInList;

.field private text:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoEditRow$PositionInList;Lkotlin/jvm/functions/Function1;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/squareup/noho/NohoEditRow$PositionInList;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;I)V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hint"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "text"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/squareup/mosaic/core/StandardUiModel;-><init>()V

    iput-object p1, p0, Lcom/squareup/mosaic/components/EditUiModel;->params:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/mosaic/components/EditUiModel;->hint:Lcom/squareup/resources/TextModel;

    iput-object p3, p0, Lcom/squareup/mosaic/components/EditUiModel;->text:Lcom/squareup/resources/TextModel;

    iput-object p4, p0, Lcom/squareup/mosaic/components/EditUiModel;->position:Lcom/squareup/noho/NohoEditRow$PositionInList;

    iput-object p5, p0, Lcom/squareup/mosaic/components/EditUiModel;->onChange:Lkotlin/jvm/functions/Function1;

    iput p6, p0, Lcom/squareup/mosaic/components/EditUiModel;->leftIcon:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoEditRow$PositionInList;Lkotlin/jvm/functions/Function1;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_0

    .line 26
    sget-object p2, Lcom/squareup/resources/TextModel;->Companion:Lcom/squareup/resources/TextModel$Companion;

    invoke-virtual {p2}, Lcom/squareup/resources/TextModel$Companion;->getEmpty()Lcom/squareup/resources/FixedText;

    move-result-object p2

    check-cast p2, Lcom/squareup/resources/TextModel;

    :cond_0
    move-object v2, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_1

    .line 27
    sget-object p2, Lcom/squareup/resources/TextModel;->Companion:Lcom/squareup/resources/TextModel$Companion;

    invoke-virtual {p2}, Lcom/squareup/resources/TextModel$Companion;->getEmpty()Lcom/squareup/resources/FixedText;

    move-result-object p2

    move-object p3, p2

    check-cast p3, Lcom/squareup/resources/TextModel;

    :cond_1
    move-object v3, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_2

    .line 28
    sget-object p4, Lcom/squareup/noho/NohoEditRow$PositionInList;->MIDDLE:Lcom/squareup/noho/NohoEditRow$PositionInList;

    :cond_2
    move-object v4, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_3

    const/4 p2, 0x0

    .line 29
    move-object p5, p2

    check-cast p5, Lkotlin/jvm/functions/Function1;

    :cond_3
    move-object v5, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_4

    const/4 p6, 0x0

    const/4 v6, 0x0

    goto :goto_0

    :cond_4
    move v6, p6

    :goto_0
    move-object v0, p0

    move-object v1, p1

    .line 30
    invoke-direct/range {v0 .. v6}, Lcom/squareup/mosaic/components/EditUiModel;-><init>(Ljava/lang/Object;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoEditRow$PositionInList;Lkotlin/jvm/functions/Function1;I)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mosaic/components/EditUiModel;Ljava/lang/Object;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoEditRow$PositionInList;Lkotlin/jvm/functions/Function1;IILjava/lang/Object;)Lcom/squareup/mosaic/components/EditUiModel;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditUiModel;->getParams()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/mosaic/components/EditUiModel;->hint:Lcom/squareup/resources/TextModel;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/mosaic/components/EditUiModel;->text:Lcom/squareup/resources/TextModel;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/mosaic/components/EditUiModel;->position:Lcom/squareup/noho/NohoEditRow$PositionInList;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/mosaic/components/EditUiModel;->onChange:Lkotlin/jvm/functions/Function1;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget p6, p0, Lcom/squareup/mosaic/components/EditUiModel;->leftIcon:I

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/mosaic/components/EditUiModel;->copy(Ljava/lang/Object;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoEditRow$PositionInList;Lkotlin/jvm/functions/Function1;I)Lcom/squareup/mosaic/components/EditUiModel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/mosaic/components/EditUiModel;->hint:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final component3()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/mosaic/components/EditUiModel;->text:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final component4()Lcom/squareup/noho/NohoEditRow$PositionInList;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mosaic/components/EditUiModel;->position:Lcom/squareup/noho/NohoEditRow$PositionInList;

    return-object v0
.end method

.method public final component5()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/mosaic/components/EditUiModel;->onChange:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lcom/squareup/mosaic/components/EditUiModel;->leftIcon:I

    return v0
.end method

.method public final copy(Ljava/lang/Object;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoEditRow$PositionInList;Lkotlin/jvm/functions/Function1;I)Lcom/squareup/mosaic/components/EditUiModel;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/squareup/noho/NohoEditRow$PositionInList;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;I)",
            "Lcom/squareup/mosaic/components/EditUiModel<",
            "TP;>;"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hint"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "text"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/mosaic/components/EditUiModel;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/mosaic/components/EditUiModel;-><init>(Ljava/lang/Object;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoEditRow$PositionInList;Lkotlin/jvm/functions/Function1;I)V

    return-object v0
.end method

.method public createViewRef(Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v0, Lcom/squareup/mosaic/components/EditViewRef;

    invoke-direct {v0, p1}, Lcom/squareup/mosaic/components/EditViewRef;-><init>(Landroid/content/Context;)V

    check-cast v0, Lcom/squareup/mosaic/core/ViewRef;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mosaic/components/EditUiModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mosaic/components/EditUiModel;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mosaic/components/EditUiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/components/EditUiModel;->hint:Lcom/squareup/resources/TextModel;

    iget-object v1, p1, Lcom/squareup/mosaic/components/EditUiModel;->hint:Lcom/squareup/resources/TextModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/components/EditUiModel;->text:Lcom/squareup/resources/TextModel;

    iget-object v1, p1, Lcom/squareup/mosaic/components/EditUiModel;->text:Lcom/squareup/resources/TextModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/components/EditUiModel;->position:Lcom/squareup/noho/NohoEditRow$PositionInList;

    iget-object v1, p1, Lcom/squareup/mosaic/components/EditUiModel;->position:Lcom/squareup/noho/NohoEditRow$PositionInList;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/components/EditUiModel;->onChange:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/mosaic/components/EditUiModel;->onChange:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/mosaic/components/EditUiModel;->leftIcon:I

    iget p1, p1, Lcom/squareup/mosaic/components/EditUiModel;->leftIcon:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHint()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/mosaic/components/EditUiModel;->hint:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final getLeftIcon()I
    .locals 1

    .line 30
    iget v0, p0, Lcom/squareup/mosaic/components/EditUiModel;->leftIcon:I

    return v0
.end method

.method public final getOnChange()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/mosaic/components/EditUiModel;->onChange:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/mosaic/components/EditUiModel;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public final getPosition()Lcom/squareup/noho/NohoEditRow$PositionInList;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/mosaic/components/EditUiModel;->position:Lcom/squareup/noho/NohoEditRow$PositionInList;

    return-object v0
.end method

.method public final getText()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/mosaic/components/EditUiModel;->text:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/components/EditUiModel;->hint:Lcom/squareup/resources/TextModel;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/components/EditUiModel;->text:Lcom/squareup/resources/TextModel;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/components/EditUiModel;->position:Lcom/squareup/noho/NohoEditRow$PositionInList;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/components/EditUiModel;->onChange:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/mosaic/components/EditUiModel;->leftIcon:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final setHint(Lcom/squareup/resources/TextModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iput-object p1, p0, Lcom/squareup/mosaic/components/EditUiModel;->hint:Lcom/squareup/resources/TextModel;

    return-void
.end method

.method public final setLeftIcon(I)V
    .locals 0

    .line 30
    iput p1, p0, Lcom/squareup/mosaic/components/EditUiModel;->leftIcon:I

    return-void
.end method

.method public final setOnChange(Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 29
    iput-object p1, p0, Lcom/squareup/mosaic/components/EditUiModel;->onChange:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setPosition(Lcom/squareup/noho/NohoEditRow$PositionInList;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/squareup/mosaic/components/EditUiModel;->position:Lcom/squareup/noho/NohoEditRow$PositionInList;

    return-void
.end method

.method public final setText(Lcom/squareup/resources/TextModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iput-object p1, p0, Lcom/squareup/mosaic/components/EditUiModel;->text:Lcom/squareup/resources/TextModel;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EditUiModel(params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditUiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", hint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/components/EditUiModel;->hint:Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/components/EditUiModel;->text:Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", position="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/components/EditUiModel;->position:Lcom/squareup/noho/NohoEditRow$PositionInList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onChange="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/components/EditUiModel;->onChange:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", leftIcon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/mosaic/components/EditUiModel;->leftIcon:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
