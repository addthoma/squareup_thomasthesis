.class public final Lcom/squareup/mosaic/components/LabelUiModel;
.super Lcom/squareup/mosaic/core/StandardUiModel;
.source "LabelUiModel.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/mosaic/core/StandardUiModel<",
        "Lcom/squareup/noho/NohoLabel;",
        "TP;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0017\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002H\u00010\u0003B3\u0012\u0006\u0010\u0005\u001a\u00028\u0000\u0012\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0007\u0012\u000e\u0008\u0002\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u00a2\u0006\u0002\u0010\rJ\u000e\u0010\u001d\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000fJ\t\u0010\u001e\u001a\u00020\u0007H\u00c6\u0003J\u000f\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u00c6\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\u000cH\u00c6\u0003JD\u0010!\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0008\u0008\u0002\u0010\u0005\u001a\u00028\u00002\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u00072\u000e\u0008\u0002\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00c6\u0001\u00a2\u0006\u0002\u0010\"J\u0018\u0010#\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030$2\u0006\u0010%\u001a\u00020&H\u0016J\u0013\u0010\'\u001a\u00020(2\u0008\u0010)\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010*\u001a\u00020\u0007H\u00d6\u0001J\t\u0010+\u001a\u00020,H\u00d6\u0001R\u0016\u0010\u0005\u001a\u00028\u0000X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\u0008\u000e\u0010\u000fR\u001a\u0010\u0006\u001a\u00020\u0007X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014R \u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\"\u0004\u0008\u0017\u0010\u0018R\u001c\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\"\u0004\u0008\u001b\u0010\u001c\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/LabelUiModel;",
        "P",
        "",
        "Lcom/squareup/mosaic/core/StandardUiModel;",
        "Lcom/squareup/noho/NohoLabel;",
        "params",
        "styleRes",
        "",
        "text",
        "Lcom/squareup/resources/TextModel;",
        "",
        "type",
        "Lcom/squareup/noho/NohoLabel$Type;",
        "(Ljava/lang/Object;ILcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoLabel$Type;)V",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "getStyleRes",
        "()I",
        "setStyleRes",
        "(I)V",
        "getText",
        "()Lcom/squareup/resources/TextModel;",
        "setText",
        "(Lcom/squareup/resources/TextModel;)V",
        "getType",
        "()Lcom/squareup/noho/NohoLabel$Type;",
        "setType",
        "(Lcom/squareup/noho/NohoLabel$Type;)V",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "(Ljava/lang/Object;ILcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoLabel$Type;)Lcom/squareup/mosaic/components/LabelUiModel;",
        "createViewRef",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "context",
        "Landroid/content/Context;",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private styleRes:I

.field private text:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private type:Lcom/squareup/noho/NohoLabel$Type;


# direct methods
.method public constructor <init>(Ljava/lang/Object;ILcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoLabel$Type;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;I",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/squareup/noho/NohoLabel$Type;",
            ")V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "text"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/squareup/mosaic/core/StandardUiModel;-><init>()V

    iput-object p1, p0, Lcom/squareup/mosaic/components/LabelUiModel;->params:Ljava/lang/Object;

    iput p2, p0, Lcom/squareup/mosaic/components/LabelUiModel;->styleRes:I

    iput-object p3, p0, Lcom/squareup/mosaic/components/LabelUiModel;->text:Lcom/squareup/resources/TextModel;

    iput-object p4, p0, Lcom/squareup/mosaic/components/LabelUiModel;->type:Lcom/squareup/noho/NohoLabel$Type;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;ILcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoLabel$Type;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    .line 22
    sget-object p3, Lcom/squareup/resources/TextModel;->Companion:Lcom/squareup/resources/TextModel$Companion;

    invoke-virtual {p3}, Lcom/squareup/resources/TextModel$Companion;->getEmpty()Lcom/squareup/resources/FixedText;

    move-result-object p3

    check-cast p3, Lcom/squareup/resources/TextModel;

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    .line 23
    sget-object p4, Lcom/squareup/noho/NohoLabel$Type;->BODY:Lcom/squareup/noho/NohoLabel$Type;

    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/mosaic/components/LabelUiModel;-><init>(Ljava/lang/Object;ILcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoLabel$Type;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mosaic/components/LabelUiModel;Ljava/lang/Object;ILcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoLabel$Type;ILjava/lang/Object;)Lcom/squareup/mosaic/components/LabelUiModel;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/LabelUiModel;->getParams()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/LabelUiModel;->getStyleRes()I

    move-result p2

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/mosaic/components/LabelUiModel;->text:Lcom/squareup/resources/TextModel;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/mosaic/components/LabelUiModel;->type:Lcom/squareup/noho/NohoLabel$Type;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/mosaic/components/LabelUiModel;->copy(Ljava/lang/Object;ILcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoLabel$Type;)Lcom/squareup/mosaic/components/LabelUiModel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/LabelUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final component2()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/LabelUiModel;->getStyleRes()I

    move-result v0

    return v0
.end method

.method public final component3()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/mosaic/components/LabelUiModel;->text:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final component4()Lcom/squareup/noho/NohoLabel$Type;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mosaic/components/LabelUiModel;->type:Lcom/squareup/noho/NohoLabel$Type;

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;ILcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoLabel$Type;)Lcom/squareup/mosaic/components/LabelUiModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;I",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/squareup/noho/NohoLabel$Type;",
            ")",
            "Lcom/squareup/mosaic/components/LabelUiModel<",
            "TP;>;"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "text"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/mosaic/components/LabelUiModel;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/mosaic/components/LabelUiModel;-><init>(Ljava/lang/Object;ILcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoLabel$Type;)V

    return-object v0
.end method

.method public createViewRef(Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lcom/squareup/mosaic/components/LabelViewRef;

    invoke-direct {v0, p1}, Lcom/squareup/mosaic/components/LabelViewRef;-><init>(Landroid/content/Context;)V

    check-cast v0, Lcom/squareup/mosaic/core/ViewRef;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mosaic/components/LabelUiModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mosaic/components/LabelUiModel;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/LabelUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mosaic/components/LabelUiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/LabelUiModel;->getStyleRes()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/mosaic/components/LabelUiModel;->getStyleRes()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/components/LabelUiModel;->text:Lcom/squareup/resources/TextModel;

    iget-object v1, p1, Lcom/squareup/mosaic/components/LabelUiModel;->text:Lcom/squareup/resources/TextModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/components/LabelUiModel;->type:Lcom/squareup/noho/NohoLabel$Type;

    iget-object p1, p1, Lcom/squareup/mosaic/components/LabelUiModel;->type:Lcom/squareup/noho/NohoLabel$Type;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/squareup/mosaic/components/LabelUiModel;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public getStyleRes()I
    .locals 1

    .line 21
    iget v0, p0, Lcom/squareup/mosaic/components/LabelUiModel;->styleRes:I

    return v0
.end method

.method public final getText()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/squareup/mosaic/components/LabelUiModel;->text:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final getType()Lcom/squareup/noho/NohoLabel$Type;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/mosaic/components/LabelUiModel;->type:Lcom/squareup/noho/NohoLabel$Type;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/LabelUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/LabelUiModel;->getStyleRes()I

    move-result v2

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/components/LabelUiModel;->text:Lcom/squareup/resources/TextModel;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/components/LabelUiModel;->type:Lcom/squareup/noho/NohoLabel$Type;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public setStyleRes(I)V
    .locals 0

    .line 21
    iput p1, p0, Lcom/squareup/mosaic/components/LabelUiModel;->styleRes:I

    return-void
.end method

.method public final setText(Lcom/squareup/resources/TextModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/squareup/mosaic/components/LabelUiModel;->text:Lcom/squareup/resources/TextModel;

    return-void
.end method

.method public final setType(Lcom/squareup/noho/NohoLabel$Type;)V
    .locals 0

    .line 23
    iput-object p1, p0, Lcom/squareup/mosaic/components/LabelUiModel;->type:Lcom/squareup/noho/NohoLabel$Type;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LabelUiModel(params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/LabelUiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", styleRes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/LabelUiModel;->getStyleRes()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/components/LabelUiModel;->text:Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/components/LabelUiModel;->type:Lcom/squareup/noho/NohoLabel$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
