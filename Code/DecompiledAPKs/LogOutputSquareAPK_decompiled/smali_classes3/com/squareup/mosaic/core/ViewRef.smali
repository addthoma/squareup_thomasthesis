.class public abstract Lcom/squareup/mosaic/core/ViewRef;
.super Ljava/lang/Object;
.source "ViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lcom/squareup/mosaic/core/UiModel<",
        "*>;V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ViewRef.kt\ncom/squareup/mosaic/core/ViewRef\n*L\n1#1,189:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\n\n\u0002\u0010\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008&\u0018\u0000*\u000c\u0008\u0000\u0010\u0001*\u0006\u0012\u0002\u0008\u00030\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u00020\u0005B\u0005\u00a2\u0006\u0002\u0010\u0006J\u0013\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u0013\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u0018J\u0012\u0010\u001c\u001a\u00020\u00102\n\u0010\u001d\u001a\u0006\u0012\u0002\u0008\u00030\u0002J\u001d\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00028\u00002\u0006\u0010\u001d\u001a\u00028\u0000H\u0014\u00a2\u0006\u0002\u0010 J\u001f\u0010!\u001a\u00020\u001b2\u0008\u0010\"\u001a\u0004\u0018\u00018\u00002\u0006\u0010\u001d\u001a\u00028\u0000H&\u00a2\u0006\u0002\u0010#J\u0010\u0010$\u001a\u00020\u001b2\u0006\u0010%\u001a\u00020&H&J\u0008\u0010\'\u001a\u00020&H&R\u0012\u0010\u0007\u001a\u00028\u0001X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR(\u0010\n\u001a\u0010\u0012\u000c\u0012\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00000\u000b8VX\u0097\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u000c\u0010\u0006\u001a\u0004\u0008\r\u0010\u000eR\u001e\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\u0010@BX\u0084\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R(\u0010\u0013\u001a\u0004\u0018\u00018\u00008\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0016\n\u0002\u0010\u0019\u0012\u0004\u0008\u0014\u0010\u0006\u001a\u0004\u0008\u0015\u0010\u0016\"\u0004\u0008\u0017\u0010\u0018\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/mosaic/core/ViewRef;",
        "M",
        "Lcom/squareup/mosaic/core/UiModel;",
        "V",
        "Landroid/view/View;",
        "",
        "()V",
        "androidView",
        "getAndroidView",
        "()Landroid/view/View;",
        "children",
        "Lkotlin/sequences/Sequence;",
        "children$annotations",
        "getChildren",
        "()Lkotlin/sequences/Sequence;",
        "<set-?>",
        "",
        "isBinding",
        "()Z",
        "model",
        "model$annotations",
        "getModel",
        "()Lcom/squareup/mosaic/core/UiModel;",
        "setModel",
        "(Lcom/squareup/mosaic/core/UiModel;)V",
        "Lcom/squareup/mosaic/core/UiModel;",
        "bind",
        "",
        "canAccept",
        "newModel",
        "canUpdateTo",
        "currentModel",
        "(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)Z",
        "doBind",
        "oldModel",
        "(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V",
        "restoreInstanceState",
        "parcelable",
        "Landroid/os/Parcelable;",
        "saveInstanceState",
        "mosaic-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private isBinding:Z

.field private model:Lcom/squareup/mosaic/core/UiModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TM;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic children$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic model$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final bind(Lcom/squareup/mosaic/core/UiModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;)V"
        }
    .end annotation

    const-string v0, "model"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 41
    iput-boolean v0, p0, Lcom/squareup/mosaic/core/ViewRef;->isBinding:Z

    .line 42
    iget-object v0, p0, Lcom/squareup/mosaic/core/ViewRef;->model:Lcom/squareup/mosaic/core/UiModel;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/mosaic/core/ViewRef;->doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V

    .line 43
    iput-object p1, p0, Lcom/squareup/mosaic/core/ViewRef;->model:Lcom/squareup/mosaic/core/UiModel;

    const/4 p1, 0x0

    .line 44
    iput-boolean p1, p0, Lcom/squareup/mosaic/core/ViewRef;->isBinding:Z

    return-void
.end method

.method public final canAccept(Lcom/squareup/mosaic/core/UiModel;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "newModel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/mosaic/core/ViewRef;->model:Lcom/squareup/mosaic/core/UiModel;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 90
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0, p1}, Lcom/squareup/mosaic/core/ViewRef;->canUpdateTo(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method protected canUpdateTo(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;TM;)Z"
        }
    .end annotation

    const-string v0, "currentModel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "newModel"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1
.end method

.method public abstract doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;TM;)V"
        }
    .end annotation
.end method

.method public abstract getAndroidView()Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation
.end method

.method public getChildren()Lkotlin/sequences/Sequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/sequences/Sequence<",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;>;"
        }
    .end annotation

    .line 52
    invoke-static {}, Lkotlin/sequences/SequencesKt;->emptySequence()Lkotlin/sequences/Sequence;

    move-result-object v0

    return-object v0
.end method

.method public final getModel()Lcom/squareup/mosaic/core/UiModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TM;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/mosaic/core/ViewRef;->model:Lcom/squareup/mosaic/core/UiModel;

    return-object v0
.end method

.method protected final isBinding()Z
    .locals 1

    .line 30
    iget-boolean v0, p0, Lcom/squareup/mosaic/core/ViewRef;->isBinding:Z

    return v0
.end method

.method public abstract restoreInstanceState(Landroid/os/Parcelable;)V
.end method

.method public abstract saveInstanceState()Landroid/os/Parcelable;
.end method

.method public final setModel(Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;)V"
        }
    .end annotation

    .line 27
    iput-object p1, p0, Lcom/squareup/mosaic/core/ViewRef;->model:Lcom/squareup/mosaic/core/UiModel;

    return-void
.end method
