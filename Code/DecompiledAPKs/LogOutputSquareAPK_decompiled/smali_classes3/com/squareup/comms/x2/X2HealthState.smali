.class Lcom/squareup/comms/x2/X2HealthState;
.super Ljava/lang/Object;
.source "X2HealthState.java"


# instance fields
.field private final callback:Lcom/squareup/comms/HealthChecker$Callback;

.field private healthy:Z

.field private pendingRequestCount:I

.field private volatile stopped:Z


# direct methods
.method constructor <init>(Lcom/squareup/comms/HealthChecker$Callback;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/comms/x2/X2HealthState;->callback:Lcom/squareup/comms/HealthChecker$Callback;

    return-void
.end method


# virtual methods
.method isStopped()Z
    .locals 1

    .line 37
    iget-boolean v0, p0, Lcom/squareup/comms/x2/X2HealthState;->stopped:Z

    return v0
.end method

.method onRequestSend()V
    .locals 2

    .line 46
    iget v0, p0, Lcom/squareup/comms/x2/X2HealthState;->pendingRequestCount:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/comms/x2/X2HealthState;->pendingRequestCount:I

    .line 47
    iget v0, p0, Lcom/squareup/comms/x2/X2HealthState;->pendingRequestCount:I

    if-le v0, v1, :cond_0

    .line 48
    iget-boolean v0, p0, Lcom/squareup/comms/x2/X2HealthState;->healthy:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 49
    iput-boolean v0, p0, Lcom/squareup/comms/x2/X2HealthState;->healthy:Z

    .line 50
    iget-object v0, p0, Lcom/squareup/comms/x2/X2HealthState;->callback:Lcom/squareup/comms/HealthChecker$Callback;

    invoke-interface {v0}, Lcom/squareup/comms/HealthChecker$Callback;->onUnhealthy()V

    :cond_0
    return-void
.end method

.method onResponseReceived()V
    .locals 1

    const/4 v0, 0x0

    .line 60
    iput v0, p0, Lcom/squareup/comms/x2/X2HealthState;->pendingRequestCount:I

    .line 61
    iget-boolean v0, p0, Lcom/squareup/comms/x2/X2HealthState;->healthy:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 62
    iput-boolean v0, p0, Lcom/squareup/comms/x2/X2HealthState;->healthy:Z

    .line 63
    iget-object v0, p0, Lcom/squareup/comms/x2/X2HealthState;->callback:Lcom/squareup/comms/HealthChecker$Callback;

    invoke-interface {v0}, Lcom/squareup/comms/HealthChecker$Callback;->onHealthy()V

    :cond_0
    return-void
.end method

.method stop()V
    .locals 1

    const/4 v0, 0x0

    .line 28
    iput v0, p0, Lcom/squareup/comms/x2/X2HealthState;->pendingRequestCount:I

    .line 29
    iput-boolean v0, p0, Lcom/squareup/comms/x2/X2HealthState;->healthy:Z

    const/4 v0, 0x1

    .line 30
    iput-boolean v0, p0, Lcom/squareup/comms/x2/X2HealthState;->stopped:Z

    return-void
.end method
