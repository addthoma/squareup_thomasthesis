.class public Lcom/squareup/comms/x2/X2RemoteBusBuilder;
.super Lcom/squareup/comms/RemoteBusBuilder;
.source "X2RemoteBusBuilder.java"


# instance fields
.field private healthCheckIntervalMs:Ljava/lang/Integer;

.field private useHealthChecker:Z


# direct methods
.method private constructor <init>(Lcom/squareup/comms/Serializer;Ljava/lang/String;I)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/comms/RemoteBusBuilder;-><init>(Lcom/squareup/comms/Serializer;Ljava/lang/String;I)V

    const/4 p1, 0x1

    .line 11
    iput-boolean p1, p0, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->useHealthChecker:Z

    return-void
.end method

.method private constructor <init>(Lcom/squareup/comms/Serializer;Ljava/lang/String;ILjava/lang/String;Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;)V
    .locals 0

    .line 34
    invoke-direct/range {p0 .. p5}, Lcom/squareup/comms/RemoteBusBuilder;-><init>(Lcom/squareup/comms/Serializer;Ljava/lang/String;ILjava/lang/String;Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;)V

    const/4 p1, 0x1

    .line 11
    iput-boolean p1, p0, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->useHealthChecker:Z

    return-void
.end method

.method private static defaultSerializer()Lcom/squareup/comms/x2/X2Serializer;
    .locals 1

    .line 62
    new-instance v0, Lcom/squareup/comms/x2/X2Serializer;

    invoke-direct {v0}, Lcom/squareup/comms/x2/X2Serializer;-><init>()V

    return-object v0
.end method

.method public static forClient(Ljava/lang/String;ILjava/lang/String;)Lcom/squareup/comms/x2/X2RemoteBusBuilder;
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/comms/net/socket/SocketConnectionFailureListener$NoSocketConnectionErrorListener;

    invoke-direct {v0}, Lcom/squareup/comms/net/socket/SocketConnectionFailureListener$NoSocketConnectionErrorListener;-><init>()V

    invoke-static {p0, p1, p2, v0}, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->forClient(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;)Lcom/squareup/comms/x2/X2RemoteBusBuilder;

    move-result-object p0

    return-object p0
.end method

.method public static forClient(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;)Lcom/squareup/comms/x2/X2RemoteBusBuilder;
    .locals 7

    .line 20
    new-instance v6, Lcom/squareup/comms/x2/X2RemoteBusBuilder;

    invoke-static {}, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->defaultSerializer()Lcom/squareup/comms/x2/X2Serializer;

    move-result-object v1

    move-object v0, v6

    move-object v2, p0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/comms/x2/X2RemoteBusBuilder;-><init>(Lcom/squareup/comms/Serializer;Ljava/lang/String;ILjava/lang/String;Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;)V

    return-object v6
.end method

.method public static forServer(Ljava/lang/String;I)Lcom/squareup/comms/x2/X2RemoteBusBuilder;
    .locals 2

    .line 15
    new-instance v0, Lcom/squareup/comms/x2/X2RemoteBusBuilder;

    invoke-static {}, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->defaultSerializer()Lcom/squareup/comms/x2/X2Serializer;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Lcom/squareup/comms/x2/X2RemoteBusBuilder;-><init>(Lcom/squareup/comms/Serializer;Ljava/lang/String;I)V

    return-object v0
.end method


# virtual methods
.method public build()Lcom/squareup/comms/RemoteBus;
    .locals 3

    .line 49
    iget-boolean v0, p0, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->useHealthChecker:Z

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p0, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->healthCheckIntervalMs:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 51
    new-instance v0, Lcom/squareup/comms/x2/X2HealthChecker;

    iget-object v1, p0, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->ioThread:Lcom/squareup/comms/common/IoThread;

    iget-object v2, p0, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->healthCheckIntervalMs:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/x2/X2HealthChecker;-><init>(Lcom/squareup/comms/common/IoThread;I)V

    invoke-virtual {p0, v0}, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->healthChecker(Lcom/squareup/comms/HealthChecker;)V

    goto :goto_0

    .line 53
    :cond_0
    new-instance v0, Lcom/squareup/comms/x2/X2HealthChecker;

    iget-object v1, p0, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->ioThread:Lcom/squareup/comms/common/IoThread;

    invoke-direct {v0, v1}, Lcom/squareup/comms/x2/X2HealthChecker;-><init>(Lcom/squareup/comms/common/IoThread;)V

    invoke-virtual {p0, v0}, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->healthChecker(Lcom/squareup/comms/HealthChecker;)V

    goto :goto_0

    .line 56
    :cond_1
    new-instance v0, Lcom/squareup/comms/X2NullHealthChecker;

    invoke-direct {v0}, Lcom/squareup/comms/X2NullHealthChecker;-><init>()V

    invoke-virtual {p0, v0}, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->healthChecker(Lcom/squareup/comms/HealthChecker;)V

    .line 58
    :goto_0
    invoke-super {p0}, Lcom/squareup/comms/RemoteBusBuilder;->build()Lcom/squareup/comms/RemoteBus;

    move-result-object v0

    return-object v0
.end method

.method public useHealthChecker(Z)Lcom/squareup/comms/x2/X2RemoteBusBuilder;
    .locals 0

    .line 38
    iput-boolean p1, p0, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->useHealthChecker:Z

    return-object p0
.end method

.method public useHealthChecker(ZI)Lcom/squareup/comms/x2/X2RemoteBusBuilder;
    .locals 0

    .line 43
    iput-boolean p1, p0, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->useHealthChecker:Z

    .line 44
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->healthCheckIntervalMs:Ljava/lang/Integer;

    return-object p0
.end method
